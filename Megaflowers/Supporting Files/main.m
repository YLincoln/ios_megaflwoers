//
//  main.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_ERROR;

#ifdef DEBUG
void eHandler(NSException *);

void eHandler(NSException *exception) {
    DDLogError(@"\n‼️Exception\n%@\n", exception);
    DDLogError(@"\n‼️Exception name\n%@\n", [exception name]);
    DDLogError(@"\n‼️Exception reason\n%@\n", [exception reason]);
    DDLogError(@"\n‼️Exception callStackSymbols\n%@\n", [exception callStackSymbols]);
    DDLogError(@"\n‼️Exception callStackReturnAddresses\n%@\n", [exception callStackReturnAddresses]);
    DDLogError(@"\n‼️Exception userInfo\n%@", [exception userInfo]);
}
#endif

int main(int argc, char * argv[]) {
#ifdef DEBUG
    NSSetUncaughtExceptionHandler(&eHandler);
#endif
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
