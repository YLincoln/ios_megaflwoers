//
//  NSObject_Constants.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright (c) 2013 Megaflowers. All rights reserved.
//

#define YOUR_APP_STORE_ID 1
#define APP_RATE_URL [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%d", YOUR_APP_STORE_ID]]

#define ApplicationDelegate                             ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define WINDOW                                          [UIApplication sharedApplication].delegate.window
#define UserDefaults                                    [NSUserDefaults standardUserDefaults]
#define SharedApplication                               [UIApplication sharedApplication]
#define Bundle                                          [NSBundle mainBundle]
#define MainScreen                                      [UIScreen mainScreen]
#define ShowNetworkActivityIndicator()                  [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator()                  [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
#define NetworkActivityIndicatorVisible(x)              [UIApplication sharedApplication].networkActivityIndicatorVisible = x
#define NavBar                                          self.navigationController.navigationBar
#define TabBar                                          self.tabBarController.tabBar
#define NavBarHeight                                    self.navigationController.navigationBar.bounds.size.height
#define TabBarHeight                                    self.tabBarController.tabBar.bounds.size.height
#define ScreenWidth                                     [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                                    [[UIScreen mainScreen] bounds].size.height
#define TouchHeightDefault                              44
#define TouchHeightSmall                                32
#define ViewWidth(v)                                    v.frame.size.width
#define ViewHeight(v)                                   v.frame.size.height
#define ViewX(v)                                        v.frame.origin.x
#define ViewY(v)                                        v.frame.origin.y
#define SelfViewWidth                                   self.view.bounds.size.width
#define SelfViewHeight                                  self.view.bounds.size.height
#define RectX(f)                                        f.origin.x
#define RectY(f)                                        f.origin.y
#define RectWidth(f)                                    f.size.width
#define RectHeight(f)                                   f.size.height
#define RectSetWidth(f, w)                              CGRectMake(RectX(f), RectY(f), w, RectHeight(f))
#define RectSetHeight(f, h)                             CGRectMake(RectX(f), RectY(f), RectWidth(f), h)
#define RectSetX(f, x)                                  CGRectMake(x, RectY(f), RectWidth(f), RectHeight(f))
#define RectSetY(f, y)                                  CGRectMake(RectX(f), y, RectWidth(f), RectHeight(f))
#define RectSetSize(f, w, h)                            CGRectMake(RectX(f), RectY(f), w, h)
#define RectSetOrigin(f, x, y)                          CGRectMake(x, y, RectWidth(f), RectHeight(f))
#define DATE_COMPONENTS                                 NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
#define TIME_COMPONENTS                                 NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
#define RGB(r, g, b)                                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                                [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define weaken(object) __typeof__(self) __weak weakSelf = object
#define weakenObject(object, name) __typeof__(object) __weak name = object

#define METHOD_NAME [NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__]

#define _(text) NSLocalizedString (text, nil)
#define _s(className) NSStringFromClass([className class])

#define CLCOORDINATE_EPSILON 0.005f
#define CLCOORDINATES_EQUAL( coord1, coord2 ) (fabs(coord1.latitude - coord2.latitude) < CLCOORDINATE_EPSILON && fabs(coord1.longitude - coord2.longitude) < CLCOORDINATE_EPSILON)

#define IS_IPHONE_4                                     ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )
#define IS_IPHONE_5                                     ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6                                     ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667 ) < DBL_EPSILON )
#define IS_IPHONE_6_PLUS                                ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736 ) < DBL_EPSILON )

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)      ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define IOS_8                                           ([[UIDevice currentDevice].systemVersion floatValue] < 9.0)

#define keyPath(property) [[(@""#property) componentsSeparatedByString:@"."] lastObject]

//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
