//
//  NITServerAPIManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITServerAPIManager.h"
#import "SWGConfiguration.h"
#import "SWGUserApi.h"
#import "NITBusketBouquet.h"
#import "SWGLogger.h"
#import "SWGApiClient.h"
#import "NITLoginInteractor.h"
#import "NITLoginAPIDataManager.h"
#import "NITCitiesPresenter.h"
#import "NITPersonalDataAPIDataManager.h"
#import "SWGDefaultConfiguration.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "SWGBasketApi.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"
#import "NITCoupon.h"
#import "SWGDeviceApi.h"
#import "NITOrderModel.h"
#import <OneSignal/OneSignal.h>
#import "NITBasketProtocols.h"

@interface NITServerAPIManager ()

@property (nonatomic) Reachability * reachability;
@property (nonatomic) BOOL isOffline;
@property (nonatomic) NSString * accessToken;
@property (nonatomic) BOOL basketValidationInProggres;
@property (nonatomic) NSInteger basketValidationCounter;

@end

@implementation NITServerAPIManager

+ (NITServerAPIManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITServerAPIManager * shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setup
{
    [self setupCache];
    [self addObservers];
    [self setupReachability];
    self.basketValidationCounter = 0;
    self.basketValidationInProggres = false;
}

#pragma mark - Custom accessors
- (NSNumber *)site
{
    return @(4);
}

- (NSString *)imageKey
{
    return IS_IPHONE_6_PLUS ? @"high" : @"low";
}

- (NSString *)accessToken
{
    if (!_accessToken)
    {
        _accessToken = USER.appAccessToken;
    }
    
    return _accessToken;
}

#pragma mark - Puplic
- (void)updateCurrentSession
{
    if (USER.isAutorize)
    {
        [SWGDefaultConfiguration sharedConfig].accessToken = USER.accessToken;
        [[SWGDefaultConfiguration sharedConfig] setApiKey:USER.accessToken forApiKeyIdentifier:kAuthorizationKey];
        [self syncUserData];
    }
}

- (void)checkSessionError:(NSError *)error
{
    /*if ([USER isAutorize] && error.swaggerCode == 401)
    {
        if (HELPER.interfaceLoaded)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kResetSessionNotification object:nil];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self checkSessionError:error];
            });
        }
    }
    else*/ if (error.swaggerCode == kCFURLErrorNotConnectedToInternet && !self.isOffline)
    {
        if (HELPER.interfaceLoaded)
        {
            self.isOffline = true;
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Oк", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:WINDOW.visibleController withTitle:NSLocalizedString(@"Отсутствует соединение с сетью", nil) actionButtons:nil cancelButton:cancel];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self checkSessionError:error];
            });
        }
    }
}

- (NSString *)addAccessTokenForUrlString:(NSString *)urlString
{
    if (self.accessToken.length > 0)
    {
        NSRange range = [urlString rangeOfString:@"?"];
        if (range.location == NSNotFound)
        {
            urlString = [urlString stringByAppendingString:@"?access-token="];
        }
        else
        {
            urlString = [urlString stringByAppendingString:@"&access-token="];
        }
        
        urlString = [urlString stringByAppendingString:self.accessToken];
    }
    
    return urlString;
}

- (NSDictionary *)updateHeaderParams:(NSDictionary *)headerParams method:(NSString *)method
{
    if ([method isEqualToString:@"/basket"])
    {
        if ([USER isAutorize])
        {
            if (!headerParams)
            {
                return @{@"Authorization" : USER.accessToken};
            }
            else
            {
                NSMutableDictionary * params = [[NSMutableDictionary alloc] initWithDictionary:headerParams];
                [params setObject:USER.accessToken forKey:@"Authorization"];
                
                return params;
            }
        }
    }

    return headerParams;
}

- (NSArray *)updateAuthSettings:(NSArray *)authSettings method:(NSString *)method
{
    if ([method isEqualToString:@"/basket"])
    {
        if ([USER isAutorize])
        {
            if (!authSettings)
            {
                return @[@"bearer"];
            }
            else
            {
                NSMutableArray * params = [[NSMutableArray alloc] initWithArray:authSettings];
                [params addObject:@"bearer"];
                
                return params;
            }
        }
    }
    
    return authSettings;
}

#pragma mark - Image
- (void)imageByPath:(NSString *)path complition:(void(^)(UIImage *image))complition
{    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:path done:^(UIImage *image, SDImageCacheType cacheType) {
        
        if (!image)
        {
            [self downloadAndSaveImage:path withCompletion:^(UIImage *image){
                 if (complition) complition(image);
             }];
        }
        else
        {
            if (complition) complition(image);
        }
    }];
}

- (void)downloadAndSaveImage:(NSString *)urlString withCompletion:(void(^)(UIImage *image))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage * image = [UIImage imageWithData:imageData];
            if (image) [[SDImageCache sharedImageCache] storeImage:image forKey:urlString];
            if (completion) completion(image);
        });
    });
}

#pragma mark - App settings
- (void)loadSettingsIfNeed
{
    [[SWGDeviceApi new] settingsGetWithSite:self.site city:nil lang:HELPER.lang completionHandler:^(NSArray<SWGInlineResponse2005> *output, NSError *error) {
        for (SWGInlineResponse2005 * model in output)
        {
            if ([UserDefaults boolForKey:model.code]) continue;
            
            if ([model.code isEqualToString:@"defaultview"])
            {
                USER.viewType = [model.value isEqualToString:@"1"] ? CatalogItemsViewModeList : CatalogItemsViewModeSection;
                [UserDefaults setBool:true forKey:model.code];
            }
            else if ([model.code isEqualToString:@"defaultcity"])
            {
                USER.cityID = @([model.value integerValue]);
                [UserDefaults setBool:true forKey:model.code];
            }
            else if ([model.code isEqualToString:@"access-token"])
            {
                USER.appAccessToken = model.value;
                self.accessToken = model.value;
            }
        }
    }];
}

#pragma mark - Basket
- (void)validateBasket
{
    self.basketValidationCounter++;
    
    if (!self.basketValidationInProggres)
    {
        [self sendBasketValidation];
    }
}

- (void)sendBasketValidation
{
    self.basketValidationInProggres = true;
    [[NSNotificationCenter defaultCenter] postNotificationName:kStartBasketValidation object:nil];
    
    SWGBasketRequest * request = [self createBasket];
    
    if (request.bouquets.count > 0 || request.attaches.count > 0)
    {
        [HELPER startLoading];
        [HELPER logString:[NSString stringWithFormat:@"%@", request]];
        [[SWGBasketApi new] basketPutWithLang:HELPER.lang site:API.site city:USER.cityID basket:request completionHandler:^(SWGBasket *output, NSError *error) {
            [HELPER stopLoading];
            
            if (error)
            {
                [HELPER logError:error method:METHOD_NAME];
                NSDictionary * info = @{@"state" : @(OrderButtonStateInactive)};
                [[NSNotificationCenter defaultCenter] postNotificationName:kStopBasketValidation object:info];
                [self finishBasketValidation];
            }
            else if (output)
            {
                [NITBusketBouquet clean];
                [NITBusketAttachment clean];
                
                [NITBusketBouquet addBouquets:output.bouquets];
                [NITBusketAttachment addAttachments:output.attaches];
                [NITCoupon addBusketCoupon:output.coupon];
                [NITOrderModel new].deliveryPrice = output.delivery;
                
                USER.basketPrice = output.price;
                
                NSDictionary * info = @{@"state" : @(OrderButtonStateAvailable),
                                        @"price" : output.price};
                [[NSNotificationCenter defaultCenter] postNotificationName:kStopBasketValidation object:info];
                [self finishBasketValidation];
            }
        }];
    }
    else
    {
        NSDictionary * info = @{@"state" : @(OrderButtonStateDisable)};
        [[NSNotificationCenter defaultCenter] postNotificationName:kStopBasketValidation object:info];
        
        self.basketValidationInProggres = false;
        [self finishBasketValidation];
    }
}

- (void)finishBasketValidation
{
    self.basketValidationCounter--;
    self.basketValidationInProggres = false;
    
    if (self.basketValidationCounter > 0)
    {
        [self sendBasketValidation];
    }
}

- (SWGBasketRequest *)createBasket
{
    NSMutableArray * bouquets = [NSMutableArray new];
    NSMutableArray * attachments = [NSMutableArray new];
    SWGBasketRequest * request = [SWGBasketRequest new];
    
    for (NITBusketBouquet * bouquet in [NITBusketBouquet allBouquets])
    {
        [bouquets addObjectsFromArray:[[bouquet.sku array] bk_map:^id(NITBusketBouquetSku *sku) {
            
            SWGBasketRequestBouquets * requestBouquet = [SWGBasketRequestBouquets new];
            requestBouquet.skuId = @(sku.base_id.integerValue);
            requestBouquet.count = @(sku.count);
            requestBouquet.components = (id)[[sku.components array] bk_map:^id(NITBusketBouquetComponent *comp) {
                
                SWGBasketRequestComponents * requestComponent = [SWGBasketRequestComponents new];
                requestComponent._id = @(comp.identifier.integerValue);
                requestComponent.colorId = @(comp.color.integerValue);
                requestComponent.count = @(comp.count);
                
                return requestComponent;
            }];
            
            return requestBouquet;
        }]];
    }
    
    for (NITBusketAttachment * attachment in [NITBusketAttachment allAttachments])
    {
        [attachments addObjectsFromArray:[[attachment.sku array] bk_map:^id(NITBusketAttachmentSku *sku) {
            
            SWGBasketRequestBouquets * requestAttacment = [SWGBasketRequestBouquets new];
            requestAttacment.skuId = @(sku.identifier.integerValue);
            requestAttacment.count = @(sku.count);
            requestAttacment.components = (id)[[sku.components array] bk_map:^id(NITBusketAttachmentComponent *comp) {
                
                SWGBasketRequestComponents * requestComponent = [SWGBasketRequestComponents new];
                requestComponent._id = @(comp.identifier.integerValue);
                requestComponent.colorId = @(comp.color.integerValue);
                requestComponent.count = @(comp.count);
                
                return requestComponent;
            }];
            
            return requestAttacment;
        }]];
    }
    
    request.bouquets = (id)bouquets;
    request.attaches = (id)attachments;
    request.isPickup = @([NITOrderModel new].deliveryType);
    
    NITCoupon * coupon = [NITCoupon currentCoupon];
    if (coupon)
    {
        request.coupon = coupon.code;
    }
    
    return request;
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityChanged) name:kChangeCityNotification object:nil];
}

- (void)setupCache
{
    NSURLCache * URLCache = [[NSURLCache alloc] initWithMemoryCapacity:5 * 1024 * 1024
                                                          diskCapacity:50 * 1024 * 1024
                                                              diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
}

- (void)setupReachability
{
    if (self.reachability)
    {
        [self.reachability stopNotifier];
        self.reachability = nil;
    }
    self.reachability = [Reachability reachabilityWithHostname:@"http://www.google.com"];
    [self.reachability startNotifier];
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    if ([self.reachability currentReachabilityStatus] != NotReachable)
    {
        self.isOffline = false;
        [self loadSettingsIfNeed];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkConnectionRestored object:nil];
    }
}

#pragma mark - Current user
- (void)syncUserData
{
    NITLoginInteractor * loginInteractor = [NITLoginInteractor new];
    loginInteractor.APIDataManager = [NITLoginAPIDataManager new];
    [loginInteractor syncUserDataWithHandler:^(BOOL success, NSError *error) {
        [self sendPushTags];
    }];
}

- (void)cityChanged
{
    [TRACKER trackEvent:TrackerEventSelectCity parameters:@{@"id":USER.cityID}];
    [self validateBasket];
    
    if ([USER isAutorize])
    {
        NSMutableDictionary * userData = [NSMutableDictionary new];
        [userData setObject:@(USER.gender) forKey:keyPath(USER.gender)];
        if (USER.name)       [userData setObject:USER.name forKey:keyPath(USER.name)];
        if (USER.birthday)   [userData setObject:[USER.birthday serverString] forKey:keyPath(USER.birthday)];
        if (USER.cityID)     [userData setObject:USER.cityID forKey:keyPath(USER.cityID)];
        
        NITPersonalDataAPIDataManager * api = [NITPersonalDataAPIDataManager new];
        weaken(self);
        [api putUserData:userData withHandler:^(NSError *error) {
            [weakSelf updateCurrentSession];
        }];
    }
}

#pragma mark - Onesignal
- (void)sendPushTags
{
    NSMutableDictionary * tags = [NSMutableDictionary new];
    
    [tags setObject:HELPER.vendorUDID forKey:@"device_uuid"];
    [tags setObject:API.site forKey:@"site_id"];
    
    if (USER.identifier.length > 0)
    {
        [tags setObject:USER.identifier forKey:@"user_id"];
    }
    
    if (USER.cityID)
    {
        [tags setObject:USER.cityID forKey:@"city_id"];
    }
    
    if (USER.cityName)
    {
        [tags setObject:USER.cityName forKey:@"city_name"];
    }
    
    if (USER.countryID)
    {
        [tags setObject:USER.countryID forKey:@"country_id"];
    }
    
    if (USER.lastOrderID)
    {
        [tags setObject:USER.lastOrderID forKey:@"last_order_id"];
    }
    
    if (USER.lastOrderDate)
    {
        [tags setObject:USER.lastOrderDate forKey:@"last_order_date"];
    }
    
    if (tags.allKeys.count > 0)
    {
        [OneSignal sendTags:tags onSuccess:^(NSDictionary *result) {
            [HELPER logString:[NSString stringWithFormat:@"OneSignal sendTags success: %@", tags]];
        } onFailure:^(NSError *error) {
            [HELPER logError:error method:THIS_METHOD];
        }];
    }
}

@end
