//
//  NITServerAPIManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITServerAPIConstants.h"

@class SWGBasket, SWGBasketRequest;

#define API [NITServerAPIManager sharedInstance]

static NSString * const kResetSessionNotification   = @"reset_session_notification";
static NSString * const kNetworkConnectionRestored  = @"network_connection_restored";
static NSString * const kStartBasketValidation      = @"start_basket_validation";
static NSString * const kStopBasketValidation       = @"stop_basket_validation";
static NSString * const kUpdateBasketPrice          = @"update_basket_price";

@interface NITServerAPIManager : NSObject

@property (nonatomic) NSNumber * site;
@property (nonatomic) NSString * imageKey;

+ (NITServerAPIManager *)sharedInstance;

- (void)updateCurrentSession;
- (void)checkSessionError:(NSError *)error;
- (NSString *)addAccessTokenForUrlString:(NSString *)urlString;
- (NSDictionary *)updateHeaderParams:(NSDictionary *)headerParams method:(NSString *)method;
- (NSArray *)updateAuthSettings:(NSArray *)authSettings method:(NSString *)method;
- (void)loadSettingsIfNeed;
- (void)imageByPath:(NSString *)path complition:(void(^)(UIImage *image))complition;
- (void)validateBasket;
- (SWGBasketRequest *)createBasket;
- (void)sendPushTags;

@end
