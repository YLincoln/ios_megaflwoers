#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGReview
@end

@interface SWGReview : SWGObject

/* ID of bouquet [optional]
 */
@property(nonatomic) NSNumber* _id;
/* Name of user [optional]
 */
@property(nonatomic) NSString* name;
/* Date review [optional]
 */
@property(nonatomic) NSString* date;
/* Text review [optional]
 */
@property(nonatomic) NSString* message;
/* Count likes [optional]
 */
@property(nonatomic) NSNumber* likes;
/* Count dislikes [optional]
 */
@property(nonatomic) NSNumber* dislikes;
/* Rating review [optional]
 */
@property(nonatomic) NSNumber* rate;

@end
