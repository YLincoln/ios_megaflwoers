#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGSalonExterier
@end

@interface SWGSalonExterier : SWGObject

/* mobile image [optional]
 */
@property(nonatomic) NSString* low;
/* desktop middle image [optional]
 */
@property(nonatomic) NSString* medium;
/* desktop image [optional]
 */
@property(nonatomic) NSString* high;

@end
