#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGBasketRequest.h"
#import "SWGOrder.h"


@protocol SWGRequestOrder
@end

@interface SWGRequestOrder : SWGOrder


@property(nonatomic) SWGBasketRequest* basket;

@end
