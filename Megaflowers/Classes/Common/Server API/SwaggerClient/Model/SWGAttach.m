#import "SWGAttach.h"

@implementation SWGAttach

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"name": @"name", @"code": @"code", @"article": @"article", @"position": @"position", @"image1": @"image1", @"image2": @"image2", @"preview": @"preview", @"detail": @"detail", @"attachSectionId": @"attach_section_id", @"defaultSkuId": @"default_sku_id", @"sku": @"sku" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"code", @"article", @"position", @"image1", @"image2", @"preview", @"detail", @"attachSectionId", @"defaultSkuId", @"sku"];
  return [optionalProperties containsObject:propertyName];
}

@end
