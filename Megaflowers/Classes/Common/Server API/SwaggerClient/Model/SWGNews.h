#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGNews
@end

@interface SWGNews : SWGObject

/* ID of news [optional]
 */
@property(nonatomic) NSNumber* _id;
/* Name of news 
 */
@property(nonatomic) NSString* name;
/* Symbolic code of news [optional]
 */
@property(nonatomic) NSString* code;
/* Date of news [optional]
 */
@property(nonatomic) NSString* date;
/* Image of news [optional]
 */
@property(nonatomic) NSObject* image;
/* Preview text of news [optional]
 */
@property(nonatomic) NSString* preview;
/* Detail text of news [optional]
 */
@property(nonatomic) NSString* detail;

@end
