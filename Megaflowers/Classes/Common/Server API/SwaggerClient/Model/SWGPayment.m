#import "SWGPayment.h"

@implementation SWGPayment

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"code": @"code", @"position": @"position", @"manualLink": @"manual_link", @"_auto": @"auto", @"cash": @"cash", @"returnType": @"return_type", @"official": @"official", @"image": @"image", @"paymentSectionId": @"payment_section_id", @"currencyId": @"currency_id", @"amountLimit": @"amount_limit", @"canSdk": @"can_sdk", @"paymentType": @"payment_type", @"name": @"name", @"_description": @"description" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"code", @"position", @"manualLink", @"_auto", @"cash", @"returnType", @"official", @"image", @"paymentSectionId", @"currencyId", @"amountLimit", @"canSdk", @"paymentType", @"name", @"_description"];
  return [optionalProperties containsObject:propertyName];
}

@end
