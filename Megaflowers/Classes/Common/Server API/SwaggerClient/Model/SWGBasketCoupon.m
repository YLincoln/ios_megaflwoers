#import "SWGBasketCoupon.h"

@implementation SWGBasketCoupon

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"code": @"code", @"name": @"name", @"_description": @"description", @"value": @"value", @"type": @"type", @"dateStart": @"date_start", @"dateEnd": @"date_end", @"updatedAt": @"updated_at" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"code", @"name", @"_description", @"value", @"type", @"dateStart", @"dateEnd", @"updatedAt"];
  return [optionalProperties containsObject:propertyName];
}

@end
