#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/


#import "SWGBillAcquiring.h"
#import "SWGPayment.h"


@protocol SWGBill
@end

@interface SWGBill : SWGObject

/* Bill's ID [optional]
 */
@property(nonatomic) NSNumber* _id;
/* Bill's document ID [optional]
 */
@property(nonatomic) NSNumber* xmlId;
/* Parent's ID [optional]
 */
@property(nonatomic) NSNumber* toId;

@property(nonatomic) NSString* status;

@property(nonatomic) NSString* statusText;

@property(nonatomic) NSString* num;

@property(nonatomic) NSNumber* sum;
/* ID of payment system [optional]
 */
@property(nonatomic) NSNumber* paymentId;

@property(nonatomic) SWGPayment* payment;

@property(nonatomic) SWGBillAcquiring* acquiring;
/* ID transaction of payment system [optional]
 */
@property(nonatomic) NSString* transactionId;
/* ID transaction of payment system from SDK [optional]
 */
@property(nonatomic) NSString* transactionSdk;

@end
