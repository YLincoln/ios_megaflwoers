#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGAttachSku
@end

@interface SWGAttachSku : SWGObject

/* ID of SKU [optional]
 */
@property(nonatomic) NSNumber* _id;
/* Name of SKU 
 */
@property(nonatomic) NSString* name;
/* Symbolic code of SKU [optional]
 */
@property(nonatomic) NSString* code;
/* offset image SKU in % 0-100 [optional]
 */
@property(nonatomic) NSString* offset;
/* First image of SKU [optional]
 */
@property(nonatomic) NSObject* image1;
/* Second image of SKU [optional]
 */
@property(nonatomic) NSObject* image2;
/* Price of SKU [optional]
 */
@property(nonatomic) NSNumber* price;
/* Price of SKU with discounts [optional]
 */
@property(nonatomic) NSNumber* discountPrice;
/* Available quantity [optional]
 */
@property(nonatomic) NSNumber* count;

@end
