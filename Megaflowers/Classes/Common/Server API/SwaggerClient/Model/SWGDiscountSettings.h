#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGDiscountSettings
@end

@interface SWGDiscountSettings : SWGObject

/* Minimal amount for discount apply [optional]
 */
@property(nonatomic) NSNumber* amountMin;
/* Upper border amount for discount apply [optional]
 */
@property(nonatomic) NSNumber* amountMax;
/* Type of discount (0-accumulative;1-fixed) [optional]
 */
@property(nonatomic) NSNumber* discountType;
/* Amount of discount [optional]
 */
@property(nonatomic) NSString* value;

@end
