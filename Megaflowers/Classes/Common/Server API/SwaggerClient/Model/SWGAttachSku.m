#import "SWGAttachSku.h"

@implementation SWGAttachSku

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"name": @"name", @"code": @"code", @"offset": @"offset", @"image1": @"image1", @"image2": @"image2", @"price": @"price", @"discountPrice": @"discount_price", @"count": @"count" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"code", @"offset", @"image1", @"image2", @"price", @"discountPrice", @"count"];
  return [optionalProperties containsObject:propertyName];
}

@end
