#import "SWGCity.h"

@implementation SWGCity

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"type": @"type", @"name": @"name", @"code": @"code", @"lat": @"lat", @"lon": @"lon", @"deliveryPrice": @"delivery_price", @"freeDeliveryAt": @"free_delivery_at", @"country": @"country" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"type", @"code", @"lat", @"lon", @"deliveryPrice", @"freeDeliveryAt", @"country"];
  return [optionalProperties containsObject:propertyName];
}

@end
