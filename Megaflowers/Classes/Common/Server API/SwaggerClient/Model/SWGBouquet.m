#import "SWGBouquet.h"

@implementation SWGBouquet

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"type": @"type", @"name": @"name", @"code": @"code", @"article": @"article", @"position": @"position", @"nameColor": @"name_color", @"preventModify": @"prevent_modify", @"preventDeliveryOnHoliday": @"prevent_delivery_on_holiday", @"isExample": @"is_example", @"isDifficult": @"is_difficult", @"countViews": @"count_views", @"countOrders": @"count_orders", @"preview": @"preview", @"detail": @"detail", @"label": @"label", @"active": @"active", @"defaultSkuId": @"default_sku_id", @"sku": @"sku" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"type", @"code", @"article", @"position", @"nameColor", @"preventModify", @"preventDeliveryOnHoliday", @"isExample", @"isDifficult", @"countViews", @"countOrders", @"preview", @"detail", @"label", @"active", @"defaultSkuId", @"sku"];
  return [optionalProperties containsObject:propertyName];
}

@end
