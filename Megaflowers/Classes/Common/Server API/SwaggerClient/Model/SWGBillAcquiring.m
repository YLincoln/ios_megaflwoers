#import "SWGBillAcquiring.h"

@implementation SWGBillAcquiring

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"url": @"url", @"formData": @"form_data", @"paymentUrl": @"payment_url", @"sdkParam": @"sdk_param", @"successUrl": @"successUrl", @"failUrl": @"failUrl" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"url", @"formData", @"paymentUrl", @"sdkParam", @"successUrl", @"failUrl"];
  return [optionalProperties containsObject:propertyName];
}

@end
