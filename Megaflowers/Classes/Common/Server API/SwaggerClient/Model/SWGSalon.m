#import "SWGSalon.h"

@implementation SWGSalon

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"lat": @"lat", @"lon": @"lon", @"scheduleWeekday": @"schedule_weekday", @"scheduleHoliday": @"schedule_holiday", @"deliveryWeekday": @"delivery_weekday", @"deliveryHoliday": @"delivery_holiday", @"streetType": @"street_type", @"street": @"street", @"buildingType": @"building_type", @"building": @"building", @"phones": @"phones", @"publicHow": @"public_how", @"autoHow": @"auto_how", @"exterier": @"exterier", @"interier": @"interier", @"scheme": @"scheme" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"lat", @"lon", @"scheduleWeekday", @"scheduleHoliday", @"deliveryWeekday", @"deliveryHoliday", @"streetType", @"street", @"buildingType", @"building", @"phones", @"publicHow", @"autoHow", @"exterier", @"interier", @"scheme"];
  return [optionalProperties containsObject:propertyName];
}

@end
