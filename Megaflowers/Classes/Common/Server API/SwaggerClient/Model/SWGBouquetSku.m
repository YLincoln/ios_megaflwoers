#import "SWGBouquetSku.h"

@implementation SWGBouquetSku

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"name": @"name", @"code": @"code", @"width": @"width", @"height": @"height", @"offset": @"offset", @"image": @"image", @"video": @"video", @"videoDuration": @"video_duration", @"videoDate": @"video_date", @"videoComment": @"video_comment", @"masterClass": @"master_class", @"axis": @"axis", @"axisPlug": @"axis_plug", @"components": @"components", @"price": @"price", @"cost": @"cost", @"discountPrice": @"discount_price", @"count": @"count" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"code", @"width", @"height", @"offset", @"image", @"video", @"videoDuration", @"videoDate", @"videoComment", @"masterClass", @"axis", @"axisPlug", @"components", @"price", @"cost", @"discountPrice", @"count"];
  return [optionalProperties containsObject:propertyName];
}

@end
