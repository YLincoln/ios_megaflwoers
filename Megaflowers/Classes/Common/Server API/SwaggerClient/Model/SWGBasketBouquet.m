#import "SWGBasketBouquet.h"

@implementation SWGBasketBouquet

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"bouquetId": @"bouquet_id", @"bouquetSkuId": @"bouquet_sku_id", @"name": @"name", @"skuName": @"sku_name", @"image": @"image", @"components": @"components", @"price": @"price", @"position": @"position", @"count": @"count", @"isChanged": @"isChanged" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"bouquetId", @"bouquetSkuId", @"name", @"skuName", @"image", @"components", @"price", @"position", @"count", @"isChanged"];
  return [optionalProperties containsObject:propertyName];
}

@end
