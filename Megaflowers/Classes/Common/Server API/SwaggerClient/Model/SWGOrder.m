#import "SWGOrder.h"

@implementation SWGOrder

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"num": @"num", @"orderHash": @"order_hash", @"senderName": @"sender_name", @"senderCityId": @"sender_city_id", @"senderAddress": @"sender_address", @"senderPhone": @"sender_phone", @"senderEmail": @"sender_email", @"senderInfo": @"sender_info", @"receiverName": @"receiver_name", @"receiverCityId": @"receiver_city_id", @"receiverAddress": @"receiver_address", @"receiverPhone": @"receiver_phone", @"receiverEmail": @"receiver_email", @"receiverInfo": @"receiver_info", @"deliveryDate": @"delivery_date", @"deliveryTime": @"delivery_time", @"takePhoto": @"take_photo", @"instaPhoto": @"insta_photo", @"emailPhoto": @"email_photo", @"leaveNeighbors": @"leave_neighbors", @"dontCallReceiver": @"dont_call_receiver", @"dontDisturbSender": @"dont_disturb_sender", @"notifyAboutDelivery": @"notify_about_delivery", @"knowOnlyPhone": @"know_only_phone", @"remindMonth": @"remind_month", @"remindYear": @"remind_year", @"asap": @"asap", @"letRecipientKnown": @"let_recipient_known", @"forMe": @"for_me", @"postcardText": @"postcard_text", @"basket": @"basket", @"isPickup": @"is_pickup", @"deliverySalonId": @"delivery_salon_id", @"statusId": @"status_id", @"sumRemain": @"sumRemain" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"num", @"orderHash", @"senderName", @"senderCityId", @"senderAddress", @"senderPhone", @"senderEmail", @"senderInfo", @"receiverName", @"receiverCityId", @"receiverAddress", @"receiverPhone", @"receiverEmail", @"receiverInfo", @"deliveryDate", @"deliveryTime", @"takePhoto", @"instaPhoto", @"emailPhoto", @"leaveNeighbors", @"dontCallReceiver", @"dontDisturbSender", @"notifyAboutDelivery", @"knowOnlyPhone", @"remindMonth", @"remindYear", @"asap", @"letRecipientKnown", @"forMe", @"postcardText", @"basket", @"isPickup", @"deliverySalonId", @"statusId", @"sumRemain"];
  return [optionalProperties containsObject:propertyName];
}

@end
