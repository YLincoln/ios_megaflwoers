#import "SWGBill.h"

@implementation SWGBill

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"xmlId": @"xml_id", @"toId": @"to_id", @"status": @"status", @"statusText": @"status_text", @"num": @"num", @"sum": @"sum", @"paymentId": @"payment_id", @"payment": @"payment", @"acquiring": @"acquiring", @"transactionId": @"transaction_id", @"transactionSdk": @"transaction_sdk" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"xmlId", @"toId", @"status", @"statusText", @"num", @"sum", @"paymentId", @"payment", @"acquiring", @"transactionId", @"transactionSdk"];
  return [optionalProperties containsObject:propertyName];
}

@end
