#import "SWGBasket.h"

@implementation SWGBasket

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"bouquets": @"bouquets", @"attaches": @"attaches", @"coupon": @"coupon", @"couponInfo": @"couponInfo", @"price": @"price", @"delivery": @"delivery", @"priceBasket": @"priceBasket" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"bouquets", @"attaches", @"coupon", @"couponInfo", @"price", @"delivery", @"priceBasket"];
  return [optionalProperties containsObject:propertyName];
}

@end
