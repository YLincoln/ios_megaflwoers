#import "SWGEvent.h"

@implementation SWGEvent

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"name": @"name", @"date": @"date", @"text": @"text", @"periodId": @"period_id", @"remindId": @"remind_id", @"photo": @"photo", @"typeId": @"type_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"date", @"text", @"periodId", @"remindId", @"photo", @"typeId"];
  return [optionalProperties containsObject:propertyName];
}

@end
