#import <Foundation/Foundation.h>
#import "SWGErrorModel.h"
#import "SWGApi.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGAcquiringApi: NSObject <SWGApi>

extern NSString* kSWGAcquiringApiErrorDomain;
extern NSInteger kSWGAcquiringApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// Оповещение о платеже от IntellectMoney
/// 
///
/// @param eshopId Номер сайта интернет-магазина, на который Пользователь (Покупатель) совершает платеж.
/// @param paymentId Номер покупки (СКО) в системе IntellectMoney
/// @param orderId Номер покупки в соответствии с системой учета интернет-магазина, полученный системой с веб-сайта интернет-магазина.
/// @param eshopAccount Номер счета интернет-магазина в системе IntellectMoney. На этот счет будут зачислены средства после оплаты покупки.
/// @param serviceName Назначение платежа в соответствии с системой учета Интернет-магазина, получаемое системой с веб-сайта Интернет-магазина.
/// @param recipientOriginalAmount Исходная сумма платежа. Разделитель дробной части “.“, точность до копейки (2 знака после  разделителя). Не изменяется в процессе оплаты.
/// @param recipientAmount Сумма, которую получает Интернет-магазин. Дробная часть отделяется точкой. В процессе оплаты может быть изменена
/// @param recipientCurrency Валюта платежа (RUB или TST)
/// @param paymentStatus Статус платежа может иметь следующие значения: 3 (создан счет к оплате (СКО) за покупку), 4 (СКО аннулирован, деньги возвращены пользователю), 7 (СКО частично подтвержден , в данном случае сумма которая уже подтвержден а передается в параметре recipientAmount), 5 (СКО полностью подтвержден , деньги переведены на счет интернет-магазина), 6 (необходимая сумма заблокирована на СКО, ожидается запрос на списание или разблокировку средств или истечение срока блокировки)
/// @param userName Имя Пользователя (покупателя) выписавшего счет в системе IntellectMoney
/// @param userEmail Email Пользователя (покупателя) выписавшего счет в системе IntellectMoney
/// @param paymentData Дата и время реального прохождения платежа в системе IntellectMoney в формате &#39;yyyy-MM-dd HH:mm:ss&#39;
/// @param payMethod Способ оплаты счета
/// @param shortPan Короткий номер пластиковой карты (первая и четыре последние цифры пластиковой карты), которой был оплачен счет.
/// @param country Страна, в которой была выпущена пластиковая карта.
/// @param bank Банк, выпустивший пластиковую карту.
/// @param ipAddress IP адрес пользователя, совершившего оплату счета.
/// @param secretKey Значение Секретный ключ, известное только интернет-магазину и системе IntellectMoney. 
/// @param hash Контрольная подпись оповещения о выполнении платежа, которая используется для проверки целостности полученной информации и однозначной идентификации отправителя.
/// 
///  code:200 message:"OK",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) acquiringIntellectmoneyPostWithEshopId: (NSNumber*) eshopId
    paymentId: (NSNumber*) paymentId
    orderId: (NSNumber*) orderId
    eshopAccount: (NSString*) eshopAccount
    serviceName: (NSString*) serviceName
    recipientOriginalAmount: (NSString*) recipientOriginalAmount
    recipientAmount: (NSString*) recipientAmount
    recipientCurrency: (NSString*) recipientCurrency
    paymentStatus: (NSNumber*) paymentStatus
    userName: (NSString*) userName
    userEmail: (NSString*) userEmail
    paymentData: (NSString*) paymentData
    payMethod: (NSString*) payMethod
    shortPan: (NSString*) shortPan
    country: (NSString*) country
    bank: (NSString*) bank
    ipAddress: (NSString*) ipAddress
    secretKey: (NSString*) secretKey
    hash: (NSString*) hash
    completionHandler: (void (^)(NSError* error)) handler;


/// Оповещение о платеже от PayOnline
/// 
///
/// @param dateTime Дата и время платежа, часовой пояс GMT+0 (UTC). в формате «yyyy-MM-dd HH:mm:ss»
/// @param transactionID Идентификатор транзакции. Целое число (Long).
/// @param orderId Идентификатор заказа в системе ТСП
/// @param amount Сумма транзакции.Число с фиксированной точкой (Decimal), два знака после точки, например 1500.99
/// @param currency Валюта транзакции. Строка, три символа
/// @param securityKey Открытый ключ, подтверждающий целостность параметров запроса. Строка, 32 символа в нижнем регистре
/// @param paymentAmount Сумма транзакции в валюте платежа.Число с фиксированной точкой (Decimal), два знака после точки, например 1500.99 (optional)
/// @param paymentCurrency Валюта платежа (optional)
/// @param cardHolder Имя держателя карты (optional)
/// @param cardNumber Маскированный номер карты — 12 символов ‘*’ и последние 4 цифры (optional)
/// @param country Код страны в соответствии с ISO 3166. Строка, 2 символа. (optional)
/// @param city Город (optional)
/// @param address Адрес. (optional)
/// @param ipAddress IP-адрес плательщика (optional)
/// @param ipCountry Код страны, определенный по IP-адресу (optional)
/// @param binCountry Код страны, определенный по BIN эмитента карты (optional)
/// @param specialConditions Особые условия. (optional)
/// @param provider Способ оплаты (optional)
/// @param rebillAnchor Ссылка на повторные платежи по данной карте (optional)
/// 
///  code:200 message:"OK",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) acquiringPayonlinePostWithDateTime: (NSString*) dateTime
    transactionID: (NSNumber*) transactionID
    orderId: (NSString*) orderId
    amount: (NSString*) amount
    currency: (NSString*) currency
    securityKey: (NSString*) securityKey
    paymentAmount: (NSString*) paymentAmount
    paymentCurrency: (NSString*) paymentCurrency
    cardHolder: (NSString*) cardHolder
    cardNumber: (NSString*) cardNumber
    country: (NSString*) country
    city: (NSString*) city
    address: (NSString*) address
    ipAddress: (NSString*) ipAddress
    ipCountry: (NSString*) ipCountry
    binCountry: (NSString*) binCountry
    specialConditions: (NSString*) specialConditions
    provider: (NSString*) provider
    rebillAnchor: (NSString*) rebillAnchor
    completionHandler: (void (^)(NSError* error)) handler;


/// Оповещение о платеже от PayPal
/// 
///
/// 
///  code:200 message:"OK",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) acquiringPaypalPostWithCompletionHandler: 
    (void (^)(NSError* error)) handler;


/// Action request Paypal redirect временный action для обработки paypal запроса - в дальнешем надо это реализовать на сайте
/// 
///
/// 
///  code:200 message:"OK",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) acquiringPaypalgetPostWithCompletionHandler: 
    (void (^)(NSError* error)) handler;


/// Оповещение о платеже от Tinkoff
/// 
///
/// @param terminalKey Идентификатор терминала, выдается Продавцу Банком
/// @param orderId Номер заказа в системе Продавца
/// @param success Успешность операции (true/false)
/// @param status Статус транзакции
/// @param paymentId Уникальный идентификатор транзакции в системе Банка
/// @param errorCode Код ошибки, «0» - если успешно
/// @param amount Текущая сумма транзакции в копейках
/// @param rebillId В случае если при вызове метода Init платеж был помечен как рекуррентный, то после подтверждения оплаты в этот параметр будет передан идентификатор рекуррентного платежа
/// @param cardId В случае если разрешена автоматическая привязка карт Покупателей к терминалу и при вызове метода Init был передан параметр CustomerKey, в этот параметр будет передан идентификатор привязанной карты
/// @param pan Маскированный номер карты
/// @param token Подпись запроса. Формируется по такому же принципу, как и в случае запросов в банк
/// 
///  code:200 message:"OK",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) acquiringTinkoffPostWithTerminalKey: (NSString*) terminalKey
    orderId: (NSString*) orderId
    success: (NSNumber*) success
    status: (NSString*) status
    paymentId: (NSNumber*) paymentId
    errorCode: (NSString*) errorCode
    amount: (NSNumber*) amount
    rebillId: (NSNumber*) rebillId
    cardId: (NSNumber*) cardId
    pan: (NSString*) pan
    token: (NSString*) token
    completionHandler: (void (^)(NSError* error)) handler;



@end
