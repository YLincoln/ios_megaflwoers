#import "SWGPaymentsApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGBill.h"
#import "SWGErrorModel.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse2002.h"


@interface SWGPaymentsApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGPaymentsApi

NSString* kSWGPaymentsApiErrorDomain = @"SWGPaymentsApiErrorDomain";
NSInteger kSWGPaymentsApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Get list of bills
/// 
///  @param lang language (1 - ru, 2 - en) 
///
///  @param site site (4 - apps) 
///
///  @param city city 
///
///  @param orderId ID of order 
///
///  @param orderHash Hash of order (required if order_id defined) 
///
///  @returns NSArray<SWGBill>*
///
-(NSURLSessionTask*) paymentsBillGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    orderId: (NSNumber*) orderId
    orderHash: (NSString*) orderHash
    completionHandler: (void (^)(NSArray<SWGBill>* output, NSError* error)) handler {
    // verify the required parameter 'lang' is set
    if (lang == nil) {
        NSParameterAssert(lang);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"lang"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'site' is set
    if (site == nil) {
        NSParameterAssert(site);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"site"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'city' is set
    if (city == nil) {
        NSParameterAssert(city);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"city"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderHash' is set
    if (orderHash == nil) {
        NSParameterAssert(orderHash);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderHash"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/bill"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (lang != nil) {
        queryParams[@"lang"] = lang;
    }
    if (site != nil) {
        queryParams[@"site"] = site;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (orderId != nil) {
        queryParams[@"order_id"] = orderId;
    }
    if (orderHash != nil) {
        queryParams[@"order_hash"] = orderHash;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGBill>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGBill>*)data, error);
                                }
                            }];
}

///
/// Get info about bill
/// 
///  @param lang language (1 - ru, 2 - en) 
///
///  @param site site (4 - apps) 
///
///  @param city city 
///
///  @param _id ID of aquired bill 
///
///  @param orderId ID of order 
///
///  @param orderHash Hash of order (required if order_id defined) 
///
///  @returns SWGBill*
///
-(NSURLSessionTask*) paymentsBillIdGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    _id: (NSNumber*) _id
    orderId: (NSNumber*) orderId
    orderHash: (NSString*) orderHash
    completionHandler: (void (^)(SWGBill* output, NSError* error)) handler {
    // verify the required parameter 'lang' is set
    if (lang == nil) {
        NSParameterAssert(lang);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"lang"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'site' is set
    if (site == nil) {
        NSParameterAssert(site);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"site"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'city' is set
    if (city == nil) {
        NSParameterAssert(city);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"city"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter '_id' is set
    if (_id == nil) {
        NSParameterAssert(_id);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"_id"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderHash' is set
    if (orderHash == nil) {
        NSParameterAssert(orderHash);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderHash"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/bill/{id}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (lang != nil) {
        queryParams[@"lang"] = lang;
    }
    if (site != nil) {
        queryParams[@"site"] = site;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (orderId != nil) {
        queryParams[@"order_id"] = orderId;
    }
    if (orderHash != nil) {
        queryParams[@"order_hash"] = orderHash;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGBill*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGBill*)data, error);
                                }
                            }];
}

///
/// Update status and/or payment type
/// 
///  @param lang language (1 - ru, 2 - en) 
///
///  @param site site (4 - apps) 
///
///  @param city city 
///
///  @param _id ID of aquired bill 
///
///  @param orderId ID of order 
///
///  @param orderHash Hash of order (required if order_id defined) 
///
///  @param status Bill's attributes (optional)
///
///  @param paymentId ID of payment system (optional)
///
///  @param transactionId The unique identifier of transaction in system of Bank.   *                      Если используется SDK метод оплаты, то после формирования счета, необходимо вернуть его (optional)
///
///  @returns void
///
-(NSURLSessionTask*) paymentsBillIdPatchWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    _id: (NSNumber*) _id
    orderId: (NSNumber*) orderId
    orderHash: (NSString*) orderHash
    status: (NSNumber*) status
    paymentId: (NSNumber*) paymentId
    transactionId: (NSString*) transactionId
    completionHandler: (void (^)(NSError* error)) handler {
    // verify the required parameter 'lang' is set
    if (lang == nil) {
        NSParameterAssert(lang);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"lang"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'site' is set
    if (site == nil) {
        NSParameterAssert(site);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"site"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'city' is set
    if (city == nil) {
        NSParameterAssert(city);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"city"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter '_id' is set
    if (_id == nil) {
        NSParameterAssert(_id);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"_id"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'orderHash' is set
    if (orderHash == nil) {
        NSParameterAssert(orderHash);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderHash"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/bill/{id}"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (lang != nil) {
        queryParams[@"lang"] = lang;
    }
    if (site != nil) {
        queryParams[@"site"] = site;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (orderId != nil) {
        queryParams[@"order_id"] = orderId;
    }
    if (orderHash != nil) {
        queryParams[@"order_hash"] = orderHash;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/x-www-form-urlencoded"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    if (status) {
        formParams[@"status"] = status;
    }
    if (paymentId) {
        formParams[@"payment_id"] = paymentId;
    }
    if (transactionId) {
        formParams[@"transaction_id"] = transactionId;
    }

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PATCH"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}

///
/// Current status of bill
/// 
///  @param lang language (1 - ru, 2 - en) 
///
///  @param site site (4 - apps) 
///
///  @param city city 
///
///  @param _id ID of aquired bill 
///
///  @param orderId ID of order 
///
///  @param orderHash Hash of order (required if order_id defined) 
///
///  @returns SWGInlineResponse200*
///
-(NSURLSessionTask*) paymentsBillIdStatusGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    _id: (NSNumber*) _id
    orderId: (NSNumber*) orderId
    orderHash: (NSString*) orderHash
    completionHandler: (void (^)(SWGInlineResponse200* output, NSError* error)) handler {
    // verify the required parameter 'lang' is set
    if (lang == nil) {
        NSParameterAssert(lang);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"lang"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'site' is set
    if (site == nil) {
        NSParameterAssert(site);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"site"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'city' is set
    if (city == nil) {
        NSParameterAssert(city);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"city"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter '_id' is set
    if (_id == nil) {
        NSParameterAssert(_id);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"_id"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderHash' is set
    if (orderHash == nil) {
        NSParameterAssert(orderHash);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderHash"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/bill/{id}/status"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (lang != nil) {
        queryParams[@"lang"] = lang;
    }
    if (site != nil) {
        queryParams[@"site"] = site;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (orderId != nil) {
        queryParams[@"order_id"] = orderId;
    }
    if (orderHash != nil) {
        queryParams[@"order_hash"] = orderHash;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGInlineResponse200*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGInlineResponse200*)data, error);
                                }
                            }];
}

///
/// Creates new bill
/// 
///  @param lang language (1 - ru, 2 - en) 
///
///  @param site site (4 - apps) 
///
///  @param city city 
///
///  @param orderId Order ID 
///
///  @param orderHash Hash of order 
///
///  @param paymentId ID of payment system 
///
///  @param sum Payment amount 
///
///  @returns SWGBill*
///
-(NSURLSessionTask*) paymentsBillPostWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    orderId: (NSNumber*) orderId
    orderHash: (NSString*) orderHash
    paymentId: (NSNumber*) paymentId
    sum: (NSNumber*) sum
    completionHandler: (void (^)(SWGBill* output, NSError* error)) handler {
    // verify the required parameter 'lang' is set
    if (lang == nil) {
        NSParameterAssert(lang);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"lang"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'site' is set
    if (site == nil) {
        NSParameterAssert(site);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"site"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'city' is set
    if (city == nil) {
        NSParameterAssert(city);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"city"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'orderHash' is set
    if (orderHash == nil) {
        NSParameterAssert(orderHash);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderHash"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'paymentId' is set
    if (paymentId == nil) {
        NSParameterAssert(paymentId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"paymentId"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'sum' is set
    if (sum == nil) {
        NSParameterAssert(sum);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"sum"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/bill"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (lang != nil) {
        queryParams[@"lang"] = lang;
    }
    if (site != nil) {
        queryParams[@"site"] = site;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/x-www-form-urlencoded"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    if (orderId) {
        formParams[@"order_id"] = orderId;
    }
    if (orderHash) {
        formParams[@"order_hash"] = orderHash;
    }
    if (paymentId) {
        formParams[@"payment_id"] = paymentId;
    }
    if (sum) {
        formParams[@"sum"] = sum;
    }

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGBill*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGBill*)data, error);
                                }
                            }];
}

///
/// Get list of available payment methods
/// 
///  @param lang language (1 - ru, 2 - en) 
///
///  @param site site (4 - apps) 
///
///  @param city city 
///
///  @param orderId  (optional)
///
///  @param sum  (optional)
///
///  @param online  (optional)
///
///  @returns NSArray<SWGInlineResponse2002>*
///
-(NSURLSessionTask*) paymentsListGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    orderId: (NSNumber*) orderId
    sum: (NSNumber*) sum
    online: (NSNumber*) online
    completionHandler: (void (^)(NSArray<SWGInlineResponse2002>* output, NSError* error)) handler {
    // verify the required parameter 'lang' is set
    if (lang == nil) {
        NSParameterAssert(lang);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"lang"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'site' is set
    if (site == nil) {
        NSParameterAssert(site);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"site"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'city' is set
    if (city == nil) {
        NSParameterAssert(city);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"city"] };
            NSError* error = [NSError errorWithDomain:kSWGPaymentsApiErrorDomain code:kSWGPaymentsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/payments/list"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (lang != nil) {
        queryParams[@"lang"] = lang;
    }
    if (site != nil) {
        queryParams[@"site"] = site;
    }
    if (city != nil) {
        queryParams[@"city"] = city;
    }
    if (orderId != nil) {
        queryParams[@"order_id"] = orderId;
    }
    if (sum != nil) {
        queryParams[@"sum"] = sum;
    }
    if (online != nil) {
        queryParams[@"online"] = online;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SWGInlineResponse2002>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SWGInlineResponse2002>*)data, error);
                                }
                            }];
}



@end
