#import <Foundation/Foundation.h>
#import "SWGComponent.h"
#import "SWGErrorModel.h"
#import "SWGApi.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGComponentApi: NSObject <SWGApi>

extern NSString* kSWGComponentApiErrorDomain;
extern NSInteger kSWGComponentApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// List of components
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGComponent>*
-(NSURLSessionTask*) componentGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGComponent>* output, NSError* error)) handler;


/// Detail view of component
/// 
///
/// @param _id ID of component
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return SWGComponent*
-(NSURLSessionTask*) componentIdGetWithId: (NSNumber*) _id
    lang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(SWGComponent* output, NSError* error)) handler;



@end
