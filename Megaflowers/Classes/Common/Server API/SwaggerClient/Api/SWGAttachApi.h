#import <Foundation/Foundation.h>
#import "SWGAttach.h"
#import "SWGErrorModel.h"
#import "SWGApi.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGAttachApi: NSObject <SWGApi>

extern NSString* kSWGAttachApiErrorDomain;
extern NSInteger kSWGAttachApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// List of attaches
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// @param parent parent section (optional)
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGAttach>*
-(NSURLSessionTask*) attachGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    parent: (NSNumber*) parent
    completionHandler: (void (^)(NSArray<SWGAttach>* output, NSError* error)) handler;


/// Detail view of attach
/// 
///
/// @param _id ID of attach
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return SWGAttach*
-(NSURLSessionTask*) attachIdGetWithId: (NSNumber*) _id
    lang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(SWGAttach* output, NSError* error)) handler;



@end
