#import "SWGAcquiringApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGErrorModel.h"


@interface SWGAcquiringApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGAcquiringApi

NSString* kSWGAcquiringApiErrorDomain = @"SWGAcquiringApiErrorDomain";
NSInteger kSWGAcquiringApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Оповещение о платеже от IntellectMoney
/// 
///  @param eshopId Номер сайта интернет-магазина, на который Пользователь (Покупатель) совершает платеж. 
///
///  @param paymentId Номер покупки (СКО) в системе IntellectMoney 
///
///  @param orderId Номер покупки в соответствии с системой учета интернет-магазина, полученный системой с веб-сайта интернет-магазина. 
///
///  @param eshopAccount Номер счета интернет-магазина в системе IntellectMoney. На этот счет будут зачислены средства после оплаты покупки. 
///
///  @param serviceName Назначение платежа в соответствии с системой учета Интернет-магазина, получаемое системой с веб-сайта Интернет-магазина. 
///
///  @param recipientOriginalAmount Исходная сумма платежа. Разделитель дробной части “.“, точность до копейки (2 знака после  разделителя). Не изменяется в процессе оплаты. 
///
///  @param recipientAmount Сумма, которую получает Интернет-магазин. Дробная часть отделяется точкой. В процессе оплаты может быть изменена 
///
///  @param recipientCurrency Валюта платежа (RUB или TST) 
///
///  @param paymentStatus Статус платежа может иметь следующие значения: 3 (создан счет к оплате (СКО) за покупку), 4 (СКО аннулирован, деньги возвращены пользователю), 7 (СКО частично подтвержден , в данном случае сумма которая уже подтвержден а передается в параметре recipientAmount), 5 (СКО полностью подтвержден , деньги переведены на счет интернет-магазина), 6 (необходимая сумма заблокирована на СКО, ожидается запрос на списание или разблокировку средств или истечение срока блокировки) 
///
///  @param userName Имя Пользователя (покупателя) выписавшего счет в системе IntellectMoney 
///
///  @param userEmail Email Пользователя (покупателя) выписавшего счет в системе IntellectMoney 
///
///  @param paymentData Дата и время реального прохождения платежа в системе IntellectMoney в формате 'yyyy-MM-dd HH:mm:ss' 
///
///  @param payMethod Способ оплаты счета 
///
///  @param shortPan Короткий номер пластиковой карты (первая и четыре последние цифры пластиковой карты), которой был оплачен счет. 
///
///  @param country Страна, в которой была выпущена пластиковая карта. 
///
///  @param bank Банк, выпустивший пластиковую карту. 
///
///  @param ipAddress IP адрес пользователя, совершившего оплату счета. 
///
///  @param secretKey Значение Секретный ключ, известное только интернет-магазину и системе IntellectMoney.  
///
///  @param hash Контрольная подпись оповещения о выполнении платежа, которая используется для проверки целостности полученной информации и однозначной идентификации отправителя. 
///
///  @returns void
///
-(NSURLSessionTask*) acquiringIntellectmoneyPostWithEshopId: (NSNumber*) eshopId
    paymentId: (NSNumber*) paymentId
    orderId: (NSNumber*) orderId
    eshopAccount: (NSString*) eshopAccount
    serviceName: (NSString*) serviceName
    recipientOriginalAmount: (NSString*) recipientOriginalAmount
    recipientAmount: (NSString*) recipientAmount
    recipientCurrency: (NSString*) recipientCurrency
    paymentStatus: (NSNumber*) paymentStatus
    userName: (NSString*) userName
    userEmail: (NSString*) userEmail
    paymentData: (NSString*) paymentData
    payMethod: (NSString*) payMethod
    shortPan: (NSString*) shortPan
    country: (NSString*) country
    bank: (NSString*) bank
    ipAddress: (NSString*) ipAddress
    secretKey: (NSString*) secretKey
    hash: (NSString*) hash
    completionHandler: (void (^)(NSError* error)) handler {
    // verify the required parameter 'eshopId' is set
    if (eshopId == nil) {
        NSParameterAssert(eshopId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"eshopId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'paymentId' is set
    if (paymentId == nil) {
        NSParameterAssert(paymentId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"paymentId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'eshopAccount' is set
    if (eshopAccount == nil) {
        NSParameterAssert(eshopAccount);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"eshopAccount"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'serviceName' is set
    if (serviceName == nil) {
        NSParameterAssert(serviceName);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"serviceName"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'recipientOriginalAmount' is set
    if (recipientOriginalAmount == nil) {
        NSParameterAssert(recipientOriginalAmount);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"recipientOriginalAmount"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'recipientAmount' is set
    if (recipientAmount == nil) {
        NSParameterAssert(recipientAmount);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"recipientAmount"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'recipientCurrency' is set
    if (recipientCurrency == nil) {
        NSParameterAssert(recipientCurrency);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"recipientCurrency"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'paymentStatus' is set
    if (paymentStatus == nil) {
        NSParameterAssert(paymentStatus);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"paymentStatus"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'userName' is set
    if (userName == nil) {
        NSParameterAssert(userName);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"userName"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'userEmail' is set
    if (userEmail == nil) {
        NSParameterAssert(userEmail);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"userEmail"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'paymentData' is set
    if (paymentData == nil) {
        NSParameterAssert(paymentData);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"paymentData"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'payMethod' is set
    if (payMethod == nil) {
        NSParameterAssert(payMethod);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"payMethod"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'shortPan' is set
    if (shortPan == nil) {
        NSParameterAssert(shortPan);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"shortPan"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'country' is set
    if (country == nil) {
        NSParameterAssert(country);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"country"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'bank' is set
    if (bank == nil) {
        NSParameterAssert(bank);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"bank"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'ipAddress' is set
    if (ipAddress == nil) {
        NSParameterAssert(ipAddress);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"ipAddress"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'secretKey' is set
    if (secretKey == nil) {
        NSParameterAssert(secretKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"secretKey"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'hash' is set
    if (hash == nil) {
        NSParameterAssert(hash);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"hash"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/acquiring/intellectmoney"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/x-www-form-urlencoded"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    if (eshopId) {
        formParams[@"eshopId"] = eshopId;
    }
    if (paymentId) {
        formParams[@"paymentId"] = paymentId;
    }
    if (orderId) {
        formParams[@"orderId"] = orderId;
    }
    if (eshopAccount) {
        formParams[@"eshopAccount"] = eshopAccount;
    }
    if (serviceName) {
        formParams[@"serviceName"] = serviceName;
    }
    if (recipientOriginalAmount) {
        formParams[@"recipientOriginalAmount"] = recipientOriginalAmount;
    }
    if (recipientAmount) {
        formParams[@"recipientAmount"] = recipientAmount;
    }
    if (recipientCurrency) {
        formParams[@"recipientCurrency"] = recipientCurrency;
    }
    if (paymentStatus) {
        formParams[@"paymentStatus"] = paymentStatus;
    }
    if (userName) {
        formParams[@"userName"] = userName;
    }
    if (userEmail) {
        formParams[@"userEmail"] = userEmail;
    }
    if (paymentData) {
        formParams[@"paymentData"] = paymentData;
    }
    if (payMethod) {
        formParams[@"payMethod"] = payMethod;
    }
    if (shortPan) {
        formParams[@"shortPan"] = shortPan;
    }
    if (country) {
        formParams[@"country"] = country;
    }
    if (bank) {
        formParams[@"bank"] = bank;
    }
    if (ipAddress) {
        formParams[@"ipAddress"] = ipAddress;
    }
    if (secretKey) {
        formParams[@"secretKey"] = secretKey;
    }
    if (hash) {
        formParams[@"hash"] = hash;
    }

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}

///
/// Оповещение о платеже от PayOnline
/// 
///  @param dateTime Дата и время платежа, часовой пояс GMT+0 (UTC). в формате «yyyy-MM-dd HH:mm:ss» 
///
///  @param transactionID Идентификатор транзакции. Целое число (Long). 
///
///  @param orderId Идентификатор заказа в системе ТСП 
///
///  @param amount Сумма транзакции.Число с фиксированной точкой (Decimal), два знака после точки, например 1500.99 
///
///  @param currency Валюта транзакции. Строка, три символа 
///
///  @param securityKey Открытый ключ, подтверждающий целостность параметров запроса. Строка, 32 символа в нижнем регистре 
///
///  @param paymentAmount Сумма транзакции в валюте платежа.Число с фиксированной точкой (Decimal), два знака после точки, например 1500.99 (optional)
///
///  @param paymentCurrency Валюта платежа (optional)
///
///  @param cardHolder Имя держателя карты (optional)
///
///  @param cardNumber Маскированный номер карты — 12 символов ‘*’ и последние 4 цифры (optional)
///
///  @param country Код страны в соответствии с ISO 3166. Строка, 2 символа. (optional)
///
///  @param city Город (optional)
///
///  @param address Адрес. (optional)
///
///  @param ipAddress IP-адрес плательщика (optional)
///
///  @param ipCountry Код страны, определенный по IP-адресу (optional)
///
///  @param binCountry Код страны, определенный по BIN эмитента карты (optional)
///
///  @param specialConditions Особые условия. (optional)
///
///  @param provider Способ оплаты (optional)
///
///  @param rebillAnchor Ссылка на повторные платежи по данной карте (optional)
///
///  @returns void
///
-(NSURLSessionTask*) acquiringPayonlinePostWithDateTime: (NSString*) dateTime
    transactionID: (NSNumber*) transactionID
    orderId: (NSString*) orderId
    amount: (NSString*) amount
    currency: (NSString*) currency
    securityKey: (NSString*) securityKey
    paymentAmount: (NSString*) paymentAmount
    paymentCurrency: (NSString*) paymentCurrency
    cardHolder: (NSString*) cardHolder
    cardNumber: (NSString*) cardNumber
    country: (NSString*) country
    city: (NSString*) city
    address: (NSString*) address
    ipAddress: (NSString*) ipAddress
    ipCountry: (NSString*) ipCountry
    binCountry: (NSString*) binCountry
    specialConditions: (NSString*) specialConditions
    provider: (NSString*) provider
    rebillAnchor: (NSString*) rebillAnchor
    completionHandler: (void (^)(NSError* error)) handler {
    // verify the required parameter 'dateTime' is set
    if (dateTime == nil) {
        NSParameterAssert(dateTime);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"dateTime"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'transactionID' is set
    if (transactionID == nil) {
        NSParameterAssert(transactionID);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"transactionID"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'amount' is set
    if (amount == nil) {
        NSParameterAssert(amount);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"amount"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'currency' is set
    if (currency == nil) {
        NSParameterAssert(currency);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"currency"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'securityKey' is set
    if (securityKey == nil) {
        NSParameterAssert(securityKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"securityKey"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/acquiring/payonline"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/x-www-form-urlencoded"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    if (dateTime) {
        formParams[@"DateTime"] = dateTime;
    }
    if (transactionID) {
        formParams[@"TransactionID"] = transactionID;
    }
    if (orderId) {
        formParams[@"OrderId"] = orderId;
    }
    if (amount) {
        formParams[@"Amount"] = amount;
    }
    if (currency) {
        formParams[@"Currency"] = currency;
    }
    if (securityKey) {
        formParams[@"SecurityKey"] = securityKey;
    }
    if (paymentAmount) {
        formParams[@"PaymentAmount"] = paymentAmount;
    }
    if (paymentCurrency) {
        formParams[@"PaymentCurrency"] = paymentCurrency;
    }
    if (cardHolder) {
        formParams[@"CardHolder"] = cardHolder;
    }
    if (cardNumber) {
        formParams[@"CardNumber"] = cardNumber;
    }
    if (country) {
        formParams[@"Country"] = country;
    }
    if (city) {
        formParams[@"City"] = city;
    }
    if (address) {
        formParams[@"Address"] = address;
    }
    if (ipAddress) {
        formParams[@"IpAddress"] = ipAddress;
    }
    if (ipCountry) {
        formParams[@"IpCountry"] = ipCountry;
    }
    if (binCountry) {
        formParams[@"BinCountry"] = binCountry;
    }
    if (specialConditions) {
        formParams[@"SpecialConditions"] = specialConditions;
    }
    if (provider) {
        formParams[@"Provider"] = provider;
    }
    if (rebillAnchor) {
        formParams[@"RebillAnchor"] = rebillAnchor;
    }

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}

///
/// Оповещение о платеже от PayPal
/// 
///  @returns void
///
-(NSURLSessionTask*) acquiringPaypalPostWithCompletionHandler: 
    (void (^)(NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/acquiring/paypal"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}

///
/// Action request Paypal redirect временный action для обработки paypal запроса - в дальнешем надо это реализовать на сайте
/// 
///  @returns void
///
-(NSURLSessionTask*) acquiringPaypalgetPostWithCompletionHandler: 
    (void (^)(NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/acquiring/paypalget"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}

///
/// Оповещение о платеже от Tinkoff
/// 
///  @param terminalKey Идентификатор терминала, выдается Продавцу Банком 
///
///  @param orderId Номер заказа в системе Продавца 
///
///  @param success Успешность операции (true/false) 
///
///  @param status Статус транзакции 
///
///  @param paymentId Уникальный идентификатор транзакции в системе Банка 
///
///  @param errorCode Код ошибки, «0» - если успешно 
///
///  @param amount Текущая сумма транзакции в копейках 
///
///  @param rebillId В случае если при вызове метода Init платеж был помечен как рекуррентный, то после подтверждения оплаты в этот параметр будет передан идентификатор рекуррентного платежа 
///
///  @param cardId В случае если разрешена автоматическая привязка карт Покупателей к терминалу и при вызове метода Init был передан параметр CustomerKey, в этот параметр будет передан идентификатор привязанной карты 
///
///  @param pan Маскированный номер карты 
///
///  @param token Подпись запроса. Формируется по такому же принципу, как и в случае запросов в банк 
///
///  @returns void
///
-(NSURLSessionTask*) acquiringTinkoffPostWithTerminalKey: (NSString*) terminalKey
    orderId: (NSString*) orderId
    success: (NSNumber*) success
    status: (NSString*) status
    paymentId: (NSNumber*) paymentId
    errorCode: (NSString*) errorCode
    amount: (NSNumber*) amount
    rebillId: (NSNumber*) rebillId
    cardId: (NSNumber*) cardId
    pan: (NSString*) pan
    token: (NSString*) token
    completionHandler: (void (^)(NSError* error)) handler {
    // verify the required parameter 'terminalKey' is set
    if (terminalKey == nil) {
        NSParameterAssert(terminalKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"terminalKey"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'orderId' is set
    if (orderId == nil) {
        NSParameterAssert(orderId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"orderId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'success' is set
    if (success == nil) {
        NSParameterAssert(success);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"success"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'status' is set
    if (status == nil) {
        NSParameterAssert(status);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"status"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'paymentId' is set
    if (paymentId == nil) {
        NSParameterAssert(paymentId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"paymentId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'errorCode' is set
    if (errorCode == nil) {
        NSParameterAssert(errorCode);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"errorCode"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'amount' is set
    if (amount == nil) {
        NSParameterAssert(amount);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"amount"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'rebillId' is set
    if (rebillId == nil) {
        NSParameterAssert(rebillId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"rebillId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'cardId' is set
    if (cardId == nil) {
        NSParameterAssert(cardId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"cardId"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'pan' is set
    if (pan == nil) {
        NSParameterAssert(pan);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"pan"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'token' is set
    if (token == nil) {
        NSParameterAssert(token);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"token"] };
            NSError* error = [NSError errorWithDomain:kSWGAcquiringApiErrorDomain code:kSWGAcquiringApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/acquiring/tinkoff"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/x-www-form-urlencoded"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    if (terminalKey) {
        formParams[@"TerminalKey"] = terminalKey;
    }
    if (orderId) {
        formParams[@"OrderId"] = orderId;
    }
    if (success) {
        formParams[@"Success"] = success;
    }
    if (status) {
        formParams[@"Status"] = status;
    }
    if (paymentId) {
        formParams[@"PaymentId"] = paymentId;
    }
    if (errorCode) {
        formParams[@"ErrorCode"] = errorCode;
    }
    if (amount) {
        formParams[@"Amount"] = amount;
    }
    if (rebillId) {
        formParams[@"RebillId"] = rebillId;
    }
    if (cardId) {
        formParams[@"CardId"] = cardId;
    }
    if (pan) {
        formParams[@"Pan"] = pan;
    }
    if (token) {
        formParams[@"Token"] = token;
    }

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}



@end
