#import <Foundation/Foundation.h>
#import "SWGBouquet.h"
#import "SWGErrorModel.h"
#import "SWGOffer.h"
#import "SWGOptions.h"
#import "SWGPopular.h"
#import "SWGSalon.h"
#import "SWGUser.h"
#import "SWGUserBouquet.h"
#import "SWGUserCoupon.h"
#import "SWGUserDiscount.h"
#import "SWGUserEmail.h"
#import "SWGUserEvent.h"
#import "SWGUserPhone.h"
#import "SWGUserPush.h"
#import "SWGUserSocial.h"
#import "SWGApi.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGUserApi: NSObject <SWGApi>

extern NSString* kSWGUserApiErrorDomain;
extern NSInteger kSWGUserApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// Offer
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return SWGOffer*
-(NSURLSessionTask*) offerGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    completionHandler: (void (^)(SWGOffer* output, NSError* error)) handler;


/// List of user bouquets with full model and selected SKU
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGBouquet>*
-(NSURLSessionTask*) userBouquetFullGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGBouquet>* output, NSError* error)) handler;


/// List of user bouquets
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserBouquet>*
-(NSURLSessionTask*) userBouquetGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGUserBouquet>* output, NSError* error)) handler;


/// Delete bouquet from user
/// 
///
/// @param _id ID of bouquet SKU
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userBouquetIdDeleteWithId: (NSNumber*) _id
    completionHandler: (void (^)(NSError* error)) handler;


/// Add bouquet to user
/// 
///
/// @param user ID of user
/// @param bouquet ID of bouquet SKU
/// 
///  code:201 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userBouquetPostWithUser: (NSNumber*) user
    bouquet: (NSNumber*) bouquet
    completionHandler: (void (^)(NSError* error)) handler;


/// List of user coupons
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserCoupon>*
-(NSURLSessionTask*) userCouponGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGUserCoupon>* output, NSError* error)) handler;


/// Delete coupon of user
/// 
///
/// @param _id ID of coupon
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userCouponIdDeleteWithId: (NSNumber*) _id
    completionHandler: (void (^)(NSError* error)) handler;


/// Edit of coupon
/// 
///
/// @param _id ID of coupon
/// 
///  code:202 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userCouponIdPutWithId: (NSNumber*) _id
    completionHandler: (void (^)(NSError* error)) handler;


/// Create coupon for user
/// 
///
/// @param code Code of coupon
/// 
///  code:201 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userCouponPostWithCode: (NSString*) code
    completionHandler: (void (^)(NSError* error)) handler;


/// Add discount card for user
/// Link discount card with user account
///
/// @param code card string 13 digits chars
/// @param lang Language ID (1-ru;2-en) (optional) (default to 1)
/// 
///  code:201 message:"ok",
///  code:400 message:"discount card link with out user",
///  code:403 message:"access denied",
///  code:404 message:"discount card not found",
///  code:422 message:"validation failed"
///
/// @return 
-(NSURLSessionTask*) userDiscountAddPostWithCode: (NSString*) code
    lang: (NSNumber*) lang
    completionHandler: (void (^)(NSError* error)) handler;


/// Virtual discount card for user list
/// Get user's discount cards
///
/// 
///  code:200 message:"ok"
///
/// @return NSArray<SWGUserDiscount>*
-(NSURLSessionTask*) userDiscountGetWithCompletionHandler: 
    (void (^)(NSArray<SWGUserDiscount>* output, NSError* error)) handler;


/// Create virtual discount card for user
/// Create new virtual discount card for user
///
/// @param firstName First Name
/// @param lastName Family name
/// @param birthday format YYYY-MM-DD
/// @param phone 
/// @param lang Language ID (1-ru;2-en) (optional) (default to 1)
/// 
///  code:201 message:"ok",
///  code:204 message:"already has discount",
///  code:400 message:"not enough parameters",
///  code:403 message:"access denied",
///  code:422 message:"validation failed"
///
/// @return 
-(NSURLSessionTask*) userDiscountPostWithFirstName: (NSString*) firstName
    lastName: (NSString*) lastName
    birthday: (NSDate*) birthday
    phone: (NSString*) phone
    lang: (NSNumber*) lang
    completionHandler: (void (^)(NSError* error)) handler;


/// List of user emails
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserEmail>*
-(NSURLSessionTask*) userEmailGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGUserEmail>* output, NSError* error)) handler;


/// Delete email of user
/// 
///
/// @param _id ID of email
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userEmailIdDeleteWithId: (NSNumber*) _id
    completionHandler: (void (^)(NSError* error)) handler;


/// Create email for user
/// 
///
/// @param email Email
/// 
///  code:201 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userEmailPostWithEmail: (NSString*) email
    completionHandler: (void (^)(NSError* error)) handler;


/// List of user events
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserEvent>*
-(NSURLSessionTask*) userEventGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGUserEvent>* output, NSError* error)) handler;


/// Delete event for user by id
/// 
///
/// @param _id ID of event
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userEventIdDeleteWithId: (NSNumber*) _id
    completionHandler: (void (^)(NSError* error)) handler;


/// List of user event by id
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// @param _id ID of user event
/// 
///  code:200 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserEvent>*
-(NSURLSessionTask*) userEventIdGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    _id: (NSNumber*) _id
    completionHandler: (void (^)(NSArray<SWGUserEvent>* output, NSError* error)) handler;


/// Edit user event by Id
/// 
///
/// @param _id ID of Event
/// @param date Date of Event: Y-m-d hh:mm:ss. Examples: 2017-08-23 21:02:13
/// @param periodId Frequency of Event (0 - month, 1 - year, 2 - never, 3 - day, 4 - week, 5 - 2week)
/// @param remindId Reminder time (0 - 1 day, 1 - 3 days, 2 - 7 days, 3 - now, 4 - 5 m, 5 - 15 m, 6 - 30m, 7 - 1h, 8 - 2h, 9 - 2days)
/// @param name Title Event (optional)
/// @param text Text of event (optional)
/// 
///  code:202 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userEventIdPutWithId: (NSNumber*) _id
    date: (NSDate*) date
    periodId: (NSNumber*) periodId
    remindId: (NSNumber*) remindId
    name: (NSString*) name
    text: (NSString*) text
    completionHandler: (void (^)(NSError* error)) handler;


/// Update user event photo by Id
/// 
///
/// @param _id ID of Event
/// @param photo Photo Event (optional)
/// 
///  code:202 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userEventIdUpdatePhotoPostWithId: (NSNumber*) _id
    photo: (NSURL*) photo
    completionHandler: (void (^)(NSError* error)) handler;


/// Create event for user
/// 
///
/// @param name Name of Event
/// @param date Date of Event: Y-m-d hh:mm:ss. Examples: 2017-08-23 21:02:13
/// @param periodId Frequency of Event (0 - month, 1 - year, 2 - never, 3 - day, 4 - week, 5 - 2week)
/// @param remindId Reminder time (0 - 1 day, 1 - 3 days, 2 - 7 days, 3 - now, 4 - 5 m, 5 - 15 m, 6 - 30m, 7 - 1h, 8 - 2h, 9 - 2days)
/// @param text Description of Event (optional)
/// @param photo user photo (optional)
/// @param typeId types user events. based on GET /events,  (optional)
/// 
///  code:201 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return NSNumber*
-(NSURLSessionTask*) userEventPostWithName: (NSString*) name
    date: (NSDate*) date
    periodId: (NSNumber*) periodId
    remindId: (NSNumber*) remindId
    text: (NSString*) text
    photo: (NSURL*) photo
    typeId: (NSNumber*) typeId
    completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;


/// Detail view of user
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return SWGUser*
-(NSURLSessionTask*) userGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(SWGUser* output, NSError* error)) handler;


/// Change user password by old
/// 
///
/// @param oldPassword Old user Password
/// @param password New user Password
/// @param confirmPassword New user Password confirm
/// 
///  code:201 message:"ok",
///  code:400 message:"Confirm password incorrect",
///  code:403 message:"access denied - old password incorrect",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPasswordPostWithOldPassword: (NSString*) oldPassword
    password: (NSString*) password
    confirmPassword: (NSString*) confirmPassword
    completionHandler: (void (^)(NSError* error)) handler;


/// List of user phones
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserPhone>*
-(NSURLSessionTask*) userPhoneGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGUserPhone>* output, NSError* error)) handler;


/// Delete phone of user
/// 
///
/// @param _id ID of phone
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPhoneIdDeleteWithId: (NSNumber*) _id
    completionHandler: (void (^)(NSError* error)) handler;


/// Create phone for user
/// 
///
/// @param phone Phone number
/// 
///  code:201 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPhonePostWithPhone: (NSString*) phone
    completionHandler: (void (^)(NSError* error)) handler;


/// Create a user
/// 
///
/// @param site site (4 - apps)
/// @param phone Phone of user
/// @param name Name of user (optional)
/// @param email Email of user (optional)
/// @param password Password of user (optional)
/// @param code Verify code from email or sms (optional)
/// 
///  code:201 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSString*
-(NSURLSessionTask*) userPostWithSite: (NSNumber*) site
    phone: (NSString*) phone
    name: (NSString*) name
    email: (NSString*) email
    password: (NSString*) password
    code: (NSString*) code
    completionHandler: (void (^)(NSString* output, NSError* error)) handler;


/// Delete push of user by device UUID
/// 
///
/// @param device ID of device push
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:404 message:"user push not found",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPushDeviceDeleteWithDevice: (NSNumber*) device
    completionHandler: (void (^)(NSError* error)) handler;


/// Sets a new value for specific push-option for user
/// 
///
/// @param site site (4 - apps) (default to 4)
/// @param lang language (1 - ru, 2 - en) (default to 1)
/// @param device Push device UUID
/// @param key key of push options
/// @param value value of push options (0-disable, 1-enable)
/// @param type TYPE_IPHONE &#x3D; 0, TYPE_ANDROID &#x3D; 1, TYPE_WINDOWS &#x3D; 2
/// 
///  code:202 message:"ok",
///  code:404 message:"not found",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPushDevicePatchWithSite: (NSNumber*) site
    lang: (NSNumber*) lang
    device: (NSString*) device
    key: (NSString*) key
    value: (NSNumber*) value
    type: (NSNumber*) type
    completionHandler: (void (^)(NSError* error)) handler;


/// Edit user push by push Id
/// 
///
/// @param device device UUID of push
/// @param options 
/// 
///  code:202 message:"ok",
///  code:400 message:"not enough parameters",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPushDevicePutWithDevice: (NSString*) device
    options: (NSArray<SWGOptions>*) options
    completionHandler: (void (^)(NSError* error)) handler;


/// List of user pushes
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// @param device UUID of user device (optional)
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserPush>*
-(NSURLSessionTask*) userPushGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    device: (NSString*) device
    completionHandler: (void (^)(NSArray<SWGUserPush>* output, NSError* error)) handler;


/// Create push for user
/// 
///
/// @param device Device&#39;s UUID
/// @param options 
/// 
///  code:201 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPushPostWithDevice: (NSString*) device
    options: (NSArray<SWGOptions>*) options
    completionHandler: (void (^)(NSError* error)) handler;


/// Updating a user
/// 
///
/// @param name Name of user
/// @param gender Gender of user (1 - male, 2 - female)
/// @param city ID of user city (optional)
/// @param address Address of user (optional)
/// @param birthday Birth day of user (optional)
/// 
///  code:202 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userPutWithName: (NSString*) name
    gender: (NSNumber*) gender
    city: (NSNumber*) city
    address: (NSString*) address
    birthday: (NSString*) birthday
    completionHandler: (void (^)(NSError* error)) handler;


/// List of user salons
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city is optional, if need only select city (optional)
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGSalon>*
-(NSURLSessionTask*) userSalonGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(NSArray<SWGSalon>* output, NSError* error)) handler;


/// Delete salon of user
/// 
///
/// @param _id ID of salon
/// @param site site (4 - apps)
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userSalonIdDeleteWithId: (NSNumber*) _id
    site: (NSNumber*) site
    completionHandler: (void (^)(NSError* error)) handler;


/// Add salon to favorite
/// 
///
/// @param site site (4 - apps)
/// @param salonId Salon id
/// 
///  code:201 message:"ok",
///  code:403 message:"access denied",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userSalonPostWithSite: (NSNumber*) site
    salonId: (NSNumber*) salonId
    completionHandler: (void (^)(NSError* error)) handler;


/// Search popular queries list for user
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city, if empty | popular search aggregate by all cities
/// @param q Start query string, for relevant query search find. Can be empty for top 10 result (optional)
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGPopular>*
-(NSURLSessionTask*) userSearchPopularGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    q: (NSString*) q
    completionHandler: (void (^)(NSArray<SWGPopular>* output, NSError* error)) handler;


/// List of user socials
/// 
///
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return NSArray<SWGUserSocial>*
-(NSURLSessionTask*) userSocialGetWithCompletionHandler: 
    (void (^)(NSArray<SWGUserSocial>* output, NSError* error)) handler;


/// Delete social of user
/// 
///
/// @param _id ID of social
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// 
///  code:204 message:"ok",
///  code:403 message:"access denied",
///  code:404 message:"not found",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userSocialIdDeleteWithId: (NSNumber*) _id
    lang: (NSNumber*) lang
    site: (NSNumber*) site
    completionHandler: (void (^)(NSError* error)) handler;


/// Create social for user
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps) (default to 4)
/// @param serviceName Name of service
/// @param token Token of service
/// 
///  code:201 message:"ok",
///  code:400 message:"access denied",
///  code:403 message:"access denied",
///  code:404 message:"not found",
///  code:0 message:"unexpected error"
///
/// @return 
-(NSURLSessionTask*) userSocialPostWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    serviceName: (NSString*) serviceName
    token: (NSString*) token
    completionHandler: (void (^)(NSError* error)) handler;



@end
