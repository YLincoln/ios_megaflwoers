#import <Foundation/Foundation.h>
#import "SWGErrorModel.h"
#import "SWGFilter.h"
#import "SWGInlineResponse2001.h"
#import "SWGApi.h"

/**
* MegaApi
* REST API
*
* OpenAPI spec version: 1.0
* Contact: itdep@megaflowers.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SWGFilterApi: NSObject <SWGApi>

extern NSString* kSWGFilterApiErrorDomain;
extern NSInteger kSWGFilterApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// List of filters
/// 
///
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// @param parent parent section (optional)
/// @param filters filters array (optional)
/// @param price price filter (optional)
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return SWGInlineResponse2001*
-(NSURLSessionTask*) filterGetWithLang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    parent: (NSNumber*) parent
    filters: (NSString*) filters
    price: (NSString*) price
    completionHandler: (void (^)(SWGInlineResponse2001* output, NSError* error)) handler;


/// Detail view of filter
/// 
///
/// @param _id ID of filter
/// @param lang language (1 - ru, 2 - en)
/// @param site site (4 - apps)
/// @param city city
/// 
///  code:200 message:"ok",
///  code:0 message:"unexpected error"
///
/// @return SWGFilter*
-(NSURLSessionTask*) filterIdGetWithId: (NSNumber*) _id
    lang: (NSNumber*) lang
    site: (NSNumber*) site
    city: (NSNumber*) city
    completionHandler: (void (^)(SWGFilter* output, NSError* error)) handler;



@end
