//
//  RLMArray+NSArray.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "RLMArray+NSArray.h"

@implementation RLMArray (NSArray)

- (NSArray *)array
{
    NSMutableArray *array = [NSMutableArray new];
    
    for (RLMObject * object in self)
    {
        [array addObject:object];
    }
    
    return array;
}

@end
