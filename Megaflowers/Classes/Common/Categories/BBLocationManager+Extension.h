//
//  BBLocationManager+Extension.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "BBLocationManager.h"

#define LOCATION_MANAGER [BBLocationManager sharedManager]

@interface BBLocationManager (Extension)

- (CLLocation *)currentLocation;
- (void)updateLocationWithHandler:(void(^)())handler;
- (void)googleFetchPolylineToDestination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler;

@end
