//
//  UIScrollView+Extension.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "UIScrollView+Extension.h"

@implementation UIScrollView (Extension)

- (BOOL)isBottom
{
    return self.contentOffset.y >= (self.contentSize.height - self.frame.size.height);
}

- (BOOL)isTop
{
    return self.contentOffset.y < 0;
}

- (void)registerNibArray:(NSArray <NSString *> *)nibArray
{
    if ([self isKindOfClass:[UITableView class]])
    {
        UITableView * tableView = (UITableView *)self;
        for (NSString * className in nibArray)
        {
            [tableView registerNib:[UINib nibWithNibName:className bundle:nil] forCellReuseIdentifier:className];
        }
        
    }
    else if ([self isKindOfClass:[UICollectionView class]])
    {
        UICollectionView * collectionView = (UICollectionView *)self;
        for (NSString * className in nibArray)
        {
            [collectionView registerNib:[UINib nibWithNibName:className bundle:nil] forCellWithReuseIdentifier:className];
        }
    }
}

@end
