//
//  RLMResults+NSArray.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMResults (NSArray)

- (NSArray *)array;

@end
