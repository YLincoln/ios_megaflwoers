//
//  NSObject+Extension.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/28/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Extension)

- (NSArray *)allPropertyNames;
- (NSDictionary *)propertyDictionary;

@end
