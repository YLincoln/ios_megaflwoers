//
//  NSDate+Extension.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NSDate+Extension.h"

@implementation NSDate (Extension)

- (NSString *)dateStringByLocalFormat:(NSString *)format
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:self];
}

- (NSString *)dateStringByFormat:(NSString *)format
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    return [dateFormatter stringFromDate:self];
}

- (NSString *)serverString
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:serverFormat];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    return [dateFormatter stringFromDate:self];
}

- (NSString *)iso8601String
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale * enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:iso8601Format];
    
    return [dateFormatter stringFromDate:self];
}

- (NSString *)presentString
{
    if ([self isEqualToDay:[NSDate date]])
    {
        return NSLocalizedString(@"Сегодня", nil);
    }
    else
    {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:presentFormat];
        
        return [dateFormatter stringFromDate:self];
    }
}

+ (NSDate *)dateFromString:(NSString *)dateString format:(NSString *)format
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    return [dateFormatter dateFromString:dateString];
}

+ (NSDate *)dateFromServerString:(NSString *)dateString
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:serverFormat];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    return [dateFormatter dateFromString:dateString];
}

- (NSDate *)dateByMovingToBeginningOfDay
{
    NSDateComponents * parts = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:self];
    [parts setHour:0];
    [parts setMinute:0];
    [parts setSecond:0];
    
    return [[[NSCalendar currentCalendar] dateFromComponents:parts] GMTDate];
}

- (NSDate *)dateByMovingToEndOfDay
{
    NSDateComponents * parts = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:self];
    
    [parts setHour:23];
    [parts setMinute:59];
    [parts setSecond:59];
    
    return [[[NSCalendar currentCalendar] dateFromComponents:parts] GMTDate];
}

- (NSDate *)GMTDate
{
    NSDateComponents * parts = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:self];
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    return [calendar dateFromComponents:parts];
}

@end
