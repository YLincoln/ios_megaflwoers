//
//  RLMArray+NSArray.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMArray (NSArray)

- (NSArray *)array;

@end
