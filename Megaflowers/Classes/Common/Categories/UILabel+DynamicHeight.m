//
//  UILabel+DynamicHeight.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "UILabel+DynamicHeight.h"

@implementation UILabel (DynamicHeight)

- (CGSize)sizeOfMultiLineLabel
{
    CGSize newSize = [self.text boundingRectWithSize:(CGSize){CGFLOAT_MAX, CGRectGetWidth(self.bounds)}
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName : self.font}
                                             context:nil].size;
    
    return CGSizeMake(newSize.width + 10, newSize.height + 10);
}

- (CGFloat)heightOfMultiLineLabel
{
    CGSize newSize = [self.text boundingRectWithSize:(CGSize){CGRectGetWidth(self.bounds), CGFLOAT_MAX}
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName : self.font}
                                             context:nil].size;
    
    return newSize.height + 1;
}

@end
