//
//  UIScrollView+Extension.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Extension)

- (BOOL)isBottom;
- (BOOL)isTop;
- (void)registerNibArray:(NSArray <NSString *> *)nibArray;

@end
