//
//  RLMResults+NSArray.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "RLMResults+NSArray.h"

@implementation RLMResults (NSArray)

- (NSArray *)array
{
    NSMutableArray *array = [NSMutableArray new];
    
    for (RLMObject * object in self)
    {
        [array addObject:object];
    }
    
    return array;
}

@end
