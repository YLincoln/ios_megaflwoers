//
//  NSString+Extension.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

- (id)objectFromJSONString;
- (NSString *)stringByStrippingHTML;

@end
