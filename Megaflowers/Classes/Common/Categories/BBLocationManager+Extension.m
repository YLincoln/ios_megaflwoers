//
//  BBLocationManager+Extension.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "BBLocationManager+Extension.h"

@implementation BBLocationManager (Extension)

- (CLLocation *)currentLocation
{
    NSDictionary * dict = LOCATION_MANAGER.location;
    
    if (dict)
    {
        return [[CLLocation alloc] initWithLatitude:[dict[BB_LATITUDE] doubleValue] longitude:[dict[BB_LONGITUDE] doubleValue]];
    }
    
    return nil;
    
}

- (void)updateLocationWithHandler:(void (^)())handler
{
    [LOCATION_MANAGER getCurrentLocationWithCompletion:^(BOOL success, NSDictionary *locationDictionary, NSError *error) {
        if (handler) handler();
    }];
}

- (void)googleFetchPolylineToDestination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    CLLocation * origin = [LOCATION_MANAGER currentLocation];
    NSString * originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString * destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString * directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString * directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving", directionsAPI, originString, destinationString];
    NSURL * directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    
    NSURLSessionDataTask * fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                  ^(NSData *data, NSURLResponse *response, NSError *error)
                                                  {
                                                      if (error)
                                                      {
                                                          if (completionHandler)
                                                          {
                                                              completionHandler(nil);
                                                          }
                                                          return;
                                                      }
                                                      else if (data)
                                                      {
                                                          NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                          NSArray * routesArray = [json objectForKey:@"routes"];
                                                          
                                                          dispatch_sync(dispatch_get_main_queue(), ^{
                                                              
                                                              GMSPolyline * polyline = nil;
                                                              if ([routesArray count] > 0)
                                                              {
                                                                  NSDictionary * routeDict = [routesArray objectAtIndex:0];
                                                                  NSDictionary * routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                                  NSString * points = [routeOverviewPolyline objectForKey:@"points"];
                                                                  GMSPath * path = [GMSPath pathFromEncodedPath:points];
                                                                  polyline = [GMSPolyline polylineWithPath:path];
                                                              }
                                                              
                                                              if (completionHandler)
                                                              {
                                                                  completionHandler(polyline);
                                                              }
                                                          });
                                                      }
                                                  }];
    [fetchDirectionsTask resume];
}

@end
