//
//  UIWindow+PazLabs.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/31/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (PazLabs)

- (UIViewController *)visibleController;
- (CGRect)rectForTabbarItemByIndex:(NSInteger)index;
- (CGPoint)basketCenter;

@end
