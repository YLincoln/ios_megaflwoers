//
//  UILabel+DynamicHeight.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (DynamicHeight)

- (CGSize)sizeOfMultiLineLabel;
- (CGFloat)heightOfMultiLineLabel;

@end
