//
//  NSError+SWGResponseObject.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NSError+SWGResponseObject.h"

@implementation NSError (SWGResponseObject)

- (NSString *)swaggerDescription
{
    if (self.userInfo[@"SWGResponseObject"])
    {
        if ([self.userInfo[@"SWGResponseObject"] isKindOfClass:[NSDictionary class]])
        {
            return self.userInfo[@"SWGResponseObject"][@"message"];
        }
        else if ([self.userInfo[@"SWGResponseObject"] isKindOfClass:[NSData class]])
        {
            NSDictionary * userInfo = [[[NSString alloc] initWithData:self.userInfo[@"SWGResponseObject"] encoding:NSUTF8StringEncoding] objectFromJSONString];
            if ([userInfo isKindOfClass:[NSDictionary class]])
            {
                return userInfo[@"message"];
            }
        }
    }
    
    return self.localizedDescription;
}

- (NSInteger)swaggerCode
{
    if (self.userInfo[@"SWGResponseObject"])
    {
        if ([self.userInfo[@"SWGResponseObject"] isKindOfClass:[NSDictionary class]])
        {
            return [self.userInfo[@"SWGResponseObject"][@"status"] integerValue];
        }
        else if ([self.userInfo[@"SWGResponseObject"] isKindOfClass:[NSData class]])
        {
            NSDictionary * userInfo = [[[NSString alloc] initWithData:self.userInfo[@"SWGResponseObject"] encoding:0] objectFromJSONString];
            if ([userInfo isKindOfClass:[NSDictionary class]])
            {
                return [userInfo[@"status"] integerValue];
            }
        }
    }
    
    return self.code;
}

- (NSDictionary *)errorFields
{
    if ([self.userInfo[@"SWGResponseObject"] isKindOfClass:[NSData class]])
    {
        NSDictionary * userInfo = [[[NSString alloc] initWithData:self.userInfo[@"SWGResponseObject"] encoding:NSUTF8StringEncoding] objectFromJSONString];
        if ([userInfo isKindOfClass:[NSDictionary class]])
        {
            return userInfo[@"errors"];
        }
    }
    
    return nil;;
}

- (NSString *)interfaceDescription
{
    if (self.swaggerCode == kCFURLErrorNotConnectedToInternet ||self.swaggerCode == kCFURLErrorNetworkConnectionLost)
    {
        return NSLocalizedString(@"Отсутствует соединение с сетью", nil);
    }
    else if (self.swaggerCode >= 400 && self.swaggerCode < 500)
    {
        return self.swaggerDescription;
    }
    else if ((self.swaggerCode >= 500 && self.swaggerCode < 600) || self.swaggerCode == 0)
    {
        return NSLocalizedString(@"Что-то пошло не так( Попробуйте позже", nil);
    }
    
    return @"";
}

@end
