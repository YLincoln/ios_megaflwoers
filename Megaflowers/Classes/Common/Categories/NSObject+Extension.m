//
//  NSObject+Extension.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/28/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NSObject+Extension.h"
#import <objc/runtime.h>

@implementation NSObject (Extension)

- (NSArray *)allPropertyNames
{
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableArray *rv = [NSMutableArray array];
    
    unsigned i;
    for (i = 0; i < count; i++)
    {
        objc_property_t property = properties[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        [rv addObject:name];
    }
    
    free(properties);
    
    return rv;
}

- (NSDictionary *)propertyDictionary
{
    NSMutableDictionary * dictionary = [NSMutableDictionary new];
    
    for (NSString * key in [self allPropertyNames])
    {
        id value = [self valueForKey:key];
        if (value) [dictionary setValue:value forKey:key];
    }
    
    return dictionary;
}

@end
