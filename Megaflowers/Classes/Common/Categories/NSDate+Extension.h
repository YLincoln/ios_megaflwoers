//
//  NSDate+Extension.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * serverFormat      = @"yyyy-MM-dd";
static NSString * presentFormat     = @"E, d MMMM yyyy";
static NSString * iso8601Format     = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";

@interface NSDate (Extension)

+ (NSDate *)dateFromString:(NSString *)dateString format:(NSString *)format;
+ (NSDate *)dateFromServerString:(NSString *)dateString;
- (NSString *)dateStringByLocalFormat:(NSString *)format;
- (NSString *)dateStringByFormat:(NSString *)format;
- (NSString *)serverString;
- (NSString *)iso8601String;
- (NSString *)presentString;
- (NSDate *)dateByMovingToBeginningOfDay;
- (NSDate *)dateByMovingToEndOfDay;
- (NSDate *)GMTDate;

@end
