//
//  UIColor+HEX.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/3/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HEX)

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
