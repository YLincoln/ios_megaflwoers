//
//  UIWindow+PazLabs.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/31/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "UIWindow+PazLabs.h"

@implementation UIWindow (PazLabs)

- (UIViewController *)visibleController
{
    UIViewController * rootViewController = self.rootViewController;
    return [UIWindow getVisibleViewControllerFrom:rootViewController];
}

+ (UIViewController *) getVisibleViewControllerFrom:(UIViewController *)vc
{
    if ([vc isKindOfClass:[UINavigationController class]])
    {
        return [UIWindow getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    }
    else if ([vc isKindOfClass:[UITabBarController class]])
    {
        return [UIWindow getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    }
    else
    {
        if (vc.presentedViewController)
        {
            return [UIWindow getVisibleViewControllerFrom:vc.presentedViewController];
        }
        else
        {
            return vc;
        }
    }
}

- (CGRect)rectForTabbarItemByIndex:(NSInteger)index
{
    CGRect itemRect = [self frameForTabInTabBar:self.visibleController.tabBarController.tabBar withIndex:index];
    return CGRectMake(itemRect.origin.x, ViewHeight(WINDOW) - itemRect.size.width + 20, itemRect.size.width, itemRect.size.width);
}

- (CGPoint)basketCenter
{
    UITabBarController * tabBarController = self.visibleController.tabBarController;
    CGRect tabBarRect = tabBarController.tabBar.frame;
    NSInteger buttonCount = tabBarController.tabBar.items.count;
    
    CGFloat containingWidth = tabBarRect.size.width/buttonCount;
    CGFloat originX = containingWidth * 1;
    CGRect containingRect = CGRectMake( originX, 0, containingWidth, tabBarController.tabBar.frame.size.height );
    CGPoint center = CGPointMake( CGRectGetMidX(containingRect), CGRectGetMidY(containingRect));
    center.y += ViewHeight(self.visibleController.view);
    
    return center;
}

#pragma mark - Private
- (CGRect)frameForTabInTabBar:(UITabBar *)tabBar withIndex:(NSUInteger)index
{
    NSMutableArray * tabBarItems = [NSMutableArray arrayWithCapacity:[tabBar.items count]];
    
    for (UIView * view in tabBar.subviews)
    {
        if ([view isKindOfClass:NSClassFromString(@"UITabBarButton")] && [view respondsToSelector:@selector(frame)]) {
            // check for the selector -frame to prevent crashes in the very unlikely case that in the future
            // objects thar don't implement -frame can be subViews of an UIView
            [tabBarItems addObject:view];
        }
    }
    if ([tabBarItems count] == 0)
    {
        // no tabBarItems means either no UITabBarButtons were in the subView, or none responded to -frame
        // return CGRectZero to indicate that we couldn't figure out the frame
        return CGRectZero;
    }
    
    // sort by origin.x of the frame because the items are not necessarily in the correct order
    [tabBarItems sortUsingComparator:^NSComparisonResult(UIView *view1, UIView *view2) {
        if (view1.frame.origin.x < view2.frame.origin.x)
        {
            return NSOrderedAscending;
        }
        if (view1.frame.origin.x > view2.frame.origin.x)
        {
            return NSOrderedDescending;
        }
        return NSOrderedSame;
    }];
    
    CGRect frame = CGRectZero;
    if (index < [tabBarItems count])
    {
        // viewController is in a regular tab
        UIView * tabView = tabBarItems[index];
        if ([tabView respondsToSelector:@selector(frame)])
        {
            frame = tabView.frame;
        }
    }
    else
    {
        // our target viewController is inside the "more" tab
        UIView * tabView = [tabBarItems lastObject];
        if ([tabView respondsToSelector:@selector(frame)])
        {
            frame = tabView.frame;
        }
    }
    
    return frame;
}

@end
