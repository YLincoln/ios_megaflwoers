//
//  UserData.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITStorageManager.h"
#import "UserDataContstants.h"

@class SWGUserDiscount, SWGOrder;

#define USER [UserData sharedInstance]

@interface UserDiscount : NITStorageManager

@property (nonatomic) DiscountType type;
@property (nonatomic) NSString * code;
@property (nonatomic) NSNumber * value;
@property (nonatomic) NSNumber * order_sum;
@property (nonatomic) NSNumber * order_sum_retail;

+ (UserDiscount *)discontFromModel:(SWGUserDiscount *)model;
- (void)clear;

@end

@interface UserData : NITStorageManager

+ (UserData *) sharedInstance;

@property (nonatomic) NSString * identifier;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * password;
@property (nonatomic) NSString * cityName;
@property (nonatomic) NSString * address;
@property (nonatomic) NSString * email;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSString * cityPhone;
@property (nonatomic) NSString * accessToken;
@property (nonatomic) NSString * appAccessToken;

@property (nonatomic) NSNumber * cityID;
@property (nonatomic) NSNumber * countryID;
@property (nonatomic) NSNumber * bonus;
@property (nonatomic) UserDiscount * discount;
@property (nonatomic) NSNumber * basketPrice;
@property (nonatomic) NSNumber * consent;
@property (nonatomic) NSDate * birthday;

@property (nonatomic) NSInteger appRate;
@property (nonatomic) NSDate * showRateAppDate;

@property (nonatomic) NSString  * pushId;
@property (nonatomic) NSData  * pushToken;

@property (nonatomic) NSDictionary * attributes;

@property (nonatomic) Gender gender;
@property (nonatomic) CatalogItemsViewMode viewType;
@property (nonatomic) AuthorizeState state;

@property (nonatomic, readonly) NSString * pushTokenString;
@property (nonatomic, readonly) BOOL isAutorize;

@property (nonatomic) NSNumber * orderID;
@property (nonatomic) NSNumber * lastOrderID;
@property (nonatomic) NSString * lastOrderDate;

- (void)logout;
- (void)checkAutorizationWithHandler:(void(^)(BOOL success))handler;
- (void)updateLastOrder:(SWGOrder *)order;

@end
