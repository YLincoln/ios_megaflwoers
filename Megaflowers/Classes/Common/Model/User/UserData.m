//
//  UserData.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "UserData.h"
#import "NITOrder.h"
#import "NITCoupon.h"
#import "NITFavoriteBouquet.h"
#import "NITFavoriteSalon.h"
#import "SWGConfiguration.h"
#import "SWGDefaultConfiguration.h"
#import "SWGUserDiscount.h"
#import "SWGAccountApi.h"
#import "SWGOrder.h"

@implementation UserDiscount

+ (UserDiscount *)discontFromModel:(SWGUserDiscount *)model
{
    UserDiscount * discount = [UserDiscount new];
    
    if (model)
    {
        discount.type = model.type.integerValue;
        discount.code = model.code;
        discount.value = model.value;
        discount.order_sum = model.orderSum;
        discount.order_sum_retail = model.orderSumRetail;
    }
    
    return discount;
}

#pragma mark - Custom Accessors
- (DiscountType)type
{
    return (DiscountType)[self Integer:kDiscountType];
}

- (void)setType:(DiscountType)type
{
    [self setInteger:kDiscountType value:type];
}

- (NSString *)code
{
    return [self CriptoString:kDiscountCode];
}

- (void)setCode:(NSString *)code
{
    [self setCriptoString:kDiscountCode value:code];
}

- (NSNumber *)value
{
    return @([self Integer:kDiscountValue]);
}

- (void)setValue:(NSNumber *)value
{
    [self setInteger:kDiscountValue value:value.integerValue];
}

- (NSNumber *)order_sum
{
    return @([self Integer:kDiscountSum]);
}

- (void)setOrder_sum:(NSNumber *)order_sum
{
    [self setInteger:kDiscountSum value:order_sum.integerValue];
}

- (NSNumber *)order_sum_retail
{
    return @([self Integer:kDiscountSumRetail]);
}

- (void)setOrder_sum_retail:(NSNumber *)order_sum_retail
{
    [self setInteger:kDiscountSumRetail value:order_sum_retail.integerValue];
}

#pragma mark - Public
- (void)clear
{
    self.code = nil;
    self.value = nil;
    self.order_sum = nil;
    self.order_sum_retail = nil;
}

@end

#pragma mark -
@implementation UserData

+ (UserData *)sharedInstance
{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
        [instance setup];
    });
    
    return instance;
}

- (void)setup
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout) name:kResetSessionNotification object:nil];
}

#pragma mark - Custom Accessors
- (NSString *)identifier
{
    return [self CriptoString:kUserIdentifier];
}

- (void)setIdentifier:(NSString *)identifier
{
    [self setCriptoString:kUserIdentifier value:identifier];
}

- (NSString *)name
{
    return [self CriptoString:kUserName];
}

- (void)setName:(NSString *)name
{
    [self setCriptoString:kUserName value:name];
}

- (NSString *)password
{
    return [self CriptoString:kUserPassword];
}

- (void)setPassword:(NSString *)password
{
    [self setCriptoString:kUserPassword value:password];
}

- (NSString *)cityName
{
    return [self CriptoString:kUserCityName];
}

- (void)setCityName:(NSString *)cityName
{
    [self setCriptoString:kUserCityName value:cityName];
}

- (NSString *)cityPhone
{
    return [self CriptoString:kUserCityPhone];
}

- (void)setCityPhone:(NSString *)cityPhone
{
    [self setCriptoString:kUserCityPhone value:cityPhone];
}

- (NSNumber *)cityID
{
    return @([self Integer:kUserCityID]);
}

- (void)setCityID:(NSNumber *)cityID
{
    [self setInteger:kUserCityID value:[cityID integerValue]];
}

- (NSNumber *)countryID
{
    return @([self Integer:kUserCountryID]);
}

- (void)setCountryID:(NSNumber *)countryID
{
    [self setInteger:kUserCountryID value:[countryID integerValue]];
}

- (NSString *)address
{
    return [self CriptoString:kUserAddress];
}

- (void)setAddress:(NSString *)address
{
    [self setCriptoString:kUserAddress value:address];
}

- (NSNumber *)bonus
{
    return @([self Integer:kUserBonus]);
}

- (void)setBonus:(NSNumber *)bonus
{
    [self setInteger:kUserBonus value:[bonus integerValue]];
}

- (UserDiscount *)discount
{
    UserDiscount * discount = [UserDiscount new];
    
    if (discount.code.length > 0)
    {
        return discount;
    }
    
    return nil;
}

- (void)setDiscount:(UserDiscount *)discount
{
    if (discount == nil)
    {
        [[UserDiscount new] clear];
    }
}

- (NSNumber *)consent
{
    return @([self Integer:kUserConsent]);
}

- (void)setConsent:(NSNumber *)consent
{
    [self setInteger:kUserConsent value:[consent integerValue]];
}

- (NSDate *)birthday
{
    return [self Object:kUserBirthday];
}

- (void)setBirthday:(NSDate *)birthday
{
    [self setObject:kUserBirthday value:birthday];
}

- (Gender)gender
{
    return (Gender)[self Integer:kUserGender];
}

- (void)setGender:(Gender)gender
{
    NSAssert(gender == GenderUnknow || gender == GenderMale || gender == GenderFamale, @"Error range gender");
    [self setInteger:kUserGender value:gender];
}

- (CatalogItemsViewMode)viewType
{
    return (CatalogItemsViewMode)[self Integer:kCatalogItemsViewMode];
}

- (void)setViewType:(CatalogItemsViewMode)viewType
{
    [self setInteger:kCatalogItemsViewMode value:viewType];
}

- (NSString *)phone
{
    return [self CriptoString:kUserPhone];
}

- (void)setPhone:(NSString *)phone
{
    [self setCriptoString:kUserPhone value:phone];
}

- (NSString *)email
{
    return [self CriptoString:kUserEmail];
}

- (void)setEmail:(NSString *)email
{
    [self setCriptoString:kUserEmail value:email];
}

- (NSString *)accessToken
{
    return [self CriptoString:kUserAccessToken];
}

- (void)setAccessToken:(NSString *)accessToken
{
    [[SWGDefaultConfiguration sharedConfig] setApiKey:accessToken forApiKeyIdentifier:kAuthorizationKey];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateSearchesResults object:nil];
    [self setCriptoString:kUserAccessToken value:accessToken];
}

- (NSString *)appAccessToken
{
    return [self CriptoString:kAppAccessToken];
}

- (void)setAppAccessToken:(NSString *)appAccessToken
{
    [self setCriptoString:kAppAccessToken value:appAccessToken];
}

- (void)setPushId:(NSString *)pushId
{
    [self setCriptoString:kUserPushId value:pushId];
}

- (NSString *)pushId
{
    return [self CriptoString:kUserPushId];
}

- (void)setPushToken:(NSData *)pushToken
{
    [self setData:kUserPushToken value:pushToken];
}

- (NSData *)pushToken
{
    return [self Data:kUserPushToken];
}

- (NSString *)pushTokenString
{
    NSData *tokenData = self.pushToken;
    
    if (tokenData)
    {
        const char * data = [tokenData bytes];
        NSMutableString * token = [NSMutableString string];
        
        for (int i = 0; i < [tokenData length]; i++)
        {
            [token appendFormat:@"%02.2hhX", data[i]];
        }
        
        return [token copy];
    }
    else
    {
        return nil;
    }
}

- (NSInteger)appRate
{
    return [self Integer:kAppRate];
}

- (void)setAppRate:(NSInteger)appRate
{
    [self setInteger:kAppRate value:appRate];
}

- (NSDate *)showRateAppDate
{
    return [self Object:kShowRateAppDate];
}

- (void)setShowRateAppDate:(NSDate *)showRateAppDate
{
    [self setObject:kShowRateAppDate value:showRateAppDate];
}

- (NSDictionary *)attributes
{
    NSMutableDictionary * result = [NSMutableDictionary new];
    
    return result;
}

- (void)setAttributes:(NSDictionary *)attributes
{
    
}

- (BOOL)isAutorize
{
    return self.identifier.length > 0 && self.accessToken.length > 0;
}

- (NSNumber *)orderID
{
    return @([self Integer:kUserOrderID]);
}

- (void)setOrderID:(NSNumber *)orderID
{
    [self setInteger:kUserOrderID value:[orderID integerValue]];
}

- (NSNumber *)lastOrderID
{
    return @([self Integer:kUserLastOrderID]);
}

- (void)setLastOrderID:(NSNumber *)lastOrderID
{
    [self setInteger:kUserLastOrderID value:[lastOrderID integerValue]];
}

- (NSString *)lastOrderDate
{
    return [self CriptoString:kUserLastOrderDate];
}

- (void)setLastOrderDate:(NSString *)lastOrderDate
{
    [self setCriptoString:kUserLastOrderDate value:lastOrderDate];
}

- (NSNumber *)basketPrice
{
    return @([self Integer:kBasketPrice]);
}

- (void)setBasketPrice:(NSNumber *)basketPrice
{
    [self setInteger:kBasketPrice value:basketPrice.integerValue];
}

- (AuthorizeState)state
{
    return (AuthorizeState)[self Integer:kAuthorizeState];
}

- (void)setState:(AuthorizeState)state
{
    [self setInteger:kAuthorizeState value:state];
}

#pragma mark - Public
- (void)logout
{
    self.identifier     = nil;
    self.address        = nil;
    self.email          = nil;
    self.phone          = nil;
    self.bonus          = nil;
    self.birthday       = nil;
    self.discount       = nil;
    self.consent        = nil;
    self.pushToken      = nil;
    self.attributes     = nil;
    self.accessToken    = nil;
    self.password       = nil;
    self.discount       = nil;
    self.gender         = GenderUnknow;
    self.state          = AuthorizeStateNone;
    
    [NITEvent deleteAllEvents];
    [NITOrder deleteAllOrders];
    [NITCoupon deleteAllCoupons];
    [NITFavoriteBouquet deleteAllBouquets];
    [NITFavoriteSalon deleteAllSalons];
}

- (void)checkAutorizationWithHandler:(void(^)(BOOL success))handler
{
    if ([self isAutorize])
    {
        if (handler) handler(true);
    }
    else
    {
        [HELPER startLoading];
        [[SWGAccountApi new] loginPostWithPhone:nil
                                          email:@"api@mega.ru"
                                    serviceName:nil
                                   serviceToken:nil
                                       password:@"megapassword"
                              completionHandler:^(NSString *output, NSError *error) {
                                  [HELPER stopLoading];
                                  if (error)
                                  {
                                      [HELPER logError:error method:METHOD_NAME];
                                      handler(false);
                                  }
                                  else
                                  {
                                      USER.accessToken = output;
                                      handler(true);
                                  }
                              }];
    }
}

- (void)updateLastOrder:(SWGOrder *)order
{
    self.lastOrderID = order._id;
    self.lastOrderDate = order.deliveryDate;
    
    [API sendPushTags];
}

@end
