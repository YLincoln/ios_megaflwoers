//
//  UserDataContstants.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef NS_ENUM(NSInteger, Gender) {
    GenderUnknow    = 0,
    GenderMale      = 1,
    GenderFamale    = 2
};

typedef NS_ENUM(NSInteger, DiscountType) {
    DiscountTypeAccumilation = 0,
    DiscountTypeFixed        = 1
};

typedef CF_ENUM (NSUInteger, CatalogItemsViewMode) {
    CatalogItemsViewModeList    = 0,
    CatalogItemsViewModeSection = 1,
};

typedef NS_ENUM(NSInteger, AuthorizeState) {
    AuthorizeStateNone      = 0,
    AuthorizeStateApi       = 1,
    AuthorizeStateVirtual   = 2,
    AuthorizeStateSuccess   = 3
};

static NSString * const kUserIdentifier     = @"kUserIdentifier";
static NSString * const kUserCityName       = @"kUserCityName";
static NSString * const kUserCityPhone      = @"kUserCityPhone";
static NSString * const kUserCountryID      = @"kUserCountryID";
static NSString * const kUserCityID         = @"kUserCityID";
static NSString * const kUserCity           = @"kUserCity";
static NSString * const kUserAddress        = @"kUserAddress";
static NSString * const kUserBonus          = @"kUserBonus";
static NSString * const kUserConsent        = @"kUserConsent";
static NSString * const kUserBirthday       = @"kUserBirthday";
static NSString * const kUserGender         = @"kUserGender";
static NSString * const kUserPhone          = @"kUserPhone";
static NSString * const kUserEmail          = @"kUserEmail";
static NSString * const kUserPushId         = @"kUserPushId";
static NSString * const kUserPushToken      = @"kUserPushToken";
static NSString * const kUserAccessToken    = @"kUserAccessToken";
static NSString * const kAppAccessToken     = @"kAppAccessToken";
static NSString * const kUserName           = @"kUserName";
static NSString * const kUserPassword       = @"kUserPassword";
static NSString * const kAppRate            = @"kAppRate";
static NSString * const kShowRateAppDate    = @"kShowRateAppDate";
static NSString * const kCatalogItemsViewMode = @"CatalogItemsViewMode";

static NSString * const kUserOrderID        = @"kUserOrderID";
static NSString * const kUserLastOrderID    = @"kUserLastOrderID";
static NSString * const kUserLastOrderDate  = @"kUserLastOrderDate";
static NSString * const kUserLastOrderHash  = @"kUserLastOrderHash";

static NSString * const kDiscountType       = @"kDiscountType";
static NSString * const kDiscountCode       = @"kDiscountCode";
static NSString * const kDiscountValue      = @"kDiscountValue";
static NSString * const kDiscountSum        = @"kDiscountSum";
static NSString * const kDiscountSumRetail  = @"kDiscountSumRetail";
static NSString * const kBasketPrice        = @"kBasketPrice";
static NSString * const kAuthorizeState     = @"kAuthorizeState";

