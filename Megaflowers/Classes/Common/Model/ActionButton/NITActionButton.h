//
//  NITActionButton.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef void (^NITActionButtonBlock)();

@interface NITActionButton : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NITActionButtonBlock actionBlock;

- (id)initWithTitle:(NSString *)title actionBlock:(NITActionButtonBlock)actionBlock;

@end
