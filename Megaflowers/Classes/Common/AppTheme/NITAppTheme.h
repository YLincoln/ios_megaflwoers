//
//  NITAppTheme.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//


@interface NITAppTheme : NSObject

+ (void)applyTheme;

@end
