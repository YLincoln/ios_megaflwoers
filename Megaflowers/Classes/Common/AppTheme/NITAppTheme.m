//
//  NITAppTheme.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAppTheme.h"

@implementation NITAppTheme

+ (void)applyTheme
{
    // nav bar
    [[UINavigationBar appearance] setTintColor:RGB(10, 88, 43)];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:RGB(10, 88, 43), NSFontAttributeName:[UIFont systemFontOfSize:17]}];
    
    // tab bar
    [[UITabBar appearance] setTintColor:RGB(10, 88, 43)];
    if ([[UITabBar appearance] respondsToSelector:@selector(setUnselectedItemTintColor:)])
    {
        [[UITabBar appearance] setUnselectedItemTintColor:RGB(125, 166, 142)];
    }
}


@end
