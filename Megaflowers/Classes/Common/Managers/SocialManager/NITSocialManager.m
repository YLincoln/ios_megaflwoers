//
//  NITSocialManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/9/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITSocialManager.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <VKSdk.h>
#import "OKSDK.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

static NSString * const kVkAppID  = @"5916214";
static NSString * const kOKAppID  = @"1250181632";
static NSString * const kOKAppKey = @"CBADEFILEBABABABA";

@interface NITSocialManager () <VKSdkDelegate>

@property (nonatomic, strong) UIViewController * currentVC;
@property (nonatomic, strong) SocialResponceBlock currentResponseBlock;

@end

@implementation NITSocialManager

+ (NITSocialManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITSocialManager * shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id) init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void) setup
{
    
}

#pragma mark - Public
- (void)setViewController:(UIViewController *)viewController
{
    self.currentVC = viewController;
}

#pragma mark - FB
- (void)fbLoginWithHandler:(SocialResponceBlock)handler
{
    NSParameterAssert(self.currentVC);
    
    FBSDKLoginManager * login = [[FBSDKLoginManager alloc] init];
    
    [login logInWithReadPermissions:@[@"email", @"public_profile", @"user_birthday", @"user_about_me", @"user_location", @"user_friends", @"user_relationships"] fromViewController:self.currentVC handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error)
        {
            DDLogError(@"FB logien error: %@", error.localizedDescription);
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(false, nil, error);
            });
        }
        else if (result.isCancelled)
        {
            DDLogInfo(@"FB logien cancelled");
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(false, nil, error);
            });
        }
        else
        {
            if ([result.grantedPermissions containsObject:@"email"] && [FBSDKAccessToken currentAccessToken])
            {
                handler(true, [FBSDKAccessToken currentAccessToken].tokenString, nil);
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, picture.type(large), email, age_range, birthday, gender"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                     
                     if (!error)
                     {
                         DDLogInfo(@"fb user info : %@", result);
//                         dispatch_async(dispatch_get_main_queue(), ^{
//                             handler(true, result, error);
//                         });
                     }
                     else
                     {
                         DDLogError(@"error : %@",error);
//                         dispatch_async(dispatch_get_main_queue(), ^{
//                             handler(false, result, error);
//                         });
                     }
                 }];
            }
        }
    }];
}

#pragma mark - OK
- (void)okLoginWithHandler:(SocialResponceBlock)handler
{
    NSParameterAssert(handler);

    OKSDKInitSettings * settings = [OKSDKInitSettings new];
    settings.appKey = kOKAppKey;
    settings.appId  = kOKAppID;
    settings.controllerHandler = ^{
        return WINDOW.visibleController;
    };
    
    [OKSDK initWithSettings: settings];
    
    [OKSDK authorizeWithPermissions:@[@"VALUABLE_ACCESS",@"LONG_ACCESS_TOKEN",@"PHOTO_CONTENT",@"GET_EMAIL"]
                            success:^(NSArray *data) {
                                
                                DDLogInfo(@"ok user info : %@", data);

                                [OKSDK invokeMethod:@"users.getCurrentUser" arguments:@{} success:^(NSDictionary* data) {
                                    
                                    DDLogInfo(@"ok user info : %@", data);

                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        handler(true, [OKSDK currentAccessToken], nil);
                                    });
                                    
                                } error:^(NSError *error) {
                                    
                                    DDLogError(@"error : %@",error);
                                    
                                    if (error.code == 102)
                                    {
                                        [OKSDK clearAuth];
                                        [self okLoginWithHandler:handler];
                                    }
                                    else
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            handler(false, nil, error);
                                        });
                                    }
                                }];
                            }
                              error:^(NSError *error) {
                                  
                                  DDLogError(@"error : %@",error);
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      handler(false, nil, error);
                                  });
                              }
     ];
}

#pragma mark - VK
- (void)vkLoginWithHandler:(SocialResponceBlock)handler
{
    NSParameterAssert(self.currentVC);
    NSParameterAssert(handler);
    
    self.currentResponseBlock = handler;
    
    [VKSdk initializeWithDelegate:self andAppId:kVkAppID];
    [VKSdk authorize:@[VK_PER_FRIENDS, VK_PER_EMAIL,
//                       VK_PER_ADS,
//                       VK_PER_NOTIFY,
//                       VK_PER_PHOTOS,
//                       VK_PER_AUDIO,
//                       VK_PER_VIDEO,
//                       VK_PER_DOCS,
//                       VK_PER_NOTES,
//                       VK_PER_PAGES,
//                       VK_PER_STATUS,
//                       VK_PER_WALL,
//                       VK_PER_GROUPS,
//                       VK_PER_MESSAGES,
//                       VK_PER_NOTIFICATIONS,
//                       VK_PER_STATS,
//                       VK_PER_OFFLINE,
//                       VK_PER_NOHTTPS
                       ]];
}

#pragma mark - VKSdkDelegate
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self.currentVC presentViewController:controller animated:true completion:nil];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    NSMutableDictionary * data = [NSMutableDictionary new];
    if (newToken.accessToken)
    {
        [data setObject:newToken.accessToken forKey:@"token"];
    }
    if (newToken.email)
    {
        [data setObject:newToken.email forKey:@"email"];
    }

    self.currentResponseBlock(true, data, nil);
    
   /*NSArray * fields = @[
                         keyPath(VKUser.id),
                         keyPath(VKUser.first_name),
                         keyPath(VKUser.last_name),
                         keyPath(VKUser.uid),
                         keyPath(VKUser.photo_100)
                         ];
    
    VKRequest * request = [[VKApi users] get:@{VK_API_FIELDS:[[VKUser new].allPropertyNames componentsJoinedByString:@","], VK_API_USER_IDS:@[[[VKSdk getAccessToken] userId]]}];
    
    [request executeWithResultBlock:^(VKResponse *response) {
        NSArray * data = response.json;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.currentResponseBlock(true, data.firstObject, nil);
            self.currentResponseBlock = nil;
        });
    } errorBlock:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.currentResponseBlock(false, nil, error);
            self.currentResponseBlock = nil;
        });
    }];*/
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    DDLogInfo(@"vkSdkUserDeniedAccess: %@", authorizationError.description);
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    DDLogInfo(@"vkSdkUserDeniedAccess: %@", captchaError.description);
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    DDLogInfo(@"vkSdkTokenHasExpired: %@", expiredToken.accessToken);
    [self vkLoginWithHandler:self.currentResponseBlock];
}

#pragma mark - UIApplicationDelegate
/**
 *  run in Appdelegate openURL
 */
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    // VK
    [VKSdk processOpenURL:url fromApplication:sourceApplication];
    
    // Facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:sourceApplication
                                                annotation:annotation];
    
    // OK
    [OKSDK openUrl:url];
    
    return true;
}

@end
