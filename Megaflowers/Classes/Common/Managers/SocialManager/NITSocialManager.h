//
//  NITSocialManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/9/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#define SOCIAL [NITSocialManager sharedInstance]

typedef void (^ SocialResponceBlock) (BOOL success, id result, NSError * error);

@interface NITSocialManager : NSObject

+ (NITSocialManager *)sharedInstance;

- (void)setViewController:(id)viewController;

#pragma mark - FB
- (void)fbLoginWithHandler:(SocialResponceBlock)handler;

#pragma mark - OK
- (void)okLoginWithHandler:(SocialResponceBlock)handler;

#pragma mark - VK
- (void)vkLoginWithHandler:(SocialResponceBlock)handler;

#pragma mark - UIApplicationDelegate
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

@end
