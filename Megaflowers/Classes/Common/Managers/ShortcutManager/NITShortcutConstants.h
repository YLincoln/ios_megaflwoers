//
//  NITShortcutConstants.h
//  Megaflowers
//
//  Created by Yaroslav Zavyalov on 28.02.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#pragma mark - Keys
static NSString * const kPathViewControllerKey  = @"PathViewController";
