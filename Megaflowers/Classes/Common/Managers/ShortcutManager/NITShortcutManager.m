//
//  NITShortcutManager.m
//  Megaflowers
//
//  Created by Yaroslav Zavyalov on 28.02.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITShortcutManager.h"
#import "NITOrdersListWireFrame.h"
#import "NSArray+BlocksKit.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"

@interface NITShortcutManager ()

@property (nonatomic) NSArray <NSDictionary *> * shortcutItems;

@property (nonatomic) NSMutableDictionary * shortcutItemsActions;
@property (nonatomic) NSArray <NITOrder *> * currentData;
@property (nonatomic) NSArray <NITOrder *> * complitedData;

@end

@implementation NITShortcutManager

#pragma mark - Init
+ (id)sharedInstance
{
    static dispatch_once_t      once;
    static NITShortcutManager   * shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        //Icons
        UIApplicationShortcutIcon *shortcutSearchSearch     = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeSearch];
        UIApplicationShortcutIcon *shortcutSearchAdd        = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeAdd];
        UIApplicationShortcutIcon *shortcutSearchHome       = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeHome];
        UIApplicationShortcutIcon *shortcutSearchShare      = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeShare];
        UIApplicationShortcutIcon *shortcutSearchContact    = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeContact];
        UIApplicationShortcutIcon *shortcutSearchCatalog    = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeBookmark];
        UIApplicationShortcutIcon *shortcutSearchSalons     = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeBookmark];
        UIApplicationShortcutIcon *shortcutSearchCloud      = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeCloud];
        UIApplicationShortcutIcon *shortcutSearchLocation   = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeMarkLocation];
        
        //Items
        self.shortcutItems = [NSArray arrayWithObjects:
                              //1 - Shortcut: [1] - ArrIndex = [0]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"Fast",                                            @"Type",
                                                                            NSLocalizedString(@"Быстрая покупка", nil),         @"Title",
                                                                            NSLocalizedString(@"Звонок в компанию", nil),       @"Subtitle",
                                                                            shortcutSearchContact,                              @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //1 - Shortcut: [2] - ArrIndex = [1]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"Catalog",                                         @"Type",
                                                                            NSLocalizedString(@"Каталог", nil),                 @"Title",
                                                                            NSLocalizedString(@"", nil),                        @"Subtitle",
                                                                            shortcutSearchCatalog,                              @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //1 - Shortcut: [3] - ArrIndex = [2]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"Salons",                                          @"Type",
                                                                            NSLocalizedString(@"Салоны", nil),                  @"Title",
                                                                            NSLocalizedString(@"", nil),                        @"Subtitle",
                                                                            shortcutSearchSalons,                               @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //1 - Shortcut: [4] - ArrIndex = [3]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"Share",                                           @"Type",
                                                                            NSLocalizedString(@"Поделиться приложением", nil),  @"Title",
                                                                            NSLocalizedString(@"", nil),                        @"Subtitle",
                                                                            shortcutSearchShare,                                @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //2 - Shortcut: [1] - ArrIndex = [4]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"Basket",                                          @"Type",
                                                                            NSLocalizedString(@"Корзина", nil),                 @"Title",
                                                                            NSLocalizedString(@"", nil),                        @"Subtitle",
                                                                            shortcutSearchAdd,                                  @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //3 - Shortcut: [1] - ArrIndex = [5]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"Status",                                          @"Type",
                                                                            NSLocalizedString(@"Статус заказа", nil),           @"Title",
                                                                            NSLocalizedString(@"", nil),                        @"Subtitle",
                                                                            shortcutSearchSearch,                               @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //4 - Shortcut: [1] - ArrIndex = [6]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"Room",                                            @"Type",
                                                                            NSLocalizedString(@"Личный кабинет", nil),          @"Title",
                                                                            NSLocalizedString(@"", nil),                        @"Subtitle",
                                                                            shortcutSearchHome,                                 @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //5 - Shortcut: [1] - ArrIndex = [7]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"OrdersComplited",                                 @"Type",
                                                                            NSLocalizedString(@"Мои заказы", nil),              @"Title",
                                                                            NSLocalizedString(@"Заверщенные заказы", nil),      @"Subtitle",
                                                                            shortcutSearchCloud,                                @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //6 - Shortcut: [1] - ArrIndex = [8]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"OrderActive",                                     @"Type",
                                                                            NSLocalizedString(@"Статус заказа", nil),           @"Title",
                                                                            NSLocalizedString(@"Активный заказ", nil),          @"Subtitle",
                                                                            shortcutSearchCloud,                                @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //7 - Shortcut: [1] - ArrIndex = [9]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"OrderPickup",                                     @"Type",
                                                                            NSLocalizedString(@"Мои заказы", nil),              @"Title",
                                                                            NSLocalizedString(@"Активный заказ", nil),          @"Subtitle",
                                                                            shortcutSearchCloud,                                @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                              //7 - Shortcut: [4] - ArrIndex = [10]
                              [NSDictionary dictionaryWithObjectsAndKeys:   @"SalonsWay",                                       @"Type",
                                                                            NSLocalizedString(@"Маршрут", nil),                 @"Title",
                                                                            NSLocalizedString(@"Ближайший салон", nil),         @"Subtitle",
                                                                            shortcutSearchLocation,                             @"Icon",
                                                                            [NSNull null],                                      @"UserInfo",
                               nil],
                             nil];
    }
    
    return self;
}

#pragma mark - Public
- (void)createShortcut
{
    self.shortcutItemsActions = [[NSMutableDictionary alloc] init];

    NSMutableArray <UIApplicationShortcutItem *> * arr = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.shortcutItems.count; i++)
    {
        UIApplicationShortcutItem * item = [self applicationShortcutItemWithType:[[self.shortcutItems objectAtIndex:i] objectForKey:@"Type"]
                                                                       withTitle:[[self.shortcutItems objectAtIndex:i] objectForKey:@"Title"]
                                                                    withSubtitle:[[self.shortcutItems objectAtIndex:i] objectForKey:@"Subtitle"]
                                                                        withIcon:[[self.shortcutItems objectAtIndex:i] objectForKey:@"Icon"]
                                                                    withUserInfo:[[self.shortcutItems objectAtIndex:i] objectForKey:@"UserInfo"]
                                            ];
        [arr addObject:item];
    }
    
    [self.shortcutItemsActions setObject:@[        arr[0],  arr[1], arr[2]        ] forKey:@"First"];
    [self.shortcutItemsActions setObject:@[arr[4], arr[0],  arr[1], arr[2], arr[3]] forKey:@"Second"];
    [self.shortcutItemsActions setObject:@[arr[5], arr[0],  arr[1], arr[2], arr[3]] forKey:@"Third"];
    [self.shortcutItemsActions setObject:@[arr[6], arr[0],  arr[1], arr[2], arr[3]] forKey:@"Fourth"];
    [self.shortcutItemsActions setObject:@[arr[7], arr[0],  arr[1], arr[2], arr[3]] forKey:@"Fiveth"];
    [self.shortcutItemsActions setObject:@[arr[8], arr[0],  arr[1], arr[2], arr[3]] forKey:@"Sixth"];
    [self.shortcutItemsActions setObject:@[arr[9], arr[10], arr[0], arr[1], arr[3]] forKey:@"Seventh"];
    
    [self createShortcutByModel];
}

- (void)deleteShortcut
{
    [UserDefaults removeObjectForKey:kPathViewControllerKey];
}

- (BOOL)isCreatedShortcut
{
    if([[[UserDefaults dictionaryRepresentation] allKeys] containsObject:kPathViewControllerKey] &&
       [[UserDefaults objectForKey:kPathViewControllerKey] isKindOfClass:[NSString class]])
    {
        return true;
    }
    
    return false;
}

- (BOOL)handleShortCutItem : (UIApplicationShortcutItem *)shortcutItem
{
    __block BOOL handled = NO;
    
    NSString *key = shortcutItem.type;
    
    void (^selectedCase)() = @{
                               @"Fast" :                ^{ handled = [self createPathViewControllerWithHandler:@"Fast"];            },
                               @"Catalog" :             ^{ handled = [self createPathViewControllerWithHandler:@"Catalog"];         },
                               @"Salons" :              ^{ handled = [self createPathViewControllerWithHandler:@"Salons"];          },
                               @"Basket" :              ^{ handled = [self createPathViewControllerWithHandler:@"Basket"];          },
                               @"Status" :              ^{ handled = [self createPathViewControllerWithHandler:@"Status"];          },
                               @"Room" :                ^{ handled = [self createPathViewControllerWithHandler:@"Room"];            },
                               @"OrdersComplited" :     ^{ handled = [self createPathViewControllerWithHandler:@"OrdersComplited"]; },
                               @"OrderActive" :         ^{ handled = [self createPathViewControllerWithHandler:@"OrderActive"];     },
                               @"OrderPickup" :         ^{ handled = [self createPathViewControllerWithHandler:@"OrderPickup"];     },
                               @"SalonsWay" :           ^{ handled = [self createPathViewControllerWithHandler:@"SalonsWay"];       },
                               @"Share" :               ^{ handled = [self createPathViewControllerWithHandler:@"Share"];           },
                            }[key];
    if (selectedCase != nil)
        selectedCase();
    
    return handled;
}

- (BOOL)isAvailableShortcutByTag:(NSString *)tag
{
    if([[UserDefaults objectForKey:kPathViewControllerKey] isEqualToString:tag])
    {
        return true;
    }
    return false;
}

#pragma mark - Private
- (void)createShortcutByModel
{
    self.currentData = [NITOrder currentOrders];
    self.complitedData = [NITOrder complitedOrders];
    
    //TODO: is any self.currentData contain processStatus == Pickup?
    BOOL isCurrentDataPickup = [self pickupOrder:self.currentData];
    BOOL isCurrentDataIndex = [self activeOrder:self.currentData];
    
    //Check Shortcut here ->
    if((![USER isAutorize]) && (([NITBusketBouquet allBouquets].count + [NITBusketAttachment allAttachments].count) == 0))
        [UIApplication sharedApplication].shortcutItems = [self.shortcutItemsActions objectForKey:@"First"];
    else if((![USER isAutorize]) && (([NITBusketBouquet allBouquets].count + [NITBusketAttachment allAttachments].count) > 0))
        [UIApplication sharedApplication].shortcutItems = [self.shortcutItemsActions objectForKey:@"Second"];
    else if(([USER isAutorize]) && (self.currentData) && (!self.complitedData) && (!isCurrentDataPickup) && (!isCurrentDataIndex))
        [UIApplication sharedApplication].shortcutItems = [self.shortcutItemsActions objectForKey:@"Third"];
    else if(([USER isAutorize]) && (!self.currentData) && (!self.complitedData))
        [UIApplication sharedApplication].shortcutItems = [self.shortcutItemsActions objectForKey:@"Fourth"];
    else if(([USER isAutorize]) && (!self.currentData) && (self.complitedData) && (!isCurrentDataIndex) && (!isCurrentDataPickup))
        [UIApplication sharedApplication].shortcutItems = [self.shortcutItemsActions objectForKey:@"Fiveth"];
    else if(([USER isAutorize]) && (self.currentData) && (!isCurrentDataPickup) && (isCurrentDataIndex))
        [UIApplication sharedApplication].shortcutItems = [self.shortcutItemsActions objectForKey:@"Sixth"];
    else if(([USER isAutorize]) && (self.currentData) && (isCurrentDataPickup))
        [UIApplication sharedApplication].shortcutItems = [self.shortcutItemsActions objectForKey:@"Seventh"];
}

- (BOOL)createPathViewControllerWithHandler:(NSString *)pathVC
{
    [UserDefaults setObject:pathVC forKey:kPathViewControllerKey];
    return YES;
}

- (UIApplicationShortcutItem *)applicationShortcutItemWithType:(NSString *)type
                                                     withTitle:(NSString *)title
                                                  withSubtitle:(NSString *)subtitle
                                                      withIcon:(UIApplicationShortcutIcon *)icon
                                                  withUserInfo:(NSDictionary *)userInfo
{
    UIApplicationShortcutItem *shortcutItem = [[UIApplicationShortcutItem alloc]
                                               initWithType:type
                                               localizedTitle:title
                                               localizedSubtitle:subtitle ? subtitle : nil
                                               icon:icon
                                               userInfo:nil];
    return  shortcutItem;
}


- (BOOL)activeOrder:(NSArray *)currentData
{
    return [currentData bk_select:^BOOL(NITOrder *obj) {
        return [obj.statusId isEqualToString:@"3"];
    }].count > 0;
}

- (BOOL)pickupOrder:(NSArray *)currentData
{
    return [currentData bk_select:^BOOL(NITOrder *obj){
        return (obj.isPickup == true);
    }].count > 0;
}

@end
