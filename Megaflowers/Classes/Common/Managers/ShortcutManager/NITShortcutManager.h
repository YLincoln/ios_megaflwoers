//
//  NITShortcutManager.h
//  Megaflowers
//
//  Created by Yaroslav Zavyalov on 28.02.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITShortcutConstants.h"

#define Shortcut [NITShortcutManager sharedInstance]

@interface NITShortcutManager : NSObject

+ (id)sharedInstance;

- (void)createShortcut;
- (void)deleteShortcut;
- (BOOL)isCreatedShortcut;
- (BOOL)isAvailableShortcutByTag:(NSString *)tag;
- (BOOL)handleShortCutItem : (UIApplicationShortcutItem *)shortcutItem;

@end
