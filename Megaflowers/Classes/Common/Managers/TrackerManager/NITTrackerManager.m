//
//  NITTrackerManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITTrackerManager.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import <Google/Analytics.h>

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITTrackerManager ()

@end

@implementation NITTrackerManager

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static NITTrackerManager * shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    
}

#pragma mark - Events
- (void)trackEvent:(TrackerEvent)eventType
{
    [self trackEvent:eventType parameters:nil];
}

- (void)trackEvent:(TrackerEvent)eventType parameters:(NSDictionary *)parameters
{
    NSString * eventName = @"";
    
    switch (eventType) {
        case TrackerEventSelectCity:            eventName = @"select_city";                 break;
        case TrackerEventSelectCategory:        eventName = @"select_bouquets_category";    break;
        case TrackerEventRate:                  eventName = @"app_rate";                    break;
        case TrackerEventRateAppInStore:        eventName = @"app_rate_in_appstore";        break;
        case TrackerEventSelectFilters:         eventName = @"select_filter";               break;
        case TrackerEventSort:                  eventName = @"sort_bouquets";               break;
        case TrackerEventOpenBouquet:           eventName = @"show_bouquet";                break;
        case TrackerEventPreviewBouquet:        eventName = @"show_bouquet_by_force_touch"; break;
        case TrackerEventLikeBouquet:           eventName = @"like_bouquet";                break;
        case TrackerEventAddBouquetToBasket:    eventName = @"add_bouquet_to_basket";       break;
        case TrackerEventOpenAttach:            eventName = @"show_attach";                 break;
        case TrackerEventAddAttachToBasket:     eventName = @"add_attach_to_basket";        break;
        case TrackerEventBouquetEdite:          eventName = @"edite_bouquet";               break;
        case TrackerEventOrderCompleting:       eventName = @"create_order";                break;
        case TrackerEventSelectPaymentType:     eventName = @"select_payment_type";         break;
        case TrackerEventOrderPayment:          eventName = @"order_payment";               break;
        case TrackerEventOrderByPhone:          eventName = @"order_by_phone";              break;
        case TrackerEventRequestCall:           eventName = @"request_call";                break;
        case TrackerEventRegister:              eventName = @"register_user";               break;
        case TrackerEventSocialAuthorization:   eventName = @"social_authorization";        break;
        case TrackerEventLogin:                 eventName = @"login_user";                  break;
        case TrackerEventPickupSalon:           eventName = @"select_salon_for_pickup";     break;
        case TrackerEventLikeSalon:             eventName = @"like_salon";                  break;
        case TrackerEventCouponeUse:            eventName = @"use_coupone";                 break;
        case TrackerEventPushSettingOff:        eventName = @"push_setting_off";            break;
        case TrackerEventDiscontCardLinked:     eventName = @"linked_discont_card";         break;
        case TrackerEventDiscontCardScan:       eventName = @"scan_discont_card";           break;
        case TrackerEventDiscontCardCreate:     eventName = @"create_discont_card";         break;
        case TrackerEventSendFeedback:          eventName = @"send_feedback";               break;
        case TrackerEventCalendarEventCreate:   eventName = @"calendar_event_create";       break;
    }
    
    [self sendEvent:eventName parameters:parameters];
    
    [HELPER logString:[NSString stringWithFormat:@"Send event: %@ parameters: %@", eventName, parameters ? : @""]];
}

- (void)sendEvent:(NSString *)event parameters:(NSDictionary *)parameters
{
    [self sendYandexMetricaEvent:event parameters:parameters];
    [self sendGoogleAnalyticsEvent:event parameters:parameters];
}

#pragma mark - YandexMetrica
/**
 *  run in Appdelegate initialize
 */
- (void)initYandexMetrica
{
    [YMMYandexMetrica activateWithApiKey:@"5f19ef94-eea0-461f-9401-411a435a521a"];
    [YMMYandexMetrica setReportCrashesEnabled:false];
    [YMMYandexMetrica setTrackLocationEnabled:false];
}

- (void)sendYandexMetricaEvent:(NSString *)event parameters:(NSDictionary *)parameters
{
    [YMMYandexMetrica reportEvent:event
                       parameters:parameters
                        onFailure:^(NSError *error) {
                            DDLogError(@"report event error: %@", [error localizedDescription]);
                        }];
}

#pragma mark - Google Analytics
- (void)initGoogleAnalytics
{
    NSError * configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    
    #if DEBUG
        NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    #endif
    
    // Optional: configure GAI options.
    GAI * gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = false;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelWarning;  // remove before app release
}

- (void)sendGoogleAnalyticsEvent:(NSString *)event parameters:(NSDictionary *)parameters
{
    // May return nil if a tracker has not already been initialized with a property
    // ID.
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    GAIDictionaryBuilder * builder = [GAIDictionaryBuilder createEventWithCategory:@"action"
                                                                            action:event
                                                                             label:@"event"
                                                                             value:nil];
    if (parameters)
    {
        [builder setAll:parameters];
    }
    
    [tracker send:[builder build]];
}

@end
