//
//  NITTrackerManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TRACKER [NITTrackerManager sharedInstance]

typedef CF_ENUM (NSUInteger, TrackerEvent) {
    TrackerEventSelectCity          = 0,    // Клик на город (выбор города по умолчанию): Id города
    TrackerEventSelectCategory      = 1,    // Выбор категории каталога: Id категории
    TrackerEventRate                = 2,    // Клик "оценить": Количество звездочек
    TrackerEventRateAppInStore      = 3,    // Клик "перейти в апстор" (для оценки)
    TrackerEventSelectFilters       = 4,    // Количество кликов в фильтры (выбор фильтра, определяем популярные фильтры): Id фильтров
    TrackerEventSort                = 5,    // Количество кликов по сортировке (определяем популярную сортировку): Название поля
    TrackerEventOpenBouquet         = 6,    // Клик по букету (переход в карточку): Id товара
    TrackerEventPreviewBouquet      = 7,    // 3D Touch клик по букету: Id товара
    TrackerEventLikeBouquet         = 8,    // Добавление товара в избранное: Id товара
    TrackerEventAddBouquetToBasket  = 9,    // Добавлений товара в корзину: Id товара
    TrackerEventOpenAttach          = 10,   // Кликов по доп.товару: Id товара
    TrackerEventAddAttachToBasket   = 11,   // Добавлений доп.товара в корзину: Id товара
    TrackerEventBouquetEdite        = 12,   // Редактирование стандартного состава букетов: Id товара, фиксируется только при первом нажатии на + или -
    TrackerEventOrderCompleting     = 13,   // Завершение оформления заказа: Тип доставки, Сумма
    TrackerEventSelectPaymentType   = 14,   // Выбор способа оплаты (определяем популярный): Тип оплаты
    TrackerEventOrderPayment        = 15,   // Оплата заказа: Сумма, Тип оплаты
    TrackerEventOrderByPhone        = 16,   // Клик по кнопке заказать по телефону
    TrackerEventRequestCall         = 17,   // Клик по кнопке заказать звонок
    TrackerEventRegister            = 18,   // Создание аккаунта через моб.приложение
    TrackerEventSocialAuthorization = 19,   // Вход через соц сети (считаем авторизованных через соц сети пользователей): Тип соцсети
    TrackerEventLogin               = 20,   // Количество пользователей использующих ранее созданный аккаунт megaflowers: Это просто авторизация
    TrackerEventPickupSalon         = 21,   // Кликов по адресу магазина (выбор салона - самовывоз, определяем популярные): Id магазина. Происходит при выборе салона в заказе
    TrackerEventLikeSalon           = 22,   // Добавление магазина в избранное: Id магазина
    TrackerEventCouponeUse          = 23,   // Клик по купону (применение купона): Id купона. Применение купона при оплате
    TrackerEventPushSettingOff      = 24,   // Пользователи с включенными пушами (по каждому виду пушей). Событие на отключение пуша: Тип пуша
    TrackerEventDiscontCardLinked   = 25,   // Привязка дисконтной карты (кнопка привязать карту)
    TrackerEventDiscontCardScan     = 26,   // Сканирование карты
    TrackerEventDiscontCardCreate   = 27,   // Создание дисконтной карты (кнопка завести карту)
    TrackerEventSendFeedback        = 28,   // Отзыв от пользователя (напишите нам)
    TrackerEventCalendarEventCreate = 29    // Создание событий в календаре
};

@interface NITTrackerManager : NSObject

+ (id)sharedInstance;

#pragma mark - Events
- (void)trackEvent:(TrackerEvent)eventType;
- (void)trackEvent:(TrackerEvent)eventType parameters:(NSDictionary *)parameters;

#pragma mark - YandexMetrica
- (void)initYandexMetrica;

#pragma mark - Google Analytics
- (void)initGoogleAnalytics;

@end
