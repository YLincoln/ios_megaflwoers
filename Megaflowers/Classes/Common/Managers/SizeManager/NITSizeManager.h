//
//  NITSizeManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/26/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SIZE_MANAGER [NITSizeManager sharedInstance]

@interface NITSizeManager : NSObject

+ (id)sharedInstance;

- (CGFloat)currentScale;
- (void)scaleConstraints:(NSArray *)constraints;

@end
