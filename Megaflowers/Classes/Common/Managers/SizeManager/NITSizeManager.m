//
//  NITSizeManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/26/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSizeManager.h"

@implementation NITSizeManager

+ (id)sharedInstance
{
    static dispatch_once_t	once;
    static NITSizeManager   *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void) setup
{
    
}

#pragma mark - Public
- (CGFloat)currentScale
{
    if      (IS_IPHONE_6_PLUS)  return 1.0;
    else if (IS_IPHONE_6)       return 0.87;
    else if (IS_IPHONE_4)       return 0.67;
    else                        return 0.77;
}

- (void)scaleConstraints:(NSArray *)constraints
{
    for (NSLayoutConstraint * cons in constraints)
    {
        cons.constant = cons.constant * self.currentScale;
    }
}

@end
