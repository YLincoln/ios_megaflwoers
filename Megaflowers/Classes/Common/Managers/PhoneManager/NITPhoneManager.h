//
//  NITPhoneManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/3/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PHONE [NITPhoneManager sharedInstance]

@interface NITPhoneManager : NSObject

@property (nonatomic) NSArray * phoneCountryCodes;

+ (NITPhoneManager *)sharedInstance;

- (void)callToPhoneNumber:(NSString *)phoneNumber;
- (BOOL)isValidPhoneNumber:(NSString *)phoneNumber;
- (NSString *)imagePathForCountryCode:(NSString *)code;

@end
