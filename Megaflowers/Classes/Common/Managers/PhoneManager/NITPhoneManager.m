//
//  NITPhoneManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/3/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPhoneManager.h"
#import "UIImage+Saving.h"
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"

@interface NITPhoneManager ()

@property (nonatomic) NSDictionary * countryFlags;

@end

@implementation NITPhoneManager

+ (NITPhoneManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITPhoneManager * shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id) init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void) setup
{
    
}

#pragma mark - Custom accessors
- (NSArray *)phoneCountryCodes
{
    if (!_phoneCountryCodes)
    {
        _phoneCountryCodes = [self getPhoneCountryCodes];
    }
    
    return _phoneCountryCodes;
}

- (NSDictionary *)countryFlags
{
    if (!_countryFlags)
    {
        _countryFlags = [self getCountryFlags];
    }
    
    return _countryFlags;
}

#pragma mark - Public
- (void)callToPhoneNumber:(NSString *)phoneNumber
{
    NSURL * phoneUrl = [self phoneUrlFromText:phoneNumber];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (BOOL)isValidPhoneNumber:(NSString *)phoneNumber
{
    if (phoneNumber.length == 0)
    {
        return false;
    }
    
    NSError * anError = nil;
    NBPhoneNumberUtil * phoneUtil = [NBPhoneNumberUtil new];
    NBPhoneNumber * myNumber = [phoneUtil parse:phoneNumber defaultRegion:@"RU" error:&anError];
    
    return [phoneUtil isValidNumber:myNumber];
}

- (NSString *)imagePathForCountryCode:(NSString *)code
{
    NSString * tempPath = NSTemporaryDirectory();
    NSString * tempFile = [tempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", [@"flags_" stringByAppendingString:code], @"png"]];
    NSURL * url = [NSURL fileURLWithPath:tempFile];
    
    UIImage * result = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    if (!result)
    {
        NSNumber * y = self.countryFlags[code] ? : @0;
        
        CGImageRef cgImage = CGImageCreateWithImageInRect([[UIImage imageWithContentsOfFile:[[self codeBundle] pathForResource:@"flags" ofType:@"png"]] CGImage], CGRectMake(0, y.integerValue * 2, 32, 32));
        result = [UIImage imageWithCGImage:cgImage scale:2.0 orientation:UIImageOrientationUp];
        CGImageRelease(cgImage);
        
        [result saveToURL:url];
    }

    return tempFile;
}

#pragma mark - Private
- (NSURL *)phoneUrlFromText:(NSString *)text
{
    NSString * phoneNumber = [[text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
    NSURL * phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", phoneNumber]];
    
    return phoneUrl;
}

- (NSBundle *)codeBundle
{
    return [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Phone-Country-Code-and-Flags" ofType:@"bundle"]];
}

- (NSArray *)getPhoneCountryCodes
{
    NSData * file = [[[NSString alloc] initWithContentsOfFile:[[self codeBundle] pathForResource:@"phone_country_code" ofType:@"json"]
                                                     encoding:NSUTF8StringEncoding
                                                        error:NULL]
                     dataUsingEncoding:NSUTF8StringEncoding];
    
    return [NSJSONSerialization JSONObjectWithData:file
                                           options:kNilOptions
                                             error:nil];
}

- (NSDictionary *)getCountryFlags
{
    NSData * file = [[[NSString alloc] initWithContentsOfFile:[[self codeBundle] pathForResource:@"flag_indices" ofType:@"json"]
                                                     encoding:NSUTF8StringEncoding
                                                        error:NULL]
                     dataUsingEncoding:NSUTF8StringEncoding];
    
    return [NSJSONSerialization JSONObjectWithData:file
                                           options:kNilOptions
                                             error:nil];
}

@end
