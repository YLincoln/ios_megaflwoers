//
//  NITFilterManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterManager.h"

@interface NITFilterManager ()

@property (nonatomic) NSMutableDictionary * filters;
@property (nonatomic) NSDictionary * priceFilters;

@end

@implementation NITFilterManager

+ (id)sharedInstance
{
    static dispatch_once_t	once;
    static NITFilterManager *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void) setup
{
    
}

- (NSMutableDictionary *)filters
{
    if (!_filters)
    {
        _filters = [NSMutableDictionary new];
    }
    
    return _filters;
}

- (NSDictionary *)priceFilters
{
    if (!_priceFilters)
    {
        _priceFilters = [NSDictionary new];
    }
    
    return _priceFilters;
}

#pragma mark - Public
- (void)setFilterID:(NSNumber *)filterID withName:(NSString *)name
{
    [self.filters setObject:name forKey:filterID];
    [TRACKER trackEvent:TrackerEventSelectFilters parameters:@{@"id":filterID}];
}

- (void)removeFilterByID:(NSNumber *)filterID
{
    if (filterID.integerValue == kPriceFilterKey)
    {
        _priceFilters = nil;
    }
    else
    {
        [self.filters removeObjectForKey:filterID];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeFiltersNotification object:nil];
}

- (void)setPriceFilter:(PriceValue)priceFilter
{
    self.priceFilters = @{
                          @"min" : @(priceFilter.min),
                          @"max" : @(priceFilter.max)
                          };
    [TRACKER trackEvent:TrackerEventSelectFilters parameters:self.priceFilters];
}

- (void)resetFilters
{
    _filters = nil;
    _priceFilters = nil;
}

- (BOOL)isActiveFilterID:(NSNumber *)filterID
{
    if ([self.filters objectForKey:filterID])
    {
        return true;
    }
    
    return false;
}

- (NSString *)activeFilters
{
    if ([self.filters allKeys].count == 0)
    {
        return nil;
    }
    else
    {
        return [[self.filters allKeys] componentsJoinedByString:@","];
    }
}

- (NSString *)activePriceFilters
{
    if ([self.priceFilters allKeys].count == 0)
    {
        return nil;
    }
    else
    {
        return [@[self.priceFilters[@"min"], self.priceFilters[@"max"]] componentsJoinedByString:@","];
    }
}

- (PriceValue)activePriceValues
{
    if (self.activePriceFilters)
    {
        return PriceValueMake([self.priceFilters[@"min"] integerValue], [self.priceFilters[@"max"] integerValue]);
    }
    
    return PriceValueMake(0, 0);
}

- (NSString *)priceStringFromFilterValue:(PriceValue)value
{
    return [NSString stringWithFormat:@"%@ %ld %@ %ld %@",
            NSLocalizedString(@"от", nil),
            (long)value.min,
            NSLocalizedString(@"до", nil),
            (long)value.max,
            NSLocalizedString(@"руб", nil)];
}

- (NSDictionary *)allActiveFilters
{
    NSMutableDictionary * items = [NSMutableDictionary dictionaryWithDictionary:self.filters];
    if (self.activePriceFilters)
    {
        [items setObject:[self priceStringFromFilterValue:self.activePriceValues] forKey:@(kPriceFilterKey)];
    }
   
    return items;
}

@end
