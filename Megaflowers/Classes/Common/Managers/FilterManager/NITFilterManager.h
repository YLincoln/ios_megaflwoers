//
//  NITFilterManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterConstants.h"

#define FILTERS [NITFilterManager sharedInstance]

@interface NITFilterManager : NSObject

+ (id)sharedInstance;

- (void)setFilterID:(NSNumber *)filterID withName:(NSString *)name;
- (void)removeFilterByID:(NSNumber *)filterID;
- (void)setPriceFilter:(PriceValue)priceFilter;
- (void)resetFilters;
- (BOOL)isActiveFilterID:(NSNumber *)filterID;
- (NSDictionary *)allActiveFilters;
- (NSString *)activeFilters;
- (NSString *)activePriceFilters;
- (PriceValue)activePriceValues;
- (NSString *)priceStringFromFilterValue:(PriceValue)value;

@end
