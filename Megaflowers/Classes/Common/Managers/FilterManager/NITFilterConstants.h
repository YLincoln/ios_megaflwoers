//
//  NITFilterConstants.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#pragma mark - Keys
static NSString * kChangeFiltersNotification = @"change_filters_notification";

#pragma mark - Const
static NSInteger const kPriceFilterKey = -1;

#pragma mark - Structure
typedef CF_ENUM (NSUInteger, FilterType) {
    FilterTypeRadio     = 0,
    FilterTypeCheckbox  = 1,
    FilterTypeBoolean   = 2,
    FilterTypeRange     = 3,
    FilterTypeColor     = 4
};

struct PriceValue {
    NSInteger min;
    NSInteger max;
};

typedef struct PriceValue PriceValue;

CG_INLINE PriceValue PriceValueMake(NSInteger min, NSInteger max) {
    struct PriceValue structure;
    structure.min = min;
    structure.max = max;
    return structure;
}
