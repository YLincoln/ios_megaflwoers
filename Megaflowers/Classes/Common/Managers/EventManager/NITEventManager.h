//
//  NITEventManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class EKCalendar, EKEvent, EKEventStore;

#define EVENT_MANAGER [NITEventManager sharedInstance]

@interface NITEventManager : NSObject

@property (nonatomic, readonly) EKEventStore * eventStore;
@property BOOL convertDatesToGMT; // Default true.

+ (id)sharedInstance;

- (void)createEventWithTitle:(NSString *)title startDate:(NSDate *)startDate duration:(NSInteger)duration completion:(void (^)(NSString *eventIdentifier, NSError *error))completion;

- (void)updateEvent:(NSString *)eventIdentifier withTitle:(NSString *)title startDate:(NSDate *)startDate duration:(NSInteger)duration completion:(void (^)(NSString *eventIdentifier, NSError *error))completion;

- (void)updateEvent:(NSString *)eventIdentifier withTitle:(NSString *)title startDate:(NSDate *)startDate endDate:(NSDate *)endDate completion:(void (^)(NSString *eventIdentifier, NSError *error))completion;

- (void)deleteEventWithIdentifier:(NSString *)identifier completion:(void (^)(NSError *error))completion;

@end

@interface NITEventManager (Access)

- (void)isEventInCalendar:(NSString *)eventIdentifier completion:(void (^)(BOOL found))completion;

- (void)eventsInCalendarsBetweenStartDate:(NSDate *)startDate
                               andEndDate:(NSDate *)endDate
                                   filter:(BOOL (^)(EKEvent * event))filter
                             completition:(void (^)(NSArray * events))completion;

- (void)nearBirthdayEventsInCalendarsWithFilter:(BOOL (^)(EKEvent * event))filter completition:(void (^)(NSArray * events))completion;

@end

@interface NITEventManager (Realm)

- (void)syncSystemEventsWithHandler:(void(^)())handler;

@end

@interface NITEventManager (Swagger)

- (void)postBirthdayEvents;

@end
