//
//  NITHelperManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITHelperConstants.h"

#define HELPER [NITHelperManager sharedInstance]

@class NITActionButton;

@interface NITHelperManager : NSObject

@property (nonatomic) BOOL interfaceLoaded;

+ (NITHelperManager *)sharedInstance;

#pragma mark - Logs
- (void)logError:(NSError *)error method:(NSString *)method;
- (void)logString:(NSString *)string;

#pragma mark - Cache
- (NSURL *)saveImage:(UIImage *)image byAssetUrl:(NSURL *)assetUrl;
- (NSString *)saveImage:(UIImage *)image forKey:(NSString *)key;

#pragma mark - App state
- (BOOL)isFirstLanch;
- (void)setFirstLanch;

#pragma mark - 3D touch
- (BOOL)forceTouchAvailable;

#pragma mark - Language
- (NSNumber *)lang;

#pragma mark - UUID
- (NSString *)vendorUDID;
- (NSString *)vendorFormatedUDID;
- (NSString *)generateUUID;

#pragma mark - Loading
- (void) startLoading;
- (void) stopLoading;

#pragma mark - ActionSheet
- (void)showActionSheetFromView:(id)view
                      withTitle:(NSString *)title
                  actionButtons:(NSArray <NITActionButton *> *)actionButtons
                   cancelButton:(NITActionButton *)cancelButton;

- (void)showRateAppActionSheetWithTitle:(NSString *)title
                           successBlock:(void(^)(CGFloat rateValue))successBlock
                            cancelBlock:(void(^)())cancelBlock;

#pragma mark - Share Application
- (void)shareApplication;

@end
