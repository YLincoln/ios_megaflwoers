//
//  NITHelperConstants.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#pragma mark - API Keys
static NSString * const kGoogleAPIKey       = @"AIzaSyDGj3k1jAtoHERMV8ZnUjcyAQkwKYXEwdo";
static NSString * const kApplePayMerchantID = @"merchant.com.megaflowers.ApplePay";

#pragma mark - Notifications
static NSString * const kOpenbasketNotification     = @"open_basket_preview_notification";
static NSString * const kSetRootBasketNotification  = @"root_basket_navigation_preview_notification";
static NSString * const kChangeCityNotification     = @"change_city_notification";
static NSString * const kUpdateSearchesResults      = @"update_searches_result";
static NSString * const kNewPushNotification        = @"new_push_notification";

#pragma mark - Keys
static NSString * const kFirstLanchKey      = @"first_lanch_key";
static NSString * const kAuthorizationKey   = @"Authorization";

#pragma mark - Values
static NSString * const kShareAppURL    = @"https://megaflowers.ru/";
static NSString * kHotlinePhone         = @"8005554681";


#pragma mark - Test
static NSString * kTestAccessToken      = @"fdlX907lSOKJx5ajD-COuxzEGgdlPorz";
/*
static NSString * kTestBouquetUrl       = @"https://cdn.megaflowers.ru/pub/bouquet/trio_b.jpg";
static NSString * kTestBouquetVideoUrl  = @"https://youtu.be/K-x_cvdspRY";
static NSString * kTestAttachUrl        = @"http://slavjanka.ru/images/catalog/cats/1416551887.jpg";
static NSString * kTestBannerUrl        = @"http://new-flowers.ru/upload/thumbs/828-620/5e/8d/5e8daca0c975b96176c03aae9a11b869.jpg";
static NSString * kTest360              = @"https://megaflowers.ru/pub/bouquet/360/047tt.jpg";
static NSString * kTestBouquetDetail    = @"Орхидеи непостижимым образом превращают любой букет в сказочное чудо. Очертания их лепестков делают композицию одновременно яркой и легкой, захватывающей и трогательной. Бледные экзотические красавицы уравновешивают напряженность красного цвета альстромерий и декоративных украшений. Получив от вас такой подарок, женщина и сама словно расцветет.";
*/
