//
//  NITHelperManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITHelperManager.h"
#import "MSAlertController.h"
#import "NITAuthorizationWireFrame.h"
#import "DGActivityIndicatorView.h"
#import "UIImage+Saving.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITHelperManager ()
{
    /*MBProgressHUD * hud;*/
    DGActivityIndicatorView * hud;
}

@end

@implementation NITHelperManager

+ (NITHelperManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITHelperManager *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showResetSessionAlert) name:kResetSessionNotification object:nil];
}

#pragma mark - Logs
- (void)logError:(NSError *)error method:(NSString *)method
{
    if (error)
    {
        DDLogError(@"\n❗️Error --> %@ --> %@ (%@)\n", method, error.swaggerDescription, @(error.swaggerCode));
    }
}

- (void)logString:(NSString *)string
{
    DDLogError(@"\n❇️ %@\n", string);
}

#pragma mark - Cache
- (NSURL *)saveImage:(UIImage *)image byAssetUrl:(NSURL *)assetUrl
{
    NSString * tempPath = NSTemporaryDirectory();
    NSString * tempFile = [tempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", [HELPER generateUUID], [assetUrl pathExtension] ? : @"JPG"]];
    NSURL * url = [NSURL fileURLWithPath:tempFile];
    
    [image saveToURL:url];
    
    return url;
}

- (NSString *)saveImage:(UIImage *)image forKey:(NSString *)key
{
    NSString * tempPath = NSTemporaryDirectory();
    NSString * tempFile = [tempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", key, @"JPG"]];
    NSURL * url = [NSURL fileURLWithPath:tempFile];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [image saveToURL:url];
    });
    
    return url.absoluteString;
}

#pragma mark - App state
- (BOOL)isFirstLanch
{
    return ![UserDefaults boolForKey:kFirstLanchKey];
}

- (void)setFirstLanch
{
    [UserDefaults setBool:true forKey:kFirstLanchKey];
    [UserDefaults synchronize];
}

#pragma mark - Language
- (NSNumber *)lang
{
    #warning return system lang
    NSString * language = @"ru"; //[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
    if ([language isEqualToString:@"ru"])
    {
        return @(1);
    }
    
    return @(2);
}

#pragma mark - 3D touch
- (BOOL)forceTouchAvailable
{
    if ([[WINDOW visibleController] respondsToSelector:@selector(traitCollection)] &&
        [[WINDOW visibleController].traitCollection respondsToSelector:@selector(forceTouchCapability)] &&
        ([WINDOW visibleController].traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable))
    {
        return true;
    }
    
        return false;
}

#pragma mark - UUID
- (NSString *)vendorUDID
{
    return [[UIDevice currentDevice] identifierForVendor].UUIDString;
}

- (NSString *)vendorFormatedUDID
{
    NSString * notFormatedUDID = [self vendorUDID];
    NSString * formatedUDID = [notFormatedUDID stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    return formatedUDID;
}

- (NSString *)generateUUID
{
    return [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

#pragma mark - Loading
- (void)startLoading
{
    if (!hud && self.interfaceLoaded)
    {
        hud = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotateMultiple tintColor:RGB(10, 88, 43) size:50.0f];
        hud.center = WINDOW.center;
        [WINDOW addSubview:hud];
        [hud startAnimating];
    }

    ShowNetworkActivityIndicator();
}

- (void)stopLoading
{
    /*[hud hideAnimated:true];*/
    
    if (hud)
    {
        [hud stopAnimating];
        hud = nil;
    }
    
    HideNetworkActivityIndicator();
}

#pragma mark - ActionSheet
- (void)showActionSheetFromView:(UIViewController *)view
                      withTitle:(NSString *)title
                  actionButtons:(NSArray <NITActionButton *> *)actionButtons
                   cancelButton:(NITActionButton *)cancelButton
{
    MSAlertController * actionSheetController = [MSAlertController alertControllerWithTitle:nil
                                                                                    message:title
                                                                             preferredStyle:MSAlertControllerStyleActionSheet];

    actionSheetController.enabledBlurEffect = true;
    
    if (cancelButton)
    {
        MSAlertAction * cancelAction = [MSAlertAction actionWithTitle:cancelButton.title
                                                                style:MSAlertActionStyleCancel
                                                              handler:^(MSAlertAction * action) {
                                                                  cancelButton.actionBlock();
                                                              }];
        cancelAction.titleColor = RGBA(226, 150, 191, 1);
        [actionSheetController addAction:cancelAction];
    }
    
    for (NITActionButton * actionButton in actionButtons)
    {
        MSAlertAction * action = [MSAlertAction actionWithTitle:actionButton.title
                                                          style:MSAlertActionStyleDefault
                                                        handler:^(MSAlertAction * action) {
                                                            actionButton.actionBlock();
                                                        }];
        
        action.titleColor = RGBA(10, 88, 43, 1);
        [actionSheetController addAction:action];
    }
    
    [view presentViewController:actionSheetController animated:true completion:nil];
}

- (void)showRateAppActionSheetWithTitle:(NSString *)title
                           successBlock:(void(^)(CGFloat rateValue))successBlock
                            cancelBlock:(void(^)())cancelBlock
{
    NSParameterAssert(successBlock);
    NSParameterAssert(cancelBlock);
    
    title = [title stringByAppendingString:@"\n\n\n"];
    
    __block MSAlertController * actionSheetController = [MSAlertController alertControllerWithTitle:nil
                                                                                    message:title
                                                                             preferredStyle:MSAlertControllerStyleActionSheet];
    
    actionSheetController.enabledBlurEffect = true;
    
    __block HCSStarRatingView * starRatingView = [HCSStarRatingView new];
    starRatingView.maximumValue = 5;
    starRatingView.minimumValue = 0;
    starRatingView.value = 0;
    starRatingView.allowsHalfStars = false;
    starRatingView.emptyStarImage = [UIImage imageNamed:@"gray_star"];
    starRatingView.filledStarImage = [UIImage imageNamed:@"gold_star"];

    MSAlertAction * cancelAction = [MSAlertAction actionWithTitle:NSLocalizedString(@"Отмена", nil)
                                                            style:MSAlertActionStyleCancel
                                                          handler:^(MSAlertAction * action) {
                                                              cancelBlock();
                                                          }];
    cancelAction.titleColor = RGBA(226, 150, 191, 1);
    [actionSheetController addAction:cancelAction];
    
    
    MSAlertAction * action = [MSAlertAction actionWithTitle:NSLocalizedString(@"Оценить", nil)
                                                      style:MSAlertActionStyleDefault
                                                    handler:^(MSAlertAction * action) {
                                                        successBlock(starRatingView.value);
                                                        [TRACKER trackEvent:TrackerEventRate parameters:@{@"value":@(starRatingView.value)}];
                                                    }];
    
    action.titleColor = RGBA(10, 88, 43, 1);
    [actionSheetController addAction:action];

    [WINDOW.visibleController presentViewController:actionSheetController animated:true completion:^{
        starRatingView.frame = CGRectMake(actionSheetController.titleLabel.center.x - 226/2,
                                          ViewHeight(actionSheetController.tableViewHeader) - 26 - 32,
                                          226,
                                          26);
        [actionSheetController.tableViewHeader addSubview:starRatingView];
    }];
}

- (void)showResetSessionAlert
{
    MSAlertController * actionSheetController = [MSAlertController alertControllerWithTitle:nil
                                                                                    message:NSLocalizedString(@"Сессия не активна!", nil)
                                                                             preferredStyle:MSAlertControllerStyleActionSheet];
    
    actionSheetController.enabledBlurEffect = true;
    MSAlertAction * cancelAction = [MSAlertAction actionWithTitle:NSLocalizedString(@"Отмена", nil)
                                                            style:MSAlertActionStyleCancel
                                                          handler:^(MSAlertAction * action) {
                                                              [WINDOW.visibleController.navigationController popToRootViewControllerAnimated:true];
                                                          }];
    cancelAction.titleColor = RGBA(226, 150, 191, 1);
    [actionSheetController addAction:cancelAction];
    
    MSAlertAction * action = [MSAlertAction actionWithTitle:NSLocalizedString(@"Авторизация", nil)
                                                      style:MSAlertActionStyleDefault
                                                    handler:^(MSAlertAction * action) {
                                                        [WINDOW.visibleController.navigationController popToRootViewControllerAnimated:true];
                                                        [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:WINDOW.visibleController];
                                                    }];
    
    action.titleColor = RGBA(10, 88, 43, 1);
    [actionSheetController addAction:action];

    [WINDOW.visibleController presentViewController:actionSheetController animated:true completion:nil];
}

#pragma mark - Share application
- (void)shareApplication
{
    NSString * shareString = NSLocalizedString(@"Рекомендую приложение Megaflowers для быстрого заказа букетов по всему миру!! \n", nil);
    NSURL * shareURL = [NSURL URLWithString:kShareAppURL];
    NSArray * shareArray = @[shareString, shareURL];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:shareArray
                                                                                         applicationActivities:nil];
    
    NSArray * activityCategoryAction = @[UIActivityTypePrint,
                                              UIActivityTypeCopyToPasteboard,
                                              UIActivityTypeAssignToContact,
                                              UIActivityTypeSaveToCameraRoll,
                                              UIActivityTypeAddToReadingList,
                                              UIActivityTypeAirDrop];
    
    NSArray * activitycategoryShare = @[UIActivityTypeMessage,
                                             UIActivityTypeMail,
                                             UIActivityTypePostToFacebook,
                                             UIActivityTypePostToTwitter,
                                             UIActivityTypePostToFlickr,
                                             UIActivityTypePostToVimeo,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypePostToWeibo];
    
    NSArray * fullActivitiesArray = [[NSArray alloc] initWithObjects:activityCategoryAction, activitycategoryShare, nil];
    
    activityViewController.excludedActivityTypes = fullActivitiesArray;
    
    [WINDOW.visibleController presentViewController:activityViewController animated:YES completion:^{
        //With nil or adding something
    }];
}

@end
