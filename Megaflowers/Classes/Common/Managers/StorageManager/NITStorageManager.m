//
//  NITStorageManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITStorageManager.h"
#import "FBEncryptorAES.h"
#import "UICKeyChainStore.h"

@implementation NITStorageManager

+ (NITStorageManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITStorageManager *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}


- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    
}

#pragma mark - Public
- (void)setObject:(nonnull NSString *)key value:(nullable id)value
{
    if (value == nil)
    {
        [UserDefaults removeObjectForKey:key];
    }
    else
    {
        [UserDefaults setObject:value forKey:key];
    }
    
    [UserDefaults synchronize];
}

- (id)Object:(nonnull NSString *)key
{
    return [UserDefaults objectForKey:key];
}

- (void)setData:(NSString *)key value:(NSData *)value
{
    [self setObject:key value:value];
}

- (id)Data:(NSString *)key
{
    return [UserDefaults dataForKey:key];
}

- (void)setInteger:(nonnull NSString *)key value:(NSInteger)value
{
    [UserDefaults setInteger:value forKey:key];
    [UserDefaults synchronize];
}

- (NSInteger)Integer:(nonnull NSString *)key
{
    return [UserDefaults integerForKey:key];
}

- (void)setBool:(nonnull NSString *)key value:(BOOL)value
{
    [UserDefaults setBool:value forKey:key];
    [UserDefaults synchronize];
}

- (BOOL)Bool:(nonnull NSString *)key
{
    return [UserDefaults boolForKey:key];
}

- (void)setString:(nonnull NSString *)key value:(NSString *)value
{
    [self setObject:key value:value];
}

- (NSString *)String:(nonnull NSString *)key
{
    return (NSString *)[self Object:key];
}

- (NSString *)CriptoString:(nonnull NSString *)key
{
    NSString * string = [self String:key];
    
    if ([string isKindOfClass:[NSString class]])
    {
        return [FBEncryptorAES decryptBase64String:string keyString:[self keyForAES]];
    }
    else
    {
        return nil;
    }
}

- (void)setCriptoString:(nonnull NSString *)key value:(NSString *)value
{
    NSString * string;
    
    if ([value isKindOfClass:[NSString class]])
    {
        string = [FBEncryptorAES encryptBase64String:value keyString:[self keyForAES] separateLines:false];
    }
    
    [self setString:key value:string];
}

- (void)setArray:(nonnull NSString *)key value:(NSArray *)value
{
    [self setObject:key value:value];
}

- (NSArray *)Array:(nonnull NSString *)key
{
    return [UserDefaults arrayForKey:key];
}

- (void)setDictionary:(NSString *)key value:(NSDictionary *)value
{
    [self setObject:key value:value];
}

- (NSDictionary *)Dictionary:(NSString *)key
{
    return [UserDefaults dictionaryForKey:key];
}

#pragma mark - Private
- (NSString *)keyForAES
{
    NSString * keyChainKey = @"megaflowers_nit";
    NSString * key = [UICKeyChainStore stringForKey:keyChainKey];

    if ([key length] == 0)
    {
        key = [HELPER vendorFormatedUDID];
        [UICKeyChainStore setString:key forKey:keyChainKey];
    }
    
    return key;
}



@end
