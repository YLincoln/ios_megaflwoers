//
//  NITStorageManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@interface NITStorageManager : NSObject

+ (nullable NITStorageManager *)sharedInstance;

- (nullable id)Object:(nonnull NSString *)key;
- (void)setObject:(nonnull NSString *)key value:(nullable id)value;

- (nullable id)Data:(nonnull NSString *)key;
- (void)setData:(nonnull NSString *)key value:(nullable NSData *)value;

- (NSInteger)Integer:(nonnull NSString *)key;
- (void)setInteger:(nonnull NSString *)key value:(NSInteger)value;

- (BOOL)Bool:(nonnull NSString *)key;
- (void)setBool:(nonnull NSString*)key value:(BOOL)value;

- (nullable NSString*)String:(nonnull NSString*)key;
- (void)setString:(nonnull NSString*)key value:(nullable NSString*)value;

- (nullable NSString*)CriptoString:(nonnull NSString*)key;
- (void)setCriptoString:(nonnull NSString*)key value:(nullable NSString*)value;

- (nullable NSArray*)Array:(nonnull NSString*)key;
- (void)setArray:(nonnull NSString*)key value:(nullable NSArray*)value;

- (nullable NSDictionary *)Dictionary:(nonnull NSString *)key;
- (void)setDictionary:(nonnull NSString *)key value:(nullable NSDictionary *)value;

@end
