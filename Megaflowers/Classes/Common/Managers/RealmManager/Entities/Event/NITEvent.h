//
//  NITEvent.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITEvent : RLMObject

@property NSString * identifier;
@property NSString * title;
@property NSString * typeTitle;
@property NSString * formattedDate;
@property NSString * imagePath;
@property NSData * image;
@property NSInteger reminderType;
@property NSInteger repeatType;
@property NSInteger type;
@property NSDate * startDate;
@property NSDate * endDate;
@property NSDate * reminderDate;
@property BOOL local;
@property BOOL isDeleted;

@end

RLM_ARRAY_TYPE(NITEvent)

@class NITNewEventModel, SWGUserEvent;

@interface NITEvent (Accessors)

typedef CF_ENUM (NSInteger, NITEventReminderType) {
    NITEventReminderDisable = 0,
    NITEventReminderOneHour = 1,
    NITEventReminderOneDay  = 2,
    NITEventReminderOneWeek = 3
};

typedef CF_ENUM (NSInteger, NITEventRepeatType) {
    NITEventRepeatTypeDisabled      = -1,
    NITEventRepeatTypeEveryMonth    = 0,
    NITEventRepeatTypeEveryYear     = 1,
    NITEventRepeatTypeNever         = 2,
    NITEventRepeatTypeEveryDay      = 3,
    NITEventRepeatTypeEveryWeek     = 4
};

typedef CF_ENUM (NSInteger, NITEventType) {
    NITEventTypeNone        = 0,
    NITEventTypeUser        = 1,
    NITEventTypeMegaflowers = 2
};

#pragma mark - Save
+ (void)syncSystemEvents:(NSArray <EKEvent *> *)events;
+ (void)syncServerEvents:(NSArray <SWGUserEvent *> *)events;
+ (void)createOrUpdateEventByModel:(NITNewEventModel *)model;
- (void)remindByType:(NITEventReminderType)type;

#pragma mark - Delete
+ (void)deleteAllEvents;
- (void)deleteEvent;
- (void)markDeleted;

#pragma mark - Get
+ (NITEvent *)eventByID:(NSString *)identifier;
+ (NSArray <NITEvent *> *)eventsForDate:(NSDate *)date;
+ (NSArray <NITEvent *> *)searchEventsByString:(NSString *)searchString;

#pragma mark - Helpers
- (NSString *)reminderTitle;
- (UIImage *)eventImage;

@end
