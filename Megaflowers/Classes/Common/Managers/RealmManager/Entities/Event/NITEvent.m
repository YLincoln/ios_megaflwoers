//
//  NITEvent.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@import EventKit;

#import "NITEvent.h"
#import "RLMResults+NSArray.h"
#import "UIView+Screenshot.h"
#import "NITNewEventModel.h"
#import "SWGUserEvent.h"
#import "NYXImagesKit.h"
#import "APAddressBook.h"
#import "APContact.h"

static NSString * kYearRepeaatDateFormat    = @"MM dd";
static NSString * kMonthRepeaatDateFormat   = @"dd";

@implementation NITEvent

#pragma mark - Default
+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"title" : @"",
             @"typeTitle" : @"",
             @"formattedDate" : @"",
             @"reminderType" : @0,
             @"repeatType" : @2,
             @"startDate" : [NSDate date],
             @"endDate" : [NSDate date],
             @"reminderDate" : [NSDate date],
             @"local" : @false,
             @"isDeleted" : @false,
             @"imagePath" : @"",
             @"image" : [NSData data],
             @"type" : @(NITEventTypeNone)
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

#pragma mark - NITEvent+Accessors
@implementation NITEvent (Accessors)

#pragma mark - Save
+ (void)syncSystemEvents:(NSArray <EKEvent *> *)events
{
    if (events && events.count > 0)
    {
        APAddressBook * addressBook = [[APAddressBook alloc] init];
        addressBook.fieldsMask = APContactFieldDefault | APContactFieldThumbnail;
        [addressBook loadContacts:^(NSArray<APContact *> * _Nullable contacts, NSError * _Nullable error) {
            
            // filling
            NSMutableArray * eventsData = [NSMutableArray new];
            for (EKEvent * ekEvent in events)
            {
                NITEvent * event = [NITEvent createFromSystemEvent:ekEvent contacts:contacts];
                
                if (event)
                {
                    [eventsData addObject:event];
                }
            }
            
            // save
            RLMRealm * realm = REALM.database;
            [realm beginWriteTransaction];
            for (NITEvent * event in eventsData)
            {
                [NITEvent createOrUpdateInRealm:realm withValue:event];
            }
            [realm commitWriteTransaction];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kSaveEventNotification object:nil];
        }];
    }
}

+ (NITEvent *)createFromSystemEvent:(EKEvent *)ekEvent contacts:(NSArray<APContact *> *)contacts
{
    if ([NITEvent eventByID:ekEvent.eventIdentifier])
    {
        return nil;
    }
    
    NITEvent * event = [[NITEvent alloc] init];
    
    event.identifier = ekEvent.eventIdentifier;
    event.title = ekEvent.title;
    event.typeTitle =  ekEvent.calendar.type == EKCalendarTypeBirthday ? NSLocalizedString(@"День рождения", nil) : NSLocalizedString(@"Событие", nil);
    event.startDate = [ekEvent.startDate dateByMovingToBeginningOfDay];
    event.endDate = [ekEvent.endDate dateByMovingToEndOfDay];
    event.local = true;
    event.type = NITEventTypeUser;
    
    if ([ekEvent.recurrenceRules count] > 0)
    {
        EKRecurrenceRule * rule = [ekEvent.recurrenceRules firstObject];
        switch (rule.frequency) {
            case EKRecurrenceFrequencyDaily:
                event.repeatType = NITEventRepeatTypeEveryDay;
                break;
                
            case EKRecurrenceFrequencyWeekly:
                event.repeatType = NITEventRepeatTypeEveryWeek;
                event.formattedDate = @([[ekEvent.startDate GMTDate] weekday]).stringValue;
                break;
                
            case EKRecurrenceFrequencyMonthly:
                event.repeatType = NITEventRepeatTypeEveryMonth;
                event.formattedDate = [[ekEvent.startDate GMTDate] dateStringByLocalFormat:kMonthRepeaatDateFormat];
                break;
                
            case EKRecurrenceFrequencyYearly:
                event.repeatType = NITEventRepeatTypeEveryYear;
                event.formattedDate = [[ekEvent.startDate GMTDate] dateStringByLocalFormat:kYearRepeaatDateFormat];
                break;
                
            default:
                event.repeatType = NITEventRepeatTypeNever;
                break;
        }
    }
    else
    {
        event.repeatType = NITEventRepeatTypeNever;
    }
    
    if (ekEvent.calendar.type == EKCalendarTypeBirthday)
    {
        APContact * contact = [[contacts bk_select:^BOOL(APContact * obj) {
            return [obj.recordID isEqualToNumber:@(ekEvent.birthdayPersonID)];
        }] firstObject];
        
        if (contact.thumbnail)
        {
            event.imagePath = [HELPER saveImage:contact.thumbnail forKey:event.identifier];
        }
        
        if (contact.name)
        {
            event.title = contact.name.compositeName;
        }
    }
    
    return event;
}

+ (void)syncServerEvents:(NSArray <SWGUserEvent *> *)events
{
    if (events && events.count > 0)
    {
        // filling
        NSMutableArray * eventsData = [NSMutableArray new];
        for (SWGUserEvent * userEvent in events)
        {
            NITEvent * event = [NITEvent createFromServerEvent:userEvent];
            
            if (event)
            {
                [eventsData addObject:event];
            }
        }
        
        // save
        RLMRealm * realm = REALM.database;
        [realm beginWriteTransaction];
        for (NITEvent * event in eventsData)
        {
            [NITEvent createOrUpdateInRealm:realm withValue:event];
        }
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveEventNotification object:nil];
    }
}

+ (NITEvent *)createFromServerEvent:(SWGUserEvent *)userEvent
{
    NITEvent * event = [NITEvent new];
    
    event.identifier = userEvent._id.stringValue;
    event.title = userEvent.text;
    event.typeTitle =  userEvent.name;
    event.repeatType = userEvent.periodId.integerValue;
    event.startDate = [userEvent.date dateByMovingToBeginningOfDay];
    event.endDate = [userEvent.date dateByMovingToEndOfDay];
    event.local = false;
    event.imagePath = userEvent.photo;
    
    if (userEvent.typeId)
    {
        event.type = userEvent.typeId.integerValue;
    }
    
    switch (event.repeatType) {
        case NITEventRepeatTypeEveryMonth:
            event.formattedDate = [[event.startDate GMTDate] dateStringByLocalFormat:kMonthRepeaatDateFormat];
            break;
        case NITEventRepeatTypeEveryYear:
            event.formattedDate = [[event.startDate GMTDate] dateStringByLocalFormat:kYearRepeaatDateFormat];
            break;
        default:
            break;
    }
    
    return event;
}

+ (void)createOrUpdateEventByModel:(NITNewEventModel *)model
{
    NITEvent * event = [NITEvent new];
    
    event.identifier = [model.identifier length] == 0 ? [NSUUID UUID].UUIDString : model.identifier;
    event.title = model.contact;
    event.typeTitle = model.title;
    event.repeatType = model.repeat;
    event.startDate = [model.date dateByMovingToBeginningOfDay];
    event.endDate = [model.date dateByMovingToEndOfDay];
    event.type = NITEventTypeUser;
    event.local = model.local;
    
    if (model.imageUrl)
    {
        event.imagePath = model.imageUrl.absoluteString;
    }
    
    switch (model.repeat) {
        case NITEventRepeatTypeEveryMonth:
            event.formattedDate = [model.date dateStringByLocalFormat:kMonthRepeaatDateFormat];
            event.repeatType = model.repeat;
            break;
        case NITEventRepeatTypeEveryYear:
            event.formattedDate = [model.date dateStringByLocalFormat:kYearRepeaatDateFormat];
            event.repeatType = model.repeat;
            break;
        default:
            event.repeatType = NITEventRepeatTypeNever;
            break;
    }
    
    RLMRealm * realm = REALM.database;
    
    [realm beginWriteTransaction];
    [NITEvent createOrUpdateInRealm:realm withValue:event];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveEventNotification object:nil];
}

- (void)remindByType:(NITEventReminderType)type
{
    RLMRealm * realm = REALM.database;
    
    [realm beginWriteTransaction];
    self.reminderType = type;
    [NITEvent createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}

#pragma mark - Delete
+ (void)deleteAllEvents
{
    RLMResults * results = [NITEvent allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

- (void)deleteEvent
{
    RLMRealm * realm = REALM.database;
    
    [realm beginWriteTransaction];
    [realm deleteObject:self];
    [realm commitWriteTransaction];
}

- (void)markDeleted
{
    RLMRealm * realm = REALM.database;
    
    [realm beginWriteTransaction];
    self.isDeleted = true;
    [NITEvent createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}


#pragma mark - Get
+ (NITEvent *)eventByID:(NSString *)identifier
{
    NSArray * predicats = @[
                            [NSPredicate predicateWithFormat:@"identifier == %@", identifier]
                            ];
    
    return [[NITEvent objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] firstObject];
}

+ (NSArray <NITEvent *> *)eventsForDate:(NSDate *)date
{
    NSMutableArray * result = [NSMutableArray new];

    // never repeats
    NSArray * predicats = @[
                            [NSPredicate predicateWithFormat:@"%K >= %@", @"startDate", [date dateByMovingToBeginningOfDay]],
                            [NSPredicate predicateWithFormat:@"%K =< %@", @"endDate", [date dateByMovingToEndOfDay]],
                            [NSPredicate predicateWithFormat:@"repeatType != %@", @(NITEventRepeatTypeEveryMonth)],
                            [NSPredicate predicateWithFormat:@"repeatType != %@", @(NITEventRepeatTypeEveryYear)],
                            [NSPredicate predicateWithFormat:@"isDeleted == %@", @false]
                            ];
    
    [result addObjectsFromArray:[[NITEvent objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array]];
    
    // year repeats
    predicats = @[
                  [NSPredicate predicateWithFormat:@"formattedDate == %@", [[date GMTDate] dateStringByLocalFormat:kYearRepeaatDateFormat]],
                  [NSPredicate predicateWithFormat:@"repeatType == %@", @(NITEventRepeatTypeEveryYear)],
                  [NSPredicate predicateWithFormat:@"isDeleted == %@", @false]
                  ];
    
    [result addObjectsFromArray:[[NITEvent objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array]];
    
    // month repeats
    predicats = @[
                  [NSPredicate predicateWithFormat:@"formattedDate == %@", [[date GMTDate] dateStringByLocalFormat:kMonthRepeaatDateFormat]],
                  [NSPredicate predicateWithFormat:@"repeatType == %@", @(NITEventRepeatTypeEveryMonth)],
                  [NSPredicate predicateWithFormat:@"isDeleted == %@", @false]
                  ];
    
    [result addObjectsFromArray:[[NITEvent objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array]];
    
    // week repeats
    predicats = @[
                  [NSPredicate predicateWithFormat:@"formattedDate == %@", @([[date GMTDate] weekday]).stringValue],
                  [NSPredicate predicateWithFormat:@"repeatType == %@", @(NITEventRepeatTypeEveryWeek)],
                  [NSPredicate predicateWithFormat:@"isDeleted == %@", @false]
                  ];
    
    [result addObjectsFromArray:[[NITEvent objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array]];
    
    // day repeats
    predicats = @[
                  [NSPredicate predicateWithFormat:@"repeatType == %@", @(NITEventRepeatTypeEveryDay)],
                  [NSPredicate predicateWithFormat:@"isDeleted == %@", @false]
                  ];
    
    [result addObjectsFromArray:[[NITEvent objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array]];
    
    return result;
}

+ (NSArray <NITEvent *> *)searchEventsByString:(NSString *)searchString
{
    NSArray * predicats = @[
                            [NSPredicate predicateWithFormat:@"isDeleted == %@", @false]
                            ];
    
    NSArray * result = [[NITEvent objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array];
    
    NSMutableSet * data = [NSMutableSet setWithArray:[result filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(title contains[cd] %@)", searchString]]];
    [data addObjectsFromArray:[result filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(typeTitle contains[cd] %@)", searchString]]];
    
    return [data allObjects];
}

#pragma mark - Helpers
- (NSString *)reminderTitle
{
    NITEventReminderType type = self.reminderType;
    switch (type) {
        case NITEventReminderDisable:
            return NSLocalizedString(@"Не напоминать", nil);
        case NITEventReminderOneHour:
            return NSLocalizedString(@"Напомнить за час", nil);
        case NITEventReminderOneDay:
            return NSLocalizedString(@"Напомнить за день", nil);
        case NITEventReminderOneWeek:
            return NSLocalizedString(@"Напомнить за неделю", nil);
    }
}

- (UIImage *)eventImage
{
    return [UIImage imageNamed:self.type == NITEventTypeUser ? @"ava_photo" : @"ava_flowers"];;
}

@end
