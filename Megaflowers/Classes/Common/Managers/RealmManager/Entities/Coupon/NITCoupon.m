//
//  NITCoupon.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCoupon.h"
#import "NITCouponModel.h"
#import "SWGBasketCoupon.h"

@implementation NITCoupon

#pragma mark - Default
+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @0,
             @"type" : @0,
             @"value" : @0,
             @"updatedAt" : @"",
             @"dateStart" : @"",
             @"dateEnd" : @"",
             @"title" : @"",
             @"detail" : @"",
             @"code" : @""
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

#pragma mark - Save
+ (void)addBusketCoupon:(SWGBasketCoupon *)model
{
    if (model)
    {
        [NITCoupon deleteAllCoupons];

        NITCouponModel * couponModel = [[NITCouponModel alloc] initWidthBasketCoupon:model];
        NITCoupon * coupon = [[NITCoupon alloc] init];
        
        coupon.identifier = [couponModel.identifier integerValue];
        coupon.updatedAt  = couponModel.updatedAt;
        coupon.dateStart  = couponModel.dateStart;
        coupon.dateEnd    = couponModel.dateEnd;
        coupon.title      = couponModel.title;
        coupon.detail     = couponModel.detail;
        coupon.type       = [couponModel.type integerValue];
        coupon.value      = [couponModel.value integerValue];
        coupon.code       = couponModel.code;
        
        // save
        RLMRealm * realm = REALM.database;
        [realm beginWriteTransaction];
        [NITCoupon createOrUpdateInRealm:realm withValue:coupon];
        [realm commitWriteTransaction];
    }
}

+ (void)addToBusketCoupon:(NITCouponModel *)couponModel
{
    if (couponModel)
    {
        [NITCoupon deleteAllCoupons];
        
        NITCoupon * coupon = [[NITCoupon alloc] init];
        
        coupon.identifier = [couponModel.identifier integerValue];
        coupon.updatedAt  = couponModel.updatedAt;
        coupon.dateStart  = couponModel.dateStart;
        coupon.dateEnd    = couponModel.dateEnd;
        coupon.title      = couponModel.title;
        coupon.detail     = couponModel.detail;
        coupon.type       = [couponModel.type integerValue];
        coupon.value      = [couponModel.value integerValue];
        coupon.code       = couponModel.code;
        
        // save
        RLMRealm * realm = REALM.database;
        [realm beginWriteTransaction];
        [NITCoupon createOrUpdateInRealm:realm withValue:coupon];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveCouponNotification object:nil];
        [API validateBasket];
    }
}

#pragma mark - Delete
+ (void)deleteCurrentCoupon
{
    [NITCoupon deleteAllCoupons];
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveCouponNotification object:nil];
    [API validateBasket];
}

+ (void)deleteAllCoupons
{
    RLMResults * results = [NITCoupon allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

- (void)deleteCoupon
{
    RLMRealm * realm = REALM.database;
    
    [realm beginWriteTransaction];
    [realm deleteObject:self];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveCouponNotification object:nil];
    [API validateBasket];
}

#pragma mark - Get
+ (NITCoupon *)currentCoupon
{
    return [[NITCoupon allObjects] firstObject];
}

@end
