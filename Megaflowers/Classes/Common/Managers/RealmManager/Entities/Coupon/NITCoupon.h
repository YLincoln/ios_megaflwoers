//
//  NITCoupon.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@class NITCouponModel, SWGBasketCoupon;

@interface NITCoupon : RLMObject

@property NSInteger identifier;
@property NSInteger type;
@property NSInteger value;
@property NSString * updatedAt;
@property NSString * dateStart;
@property NSString * dateEnd;
@property NSString * title;
@property NSString * detail;
@property NSString * code;

+ (void)addBusketCoupon:(SWGBasketCoupon *)model;
+ (void)addToBusketCoupon:(NITCouponModel *)couponModel;
+ (void)deleteCurrentCoupon;
+ (void)deleteAllCoupons;
+ (NITCoupon *)currentCoupon;

@end

RLM_ARRAY_TYPE(NITCoupon)
