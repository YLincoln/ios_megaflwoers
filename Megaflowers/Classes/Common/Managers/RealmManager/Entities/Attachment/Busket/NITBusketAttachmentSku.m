//
//  NITBusketAttachmentSku.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBusketAttachmentSku.h"
#import "NITBasketProductModel.h"

@implementation NITBusketAttachmentSku

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"name" : @"",
             @"imagePath" : @0,
             @"width" : @0,
             @"height" : @0,
             @"price" : @0,
             @"count" : @0,
             @"components" : [[RLMArray alloc] initWithObjectClassName:_s(NITBusketAttachmentComponent)]
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

#pragma mark - NITBusketBouquetSku+Accessors
@implementation NITBusketAttachmentSku (Accessors)

#pragma mark - Create
+ (NITBusketAttachmentSku *)createBySku:(NITBusketAttachmentSku *)oldSku
{
    NITBusketAttachmentSku * sku = [NITBusketAttachmentSku new];
    sku.identifier = oldSku.identifier;
    sku.name = oldSku.name;
    sku.imagePath = oldSku.imagePath;
    sku.width = oldSku.width;
    sku.height = oldSku.height;
    sku.price = oldSku.price;
    sku.count = oldSku.count;
    
    for (NITBusketAttachmentComponent * component in oldSku.components)
    {
        [sku.components addObject:[NITBusketAttachmentComponent createByComponent:component]];
    }
    
    return sku;
}

+ (NITBusketAttachmentSku *)createByModel:(NITBasketProductModel *)model
{
    NITBusketAttachmentSku * sku = [NITBusketAttachmentSku new];
    sku.identifier = model.skuIdentifier;
    sku.name = model.skuName;
    sku.imagePath = model.skuImagePath;
    sku.width = model.skuWidth.integerValue;
    sku.height = model.skuHeight.integerValue;
    sku.price = model.skuPrice.integerValue;
    sku.count = model.skuCount.integerValue;
    
    for (NSDictionary * dictionary in model.components)
    {
        [sku.components addObject:[NITBusketAttachmentComponent createByDictionary:dictionary]];
    }
    
    return sku;
}

#pragma mark - Get
+ (NITBusketAttachmentSku *)skuByID:(NSString *)skuID
{
    return [NITBusketAttachmentSku objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", skuID]].firstObject;
}

#pragma mark - Edite
+ (void)updateCount:(NSInteger)count bySkuID:(NSString *)skuID
{
    NITBusketAttachmentSku * sku = [NITBusketAttachmentSku skuByID:skuID];
    
    if (sku)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        sku.count = count;
        [realm commitWriteTransaction];
        
        [API validateBasket];
    }
}

#pragma mark - Delete
+ (void)deleteByID:(NSString *)skuID
{
    NITBusketAttachmentSku * sku = [NITBusketAttachmentSku skuByID:skuID];
    
    if (sku)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:sku];
        [realm commitWriteTransaction];
    }
}

@end
