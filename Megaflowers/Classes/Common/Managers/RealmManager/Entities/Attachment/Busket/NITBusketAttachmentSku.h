//
//  NITBusketAttachmentSku.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITBusketAttachmentComponent.h"

@interface NITBusketAttachmentSku : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSString * imagePath;
@property NSInteger width;
@property NSInteger height;
@property NSInteger price;
@property NSInteger count;
@property RLMArray <NITBusketAttachmentComponent *> <NITBusketAttachmentComponent> * components;

@end

@class NITBasketProductModel;

RLM_ARRAY_TYPE(NITBusketAttachmentSku)

@class NITBasketProductModel;

@interface NITBusketAttachmentSku (Accessors)

#pragma mark - Create
+ (NITBusketAttachmentSku *)createBySku:(NITBusketAttachmentSku *)oldSku;
+ (NITBusketAttachmentSku *)createByModel:(NITBasketProductModel *)model;

#pragma mark - Get
+ (NITBusketAttachmentSku *)skuByID:(NSString *)skuID;

#pragma mark - Edite
+ (void)updateCount:(NSInteger)count bySkuID:(NSString *)skuID;

#pragma mark - Delete
+ (void)deleteByID:(NSString *)skuID;

@end
