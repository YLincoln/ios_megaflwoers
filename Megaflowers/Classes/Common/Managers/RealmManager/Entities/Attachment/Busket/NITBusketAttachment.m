//
//  NITBusketAttachment.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBusketAttachment.h"
#import "NITBasketProductModel.h"
#import "RLMResults+NSArray.h"
#import "RLMArray+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "SWGBasketAttach.h"

@implementation NITBusketAttachment

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"name" : @"",
             @"sku" : [[RLMArray alloc] initWithObjectClassName:_s(NITBusketAttachmentSku)]
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

#pragma mark - NITBusketBouquet+Accessors
@implementation NITBusketAttachment (Accessors)

#pragma mark - Save
+ (void)addAttachments:(NSArray<SWGBasketAttach *> *)models
{
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    
    for (SWGBasketAttach * swgModel in models)
    {
        NITBasketProductModel * model = [NITBasketProductModel modelBySWGAttaches:swgModel];
        NITBusketAttachment * attachment = [NITBusketAttachment attachmentByID:model.identifier.stringValue];
        
        if (attachment)
        {
            NITBusketAttachmentSku * sku = [attachment.sku.array bk_select:^BOOL(NITBusketAttachmentSku *obj) {
                return [obj.identifier isEqualToString:model.skuIdentifier];
            }].firstObject;
            
            if (sku)
            {
                sku.count += 1;
            }
            else
            {
                sku = [NITBusketAttachmentSku createByModel:model];
                
                [realm addOrUpdateObject:sku];
                [attachment.sku addObject:sku];
            }
        }
        else
        {
            attachment = [NITBusketAttachment new];
            attachment.identifier = model.identifier.stringValue;
            attachment.name = model.name;
            
            NITBusketAttachmentSku * sku = [NITBusketAttachmentSku createByModel:model];
            [attachment.sku addObject:sku];
        }
        
        [realm addOrUpdateObject:attachment];
    }
    
    [realm commitWriteTransaction];
}

+ (NITBusketAttachment *)addAttachment:(NITBasketProductModel *)model
{
    if (!model)
    {
        return nil;
    }
    
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    
    NITBusketAttachment * attachment = [NITBusketAttachment attachmentByID:model.identifier.stringValue];
    
    if (attachment)
    {
        NITBusketAttachmentSku * sku = [attachment.sku.array bk_select:^BOOL(NITBusketAttachmentSku *obj) {
            return [obj.identifier isEqualToString:model.skuIdentifier];
        }].firstObject;
        
        if (sku)
        {
            sku.count += 1;
        }
        else
        {
            sku = [NITBusketAttachmentSku createByModel:model];
            
            [realm addOrUpdateObject:sku];
            [attachment.sku addObject:sku];
        }
    }
    else
    {
        attachment = [NITBusketAttachment new];
        attachment.identifier = model.identifier.stringValue;
        attachment.name = model.name;
        
        NITBusketAttachmentSku * sku = [NITBusketAttachmentSku createByModel:model];
        [attachment.sku addObject:sku];
    }
    
    [realm addOrUpdateObject:attachment];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketAttachmentNotification object:nil];
    [TRACKER trackEvent:TrackerEventAddAttachToBasket parameters:@{@"id":model.baseSkuId}];
    [API validateBasket];
    
    return attachment;
}

+ (NITBusketAttachment *)updateAttachment:(NITBasketProductModel *)model
{
    if (!model)
    {
        return nil;
    }
    
    NITBusketAttachment * oldAttachment = [NITBusketAttachment attachmentByID:model.identifier.stringValue];
    NITBusketAttachment * attachment = [NITBusketAttachment new];
    
    attachment.identifier = model.identifier.stringValue;
    attachment.name = model.name;
    
    for (NITBusketAttachmentSku * oldSku in oldAttachment.sku)
    {
        NITBusketAttachmentSku * sku = [NITBusketAttachmentSku createBySku:oldSku];
        if ([oldSku.identifier isEqualToString:model.skuIdentifier])
        {
            sku.count = oldSku.count;
        }
        
        [attachment.sku addObject:sku];
    }
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITBusketAttachment createOrUpdateInRealm:realm withValue:attachment];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketAttachmentNotification object:nil];
    [API validateBasket];
    
    return attachment;
}


#pragma mark - Get
+ (NSArray *)allAttachments
{
    return [[NITBusketAttachment allObjects] array];
}

+ (NITBusketAttachment *)attachmentByID:(NSString *)attachmentID
{
    return [NITBusketAttachment objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", attachmentID]].firstObject;
}

#pragma mark - Delete
+ (void)clean
{
    RLMResults * results = [NITBusketAttachment allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

+ (void)deleteAllAttachments
{
    RLMResults * results = [NITBusketAttachment allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

- (void)deleteAttachment
{
    RLMRealm * realm = REALM.database;
    
    [realm beginWriteTransaction];
    [realm deleteObject:self];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketAttachmentNotification object:nil];
    [API validateBasket];
}

- (void)deleteAttachmentSkuByID:(NSString *)skuID
{
    [NITBusketAttachmentSku deleteByID:skuID];
    
    NITBusketAttachment * attachment = [NITBusketAttachment attachmentByID:self.identifier];
    
    if ([attachment.sku count] == 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:self];
        [realm commitWriteTransaction];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketAttachmentNotification object:nil];
    [API validateBasket];
}

@end
