//
//  NITBusketAttachment.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITBusketAttachmentSku.h"

@class SWGBasket;

@interface NITBusketAttachment : RLMObject

@property NSString * identifier;
@property NSString * name;
@property RLMArray <NITBusketAttachmentSku *> <NITBusketAttachmentSku> * sku;

@end

RLM_ARRAY_TYPE(NITBusketAttachment)

@class NITBasketProductModel, SWGBasketAttach;

@interface NITBusketAttachment (Accessors)

#pragma mark - Save
+ (void)addAttachments:(NSArray <SWGBasketAttach *> *)models;
+ (NITBusketAttachment *)addAttachment:(NITBasketProductModel *)model;
+ (NITBusketAttachment *)updateAttachment:(NITBasketProductModel *)model;

#pragma mark - Get
+ (NSArray *)allAttachments;
+ (NITBusketAttachment *)attachmentByID:(NSString *)attachmentID;

#pragma mark - Delete
+ (void)clean;
+ (void)deleteAllAttachments;
- (void)deleteAttachment;
- (void)deleteAttachmentSkuByID:(NSString *)skuID;

@end
