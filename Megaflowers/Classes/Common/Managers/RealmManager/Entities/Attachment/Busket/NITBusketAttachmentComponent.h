//
//  NITBusketAttachmentComponent.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITBusketAttachmentComponent : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSString * color;
@property NSInteger count;

@end

RLM_ARRAY_TYPE(NITBusketAttachmentComponent)

@interface NITBusketAttachmentComponent (Accessors)

#pragma mark - Create
+ (NITBusketAttachmentComponent *)createByComponent:(NITBusketAttachmentComponent *)component;
+ (NITBusketAttachmentComponent *)createByDictionary:(NSDictionary *)dictionary;

@end
