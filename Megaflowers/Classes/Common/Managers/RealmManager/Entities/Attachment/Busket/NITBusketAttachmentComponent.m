//
//  NITBusketAttachmentComponent.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBusketAttachmentComponent.h"
#import "NITBasketProductModel.h"

@implementation NITBusketAttachmentComponent

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"name" : @"",
             @"color" : @"",
             @"count" : @0
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

#pragma mark - NITBusketBouquetComponent+Accessors
@implementation NITBusketAttachmentComponent (Accessors)

#pragma mark - Create
+ (NITBusketAttachmentComponent *)createByComponent:(NITBusketAttachmentComponent *)oldComponent
{
    NITBusketAttachmentComponent * component = [NITBusketAttachmentComponent new];
    component.identifier = oldComponent.identifier;
    component.name = oldComponent.name;
    component.count = oldComponent.count;
    component.color = oldComponent.color;
    
    return component;
}

+ (NITBusketAttachmentComponent *)createByDictionary:(NSDictionary *)dictionary
{
    NITBusketAttachmentComponent * component = [NITBusketAttachmentComponent new];
    component.identifier = dictionary[kComponentIDkey];
    component.name = dictionary[kComponentNamekey];
    component.count = [dictionary[kComponentCountkey] integerValue];
    component.color = dictionary[kComponentColorkey];
    
    return component;
}

@end
