//
//  NITFavoriteBouquet.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquet.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"

@implementation NITFavoriteBouquet

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"name" : @"",
             @"imagePath" : @"",
             @"previewPath" : @"",
             @"width" : @0,
             @"height" : @0,
             @"price" : @0,
             @"discountPrice" : @0,
             @"bouquetID" : @"",
             @"bouquetName" : @"",
             @"active" : @true
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITFavoriteBouquet (Extension)

#pragma mark - Create
+ (NITFavoriteBouquet *)createFavoriteBouquetByModel:(NITCatalogItemViewModel *)model andSkuModel:(NITCatalogItemSKUViewModel *)skuModel
{
    if (!model || !skuModel)
    {
        return nil;
    }
    
    NITFavoriteBouquet * bouquet = [[NITFavoriteBouquet alloc] init];
    
    bouquet.identifier  = skuModel.identifier.stringValue;
    bouquet.name        = skuModel.title;
    bouquet.imagePath   = skuModel.imagePath;
    bouquet.previewPath = skuModel.previewPath;
    bouquet.width       = skuModel.width.integerValue;
    bouquet.height      = skuModel.height.integerValue;
    bouquet.price       = skuModel.price.integerValue;
    bouquet.discountPrice = skuModel.discountPrice.integerValue;
    bouquet.bouquetID   = model.identifier.stringValue;
    bouquet.bouquetName = model.title;
    bouquet.active      = model.enabled;
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITFavoriteBouquet createOrUpdateInRealm:realm withValue:bouquet];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteBouquetNotification object:nil];
    
    return bouquet;
}

#pragma mark - Get
+ (NSArray <NITFavoriteBouquet *> *)allBouquets
{
    return [[NITFavoriteBouquet allObjects] array];
}

+ (NITFavoriteBouquet *)bouquetByID:(NSNumber *)bouquetID
{
    return [NITFavoriteBouquet objectsWithPredicate:[NSPredicate predicateWithFormat:@"bouquetID == %@", bouquetID.stringValue]].firstObject;
}

+ (NSArray *)skusByBouquetID:(NSNumber *)bouquetID
{
    return [[NITFavoriteBouquet objectsWithPredicate:[NSPredicate predicateWithFormat:@"bouquetID == %@", bouquetID.stringValue]] array];
}

+ (NITFavoriteBouquet *)bouquetBySkuID:(NSNumber *)skuID
{
    return [NITFavoriteBouquet objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", skuID.stringValue]].firstObject;
}

+ (BOOL)isFavoriteBouquetID:(NSNumber *)bouquetID
{
    return [NITFavoriteBouquet bouquetByID:bouquetID] != nil;
}

+ (BOOL)isFavoriteSkuID:(NSNumber *)skuID
{
    return [NITFavoriteBouquet bouquetBySkuID:skuID] != nil;
}

#pragma mark - Delete
+ (void)deleteAllBouquets
{
    RLMResults * results = [NITFavoriteBouquet allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteBouquetNotification object:nil];
    }
}

+ (void)deleteBouquetBySkuID:(NSNumber *)skuID
{
    NITFavoriteBouquet * bouquet = [NITFavoriteBouquet bouquetBySkuID:skuID];
    
    if (bouquet)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:bouquet];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteBouquetNotification object:nil];
    }
}

#pragma mark -
- (NSString *)presentSize
{
    if (self.width > 0 && self.height > 0)
    {
        return [NSString stringWithFormat:@"%ld-%ld %@", (long)self.width, (long)self.height, NSLocalizedString(@"см", nil)];
    }
    else if (self.width > 0 && self.height == 0)
    {
        return [NSString stringWithFormat:@"%ld %@", (long)self.width, NSLocalizedString(@"см", nil)];
    }
    else if (self.width == 0 && self.height > 0)
    {
        return [NSString stringWithFormat:@"%ld %@", (long)self.height, NSLocalizedString(@"см", nil)];
    }
    else
    {
        return @"";
    }
}

@end
