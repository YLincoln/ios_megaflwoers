//
//  NITFavoriteBouquet.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITFavoriteBouquet : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSString * imagePath;
@property NSString * previewPath;
@property NSInteger width;
@property NSInteger height;
@property NSInteger price;
@property NSInteger discountPrice;
@property NSString * bouquetID;
@property NSString * bouquetName;
@property BOOL active;

@end

RLM_ARRAY_TYPE(NITFavoriteBouquet)

@class NITCatalogItemViewModel, NITCatalogItemSKUViewModel;

@interface NITFavoriteBouquet (Extension)

#pragma mark - Create
+ (NITFavoriteBouquet *)createFavoriteBouquetByModel:(NITCatalogItemViewModel *)model andSkuModel:(NITCatalogItemSKUViewModel *)skuModel;

#pragma mark - Get
+ (NSArray <NITFavoriteBouquet *> *)allBouquets;
+ (NITFavoriteBouquet *)bouquetBySkuID:(NSNumber *)skuID;
+ (NSArray *)skusByBouquetID:(NSNumber *)bouquetID;
+ (BOOL)isFavoriteBouquetID:(NSNumber *)bouquetID;
+ (BOOL)isFavoriteSkuID:(NSNumber *)skuID;

#pragma mark - Delete
+ (void)deleteAllBouquets;
+ (void)deleteBouquetBySkuID:(NSNumber *)skuID;

#pragma mark -
- (NSString *)presentSize;

@end
