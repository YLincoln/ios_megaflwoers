//
//  NITBusketBouquetComponent.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBusketBouquetComponent.h"
#import "NITBasketProductModel.h"

@implementation NITBusketBouquetComponent

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"name" : @"",
             @"color" : @"",
             @"count" : @0
             };
}

@end

#pragma mark - NITBusketBouquetComponent+Accessors
@implementation NITBusketBouquetComponent (Accessors)

#pragma mark - Create
+ (NITBusketBouquetComponent *)createByDictionary:(NSDictionary *)dictionary
{
    NITBusketBouquetComponent * component = [NITBusketBouquetComponent new];
    component.identifier = dictionary[kComponentIDkey];
    component.name = dictionary[kComponentNamekey];
    component.count = [dictionary[kComponentCountkey] integerValue];
    component.color = dictionary[kComponentColorkey];
    
    return component;
}

@end
