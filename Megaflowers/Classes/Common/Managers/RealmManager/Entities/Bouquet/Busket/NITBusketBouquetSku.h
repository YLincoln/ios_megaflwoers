//
//  NITBusketBouquetSku.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITBusketBouquetComponent.h"

@interface NITBusketBouquetSku : RLMObject

@property NSString * identifier;
@property NSString * base_id;
@property NSString * name;
@property NSString * imagePath;
@property NSInteger width;
@property NSInteger height;
@property NSInteger count;
@property NSInteger price;
@property RLMArray <NITBusketBouquetComponent *> <NITBusketBouquetComponent> * components;

@end

@class NITBasketProductModel;

RLM_ARRAY_TYPE(NITBusketBouquetSku)

@class NITBasketProductModel;

@interface NITBusketBouquetSku (Accessors)

#pragma mark - Create
+ (NITBusketBouquetSku *)createByModel:(NITBasketProductModel *)model;

#pragma mark - Get
+ (NITBusketBouquetSku *)skuByID:(NSString *)skuID;
+ (NITBusketBouquetSku *)editedSkuByComponents:(NSArray <NSDictionary *> *)components;
+ (NSString *)editedSkuIDByComponents:(NSArray <NSDictionary *> *)components;

#pragma mark - Edite
+ (void)updateCount:(NSInteger)count bySkuID:(NSString *)skuID;

#pragma mark - Delete
+ (void)deleteByID:(NSString *)skuID;

@end
