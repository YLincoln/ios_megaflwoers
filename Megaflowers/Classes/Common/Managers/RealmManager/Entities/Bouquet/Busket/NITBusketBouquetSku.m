//
//  NITBusketBouquetSku.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBusketBouquetSku.h"
#import "NITBasketProductModel.h"
#import "NSArray+BlocksKit.h"
#import "RLMResults+NSArray.h"
#import "RLMArray+NSArray.h"

@implementation NITBusketBouquetSku

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"base_id" : @"",
             @"name" : @"",
             @"imagePath" : @0,
             @"width" : @0,
             @"height" : @0,
             @"price" : @0,
             @"count" : @0,
             @"components" : [[RLMArray alloc] initWithObjectClassName:NSStringFromClass([NITBusketBouquetComponent class])]
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

#pragma mark - NITBusketBouquetSku+Accessors
@implementation NITBusketBouquetSku (Accessors)

#pragma mark - Create
+ (NITBusketBouquetSku *)createByModel:(NITBasketProductModel *)model
{
    NITBusketBouquetSku * sku = [NITBusketBouquetSku new];
    sku.identifier = model.skuIdentifier;
    sku.base_id = model.baseSkuId.stringValue;
    sku.name = model.skuName;
    sku.imagePath = model.skuImagePath;
    sku.width = model.skuWidth.integerValue;
    sku.height = model.skuHeight.integerValue;
    sku.price = model.skuPrice.integerValue;
    sku.count = model.skuCount.integerValue;
    
    for (NSDictionary * dictionary in model.components)
    {
        [sku.components addObject:[NITBusketBouquetComponent createByDictionary:dictionary]];
    }
    
    return sku;
}

#pragma mark - Get
+ (NITBusketBouquetSku *)skuByID:(NSString *)skuID
{
    return [NITBusketBouquetSku objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", skuID]].firstObject;
}

+ (NITBusketBouquetSku *)editedSkuByComponents:(NSArray <NSDictionary *> *)components
{
    NSArray * skus = [NITBusketBouquetSku objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier != base_id"]].array;
    
    return [skus bk_select:^BOOL(NITBusketBouquetSku *sku) {
        
        if (components.count != sku.components.count)
        {
            return false;
        }
        else
        {
            NSInteger checkSum = 0;
            for (NSDictionary * dictionary in components)
            {
                NSString * identifier = dictionary[kComponentIDkey];
                NSInteger count = [dictionary[kComponentCountkey] integerValue];
                
                NITBusketBouquetComponent * component = [sku.components.array bk_select:^BOOL(NITBusketBouquetComponent *obj) {
                    return [obj.identifier isEqualToString:identifier];
                }].firstObject;
                
                if (component && component.count == count)
                {
                    checkSum++;
                }
            }
            
            return checkSum == components.count;
        }
    }].firstObject;
}

+ (NSString *)editedSkuIDByComponents:(NSArray <NSDictionary *> *)components
{
    NITBusketBouquetSku * sku = [NITBusketBouquetSku editedSkuByComponents:components];

    if (sku)
    {
        return sku.identifier;
    }
    else
    {
        return [HELPER generateUUID];
    }
}

#pragma mark - Edite
+ (void)updateCount:(NSInteger)count bySkuID:(NSString *)skuID
{
    NITBusketBouquetSku * sku = [NITBusketBouquetSku skuByID:skuID];
    
    if (sku)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        sku.count = count;
        [realm commitWriteTransaction];
        
        [API validateBasket];
    }
}

#pragma mark - Delete
+ (void)deleteByID:(NSString *)skuID
{
    NITBusketBouquetSku * sku = [NITBusketBouquetSku skuByID:skuID];
    
    if (sku)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:sku];
        [realm commitWriteTransaction];
    }
}

@end
