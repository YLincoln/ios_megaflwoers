//
//  NITBusketBouquet.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBusketBouquet.h"
#import "NITBasketProductModel.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "RLMArray+NSArray.h"
#import "SWGBasketApi.h"
#import "NITBusketAttachment.h"
#import "NITProductDetailsAPIDataManager.h"
#import "NITCatalogItemViewModel.h"

@implementation NITBusketBouquet

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"name" : @"",
             @"sku" : [[RLMArray alloc] initWithObjectClassName:NSStringFromClass([NITBusketBouquetSku class])]
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

#pragma mark - NITBusketBouquet+Accessors
@implementation NITBusketBouquet (Accessors)

#pragma mark - Save
+ (void)addBouquets:(NSArray <SWGBasketBouquet *> *)models
{
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    
    for (SWGBasketBouquet * swgModel in models)
    {
        NITBasketProductModel * model = [NITBasketProductModel modelBySWGBouquet:swgModel];
        NITBusketBouquet * bouquet = [NITBusketBouquet bouquetByID:model.identifier.stringValue];
        
        if (bouquet)
        {
            NITBusketBouquetSku * sku = [bouquet.sku.array bk_select:^BOOL(NITBusketBouquetSku *obj) {
                return [obj.identifier isEqualToString:model.skuIdentifier];
            }].firstObject;
            
            if (sku)
            {
                sku.count += model.skuCount.integerValue;
            }
            else
            {
                sku = [NITBusketBouquetSku createByModel:model];
                
                [realm addOrUpdateObject:sku];
                [bouquet.sku addObject:sku];
            }
        }
        else
        {
            bouquet = [NITBusketBouquet new];
            bouquet.identifier = model.identifier.stringValue;
            bouquet.name = model.name;
            
            NITBusketBouquetSku * sku = [NITBusketBouquetSku createByModel:model];
            [bouquet.sku addObject:sku];
        }
        
        [realm addOrUpdateObject:bouquet];
    }
    
    [realm commitWriteTransaction];
}

+ (NITBusketBouquet *)addBouquet:(NITBasketProductModel *)model
{
    if (!model)
    {
        return nil;
    }
    
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    
    NITBusketBouquet * bouquet = [NITBusketBouquet bouquetByID:model.identifier.stringValue];
    
    if (bouquet)
    {
        NITBusketBouquetSku * sku = [bouquet.sku.array bk_select:^BOOL(NITBusketBouquetSku *obj) {
            return [obj.identifier isEqualToString:model.skuIdentifier];
        }].firstObject;
        
        if (sku)
        {
            sku.count += 1;
        }
        else
        {
            sku = [NITBusketBouquetSku createByModel:model];
            
            [realm addOrUpdateObject:sku];
            [bouquet.sku addObject:sku];
        }
    }
    else
    {
        bouquet = [NITBusketBouquet new];
        bouquet.identifier = model.identifier.stringValue;
        bouquet.name = model.name;
        
        NITBusketBouquetSku * sku = [NITBusketBouquetSku createByModel:model];
        [bouquet.sku addObject:sku];
    }

    [realm addOrUpdateObject:bouquet];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketBouquetNotification object:nil];
    [TRACKER trackEvent:TrackerEventAddBouquetToBasket parameters:@{@"id":model.baseSkuId}];
    [API validateBasket];

    return bouquet;
}

+ (NITBusketBouquet *)updateBouquet:(NITBasketProductModel *)model
{
    if (!model)
    {
        return nil;
    }
    
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    
    NITBusketBouquet * bouquet = [NITBusketBouquet bouquetByID:model.identifier.stringValue];
    
    if (bouquet)
    {
        NITBusketBouquetSku * skuForUpdate = [bouquet.sku.array bk_select:^BOOL(NITBusketBouquetSku *obj) {
            return [obj.identifier isEqualToString:model.skuIdentifier];
        }].firstObject;
        
        NITBusketBouquetSku * skuForDelete = [bouquet.sku.array bk_select:^BOOL(NITBusketBouquetSku *obj) {
            return [obj.identifier isEqualToString:model.oldSkuIdentifier];
        }].firstObject;

        NSInteger count = 0;
        
        if (skuForUpdate && skuForDelete && [skuForUpdate isEqual:skuForDelete])
        {
            count = skuForUpdate.count;
            [realm deleteObject:skuForUpdate];
        }
        else
        {
            if (skuForUpdate)
            {
                count += skuForUpdate.count;
                [realm deleteObject:skuForUpdate];
            }
            
            if (skuForDelete)
            {
                count += skuForDelete.count;
                [realm deleteObject:skuForDelete];
            }
        }

        NITBusketBouquetSku * sku = [NITBusketBouquetSku createByModel:model];
        sku.count = count;
        
        [realm addOrUpdateObject:sku];
        [bouquet.sku addObject:sku];
    }
    else
    {
        bouquet = [NITBusketBouquet new];
        bouquet.identifier = model.identifier.stringValue;
        bouquet.name = model.name;
        
        NITBusketBouquetSku * sku = [NITBusketBouquetSku createByModel:model];
        [bouquet.sku addObject:sku];
    }
    
    [realm addOrUpdateObject:bouquet];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketBouquetNotification object:nil];
    [API validateBasket];
    
    return bouquet;
}

#pragma mark - Get
+ (NSArray *)allBouquets
{
    return [[NITBusketBouquet allObjects] array];
}

+ (NITBusketBouquet *)bouquetByID:(NSString *)bouquetID
{
    return [NITBusketBouquet objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", bouquetID]].firstObject;
}

#pragma mark - Delete
+ (void)clean
{
    RLMResults * results = [NITBusketBouquet allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

+ (void)deleteAllBouquets
{
    RLMResults * results = [NITBusketBouquet allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

- (void)deleteBouquet
{
    RLMRealm * realm = REALM.database;
    
    [realm beginWriteTransaction];
    [realm deleteObject:self];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketBouquetNotification object:nil];
    [API validateBasket];
}

- (void)deleteBouquetSkuByID:(NSString *)skuID
{
    [NITBusketBouquetSku deleteByID:skuID];
    
    NITBusketBouquet * bouquet = [NITBusketBouquet bouquetByID:self.identifier];
    
    if ([bouquet.sku count] == 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:self];
        [realm commitWriteTransaction];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBusketBouquetNotification object:nil];
    [API validateBasket];
}

@end
