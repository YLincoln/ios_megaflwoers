//
//  NITBusketBouquet.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITBusketBouquetSku.h"

@interface NITBusketBouquet : RLMObject

@property NSString * identifier;
@property NSString * name;
@property RLMArray <NITBusketBouquetSku *> <NITBusketBouquetSku> * sku;

@end

RLM_ARRAY_TYPE(NITBusketBouquet)

@class NITBasketProductModel, SWGBasketBouquet;

@interface NITBusketBouquet (Accessors)

#pragma mark - Save
+ (void)addBouquets:(NSArray <SWGBasketBouquet *> *)models;
+ (NITBusketBouquet *)addBouquet:(NITBasketProductModel *)model;
+ (NITBusketBouquet *)updateBouquet:(NITBasketProductModel *)model;

#pragma mark - Get
+ (NSArray *)allBouquets;
+ (NITBusketBouquet *)bouquetByID:(NSString *)bouquetID;

#pragma mark - Delete
+ (void)clean;
+ (void)deleteAllBouquets;
- (void)deleteBouquet;
- (void)deleteBouquetSkuByID:(NSString *)skuID;

@end
