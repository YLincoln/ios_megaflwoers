//
//  NITBusketBouquetComponent.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITBusketBouquetComponent : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSString * color;
@property NSInteger count;

@end

RLM_ARRAY_TYPE(NITBusketBouquetComponent)

@interface NITBusketBouquetComponent (Accessors)

#pragma mark - Create
+ (NITBusketBouquetComponent *)createByDictionary:(NSDictionary *)dictionary;

@end
