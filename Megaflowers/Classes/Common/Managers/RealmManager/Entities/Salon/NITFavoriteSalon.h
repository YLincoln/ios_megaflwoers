//
//  NITFavoriteSalon.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITFavoriteSalon : RLMObject

@property NSString * identifier;
@property NSString * address;
@property NSString * schedule;
@property NSString * phone;
@property CGFloat lat;
@property CGFloat lon;

@end

RLM_ARRAY_TYPE(NITFavoriteSalon)

@class NITAddressModel;

@interface NITFavoriteSalon (Extension)

#pragma mark - Save
+ (void)addSalons:(NSArray <NITAddressModel *> *)salons;

#pragma mark - Get
- (CLLocation *)location;
+ (NITFavoriteSalon *)salonByID:(NSNumber *)salonID;
+ (NSArray <NITFavoriteSalon *> *)allSalons;
+ (BOOL)isFavoriteSalonID:(NSNumber *)salonID;

#pragma mark - Delete
+ (void)deleteAllSalons;
+ (void)deleteSalonByID:(NSNumber *)salonID;

@end
