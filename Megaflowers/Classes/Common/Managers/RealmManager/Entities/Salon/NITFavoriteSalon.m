//
//  NITFavoriteSalon.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalon.h"
#import "NITAddressModel.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"

@implementation NITFavoriteSalon

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"address" : @"",
             @"schedule" : @"",
             @"phone" : @"",
             @"lat" : @0,
             @"lon" : @0
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITFavoriteSalon (Extension)

#pragma mark - Save
+ (void)addSalons:(NSArray <NITAddressModel *> *)salons
{
    if (salons && salons.count > 0)
    {
        // filling
        NSMutableArray * salonsData = [NSMutableArray new];
        for (NITAddressModel * favoriteModel in salons)
        {
            NITFavoriteSalon * salon = [NITFavoriteSalon createSalonModel:favoriteModel];
            
            if (salon)
            {
                [salonsData addObject:salon];
            }
        }
        
        // save
        RLMRealm * realm = REALM.database;
        [realm beginWriteTransaction];
        for (NITFavoriteSalon * salon in salonsData)
        {
            [NITFavoriteSalon createOrUpdateInRealm:realm withValue:salon];
        }
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteSalonNotification object:nil];
    }
}

+ (NITFavoriteSalon *)createSalonModel:(NITAddressModel *)salonModel
{
    if (!salonModel)
    {
        return nil;
    }
    
    NITFavoriteSalon * salon = [[NITFavoriteSalon alloc] init];
    
    salon.identifier    = [salonModel.identifier stringValue];
    salon.address       = salonModel.address;
    salon.schedule      = salonModel.schedule;
    salon.phone         = salonModel.phone;
    salon.lat           = salonModel.location.coordinate.latitude;
    salon.lon           = salonModel.location.coordinate.longitude;
    
    return salon;
}

#pragma mark - Get
- (CLLocation *)location
{
    return [[CLLocation alloc] initWithLatitude:self.lat longitude:self.lon];
}

+ (NITFavoriteSalon *)salonByID:(NSNumber *)salonID
{
    return [NITFavoriteSalon objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", salonID.stringValue]].firstObject;
}

+ (NSArray <NITFavoriteSalon *> *)allSalons
{
    return [[NITFavoriteSalon allObjects] array];
}

+ (BOOL)isFavoriteSalonID:(NSNumber *)salonID
{
    return [NITFavoriteSalon salonByID:salonID] != nil;
}

#pragma mark - Delete
+ (void)deleteAllSalons
{
    RLMResults * results = [NITFavoriteSalon allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteSalonNotification object:nil];
    }
}

+ (void)deleteSalonByID:(NSNumber *)salonID
{
    NITFavoriteSalon * salon = [NITFavoriteSalon salonByID:salonID];
    
    if (salon)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:salon];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteSalonNotification object:nil];
    }
}

@end
