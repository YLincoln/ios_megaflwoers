//
//  NITOrder.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/15/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITOrder : RLMObject

@property NSString * identifier;
@property NSString * orderHash;
@property NSInteger num;
@property NSString * senderName;
@property NSString * senderPhone;
@property NSString * senderEmail;
@property NSString * receiverName;
@property NSString * receiverPhone;
@property NSString * receiverAddress;
@property NSString * statusId;
@property NSDate * deliveryDate;
@property NSInteger deliveryTime;
@property BOOL takePhoto;
@property BOOL leaveNeighbors;
@property BOOL dontCallReceiver;
@property BOOL knowOnlyPhone;
@property BOOL forMe;
@property BOOL isPickup;

@end

RLM_ARRAY_TYPE(NITOrder)

@class SWGOrder;

@interface NITOrder (Extension)

#pragma mark - Save
+ (void)addOrders:(NSArray <SWGOrder *> *)orders;

#pragma mark - Get
+ (NITOrder *)orderByID:(NSNumber *)orderID;
+ (NSArray <NITOrder *> *)allOrders;
+ (NSArray <NITOrder *> *)currentOrders;
+ (NSArray <NITOrder *> *)complitedOrders;
+ (NITOrder *)lastOrder;

#pragma mark - Delete
+ (void)deleteAllOrders;
+ (void)deleteOrderByID:(NSNumber *)orderID;

@end
