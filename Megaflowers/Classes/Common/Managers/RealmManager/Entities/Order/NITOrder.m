//
//  NITOrder.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/15/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrder.h"
#import "SWGOrder.h"
#import "RLMResults+NSArray.h"

@implementation NITOrder

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier" : @"",
             @"orderHash" : @"",
             @"num" : @0,
             @"senderName" : @"",
             @"senderPhone" : @"",
             @"senderEmail" : @"",
             @"receiverName" : @"",
             @"receiverPhone" : @"",
             @"receiverAddress" : @"",
             @"statusId" : @"",
             @"deliveryDate" : [NSDate date],
             @"deliveryTime" : @false,
             @"takePhoto" : @false,
             @"leaveNeighbors" : @false,
             @"dontCallReceiver" : @false,
             @"knowOnlyPhone" : @false,
             @"forMe" : @false,
             @"isPickup" : @false
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITOrder (Extension)

#pragma mark - Save
+ (void)addOrders:(NSArray <SWGOrder *> *)orders
{
    if (orders && orders.count > 0)
    {
        // filling
        NSMutableArray * ordersData = [NSMutableArray new];
        for (SWGOrder * model in orders)
        {
            NITOrder * order = [NITOrder createOrderByModel:model];
            
            if (order)
            {
                [ordersData addObject:order];
            }
        }
        
        // save
        RLMRealm * realm = REALM.database;
        [realm beginWriteTransaction];
        for (NITOrder * salon in ordersData)
        {
            [NITOrder createOrUpdateInRealm:realm withValue:salon];
        }
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveOrderNotification object:nil];
    }
}

+ (NITOrder *)createOrderByModel:(SWGOrder *)model
{
    if (!model)
    {
        return nil;
    }
    
    NITOrder * order = [[NITOrder alloc] init];
    
    order.identifier        = model._id.stringValue;
    order.orderHash         = model.orderHash;
    order.num               = model.num.integerValue;
    order.senderName        = model.senderName;
    order.senderPhone       = model.senderPhone;
    order.senderEmail       = model.senderEmail;
    order.receiverName      = model.receiverName;
    order.receiverPhone     = model.receiverPhone;
    order.receiverAddress   = model.receiverAddress;
    order.deliveryDate      = [NSDate dateFromServerString:model.deliveryDate];
    order.takePhoto         = model.takePhoto.boolValue;
    order.leaveNeighbors    = model.leaveNeighbors.boolValue;
    order.dontCallReceiver  = model.dontCallReceiver.boolValue;
    order.knowOnlyPhone     = model.knowOnlyPhone.boolValue;
    order.forMe             = model.forMe.boolValue;
    order.isPickup          = model.isPickup.boolValue;
    order.statusId          = model.statusId.stringValue;

    return order;
}

#pragma mark - Get
+ (NITOrder *)orderByID:(NSNumber *)orderID
{
    return [NITOrder objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", orderID.stringValue]].firstObject;
}

+ (NSArray <NITOrder *> *)allOrders
{
    return [[NITOrder allObjects] array];
}

+ (NSArray <NITOrder *> *)currentOrders
{
    NSArray * predicats = @[
                            [NSPredicate predicateWithFormat:@"statusId != %@", @(12).stringValue], // Отменён
                            [NSPredicate predicateWithFormat:@"statusId != %@", @(15).stringValue]  // Выполнен
                            ];
    
    return [[NITOrder objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array];
}

+ (NSArray <NITOrder *> *)complitedOrders
{
    NSArray * predicats = @[
                            [NSPredicate predicateWithFormat:@"statusId == %@", @(12).stringValue], // Отменён
                            [NSPredicate predicateWithFormat:@"statusId == %@", @(15).stringValue]  // Выполнен
                            ];
    
    return [[NITOrder objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] array];
}

+ (NITOrder *)lastOrder
{
    NSArray * predicats = @[
                            [NSPredicate predicateWithFormat:@"statusId != %@", @(12).stringValue], // Отменён
                            [NSPredicate predicateWithFormat:@"statusId != %@", @(0).stringValue]
                            ];
    
    return [[[NITOrder objectsWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicats]] sortedResultsUsingKeyPath:keyPath(NITOrder.num) ascending:false] array].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllOrders
{
    RLMResults * results = [NITOrder allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveOrderNotification object:nil];
    }
}

+ (void)deleteOrderByID:(NSNumber *)orderID
{
    NITOrder * order = [NITOrder orderByID:orderID];
    
    if (order)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:order];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveOrderNotification object:nil];
    }
}


@end
