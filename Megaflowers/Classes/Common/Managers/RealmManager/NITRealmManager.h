//
//  NITRealmManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

#define REALM [NITRealmManager sharedInstance]

static NSString * kSaveCouponNotification           = @"save_coupon_notification";
static NSString * kSaveBusketBouquetNotification    = @"save_busket_bouquet_notification";
static NSString * kSaveBusketAttachmentNotification = @"save_busket_attachment_notification";
static NSString * kSaveFavoriteBouquetNotification  = @"save_favorite_bouquet_notification";
static NSString * kSaveFavoriteSalonNotification    = @"save_favorite_salon_notification";
static NSString * kSaveEventNotification            = @"save_event_notification";
static NSString * kSaveOrderNotification            = @"save_order_notification";

@interface NITRealmManager : NSObject

@property (nonatomic, strong) RLMRealm * database;

+ (NITRealmManager *) sharedInstance;

@end
