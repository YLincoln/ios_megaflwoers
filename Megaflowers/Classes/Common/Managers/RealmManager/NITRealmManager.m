//
//  NITRealmManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRealmManager.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

static NSInteger const currentDBVersion = 22;

@implementation NITRealmManager

+ (NITRealmManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITRealmManager	*shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    [self initDatabase];
}

- (RLMRealm *)database
{
    return [RLMRealm defaultRealm];
}

#pragma mark - Database
- (void)initDatabase
{
    RLMRealmConfiguration * config = [RLMRealmConfiguration defaultConfiguration];
    
    // Установим новую версию схемы. Это число должно быть больше предыдущей версии
    // (0, если вы никогда не устанавливали номер версии).
    
    config.schemaVersion = currentDBVersion;
    
    // Определим блок, который будет вызван автоматически, при открытии Realm,
    // с версией схемы меньше чем определна выше
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // Мы еще не проводили миграций, поэтому oldSchemaVersion == 0
        if (oldSchemaVersion < currentDBVersion) {
            // Можно ничего не делать!
            // Realm определит новые поля, удалит старые
            // и обновит схему на диске автоматически
            DDLogInfo(@"RELAM IS MIGRATED %llu => %ld", oldSchemaVersion, (long)currentDBVersion);
        }
    };
    
    // Установить конфигурацию для Realm по умолчанию
    [RLMRealmConfiguration setDefaultConfiguration:config];
    
    
    // Теперь, когда мы указали Realm как проводить смену схемы, открытие файла
    // автоматически произведет миграцию
    _database = [RLMRealm defaultRealm];
}

- (void)clearDatabase
{
    [self.database beginWriteTransaction];
    [self.database deleteAllObjects];
    [self.database commitWriteTransaction];
}

@end
