//
//  NITPhotoSliderController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPhotoSliderController.h"
#import "NSArray+BlocksKit.h"

@interface NITPhotoSliderController ()

@end

@implementation NITPhotoSliderController

- (id)initWithImagePaths:(NSArray *)imagePaths
{
    NSArray * imageURLs = [imagePaths bk_map:^id(NSString * obj) {
        return [NSURL URLWithString:obj];
    }];
    
    if (self = [super initWithImageURLs:imageURLs])
    {
        [self setup];
    }
    
    return self;
}

- (id)initWithImageURLs:(NSArray *)imageURLs
{
    if (self = [super initWithImageURLs:imageURLs])
    {
        [self setup];
    }
    
    return self;
}

- (id)initWithImages:(NSArray *)images
{
    if (self = [super initWithImages:images])
    {
        [self setup];
    }
    
    return self;
}

- (void) setup
{
    self.visiblePageControl = true;
    self.visibleCloseButton = true;
    self.backgroundColor = [UIColor whiteColor];
    self.currentPage = 0;
    self.enableDynamicsAnimation = false;
    self.closeBtnImageName = @"ic_close";
}


@end
