//
//  NITPhotoSliderController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NKJPhotoSliderController.h"

@interface NITPhotoSliderController : NKJPhotoSliderController

- (id)initWithImagePaths:(NSArray *)imagePaths;

@end
