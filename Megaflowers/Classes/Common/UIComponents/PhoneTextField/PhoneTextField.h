//
//  PhoneTextField.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneTextField : UITextField

@property (nonatomic) BOOL formatEnabled;

@end

@interface PhoneTextFieldDelegate : NSObject <UITextFieldDelegate>

@property (nonatomic) id <UITextFieldDelegate> realDelegate;
@property (nonatomic) BOOL formatEnabled;

- (instancetype)initWithRealDelegate:(id <UITextFieldDelegate>)realDelegate formatEnabled:(BOOL)formatEnabled;

@end
