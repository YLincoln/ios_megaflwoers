//
//  PhoneTextField.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "PhoneTextField.h"
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"

static NSString * const kDefaultRegion  = @"RU";
static NSInteger  const kMaxPhoneLenth  = 18;

@interface PhoneTextField ()

@property (nonatomic) NBPhoneNumberUtil * phoneUtil;
@property (nonatomic) PhoneTextFieldDelegate * phoneTextFieldDelegate;

@end

@implementation PhoneTextField

- (instancetype)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    self.formatEnabled = true;
    self.phoneUtil = [NBPhoneNumberUtil new];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidBeginEditing:)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidEndEditing:)
                                                 name:UITextFieldTextDidEndEditingNotification
                                               object:self];
}

#pragma mark - Custom accessors
- (void)setDelegate:(id <UITextFieldDelegate>)delegate
{
    self.phoneTextFieldDelegate = [[PhoneTextFieldDelegate alloc] initWithRealDelegate:delegate formatEnabled:self.formatEnabled];
    [super setDelegate:self.phoneTextFieldDelegate];
}

- (void)setText:(NSString *)text
{
    if (self.formatEnabled)
    {
        NSError * anError = nil;
        NBPhoneNumber * myNumber = [self.phoneUtil parse:text
                                           defaultRegion:kDefaultRegion
                                                   error:&anError];
        if (anError == nil)
        {
            text = [self.phoneUtil format:myNumber
                             numberFormat:NBEPhoneNumberFormatINTERNATIONAL
                                    error:&anError];;
        }
    }
    
    [super setText:text];
}

- (void)setFormatEnabled:(BOOL)formatEnabled
{
    _formatEnabled = formatEnabled;
    self.phoneTextFieldDelegate.formatEnabled = formatEnabled;
}

#pragma mark - Private
- (void)textFieldDidChange:(NSNotification *)notification
{
    if (self.formatEnabled && notification.object == self)
    {
        NSError * anError = nil;
        NBPhoneNumber * myNumber = [self.phoneUtil parse:self.text
                                           defaultRegion:kDefaultRegion
                                                   error:&anError];
        if (anError == nil)
        {
            self.text = [self.phoneUtil format:myNumber
                                  numberFormat:NBEPhoneNumberFormatINTERNATIONAL
                                         error:&anError];;
        }
        
        if (self.text.length == 0)
        {
            self.text = @"+";
        }
    }
}

- (void)textFieldDidBeginEditing:(NSNotification *)notification
{
    if (self.formatEnabled && notification.object == self && self.text.length == 0)
    {
        self.text = @"+7";
    }
}

- (void)textFieldDidEndEditing:(NSNotification *)notification
{
    if (self.formatEnabled && notification.object == self &&
        ([self.text isEqualToString:@"+"] || [self.text isEqualToString:@"+7"]))
    {
        self.text = @"";
        
        if ([self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)])
        {
            [self.delegate textFieldDidEndEditing:self];
        }
    }
}

@end

#pragma mark - NSObject overrides
@interface PhoneTextFieldDelegate ()

@property (nonatomic) NBPhoneNumberUtil * phoneUtil;

@end

@implementation PhoneTextFieldDelegate

- (instancetype)initWithRealDelegate:(id <UITextFieldDelegate>)realDelegate formatEnabled:(BOOL)formatEnabled
{
    if (self = [self init])
    {
        self.formatEnabled = formatEnabled;
        self.realDelegate = realDelegate;
        self.phoneUtil = [NBPhoneNumberUtil new];
    }
    
    return self;
}

#pragma mark - NSObject overrides
- (id)forwardingTargetForSelector:(SEL)aSelector
{
    return self.realDelegate;
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    if (self.realDelegate && [self.realDelegate respondsToSelector:aSelector])
    {
        return true;
    }
    
    return [super respondsToSelector:aSelector];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self.realDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]
        && ![self.realDelegate textField:textField shouldChangeCharactersInRange:range replacementString:string])
    {
        return false;
    }
    
    if (self.formatEnabled && ![string isEqualToString:@""])
    {
        if (textField.text.length == 0)
        {
            if ([string isEqualToString:@"+"])
            {
                textField.text = @"+7";
                return false;
            }
            
            textField.text = [string isEqualToString:@"7"] ? @"+" : @"+7";
        }
        else if (textField.text.length == kMaxPhoneLenth)
        {
            return false;
        }
        
        NSError * anError = nil;
        NBPhoneNumber * myNumber = [self.phoneUtil parse:textField.text
                                           defaultRegion:kDefaultRegion
                                                   error:&anError];
        
        if ([self.phoneUtil isValidNumber:myNumber])
        {
            return false;
        }
    }
    
    return true;
}

@end
