//
//  NITNotificationView.h
//  Megaflowers
//
//  Created by Yaroslav Zavyalov on 20.12.16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, NotificationPosition) {
    NotificationPositionBottom = 0,
    NotificationPositionTop = 1
};

typedef void(^PressedHandlerNotification)(id);
typedef void(^TapHandler)();

@interface NITNotificationView : UIView

@property(nonatomic,strong) NSString *body;
@property(nonatomic,strong) UIColor *textColor;
@property(nonatomic) NotificationPosition position;
@property(nonatomic) NSTextAlignment textAligment;
@property(nonatomic,copy) PressedHandlerNotification pressedHandeler;
@property(nonatomic) TapHandler tapHandler;
@property(nonatomic,strong) id userData;

+ (NITNotificationView *)showError:(NSString*)body;
+ (NITNotificationView *)showError:(NSString*)body pressedHandler:(PressedHandlerNotification)pressedHandler userData:(id)userData;
+ (void)showWithText:(NSString *)text tapHandler:(TapHandler)handler;
- (void)show;
- (void)hideAnimate;


@end
