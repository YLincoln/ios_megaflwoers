//
//  NITNotificationView.m
//  Megaflowers
//
//  Created by Yaroslav Zavyalov on 20.12.16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITNotificationView.h"
#import "NITProfileWireFrame.h"

static NSTimeInterval const animateDuration = 0.3f;
static NSTimeInterval const displayDuration = 3.f;
static NSTimeInterval const delayDuration = 0.f;

@interface NITNotificationView()
{
    UILabel *_textLabel;
    NSTimer *timer;
    NSDate *startPressed;
    UITapGestureRecognizer *singleTap;
    UISwipeGestureRecognizer *swipeup;
    UISwipeGestureRecognizer *swipedown;
}

@property(nonatomic,readonly) UILabel *textLabel;

@end


@implementation NITNotificationView

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.backgroundColor = [UIColor colorWithRed: 217.0 / 255.0 green: 128.0 / 255.0 blue: 178.0 / 255.0 alpha: 1.0f];
        self.textAligment = NSTextAlignmentCenter;
        self.textColor = [UIColor whiteColor];
        [self.textLabel setFont:[UIFont systemFontOfSize:13]];
        [self addSubview:self.textLabel];
        
        self.position = 1;
        
        singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:singleTap];
        swipeup = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeup:)];
        swipeup.direction=UISwipeGestureRecognizerDirectionUp;
        [self addGestureRecognizer:swipeup];
        swipedown = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipersdown:)];
        swipedown.direction=UISwipeGestureRecognizerDirectionDown;
        [self addGestureRecognizer:swipedown];
    }
    
    return self;
}

+ (NITNotificationView *)showError:(NSString*)body
{
    return [self showError:body pressedHandler:nil userData:nil];
}

+ (NITNotificationView *)showError:(NSString*)body pressedHandler:(PressedHandlerNotification)pressedHandler userData:(id)userData
{
    NITNotificationView *alert = [NITNotificationView new];
    alert.body = body;
    alert.textAligment = NSTextAlignmentCenter;
    alert.position = NotificationPositionTop;
    alert.pressedHandeler = pressedHandler;
    alert.userData = userData;
    [alert show];
    return alert;
}

+ (void)showWithText:(NSString *)text tapHandler:(TapHandler)handler
{
    NITNotificationView * alert = [NITNotificationView new];
    alert.body = text;
    alert.position = NotificationPositionTop;
    alert.tapHandler = handler;
    
    [alert show];
}

#pragma mark - Custom Accessors

- (void)setBody:(NSString *)body
{
    _body = body;
    self.textLabel.text = _body;
}

- (void)setPosition:(NotificationPosition)position
{
    _position = position;
}

- (UILabel*)textLabel
{
    if (!_textLabel)
    {
        _textLabel = [UILabel new];
        _textLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _textLabel.textColor = self.textColor;
        _textLabel.numberOfLines = 0;
        _textLabel.textAlignment = self.textAligment;
    }
    
    return _textLabel;
}

#pragma mark - IBActions

#pragma mark - Public

- (void)show
{
    UIWindow *superView =  [[[UIApplication sharedApplication] delegate] window];
    [superView addSubview:self];
    
    superView.translatesAutoresizingMaskIntoConstraints = YES;
    self.userInteractionEnabled = NO;
    //self.alpha = 0.f;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSMutableArray *constraints = [NSMutableArray new];
    
    NSString *formatVPosition = self.position == NotificationPositionBottom ? @"V:[self]|" : @"V:|[self]";
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:formatVPosition
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"self":self}]];
    
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[self(==superView)]"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"self":self, @"superView":superView}]];
    
    [superView addConstraints:constraints];
    
    constraints = [NSMutableArray new];
    
    NSNumber *paddingTop = self.position == NotificationPositionBottom ? @20 : @20;
    
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-paddingTop-[_textLabel]-paddingBottom-|"
                                                                             options:0
                                                                             metrics:@{@"paddingTop" : paddingTop, @"paddingBottom": @10}
                                                                               views:NSDictionaryOfVariableBindings(_textLabel)]];
    
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-lPadding-[_textLabel]-rPadding-|"
                                                                             options:0
                                                                             metrics:@{@"lPadding" : @5, @"rPadding" : (self.pressedHandeler ? @28 : @5)}
                                                                               views:NSDictionaryOfVariableBindings(_textLabel)]];
    
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_textLabel(>=minHeight)]"
                                                                             options:0
                                                                             metrics:@{@"minHeight": @40}
                                                                               views:NSDictionaryOfVariableBindings(_textLabel)]];
    
    [self addConstraints:constraints];
    
    [self startPositionNotification];
    [self showAnimate];
}

#pragma mark - Private

- (void)startPositionNotification
{
    CGRect frame = self.frame;
    if(self.position == 1)
    {
        frame = CGRectMake(frame.origin.x, -self.frame.size.height, frame.size.width, 70);
    }
    if(self.position == 0)
    {
        frame = CGRectMake(frame.origin.x, self.frame.origin.y + self.frame.size.height, frame.size.width, 70);
        self.frame = frame;
    }
    self.frame = frame;
}

- (void)showAnimate
{
    self.userInteractionEnabled = YES;
    
    CGFloat height = self.frame.size.height;
    [UIView animateWithDuration:animateDuration delay:delayDuration options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect frame = self.frame;
        if(self.position == 1)
        {
            frame.origin.y +=height;
        }
        if(self.position == 0)
        {
            frame.origin.y -=height;
        }
        self.frame = frame;
    } completion:^(BOOL finished) {
        [self showCompleteAnimation];
    }];
}

- (void)hideAnimate
{
    CGFloat height = self.frame.size.height;
    [self stopTimer];
    [UIView animateWithDuration:animateDuration delay:0.f options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect frame = self.frame;
        if(self.position == 1)
        {
            frame.origin.y -=height;
        }
        if(self.position == 0)
        {
            frame.origin.y +=height;
        }
        self.frame = frame;
    } completion:^(BOOL finished) {
        [self hideCompleteAnimation];
    }];
}

- (void)hideAnimateUp
{
    CGFloat height = self.frame.size.height;
    [self stopTimer];
    [UIView animateWithDuration:animateDuration delay:0.f options:UIViewAnimationOptionCurveEaseOut animations:^{
        if(self.position == 1)
        {
            CGRect frame = self.frame;
            frame.origin.y -=height;
            self.frame = frame;
        }
    } completion:^(BOOL finished) {
        [self hideCompleteAnimation];
    }];
}

- (void)hideAnimateDown
{
    CGFloat height = self.frame.size.height;
    [self stopTimer];
    [UIView animateWithDuration:animateDuration delay:0.f options:UIViewAnimationOptionCurveEaseOut animations:^{
        if(self.position == 0)
        {
            CGRect frame = self.frame;
            frame.origin.y +=height;
            self.frame = frame;
        }
    } completion:^(BOOL finished) {
        [self hideCompleteAnimation];
    }];
}

- (void)toCenterAnimate
{
    [UIView animateWithDuration:animateDuration delay:0.f options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self layoutIfNeeded];
    } completion:nil];
}

- (void)showCompleteAnimation
{
    [self startTimer];
}

- (void)hideCompleteAnimation
{
    [self stopTimer];
    [_textLabel removeFromSuperview];
    _textLabel = nil;
    [self removeFromSuperview];
}

- (void)startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:(displayDuration + animateDuration + delayDuration)
                                             target:self
                                           selector:@selector(timerTick:)
                                           userInfo:nil
                                            repeats:NO];
}

- (void)stopTimer
{
    if (timer)
    {
        [timer invalidate];
        timer = nil;
    }
}

- (void)timerTick:(NSTimer*)timer
{
    [self hideAnimate];
}

#pragma mark - Gesture

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self hideAnimate];
    
    if (self.tapHandler)
    {
        self.tapHandler();
        self.tapHandler = nil;
    }
}

- (void)swipeup:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(self.position == NotificationPositionTop)
    {
        [self hideAnimateUp];
    }
}

- (void)swipersdown:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(self.position == NotificationPositionBottom)
    {
        [self hideAnimateDown];
    }
}

@end
