//
//  OverlayView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/26/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    OverlayHoleFormRectangle        = 0,
    OverlayHoleFormCircle           = 1,
    OverlayHoleFormRoundedRectangle = 2
} OverlayHoleForm;

@interface OverlayHole : NSObject

@property (nonatomic) CGRect rect;
@property (nonatomic) OverlayHoleForm form;
@property (nonatomic) BOOL transparentEvent;
@property (nonatomic) UIView * boundView;

- (UIBezierPath *)bezierPath;
- (BOOL)containsPoint:(CGPoint)point;

@end

@interface OverlayView : UIView

@property (nonatomic) BOOL animated;
@property (nonatomic) BOOL hideWhenTapped;
@property (nonatomic, copy) void(^willShowCallback)();
@property (nonatomic, copy) void(^didShowCallback)();
@property (nonatomic, copy) void(^willHideCallback)();
@property (nonatomic, copy) void(^didHideCallback)();
@property (nonatomic, readonly) BOOL isShown;

+ (OverlayView *)showingOverlay;

- (OverlayHole *)addHoleWithRect:(CGRect)rect form:(OverlayHoleForm)form transparentEvent:(BOOL)transparentEvent;
- (OverlayHole *)addHoleWithView:(UIView*)view padding:(CGFloat)padding offset:(CGSize)offset form:(OverlayHoleForm)form transparentEvent:(BOOL)transparentEvent;

- (void)show;
- (void)hide;

@end
