//
//  OverlayView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/26/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "OverlayView.h"
#import <QuartzCore/QuartzCore.h>

#define ANIMATION_DURATION 0.4f

#pragma mark - OverlayHole
@implementation OverlayHole

- (UIBezierPath *)bezierPath
{
    UIBezierPath * path;
    switch (self.form) {
        case OverlayHoleFormRectangle:
            path = [UIBezierPath bezierPathWithRect:self.rect];
            break;
        case OverlayHoleFormCircle:
            path = [UIBezierPath bezierPathWithOvalInRect:self.rect];
            break;
        case OverlayHoleFormRoundedRectangle:
            path = [UIBezierPath bezierPathWithRoundedRect:self.rect cornerRadius:12.0f];
            break;
        default:
            break;
    }
    
    return path;
}

- (BOOL)containsPoint:(CGPoint)point
{
    if (self.boundView)
    {
        UIView * rootView = self.boundView;
        while (rootView.superview)
        {
            rootView = rootView.superview;
        }
        
        CGRect rect = [self.boundView convertRect:self.boundView.bounds toView:rootView];
        return CGRectContainsPoint(rect, point);
    }
    
    UIBezierPath * path = [self bezierPath];
    
    if (path == nil)
    {
        return true;
    }
    
    return [path containsPoint:point];
}

@end

#pragma mark - OverlayView
@interface OverlayView ()
{
    NSMutableArray * holes;
}

@end

@implementation OverlayView

static OverlayView * showingOverlay;

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self initialize];
    }
    
    return self;
}

- (void)initialize
{
    holes = [NSMutableArray array];
    self.opaque = true;
    self.backgroundColor = RGBA(10, 88, 43, 0.8);
    self.hideWhenTapped = NO;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetFillColorWithColor(context, self.backgroundColor.CGColor);
    CGContextFillRect(context, rect);
    
    for (OverlayHole * hole in holes)
    {
        if (!CGRectIsEmpty(CGRectIntersection(hole.rect, rect)))
        {
            UIBezierPath * path = [hole bezierPath];
            if (path)
            {
                [path fillWithBlendMode:kCGBlendModeClear alpha:1];
            }
        }
    }
    
    CGContextRestoreGState(context);
}

#pragma mark - Public
+ (OverlayView *)showingOverlay
{
    return showingOverlay;
}

- (OverlayHole *)addHoleWithRect:(CGRect)rect form:(OverlayHoleForm)form transparentEvent:(BOOL)transparentEvent
{
    OverlayHole * hole = [[OverlayHole alloc] init];
    hole.rect = rect;
    hole.form = form;
    hole.transparentEvent = transparentEvent;
    
    [holes addObject:hole];
    
    return hole;
}

- (OverlayHole *)addHoleWithView:(UIView *)view padding:(CGFloat)padding offset:(CGSize)offset form:(OverlayHoleForm)form transparentEvent:(BOOL)transparentEvent
{
    UIView * rootView = view;
    
    while (rootView.superview)
    {
        rootView = rootView.superview;
    }
    
    CGRect rect = [view convertRect:view.bounds toView:rootView];
    
    CGSize paddingSize = [self calculatePaddingSizeWithRect:rect defaultPadding:padding form:form];
    rect.origin.x += offset.width - paddingSize.width;
    rect.origin.y += offset.height - paddingSize.height;
    rect.size.width += paddingSize.width * 2;
    rect.size.height += paddingSize.height * 2;
    
    OverlayHole * hole = [self addHoleWithRect:rect form:form transparentEvent:transparentEvent];
    hole.boundView = view;
    
    return hole;
}

- (void)show
{
    if (self.isShown)
    {
        return;
    }
    
    if (showingOverlay)
    {
        [showingOverlay hide];
    }
    
    showingOverlay = self;
    
    if (self.willShowCallback)
    {
        self.willShowCallback();
    }
    
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    self.frame = window.bounds;
    [window addSubview:self];
    [window bringSubviewToFront:self];
    
    if (self.animated)
    {
        self.alpha = 0.0f;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.alpha = 1.0f;
        } completion:^(BOOL finished) {
            if (finished)
            {
                if (self.didShowCallback)
                {
                    self.didShowCallback();
                }
            }
        }];
    }
    else
    {
        if (self.didShowCallback)
        {
            self.didShowCallback();
        }
    }
}

- (void)hide
{
    if (!self.isShown)
    {
        return;
    }
    
    showingOverlay = nil;
    
    if (self.willHideCallback)
    {
        self.willHideCallback();
    }
    
    if (self.animated)
    {
        self.alpha = 1.0f;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.alpha = 0.0f;
        } completion:^(BOOL finished) {
            if (finished)
            {
                [self removeFromSuperview];
                if (self.didHideCallback)
                {
                    self.didHideCallback();
                }
            }
        }];
    }
    else
    {
        [self removeFromSuperview];
        if (self.didHideCallback)
        {
            self.didHideCallback();
        }
    }
}

#pragma mark - Private
- (CGSize)calculatePaddingSizeWithRect:(CGRect)rect defaultPadding:(CGFloat)defaultPadding form:(OverlayHoleForm)form
{
    if (rect.size.width == 0 || rect.size.height == 0)
    {
        return CGSizeMake(defaultPadding, defaultPadding);
    }
    
    CGSize paddingSize;
    switch (form) {
        case OverlayHoleFormRectangle:
        case OverlayHoleFormRoundedRectangle:
        {
            paddingSize.width = defaultPadding;
            paddingSize.height = defaultPadding;
            break;
        }
        case OverlayHoleFormCircle:
        {
            CGFloat w = rect.size.width;
            CGFloat h = rect.size.height;
            CGFloat theta = atan2f(h, w);
            CGFloat sin = sinf(theta);
            CGFloat cos = cosf(theta);
            CGFloat coef = sqrtf(sin * sin + (h * h) / (w * w) * cos * cos);
            CGFloat cw = w * coef / sin;
            CGFloat ch = h * coef / sin;
            paddingSize.width = (cw - w) / 2 + defaultPadding;
            paddingSize.height = (ch - h) / 2 + defaultPadding;
            break;
        }
        default:
            break;
    }
    
    return paddingSize;
}

- (BOOL)isShown
{
    return self.superview != nil;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    for (OverlayHole * hole in holes)
    {
        if (!hole.transparentEvent)
        {
            continue;
        }
        
        if ([hole containsPoint:point])
        {
            return false;
        }
    }
    
    return true;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.hideWhenTapped)
    {
        [self hide];
    }
}

@end
