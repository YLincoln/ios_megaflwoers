//
//  NITPlaceholderTextView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITPlaceholderTextView : UITextView

@property (nonatomic, readonly) BOOL showsPlaceholder;
@property (nonatomic, copy) IBInspectable NSString * placeholder;
@property (nonatomic, strong) IBInspectable UIColor * placeholderColor; // Default color is the same as in UITextField

@end
