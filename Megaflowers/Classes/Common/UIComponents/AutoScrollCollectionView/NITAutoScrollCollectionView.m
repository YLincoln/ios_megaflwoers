//
//  NITAutoScrollCollectionView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/26/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAutoScrollCollectionView.h"

@interface NITAutoScrollCollectionView ()

@property (nonatomic, strong) NSTimer * timer;

@end

@implementation NITAutoScrollCollectionView

- (instancetype)init
{
    if (self = [super init])
    {
        [self addGesture];
    }
    
    return self;
}

+ (instancetype)allocWithZone:(NSZone *)zone
{
    NITAutoScrollCollectionView * view = [super allocWithZone:zone];
    
    if (view)
    {
        [view addGesture];
    }
    
    return view;
}

- (void)dealloc
{
    [self stopTimer];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return true;
}

#pragma mark - Setter
- (void)setAutoScrollTimeInterval:(CGFloat)autoScrollTimeInterval
{
    _autoScrollTimeInterval = autoScrollTimeInterval;
    
    [self stopTimer];
    
    if (self.cellCount > 1 && autoScrollTimeInterval > 0)
    {
        [self setupTimer];
    }
}

- (void)setCellCount:(NSInteger)cellCount
{
    _cellCount = cellCount;
    
    if (cellCount > 1 && self.autoScrollTimeInterval > 0)
    {
        [self stopTimer];
        [self setupTimer];
    }
}

#pragma mark - Private
- (void) addGesture
{
    UISwipeGestureRecognizer * swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(stopAutoScroll)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer * swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(stopAutoScroll)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:swipeRight];
}

- (void)setupTimer
{
    NSTimer * timer = [NSTimer scheduledTimerWithTimeInterval:self.autoScrollTimeInterval target:self selector:@selector(automaticScroll) userInfo:nil repeats:true];
    self.timer = timer;
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer
{
    if (self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)automaticScroll
{
    if (0 == self.cellCount) return;
    
    UICollectionViewFlowLayout * flowLayout = (id)self.collectionViewLayout;
    int currentIndex = self.contentOffset.x / flowLayout.itemSize.width;
    int targetIndex = currentIndex + 1;
    
    if (targetIndex == self.cellCount)
    {
        targetIndex = 0;
    }
    
//    @try {
//        [self scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:targetIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:true];
//    }
//    @catch (NSException *exception) {
//        
//    }
}

- (void)stopAutoScroll
{
    if (self.stopAutoScrollAfterSwipe)
    {
        self.autoScrollTimeInterval = 0;
        [self stopTimer];
    }
}

@end
