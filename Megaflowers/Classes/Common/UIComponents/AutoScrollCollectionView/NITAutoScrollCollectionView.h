//
//  NITAutoScrollCollectionView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/26/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITAutoScrollCollectionView : UICollectionView

@property (nonatomic) BOOL stopAutoScrollAfterSwipe;
@property (nonatomic) CGFloat autoScrollTimeInterval;
@property (nonatomic) NSInteger cellCount;

@end
