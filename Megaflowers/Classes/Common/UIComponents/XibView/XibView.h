//
//  XibView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XibView : UIView

#pragma mark - Keyboard
@property (nonatomic) IBInspectable BOOL keyboardActiv;
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray * keyboardShiftConstraints;
@property (nonatomic, weak) IBOutlet UIScrollView * keyboardScrollViewBottomInset;

- (void)keyboardWasShown:(NSNotification *)aNotification;
- (void)keyboardWillBeHidden:(NSNotification *)aNotification;

@end
