//
//  XibView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "XibView.h"

@implementation XibView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
        [self viewDidLoad];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
        [self viewDidLoad];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Accessors
- (void)setKeyboardActiv:(BOOL)keyboardActiv
{
    if (_keyboardActiv != keyboardActiv)
    {
        _keyboardActiv = keyboardActiv;
        if (_keyboardActiv)
        {
            [self registerForKeyboardNotifications];
            [self addKeyboardHideTouch];
        }
        else
        {
            [self unregisterForKeyboardNotifications];
        }
    }
}

#pragma mark - Private
- (void)setup
{
    UIView * xibView = [[[NSBundle mainBundle] loadNibNamed:_s(self) owner:self options:nil] firstObject];
    xibView.frame = self.bounds;
    xibView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview: xibView];
}

- (void)viewDidLoad
{
    
}

#pragma mark - Keyboard
- (void)registerForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(keyboardWasShown:)
                   name:UIKeyboardWillShowNotification object:nil];
    
    [center addObserver:self
               selector:@selector(keyboardWillBeHidden:)
                   name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIViewAnimationCurve animationCurve;
    NSTimeInterval animationDuration;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    for (NSLayoutConstraint *c in self.keyboardShiftConstraints)
    {
        CGFloat devider = 1.f;
        c.constant = kbSize.height/devider;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self layoutIfNeeded];
    
    if (self.keyboardScrollViewBottomInset)
    {
        UIEdgeInsets insets = UIEdgeInsetsZero;
        insets.bottom += kbSize.height;
        self.keyboardScrollViewBottomInset.contentInset = insets;
    }
    
    [UIView commitAnimations];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    UIViewAnimationCurve animationCurve;
    NSTimeInterval animationDuration;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    for (NSLayoutConstraint *c in self.keyboardShiftConstraints)
    {
        CGFloat value = 0.f;
        c.constant = value;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self layoutIfNeeded];
    
    if (self.keyboardScrollViewBottomInset)
    {
        self.keyboardScrollViewBottomInset.contentInset = UIEdgeInsetsZero;
    }
    
    [UIView commitAnimations];
}

- (void)addKeyboardHideTouch
{
    UIGestureRecognizer * tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = false;
    
    [self addGestureRecognizer:tapper];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self endEditing:true];
}

@end
