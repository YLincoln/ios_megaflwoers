//
//  NITRateDeliveryProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryModel.h"

@protocol NITRateDeliveryInteractorOutputProtocol;
@protocol NITRateDeliveryInteractorInputProtocol;
@protocol NITRateDeliveryViewProtocol;
@protocol NITRateDeliveryPresenterProtocol;
@protocol NITRateDeliveryLocalDataManagerInputProtocol;
@protocol NITRateDeliveryAPIDataManagerInputProtocol;

@class NITRateDeliveryWireFrame;

@protocol NITRateDeliveryViewProtocol
@required
@property (nonatomic, strong) id <NITRateDeliveryPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITRateDeliveryModel *)model;
@end

@protocol NITRateDeliveryWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITRateDeliveryModuleFrom:(id)fromView orderId:(NSNumber *)orderId;
- (void)backFrom:(id)fromView;
- (void)showCatalogFrom:(id)fromView;
@end

@protocol NITRateDeliveryPresenterProtocol
@required
@property (nonatomic, weak) id <NITRateDeliveryViewProtocol> view;
@property (nonatomic, strong) id <NITRateDeliveryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRateDeliveryWireFrameProtocol> wireFrame;
@property (nonatomic) NSNumber * orderId;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)sendRate;
@end

@protocol NITRateDeliveryInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)rateSendedWithState:(BOOL)success error:(NSString *)error;
@end

@protocol NITRateDeliveryInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITRateDeliveryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRateDeliveryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRateDeliveryLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)sendRateByModel:(NITRateDeliveryModel *)model;
@end


@protocol NITRateDeliveryDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITRateDeliveryAPIDataManagerInputProtocol <NITRateDeliveryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)sendFeedBackByModel:(NITRateDeliveryModel *)model withHandler:(void(^)(NSError *error))handler;
@end

@protocol NITRateDeliveryLocalDataManagerInputProtocol <NITRateDeliveryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
