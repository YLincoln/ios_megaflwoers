//
//  NITRateDeliveryLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRateDeliveryProtocols.h"

@interface NITRateDeliveryLocalDataManager : NSObject <NITRateDeliveryLocalDataManagerInputProtocol>

@end
