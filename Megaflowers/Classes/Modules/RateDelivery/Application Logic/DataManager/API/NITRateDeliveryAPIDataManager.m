//
//  NITRateDeliveryAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryAPIDataManager.h"
#import "SWGReviewApi.h"

@implementation NITRateDeliveryAPIDataManager

- (void)sendFeedBackByModel:(NITRateDeliveryModel *)model withHandler:(void(^)(NSError *error))handler
{
    SWGReviewRequest * request = [SWGReviewRequest new];
    request.name = model.name;
    request.rate = @(model.rate);
    request.email = model.email;
    request.phone = model.phone;
    request.message = model.message;
    request.orderId = model.orderID;
    request.doNotDisturb = @(model.notDisturb);

    [HELPER startLoading];
    [[SWGReviewApi new] reviewPostWithSite:API.site lang:HELPER.lang review:request city:USER.cityID completionHandler:^(NSNumber *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(error);
    }];
}

@end
