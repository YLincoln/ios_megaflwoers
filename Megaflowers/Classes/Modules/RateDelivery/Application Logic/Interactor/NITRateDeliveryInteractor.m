//
//  NITRateDeliveryInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryInteractor.h"

@implementation NITRateDeliveryInteractor

- (void)sendRateByModel:(NITRateDeliveryModel *)model
{
    [self.APIDataManager sendFeedBackByModel:model withHandler:^(NSError *error) {
        
        if (error)
        {
            [self.presenter rateSendedWithState:false error:error.interfaceDescription];
        }
        else
        {
            [self.presenter rateSendedWithState:true error:nil];
        }
    }];
}

@end
