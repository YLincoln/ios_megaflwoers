//
//  NITRateDeliveryModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryModel.h"

@implementation NITRateDeliveryModel

- (instancetype)initWithOrderId:(NSNumber *)orderId
{
    if (self = [super init])
    {
        self.orderID = orderId;
    }
    
    return self;
}

@end
