//
//  NITRateDeliveryModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreateModel.h"

@interface NITRateDeliveryModel : NITFeedbackCreateModel

@property (nonatomic) NSNumber * orderID;
@property (nonatomic) CGFloat rate;
@property (nonatomic) BOOL notDisturb;

- (instancetype)initWithOrderId:(NSNumber *)orderId;

@end
