//
//  NITRateDeliveryInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRateDeliveryProtocols.h"

@interface NITRateDeliveryInteractor : NSObject <NITRateDeliveryInteractorInputProtocol>

@property (nonatomic, weak) id <NITRateDeliveryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRateDeliveryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRateDeliveryLocalDataManagerInputProtocol> localDataManager;

@end
