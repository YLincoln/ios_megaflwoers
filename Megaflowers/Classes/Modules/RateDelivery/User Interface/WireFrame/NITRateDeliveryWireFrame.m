//
//  NITRateDeliveryWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryWireFrame.h"

@implementation NITRateDeliveryWireFrame

+ (id)createNITRateDeliveryModule
{
    // Generating module components
    id <NITRateDeliveryViewProtocol> view = [[NITRateDeliveryViewController alloc] init];
    id <NITRateDeliveryPresenterProtocol, NITRateDeliveryInteractorOutputProtocol> presenter = [NITRateDeliveryPresenter new];
    id <NITRateDeliveryInteractorInputProtocol> interactor = [NITRateDeliveryInteractor new];
    id <NITRateDeliveryAPIDataManagerInputProtocol> APIDataManager = [NITRateDeliveryAPIDataManager new];
    id <NITRateDeliveryLocalDataManagerInputProtocol> localDataManager = [NITRateDeliveryLocalDataManager new];
    id <NITRateDeliveryWireFrameProtocol> wireFrame= [NITRateDeliveryWireFrame new];
    view = [(NITRateDeliveryWireFrame *)wireFrame createViewControllerWithKey:_s(NITRateDeliveryViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITRateDeliveryModuleFrom:(UIViewController *)fromViewController orderId:(NSNumber *)orderId
{
    NITRateDeliveryViewController * vc = [NITRateDeliveryWireFrame createNITRateDeliveryModule];
    vc.presenter.orderId = orderId;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)backFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

- (void)showCatalogFrom:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:0];
    [fromViewController.navigationController popViewControllerAnimated:false];
}

@end
