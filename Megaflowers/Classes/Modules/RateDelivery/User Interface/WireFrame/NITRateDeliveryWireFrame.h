//
//  NITRateDeliveryWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryProtocols.h"
#import "NITRateDeliveryViewController.h"
#import "NITRateDeliveryLocalDataManager.h"
#import "NITRateDeliveryAPIDataManager.h"
#import "NITRateDeliveryInteractor.h"
#import "NITRateDeliveryPresenter.h"
#import "NITRateDeliveryWireframe.h"
#import "NITRootWireframe.h"

@interface NITRateDeliveryWireFrame : NITRootWireframe <NITRateDeliveryWireFrameProtocol>

@end
