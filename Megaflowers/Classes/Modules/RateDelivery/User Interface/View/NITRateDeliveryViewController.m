//
//  NITRateDeliveryViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryViewController.h"
#import "AKNumericFormatter.h"

@interface NITRateDeliveryViewController ()
<
UITextFieldDelegate,
UITextViewDelegate
>

@property (nonatomic) IBOutlet HCSStarRatingView * rateView;
@property (nonatomic) IBOutlet UITextField * nameField;
@property (nonatomic) IBOutlet PhoneTextField * phoneField;
@property (nonatomic) IBOutlet UITextField * emailField;
@property (nonatomic) IBOutlet UITextView * messageTextView;
@property (nonatomic) IBOutlet UIButton * notDisturbBtn;
@property (nonatomic) IBOutlet UIButton * sendBtn;

@property (nonatomic) NITRateDeliveryModel * model;

@end

@implementation NITRateDeliveryViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)notDisturbChangeAction:(id)sender
{
    self.model.notDisturb = !self.model.notDisturb;
    [self updateNotDisturbState];
}

- (IBAction)sendAction:(id)sender
{
    [self.presenter sendRate];
}

- (IBAction)rateChanged:(id)sender
{
    self.model.rate = self.rateView.value;
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Оценить доставку", nil);
    
    [self setKeyboardActiv:true];
    
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.emailField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.messageTextView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Оценить доставку", nil)
                                                                                 attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    
    self.rateView.maximumValue = 5;
    self.rateView.minimumValue = 0;
    self.rateView.value = 0;
    self.rateView.allowsHalfStars = false;
    self.rateView.emptyStarImage = [UIImage imageNamed:@"gray_star"];
    self.rateView.filledStarImage = [UIImage imageNamed:@"gold_star"];
    [self.rateView addTarget:self action:@selector(rateChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)updateNotDisturbState
{
    [self.notDisturbBtn setImage:[UIImage imageNamed:self.model.notDisturb ? @"check" : @"check_ar"] forState:UIControlStateNormal];
}

- (void)updateModelByField:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 101: // name
            textField.text = [textField.text capitalizedString];
            self.model.name = textField.text;
            break;
            
        case 102: // email
            self.model.email = textField.text;
            break;
            
        case 103: // phone
            self.model.phone = textField.text;
            break;
            
        default:
            break;
    }
}

#pragma mark - NITRateDeliveryPresenterProtocol
- (void)reloadDataByModel:(NITRateDeliveryModel *)model
{
    self.model = model;
    
    self.nameField.textColor = self.model.nameHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.nameField.text = model.name;
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:
                                                                                            self.model.nameHighlight ?  RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.emailField.textColor = self.model.emailHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.emailField.text = self.model.email;
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.emailField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:
                                                                                             self.model.emailHighlight ?  RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.phoneField.textColor = self.model.phoneHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.phoneField.text = self.model.phone;
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:
                                                                                             self.model.phoneHighlight ?  RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.messageTextView.textColor = self.model.messageHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.messageTextView.text = self.model.message;
    self.messageTextView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Оценить доставку", nil)
                                                                                 attributes:@{NSForegroundColorAttributeName:
                                                                                                  self.model.messageHighlight ? RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    textField.textColor = RGB(10, 88, 43);
    [self updateModelByField:textField];
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateModelByField:textField];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    textView.textColor = RGB(10, 88, 43);
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.model.message = textView.text;
}

@end
