//
//  NITRateDeliveryViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITRateDeliveryProtocols.h"

@interface NITRateDeliveryViewController : NITBaseViewController <NITRateDeliveryViewProtocol>

@property (nonatomic, strong) id <NITRateDeliveryPresenterProtocol> presenter;

@end
