//
//  NITRateDeliveryPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRateDeliveryPresenter.h"
#import "NITRateDeliveryWireframe.h"

@interface NITRateDeliveryPresenter ()

@property (nonatomic) NITRateDeliveryModel * model;

@end

@implementation NITRateDeliveryPresenter

- (void)initData
{
    self.model = [[NITRateDeliveryModel alloc] initWithOrderId:self.orderId];
    [self.view reloadDataByModel:self.model];
}

- (void)sendRate
{
    if ([self.model isCorrect])
    {
        if (self.model.rate < 1)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ok", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:self.view
                                  withTitle:NSLocalizedString(@"Не указана оценка", nil)
                              actionButtons:nil
                               cancelButton:cancel];
        }
        else
        {
            [self.interactor sendRateByModel:self.model];
        }
    }
    else
    {
        [self.view reloadDataByModel:self.model];
    }
}

#pragma mark - NITFeedbackCreateInteractorOutputProtocol
- (void)rateSendedWithState:(BOOL)success error:(NSString *)error
{
    weaken(self);
    if (success)
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{
            [weakSelf.wireFrame backFrom:weakSelf.view];
        }];
        
        NITActionButton * action = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Перейти в каталог", nil) actionBlock:^{
            [weakSelf.wireFrame showCatalogFrom:weakSelf.view];
        }];
        
        [HELPER showActionSheetFromView:self.view
                              withTitle:NSLocalizedString(@"Спасибо за ваш отзыв!\nВы делаете нас лучше!", nil)
                          actionButtons:@[action]
                           cancelButton:cancel];
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];

        [HELPER showActionSheetFromView:self.view
                              withTitle:error
                          actionButtons:nil
                           cancelButton:cancel];
    }

}

@end
