//
//  NITRateDeliveryPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRateDeliveryProtocols.h"

@class NITRateDeliveryWireFrame;

@interface NITRateDeliveryPresenter : NITRootPresenter <NITRateDeliveryPresenterProtocol, NITRateDeliveryInteractorOutputProtocol>

@property (nonatomic, weak) id <NITRateDeliveryViewProtocol> view;
@property (nonatomic, strong) id <NITRateDeliveryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRateDeliveryWireFrameProtocol> wireFrame;
@property (nonatomic) NSNumber * orderId;

@end
