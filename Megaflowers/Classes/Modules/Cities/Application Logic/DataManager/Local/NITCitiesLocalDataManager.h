//
//  NITCitiesLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"

@interface NITCitiesLocalDataManager : NSObject <NITCitiesLocalDataManagerInputProtocol>

@end
