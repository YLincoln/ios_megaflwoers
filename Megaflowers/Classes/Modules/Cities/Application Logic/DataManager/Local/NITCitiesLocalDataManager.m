//
//  NITCitiesLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesLocalDataManager.h"
#import "NITCoupon.h"

@implementation NITCitiesLocalDataManager

- (NSArray <NITBusketBouquet *> *)allBouquets
{
    return [NITBusketBouquet allBouquets];
}

- (void)deleteBouquets
{
    [NITBusketBouquet deleteAllBouquets];
}

- (NSArray <NITBusketAttachment *> *)allAttachments
{
    return [NITBusketAttachment allAttachments];
}

- (void)deleteAttachments
{
    [NITBusketAttachment deleteAllAttachments];
}

- (void)deleteCoupon
{
    [NITCoupon deleteAllCoupons];
}

@end
