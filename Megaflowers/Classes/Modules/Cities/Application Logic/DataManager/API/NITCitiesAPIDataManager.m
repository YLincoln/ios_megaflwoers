//
//  NITCitiesAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesAPIDataManager.h"
#import "SWGCityApi.h"
#import "SWGCity.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITCitiesAPIDataManager

- (void)getCitiesWithHandler:(void (^)(NSArray<SWGCity*> *result))handler
{
    [HELPER startLoading];
    [[SWGCityApi new] cityGetWithLang:HELPER.lang site:API.site completionHandler:^(NSArray<SWGCity> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }

        handler(output);
    }];
}

@end
