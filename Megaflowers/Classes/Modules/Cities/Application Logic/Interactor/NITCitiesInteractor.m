//
//  NITCitiesInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesInteractor.h"
#import "NITCityCellModel.h"
#import "NSArray+BlocksKit.h"

@interface NITCitiesInteractor ()

@property (nonatomic) NSArray * data;

@end

@implementation NITCitiesInteractor

#pragma mark - Public
- (void)updateDataWithHandler:(void (^)(NSArray<NITCityCellModel *> *models))handler
{
    weaken(self);
    [self.APIDataManager getCitiesWithHandler:^(NSArray<SWGCity *> *result) {
        weakSelf.data = result;
        handler([NITCityCellModel modelsFromEntities:result]);
    }];
}

- (NSArray <NITCityCellModel *> *)updateDataBySearchString:(NSString *)searchString
{
    if (searchString && searchString.length > 0)
    {
        NSPredicate * resultPredicate = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", searchString];
        return [NITCityCellModel modelsFromEntities:[self.data filteredArrayUsingPredicate:resultPredicate]];
    }
    else
    {
        return [NITCityCellModel modelsFromEntities:self.data];
    }
}

- (BOOL)basketIsEmpty
{
    if ([self.localDataManager allBouquets].count > 0 ||
        [self.localDataManager allAttachments].count > 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

- (void)cleanBasket
{
    [self.localDataManager deleteCoupon];
    [self.localDataManager deleteBouquets];
    [self.localDataManager deleteAttachments];
}

@end
