//
//  NITCitiesInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"

@interface NITCitiesInteractor : NSObject <NITCitiesInteractorInputProtocol>

@property (nonatomic, weak) id <NITCitiesInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCitiesAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCitiesLocalDataManagerInputProtocol> localDataManager;

@end
