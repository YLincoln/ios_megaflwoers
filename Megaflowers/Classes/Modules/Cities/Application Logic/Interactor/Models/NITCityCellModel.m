//
//  NITCityCellModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCityCellModel.h"
#import "SWGCity.h"

@implementation NITCityCellModel

- (instancetype)initWithCity:(SWGCity *)city
{
    if (self = [super init])
    {
        self.identifier = city._id;
        self.title = city.name;
        self.selected = [city._id isEqualToNumber:USER.cityID];
        self.countryId = city.country._id;
        
        if (city.country && city.country.phone)
        {
            self.phone = city.country.phone;
        }
        else
        {
            self.phone = kHotlinePhone;
        }
        
        CLLocation * location;
        if (city.lat.length > 0 && city.lon.length > 0)
        {
            location = [[CLLocation alloc] initWithLatitude:[city.lat doubleValue] longitude:[city.lon doubleValue]];
        }
        
        self.location = location;
    }
    
    return self;
}

- (CGFloat)distance
{
    CLLocation * currentLocation = [LOCATION_MANAGER currentLocation];
    
    if (currentLocation && self.location)
    {
        CLLocationDistance meters = [currentLocation distanceFromLocation:self.location];
        return meters;
    }
    else
    {
        return 0;
    }
}

+ (NSArray <NITCityCellModel *> *)modelsFromEntities:(NSArray <SWGCity *> *)entities
{
    return [entities bk_map:^id(SWGCity * obj) {
        return [[NITCityCellModel alloc] initWithCity:obj];
    }];
}

@end
