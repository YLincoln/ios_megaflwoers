//
//  NITCityCellModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGCity;

@interface NITCityCellModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSNumber * countryId;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * phone;
@property (nonatomic) CLLocation * location;
@property (nonatomic) CGFloat distance;
@property (nonatomic) BOOL selected;

- (instancetype)initWithCity:(SWGCity *)city;
+ (NSArray <NITCityCellModel *> *)modelsFromEntities:(NSArray <SWGCity *> *)entities;

@end
