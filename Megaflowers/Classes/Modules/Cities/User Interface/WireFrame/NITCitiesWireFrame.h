//
//  NITCitiesWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"
#import "NITCitiesViewController.h"
#import "NITCitiesLocalDataManager.h"
#import "NITCitiesAPIDataManager.h"
#import "NITCitiesInteractor.h"
#import "NITCitiesPresenter.h"
#import "NITCitiesWireframe.h"
#import "NITRootWireframe.h"

@interface NITCitiesWireFrame : NITRootWireframe <NITCitiesWireFrameProtocol>

@property (nonatomic) SelectCityBlock block;

@end
