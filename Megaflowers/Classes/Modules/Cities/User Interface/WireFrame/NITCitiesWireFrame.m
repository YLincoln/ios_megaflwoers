//
//  NITCitiesWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesWireFrame.h"

@implementation NITCitiesWireFrame

+ (id)createNITCitiesModuleWithBlock:(SelectCityBlock)block
{
    // Generating module components
    id <NITCitiesPresenterProtocol, NITCitiesInteractorOutputProtocol> presenter = [NITCitiesPresenter new];
    id <NITCitiesInteractorInputProtocol> interactor = [NITCitiesInteractor new];
    id <NITCitiesAPIDataManagerInputProtocol> APIDataManager = [NITCitiesAPIDataManager new];
    id <NITCitiesLocalDataManagerInputProtocol> localDataManager = [NITCitiesLocalDataManager new];
    id <NITCitiesWireFrameProtocol> wireFrame= [NITCitiesWireFrame new];
    id <NITCitiesViewProtocol> view = [(NITCitiesWireFrame *)wireFrame createViewControllerWithKey:_s(NITCitiesViewController) storyboardType:StoryboardTypeGuide];;
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    wireFrame.block = block;
    
    return view;
}

+ (void)presentNITCitiesModuleFrom:(UIViewController *)fromViewController withType:(CityChangeType)type
{
    [NITCitiesWireFrame presentNITCitiesModuleFrom:fromViewController currentCityId:nil type:type andHandler:nil];
}

+ (void)presentNITCitiesModuleFrom:(UIViewController *)fromViewController currentCityId:(NSNumber *)currentCityId type:(CityChangeType)type andHandler:(SelectCityBlock)handler
{
    NITCitiesViewController * view = [NITCitiesWireFrame createNITCitiesModuleWithBlock:handler];
    view.presenter.changeType = type;
    view.presenter.currentCityId = currentCityId;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:view];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

- (void)hideView:(UIViewController *)viewController withCity:(NITCityCellModel *)city
{
    [viewController dismissViewControllerAnimated:true completion:^{
        
        if (self.block && city)
        {
            self.block(city);
        }
    }];
}

- (BOOL)isAlertViewShowing
{
    for (UIWindow * window in [UIApplication sharedApplication].windows)
    {
        NSArray * subviews = window.subviews;
        if ([subviews count] > 0)
        {
            for (id view in subviews)
            {
                if ([view isKindOfClass:[UIAlertView class]])
                {
                    return true;
                }
            }
        }
    }
    
    return false;
}

@end
