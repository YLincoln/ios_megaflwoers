//
//  NITCitiesViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITCitiesProtocols.h"

@interface NITCitiesViewController : NITBaseViewController <NITCitiesViewProtocol>

@property (nonatomic, strong) id <NITCitiesPresenterProtocol> presenter;

@end
