//
//  NITCitiesViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesViewController.h"
#import "NITCityTableViewCell.h"
#import "NITCityCellModel.h"

@interface NITCitiesViewController ()
<
UISearchBarDelegate,
UITableViewDelegate,
UITableViewDataSource,
TOSearchBarDelegate,
NITCityTableViewCellDelegate
>

@property (weak, nonatomic) IBOutlet TOSearchBar * searchBar;
@property (weak, nonatomic) IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet UIView * mainTitleView;
@property (weak, nonatomic) IBOutlet UIView * changeTitleView;
@property (weak, nonatomic) IBOutlet UILabel * changeTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * changeTitleW;

@property (nonatomic) NSArray <NITCityCellModel *> * items;

@end

@implementation NITCitiesViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter updateData];
}

#pragma mark - Private
- (void)initUI
{
    self.searchBar.barBackgroundTintColor = [UIColor whiteColor];
    self.searchBar.barBackgroundView.layer.cornerRadius = 14.0f;
    self.searchBar.barBackgroundView.layer.masksToBounds = true;
    self.searchBar.searchTextField.textColor = RGB(10, 88, 43);
    self.searchBar.delegate = self;
    
    [self setKeyboardActiv:true];
    
    [self.tableView setBackgroundColor:RGB(240, 245, 242)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCityTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCityTableViewCell)];
    
    switch (self.presenter.changeType) {
        case CityChangeTypeFirst:
            [self showMainBar];
            break;
        case CityChangeTypeGlobal:
            [self showChangeBarWithSearchBarPlaceholder:NSLocalizedString(@"  Город доставки", nil)];
            break;
        case CityChangeTypeLocal:
            [self showChangeBarWithSearchBarPlaceholder:NSLocalizedString(@"  Город", nil)];
            break;
    }
}

- (void)showMainBar
{
    self.mainTitleView.hidden = false;
    self.changeTitleView.hidden = true;
    
    self.searchBar.placeholderText = NSLocalizedString(@"  Город доставки", nil);
}

- (void)showChangeBarWithSearchBarPlaceholder:(NSString *)searchBarPlaceholder
{
    self.mainTitleView.hidden = true;
    self.changeTitleView.hidden = false;
        
    self.searchBar.placeholderText = searchBarPlaceholder;
    
    self.changeTitle.text = USER.cityName;
    self.changeTitleW.constant = MIN([self.changeTitle sizeOfMultiLineLabel].width, ViewWidth(WINDOW) * 0.57);
    
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Отмена", nil)
                                                              style:UIBarButtonItemStylePlain
                                                             target:self.presenter
                                                             action:@selector(hideView)];
    item.tintColor = RGB(10, 88, 43);
    self.navigationItem.leftBarButtonItem = item;
}

#pragma mark - NITCitiesPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITCityCellModel *> *)models andSelectedIndexPath:(NSIndexPath *)indexPath isSearch:(BOOL)isSearch
{
    self.items = models;
    [self.tableView reloadData];
    if (indexPath) [self.tableView selectRowAtIndexPath:indexPath
                                               animated:isSearch ? false : true
                                         scrollPosition:isSearch ? UITableViewScrollPositionNone : UITableViewScrollPositionMiddle];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCityTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCityTableViewCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - TOSearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.presenter updateDataBySearchString:searchBar.text];
}

#pragma mark - NITCityTableViewCellDelegate
- (void)didSelectCityCellModel:(NITCityCellModel *)model
{
    [self.presenter changeCity:model];
}

@end
