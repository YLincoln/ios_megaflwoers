//
//  NITCityTableViewCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCityCellModel.h"

@protocol NITCityTableViewCellDelegate <NSObject>
@optional;
- (void)didSelectCityCellModel:(NITCityCellModel *)model;
@end

@interface NITCityTableViewCell : UITableViewCell

@property (nonatomic) NITCityCellModel * model;
@property (nonatomic, weak) id <NITCityTableViewCellDelegate> delegate;

@end
