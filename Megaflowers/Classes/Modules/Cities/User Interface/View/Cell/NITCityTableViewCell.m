//
//  NITCityTableViewCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCityTableViewCell.h"

@interface NITCityTableViewCell ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet NSLayoutConstraint * titleW;
@property (nonatomic) IBOutlet UIImageView * check;

@end

@implementation NITCityTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.textLabel setTextColor:RGB(10, 88, 43)];
    [self setBackgroundColor:RGB(240, 245, 242)];
    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    selectedBackgroundView.backgroundColor = RGB(226, 235, 230);
    [self setSelectedBackgroundView:selectedBackgroundView];
    
    UIGestureRecognizer * tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAction)];
    tapper.cancelsTouchesInView = false;
    [self addGestureRecognizer:tapper];
}

- (void)setModel:(NITCityCellModel *)model
{
    _model = model;
    
    [self updateData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.model.selected = selected;
    [self updateCheckmark];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.titleW.constant = MIN([self.title sizeOfMultiLineLabel].width, ViewWidth(WINDOW) * 0.8);
    [self updateCheckmark];
}

- (void)updateCheckmark
{
    self.check.hidden = !self.model.selected;
}

- (void)selectAction
{
    self.model.selected = !self.model.selected;
    [self updateCheckmark];
    
    if ([self.delegate respondsToSelector:@selector(didSelectCityCellModel:)])
    {
        [self.delegate didSelectCityCellModel:self.model];
    }
}

@end
