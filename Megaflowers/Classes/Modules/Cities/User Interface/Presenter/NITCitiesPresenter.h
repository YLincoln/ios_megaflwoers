//
//  NITCitiesPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"

@class NITCitiesWireFrame;

@interface NITCitiesPresenter : NITRootPresenter <NITCitiesPresenterProtocol, NITCitiesInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCitiesViewProtocol> view;
@property (nonatomic, strong) id <NITCitiesInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCitiesWireFrameProtocol> wireFrame;
@property (nonatomic) CityChangeType changeType;
@property (nonatomic) NSNumber * currentCityId;

@end
