//
//  NITCitiesPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesPresenter.h"
#import "NITCitiesWireframe.h"
#import "NITCityCellModel.h"
#import "NSArray+BlocksKit.h"
#import "NITCityModel.h"
#import "AppDependencies.h"

@implementation NITCitiesPresenter

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - NITCitiesPresenterProtocol
- (void)updateData
{
    switch (self.changeType) {
        case CityChangeTypeFirst:
        case CityChangeTypeGlobal:
            self.currentCityId = USER.cityID;
            break;
        default:
            break;
    }
    
    weaken(self);
    [LOCATION_MANAGER updateLocationWithHandler:^{
        [weakSelf.interactor updateDataWithHandler:^(NSArray<NITCityCellModel *> * models) {
            [weakSelf updateDataByModels:models isSearch:false];
            [weakSelf checkCityByUserLocationIfNeedByModels:models];
        }];
    }];
}

- (void)updateDataBySearchString:(NSString *)searchString
{
    NSArray * models = [self.interactor updateDataBySearchString:searchString];
    [self updateDataByModels:models isSearch:true];
}

- (void)changeCity:(NITCityCellModel *)cityModel
{
    switch (self.changeType) {
        case CityChangeTypeFirst:
        case CityChangeTypeGlobal: {
            
            BOOL changed = [USER.cityID isEqualToNumber:cityModel.identifier] || [USER.countryID isEqualToNumber:cityModel.countryId];
            
            USER.cityName   = cityModel.title;
            USER.cityID     = cityModel.identifier;
            USER.cityPhone  = cityModel.phone;
            USER.countryID  = cityModel.countryId;
            
            if (changed)
            {
                [[NITCityModel new] clean];
                [[NSNotificationCenter defaultCenter] postNotificationName:kChangeCityNotification object:nil];
            }
            
        } break;
            
        default:
            break;
    }
    
    [self dismissWithCity:cityModel];
}

- (void)hideView
{
    [self.wireFrame hideView:self.view withCity:nil];
}

#pragma mark - Private
- (void)updateDataByModels:(NSArray<NITCityCellModel *> *)models isSearch:(BOOL)isSearch
{
    NSIndexPath * selectedIndexPath;
    
    if (models)
    {
        NITCityCellModel * model = [[models bk_select:^BOOL(NITCityCellModel *obj) {
            return [obj.identifier isEqualToNumber:self.currentCityId];
        }] firstObject];
        
        if (model)
        {
            selectedIndexPath = [NSIndexPath indexPathForRow:[models indexOfObject:model] inSection:0];
        }
    }
    
    [self.view reloadDataByModels:models andSelectedIndexPath:selectedIndexPath isSearch:isSearch];
}

- (void)checkCityByUserLocationIfNeedByModels:(NSArray<NITCityCellModel *> *)models
{
    if (([USER.cityID integerValue] < 1 || [USER.cityName length] == 0) && [LOCATION_MANAGER currentLocation])
    {
        NSMutableArray * sortedModels = [[models bk_select:^BOOL(NITCityCellModel *obj) {
            return obj.location != nil;
        }] mutableCopy];
        [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:keyPath(NITCityCellModel.distance) ascending:true]]];
        
        NITCityCellModel * city = sortedModels.firstObject;
        
        if (city)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
            
            weaken(self);
            NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Использовать", nil) actionBlock:^{
                [weakSelf changeCity:city];
            }];
            
            NSString * title = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"Использовать", nil), city.title, NSLocalizedString(@"как город доставки?", nil)];
            
            if ([self.wireFrame isAlertViewShowing])
            {
                weaken(self);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf checkCityByUserLocationIfNeedByModels:models];
                });
            }
            else
            {
                [HELPER showActionSheetFromView:WINDOW.visibleController withTitle:title actionButtons:@[action1] cancelButton:cancel];
            }
        }
    }
}

- (void)dismissWithCity:(NITCityCellModel *)cityModel
{
    weaken(self);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf.wireFrame hideView:weakSelf.view withCity:(cityModel)];
        
        if (![UserDefaults boolForKey:@"push_registered"])
        {
            [AppDependencies registerPushNotificationsWithOptions:nil];
            [UserDefaults setBool:true forKey:@"push_registered"];
        }
    });
}

@end
