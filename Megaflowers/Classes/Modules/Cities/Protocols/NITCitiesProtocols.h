//
//  NITCitiesProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"
#import "NITCityCellModel.h"

typedef CF_ENUM (NSUInteger, CityChangeType) {
    CityChangeTypeFirst     = 0,
    CityChangeTypeGlobal    = 1,
    CityChangeTypeLocal     = 2
};

typedef void (^SelectCityBlock) (NITCityCellModel * city);

@protocol NITCitiesInteractorOutputProtocol;
@protocol NITCitiesInteractorInputProtocol;
@protocol NITCitiesViewProtocol;
@protocol NITCitiesPresenterProtocol;
@protocol NITCitiesLocalDataManagerInputProtocol;
@protocol NITCitiesAPIDataManagerInputProtocol;

@class NITCitiesWireFrame, NITCityCellModel, SWGCity;

@protocol NITCitiesViewProtocol
@required
@property (nonatomic, strong) id <NITCitiesPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray<NITCityCellModel *> *)models andSelectedIndexPath:(NSIndexPath *)indexPath isSearch:(BOOL)isSearch;
@end

@protocol NITCitiesWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@property (nonatomic) SelectCityBlock block;
+ (void)presentNITCitiesModuleFrom:(id)fromView withType:(CityChangeType)type;
+ (void)presentNITCitiesModuleFrom:(id)fromView currentCityId:(NSNumber *)currentCityId type:(CityChangeType)type andHandler:(SelectCityBlock)handler;
- (void)hideView:(id)view withCity:(NITCityCellModel *)city;
- (BOOL)isAlertViewShowing;
@end

@protocol NITCitiesPresenterProtocol
@required
@property (nonatomic, weak) id <NITCitiesViewProtocol> view;
@property (nonatomic, strong) id <NITCitiesInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCitiesWireFrameProtocol> wireFrame;
@property (nonatomic) CityChangeType changeType;
@property (nonatomic) NSNumber * currentCityId;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)updateDataBySearchString:(NSString *)searchString;
- (void)changeCity:(NITCityCellModel *)cityModel;
- (void)hideView;
@end

@protocol NITCitiesInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITCitiesInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCitiesInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCitiesAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCitiesLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)updateDataWithHandler:(void (^)(NSArray<NITCityCellModel*> *models))handler;
- (NSArray <NITCityCellModel *> *)updateDataBySearchString:(NSString *)searchString;
- (BOOL)basketIsEmpty;
- (void)cleanBasket;
@end


@protocol NITCitiesDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCitiesAPIDataManagerInputProtocol <NITCitiesDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void) getCitiesWithHandler:(void (^)(NSArray<SWGCity*> *result))handler;
@end

@protocol NITCitiesLocalDataManagerInputProtocol <NITCitiesDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITBusketBouquet *> *)allBouquets;
- (NSArray <NITBusketAttachment *> *)allAttachments;
- (void)deleteBouquets;
- (void)deleteAttachments;
- (void)deleteCoupon;
@end
