//
//  NITTabBarWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITTabBarProtocols.h"
#import "NITTabBarViewController.h"
#import "NITTabBarLocalDataManager.h"
#import "NITTabBarAPIDataManager.h"
#import "NITTabBarInteractor.h"
#import "NITTabBarPresenter.h"
#import "NITTabBarWireframe.h"
#import "NITRootWireframe.h"

@interface NITTabBarWireFrame : NITRootWireframe <NITTabBarWireFrameProtocol>

@end
