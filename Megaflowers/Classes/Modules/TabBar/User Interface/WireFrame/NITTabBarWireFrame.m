//
//  NITTabBarWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarWireFrame.h"
#import "NITTabBarViewController.h"
#import "NITCatalogWireFrame.h"
#import "NITBasketWireFrame.h"
#import "NITProfileWireFrame.h"
#import "NITSalonsWireFrame.h"
#import "NITAppFeedbackWireFrame.h"

static NSString * const kCatalogNC  = @"NITCatalogNC";
static NSString * const kBasketNC   = @"NITBasketNC";
static NSString * const kProfileNC  = @"NITProfileNC";
static NSString * const kSalonsNC   = @"NITSalonsNC";

@implementation NITTabBarWireFrame

+ (id)createNITTabBarModule
{
    // Generating module components
    id <NITTabBarPresenterProtocol, NITTabBarInteractorOutputProtocol> presenter = [NITTabBarPresenter new];
    id <NITTabBarInteractorInputProtocol> interactor = [NITTabBarInteractor new];
    id <NITTabBarAPIDataManagerInputProtocol> APIDataManager = [NITTabBarAPIDataManager new];
    id <NITTabBarLocalDataManagerInputProtocol> localDataManager = [NITTabBarLocalDataManager new];
    id <NITTabBarWireFrameProtocol> wireFrame= [NITTabBarWireFrame new];
    id <NITTabBarViewProtocol> view = [(NITTabBarWireFrame *)wireFrame createViewControllerWithKey:_s(NITTabBarViewController) storyboardType:StoryboardTypeGuide];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}


+ (void)presentNITTabBarModuleFromWindow:(UIWindow *)window
{
    NITTabBarViewController * view = [NITTabBarWireFrame createNITTabBarModule];
    [window setRootViewController:view];
}

- (void)setupViewControllersForTabbar:(UITabBarController *)tabbar
{
    // catalog
    UINavigationController * catalogNC = [self createViewControllerWithKey:kCatalogNC storyboardType:StoryboardTypeCatalog];
    NITCatalogViewController * catalogVC = [NITCatalogWireFrame createNITCatalogModule];
    NSParameterAssert(catalogNC);
    NSParameterAssert(catalogVC);
    catalogNC.viewControllers = @[catalogVC];

    // basket
    UINavigationController * basketNC = [self createViewControllerWithKey:kBasketNC storyboardType:StoryboardTypeBasket];
    NITBasketViewController * basketVC = [NITBasketWireFrame createNITBasketModule];
    NSParameterAssert(basketNC);
    NSParameterAssert(basketVC);
    basketNC.viewControllers = @[basketVC];
    
    // account
    UINavigationController * accountNC = [self createViewControllerWithKey:kProfileNC storyboardType:StoryboardTypeProfile];
    NITProfileWireFrame * accountVC = [NITProfileWireFrame createNITProfileModule];
    NSParameterAssert(accountNC);
    NSParameterAssert(accountVC);
    accountNC.viewControllers = @[accountVC];
    
    // salons
    UINavigationController * salonsNC = [self createViewControllerWithKey:kSalonsNC storyboardType:StoryboardTypeSalons];
    NITSalonsWireFrame * salonsVC = [NITSalonsWireFrame createNITSalonsModule];
    NSParameterAssert(salonsNC);
    NSParameterAssert(salonsVC);
    salonsNC.viewControllers = @[salonsVC];
    
    tabbar.viewControllers = @[catalogNC, basketNC, accountNC, salonsNC];
}

- (void)showSendAppFeedbackFrom:(UIViewController *)fromViewcontroller
{
    [NITAppFeedbackWireFrame presentNITAppFeedbackModuleFrom:fromViewcontroller];
}

- (void)showTabByIndex:(NSInteger)index fromTabbar:(UITabBarController *)tabbar
{
    [tabbar setSelectedIndex:index];
    [WINDOW.visibleController.navigationController popToRootViewControllerAnimated:false];
}

@end
