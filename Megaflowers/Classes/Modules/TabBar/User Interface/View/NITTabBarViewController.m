//
//  NITTabBarViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarViewController.h"

@implementation NITTabBarViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.presenter setupViewControllersForTabbar:self];
    [self initButtons];
    [self.presenter updateData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.presenter rateAppIfNeed];
    
    if([Shortcut isCreatedShortcut])
    {
        [self setSelectedIndex: [self.presenter showControllerFrom3DTouch]];
    }
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:keyPath(self.selectedViewController) context:nil];
}

#pragma mark - Private
- (void)initButtons
{
    [(UITabBarItem *)self.tabBar.items[0] setImage:[UIImage imageNamed:@"catalog_gr"]];
    [(UITabBarItem *)self.tabBar.items[1] setImage:[UIImage imageNamed:@"tb_trash_gr"]];
    [(UITabBarItem *)self.tabBar.items[2] setImage:[UIImage imageNamed:@"tb_acc_gr"]];
    [(UITabBarItem *)self.tabBar.items[3] setImage:[UIImage imageNamed:@"tb_shop_gr"]];
    
    [(UITabBarItem *)self.tabBar.items[0] setSelectedImage:[UIImage imageNamed:@"catalog"]];
    [(UITabBarItem *)self.tabBar.items[1] setSelectedImage:[UIImage imageNamed:@"tb_trash"]];
    [(UITabBarItem *)self.tabBar.items[2] setSelectedImage:[UIImage imageNamed:@"tb_acc"]];
    [(UITabBarItem *)self.tabBar.items[3] setSelectedImage:[UIImage imageNamed:@"tb_shop"]];

    [(UITabBarItem *)self.tabBar.items[0] setTitle:NSLocalizedString(@"Каталог", @"")];
    [(UITabBarItem *)self.tabBar.items[1] setTitle:NSLocalizedString(@"Корзина", @"")];
    [(UITabBarItem *)self.tabBar.items[2] setTitle:NSLocalizedString(@"Аккаунт", @"")];
    [(UITabBarItem *)self.tabBar.items[3] setTitle:NSLocalizedString(@"Салоны", @"")];
    
    if ([(UITabBarItem *)self.tabBar.items[1] respondsToSelector:@selector(setBadgeColor:)])
    {
       [(UITabBarItem *)self.tabBar.items[1] setBadgeColor:RGB(10, 88, 43)];
    }

    [self addObserver:self forKeyPath:keyPath(self.selectedViewController) options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:keyPath(self.selectedViewController)])
    {
        UIViewController * new = change[@"new"];
        UIViewController * old = change[@"old"];
        
        if ([self.viewControllers indexOfObject:new] == 1 && ![new isEqual:old])
        {
            [API validateBasket];
        }
    }
}

#pragma mark - NITTabBarPresenterProtocol
- (void)reloadBascketCounter:(NSInteger)counter
{
    [(UITabBarItem *)self.tabBar.items[1] setBadgeValue:counter > 0 ? @(counter).stringValue : nil];
}

@end
