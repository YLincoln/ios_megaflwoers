//
//  NITTabBarViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITTabBarProtocols.h"

@interface NITTabBarViewController : UITabBarController <NITTabBarViewProtocol>

@property (nonatomic, strong) id <NITTabBarPresenterProtocol> presenter;

@end
