//
//  NITTabBarPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarPresenter.h"
#import "NITTabBarWireframe.h"
#import "NITCouponsAPIDataManager.h"
#import "NITCoupon.h"
#import "NITProductDetailsAPIDataManager.h"
#import "NITOrderDetailsAPIDataManager.h"
#import "NITOrderDetailsWireFrame.h"
#import "NITCatalogDirectoryWireFrame.h"
#import "NITOrderDetailsInteractor.h"
#import "NITOrderDetailsLocalDataManager.h"
#import "NITOrderDetailsAPIDataManager.h"

@implementation NITTabBarPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Data
- (void)updateData
{
    NSInteger counter = [self.interactor getBasketCounter];
    [self.view reloadBascketCounter:counter];
}

- (void)rateAppIfNeed
{
    if (USER.appRate == 0 && !USER.showRateAppDate)
    {
        USER.showRateAppDate = [NSDate date];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        NSUInteger interval = - [USER.showRateAppDate timeIntervalSinceNow];
        if (USER.appRate == 0 && interval > 60*60*24*2)
        {
            [self showRate1];
        }
    });
}

- (void)showRate1
{
    [HELPER showRateAppActionSheetWithTitle:NSLocalizedString(@"Пожалуйста, оцените\nприложение Megaflowers!", nil)
                               successBlock:^(CGFloat rateValue) {
                                   USER.appRate = rateValue;
                                   
                                   NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
                                   
                                   NITActionButton * action = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Перейти в Appstore", nil) actionBlock:^{
                                       [TRACKER trackEvent:TrackerEventRateAppInStore];
                                       [[UIApplication sharedApplication] openURL:APP_RATE_URL];
                                   }];
                                   
                                   [HELPER showActionSheetFromView:WINDOW.visibleController
                                                         withTitle:NSLocalizedString(@"Спасибо за вашу оценку!\nБудем также благодарны,\nесли вы оставите отзыв\nв Appstore", nil)
                                                     actionButtons:@[action]
                                                      cancelButton:cancel];
                               }
                                cancelBlock:^{
                                    USER.showRateAppDate = [NSDate date];
                                }];
}

- (void)showRate2
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{
        USER.showRateAppDate = [NSDate date];
    }];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Недоволен работой приложения", nil) actionBlock:^{
        USER.appRate = 1;
        [weakSelf.wireFrame showSendAppFeedbackFrom:WINDOW.visibleController];
    }];
    
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Мне все нравится", nil) actionBlock:^{
        USER.appRate = 5;
        [weakSelf.wireFrame showSendAppFeedbackFrom:WINDOW.visibleController];
    }];
    
    [HELPER showActionSheetFromView:WINDOW.visibleController
                          withTitle:NSLocalizedString(@"Пожалуйста, оцените\nприложение Megaflowers!", nil)
                      actionButtons:@[action1, action2]
                       cancelButton:cancel];
}

#pragma mark - Navigation
- (void)setupViewControllersForTabbar:(UITabBarController *)tabbar
{
    [self.wireFrame setupViewControllersForTabbar:tabbar];
    HELPER.interfaceLoaded = true;
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kStartBasketValidation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kStopBasketValidation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveCouponNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveBusketBouquetNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveBusketAttachmentNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePushNotification:) name:kNewPushNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handlePushNotification:(NSNotification *)notification
{
    NSDictionary * data = notification.object;
    NSInteger type = [data[@"type"] integerValue];
    
    switch (type) {
        // Брошенная корзина (добавлять в корзину купон или доп товар в подарок)
        case 1: {
            if (data[@"cupon"])
            {
                [[NITCouponsAPIDataManager new] addNewCoupon:data[@"cupon"] withHandler:^(NSError *error) {
                    [[NITCouponsAPIDataManager new] getUserCouponsWithHandler:^(NSArray<SWGUserCoupon> *result) {
                        NITCouponModel * coupon = [[NITCouponModel modelsFromEntities:result] bk_select:^BOOL(NITCouponModel *obj) {
                            return [obj.code isEqualToString:data[@"cupon"]];
                        }].firstObject;
                        
                        if (coupon)
                        {
                            [NITCoupon addToBusketCoupon:coupon];
                            [self.wireFrame showTabByIndex:1 fromTabbar:self.view];
                        }
                    }];
                }];
            }
            if (data[@"attach"])
            {
                [[NITProductDetailsAPIDataManager new] getAttachByID:data[@"attach"] withHandler:^(SWGAttach *output) {
                    
                    if (output)
                    {
                        NITCatalogItemViewModel * attachModel = [[NITCatalogItemViewModel alloc] initWithAttach:output];
                        NITBasketProductModel * item = [[NITBasketProductModel alloc] initWidthModel:attachModel skuIndex:0];
                        [NITBusketAttachment addAttachment:item];
                        [self.wireFrame showTabByIndex:1 fromTabbar:self.view];
                    }
                }];
            }
        } break;
        // Статусы заказа - необходимо переходить в заказ
        case 2: {
            if (data[@"order"])
            {
                [[NITOrderDetailsAPIDataManager new] getUserOrderByID:data[@"order"] withHandler:^(SWGOrder *result) {
                    
                    if (result)
                    {
                        NITMyOrderModel * order = [[NITMyOrderModel alloc] initWithOrder:result];
                        [self.wireFrame showTabByIndex:2 fromTabbar:self.view];
                        [NITOrderDetailsWireFrame presentNITOrderDetailsModuleFrom:WINDOW.visibleController withModel:order];
                    }
                }];
            }
        } break;
        // События общие (поместить букет в корзину и переход в нее)
        case 3: {
            if (data[@"bouqet"])
            {
                [[NITProductDetailsAPIDataManager new] getBouquetsByID:data[@"bouqet"] withHandler:^(SWGBouquet *output, NSError *error) {

                    if (output)
                    {
                        NITCatalogItemViewModel * attachModel = [[NITCatalogItemViewModel alloc] initWithBouquet:output];
                        NITBasketProductModel * item = [[NITBasketProductModel alloc] initWidthModel:attachModel skuIndex:0];
                        [NITBusketBouquet addBouquet:item];
                        [self.wireFrame showTabByIndex:1 fromTabbar:self.view];
                    }
                }];
            }
        } break;
        // События личные (поместить букет в корзину и переход в нее)
        case 4: {
            if (data[@"bouqet"])
            {
                [[NITProductDetailsAPIDataManager new] getBouquetsByID:data[@"bouqet"] withHandler:^(SWGBouquet *output, NSError *error) {
                    
                    if (output)
                    {
                        NITCatalogItemViewModel * attachModel = [[NITCatalogItemViewModel alloc] initWithBouquet:output];
                        NITBasketProductModel * item = [[NITBasketProductModel alloc] initWidthModel:attachModel skuIndex:0];
                        [NITBusketBouquet addBouquet:item];
                        [self.wireFrame showTabByIndex:1 fromTabbar:self.view];
                    }
                }];
            }
        } break;
        // Праздники Megaflowers (если указан, то открывать раздел с букетами. Если не указан, то переход на главный экран)
        case 5: {
            [self.wireFrame showTabByIndex:0 fromTabbar:self.view];
            if (data[@"section"])
            {
                [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:WINDOW.visibleController withTitle:@"" parentID:data[@"section"]];
            }
        } break;
        // Акции (переходить в эту акцию)
        case 6: {
            if (data[@"discount"])
            {
                [self.wireFrame showTabByIndex:0 fromTabbar:self.view];
                [NITCatalogDirectoryWireFrame presentNITStockDirectoryModuleFrom:WINDOW.visibleController withTitle:@"" discountId:data[@"discount"]];
            }
        } break;
        // Повторный заказ (создать корзину на основании переданного заказа)
        case 7: {
            if (data[@"order"])
            {
                [[NITOrderDetailsAPIDataManager new] getUserOrderByID:data[@"order"] withHandler:^(SWGOrder *result) {
                    
                    if (result)
                    {
                        NITMyOrderModel * order = [[NITMyOrderModel alloc] initWithOrder:result];
                        NITOrderDetailsInteractor * interactor = [NITOrderDetailsInteractor new];
                        interactor.APIDataManager = [NITOrderDetailsAPIDataManager new];
                        interactor.localDataManager = [NITOrderDetailsLocalDataManager new];
                        [interactor repeatOrder:order withHandler:^{
                            [self.wireFrame showTabByIndex:1 fromTabbar:self.view];
                        }];
                    }
                }];
            }
        } break;
            
        default:
            break;
    }
}

#pragma mark - 3D Touch
- (NSInteger)showControllerFrom3DTouch
{
    NSString * type = [UserDefaults objectForKey:kPathViewControllerKey];
    if ([type isEqualToString:@"Fast"])              return 0;
    if ([type isEqualToString:@"Catalog"])           return 0;
    if ([type isEqualToString:@"Salons"])            return 3;
    if ([type isEqualToString:@"Basket"])            return 1;
    if ([type isEqualToString:@"Status"])            return 2;
    if ([type isEqualToString:@"Room"])              return 2;
    if ([type isEqualToString:@"OrdersComplited"])   return 2;
    if ([type isEqualToString:@"OrderActive"])       return 2;
    if ([type isEqualToString:@"OrderPickup"])       return 2;
    if ([type isEqualToString:@"SalonsWay"])         return 3;
    if ([type isEqualToString:@"Share"])             return 0; //Create Share App
    return 0;
}

@end
