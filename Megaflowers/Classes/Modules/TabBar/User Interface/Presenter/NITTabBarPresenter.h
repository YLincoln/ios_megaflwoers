//
//  NITTabBarPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITTabBarProtocols.h"

@class NITTabBarWireFrame;

@interface NITTabBarPresenter : NITRootPresenter <NITTabBarPresenterProtocol, NITTabBarInteractorOutputProtocol>

@property (nonatomic, weak) id <NITTabBarViewProtocol> view;
@property (nonatomic, strong) id <NITTabBarInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITTabBarWireFrameProtocol> wireFrame;

@end
