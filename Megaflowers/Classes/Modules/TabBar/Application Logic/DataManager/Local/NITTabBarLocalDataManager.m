//
//  NITTabBarLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarLocalDataManager.h"

@implementation NITTabBarLocalDataManager

- (NSInteger)getBasketCounter
{
    NSInteger total = 0;

    NSArray * bouqets = [NITBusketBouquet allBouquets];
    for (NITBusketBouquet * bouqet in bouqets)
    {
        total += [bouqet.sku count];
    }
    
    NSArray * attachments = [NITBusketAttachment allAttachments];
    for (NITBusketAttachment * attachment in attachments)
    {
        total += [attachment.sku count];
    }
    
    return total;
}

@end
