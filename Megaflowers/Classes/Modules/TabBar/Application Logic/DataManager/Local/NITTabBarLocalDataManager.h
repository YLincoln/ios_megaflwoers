//
//  NITTabBarLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITTabBarProtocols.h"

@interface NITTabBarLocalDataManager : NSObject <NITTabBarLocalDataManagerInputProtocol>

@end
