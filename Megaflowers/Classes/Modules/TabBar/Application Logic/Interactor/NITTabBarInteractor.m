//
//  NITTabBarInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarInteractor.h"

@implementation NITTabBarInteractor

- (NSInteger)getBasketCounter
{
    return [self.localDataManager getBasketCounter];
}

@end
