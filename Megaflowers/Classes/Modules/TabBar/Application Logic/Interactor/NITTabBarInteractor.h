//
//  NITTabBarInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITTabBarProtocols.h"

@interface NITTabBarInteractor : NSObject <NITTabBarInteractorInputProtocol>

@property (nonatomic, weak) id <NITTabBarInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITTabBarAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITTabBarLocalDataManagerInputProtocol> localDataManager;

@end
