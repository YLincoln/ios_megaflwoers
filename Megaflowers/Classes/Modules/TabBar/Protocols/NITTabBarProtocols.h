//
//  NITTabBarProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"

@protocol NITTabBarInteractorOutputProtocol;
@protocol NITTabBarInteractorInputProtocol;
@protocol NITTabBarViewProtocol;
@protocol NITTabBarPresenterProtocol;
@protocol NITTabBarLocalDataManagerInputProtocol;
@protocol NITTabBarAPIDataManagerInputProtocol;

@class NITTabBarWireFrame;

@protocol NITTabBarViewProtocol
@required
@property (nonatomic, strong) id <NITTabBarPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadBascketCounter:(NSInteger)counter;
@end

@protocol NITTabBarWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITTabBarModule;
+ (void)presentNITTabBarModuleFromWindow:(UIWindow *)window;
- (void)setupViewControllersForTabbar:(UITabBarController *)tabbar;
- (void)showSendAppFeedbackFrom:(id )fromView;
- (void)showTabByIndex:(NSInteger)index fromTabbar:(id)tabbar;
@end

@protocol NITTabBarPresenterProtocol
@required
@property (nonatomic, weak) id <NITTabBarViewProtocol> view;
@property (nonatomic, strong) id <NITTabBarInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITTabBarWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)setupViewControllersForTabbar:(UITabBarController *)tabbar;
- (void)updateData;
- (void)rateAppIfNeed;
- (NSInteger)showControllerFrom3DTouch;
@end

@protocol NITTabBarInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITTabBarInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITTabBarInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITTabBarAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITTabBarLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (NSInteger)getBasketCounter;
@end


@protocol NITTabBarDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITTabBarAPIDataManagerInputProtocol <NITTabBarDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITTabBarLocalDataManagerInputProtocol <NITTabBarDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSInteger)getBasketCounter;
@end
