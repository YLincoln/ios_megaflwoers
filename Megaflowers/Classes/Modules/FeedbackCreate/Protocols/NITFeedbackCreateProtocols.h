//
//  NITFeedbackCreateProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreateModel.h"
#import "SWGFeedbackRequest.h"

@protocol NITFeedbackCreateInteractorOutputProtocol;
@protocol NITFeedbackCreateInteractorInputProtocol;
@protocol NITFeedbackCreateViewProtocol;
@protocol NITFeedbackCreatePresenterProtocol;
@protocol NITFeedbackCreateLocalDataManagerInputProtocol;
@protocol NITFeedbackCreateAPIDataManagerInputProtocol;

@class NITFeedbackCreateWireFrame;

@protocol NITFeedbackCreateViewProtocol
@required
@property (nonatomic, strong) id <NITFeedbackCreatePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITFeedbackCreateModel *)model;
@end

@protocol NITFeedbackCreateWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITFeedbackCreateModuleFrom:(id)fromView;
- (void)backFrom:(id)fromView;
@end

@protocol NITFeedbackCreatePresenterProtocol
@required
@property (nonatomic, weak) id <NITFeedbackCreateViewProtocol> view;
@property (nonatomic, strong) id <NITFeedbackCreateInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFeedbackCreateWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)sendAction;
@end

@protocol NITFeedbackCreateInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)feedbackSended;
- (void)feedbackSendError:(NSString *)error;
@end

@protocol NITFeedbackCreateInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITFeedbackCreateInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFeedbackCreateAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFeedbackCreateLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)sendFeedbackByModel:(NITFeedbackCreateModel *)model;
@end


@protocol NITFeedbackCreateDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITFeedbackCreateAPIDataManagerInputProtocol <NITFeedbackCreateDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)sendFeedback:(SWGFeedbackRequest *)feedback withHandler:(void(^)(NSError *error))handler;
@end

@protocol NITFeedbackCreateLocalDataManagerInputProtocol <NITFeedbackCreateDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
