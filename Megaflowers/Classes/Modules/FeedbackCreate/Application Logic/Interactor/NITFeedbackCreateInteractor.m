//
//  NITFeedbackCreateInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreateInteractor.h"

@implementation NITFeedbackCreateInteractor

- (void)sendFeedbackByModel:(NITFeedbackCreateModel *)model
{
    SWGFeedbackRequest * feedback = [SWGFeedbackRequest new];
    
    feedback.userId     = @(USER.identifier.integerValue);
    feedback.name       = model.name;
    feedback.email      = model.email;
    feedback.phone      = model.phone;
    feedback.message    = model.message;
    
    weaken(self);
    [self.APIDataManager sendFeedback:feedback withHandler:^(NSError *error) {
        
        if (error)
        {
            [weakSelf.presenter feedbackSendError:error.interfaceDescription];
        }
        else
        {
            [weakSelf.presenter feedbackSended];
        }
    }];
}

@end
