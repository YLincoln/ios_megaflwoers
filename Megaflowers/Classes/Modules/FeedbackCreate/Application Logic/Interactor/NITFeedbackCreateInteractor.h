//
//  NITFeedbackCreateInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackCreateProtocols.h"

@interface NITFeedbackCreateInteractor : NSObject <NITFeedbackCreateInteractorInputProtocol>

@property (nonatomic, weak) id <NITFeedbackCreateInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFeedbackCreateAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFeedbackCreateLocalDataManagerInputProtocol> localDataManager;

@end
