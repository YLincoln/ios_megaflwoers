//
//  NITFeedbackCreateModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreateModel.h"

static NSString * const kFeedbackName       = @"kFeedbackName";
static NSString * const kFeedbackPhone      = @"kFeedbackPhone";
static NSString * const kFeedbackEmail      = @"kFeedbackEmail";
static NSString * const kFeedbackMessage    = @"kFeedbackMessage";

@implementation NITFeedbackCreateModel

#pragma mark - Custom Accessors
- (NSString *)name
{
    return [self CriptoString:kFeedbackName];
}

- (void)setName:(NSString *)name
{
    [self setCriptoString:kFeedbackName value:name];
}

- (NSString *)phone
{
    return [self CriptoString:kFeedbackPhone];
}

- (void)setPhone:(NSString *)phone
{
    [self setCriptoString:kFeedbackPhone value:phone];
}

- (NSString *)email
{
    return [self CriptoString:kFeedbackEmail];
}

- (void)setEmail:(NSString *)email
{
    [self setCriptoString:kFeedbackEmail value:email];
}

#pragma mark - Public
- (BOOL)isCorrect
{
    BOOL correct = true;
    
    self.nameHighlight      = false;
    self.phoneHighlight     = false;
    self.messageHighlight   = false;
    self.emailHighlight     = false;
    
    if ([self.name length] == 0)
    {
        self.nameHighlight = true;
        correct = false;
    }

    if ([self.message length] == 0)
    {
        self.messageHighlight = true;
        correct = false;
    }
    
    if ([self.phone length] == 0 && ![self isValidEmail])
    {
        self.phoneHighlight = true;
        self.emailHighlight = true;
        correct = false;
    }

    return correct;
}


#pragma mark - Private
- (BOOL)isValidEmail
{
    if (self.email.length == 0)
    {
        return false;
    }
    
    NSString * emailid = self.email;
    NSString * emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate * emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailid];
}


@end
