//
//  NITFeedbackCreateModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITStorageManager.h"

@interface NITFeedbackCreateModel : NITStorageManager

@property (nonatomic) NSString * name;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSString * email;
@property (nonatomic) NSString * message;

@property (nonatomic) BOOL nameHighlight;
@property (nonatomic) BOOL phoneHighlight;
@property (nonatomic) BOOL emailHighlight;
@property (nonatomic) BOOL messageHighlight;

- (BOOL)isCorrect;

@end
