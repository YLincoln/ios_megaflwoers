//
//  NITFeedbackCreateWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreateWireFrame.h"

@implementation NITFeedbackCreateWireFrame

+ (id)createNITFeedbackCreateModule
{
    // Generating module components
    id <NITFeedbackCreatePresenterProtocol, NITFeedbackCreateInteractorOutputProtocol> presenter = [NITFeedbackCreatePresenter new];
    id <NITFeedbackCreateInteractorInputProtocol> interactor = [NITFeedbackCreateInteractor new];
    id <NITFeedbackCreateAPIDataManagerInputProtocol> APIDataManager = [NITFeedbackCreateAPIDataManager new];
    id <NITFeedbackCreateLocalDataManagerInputProtocol> localDataManager = [NITFeedbackCreateLocalDataManager new];
    id <NITFeedbackCreateWireFrameProtocol> wireFrame= [NITFeedbackCreateWireFrame new];
    id <NITFeedbackCreateViewProtocol> view = [(NITFeedbackCreateWireFrame *)wireFrame createViewControllerWithKey:_s(NITFeedbackCreateViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITFeedbackCreateModuleFrom:(UIViewController *)fromViewController
{
    NITFeedbackCreateViewController * vc = [NITFeedbackCreateWireFrame createNITFeedbackCreateModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)backFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
