//
//  NITFeedbackCreateWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreateProtocols.h"
#import "NITFeedbackCreateViewController.h"
#import "NITFeedbackCreateLocalDataManager.h"
#import "NITFeedbackCreateAPIDataManager.h"
#import "NITFeedbackCreateInteractor.h"
#import "NITFeedbackCreatePresenter.h"
#import "NITFeedbackCreateWireframe.h"
#import "NITRootWireframe.h"

@interface NITFeedbackCreateWireFrame : NITRootWireframe <NITFeedbackCreateWireFrameProtocol>

@end
