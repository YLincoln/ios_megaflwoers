//
//  NITFeedbackCreatePresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackCreateProtocols.h"

@class NITFeedbackCreateWireFrame;

@interface NITFeedbackCreatePresenter : NITRootPresenter <NITFeedbackCreatePresenterProtocol, NITFeedbackCreateInteractorOutputProtocol>

@property (nonatomic, weak) id <NITFeedbackCreateViewProtocol> view;
@property (nonatomic, strong) id <NITFeedbackCreateInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFeedbackCreateWireFrameProtocol> wireFrame;

@end
