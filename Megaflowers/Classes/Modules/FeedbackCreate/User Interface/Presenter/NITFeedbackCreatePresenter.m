//
//  NITFeedbackCreatePresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreatePresenter.h"
#import "NITFeedbackCreateWireframe.h"

@interface NITFeedbackCreatePresenter ()

@property (nonatomic) NITFeedbackCreateModel * model;

@end

@implementation NITFeedbackCreatePresenter

- (void)initData
{
    self.model = [NITFeedbackCreateModel new];
    if([USER isAutorize])
    {
        self.model.name = USER.name;
        self.model.phone = USER.phone;
        self.model.email = USER.email;
    }
    [self.view reloadDataByModel:self.model];
}

- (void)sendAction
{
    if ([self.model isCorrect])
    {
        [self.interactor sendFeedbackByModel:self.model];
    }
    else
    {
        [self.view reloadDataByModel:self.model];
    }
}

#pragma mark - NITFeedbackCreateInteractorOutputProtocol
- (void)feedbackSended
{
    [TRACKER trackEvent:TrackerEventSendFeedback];

    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{
        [self.wireFrame backFrom:self.view];
    }];
    
    [HELPER showActionSheetFromView:self.view
                          withTitle:NSLocalizedString(@"Спасибо за ваш отзыв!\nВы делаете нас лучше!", nil)
                      actionButtons:@[]
                       cancelButton:cancel];
}

- (void)feedbackSendError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
    
    [HELPER showActionSheetFromView:self.view
                          withTitle:error
                      actionButtons:@[]
                       cancelButton:cancel];
}


@end
