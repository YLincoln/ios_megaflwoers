//
//  NITUserAgreementProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SWGOffer.h"

@protocol NITUserAgreementInteractorOutputProtocol;
@protocol NITUserAgreementInteractorInputProtocol;
@protocol NITUserAgreementViewProtocol;
@protocol NITUserAgreementPresenterProtocol;
@protocol NITUserAgreementLocalDataManagerInputProtocol;
@protocol NITUserAgreementAPIDataManagerInputProtocol;

@class NITUserAgreementWireFrame;

@protocol NITUserAgreementDelegate <NSObject>
- (void)didAcceptAgreement;
@end

@protocol NITUserAgreementViewProtocol
@required
@property (nonatomic, strong) id <NITUserAgreementPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByHtmlString:(NSString *)htmlString;
@end

@protocol NITUserAgreementWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITUserAgreementModuleFrom:(id)fromViewController withDelegate:(id)delegate;
- (void)backActionFrom:(id)fromView;
@end

@protocol NITUserAgreementPresenterProtocol
@required
@property (nonatomic, weak) id <NITUserAgreementViewProtocol> view;
@property (nonatomic, strong) id <NITUserAgreementInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITUserAgreementWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITUserAgreementDelegate> delegate;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)acceptAgreement;
@end

@protocol NITUserAgreementInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDataByHtmlString:(NSString *)htmlString;
@end

@protocol NITUserAgreementInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITUserAgreementInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITUserAgreementAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITUserAgreementLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getUserAgreement;
@end


@protocol NITUserAgreementDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITUserAgreementAPIDataManagerInputProtocol <NITUserAgreementDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserAgreementWithHandler:(void(^)(SWGOffer * output))handler;
@end

@protocol NITUserAgreementLocalDataManagerInputProtocol <NITUserAgreementDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
