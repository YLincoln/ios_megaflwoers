//
//  NITUserAgreementWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITUserAgreementProtocols.h"
#import "NITUserAgreementViewController.h"
#import "NITUserAgreementLocalDataManager.h"
#import "NITUserAgreementAPIDataManager.h"
#import "NITUserAgreementInteractor.h"
#import "NITUserAgreementPresenter.h"
#import "NITUserAgreementWireframe.h"
#import "NITRootWireframe.h"

@interface NITUserAgreementWireFrame : NITRootWireframe <NITUserAgreementWireFrameProtocol>

@end
