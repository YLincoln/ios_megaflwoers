//
//  NITUserAgreementWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITUserAgreementWireFrame.h"

@implementation NITUserAgreementWireFrame

+ (id)createNITUserAgreementModule
{
    // Generating module components
    id <NITUserAgreementPresenterProtocol, NITUserAgreementInteractorOutputProtocol> presenter = [NITUserAgreementPresenter new];
    id <NITUserAgreementInteractorInputProtocol> interactor = [NITUserAgreementInteractor new];
    id <NITUserAgreementAPIDataManagerInputProtocol> APIDataManager = [NITUserAgreementAPIDataManager new];
    id <NITUserAgreementLocalDataManagerInputProtocol> localDataManager = [NITUserAgreementLocalDataManager new];
    id <NITUserAgreementWireFrameProtocol> wireFrame= [NITUserAgreementWireFrame new];
    id <NITUserAgreementViewProtocol> view = [(NITUserAgreementWireFrame *)wireFrame createViewControllerWithKey:_s(NITUserAgreementViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITUserAgreementModuleFrom:(UIViewController *)fromViewController withDelegate:(id)delegate
{
    NITUserAgreementViewController * view = [NITUserAgreementWireFrame createNITUserAgreementModule];
    view.presenter.delegate = delegate;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromView
{
    [fromView.navigationController popViewControllerAnimated:true];
}

@end
