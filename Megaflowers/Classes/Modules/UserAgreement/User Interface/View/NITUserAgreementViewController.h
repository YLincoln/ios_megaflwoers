//
//  NITUserAgreementViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITUserAgreementProtocols.h"

@interface NITUserAgreementViewController : NITBaseViewController <NITUserAgreementViewProtocol>

@property (nonatomic, strong) id <NITUserAgreementPresenterProtocol> presenter;

@end
