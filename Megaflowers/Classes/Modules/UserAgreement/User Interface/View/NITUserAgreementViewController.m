//
//  NITUserAgreementViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITUserAgreementViewController.h"

@interface NITUserAgreementViewController ()
<
UIWebViewDelegate
>

@property (nonatomic) IBOutlet UIWebView * webView;

@end

@implementation NITUserAgreementViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)acceptAction:(id)sender
{
    [self.presenter acceptAgreement];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Пользовательское соглашение", nil);
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.fontFamily =\"-apple-system\""];
}

#pragma mark - NITUserAgreementPresenterProtocol
- (void)reloadDataByHtmlString:(NSString *)htmlString
{
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

@end
