//
//  NITUserAgreementPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITUserAgreementProtocols.h"

@class NITUserAgreementWireFrame;

@interface NITUserAgreementPresenter : NITRootPresenter <NITUserAgreementPresenterProtocol, NITUserAgreementInteractorOutputProtocol>

@property (nonatomic, weak) id <NITUserAgreementViewProtocol> view;
@property (nonatomic, strong) id <NITUserAgreementInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITUserAgreementWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITUserAgreementDelegate> delegate;

@end
