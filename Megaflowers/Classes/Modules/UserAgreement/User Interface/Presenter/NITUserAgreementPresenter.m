//
//  NITUserAgreementPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITUserAgreementPresenter.h"
#import "NITUserAgreementWireframe.h"

@implementation NITUserAgreementPresenter

- (void)initData
{
    [self.interactor getUserAgreement];
}

- (void)acceptAgreement
{
    if ([self.delegate respondsToSelector:@selector(didAcceptAgreement)])
    {
        [self.delegate didAcceptAgreement];
    }
    
    [self.wireFrame backActionFrom:self.view];
}

#pragma mark - NITUserAgreementInteractorOutputProtocol
- (void)updateDataByHtmlString:(NSString *)htmlString
{
    [self.view reloadDataByHtmlString:htmlString];
}

@end
