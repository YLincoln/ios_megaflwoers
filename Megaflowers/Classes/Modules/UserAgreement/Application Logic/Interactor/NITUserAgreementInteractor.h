//
//  NITUserAgreementInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITUserAgreementProtocols.h"

@interface NITUserAgreementInteractor : NSObject <NITUserAgreementInteractorInputProtocol>

@property (nonatomic, weak) id <NITUserAgreementInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITUserAgreementAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITUserAgreementLocalDataManagerInputProtocol> localDataManager;

@end
