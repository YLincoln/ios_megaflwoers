//
//  NITUserAgreementInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITUserAgreementInteractor.h"

@implementation NITUserAgreementInteractor

- (void)getUserAgreement
{
    weaken(self);
    [self.APIDataManager getUserAgreementWithHandler:^(SWGOffer *output) {
        [weakSelf.presenter updateDataByHtmlString:output.text];
    }];
}

@end
