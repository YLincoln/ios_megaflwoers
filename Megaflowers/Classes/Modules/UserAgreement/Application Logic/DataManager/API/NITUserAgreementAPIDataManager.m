//
//  NITUserAgreementAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITUserAgreementAPIDataManager.h"
#import "SWGUserApi.h"

@implementation NITUserAgreementAPIDataManager

- (void)getUserAgreementWithHandler:(void(^)(SWGOffer * output))handler
{
    [HELPER stopLoading];
    [[SWGUserApi new] offerGetWithLang:HELPER.lang site:API.site completionHandler:^(SWGOffer *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:THIS_METHOD];
        }
        handler(output);
    }];
}

@end
