//
//  NITUserAgreementAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITUserAgreementProtocols.h"

@interface NITUserAgreementAPIDataManager : NSObject <NITUserAgreementAPIDataManagerInputProtocol>

@end
