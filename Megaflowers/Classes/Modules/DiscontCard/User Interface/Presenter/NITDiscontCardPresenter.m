//
//  NITDiscontCardPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardPresenter.h"
#import "NITDiscontCardWireframe.h"
#import "NITRegistrationCardProtocols.h"
#import "RSCodeGen.h"

@interface NITDiscontCardPresenter ()
<
NITRegistrationCardDelegate
>

@end

@implementation NITDiscontCardPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self initData];
}

#pragma mark - NITDiscontCardPresenterProtocol
- (void)initData
{
    [self.interactor updateCouponsData];
    [self.interactor updateCurrentDiscount];
}

- (void)attachCardByCode:(NSString *)code
{
    NSString * codeValue = [code stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (codeValue.length == 13)
    {
        [self.interactor attachCardByCode:codeValue];
    }
    else
    {
        [self.view showErrorDiscontCardEnter];
    }
}

- (void)addNewCoupon:(NSString *)code
{
    [self.interactor addNewCoupon:code];
}

- (void)scanNewCoupon
{
    weaken(self);
    [self.wireFrame showScanCouponFrom:self.view withHandler:^(NSString *code) {
        [weakSelf addNewCoupon:code];
        [TRACKER trackEvent:TrackerEventDiscontCardScan];
    }];
}

- (void)scanCode
{
    weaken(self);
    [self.wireFrame showScanCodeFrom:self.view withHandler:^(NSString *code) {
        [weakSelf.view reloadCodeFieldByCode:code];
    }];
}

- (void)createCard
{
    [self.wireFrame showCreateCardFrom:self.view withDelegate:self];
}

- (void)addCouponToBasket:(NITCouponModel *)coupon
{
    if ([coupon isKindOfClass:[NITCouponModel class]])
    {
        [self.interactor addCouponToBasket:coupon];
    }
}

#pragma mark - NITDiscontCardInteractorOutputProtocol
// Dicount card
- (void)updateDiscountModels:(NSArray<NITDiscontCardModel *> *)models
{
    if (models.count > 0)
    {
        NSString * code = [USER.discount code];
        UIImage * image = [CodeGen genCodeWithContents:code machineReadableCodeObjectType:AVMetadataObjectTypeEAN13Code];
        [self.view reloadDiscontViewByType:DiscontViewTypeView];
        [self.view reloadDiscontDataByModels:models barcodeImage:image code:code];
    }
    else
    {
        [self.view reloadDiscontViewByType:DiscontViewTypeCreate];
        [self.view showErrorDiscontCardEnter];
    }
}

- (void)discountCardAttachedSuccess
{
    [TRACKER trackEvent:TrackerEventDiscontCardLinked];
    [self.interactor updateCurrentDiscount];
}

- (void)discountCardAttachError:(NSString *)error
{
    [self.view showErrorDiscontCardEnter];
    [self showError:error];
}

// Coupons
- (void)updateCouponModels:(NSArray<NITCouponModel *> *)models
{
    NSArray * sections = @[
                           [[NITCouponSection alloc] initWithType:CouponSectionTypeAdd models:@[@true]],
                           [[NITCouponSection alloc] initWithType:CouponSectionTypeView models:models],
                           [[NITCouponSection alloc] initWithType:CouponSectionTypeInfo models:nil /*USER.birthday ? nil : @[@true]*/]
                           ];
    
    [self.view reloadCouponsBySectionModels:sections];
}

- (void)couponAddedSuccess
{
    [self.view reloadAddCouponSection];
    [self.interactor updateCouponsData];
}

- (void)couponAddError:(NSString *)error
{
    [self showError:error];
}

#pragma mark - NITRegistrationCardDelegate
- (void)didRegisterDiscontCard
{
    [self.interactor updateCurrentDiscount];
}

#pragma mark - Private
- (void)showError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"OK", nil) actionBlock:^{}];
    [HELPER showActionSheetFromView:self.view withTitle:error actionButtons:nil cancelButton:cancel];
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initData) name:kSaveCouponNotification object:nil];
}

@end
