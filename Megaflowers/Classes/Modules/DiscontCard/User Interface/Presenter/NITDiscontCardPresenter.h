//
//  NITDiscontCardPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITDiscontCardProtocols.h"

@class NITDiscontCardWireFrame;

@interface NITDiscontCardPresenter : NITRootPresenter <NITDiscontCardPresenterProtocol, NITDiscontCardInteractorOutputProtocol>

@property (nonatomic, weak) id <NITDiscontCardViewProtocol> view;
@property (nonatomic, strong) id <NITDiscontCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITDiscontCardWireFrameProtocol> wireFrame;
@property (nonatomic) DiscontSectionType sectionType;
@property (nonatomic) DiscontViewType discontViewType;

@end
