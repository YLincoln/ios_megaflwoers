//
//  NITDiscontCardWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardWireFrame.h"
#import "NITBarcodeScannerWireFrame.h"
#import "NITRegistrationCardWireFrame.h"

@implementation NITDiscontCardWireFrame

+ (id)createNITDiscontCardModule
{
    // Generating module components
    id <NITDiscontCardPresenterProtocol, NITDiscontCardInteractorOutputProtocol> presenter = [NITDiscontCardPresenter new];
    id <NITDiscontCardInteractorInputProtocol> interactor = [NITDiscontCardInteractor new];
    id <NITDiscontCardAPIDataManagerInputProtocol> APIDataManager = [NITDiscontCardAPIDataManager new];
    id <NITDiscontCardLocalDataManagerInputProtocol> localDataManager = [NITDiscontCardLocalDataManager new];
    id <NITDiscontCardWireFrameProtocol> wireFrame= [NITDiscontCardWireFrame new];
    id <NITDiscontCardViewProtocol> view = [(NITDiscontCardWireFrame *)wireFrame createViewControllerWithKey:_s(NITDiscontCardViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITDiscontCardModuleFrom:(UIViewController *)fromViewController
{
    NITDiscontCardViewController * view = [NITDiscontCardWireFrame createNITDiscontCardModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showScanCodeFrom:(UIViewController *)fromViewController withHandler:(void(^)(NSString * code))handler
{
    [NITBarcodeScannerWireFrame presentNITBarcodeScannerModuleFrom:fromViewController
                                                             title:NSLocalizedString(@"Наведите на карту.\nСканирование начнется автоматически!", nil)
                                                           handler:handler];
}

- (void)showScanCouponFrom:(UIViewController *)fromViewController withHandler:(void(^)(NSString * code))handler
{
    [NITBarcodeScannerWireFrame presentNITBarcodeScannerModuleFrom:fromViewController
                                                             title:NSLocalizedString(@"Наведите на купон.\nСканирование начнется автоматически!", nil)
                                                           handler:handler];
}

- (void)showCreateCardFrom:(UIViewController *)fromViewController withDelegate:(id)delegate
{
    [NITRegistrationCardWireFrame presentNITRegistrationCardModuleFrom:fromViewController withDelegate:delegate];
}

@end
