//
//  NITDiscontCardWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardProtocols.h"
#import "NITDiscontCardViewController.h"
#import "NITDiscontCardLocalDataManager.h"
#import "NITDiscontCardAPIDataManager.h"
#import "NITDiscontCardInteractor.h"
#import "NITDiscontCardPresenter.h"
#import "NITDiscontCardWireframe.h"
#import "NITRootWireframe.h"

@interface NITDiscontCardWireFrame : NITRootWireframe <NITDiscontCardWireFrameProtocol>

@end
