//
//  NITDiscontCardViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardViewController.h"
#import "AKNumericFormatter.h"
#import "UITextField+AKNumericFormatter.h"
#import "NITDiscontView.h"
#import "RSCodeView.h"
#import "NITCouponCell.h"
#import "NITCouponAddCell.h"
#import "NITPersonalDiscontCell.h"
#import "NITCouponBaseCell.h"
#import "UIView+Screenshot.h"
#import "UIView+Genie.h"

static NSString * formatterCodeMask = @" *        * * * * * *       * * * * * *";

@interface NITDiscontCardViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
NITCouponAddCellDelegate
>

@property (nonatomic) IBOutlet UISegmentedControl * segmentedControl;
@property (nonatomic) IBOutlet UIView * discontSection;
@property (nonatomic) IBOutlet UIView * couponsSection;
@property (nonatomic) IBOutlet UIView * createDiscontView;
@property (nonatomic) IBOutlet UIView * discontView;
@property (nonatomic) IBOutlet UITextField * codeField;
@property (nonatomic) IBOutlet UIView * percentView;
@property (nonatomic) NITDiscontView * discontList;
@property (nonatomic) IBOutlet RSCodeView * barcodeView;
@property (nonatomic) IBOutlet UILabel * barcodeLbl;
@property (nonatomic) IBOutlet UITableView * tableView;

@property (nonatomic) NSArray <NITCouponSection *> * sections;

@end

@implementation NITDiscontCardViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}
#pragma mark - IBAction
- (IBAction)changeSectionAction:(id)sender
{
    self.presenter.sectionType = self.segmentedControl.selectedSegmentIndex;
    [self updateSectionType];
}

- (IBAction)scanCodeAction:(id)sender
{
    [self.presenter scanCode];
}

- (IBAction)attachCardAction:(id)sender
{
    [self.presenter attachCardByCode:self.codeField.text];
}

- (IBAction)createCardAction:(id)sender
{
    [self.presenter createCard];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Дисконтная карта", nil);
    
    [self setKeyboardActiv:true];
    [self updateSectionType];
    [self.tableView registerNibArray:@[_s(NITCouponCell), _s(NITCouponAddCell), _s(NITPersonalDiscontCell)]];
    [self.tableView setTableFooterView:[UIView new]];
    
    self.codeField.numericFormatter = [AKNumericFormatter formatterWithMask:formatterCodeMask
                                                       placeholderCharacter:'*'
                                                                       mode:AKNumericFormatterMixed];
    
    self.discontList = [NITDiscontView viewWithFrame:RectSetSize(self.discontView.frame, ViewWidth(self.view), ViewHeight(self.percentView))];
    [self.percentView addSubview:self.discontList];
}

- (void)updateSectionType
{
    self.discontSection.hidden = self.presenter.sectionType != DiscontSectionTypeDiscont;
    self.couponsSection.hidden = self.presenter.sectionType != DiscontSectionTypeCoupons;
}

- (void)animateItemToCartByIndex:(NSInteger)index
{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:CouponSectionTypeView];
    UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    CGRect cellFrameInSuperview = [self.tableView convertRect:[self.tableView rectForRowAtIndexPath:indexPath] toView:[self.tableView superview]];
    
    UIImageView * genieView = [[UIImageView alloc] initWithFrame:cellFrameInSuperview];
    genieView.image = [cell imageByRenderingView];
    [self.view addSubview:genieView];
    
    CGPoint endPoint = [WINDOW basketCenter];
    CGRect endRect = CGRectMake(endPoint.x, endPoint.y, 1, 1);
    
    [genieView genieInTransitionWithDuration:.6f destinationRect:endRect destinationEdge:BCRectEdgeTop completion:^{
        [genieView removeFromSuperview];
    }];
}

#pragma mark - NITDiscontCardPresenterProtocol
- (void)reloadCodeFieldByCode:(NSString *)code
{
    self.codeField.text = [AKNumericFormatter formatString:code
                                                 usingMask:formatterCodeMask
                                      placeholderCharacter:'*'
                                                      mode:AKNumericFormatterMixed];
}

- (void)showErrorDiscontCardEnter
{
    self.codeField.textColor = RGB(217, 67, 67);
}

- (void)reloadDiscontDataByModels:(NSArray<NITDiscontCardModel *> *)models barcodeImage:(UIImage *)barcodeImage code:(NSString *)code
{
    self.discontList.items = models;
    self.barcodeView.code = barcodeImage;
    self.barcodeLbl.text = code;
}

- (void)reloadDiscontViewByType:(DiscontViewType)type
{
    self.createDiscontView.hidden   = type != DiscontViewTypeCreate;
    self.discontView.hidden         = type != DiscontViewTypeView;
}

- (void)reloadCouponsBySectionModels:(NSArray<NITCouponSection *> *)models
{
    self.sections = models;
    [self.tableView reloadData];
}

- (void)reloadAddCouponSection
{
    NITCouponAddCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell resetState];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sections[section] models].count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.sections[indexPath.section] cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCouponSection * sectionModel = self.sections[indexPath.section];
    NITCouponBaseCell * cell = [tableView dequeueReusableCellWithIdentifier:sectionModel.cellId forIndexPath:indexPath];
    [cell setModel:sectionModel.models[indexPath.row] delegate:self];
    
    return cell;
}

#pragma mark - NITCouponAddCellDelegate
- (void)didAddCoupon:(NSString *)text
{
    [self.presenter addNewCoupon:text];
}

- (void)didOpenScanQRCode
{
    [self.presenter scanNewCoupon];
}

#pragma mark - NITCouponCellDelegate
- (void)didAddCouponToBasket:(NITCouponModel *)coupon
{
    NITCouponSection * sectionModel = self.sections[CouponSectionTypeView];
    [self animateItemToCartByIndex:[sectionModel.models indexOfObject:coupon]];
    [self.presenter addCouponToBasket:coupon];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.codeField.textColor = RGB(10, 88, 43);
    return true;
}

@end
