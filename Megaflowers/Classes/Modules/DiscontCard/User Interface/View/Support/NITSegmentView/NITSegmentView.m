//
//  NITSegmentView.m
//  NITImage360
//
//  Created by Алексей Гуляев on 27.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITSegmentView.h"
#import "NITSegmentCollectionLayout.h"
#import "NITSegmentViewCell.h"
#import <AudioToolbox/AudioServices.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface NITSegmentView() <UICollectionViewDelegate, UICollectionViewDataSource, NITSegmentCollectionLayoutDelegate>
{
    IBOutlet UICollectionView *_collectionView;
}

@property(nonatomic, readonly) UICollectionView *collectionView;

@end


@implementation NITSegmentView

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self prepareLayout];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self prepareLayout];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self prepareLayout];
    }
    
    return self;
}

- (void)prepareLayout
{
    [self addSubview:self.collectionView];
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSMutableArray *constraints = [NSMutableArray new];
    
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cV]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"cV":_collectionView}]];
    
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cV]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"cV":_collectionView}]];
    
    [self addConstraints:constraints];
}

#pragma mark - Custom Accessors

- (UICollectionView*)collectionView
{
    if (!_collectionView)
    {
        NITSegmentCollectionLayout *layout = [NITSegmentCollectionLayout new];
        layout.delegate = self;
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        NSString *className = NSStringFromClass([NITSegmentViewCell class]);
        [collectionView registerNib:[UINib nibWithNibName:className bundle:nil] forCellWithReuseIdentifier:className];
        collectionView.backgroundColor = [UIColor clearColor];
        collectionView.showsVerticalScrollIndicator = NO;
        collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView = collectionView;
        self.layer.masksToBounds = NO;
        _collectionView.layer.masksToBounds = NO;
    }
    
    return _collectionView;
}

- (void)setItems:(NSArray<NSString *> *)items
{
    _items = items;
    
    if (_collectionView)
    {
        [self.collectionView reloadData];
        [self calcSize:self.collectionView];
    }
}

- (void)setSelectItem:(NSUInteger)selectItem
{
    _selectItem = selectItem;
    
    if (_collectionView)
    {
        [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:selectItem inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    }
}

- (void)setHighlightItem:(NSUInteger)highlightItem
{
    _highlightItem = highlightItem;
    NSArray<NSIndexPath*> *indexPaths = [self.collectionView indexPathsForVisibleItems];
    
    for (NSIndexPath *i in indexPaths)
    {
        NITSegmentViewCell *cell = (NITSegmentViewCell*)[self.collectionView cellForItemAtIndexPath:i];
        cell.isHighlight = _highlightItem == i.row;
    }
}

#pragma mark - IBActions

#pragma mark - Public

#pragma mark - Private

#pragma marn - NITSegmentCollectionLayoutDelegate

- (CGSize)segmentCollectionItemSize
{
    CGSize itemSize = self.bounds.size;
    itemSize.width /= 3.f;
    itemSize.height *= 0.8f;
    return itemSize;
}

#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NITSegmentViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([NITSegmentViewCell class]) forIndexPath:indexPath];
    cell.title = self.items[indexPath.row];
    cell.layer.masksToBounds = NO;
    [self calcSizeToCell:cell scrollView:collectionView];
    cell.isHighlight = self.highlightItem == indexPath.row;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectItem = indexPath.row;
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self tick];
    
    if (self.delegate)
    {
        [self.delegate segmentViewSelected:self];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self calcSize:scrollView];
}

- (void)calcSize:(UIScrollView*)scrollView
{
    NSArray *cells = [self.collectionView visibleCells];
    
    for (NITSegmentViewCell *cell in cells)
    {
        [self calcSizeToCell:cell scrollView:scrollView];
    }
}

- (void)calcSizeToCell:(NITSegmentViewCell*)cell scrollView:(UIScrollView*)scrollView
{
    CGFloat center = scrollView.contentOffset.x + self.collectionView.bounds.size.width / 2.f;
    
    CGFloat toCenter = (cell.center.x - center) / cell.bounds.size.width;
    toCenter = MAX(toCenter, -toCenter);
    toCenter = MIN(toCenter, 1.f);
    cell.toCenter = toCenter;
}

- (void)tick
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0"))
    {
        UISelectionFeedbackGenerator *feedback = [UISelectionFeedbackGenerator new];
        [feedback prepare];
        [feedback selectionChanged];
    }
    
    AudioServicesPlaySystemSound(1306);
}

@end
