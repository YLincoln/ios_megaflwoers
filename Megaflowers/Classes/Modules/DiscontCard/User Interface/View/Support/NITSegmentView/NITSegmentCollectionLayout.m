//
//  NITSegmentCollectionLayout.m
//  NITImage360
//
//  Created by Алексей Гуляев on 27.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITSegmentCollectionLayout.h"

@implementation NITSegmentCollectionLayout

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    CGSize iSize = [self.delegate segmentCollectionItemSize];
    NSUInteger count = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0];
    
    if (count > 0)
    {
        CGFloat xBegin = rect.origin.x;
        CGFloat xEnd = xBegin + rect.size.width;
        
        NSUInteger rBegin = floor((xBegin) / iSize.width);
        NSUInteger rEnd = floor((xEnd) / iSize.width);
        
        rBegin = rBegin > 0 ? rBegin : 0;
        rEnd = rEnd >= count ? count - 1 : rEnd;
        
        NSMutableArray *attibutes = [NSMutableArray new];
        
        for (NSUInteger r = rBegin; r <= rEnd; r++)
        {
            [attibutes addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:r inSection:0]]];
        }
        
        return attibutes;
    }
    else
    {
        return nil;
    }
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize iSize = [self.delegate segmentCollectionItemSize];
    CGFloat lPadding = iSize.width;
    
    UICollectionViewLayoutAttributes *attr = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    
    attr.size = iSize;
    attr.center = CGPointMake((indexPath.row * iSize.width) + iSize.width / 2.f + lPadding, iSize.height / 2.f);
    
    return attr;
}

- (CGSize)collectionViewContentSize
{
    CGSize itemSize = [self.delegate segmentCollectionItemSize];
    NSUInteger count = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0];
    itemSize.width *= (count + 2);
    return itemSize;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
{
    CGRect rect = CGRectMake(self.collectionView.contentOffset.x, 0.f, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    NSArray<UICollectionViewLayoutAttributes*> *list = [self layoutAttributesForElementsInRect:rect];
    
    CGFloat centerX = self.collectionView.contentOffset.x + self.collectionView.bounds.size.width / 2.f;
    CGFloat minDistanse = CGFLOAT_MAX;
    CGFloat currentCenterIndex = 0.f;
    NSIndexPath *currentIndexPath = nil;
    
    for (UICollectionViewLayoutAttributes *i in list)
    {
        CGFloat distance = centerX - i.center.x;
        distance = MAX(distance, -distance);
        
        if (minDistanse > distance)
        {
            minDistanse = distance;
            currentCenterIndex = i.center.x;
            currentIndexPath = i.indexPath;
        }
    }
    
    [self.collectionView scrollRectToVisible:CGRectMake(currentCenterIndex - self.collectionView.bounds.size.width / 2.f, .0f, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height) animated:YES];
    
    [self.collectionView selectItemAtIndexPath:currentIndexPath animated:nil scrollPosition:UICollectionViewScrollPositionNone];
    [self.collectionView.delegate collectionView:self.collectionView didSelectItemAtIndexPath:currentIndexPath];
    
    proposedContentOffset.x = self.collectionView.contentOffset.x;
    return [super targetContentOffsetForProposedContentOffset:proposedContentOffset withScrollingVelocity:velocity];
}

@end
