//
//  NITSegmentView.h
//  NITImage360
//
//  Created by Алексей Гуляев on 27.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NITSegmentView;

@protocol NITSegmentViewDelegate <NSObject>

- (void)segmentViewSelected:(NITSegmentView*)view;

@end

@interface NITSegmentView : UIView

@property(nonatomic, strong) NSArray<NSString*> *items;
@property(nonatomic) NSUInteger selectItem;
@property(nonatomic) NSUInteger highlightItem;

@property(nonatomic, weak) IBOutlet id<NITSegmentViewDelegate> delegate;

@end
