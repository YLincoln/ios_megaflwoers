//
//  NITSegmentCollectionLayout.h
//  NITImage360
//
//  Created by Алексей Гуляев on 27.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NITSegmentCollectionLayoutDelegate <NSObject>

- (CGSize)segmentCollectionItemSize;

@end

@interface NITSegmentCollectionLayout : UICollectionViewLayout

@property(nonatomic, weak) id<NITSegmentCollectionLayoutDelegate> delegate;

@end
