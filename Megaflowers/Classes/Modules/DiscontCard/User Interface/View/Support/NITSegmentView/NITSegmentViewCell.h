//
//  NITSegmentViewCell.h
//  NITImage360
//
//  Created by Алексей Гуляев on 27.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITSegmentViewCell : UICollectionViewCell

@property(nonatomic, strong) NSString *title;
@property(nonatomic) CGFloat toCenter;
@property(nonatomic) BOOL isHighlight;

@end
