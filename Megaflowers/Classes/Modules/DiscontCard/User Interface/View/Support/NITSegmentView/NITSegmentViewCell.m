//
//  NITSegmentViewCell.m
//  NITImage360
//
//  Created by Алексей Гуляев on 27.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITSegmentViewCell.h"

@interface NITSegmentViewCell()
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet NSLayoutConstraint *_labelHeigth;
    CGFloat _toCenter;
}

@end

@implementation NITSegmentViewCell

#pragma mark - Lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    _labelHeigth.constant = _titleLabel.font.pointSize * 0.75f;
}

#pragma mark - Custom Accessors

- (void)setIsHighlight:(BOOL)isHighlight
{
    _titleLabel.alpha = isHighlight ? 1.f : .5f;
}

- (void)setTitle:(NSString *)title
{
    _titleLabel.text = title;
}

- (NSString *)title
{
    return _titleLabel.text;
}

- (void)setToCenter:(CGFloat)toCenter
{
    _toCenter = toCenter;
    CGFloat w = 1.f - toCenter - (IS_IPHONE_4 ? 0.25 : 0);
    w = MAX(w, 0.42f);
    _titleLabel.transform = CGAffineTransformMakeScale(w, w);
    
    CGFloat top = self.bounds.size.height - _labelHeigth.constant * w;
    CGPoint center = _titleLabel.center;
    center.y = top + w * _labelHeigth.constant / 2.f;
    _titleLabel.center = center;
}

#pragma mark - IBActions

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self setToCenter:_toCenter];
}

#pragma mark - Public

#pragma mark - Private

@end
