//
//  NITDiscontView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontView.h"
#import "NITDiscontCardModel.h"
#import "NITSegmentView.h"

@interface NITDiscontView () <NITSegmentViewDelegate>

@property (nonatomic) IBOutlet NITSegmentView * segmentView;
@property (nonatomic) IBOutlet UIPageControl * pageControl;
@property (nonatomic) IBOutlet UILabel * descrText;

@end

@implementation NITDiscontView

+ (instancetype)viewWithFrame:(CGRect)frame
{
    NITDiscontView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITDiscontView) owner:self options:nil] firstObject];
    view.frame = frame;
    
    return view;
}

#pragma mark - Custom accessors
- (void)setItems:(NSArray<NITDiscontCardModel *> *)items
{
    _items = items;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)changeIndex:(id)sender
{
    self.segmentView.selectItem = self.pageControl.currentPage;
    self.descrText.text = [self.items[self.pageControl.currentPage] title];
}

#pragma mark - Private
- (void)updateData
{
    self.pageControl.hidden = self.items.count < 2;
    self.segmentView.delegate = self;
    self.segmentView.items = [self.items bk_map:^id(NITDiscontCardModel *obj) {
        return obj.percent;
    }];
    
    for (int i = 0; i < self.items.count; i++)
    {
        NITDiscontCardModel * model = self.items[i];
        if (model.active)
        {
            self.descrText.text = model.title;
            self.pageControl.currentPage = i;
            self.segmentView.selectItem = i;
            self.segmentView.highlightItem = i;
            
            break;
        }
    }
}

#pragma mark - NITSegmentViewDelegate
- (void)segmentViewSelected:(NITSegmentView *)view
{
    self.pageControl.currentPage = view.selectItem;
    self.descrText.text = [self.items[view.selectItem] title];
}

@end
