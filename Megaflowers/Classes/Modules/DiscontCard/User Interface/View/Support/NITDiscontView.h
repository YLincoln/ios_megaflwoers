//
//  NITDiscontView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITDiscontCardModel;

@interface NITDiscontView : UIView

@property (nonatomic) NSArray <NITDiscontCardModel *> * items;

+ (instancetype)viewWithFrame:(CGRect)frame;

@end
