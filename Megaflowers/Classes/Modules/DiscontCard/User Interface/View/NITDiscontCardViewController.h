//
//  NITDiscontCardViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITDiscontCardProtocols.h"

@interface NITDiscontCardViewController : NITBaseViewController <NITDiscontCardViewProtocol>

@property (nonatomic, strong) id <NITDiscontCardPresenterProtocol> presenter;

@end
