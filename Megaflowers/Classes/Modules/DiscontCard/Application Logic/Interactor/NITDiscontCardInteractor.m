//
//  NITDiscontCardInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITCouponsLocalDataManager.h"

@implementation NITDiscontCardInteractor

- (void)attachCardByCode:(NSString *)code
{
    [self.APIDataManager attachDiscountCardById:code withHandler:^(NSError *error) {
        
        if (error)
        {
            [self.presenter discountCardAttachError:error.interfaceDescription];
        }
        else
        {
            [self.presenter discountCardAttachedSuccess];
        }
    }];
}

- (void)updateCurrentDiscount
{
    UserDiscount * discount = USER.discount;
    
    if (discount)
    {
        weaken(self);
        [self.APIDataManager getDiscountCardTypesWithHandler:^(NSArray<SWGDiscountSettings *> *result) {
            
            if (result)
            {
                NSMutableArray * models = [NSMutableArray new];
                for (int i = 0; i < result.count; i++)
                {
                    NSString * nextPercent = (result.count - 1 >= i + 1) ? [result[i + 1] value] : @"";
                    [models addObject:[[NITDiscontCardModel alloc] initWithModel:result[i] nextPercent:nextPercent]];
                }
                
                [weakSelf.presenter updateDiscountModels:[models bk_select:^BOOL(NITDiscontCardModel *obj) {
                    return obj.type == discount.type;
                }]];
            }
            else
            {
                [weakSelf.presenter updateDiscountModels:nil];
            }
        }];
    }
    else
    {
        [self.presenter updateDiscountModels:nil];
    }
}

- (void)updateCouponsData
{
    weaken(self);
    [self.APIDataManager getUserCouponsWithHandler:^(NSArray<SWGUserCoupon> *result) {
        [weakSelf.presenter updateCouponModels:[weakSelf modelsFromEntities:result]];
    }];
}

- (void)addNewCoupon:(NSString *)text
{
    weaken(self);
    [self.APIDataManager addNewCoupon:text withHandler:^(NSError *error) {
        
        if (error)
        {
            [weakSelf.presenter couponAddError:error.interfaceDescription];
        }
        else
        {
            [weakSelf.presenter couponAddedSuccess];
        }
    }];
}

- (void)addCouponToBasket:(NITCouponModel *)coupon
{
    NITCouponsLocalDataManager * ldm = [NITCouponsLocalDataManager new];
    [ldm addCouponToBasket:coupon];
}

#pragma mark - Private
- (NSArray <NITCouponModel *> *)modelsFromEntities:(NSArray <SWGUserCoupon *> *)entities
{
    return [entities bk_map:^id(SWGUserCoupon * obj) {
        return [[NITCouponModel alloc] initWidthUserCoupon:obj];
    }];
}

@end
