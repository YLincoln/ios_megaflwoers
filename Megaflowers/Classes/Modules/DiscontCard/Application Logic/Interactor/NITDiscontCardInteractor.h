//
//  NITDiscontCardInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITDiscontCardProtocols.h"

@interface NITDiscontCardInteractor : NSObject <NITDiscontCardInteractorInputProtocol>

@property (nonatomic, weak) id <NITDiscontCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITDiscontCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITDiscontCardLocalDataManagerInputProtocol> localDataManager;

@end
