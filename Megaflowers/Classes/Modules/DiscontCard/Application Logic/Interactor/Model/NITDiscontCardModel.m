//
//  NITDiscontCardModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardModel.h"
#import "SWGDiscountSettings.h"

@implementation NITDiscontCardModel

- (instancetype)initWithModel:(SWGDiscountSettings *)model nextPercent:(NSString *)nextPercent
{
    if (self = [super init])
    {
        self.type = model.discountType.integerValue;
        self.minSum = model.amountMin.integerValue;
        self.maxSum = model.amountMax.integerValue;
        self.percent = model.value;
        self.nextPercent = nextPercent;
        
        UserDiscount * discount = USER.discount;
        if (discount)
        {
            self.currentSum = discount.order_sum.integerValue + discount.order_sum_retail.integerValue;
            self.currentPercent = [discount.value.stringValue stringByAppendingString:@"%"];
            NSString * percentValue = [[model.value stringByReplacingOccurrencesOfString:@"%" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
            self.active = [discount.value.stringValue isEqualToString:percentValue] || self.type == DiscountTypeFixed;
        }
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSString *)title
{
    switch (self.type) {
        case DiscountTypeFixed: {
            return [NSString stringWithFormat:@"%@ %@ %@",
                    NSLocalizedString(@"Вы совершили покупки на сумму", nil),
                    @(self.currentSum),
                    NSLocalizedString(@"руб.", nil)];
        }
        case DiscountTypeAccumilation: {
            
            if ((self.minSum == 0 && self.currentSum < self.maxSum) ||
                (self.maxSum > 0 && self.currentSum >= self.minSum && self.currentSum < self.maxSum))
            {
                return [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@",
                        NSLocalizedString(@"Вы совершили покупки на сумму\n", nil),
                        @(self.currentSum),
                        NSLocalizedString(@"рублей. Для получения\nскидки", nil),
                        self.nextPercent,
                        NSLocalizedString(@"вам нужно совершить покупки еще на", nil),
                        @(self.maxSum - self.currentSum),
                        NSLocalizedString(@"рублей.", nil)];
            }
            else if (self.maxSum == 0 && self.currentSum >= self.minSum)
            {
                return [NSString stringWithFormat:@"%@ %@ %@ %@",
                        NSLocalizedString(@"Вы совершили покупки на сумму более", nil),
                        @(self.minSum),
                        NSLocalizedString(@"руб. - активирована скидка", nil),
                        self.percent];
            }
            else if (self.maxSum > 0 && self.currentSum >= self.maxSum)
            {
                return [NSString stringWithFormat:@"%@ %@ %@ %@",
                        NSLocalizedString(@"Вы совершили покупки на сумму более", nil),
                        @(self.maxSum),
                        NSLocalizedString(@"руб. - активирована скидка", nil),
                        self.currentPercent];
            }
            else if (self.minSum > 0 && self.currentSum < self.minSum)
            {
                return  [NSString stringWithFormat:@"%@ %@ %@ %@ %@",
                         NSLocalizedString(@"До получения скидки", nil),
                         self.percent,
                         NSLocalizedString(@"вам осталось накопить еще", nil),
                         @(self.minSum - self.currentSum),
                         NSLocalizedString(@"руб.", nil)];
            }
            
            return @"";
        };
    }
}

/*- (void)updateElemets
{
    switch (self.segmentView.selectItem) {
        case 0:
        {
            switch (self.segmentView.highlightItem) {
                case 0:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",
                                           NSLocalizedString(@"Вы совершили покупки на сумму\n", nil),
                                           @(self.model.currentSum),
                                           NSLocalizedString(@"рублей. Для получения\nскидки 7% вам нужно совершить покупки еще на", nil),
                                           @(self.model.middleSum - self.model.currentSum),
                                           NSLocalizedString(@"рублей.", nil)];
                    break;
                case 1:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@",
                                           NSLocalizedString(@"Вы совершили покупки на сумму более", nil),
                                           @(self.model.middleSum),
                                           NSLocalizedString(@"руб. - активирована скидка 7%", nil)];
                    break;
                    
                case 2:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@",
                                           NSLocalizedString(@"Вы совершили покупки на сумму более", nil),
                                           @(self.model.maxSum),
                                           NSLocalizedString(@"руб. - активирована скидка 10%", nil)];
                    break;
            }
        }
            
            break;
            
        case 1:
        {
            switch (self.segmentView.highlightItem) {
                case 0:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@",
                                           NSLocalizedString(@"До получения скидки 7% вам осталось накопить еще", nil),
                                           @(self.model.middleSum - self.model.currentSum),
                                           NSLocalizedString(@"руб.", nil)];
                    break;
                case 1:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",
                                           NSLocalizedString(@"Вы совершили покупки на сумму\n", nil),
                                           @(self.model.currentSum),
                                           NSLocalizedString(@"рублей. Для получения\nскидки 10% вам нужно совершить покупки еще на", nil),
                                           @(self.model.maxSum - self.model.currentSum),
                                           NSLocalizedString(@"рублей.", nil)];
                    break;
                    
                case 2:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@",
                                           NSLocalizedString(@"Вы совершили покупки на сумму более", nil),
                                           @(self.model.maxSum),
                                           NSLocalizedString(@"руб. - активирована скидка 10%", nil)];
                    break;
            }
        }
            break;
            
        case 2:
        {
            switch (self.segmentView.highlightItem) {
                case 0:
                case 1:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@",
                                           NSLocalizedString(@"До получения скидки 10% вам осталось накопить еще", nil),
                                           @(self.model.maxSum - self.model.currentSum),
                                           NSLocalizedString(@"руб.", nil)];
                    break;
                    
                case 2:
                    self.descrText.text = [NSString stringWithFormat:@"%@ %@ %@",
                                           NSLocalizedString(@"Вы совершили покупки на сумму более", nil),
                                           @(self.model.maxSum),
                                           NSLocalizedString(@"руб. - активирована скидка 10%", nil)];
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}*/

@end
