//
//  NITCouponSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/10/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponSection.h"
#import "NITCouponAddCell.h"
#import "NITCouponCell.h"
#import "NITPersonalDiscontCell.h"

@implementation NITCouponSection

- (instancetype)initWithType:(CouponSectionType)type models:(NSArray *)models
{
    if (self = [super init])
    {
        self.type = type;
        self.models = models;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSInteger)cellHeight
{
    switch (self.type) {
        case CouponSectionTypeAdd:  return 57;
        case CouponSectionTypeView:
        case CouponSectionTypeInfo: return 134;
    }
}

- (NSString *)cellId
{
    switch (self.type) {
        case CouponSectionTypeAdd:  return _s(NITCouponAddCell);
        case CouponSectionTypeView: return _s(NITCouponCell);
        case CouponSectionTypeInfo: return _s(NITPersonalDiscontCell);
    }
}

@end
