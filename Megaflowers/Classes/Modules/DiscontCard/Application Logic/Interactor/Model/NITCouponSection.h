//
//  NITCouponSection.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/10/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, CouponSectionType) {
    CouponSectionTypeAdd    = 0,
    CouponSectionTypeView   = 1,
    CouponSectionTypeInfo   = 2
};

@interface NITCouponSection : NSObject

@property (nonatomic) CouponSectionType type;
@property (nonatomic) NSArray * models;
@property (nonatomic) NSString * cellId;
@property (nonatomic) NSInteger cellHeight;

- (instancetype)initWithType:(CouponSectionType)type models:(NSArray *)models;

@end
