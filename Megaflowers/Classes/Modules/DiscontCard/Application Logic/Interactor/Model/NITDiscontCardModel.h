//
//  NITDiscontCardModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SWGDiscountSettings;

@interface NITDiscontCardModel : NSObject

@property (nonatomic) DiscountType type;
@property (nonatomic) NSInteger minSum;
@property (nonatomic) NSInteger maxSum;
@property (nonatomic) NSInteger currentSum;
@property (nonatomic) NSString * percent;
@property (nonatomic) NSString * currentPercent;
@property (nonatomic) NSString * nextPercent;
@property (nonatomic) NSString * title;
@property (nonatomic) BOOL active;

- (instancetype)initWithModel:(SWGDiscountSettings *)model nextPercent:(NSString *)nextPercent;

@end
