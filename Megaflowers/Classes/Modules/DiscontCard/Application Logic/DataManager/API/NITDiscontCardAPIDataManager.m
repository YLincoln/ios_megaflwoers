//
//  NITDiscontCardAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardAPIDataManager.h"
#import "SWGUserApi.h"
#import "SWGCardApi.h"

@implementation NITDiscontCardAPIDataManager

- (void)getUserCouponsWithHandler:(void (^)(NSArray<SWGUserCoupon> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userCouponGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserCoupon> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler(output);
    }];
}

- (void)addNewCoupon:(NSString *)text withHandler:(void (^)(NSError *error))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userCouponPostWithCode:text completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler(error);
    }];
}

- (void)getDiscountCardTypesWithHandler:(void(^)(NSArray<SWGDiscountSettings *> *result))handler
{
    [HELPER startLoading];
    [[SWGCardApi new] cardSettingsGetWithCompletionHandler:^(NSArray<SWGDiscountSettings> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler(output);
    }];
}

- (void)attachDiscountCardById:(NSString *)cardId withHandler:(void(^)(NSError *error))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userDiscountAddPostWithCode:cardId lang:HELPER.lang completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler(error);
    }];
}

@end
