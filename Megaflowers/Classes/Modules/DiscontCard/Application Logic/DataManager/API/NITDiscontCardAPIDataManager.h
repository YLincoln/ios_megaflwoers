//
//  NITDiscontCardAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITDiscontCardProtocols.h"

@interface NITDiscontCardAPIDataManager : NSObject <NITDiscontCardAPIDataManagerInputProtocol>

@end
