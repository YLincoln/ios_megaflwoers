//
//  NITDiscontCardProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscontCardModel.h"
#import "NITCouponModel.h"
#import "SWGUserCoupon.h"
#import "SWGDiscountSettings.h"
#import "NITCouponSection.h"

@protocol NITDiscontCardInteractorOutputProtocol;
@protocol NITDiscontCardInteractorInputProtocol;
@protocol NITDiscontCardViewProtocol;
@protocol NITDiscontCardPresenterProtocol;
@protocol NITDiscontCardLocalDataManagerInputProtocol;
@protocol NITDiscontCardAPIDataManagerInputProtocol;

@class NITDiscontCardWireFrame;

typedef CF_ENUM (NSUInteger, DiscontSectionType) {
    DiscontSectionTypeDiscont   = 0,
    DiscontSectionTypeCoupons   = 1
};

typedef CF_ENUM (NSUInteger, DiscontViewType) {
    DiscontViewTypeCreate   = 0,
    DiscontViewTypeView     = 1
};


@protocol NITDiscontCardViewProtocol
@required
@property (nonatomic, strong) id <NITDiscontCardPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadCodeFieldByCode:(NSString *)code;
- (void)showErrorDiscontCardEnter;
- (void)reloadDiscontDataByModels:(NSArray <NITDiscontCardModel *> *)models barcodeImage:(UIImage *)barcodeImage code:(NSString *)code;
- (void)reloadDiscontViewByType:(DiscontViewType)type;
- (void)reloadCouponsBySectionModels:(NSArray <NITCouponSection *> *)models;
- (void)reloadAddCouponSection;
@end

@protocol NITDiscontCardWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITDiscontCardModuleFrom:(id)fromView;
- (void)showScanCodeFrom:(id)fromView withHandler:(void(^)(NSString * code))handler;
- (void)showScanCouponFrom:(id)fromView withHandler:(void(^)(NSString * code))handler;
- (void)showCreateCardFrom:(id)fromView withDelegate:(id)delegate;
@end

@protocol NITDiscontCardPresenterProtocol
@required
@property (nonatomic, weak) id <NITDiscontCardViewProtocol> view;
@property (nonatomic, strong) id <NITDiscontCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITDiscontCardWireFrameProtocol> wireFrame;
@property (nonatomic) DiscontSectionType sectionType;
@property (nonatomic) DiscontViewType discontViewType;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)scanCode;
- (void)attachCardByCode:(NSString *)code;
- (void)createCard;
- (void)addNewCoupon:(NSString *)code;
- (void)scanNewCoupon;
- (void)addCouponToBasket:(NITCouponModel *)coupon;
@end

@protocol NITDiscontCardInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDiscountModels:(NSArray <NITDiscontCardModel *> *)models;
- (void)discountCardAttachedSuccess;
- (void)discountCardAttachError:(NSString *)error;
- (void)updateCouponModels:(NSArray <NITCouponModel *> *)models;
- (void)couponAddedSuccess;
- (void)couponAddError:(NSString *)error;
@end

@protocol NITDiscontCardInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITDiscontCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITDiscontCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITDiscontCardLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)attachCardByCode:(NSString *)code;
- (void)updateCurrentDiscount;
- (void)updateCouponsData;
- (void)addNewCoupon:(NSString *)text;
- (void)addCouponToBasket:(NITCouponModel *)coupon;
@end


@protocol NITDiscontCardDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITDiscontCardAPIDataManagerInputProtocol <NITDiscontCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserCouponsWithHandler:(void (^)(NSArray<SWGUserCoupon> *result))handler;
- (void)addNewCoupon:(NSString *)text withHandler:(void (^)(NSError *error))handler;
- (void)getDiscountCardTypesWithHandler:(void(^)(NSArray<SWGDiscountSettings *> *result))handler;
- (void)attachDiscountCardById:(NSString *)cardId withHandler:(void(^)(NSError *error))handler;
@end

@protocol NITDiscontCardLocalDataManagerInputProtocol <NITDiscontCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
