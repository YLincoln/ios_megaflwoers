//
//  NITCalendarInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCalendarInteractor.h"
#import "NSArray+BlocksKit.h"

@interface NITCalendarInteractor ()

@property (nonatomic) NSDictionary * data;

@end

@implementation NITCalendarInteractor

- (void)initDataWithHandler:(void(^)(NSDictionary * result))handler
{
    weaken(self);
    [self.localDataManager syncSystemEventsWithHandler:^{
        [weakSelf updateDataByStartDate:[NSDate date] withHandler:handler];
    }];
}

- (void)updateDataByStartDate:(NSDate *)date withHandler:(void(^)(NSDictionary * result))handler
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSDate * dateMonthEnd = [[date dateMonthEnd] dateByAddingDays:1];
        NSDate * currentDate = [date dateMonthStart];
        
        NSMutableDictionary * monthInfo = [NSMutableDictionary new];
        
        while ([currentDate isLessDate:dateMonthEnd])
        {
            [monthInfo setObject:[self modelsFromEntities:[self.localDataManager getEventsForDate:currentDate]]
                          forKey:[currentDate dateToday]];
            
            currentDate = [currentDate dateByAddingDays:1];
        }
        
        self.data = monthInfo;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(monthInfo);
        });
    });
}

- (NSArray <NITEventModel *> *)eventsForDate:(NSDate *)date
{
    return self.data[date.dateToday];
}

- (void)searchByText:(NSString *)text withHandler:(void (^)(NSArray<NITSearchEventModel *> *result, NSInteger selectedIndex))handler
{
    weaken(self);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSArray * result;
        NSInteger selectedIndex = 0;
        
        if (text.length > 0)
        {
            NSArray * events =  [weakSelf processSearchResult:[weakSelf.localDataManager searchEventsByString:text]];
            
            NSMutableDictionary * resultInfo = [NSMutableDictionary new];
            
            for (NITEventModel * model in events)
            {
                NSMutableArray * currentEvents = [NSMutableArray arrayWithObject:model];
                NSArray * savedEvents = resultInfo[model.startDate];
                if (savedEvents)
                {
                    [currentEvents addObjectsFromArray:savedEvents];
                }
                [resultInfo setObject:currentEvents forKey:model.startDate];
            }
            
            NSMutableArray * searchResult = [NSMutableArray new];
            
            for (NSDate * key in resultInfo)
            {
                NITSearchEventModel * model = [NITSearchEventModel new];
                model.date = key;
                model.events = resultInfo[key];
                
                [searchResult addObject:model];
            }
            
            [searchResult sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:true]]];
            
            for (int i = 0; i < searchResult.count; i++)
            {
                NITSearchEventModel * model = searchResult[i];
                
                if ([model.date year] == [NSDate date].year)
                {
                    selectedIndex = i;
                    break;
                }
            }
            
            result = searchResult;
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            handler(result, selectedIndex);
        });
    });
}

- (void)remindEvent:(NITEventModel *)model byType:(NITEventReminderType)type
{
    [self.localDataManager remindEventWithID:model.identifier byType:type];
}

- (void)removeEvent:(NITEventModel *)model
{
    [self.localDataManager markDeletedEventWithID:model.identifier];
    [self.APIDataManager deleteEventByID:@([model.identifier integerValue])];
}

#pragma mark - Private
- (NSArray <NITEventModel *> *)modelsFromEntities:(NSArray <NITEvent *> *)entities
{
    return [entities bk_map:^id(NITEvent * obj) {
        return [[NITEventModel alloc] initWithEvent:obj];
    }];
}

- (NSArray <NITEventModel *> *)processSearchResult:(NSArray <NITEvent *> *)entities
{
    NSMutableArray * result = [NSMutableArray new];
    
    // never repeats events process
    NSArray * otherEvents = [entities bk_select:^BOOL(NITEvent *obj) {
        return obj.repeatType == NITEventRepeatTypeNever;
    }];
    
    [result addObjectsFromArray:[self modelsFromEntities:otherEvents]];
    
    // every year events process
    NSArray * everyYearEvents = [entities bk_select:^BOOL(NITEvent *obj) {
        return obj.repeatType == NITEventRepeatTypeEveryYear;
    }];

    NSMutableArray * years = [NSMutableArray new];
    NSInteger startYear = [NSDate date].year - 1;
    for (int i = 0; i < 3; i++)
    {
        [years addObject:@(startYear)];
        startYear++;
    }

    for (NITEvent * event in everyYearEvents)
    {
        for (NSNumber * year in years)
        {
            NITEventModel * model = [[NITEventModel alloc] initWithEvent:event andDate:[event.startDate dateBySettingYear:year.integerValue]];
            [result addObject:model];
        }
    }
    
    // every month events process
    NSArray * everyMonthEvents = [entities bk_select:^BOOL(NITEvent *obj) {
        return obj.repeatType == NITEventRepeatTypeEveryMonth;
    }];
    
    NSMutableArray * months = [NSMutableArray new];
    NSInteger startMonth = [NSDate date].month;
    for (int i = 0; i < 12; i++)
    {
        [months addObject:@(startMonth)];
        startMonth++;
    }
    
    for (NITEvent * event in everyMonthEvents)
    {
        NSDate * date = event.startDate;
        for (NSNumber * year in years)
        {
            date = [date dateBySettingYear:year.integerValue];
            for (NSNumber * month in months)
            {
                date = [date dateBySettingMonth:month.integerValue];
                NITEventModel * model = [[NITEventModel alloc] initWithEvent:event andDate:date];
                [result addObject:model];
            }
        }
    }

    return result;
}

@end
