//
//  NITEventModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITEventModel.h"

@implementation NITEventModel

- (instancetype)initWithEvent:(NITEvent *)event
{
    return [self initWithEvent:event andDate:nil];
}

- (instancetype)initWithEvent:(NITEvent *)event andDate:(NSDate *)date
{
    if (self = [super init])
    {
        self.identifier     = event.identifier;
        self.title          = [event.title stringByStrippingHTML];
        self.typeTitle      = event.typeTitle;
        self.reminderTitle  = event.reminderTitle;
        self.reminderDate   = event.reminderDate;
        self.placeholder    = event.eventImage;
        self.imageURL       = event.imagePath.length > 0 ? [NSURL URLWithString:event.imagePath] : nil;
        self.reminderType   = event.reminderType;
        self.repeatType     = event.repeatType;
        self.startDate      = date ? [date dateToday] : event.startDate;
        self.endDate        = date ? [date dateToday] : event.endDate;
        self.local          = event.local;
        self.type           = event.type;
    }
    
    return self;
}

@end
