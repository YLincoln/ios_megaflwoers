//
//  NITEventModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITEventModel : NSObject

@property (nonatomic) NSString * identifier;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * typeTitle;
@property (nonatomic) NSString * reminderTitle;
@property (nonatomic) NSDate * startDate;
@property (nonatomic) NSDate * endDate;
@property (nonatomic) NSDate * reminderDate;
@property (nonatomic) UIImage * placeholder;
@property (nonatomic) NSURL * imageURL;
@property (nonatomic) NITEventReminderType reminderType;
@property (nonatomic) NITEventRepeatType repeatType;
@property (nonatomic) NITEventType type;
@property (nonatomic) BOOL local;

- (instancetype)initWithEvent:(NITEvent *)event;
- (instancetype)initWithEvent:(NITEvent *)event andDate:(NSDate *)date;

@end
