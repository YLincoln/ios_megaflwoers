//
//  NITSearchEventModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/29/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITEventModel;

@interface NITSearchEventModel : NSObject

@property (nonatomic) NSDate * date;
@property (nonatomic) NSString * formattedDateString;
@property (nonatomic) NSArray <NITEventModel *> * events;
@property (nonatomic) UIView * headerView;

@end
