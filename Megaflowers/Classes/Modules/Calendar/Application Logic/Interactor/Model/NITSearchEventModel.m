//
//  NITSearchEventModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/29/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSearchEventModel.h"

@implementation NITSearchEventModel

- (NSString *)formattedDateString
{
    NSString * format = [[self.date dateYearStart] isEqualToDay:[[NSDate date] dateYearStart]] ? @"dd MMMM" : @"dd MMMM yyyy";
    return [self.date dateStringByLocalFormat:format];
}

- (UIView *)headerView
{
    UIView * sectionView = [[[NSBundle mainBundle] loadNibNamed:@"NITSearchEventHeader" owner:self options:nil] firstObject];
    UILabel * title = [sectionView viewWithTag:101];
    title.text = self.formattedDateString;
    
    return sectionView;
}

@end
