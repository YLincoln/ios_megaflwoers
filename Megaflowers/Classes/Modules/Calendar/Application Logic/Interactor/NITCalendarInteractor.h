//
//  NITCalendarInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCalendarProtocols.h"

@interface NITCalendarInteractor : NSObject <NITCalendarInteractorInputProtocol>

@property (nonatomic, weak) id <NITCalendarInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCalendarAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCalendarLocalDataManagerInputProtocol> localDataManager;

@end
