//
//  NITCalendarAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCalendarProtocols.h"

@interface NITCalendarAPIDataManager : NSObject <NITCalendarAPIDataManagerInputProtocol>

@end
