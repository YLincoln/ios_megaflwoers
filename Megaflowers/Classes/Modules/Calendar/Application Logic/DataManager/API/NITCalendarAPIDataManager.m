//
//  NITCalendarAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCalendarAPIDataManager.h"
#import "SWGUserApi.h"
#import "NITNewEventModel.h"

@implementation NITCalendarAPIDataManager

- (void)deleteEventByID:(NSNumber *)eventID
{
    [HELPER startLoading];
    [[SWGUserApi new] userEventIdDeleteWithId:eventID completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
    }];
}

@end
