//
//  NITCalendarLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCalendarLocalDataManager.h"

@implementation NITCalendarLocalDataManager

- (void)syncSystemEventsWithHandler:(void(^)())handler
{
    [EVENT_MANAGER syncSystemEventsWithHandler:handler];
}

- (NSArray <NITEvent *> *)getEventsForDate:(NSDate *)date
{
    return [NITEvent eventsForDate:date] ? : [NSArray new];
}

- (NSArray <NITEvent *> *)searchEventsByString:(NSString *)searchString
{
    return [NITEvent searchEventsByString:searchString];
}

- (void)remindEventWithID:(NSString *)identifier byType:(NITEventReminderType)type
{
    NITEvent * event = [NITEvent eventByID:identifier];
    [event remindByType:type];
}

- (void)markDeletedEventWithID:(NSString *)identifier
{
    NITEvent * event = [NITEvent eventByID:identifier];
    [event markDeleted];
}

@end
