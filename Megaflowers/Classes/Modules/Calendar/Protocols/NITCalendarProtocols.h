//
//  NITCalendarProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITEventModel.h"
#import "NITSearchEventModel.h"

@protocol NITCalendarInteractorOutputProtocol;
@protocol NITCalendarInteractorInputProtocol;
@protocol NITCalendarViewProtocol;
@protocol NITCalendarPresenterProtocol;
@protocol NITCalendarLocalDataManagerInputProtocol;
@protocol NITCalendarAPIDataManagerInputProtocol;

@class NITCalendarWireFrame;

@protocol NITCalendarViewProtocol
@required
@property (nonatomic, strong) id <NITCalendarPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadEventsData:(NSDictionary *)data withSelectedDate:(NSDate *)date;
- (void)reloadTodayEventsData:(NSArray <NITEventModel *> *)data;
- (void)reloadSearchResultData:(NSArray <NITSearchEventModel *> *)data andSelectedIndex:(NSInteger)selectedIndex;
@end

@protocol NITCalendarWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCalendarModuleFrom:(id)fromView;
- (void)showCreateEventFrom:(id)fromView withDate:(NSDate *)date;
- (void)showEditEvent:(NITEventModel *)model from:(id)fromView;
- (void)showSendBouqetFrom:(id)fromView;
@end

@protocol NITCalendarPresenterProtocol
@required
@property (nonatomic, weak) id <NITCalendarViewProtocol> view;
@property (nonatomic, strong) id <NITCalendarInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCalendarWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initMonthData;
- (void)updateMonthDataByMonth:(NSDate *)month;
- (void)updateDataByDate:(NSDate *)date;
- (void)searchByText:(NSString *)text;
- (void)createNewEvent;
- (void)remindEvent:(NITEventModel *)model;
- (void)removeEvent:(NITEventModel *)model;
- (void)sendBouquetForEvent:(NITEventModel *)model;
- (void)editEvent:(NITEventModel *)model;
@end

@protocol NITCalendarInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITCalendarInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCalendarInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCalendarAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCalendarLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)initDataWithHandler:(void(^)(NSDictionary * result))handler;
- (void)updateDataByStartDate:(NSDate *)date withHandler:(void(^)(NSDictionary * result))handler;
- (NSArray <NITEventModel *> *)eventsForDate:(NSDate *)date;
- (void)searchByText:(NSString *)text withHandler:(void (^)(NSArray<NITSearchEventModel *> *result, NSInteger selectedIndex))handler;
- (void)remindEvent:(NITEventModel *)model byType:(NITEventReminderType)type;
- (void)removeEvent:(NITEventModel *)model;
@end


@protocol NITCalendarDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCalendarAPIDataManagerInputProtocol <NITCalendarDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)deleteEventByID:(NSNumber *)eventID;
@end

@protocol NITCalendarLocalDataManagerInputProtocol <NITCalendarDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)syncSystemEventsWithHandler:(void(^)())handler;
- (NSArray <NITEvent *> *)getEventsForDate:(NSDate *)date;
- (NSArray <NITEvent *> *)searchEventsByString:(NSString *)searchString;
- (void)remindEventWithID:(NSString *)identifier byType:(NITEventReminderType)type;
- (void)markDeletedEventWithID:(NSString *)identifier;

@end
