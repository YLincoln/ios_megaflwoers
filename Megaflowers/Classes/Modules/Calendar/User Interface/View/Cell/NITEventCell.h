//
//  NITEventCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITEventModel;

@protocol NITEventCellDelegate <NSObject>

- (void)didRemindEvent:(NITEventModel *)event;
- (void)didRemoveEvent:(NITEventModel *)event;
- (void)didSendBouquetForEvent:(NITEventModel *)event;
- (void)didEditEvent:(NITEventModel *)event;

@end

@interface NITEventCell : UITableViewCell

@property (nonatomic) NITEventModel * model;
@property (nonatomic, weak) id <NITEventCellDelegate> delegate;

@end
