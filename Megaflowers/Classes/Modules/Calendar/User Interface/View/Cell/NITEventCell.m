//
//  NITEventCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITEventCell.h"
#import "NITEventModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITEventCell ()

@property (nonatomic) IBOutlet UIImageView * image;
@property (nonatomic) IBOutlet UILabel * typeTitle;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * reminderLbl;
@property (nonatomic) IBOutlet UIImageView * reminderIcon;
@property (nonatomic) IBOutlet UIButton * reminderBtn;
@property (nonatomic) IBOutlet UIButton * removeBtn;

@end

@implementation NITEventCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITEventModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)remindAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didRemindEvent:)])
    {
        [self.delegate didRemindEvent:self.model];
    }
}

- (IBAction)removeAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didRemoveEvent:)])
    {
        [self.delegate didRemoveEvent:self.model];
    }
}

- (IBAction)sendBouquetAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didSendBouquetForEvent:)])
    {
        [self.delegate didSendBouquetForEvent:self.model];
    }
}

- (IBAction)editeAction:(id)sender
{
    if (self.model.type == NITEventTypeUser)
    {
        if ([self.delegate respondsToSelector:@selector(didEditEvent:)])
        {
            [self.delegate didEditEvent:self.model];
        }
    }
}

#pragma mark - Private
- (void)updateData
{
    [self.image setImageWithURL:self.model.imageURL
               placeholderImage:self.model.placeholder];
    
    self.title.text = self.model.title;
    self.typeTitle.text = self.model.typeTitle;
    
    [self updateReminder];
}

- (void)updateReminder
{
    self.reminderIcon.hidden = self.model.reminderType != NITEventReminderDisable;
    self.reminderLbl.hidden = self.model.reminderType == NITEventReminderDisable;
    
    NSMutableAttributedString * attrText = [[NSMutableAttributedString alloc] initWithString:self.model.reminderTitle];
    
    [attrText addAttribute:NSFontAttributeName
                     value:[UIFont systemFontOfSize:14 weight:UIFontWeightRegular]
                     range:NSMakeRange(0, attrText.length)];
    
    [attrText addAttribute:NSUnderlineStyleAttributeName
                     value:@(1)
                     range:NSMakeRange(0, attrText.length)];
    
    [attrText addAttribute:NSForegroundColorAttributeName
                     value:RGB(10, 88, 43)
                     range:NSMakeRange(0, attrText.length)];
    
    self.reminderLbl.attributedText = attrText;
    self.removeBtn.hidden = self.model.type != NITEventTypeUser;
}

@end
