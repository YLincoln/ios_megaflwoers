//
//  NITCalendarViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCalendarViewController.h"
#import "FSCalendar.h"
#import "NITEventCell.h"
#import "FSCalendarHeaderView.h"

@interface NITCalendarViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
TOSearchBarDelegate,
FSCalendarDelegate,
FSCalendarDataSource,
NITEventCellDelegate
>

@property (nonatomic) IBOutlet FSCalendar * calendarView;
@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UITableView * searchTableView;
@property (nonatomic) IBOutlet UIButton * previousButton;
@property (nonatomic) IBOutlet UIButton * nextButton;
@property (nonatomic) IBOutlet TOSearchBar * searchBar;
@property (nonatomic) IBOutlet UIView * noResultsView;

@property (nonatomic) UIView * mainTitle;
@property (nonatomic) UIBarButtonItem * leftBarButtonItem;
@property (nonatomic) NSArray <UIBarButtonItem *> * rightBarButtonItems;

@property (nonatomic) NSCalendar * gregorian;
@property (nonatomic) NSArray <NITEventModel *> * items;
@property (nonatomic) NSArray <NITSearchEventModel *> * searchResult;
@property (nonatomic) NSDictionary * events;

@end

@implementation NITCalendarViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initMonthData];
}

#pragma mark - IBAction
- (IBAction)searchAction:(id)sender
{
    [UIView animateWithDuration:0.1 animations:^{} completion:^(BOOL finished) {
        
        self.navigationItem.rightBarButtonItems = nil;
        self.navigationItem.leftBarButtonItem  = nil;
        self.navigationItem.titleView = self.searchBar;
        self.navigationItem.hidesBackButton = true;
        self.searchTableView.hidden = false;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.searchBar.hidden = false;
                             self.noResultsView.hidden = true;
                         } completion:^(BOOL finished) {
                             [self.searchBar becomeFirstResponder];
                             [self.presenter searchByText:self.searchBar.text];
                         }];
    }];
}

- (IBAction)addAction:(id)sender
{
    [self.presenter createNewEvent];
}

- (IBAction)previousClicked:(id)sender
{
    NSDate * currentMonth = self.calendarView.currentPage;
    NSDate * previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendarView setCurrentPage:previousMonth animated:true];
}

- (IBAction)nextClicked:(id)sender
{
    NSDate * currentMonth = self.calendarView.currentPage;
    NSDate * nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendarView setCurrentPage:nextMonth animated:true];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Календарь", nil);
    
    [self setKeyboardActiv:true];
    
    // nav bar
    self.mainTitle = self.navigationItem.titleView;
    self.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
    self.rightBarButtonItems = self.navigationItem.rightBarButtonItems;
    
    // table
    [self.tableView setTableFooterView:[UIView new]];
    [self.searchTableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITEventCell) bundle:nil] forCellReuseIdentifier:_s(NITEventCell)];
    [self.searchTableView registerNib:[UINib nibWithNibName:_s(NITEventCell) bundle:nil] forCellReuseIdentifier:_s(NITEventCell)];
    
    // calendar
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    self.calendarView.dataSource = self;
    self.calendarView.delegate = self;
    self.calendarView.placeholderType = FSCalendarPlaceholderTypeNone;
    self.calendarView.appearance.headerMinimumDissolvedAlpha = 0;
    self.calendarView.appearance.headerDateFormat = [NSDateFormatter dateFormatFromTemplate:@"MMMM" options:0 locale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
    self.calendarView.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesUpperCase;
    self.calendarView.appearance.headerTitleColor = RGB(10, 88, 43); // month
    self.calendarView.appearance.weekdayTextColor = RGB(16, 16, 16); // days
    self.calendarView.appearance.titleDefaultColor = RGB(28, 28, 28); // date
    self.calendarView.appearance.titleSelectionColor = [UIColor whiteColor]; // selected date
    self.calendarView.appearance.titleWeekendColor = RGB(164, 183, 171); // weekend date
    self.calendarView.appearance.titlePlaceholderColor = [UIColor clearColor]; // date out current month
    self.calendarView.appearance.selectionColor = RGB(10, 88, 43); // selected date
    self.calendarView.appearance.todayColor = RGB(226, 150, 191); // selected date
    self.calendarView.appearance.eventDefaultColor = RGB(164, 183, 171); // event
    self.calendarView.appearance.eventSelectionColor = RGB(164, 183, 171); // selected event
    self.calendarView.appearance.titleFont = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium]; // date
    self.calendarView.appearance.weekdayFont = [UIFont systemFontOfSize:13 weight:UIFontWeightLight]; // days
    self.calendarView.appearance.headerTitleFont = [UIFont systemFontOfSize:13 weight:UIFontWeightLight]; // month
    
    [self.calendarView setScope:FSCalendarScopeMonth animated:false];
    [self.calendarView addSubview:self.previousButton];
    [self.calendarView addSubview:self.nextButton];
    
    [self.noResultsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.searchBar action:@selector(resignFirstResponder)]];
    
    [self.searchBar.searchTextField setTextColor:RGB(10, 88, 43)];
    [self.searchBar setDelegate:self];
}

#pragma mark - NITCalendarPresenterProtocol
- (void)reloadEventsData:(NSDictionary *)data withSelectedDate:(NSDate *)date
{
    self.events = data;
    [self.calendarView reloadData];
    [self.calendarView selectDate:date];
}

- (void)reloadTodayEventsData:(NSArray<NITEventModel *> *)data
{
    self.items = data;
    [self.tableView reloadData];
}

- (void)reloadSearchResultData:(NSArray <NITSearchEventModel *> *)data andSelectedIndex:(NSInteger)selectedIndex
{
    self.searchResult = data;
    self.searchTableView.backgroundColor = data.count > 0 ? [UIColor whiteColor] : RGBA(16, 16, 16, 0.4);
    [self.searchTableView reloadData];
    
    if (data.count > 0 && selectedIndex > 0)
    {
        [self.searchTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedIndex]
                                    atScrollPosition:UITableViewScrollPositionTop
                                            animated:false];
    }
    
    if ([self.searchBar.text length] > 0)
    {
        self.noResultsView.hidden = data.count > 0;
    }
    else
    {
        self.noResultsView.hidden = true;
    }
}

#pragma mark - FSCalendarDelegate
- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.calendarView.frame = RectSetHeight(self.calendarView.frame, bounds.size.height);
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.tableView)    return 1;
    else                                return [self.searchResult count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView)    return [self.items count];
    else                                return [[self.searchResult[section] events] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView)    return nil;
    else                                return [self.searchResult[section] headerView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITEventCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITEventCell) forIndexPath:indexPath];
    cell.model = (tableView == self.tableView) ? self.items[indexPath.row] : [self.searchResult[indexPath.section] events][indexPath.row];
    cell.delegate = self;

    return cell;
}

#pragma mark - TOSearchBarDelegate
- (void)searchBarCancelButtonClicked:(TOSearchBar *)searchBar
{
    [searchBar setText:@""];
    [self.presenter searchByText:searchBar.text];
    
    [UIView animateWithDuration:0.1f animations:^{
        self.searchBar.hidden = true;
        self.searchTableView.hidden = true;
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = self.mainTitle;
        self.navigationItem.rightBarButtonItems = self.rightBarButtonItems;
        self.navigationItem.leftBarButtonItem = self.leftBarButtonItem;
        self.navigationItem.hidesBackButton = true;
    }];
}

- (BOOL)searchBarShouldClear:(TOSearchBar *)searchBar
{
    [searchBar setText:@""];
    [self.presenter searchByText:searchBar.text];
    
    return true;
}

- (void)searchBar:(TOSearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.presenter searchByText:searchBar.text];
}
- (BOOL)searchBarShouldReturn:(TOSearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self.presenter searchByText:searchBar.text];
    
    return true;
}

#pragma mark - FSCalendarDataSource
- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    return [self.events[date.dateToday] count] > 0;
}

#pragma mark - FSCalendarDelegate
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    [self.presenter updateDataByDate:date];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    [self.presenter updateMonthDataByMonth:calendar.currentPage];
}

#pragma mark - NITEventCellDelegate
- (void)didRemindEvent:(NITEventModel *)event
{
    [self.presenter remindEvent:event];
}

- (void)didRemoveEvent:(NITEventModel *)event
{
    [self.presenter removeEvent:event];
}

- (void)didSendBouquetForEvent:(NITEventModel *)event
{
    [self.presenter sendBouquetForEvent:event];
}

- (void)didEditEvent:(NITEventModel *)event
{
    [self.presenter editEvent:event];
}

@end
