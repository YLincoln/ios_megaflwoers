//
//  NITCalendarViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITCalendarProtocols.h"

@interface NITCalendarViewController : NITBaseViewController <NITCalendarViewProtocol>

@property (nonatomic, strong) id <NITCalendarPresenterProtocol> presenter;

@end
