//
//  NITCalendarWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCalendarProtocols.h"
#import "NITCalendarViewController.h"
#import "NITCalendarLocalDataManager.h"
#import "NITCalendarAPIDataManager.h"
#import "NITCalendarInteractor.h"
#import "NITCalendarPresenter.h"
#import "NITCalendarWireframe.h"
#import "NITRootWireframe.h"

@interface NITCalendarWireFrame : NITRootWireframe <NITCalendarWireFrameProtocol>

@end
