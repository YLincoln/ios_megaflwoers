//
//  NITCalendarWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCalendarWireFrame.h"
#import "NITCreateEventWireFrame.h"
#import "NITCatalogDirectoryWireFrame.h"

@implementation NITCalendarWireFrame

+ (id)createNITCalendarModule
{
    // Generating module components
    id <NITCalendarPresenterProtocol, NITCalendarInteractorOutputProtocol> presenter = [NITCalendarPresenter new];
    id <NITCalendarInteractorInputProtocol> interactor = [NITCalendarInteractor new];
    id <NITCalendarAPIDataManagerInputProtocol> APIDataManager = [NITCalendarAPIDataManager new];
    id <NITCalendarLocalDataManagerInputProtocol> localDataManager = [NITCalendarLocalDataManager new];
    id <NITCalendarWireFrameProtocol> wireFrame= [NITCalendarWireFrame new];
    id <NITCalendarViewProtocol> view = [(NITCalendarWireFrame *)wireFrame createViewControllerWithKey:_s(NITCalendarViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITCalendarModuleFrom:(UIViewController *)fromViewController
{
    NITCalendarViewController * view = [NITCalendarWireFrame createNITCalendarModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showCreateEventFrom:(UIViewController *)fromViewController withDate:(NSDate *)date
{
    [NITCreateEventWireFrame presentNITCreateEventModuleFrom:fromViewController withDate:date];
}

- (void)showEditEvent:(NITEventModel *)model from:(UIViewController *)fromViewController
{
    [NITCreateEventWireFrame presentNITEditEventModuleFrom:fromViewController withEvent:model];
}

- (void)showSendBouqetFrom:(UIViewController *)fromViewController
{
    [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:fromViewController withTitle:NSLocalizedString(@"Букеты", nil) parentID:nil];
}

@end
