//
//  NITCalendarPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCalendarPresenter.h"
#import "NITCalendarWireframe.h"
#import "NITOrderModel.h"
#import "NSArray+BlocksKit.h"

@interface NITCalendarPresenter ()

@property (nonatomic) NSDate * selectedDate;
@property (nonatomic) NSMutableArray * searchResults;

@end

@implementation NITCalendarPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Data
- (void)initMonthData
{
    self.selectedDate = [NSDate date];
    
    weaken(self);
    [self.interactor initDataWithHandler:^(NSDictionary *result) {
        [weakSelf.view reloadEventsData:result withSelectedDate:weakSelf.selectedDate];
        [weakSelf updateDataByDate:weakSelf.selectedDate];
    }];
}

- (void)updateData
{
    [self updateMonthDataByMonth:self.selectedDate];
}

- (void)updateMonthDataByMonth:(NSDate *)month
{
    weaken(self);
    self.selectedDate = [month dateMonthStart];
    [self.interactor updateDataByStartDate:self.selectedDate withHandler:^(NSDictionary *result) {
        
        if ([[month dateMonthStart] isEqualToDate:weakSelf.selectedDate])
        {
            [weakSelf.view reloadEventsData:result withSelectedDate:weakSelf.selectedDate];
            [weakSelf updateDataByDate:weakSelf.selectedDate];
        }
    }];
}

- (void)updateDataByDate:(NSDate *)date
{
    self.selectedDate = date;
    [self.view reloadTodayEventsData:[self.interactor eventsForDate:self.selectedDate]];
}

- (void)searchByText:(NSString *)text
{
    weaken(self);
    [self.interactor searchByText:text withHandler:^(NSArray<NITSearchEventModel *> *result, NSInteger selectedIndex) {
        weakSelf.searchResults = result.mutableCopy;
        [weakSelf.view reloadSearchResultData:result andSelectedIndex:selectedIndex];
    }];
}

- (void)createNewEvent
{
    [self.wireFrame showCreateEventFrom:self.view withDate:self.selectedDate];
}

- (void)editEvent:(NITEventModel *)model
{
    [self.wireFrame showEditEvent:model from:self.view];
}

- (void)remindEvent:(NITEventModel *)model
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSString * action1Title = NSLocalizedString(@"Не напоминать", nil);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:action1Title actionBlock:^{
        [weakSelf remindEvent:model byType:NITEventReminderDisable];
    }];
    
    NSString * action2Title = NSLocalizedString(@"Напомнить за неделю", nil);
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:action2Title actionBlock:^{
        [weakSelf remindEvent:model byType:NITEventReminderOneWeek];
    }];
    
    NSString * action3Title = NSLocalizedString(@"Напомнить за день", nil);
    NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:action3Title actionBlock:^{
        [weakSelf remindEvent:model byType:NITEventReminderOneDay];
    }];
    
    /*NSString * action4Title = NSLocalizedString(@"Напомнить за час", nil);
    NITActionButton * action4 = [[NITActionButton alloc] initWithTitle:action4Title actionBlock:^{
        [weakSelf remindEvent:model byType:NITEventReminderOneHour];
    }];*/
    
    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1,action2,action3] cancelButton:cancel];
}

- (void)remindEvent:(NITEventModel *)model byType:(NITEventReminderType)type
{
    [self.interactor remindEvent:model byType:type];
    [self updateMonthDataByMonth:self.selectedDate];
}

- (void)removeEvent:(NITEventModel *)model
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSString * action1Title = NSLocalizedString(@"Удалить", nil);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:action1Title actionBlock:^{
        
        [weakSelf.interactor removeEvent:model];
        [weakSelf updateMonthDataByMonth:weakSelf.selectedDate];
        
        weakSelf.searchResults = [[weakSelf.searchResults bk_map:^id(NITSearchEventModel *obj) {
            obj.events = [obj.events bk_select:^BOOL(NITEventModel *obj) {
                return ![obj.identifier isEqualToString:model.identifier];
            }];
            return obj.events.count > 0 ? obj : nil;
        }] bk_select:^BOOL(id obj) {
            return obj != [NSNull null];
        }].mutableCopy;
        
        [weakSelf.view reloadSearchResultData:weakSelf.searchResults andSelectedIndex:-1];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Вы действительно хотите удалить событие?", nil) actionButtons:@[action1] cancelButton:cancel];
}

- (void)sendBouquetForEvent:(NITEventModel *)model
{
    NITOrderModel * order = [NITOrderModel new];
    order.deliveryDate = model.startDate;
    order.senderName = model.title;
    order.receiverName = model.title;

    [self.wireFrame showSendBouqetFrom:self.view];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveEventNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
