//
//  NITCalendarPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCalendarProtocols.h"

@class NITCalendarWireFrame;

@interface NITCalendarPresenter : NITRootPresenter <NITCalendarPresenterProtocol, NITCalendarInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCalendarViewProtocol> view;
@property (nonatomic, strong) id <NITCalendarInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCalendarWireFrameProtocol> wireFrame;

@end
