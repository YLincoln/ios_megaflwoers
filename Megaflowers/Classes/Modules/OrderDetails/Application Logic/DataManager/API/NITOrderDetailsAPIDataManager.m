//
//  NITOrderDetailsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsAPIDataManager.h"
#import "SWGOrderApi.h"

@implementation NITOrderDetailsAPIDataManager

- (void)getUserOrderByID:(NSNumber *)orderID withHandler:(void(^)(SWGOrder *result))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderIdGetWithId:orderID lang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(SWGOrder *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

- (void)getOrderTimeListForDate:(NSDate *)date bouquetSkusString:(NSString *)bouquetSkusString withHandler:(void(^)(NSArray<SWGOrderTimeList *> * output))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderTimeListGetWithLang:HELPER.lang
                                           site:API.site
                                           city:USER.cityID
                                           date:[date serverString]
                                         skuIds:bouquetSkusString
                                           full:@(true)
                              completionHandler:^(NSArray<SWGOrderTimeList> *output, NSError *error) {
                                  [HELPER stopLoading];
                                  if (error)
                                  {
                                      [HELPER logError:error method:METHOD_NAME];
                                  }
                                  handler(output);
                              }];
}

@end
