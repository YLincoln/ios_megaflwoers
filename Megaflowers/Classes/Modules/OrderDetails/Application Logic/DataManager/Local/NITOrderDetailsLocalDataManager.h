//
//  NITOrderDetailsLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrderDetailsProtocols.h"

@interface NITOrderDetailsLocalDataManager : NSObject <NITOrderDetailsLocalDataManagerInputProtocol>

@end
