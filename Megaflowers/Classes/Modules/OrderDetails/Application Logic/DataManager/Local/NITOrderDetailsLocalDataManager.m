//
//  NITOrderDetailsLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsLocalDataManager.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"

@implementation NITOrderDetailsLocalDataManager

- (void)addCatalogItemToBasket:(NITBasketProductModel *)model
{
    if (model)
    {
        switch (model.contentType) {
            case ProductContentTypeBouqets: [NITBusketBouquet addBouquet:model];        break;
            case ProductContentTypeAttach:  [NITBusketAttachment addAttachment:model];  break;
        }
    }
}


@end
