//
//  NITOrderDetailsAboutModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@interface NITOrderDetailsAboutModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NSString * value;

- (instancetype)initWithTitle:(NSString *)title andValue:(NSString *)value;

@end
