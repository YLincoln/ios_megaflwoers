//
//  NITOrderDetailsAboutModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsAboutModel.h"

@implementation NITOrderDetailsAboutModel

- (instancetype)initWithTitle:(NSString *)title andValue:(NSString *)value
{
    if (self = [super init])
    {
        self.title = title;
        self.value = value;
    }
    
    return self;
}

@end
