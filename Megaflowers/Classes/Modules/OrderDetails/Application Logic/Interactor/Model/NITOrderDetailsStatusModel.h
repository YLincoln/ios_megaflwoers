//
//  NITOrderDetailsStatusModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITMyOrderModel.h"

@interface NITOrderDetailsStatusModel : NSObject

@property (nonatomic) OrdersPaymentStatus paymentStatus;
@property (nonatomic) OrdersProcessStatus processStatus;
@property (nonatomic) NSNumber * additionalPrice;
@property (nonatomic) BOOL paymentTypeCash;

@end
