//
//  NITOrderDetailsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITBillModel.h"
#import "SWGBill.h"
#import "NITPaymentOrderAPIDataManager.h"
#import "NITDeliveryMethodModel.h"
#import "NITBasketInteractor.h"
#import "NITBasketLocalDataManager.h"
#import "NITProductDetailsAPIDataManager.h"
#import "SWGBasketRequest.h"

@implementation NITOrderDetailsInteractor

- (void)getOrderDetailsByModel:(NITMyOrderModel *)model
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        if (success)
        {
            weaken(self);
            [self.APIDataManager getUserOrderByID:model.identifier withHandler:^(SWGOrder *result) {
                
                NITPaymentOrderAPIDataManager * api = [NITPaymentOrderAPIDataManager new];
                [api getOrderBillsByOrderID:model.identifier orderHash:model.orderHash withHandler:^(NSArray<SWGBill> *output) {
                    
                    NSArray <NITBillModel *> * bills = [[output bk_map:^id(SWGBill *obj) {
                        return [[NITBillModel alloc] initWithModel:obj];
                    }] bk_select:^BOOL(NITBillModel *obj) {
                        return obj.status == BillStatusNew && obj.payment.paymentType == PaymentTypeCash;
                    }];
                    
                    NITMyOrderModel * order = [[NITMyOrderModel alloc] initWithOrder:result];
                    order.paymentTypeCash =  bills.count > 0;

                    [weakSelf.presenter updateOrderModel:order];
                }];
            }];
        }
    }];
}

- (void)getTimeForOrder:(NITMyOrderModel *)model withHandler:(void (^)(NSString *))handler
{
    if (model.deliveryDate && model.deliveryTime)
    {
        NSArray * skusArray = [[model.items bk_select:^BOOL(NITBasketProductModel *obj) {
            return obj.contentType == ProductTypeBouquet;
        }] bk_map:^id(NITBasketProductModel *obj) {
            return obj.skuIdentifier;
        }];
        NSString * bouquetSkusString = [skusArray componentsJoinedByString:@","];
        
        weaken(self);
        [self.APIDataManager getOrderTimeListForDate:model.deliveryDate bouquetSkusString:bouquetSkusString withHandler:^(NSArray<SWGOrderTimeList *> *output) {
            handler([weakSelf intervalTitleByID:model.deliveryTime entities:output]);
        }];
    }
    else
    {
        handler(@"");
    }
}

- (void)getOrderTimeForDate:(NSDate *)date timeID:(NSNumber *)timeID withHandler:(void (^)(NSString *))handler
{
    if (date && timeID)
    {
        SWGBasketRequest * basket = [API createBasket];
        NSArray * skusArray = [basket.bouquets bk_map:^id(SWGBasketRequestBouquets *obj) {
            return obj.skuId.stringValue;
        }];
        NSString * bouquetSkusString = [skusArray componentsJoinedByString:@","];
        
        weaken(self);
        [self.APIDataManager getOrderTimeListForDate:date bouquetSkusString:bouquetSkusString withHandler:^(NSArray<SWGOrderTimeList *> *output) {
            handler([weakSelf intervalTitleByID:timeID entities:output]);
        }];
    }
    else
    {
        handler(@"");
    }
}

- (void)addCatalogItemToBasket:(NITBasketProductModel *)model
{
    [self.localDataManager addCatalogItemToBasket:model];
}

- (void)repeatOrder:(NITMyOrderModel *)orderModel withHandler:(void(^)())handler
{
    NITBasketInteractor * basketInteractor = [NITBasketInteractor new];
    basketInteractor.localDataManager = [NITBasketLocalDataManager new];
    [basketInteractor cleanBasket];
    
    [self.APIDataManager getUserOrderByID:orderModel.identifier withHandler:^(SWGOrder *order) {
        
        NITOrderModel * model = [NITOrderModel new];
        
        model.senderName                = order.senderName;
        model.senderName                = order.senderName;
        model.senderPhone               = order.senderPhone;
        model.senderEmail               = order.senderEmail;
        model.receiverName              = order.receiverName;
        model.receiverPhone             = order.receiverPhone;
        model.receiverAddress           = order.receiverAddress;
        model.deliveryDate              = [NSDate dateFromServerString:order.deliveryDate];
        model.takePhoto                 = order.takePhoto.boolValue;
        model.leaveNeighbors            = order.leaveNeighbors;
        model.dontCallReceiver          = order.dontCallReceiver;
        model.callMeBackToConfirmOrder  = !order.dontDisturbSender.boolValue;
        model.deliverAnonymously        = !order.notifyAboutDelivery.boolValue;
        model.knowOnlyPhone             = order.knowOnlyPhone.boolValue;
        model.forMe                     = order.forMe.boolValue;
        model.postcardText              = order.postcardText;
        model.deliverySalonId           = order.deliverySalonId;
        model.deliveryType              = order.isPickup.boolValue ? DeliveryMethodTypePickup : DeliveryMethodTypeCourier;
        
        [NITBusketBouquet addBouquets:order.basket.bouquets];
        [NITBusketAttachment addAttachments:order.basket.attaches];
        [API validateBasket];
        
        if (handler) handler();
    }];
}

#pragma mark - Private
- (NSString *)intervalTitleByID:(NSNumber *)intervalID entities:(NSArray <SWGOrderTimeList *> *)entities
{
    SWGOrderTimeList * item = [entities bk_select:^BOOL(SWGOrderTimeList *obj) {
        return [obj._id isEqualToNumber:intervalID];
    }].firstObject;
    
    return item.option;
}

@end
