//
//  NITOrderDetailsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrderDetailsProtocols.h"

@interface NITOrderDetailsInteractor : NSObject <NITOrderDetailsInteractorInputProtocol>

@property (nonatomic, weak) id <NITOrderDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITOrderDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITOrderDetailsLocalDataManagerInputProtocol> localDataManager;

@end
