//
//  NITOrderDetailsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITMyOrderModel.h"
#import "NITOrderDetailsAboutModel.h"
#import "NITOrderDetailsStatusModel.h"
#import "NITBasketProductModel.h"
#import "SWGOrderTimeList.h"
#import "SWGOrder.h"

@protocol NITOrderDetailsInteractorOutputProtocol;
@protocol NITOrderDetailsInteractorInputProtocol;
@protocol NITOrderDetailsViewProtocol;
@protocol NITOrderDetailsPresenterProtocol;
@protocol NITOrderDetailsLocalDataManagerInputProtocol;
@protocol NITOrderDetailsAPIDataManagerInputProtocol;

@class NITOrderDetailsWireFrame;

typedef CF_ENUM (NSUInteger, OrderDetailsSectionType) {
    OrderDetailsSectionTypeAbout    = 0,
    OrderDetailsSectionTypeFeedback = 1,
    OrderDetailsSectionTypeStatus   = 2,
    OrderDetailsSectionTypeProduct  = 3
};

@protocol NITOrderDetailsViewProtocol
@required
@property (nonatomic, strong) id <NITOrderDetailsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadAboutSectionByModels:(NSArray <NITOrderDetailsAboutModel *> *)models;
- (void)reloadStatusSectionByModels:(NSArray <NITOrderDetailsStatusModel *> *)models;
- (void)reloadProductSectionByModels:(NSArray <NITBasketProductModel *> *)models;
@end

@protocol NITOrderDetailsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITOrderDetailsModuleFrom:(id)fromView withModel:(NITMyOrderModel *)model;
- (void)showCallOrderFrom:(id)fromView;
- (void)showLeaveFeedbackForOrder:(NITMyOrderModel *)model from:(id)fromView;
- (void)showPaymentOrderFrom:(id)fromView forOrderID:(NSNumber *)orderID;
- (void)showBasketFrom:(id)fromView;
@end

@protocol NITOrderDetailsPresenterProtocol
@required
@property (nonatomic, weak) id <NITOrderDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITOrderDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITOrderDetailsWireFrameProtocol> wireFrame;
@property (nonatomic) NITMyOrderModel * model;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)paymentAction;
- (void)callAction;
- (void)showLeaveFeedback;
- (void)addCatalogItemToBasket:(NITBasketProductModel *)model;
- (void)repeatOrder;
@end

@protocol NITOrderDetailsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateOrderModel:(NITMyOrderModel *)model;
@end

@protocol NITOrderDetailsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITOrderDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITOrderDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITOrderDetailsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getOrderDetailsByModel:(NITMyOrderModel *)model;
- (void)getTimeForOrder:(NITMyOrderModel *)model withHandler:(void (^)(NSString *))handler;
- (void)addCatalogItemToBasket:(NITBasketProductModel *)model;
- (void)repeatOrder:(NITMyOrderModel *)model withHandler:(void(^)())handler;
@end


@protocol NITOrderDetailsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITOrderDetailsAPIDataManagerInputProtocol <NITOrderDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserOrderByID:(NSNumber *)orderID withHandler:(void(^)(SWGOrder *result))handler;
- (void)getOrderTimeListForDate:(NSDate *)date bouquetSkusString:(NSString *)bouquetSkusString withHandler:(void(^)(NSArray<SWGOrderTimeList *> * output))handler;
@end

@protocol NITOrderDetailsLocalDataManagerInputProtocol <NITOrderDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)addCatalogItemToBasket:(NITBasketProductModel *)model;
@end
