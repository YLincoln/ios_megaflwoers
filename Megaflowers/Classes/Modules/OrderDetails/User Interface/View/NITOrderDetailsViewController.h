//
//  NITOrderDetailsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITOrderDetailsProtocols.h"

@interface NITOrderDetailsViewController : NITBaseViewController <NITOrderDetailsViewProtocol>

@property (nonatomic, strong) id <NITOrderDetailsPresenterProtocol> presenter;

@end
