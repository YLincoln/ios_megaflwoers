//
//  NITOrderDetailsProductCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITBasketProductModel;

@protocol NITOrderDetailsProductCellDelegate <NSObject>

- (void)didAddToBasket:(NITBasketProductModel *)model;
- (void)didRepeatOrder;

@end

@interface NITOrderDetailsProductCell : UITableViewCell

@property (nonatomic) NITBasketProductModel * model;
@property (nonatomic, weak) id <NITOrderDetailsProductCellDelegate> delegate;

@end
