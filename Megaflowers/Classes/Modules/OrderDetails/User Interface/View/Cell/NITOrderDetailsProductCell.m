//
//  NITOrderDetailsProductCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsProductCell.h"
#import "NITBasketProductModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITOrderDetailsProductCell ()

@property (nonatomic) IBOutlet UIImageView * image;
@property (nonatomic) IBOutlet UILabel * name;
@property (nonatomic) IBOutlet UILabel * skuName;
@property (nonatomic) IBOutlet UILabel * price;
@property (nonatomic) IBOutlet UIButton * basketBtn;
@property (nonatomic) IBOutlet UIImageView * editedImg;

@end

@implementation NITOrderDetailsProductCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)setModel:(NITBasketProductModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)addToBasket:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didAddToBasket:)])
    {
        [self.delegate didAddToBasket:self.model];
    }
}

- (IBAction)repeatOrder:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didRepeatOrder)])
    {
        [self.delegate didRepeatOrder];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.name.text = self.model.name;
    self.skuName.text = self.model.skuName;
    self.price.text = [NSString stringWithFormat:@"%@ %@", self.model.skuPrice, NSLocalizedString(@"руб.", nil)];
    
    self.image.image = nil;
    [self.image setImageWithURL:[NSURL URLWithString:self.model.skuImagePath]];
    
    self.editedImg.hidden = !self.model.edited;
    
    [self updateBasketState];
}

- (void)updateBasketState
{
    NSString * imgaeName = self.model.inBasket ? @"ic_in_basket_active" : @"ic_in_basket";
    [self.basketBtn setImage:[UIImage imageNamed:imgaeName] forState:UIControlStateNormal];
    [self.basketBtn setEnabled:!self.model.inBasket];
}

@end
