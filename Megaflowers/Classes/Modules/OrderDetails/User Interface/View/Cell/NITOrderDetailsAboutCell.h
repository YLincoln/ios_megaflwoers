//
//  NITOrderDetailsAboutCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITOrderDetailsAboutModel;

@interface NITOrderDetailsAboutCell : UITableViewCell

@property (nonatomic) NITOrderDetailsAboutModel * model;

@end
