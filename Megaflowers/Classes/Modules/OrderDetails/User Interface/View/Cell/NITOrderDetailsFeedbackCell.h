//
//  NITOrderDetailsFeedbackCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/23/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@protocol NITOrderDetailsFeedbackCellDelegate <NSObject>

- (void)didSendFeedback;

@end

@interface NITOrderDetailsFeedbackCell : UITableViewCell

@property (nonatomic) id <NITOrderDetailsFeedbackCellDelegate> delegate;

@end
