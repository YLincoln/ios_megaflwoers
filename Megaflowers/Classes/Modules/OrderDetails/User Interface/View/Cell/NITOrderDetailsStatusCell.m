//
//  NITOrderDetailsStatusCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsStatusCell.h"
#import "NITOrderDetailsStatusModel.h"

@interface NITOrderDetailsStatusCell ()

@property (nonatomic) IBOutlet UIProgressView * progress;
@property (nonatomic) IBOutlet UILabel * orderLbl;
@property (nonatomic) IBOutlet UILabel * bouquetLbl;
@property (nonatomic) IBOutlet UILabel * courierLbl;
@property (nonatomic) IBOutlet UIImageView * paymentIcon;
@property (nonatomic) IBOutlet UILabel * paymentLbl;

@end

@implementation NITOrderDetailsStatusCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITOrderDetailsStatusModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    switch (self.model.processStatus) {
        case OrdersProcessStatusNone:
            self.progress.progress = 0.0;
            self.orderLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            self.bouquetLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            self.courierLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            break;
            
        case OrdersProcessStatusOrder:
            self.progress.progress = 0.3;
            self.orderLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
            self.bouquetLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            self.courierLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            break;
            
        case OrdersProcessStatusBouquet:
            self.progress.progress = 0.5;
            self.orderLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            self.bouquetLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
            self.courierLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            break;
            
        case OrdersProcessStatusCourier:
            self.progress.progress = 1.0;
            self.orderLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            self.bouquetLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
            self.courierLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
            break;
    }
    
    switch (self.model.paymentStatus) {
        case OrdersPaymentStatusNotPaid:
            self.paymentIcon.image = [UIImage imageNamed:@"ic_not_paid"];
            self.paymentLbl.text = NSLocalizedString(@"Не оплачен", nil);
            self.paymentLbl.textColor = RGB(227, 104, 104);
            break;
            
        case OrdersPaymentStatusPartPaid:
            
            self.paymentIcon.image = [UIImage imageNamed:@"ic_partially"];
            self.paymentLbl.text = [NSString stringWithFormat:@"%@ %@ %@",
                                    NSLocalizedString(@"Заказ оплачен частично. Необходимо доплатить", nil),
                                    self.model.additionalPrice,
                                    NSLocalizedString(@"руб.", nil)];
            self.paymentLbl.textColor = RGB(10, 88, 43);
            break;
            
        case OrdersPaymentStatusPaid:
            self.paymentIcon.image = [UIImage imageNamed:@"ic_check"];
            self.paymentLbl.text = NSLocalizedString(@"Оплачен", nil);
            self.paymentLbl.textColor = RGB(10, 88, 43);
            break;
            
        case OrdersPaymentStatusOverpaid:
            self.paymentIcon.image = [UIImage imageNamed:@"ic_check"];
            self.paymentLbl.text = [NSString stringWithFormat:@"%@ %@ %@",
                                    NSLocalizedString(@"Переплата по заказу", nil),
                                    self.model.additionalPrice,
                                    NSLocalizedString(@"руб.", nil)];
            self.paymentLbl.textColor = RGB(10, 88, 43);
            break;
    }
}

@end
