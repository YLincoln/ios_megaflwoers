//
//  NITOrderDetailsFeedbackCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/23/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsFeedbackCell.h"

@implementation NITOrderDetailsFeedbackCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

#pragma mark - IBAction
- (IBAction)sendFeedbackAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didSendFeedback)])
    {
        [self.delegate didSendFeedback];
    }
}

@end
