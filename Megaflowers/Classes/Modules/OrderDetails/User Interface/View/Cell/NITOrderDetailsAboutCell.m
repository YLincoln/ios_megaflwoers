//
//  NITOrderDetailsAboutCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/7/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsAboutCell.h"
#import "NITOrderDetailsAboutModel.h"

@interface NITOrderDetailsAboutCell ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * value;
@property (nonatomic) IBOutlet NSLayoutConstraint * titleW;
@property (nonatomic) IBOutlet NSLayoutConstraint * valueW;

@end

@implementation NITOrderDetailsAboutCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITOrderDetailsAboutModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.value.text = self.model.value;
    
    self.titleW.constant = [self.title sizeOfMultiLineLabel].width;
    self.valueW.constant = [self.value sizeOfMultiLineLabel].width;
}

@end
