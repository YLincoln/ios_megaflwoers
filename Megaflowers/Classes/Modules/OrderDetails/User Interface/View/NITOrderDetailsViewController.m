//
//  NITOrderDetailsViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsViewController.h"
#import "NITOrderDetailsAboutCell.h"
#import "NITOrderDetailsStatusCell.h"
#import "NITOrderDetailsProductCell.h"
#import "NITOrderDetailsFeedbackCell.h"
#import "UIView+Screenshot.h"
#import "UIView+Genie.h"


@interface NITOrderDetailsViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITOrderDetailsFeedbackCellDelegate,
NITOrderDetailsProductCellDelegate
>

@property (nonatomic) IBOutlet UIButton * paymemtBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * paymemtBtnH;
@property (nonatomic) IBOutlet UITableView * tableView;

@property (nonatomic) NSArray <NITOrderDetailsAboutModel *> * aboutItems;
@property (nonatomic) NSArray <NITOrderDetailsStatusModel *> * statusItems;
@property (nonatomic) NSArray <NITBasketProductModel *> * productItems;

@end

@implementation NITOrderDetailsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)callAction:(id)sender
{
    [self.presenter callAction];
}

- (IBAction)paymentAction:(id)sender
{
    [self.presenter paymentAction];
}

#pragma mark - Private
- (void)initUI
{
    self.title = @"";
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView setContentInset:UIEdgeInsetsMake(10, 0, 0, 0)];
    [self.tableView registerNibArray:@[_s(NITOrderDetailsAboutCell), _s(NITOrderDetailsStatusCell), _s(NITOrderDetailsProductCell), _s(NITOrderDetailsFeedbackCell)]];
}

- (void)reloadTableSection:(NSInteger)section
{
    [self.tableView beginUpdates];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void)animateItemToCartByIndex:(NSInteger)index
{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:OrderDetailsSectionTypeProduct];
    UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    CGRect cellFrameInSuperview = [self.tableView convertRect:[self.tableView rectForRowAtIndexPath:indexPath] toView:[self.tableView superview]];
    
    UIImageView * genieView = [[UIImageView alloc] initWithFrame:cellFrameInSuperview];
    genieView.image = [cell imageByRenderingView];
    [self.view addSubview:genieView];
    
    CGPoint endPoint = [WINDOW basketCenter];
    CGRect endRect = CGRectMake(endPoint.x, endPoint.y, 1, 1);
    
    [genieView genieInTransitionWithDuration:.6f destinationRect:endRect destinationEdge:BCRectEdgeTop completion:^{
        [genieView removeFromSuperview];
    }];
}

#pragma mark - NITOrderDetailsPresenterProtocol
- (void)reloadAboutSectionByModels:(NSArray<NITOrderDetailsAboutModel *> *)models
{
    self.aboutItems = models;
    [self.tableView reloadData];
}

- (void)reloadStatusSectionByModels:(NSArray<NITOrderDetailsStatusModel *> *)models
{
    self.statusItems = models;
    [self.tableView reloadData];
    
    if (models.count > 0)
    {
        NITOrderDetailsStatusModel * model = models.firstObject;
        NSString * title = @"";
        switch (model.paymentStatus) {
            case OrdersPaymentStatusNotPaid:
                self.paymemtBtnH.constant = 57;
                title = NSLocalizedString(@"Оплатить заказ", nil);
                break;
                
            case OrdersPaymentStatusPartPaid:
                self.paymemtBtnH.constant = 57;
                title = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"Доплатить", nil), model.additionalPrice, NSLocalizedString(@"руб.", nil)];
                break;
             
            case OrdersPaymentStatusOverpaid:
                self.paymemtBtnH.constant = 0;
                title = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"Переплата по заказу", nil), model.additionalPrice, NSLocalizedString(@"руб.", nil)];
                break;
            case OrdersPaymentStatusPaid:
                self.paymemtBtnH.constant = 0;
                
                break;
        }
        
        if (self.paymemtBtnH.constant > 0 && model.paymentTypeCash)
        {
            self.paymemtBtnH.constant = 0;
        }

        [self.paymemtBtn setTitle:title forState:UIControlStateNormal];
        [self.view layoutIfNeeded];
    }
    else
    {
        self.paymemtBtnH.constant = 0;
        [self.view layoutIfNeeded];
    }
}

- (void)reloadProductSectionByModels:(NSArray<NITBasketProductModel *> *)models
{
    self.productItems = models;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    OrderDetailsSectionType type = section;
    switch (type) {
        case OrderDetailsSectionTypeAbout:
            return [self.aboutItems count];
        case OrderDetailsSectionTypeFeedback:
            if (!self.statusItems)  return 0;
            else                    return [self.statusItems count] > 0 ? 0 : 1;
        case OrderDetailsSectionTypeStatus:
            return [self.statusItems count];
        case OrderDetailsSectionTypeProduct:
            return [self.productItems count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    OrderDetailsSectionType type = section;
    switch (type) {
        case OrderDetailsSectionTypeAbout:
            return 0;
        case OrderDetailsSectionTypeFeedback:
            return [self.statusItems count] > 0 ? 0 : 16;
        case OrderDetailsSectionTypeStatus:
            return 5;
        case OrderDetailsSectionTypeProduct:
            return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailsSectionType type = indexPath.section;
    switch (type) {
        case OrderDetailsSectionTypeAbout:
            return 27;
        case OrderDetailsSectionTypeFeedback:
            return 27;
        case OrderDetailsSectionTypeStatus:
            return 92;
        case OrderDetailsSectionTypeProduct:
            return 200;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailsSectionType type = indexPath.section;
    switch (type) {
        case OrderDetailsSectionTypeAbout:
        {
            NITOrderDetailsAboutCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITOrderDetailsAboutCell) forIndexPath:indexPath];
            cell.model = self.aboutItems[indexPath.row];
            return cell;
        }
        case OrderDetailsSectionTypeFeedback:
        {
            NITOrderDetailsFeedbackCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITOrderDetailsFeedbackCell) forIndexPath:indexPath];
            cell.delegate = self;
            return cell;
        }
        case OrderDetailsSectionTypeStatus:
        {
            NITOrderDetailsStatusCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITOrderDetailsStatusCell) forIndexPath:indexPath];
            cell.model = self.statusItems[indexPath.row];
            return cell;
        }
        case OrderDetailsSectionTypeProduct:
        {
            NITOrderDetailsProductCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITOrderDetailsProductCell) forIndexPath:indexPath];
            cell.model = self.productItems[indexPath.row];
            cell.delegate = self;
            return cell;
        }
    }
}

#pragma mark - NITOrderDetailsFeedbackCellDelegate
- (void)didSendFeedback
{
    [self.presenter showLeaveFeedback];
}

#pragma mark - NITOrderDetailsProductCellDelegate
- (void)didAddToBasket:(NITBasketProductModel *)model
{
    [self.presenter addCatalogItemToBasket:model];
    NSInteger index = [self.productItems indexOfObject:model];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:OrderDetailsSectionTypeProduct]] withRowAnimation:UITableViewRowAnimationNone];
    [self animateItemToCartByIndex:index];
}

- (void)didRepeatOrder
{
    [self.presenter repeatOrder];
}

@end
