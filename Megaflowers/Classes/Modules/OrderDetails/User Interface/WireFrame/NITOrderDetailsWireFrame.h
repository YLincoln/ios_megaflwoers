//
//  NITOrderDetailsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsProtocols.h"
#import "NITOrderDetailsViewController.h"
#import "NITOrderDetailsLocalDataManager.h"
#import "NITOrderDetailsAPIDataManager.h"
#import "NITOrderDetailsInteractor.h"
#import "NITOrderDetailsPresenter.h"
#import "NITOrderDetailsWireframe.h"
#import "NITRootWireframe.h"

@interface NITOrderDetailsWireFrame : NITRootWireframe <NITOrderDetailsWireFrameProtocol>

@end
