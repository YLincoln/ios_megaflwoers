//
//  NITOrderDetailsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsWireFrame.h"
#import "NITCallOrderWireFrame.h"
#import "NITRateDeliveryWireFrame.h"
#import "NITPaymentOrderWireFrame.h"

@implementation NITOrderDetailsWireFrame

+ (id)createNITOrderDetailsModule
{
    // Generating module components
    id <NITOrderDetailsPresenterProtocol, NITOrderDetailsInteractorOutputProtocol> presenter = [NITOrderDetailsPresenter new];
    id <NITOrderDetailsInteractorInputProtocol> interactor = [NITOrderDetailsInteractor new];
    id <NITOrderDetailsAPIDataManagerInputProtocol> APIDataManager = [NITOrderDetailsAPIDataManager new];
    id <NITOrderDetailsLocalDataManagerInputProtocol> localDataManager = [NITOrderDetailsLocalDataManager new];
    id <NITOrderDetailsWireFrameProtocol> wireFrame= [NITOrderDetailsWireFrame new];
    id <NITOrderDetailsViewProtocol> view = [(NITOrderDetailsWireFrame *)wireFrame createViewControllerWithKey:_s(NITOrderDetailsViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITOrderDetailsModuleFrom:(UIViewController *)fromViewController withModel:(NITMyOrderModel *)model
{
    NITOrderDetailsViewController * view = [NITOrderDetailsWireFrame createNITOrderDetailsModule];
    view.presenter.model = model;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showCallOrderFrom:(UIViewController *)fromViewController
{
    [NITCallOrderWireFrame presentNITCallOrderModuleFrom:fromViewController];
}

- (void)showLeaveFeedbackForOrder:(NITMyOrderModel *)model from:(UIViewController *)fromViewController
{
    [NITRateDeliveryWireFrame presentNITRateDeliveryModuleFrom:fromViewController orderId:model.identifier];
}

- (void)showPaymentOrderFrom:(UIViewController *)fromViewController forOrderID:(NSNumber *)orderID
{
    [NITPaymentOrderWireFrame presentNITPaymentOrderModuleFrom:fromViewController forOrderID:orderID];
}

- (void)showBasketFrom:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:1];
}

@end
