//
//  NITOrderDetailsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrderDetailsProtocols.h"

@class NITOrderDetailsWireFrame;

@interface NITOrderDetailsPresenter : NITRootPresenter <NITOrderDetailsPresenterProtocol, NITOrderDetailsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITOrderDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITOrderDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITOrderDetailsWireFrameProtocol> wireFrame;
@property (nonatomic) NITMyOrderModel * model;

@end
