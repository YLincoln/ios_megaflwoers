//
//  NITOrderDetailsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDetailsPresenter.h"
#import "NITOrderDetailsWireframe.h"

@implementation NITOrderDetailsPresenter

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self initData];
}

#pragma mark -
- (void)initData
{
    [self.interactor getOrderDetailsByModel:self.model];
}

- (void)callAction
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Заказ звонка", nil) actionBlock:^{
        [weakSelf.wireFrame showCallOrderFrom:weakSelf.view];
    }];
    
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Сделать заказ по телефону", nil) actionBlock:^{
        [PHONE callToPhoneNumber:USER.cityPhone];
        [TRACKER trackEvent:TrackerEventOrderByPhone];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1, action2] cancelButton:cancel];
}

- (void)paymentAction
{
    switch (self.model.paymentStatus) {
        case OrdersPaymentStatusNotPaid:
        case OrdersPaymentStatusPartPaid:
            [self.wireFrame showPaymentOrderFrom:self.view forOrderID:self.model.identifier];
            break;
            
        default:
            break;
    }
}

- (void)showLeaveFeedback
{
    [self.wireFrame showLeaveFeedbackForOrder:self.model from:self.view];
}

- (void)addCatalogItemToBasket:(NITBasketProductModel *)model
{
    [self.interactor addCatalogItemToBasket:model];
}

- (void)repeatOrder
{
    [self.interactor repeatOrder:self.model withHandler:^{
        [self.wireFrame showBasketFrom:self.view];
    }];
}

#pragma mark - Private
- (void)updateAboutOrderData
{
    weaken(self);
    [self.interactor getTimeForOrder:self.model withHandler:^(NSString * interval) {
        
        NSMutableArray * data = [NSMutableArray new];
        
        [data addObject:[[NITOrderDetailsAboutModel alloc] initWithTitle:NSLocalizedString(@"Заказ №", nil)
                                                                andValue:[self.model.num stringValue]]];
        [data addObject:[[NITOrderDetailsAboutModel alloc] initWithTitle:NSLocalizedString(@"Адрес", nil)
                                                                andValue:self.model.address]];
        [data addObject:[[NITOrderDetailsAboutModel alloc] initWithTitle:NSLocalizedString(@"Дата", nil)
                                                                andValue:[self.model.date dateStringByLocalFormat:@"dd.MM.yy"]]];
        [data addObject:[[NITOrderDetailsAboutModel alloc] initWithTitle:NSLocalizedString(@"Цена", nil)
                                                                andValue:self.model.price ? [NSString stringWithFormat:@"%@ %@", self.model.price, NSLocalizedString(@"руб.", nil)] : @""]];
        [data addObject:[[NITOrderDetailsAboutModel alloc] initWithTitle:NSLocalizedString(@"Дата доставки", nil)
                                                                andValue:[self.model.deliveryDate dateStringByLocalFormat:@"dd.MM.yy"]]];
        [data addObject:[[NITOrderDetailsAboutModel alloc] initWithTitle:NSLocalizedString(@"Время доставки", nil)
                                                                andValue:interval]];
        [data addObject:[[NITOrderDetailsAboutModel alloc] initWithTitle:NSLocalizedString(@"Кому", nil)
                                                                andValue:self.model.receiverName]];
        
        [weakSelf.view reloadAboutSectionByModels:data];
    }];
}

- (void)updateStatusOrderData
{
    NSArray * data;
    
    if (!self.model.complited)
    {
        NITOrderDetailsStatusModel * model = [NITOrderDetailsStatusModel new];
        model.additionalPrice = self.model.sumRemain;
        model.processStatus = self.model.processStatus;
        model.paymentStatus = self.model.paymentStatus;
        model.paymentTypeCash = self.model.paymentTypeCash;
        
        data = @[model];
    }
    
    [self.view reloadStatusSectionByModels:data];
}

- (void)updateProductOrderData
{
    [self.view reloadProductSectionByModels:self.model.items];
}

#pragma mark - NITOrderDetailsInteractorOutputProtocol
- (void)updateOrderModel:(NITMyOrderModel *)model
{
    self.model = model;
    
    [self updateAboutOrderData];
    [self updateStatusOrderData];
    [self updateProductOrderData];
}

@end
