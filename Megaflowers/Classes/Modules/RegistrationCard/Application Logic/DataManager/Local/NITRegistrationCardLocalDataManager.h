//
//  NITRegistrationCardLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegistrationCardProtocols.h"

@interface NITRegistrationCardLocalDataManager : NSObject <NITRegistrationCardLocalDataManagerInputProtocol>

@end
