//
//  NITRegistrationCardAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardAPIDataManager.h"
#import "SWGUserApi.h"

@implementation NITRegistrationCardAPIDataManager

- (void)registerCard:(NITRegistrationCardModel *)model withHandler:(void(^)(NSError *error))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userDiscountPostWithFirstName:model.name
                                           lastName:model.surname
                                           birthday:(id)model.birthday.serverString
                                              phone:model.phone
                                               lang:HELPER.lang completionHandler:^(NSError *error) {
                                                   [HELPER stopLoading];
                                                   if (error) [HELPER logError:error method:THIS_METHOD];
                                                   if (handler) handler(error);
    }];
}

@end
