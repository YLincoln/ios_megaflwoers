//
//  NITRegistrationCardInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardInteractor.h"
#import "NITLoginAPIDataManager.h"
#import "NITLoginInteractor.h"

@implementation NITRegistrationCardInteractor

- (void)registerCard:(NITRegistrationCardModel *)model
{
    weaken(self);
    [self.APIDataManager registerCard:model withHandler:^(NSError *error) {
        
        if (error)
        {
            [weakSelf.presenter discondCardRegisterWithError:error.interfaceDescription];
        }
        else
        {
            [weakSelf syncUserDataWithHandler:^(BOOL success) {
                [weakSelf.presenter discondCardSuccesRegistered];
            }];
        }
    }];
}

#pragma mark - Private
- (void)syncUserDataWithHandler:(void(^)(BOOL success))handler
{
    NITLoginInteractor * loginInteractor = [NITLoginInteractor new];
    loginInteractor.APIDataManager = [NITLoginAPIDataManager new];
    [loginInteractor syncUserDataWithHandler:^(BOOL success, NSError *error) {
        handler(success);
    }];
}

@end
