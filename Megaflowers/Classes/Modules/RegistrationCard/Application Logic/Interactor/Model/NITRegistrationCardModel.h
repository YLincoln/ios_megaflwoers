//
//  NITRegistrationCardModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITRegistrationCardModel : NSObject

@property (nonatomic) NSString * name;
@property (nonatomic) NSString * surname;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSDate * birthday;
@property (nonatomic) BOOL acceptSMS;

@property (nonatomic) BOOL nameHighlight;
@property (nonatomic) BOOL surnameHighlight;
@property (nonatomic) BOOL birthdayHighlight;
@property (nonatomic) BOOL phoneHighlight;

- (BOOL)isCorrect;

@end
