//
//  NITRegistrationCardModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardModel.h"

@implementation NITRegistrationCardModel

#pragma mark - Public
- (BOOL)isCorrect
{
    BOOL correct = true;
    
    self.nameHighlight      = false;
    self.phoneHighlight     = false;
    self.surnameHighlight   = false;
    self.birthdayHighlight  = false;
    
    if ([self.name length] == 0)
    {
        self.nameHighlight = true;
        correct = false;
    }
    
    if ([self.surname length] == 0)
    {
        self.surnameHighlight = true;
        correct = false;
    }
    
    if ([self.phone length] == 0)
    {
        self.phoneHighlight = true;
        correct = false;
    }
    
    if (self.birthday == nil)
    {
        self.birthdayHighlight = true;
        correct = false;
    }
    
    return correct;
}

@end
