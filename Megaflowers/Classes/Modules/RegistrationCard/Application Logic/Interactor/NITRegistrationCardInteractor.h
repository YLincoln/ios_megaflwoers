//
//  NITRegistrationCardInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegistrationCardProtocols.h"

@interface NITRegistrationCardInteractor : NSObject <NITRegistrationCardInteractorInputProtocol>

@property (nonatomic, weak) id <NITRegistrationCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRegistrationCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRegistrationCardLocalDataManagerInputProtocol> localDataManager;

@end
