//
//  NITRegistrationCardProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardModel.h"

@protocol NITRegistrationCardInteractorOutputProtocol;
@protocol NITRegistrationCardInteractorInputProtocol;
@protocol NITRegistrationCardViewProtocol;
@protocol NITRegistrationCardPresenterProtocol;
@protocol NITRegistrationCardLocalDataManagerInputProtocol;
@protocol NITRegistrationCardAPIDataManagerInputProtocol;

@class NITRegistrationCardWireFrame;

@protocol NITRegistrationCardDelegate <NSObject>

- (void)didRegisterDiscontCard;

@end

@protocol NITRegistrationCardViewProtocol
@required
@property (nonatomic, strong) id <NITRegistrationCardPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITRegistrationCardModel *)model;
@end

@protocol NITRegistrationCardWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITRegistrationCardModuleFrom:(id)fromView withDelegate:(id)delegate;
- (void)backFrom:(id)fromView;
@end

@protocol NITRegistrationCardPresenterProtocol
@required
@property (nonatomic, weak) id <NITRegistrationCardViewProtocol> view;
@property (nonatomic, strong) id <NITRegistrationCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRegistrationCardWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITRegistrationCardDelegate> delegate;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (NSString *)formattedDate;
- (void)registerCard;
@end

@protocol NITRegistrationCardInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)discondCardSuccesRegistered;
- (void)discondCardRegisterWithError:(NSString *)error;
@end

@protocol NITRegistrationCardInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITRegistrationCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRegistrationCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRegistrationCardLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)registerCard:(NITRegistrationCardModel *)model;
@end


@protocol NITRegistrationCardDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITRegistrationCardAPIDataManagerInputProtocol <NITRegistrationCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)registerCard:(NITRegistrationCardModel *)model withHandler:(void(^)(NSError *error))handler;
@end

@protocol NITRegistrationCardLocalDataManagerInputProtocol <NITRegistrationCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
