//
//  NITRegistrationCardViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITRegistrationCardProtocols.h"

@interface NITRegistrationCardViewController : NITBaseViewController <NITRegistrationCardViewProtocol>

@property (nonatomic, strong) id <NITRegistrationCardPresenterProtocol> presenter;

@end
