//
//  NITRegistrationCardViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardViewController.h"
#import "AKNumericFormatter.h"

@interface NITRegistrationCardViewController ()

@property (nonatomic) IBOutlet UITextField * nameField;
@property (nonatomic) IBOutlet UITextField * surnameField;
@property (nonatomic) IBOutlet UITextField * birthdayField;
@property (nonatomic) IBOutlet PhoneTextField * phoneField;
@property (nonatomic) IBOutlet UIDatePicker * datePicker;
@property (nonatomic) IBOutlet UIImageView * dateIcon;
@property (nonatomic) IBOutlet UIButton * acceptBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * datePickerH;

@property (nonatomic) NITRegistrationCardModel * model;

@end

@implementation NITRegistrationCardViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)registerAction:(id)sender
{
    [self.presenter registerCard];
}

- (IBAction)acceptAction:(id)sender
{
    self.model.acceptSMS = !self.model.acceptSMS;
    [self updateAgreement];
}

- (IBAction)selectDateAction:(id)sender
{
    self.datePicker.hidden = !self.datePicker.hidden;
    self.datePickerH.constant = self.datePicker.hidden ? 0 : 216;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self dateChangeAction:nil];
    [self updateDateIcon];
}

- (IBAction)dateChangeAction:(id)sender
{
    self.model.birthdayHighlight = false;
    self.birthdayField.textColor = RGB(10, 88, 43);
    self.model.birthday = self.datePicker.date;
    self.birthdayField.text = [self.presenter formattedDate];
}


#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Регистрация карты", nil);
    
    [self setKeyboardActiv:true];
    
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.surnameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.surnameField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.birthdayField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.birthdayField.placeholder
                                                                                 attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

- (void)updateAgreement
{
    [self.acceptBtn setImage:[UIImage imageNamed:self.model.acceptSMS ? @"check" : @"check_ar"] forState:UIControlStateNormal];
}

- (void)updateDateIcon
{
    self.dateIcon.image = [UIImage imageNamed:self.datePicker.hidden ? @"ic_down_cn" :  @"ic_up_cn"];
}

- (void)updateModelByField:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 101: // name
            textField.text = [textField.text capitalizedString];
            self.model.name = textField.text;
            break;
            
        case 102: // surname
            self.model.surname = textField.text;
            break;
            
        case 104: // phone
            self.model.phone = textField.text;
            break;
            
        default:
            break;
    }
}

#pragma mark - NITRegistrationCardPresenterProtocol
- (void)reloadDataByModel:(NITRegistrationCardModel *)model
{
    self.model = model;
    
    self.nameField.textColor = self.model.nameHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.nameField.text = model.name;
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:
                                                                                            self.model.nameHighlight ?  RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.surnameField.textColor = self.model.surnameHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.surnameField.text = self.model.surname;
    self.surnameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.surnameField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:
                                                                                             self.model.surnameHighlight ?  RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.phoneField.textColor = self.model.phoneHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.phoneField.text = self.model.phone;
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:
                                                                                             self.model.phoneHighlight ?  RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.birthdayField.textColor = self.model.birthdayHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.birthdayField.text = [self.presenter formattedDate];
    self.birthdayField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.birthdayField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:
                                                                                             self.model.birthdayHighlight ?  RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    textField.textColor = RGB(10, 88, 43);
    [self updateModelByField:textField];
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateModelByField:textField];
}

@end
