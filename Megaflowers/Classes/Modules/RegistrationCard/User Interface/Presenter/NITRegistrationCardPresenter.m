//
//  NITRegistrationCardPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardPresenter.h"
#import "NITRegistrationCardWireframe.h"

@interface NITRegistrationCardPresenter ()

@property (nonatomic) NITRegistrationCardModel * model;

@end

@implementation NITRegistrationCardPresenter

- (void)initData
{
    self.model = [NITRegistrationCardModel new];
    [self.view reloadDataByModel:self.model];
}

- (NSString *)formattedDate
{
    if (self.model.birthday)
    {
        return [self.model.birthday dateStringByLocalFormat:@"d LLLL yyyy"];
    }
    else
    {
        return @"";
    }
}

- (void)registerCard
{
    if ([self.model isCorrect])
    {
        [self.interactor registerCard:self.model];
    }
    else
    {
        [self.view reloadDataByModel:self.model];
    }
}

#pragma mark - NITRegistrationCardInteractorOutputProtocol
- (void)discondCardSuccesRegistered
{
    if ([self.delegate respondsToSelector:@selector(didRegisterDiscontCard)])
    {
        [self.delegate didRegisterDiscontCard];
    }
    
    [TRACKER trackEvent:TrackerEventDiscontCardCreate];
    
    [self.wireFrame backFrom:self.view];
}

- (void)discondCardRegisterWithError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
    
    [HELPER showActionSheetFromView:self.view
                          withTitle:error
                      actionButtons:nil
                       cancelButton:cancel];
}

@end
