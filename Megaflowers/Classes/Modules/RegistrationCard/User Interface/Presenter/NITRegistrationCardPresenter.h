//
//  NITRegistrationCardPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegistrationCardProtocols.h"

@class NITRegistrationCardWireFrame;

@interface NITRegistrationCardPresenter : NITRootPresenter <NITRegistrationCardPresenterProtocol, NITRegistrationCardInteractorOutputProtocol>

@property (nonatomic, weak) id <NITRegistrationCardViewProtocol> view;
@property (nonatomic, strong) id <NITRegistrationCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRegistrationCardWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITRegistrationCardDelegate> delegate;

@end
