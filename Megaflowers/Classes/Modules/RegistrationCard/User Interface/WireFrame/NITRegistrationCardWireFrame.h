//
//  NITRegistrationCardWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardProtocols.h"
#import "NITRegistrationCardViewController.h"
#import "NITRegistrationCardLocalDataManager.h"
#import "NITRegistrationCardAPIDataManager.h"
#import "NITRegistrationCardInteractor.h"
#import "NITRegistrationCardPresenter.h"
#import "NITRegistrationCardWireframe.h"
#import "NITRootWireframe.h"

@interface NITRegistrationCardWireFrame : NITRootWireframe <NITRegistrationCardWireFrameProtocol>

@end
