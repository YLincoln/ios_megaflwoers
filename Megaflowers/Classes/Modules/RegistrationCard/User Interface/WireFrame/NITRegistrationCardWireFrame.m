//
//  NITRegistrationCardWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/14/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegistrationCardWireFrame.h"

@implementation NITRegistrationCardWireFrame

+ (id)createNITRegistrationCardModule
{
    // Generating module components
    id <NITRegistrationCardPresenterProtocol, NITRegistrationCardInteractorOutputProtocol> presenter = [NITRegistrationCardPresenter new];
    id <NITRegistrationCardInteractorInputProtocol> interactor = [NITRegistrationCardInteractor new];
    id <NITRegistrationCardAPIDataManagerInputProtocol> APIDataManager = [NITRegistrationCardAPIDataManager new];
    id <NITRegistrationCardLocalDataManagerInputProtocol> localDataManager = [NITRegistrationCardLocalDataManager new];
    id <NITRegistrationCardWireFrameProtocol> wireFrame= [NITRegistrationCardWireFrame new];
    id <NITRegistrationCardViewProtocol> view = [(NITRegistrationCardWireFrame *)wireFrame createViewControllerWithKey:_s(NITRegistrationCardViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITRegistrationCardModuleFrom:(UIViewController *)fromViewController withDelegate:(id)delegate
{
    NITRegistrationCardViewController * vc = [NITRegistrationCardWireFrame createNITRegistrationCardModule];
    vc.presenter.delegate = delegate;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)backFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
