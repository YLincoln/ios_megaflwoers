//
//  NITGuideProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITGuidePageModel.h"

@protocol NITGuideInteractorOutputProtocol;
@protocol NITGuideInteractorInputProtocol;
@protocol NITGuideViewProtocol;
@protocol NITGuidePresenterProtocol;
@protocol NITGuideLocalDataManagerInputProtocol;
@protocol NITGuideAPIDataManagerInputProtocol;

@class NITGuideWireFrame;

typedef CF_ENUM (NSUInteger, GuideState) {
    GuideStateFirstLanch    = 0,
    GuideStateView          = 1
};

@protocol NITGuideViewProtocol
@required
@property (nonatomic, strong) id <NITGuidePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)compliteLaunchScreenAnimation;
- (void)reloadDataByModels:(NSArray <NITGuidePageModel *> *)models;
@end

@protocol NITGuideWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
- (void)presentGuideInterfaceFromWindow:(UIWindow *)window;
+ (void)presentNITGuideModuleFrom:(id)fromView widthState:(GuideState)state;
- (void)showTabbar;
- (void)loginActionWithHandler:(void(^)())handler;
- (void)registerActionWithHandler:(void(^)())handler;
- (void)hideView:(id)view withHandler:(void(^)())handler;
@end

@protocol NITGuidePresenterProtocol
@required
@property (nonatomic, weak) id <NITGuideViewProtocol> view;
@property (nonatomic, strong) id <NITGuideInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITGuideWireFrameProtocol> wireFrame;
@property (nonatomic) GuideState state;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)animateLaunchScreen;
- (void)checkLanchState;
- (void)skipGuide;
- (void)initData;
- (void)loginAction;
- (void)registerAction;
- (BOOL)pageAvialableFromIndex:(NSInteger)index;

@end

@protocol NITGuideInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITGuideInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITGuideInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITGuideAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITGuideLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITGuideDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITGuideAPIDataManagerInputProtocol <NITGuideDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITGuideLocalDataManagerInputProtocol <NITGuideDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
