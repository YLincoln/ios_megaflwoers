//
//  NITGuideWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGuideProtocols.h"
#import "NITGuideViewController.h"
#import "NITGuideLocalDataManager.h"
#import "NITGuideAPIDataManager.h"
#import "NITGuideInteractor.h"
#import "NITGuidePresenter.h"
#import "NITGuideWireframe.h"
#import "NITRootWireframe.h"

@interface NITGuideWireFrame : NITRootWireframe <NITGuideWireFrameProtocol>

@end
