//
//  NITGuideWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideWireFrame.h"
#import "NITTabBarWireFrame.h"
#import "NITRegisterWireFrame.h"
#import "NITLoginWireFrame.h"

@implementation NITGuideWireFrame

+ (id)createNITGuideModule
{
    // Generating module components
    id <NITGuidePresenterProtocol, NITGuideInteractorOutputProtocol> presenter = [NITGuidePresenter new];
    id <NITGuideInteractorInputProtocol> interactor = [NITGuideInteractor new];
    id <NITGuideAPIDataManagerInputProtocol> APIDataManager = [NITGuideAPIDataManager new];
    id <NITGuideLocalDataManagerInputProtocol> localDataManager = [NITGuideLocalDataManager new];
    id <NITGuideWireFrameProtocol> wireFrame= [NITGuideWireFrame new];
    id <NITGuideViewProtocol> view = [(NITGuideWireFrame *)wireFrame createViewControllerWithKey:_s(NITGuideViewController) storyboardType:StoryboardTypeGuide];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITGuideModuleFrom:(UIViewController *)fromViewController widthState:(GuideState)state
{
    NITGuideViewController * view = [NITGuideWireFrame createNITGuideModule];
    view.presenter.state = state;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:view];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

- (void)presentGuideInterfaceFromWindow:(UIWindow *)window
{
    NITGuideViewController * view = [NITGuideWireFrame createNITGuideModule];
    view.presenter.state = GuideStateFirstLanch;
    [self showRootViewController:view inWindow:window];
}

- (void)showTabbar
{
    [NITTabBarWireFrame presentNITTabBarModuleFromWindow:WINDOW];
}

- (void)loginActionWithHandler:(void (^)())handler
{
    if (![USER isAutorize])
    {
        [NITLoginWireFrame presentModalNITLoginModule:WINDOW.visibleController withHandler:handler];
    }
}

- (void)registerActionWithHandler:(void (^)())handler
{
    if (![USER isAutorize])
    {
       [NITRegisterWireFrame presentModalNITRegisterModule:WINDOW.visibleController withHandler:handler];
    }
}

- (void)hideView:(UIViewController *)viewController withHandler:(void(^)())handler
{
    [viewController dismissViewControllerAnimated:true completion:^{
        if (handler) handler();
    }];
}

@end
