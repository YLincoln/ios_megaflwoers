//
//  NITGuideViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideViewController.h"
#import "NITGuideCollectionViewCell.h"

@interface NITGuideViewController ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
NITGuideCollectionViewCellDelegate
>

@property (nonatomic) IBOutlet UICollectionView * collectionView;
@property (nonatomic) IBOutlet UIButton * skipBtn;
@property (nonatomic) IBOutlet UIPageControl * pageControl;
@property (nonatomic) IBOutlet UIButton * backBtn;
@property (nonatomic) IBOutlet UIButton * nextBtn;

@property (nonatomic) NSArray <NITGuidePageModel *> * items;

@end

@implementation NITGuideViewController
    
#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter animateLaunchScreen];
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

#pragma mark - IBAction
- (IBAction)skip:(id)sender
{
    [self.presenter skipGuide];
}

- (IBAction)backAction:(id)sender
{
    if (self.pageControl.currentPage > 0)
    {
        self.pageControl.currentPage -= 1;
        [self scrollPageToIndex:self.pageControl.currentPage animated:true];
    }
}

- (IBAction)nextAction:(id)sender
{
    if (self.pageControl.currentPage < self.pageControl.numberOfPages - 1 &&
        [self.presenter pageAvialableFromIndex:self.pageControl.currentPage])
    {
        self.pageControl.currentPage += 1;
        [self scrollPageToIndex:self.pageControl.currentPage animated:true];
    }
}

- (IBAction)pageChanged:(id)sender
{
    [self scrollPageToIndex:self.pageControl.currentPage animated:true];
}
    
#pragma mark - Private
- (void)initUI
{
    [self.navigationController setNavigationBarHidden:true];
    [self.collectionView registerNib:[UINib nibWithNibName:_s(NITGuideCollectionViewCell) bundle:nil] forCellWithReuseIdentifier:_s(NITGuideCollectionViewCell)];
}

- (void)scrollPageToIndex:(NSInteger)index animated:(BOOL)animated
{
    if (index < self.items.count)
    {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionNone
                                            animated:animated];
    }
}

- (void)presentPage
{
    NSInteger index = self.pageControl.currentPage;
    
    switch (index) {
        case 0:
            self.backBtn.hidden = true;
            self.nextBtn.hidden = false;
            break;
        case 1:
        case 2:
        case 3:
            self.backBtn.hidden = false;
            self.nextBtn.hidden = false;
            break;
        case 4:
            self.backBtn.hidden = true;
            self.nextBtn.hidden = true;
            break;
    }
    
    for (NSIndexPath * indexPath in self.collectionView.indexPathsForVisibleItems)
    {
        NITGuideCollectionViewCell * cell = (NITGuideCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        if (indexPath.row == index) [cell play];
        else                        [cell stop];
    }
}

#pragma mark - NITGuidePresenterProtocol
- (void)compliteLaunchScreenAnimation
{
    [self.presenter checkLanchState];
}

- (void)reloadDataByModels:(NSArray <NITGuidePageModel *> *)models
{
    self.items = models;
    
    [self.pageControl setNumberOfPages:models.count];
    [self.collectionView reloadData];
    [self.collectionView performBatchUpdates:^{}
                                  completion:^(BOOL finished) {
                                      [self presentPage];
                                  }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITGuideCollectionViewCell) forIndexPath:indexPath];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(ViewWidth(self.collectionView), ViewHeight(self.collectionView));
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(NITGuideCollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    self.pageControl.currentPage = indexPath.row;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [self.presenter pageAvialableFromIndex:self.pageControl.currentPage];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.pageControl.currentPage = self.collectionView.contentOffset.x / ViewWidth(self.collectionView);
    [self presentPage];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self presentPage];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
}

#pragma mark - NITGuideCollectionViewCellDelegate
- (void)didLoginAction
{
    [self.presenter loginAction];
}

- (void)didRegisterAction
{
    [self.presenter registerAction];
}

@end
