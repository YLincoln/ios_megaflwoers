//
//  NITVideoView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface NITVideoView : UIView

@property (nonatomic) AVPlayer * player;

//- (void)setPlayer:(AVPlayer *)player;
- (void)setVideoFillMode:(NSString *)fillMode;

@end
