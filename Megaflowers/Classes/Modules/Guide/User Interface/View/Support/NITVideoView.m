//
//  NITVideoView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITVideoView.h"

@implementation NITVideoView

- (instancetype)init
{
    if (self = [super init])
    {
        self.userInteractionEnabled = false;
        
    }
    
    return self;
}

#pragma mark - Custom accessors
- (AVPlayer *)player
{
    return [(AVPlayerLayer *)[self layer] player];
}

+ (Class)layerClass
{
    return [AVPlayerLayer class];
}

#pragma mark - Public
- (void)setPlayer:(AVPlayer *)player
{
    [(AVPlayerLayer *)[self layer] setPlayer:player];
    [self setVideoFillMode:AVLayerVideoGravityResizeAspectFill];
}

/* Specifies how the video is displayed within a player layer’s bounds.
   (AVLayerVideoGravityResizeAspect is default) */
- (void)setVideoFillMode:(NSString *)fillMode
{
    AVPlayerLayer * playerLayer = (AVPlayerLayer *)[self layer];
    playerLayer.videoGravity = fillMode;
}

@end
