//
//  NITGuideFiltersView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideFiltersView.h"
#import "OverlayView.h"

@interface NITGuideFiltersView ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * closeBtn;
@property (nonatomic) OverlayView * overlay;

@end

@implementation NITGuideFiltersView

+ (instancetype)showWidthOverlayView:(UIView *)overlayView withHandler:(void(^)())handler
{
    NITGuideFiltersView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITGuideFiltersView) owner:self options:nil] firstObject];
    view.backgroundColor = [UIColor clearColor];
    view.frame = WINDOW.frame;
    
    [[view.closeBtn layer] setBorderWidth:1.6f];
    [[view.closeBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    view.overlay = [OverlayView new];
    view.overlay.hideWhenTapped = false;
    view.overlay.animated = true;
    view.overlay.didHideCallback = ^{
        if (handler) handler();
    };
    
    [view.overlay addHoleWithView:overlayView padding:0.0f offset:CGSizeZero form:OverlayHoleFormCircle transparentEvent:false];
    [view.overlay show];
    [view.overlay addSubview:view];
    
    [UserDefaults setBool:true forKey:_s(NITGuideFiltersView)];
    
    return view;
}

+ (BOOL)notYetShown
{
    return ![UserDefaults boolForKey:_s(NITGuideFiltersView)];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.overlay hide];
}


@end
