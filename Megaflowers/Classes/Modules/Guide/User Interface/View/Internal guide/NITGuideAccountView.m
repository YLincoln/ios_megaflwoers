//
//  NITGuideAccountView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideAccountView.h"
#import "OverlayView.h"

@interface NITGuideAccountView ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * closeBtn;
@property (nonatomic) OverlayView * overlay;

@end

@implementation NITGuideAccountView

+ (instancetype)showWidthOverlayRect:(CGRect)rect
{
    NITGuideAccountView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITGuideAccountView) owner:self options:nil] firstObject];
    view.backgroundColor = [UIColor clearColor];
    view.frame = WINDOW.frame;
    
    [[view.closeBtn layer] setBorderWidth:1.6f];
    [[view.closeBtn layer] setBorderColor:[UIColor whiteColor].CGColor];

    view.overlay = [OverlayView new];
    view.overlay.hideWhenTapped = false;
    view.overlay.animated = true;
    
    [view.overlay addHoleWithRect:rect form:OverlayHoleFormCircle transparentEvent:false];
    [view.overlay show];
    [view.overlay addSubview:view];
    
    [UserDefaults setBool:true forKey:_s(NITGuideAccountView)];
    
    return view;
}

+ (BOOL)notYetShown
{
    return ![UserDefaults boolForKey:_s(NITGuideAccountView)];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.overlay hide];
}


@end
