//
//  NITGuideBouquetSwipeView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITGuideBouquetSwipeView : UIView

+ (instancetype)show;
+ (BOOL)notYetShown;

@end
