//
//  NITGuideBouquetEditedView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/27/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideBouquetEditedView.h"
#import "OverlayView.h"

@interface NITGuideBouquetEditedView ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * closeBtn;
@property (nonatomic) OverlayView * overlay;

@end

@implementation NITGuideBouquetEditedView

+ (instancetype)showWidthOverlayView:(UIView *)overlayView withYOffset:(CGFloat)offset
{
    NITGuideBouquetEditedView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITGuideBouquetEditedView) owner:self options:nil] firstObject];
    view.backgroundColor = [UIColor clearColor];
    view.frame = WINDOW.frame;
    
    if (IS_IPHONE_4)
    {
        view.center = CGPointMake(view.center.x, view.center.y - 50);
    }
    
    view.center = CGPointMake(view.center.x, view.center.y + offset);
    
    [[view.closeBtn layer] setBorderWidth:1.6f];
    [[view.closeBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    view.overlay = [OverlayView new];
    view.overlay.hideWhenTapped = false;
    view.overlay.animated = true;
    
    [view.overlay addHoleWithView:overlayView padding:15.0f offset:CGSizeZero form:OverlayHoleFormCircle transparentEvent:false];
    [view.overlay show];
    [view.overlay addSubview:view];
    
    [UserDefaults setBool:true forKey:_s(NITGuideBouquetEditedView)];
    
    return view;
}

+ (BOOL)notYetShown
{
    return ![UserDefaults boolForKey:_s(NITGuideBouquetEditedView)];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.overlay hide];
}


@end
