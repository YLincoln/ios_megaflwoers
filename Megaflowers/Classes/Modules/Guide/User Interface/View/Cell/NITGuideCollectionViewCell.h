//
//  NITGuideCollectionViewCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/5/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class NITGuidePageModel;

@protocol NITGuideCollectionViewCellDelegate <NSObject>

- (void)didLoginAction;
- (void)didRegisterAction;

@end

@interface NITGuideCollectionViewCell : UICollectionViewCell

@property (nonatomic) NITGuidePageModel * model;
@property (nonatomic) id <NITGuideCollectionViewCellDelegate> delegate;

- (void)play;
- (void)stop;

@end
