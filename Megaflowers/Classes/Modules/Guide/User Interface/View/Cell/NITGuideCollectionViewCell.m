//
//  NITGuideCollectionViewCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/5/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideCollectionViewCell.h"
#import "NITGuidePageModel.h"
#import "NITVideoView.h"

@interface NITGuideCollectionViewCell ()

@property (nonatomic) IBOutlet NITVideoView * playerView;
@property (nonatomic) IBOutlet UIImageView * bg;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutletCollection(UIView) NSArray * hidenElements;
@property (nonatomic) AVPlayer * player;

@end

@implementation NITGuideCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.playerView.player = self.player;
}

#pragma mark - Custom accessors
- (void)setModel:(NITGuidePageModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)loginAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didLoginAction)])
    {
        [self.delegate didLoginAction];
    }
}

- (IBAction)registerAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didRegisterAction)])
    {
        [self.delegate didRegisterAction];
    }
}

#pragma mark - Public
- (void)play
{
    [self.model loadPlayerItemWithHandler:^(AVPlayerItem *playerItem) {
        if (playerItem)
        {
            self.player = [AVPlayer playerWithPlayerItem:playerItem];
            self.playerView.player = self.player;
            self.playerView.hidden = false;
            [self.player play];
        }
    }];
}

- (void)stop
{
    self.playerView.hidden = true;
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.bg.image = [UIImage imageNamed:self.model.imageName];

    for (UIView * view in self.hidenElements)
    {
        view.hidden = self.model.index < 4 || [USER isAutorize];
    }
    
}

@end
