//
//  NITGuideViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITGuideProtocols.h"
#import "NITBaseViewController.h"

@interface NITGuideViewController : NITBaseViewController <NITGuideViewProtocol>

@property (nonatomic, strong) id <NITGuidePresenterProtocol> presenter;

@end
