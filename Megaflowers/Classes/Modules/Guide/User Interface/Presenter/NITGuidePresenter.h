//
//  NITGuidePresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGuideProtocols.h"

@class NITGuideWireFrame;

@interface NITGuidePresenter : NITRootPresenter <NITGuidePresenterProtocol, NITGuideInteractorOutputProtocol>

@property (nonatomic, weak) id <NITGuideViewProtocol> view;
@property (nonatomic, strong) id <NITGuideInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITGuideWireFrameProtocol> wireFrame;
@property (nonatomic) GuideState state;

@end
