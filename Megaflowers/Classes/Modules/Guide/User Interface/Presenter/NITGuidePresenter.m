//
//  NITGuidePresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuidePresenter.h"
#import "NITGuideWireframe.h"
#import "YLGIFImage.h"
#import "YLImageView.h"
#import "AppDependencies.h"

@implementation NITGuidePresenter
    
- (void)animateLaunchScreen
{
    switch (self.state) {
            
        case GuideStateFirstLanch: {
            
            UIWindow * window = [UIApplication sharedApplication].keyWindow;
            YLImageView * splash = [[YLImageView alloc] initWithFrame:window.frame];
            splash.image = [YLGIFImage imageNamed:@"Splash_animation.gif"];
            splash.contentMode = UIViewContentModeScaleAspectFill;
            [window addSubview:splash];
            
            weaken(self);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [splash removeFromSuperview];
                [weakSelf.view compliteLaunchScreenAnimation];
            });
        }
            break;
            
        case GuideStateView:
            [self.view compliteLaunchScreenAnimation];
            break;
    }
}

- (void)checkLanchState
{
    switch (self.state) {
            
        case GuideStateFirstLanch:
            if (![HELPER isFirstLanch]) [self skipGuide];
            else                        [self initData];
            break;
            
        case GuideStateView:
            [self initData];
            break;
    }
}

- (void)initData
{
    NSMutableArray * models = [NSMutableArray new];
    
    for (int i = 0; i < 5; i++)
    {
        [models addObject:[[NITGuidePageModel alloc] initWithIndex:i]];
    }
    
    [self.view reloadDataByModels:models];
}

- (BOOL)pageAvialableFromIndex:(NSInteger)index
{
    if (index == 3 && ![UserDefaults boolForKey:@"push_registered"])
    {
        [AppDependencies registerPushNotificationsWithOptions:nil];
        [UserDefaults setBool:true forKey:@"push_registered"];
        return false;
    }
    
    return true;
}

- (void)skipGuide
{
    switch (self.state) {
        case GuideStateFirstLanch: {
            /*[self pageAvialableFromIndex:3];*/
            [HELPER setFirstLanch];
            [self.wireFrame showTabbar];
        } break;
            
        case GuideStateView: {
            [self.wireFrame hideView:self.view withHandler:nil];
        } break;
    }
}

- (void)loginAction
{
    switch (self.state) {
        case GuideStateFirstLanch: {
            [HELPER setFirstLanch];
            [self.wireFrame loginActionWithHandler:^{
                [self.wireFrame showTabbar];
            }];
        } break;
            
        case GuideStateView: {
            [self.wireFrame hideView:self.view withHandler:^{
                [self.wireFrame loginActionWithHandler:^{}];
            }];
        } break;
    }
}

- (void)registerAction
{
    switch (self.state) {
        case GuideStateFirstLanch: {
            [HELPER setFirstLanch];
            [self.wireFrame registerActionWithHandler:^{
                [self.wireFrame showTabbar];
            }];
        } break;
            
        case GuideStateView: {
            [self.wireFrame hideView:self.view withHandler:^{
                [self.wireFrame registerActionWithHandler:^{}];
            }];
        } break;
    }
}

@end
