//
//  NITGuidePageModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/5/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface NITGuidePageModel : NSObject

@property (nonatomic) NSString * imageName;
@property (nonatomic) NSString * videoName;
@property (nonatomic) NSString * title;
@property (nonatomic) NSInteger index;

- (instancetype)initWithIndex:(NSInteger)index;
- (void)loadPlayerItemWithHandler:(void(^)(AVPlayerItem * playerItem))handler;

@end
