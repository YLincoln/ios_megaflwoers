//
//  NITGuidePageModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/5/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuidePageModel.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITGuidePageModel ()

@end

@implementation NITGuidePageModel

- (instancetype)initWithIndex:(NSInteger)index
{
    if (self = [super init])
    {
        self.index = index;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (void)setIndex:(NSInteger)index
{
    _index = index;
    [self setup];
}

#pragma mark - Public
- (void)loadPlayerItemWithHandler:(void(^)(AVPlayerItem * playerItem))handler
{
    if (self.videoName.length > 0)
    {
        NSURL * fileURL = [[NSBundle mainBundle] URLForResource:self.videoName withExtension:@"mov"];
        AVURLAsset * asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
        
        [asset loadValuesAsynchronouslyForKeys:@[self.videoName] completionHandler:^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSError * error;
                AVKeyValueStatus status = [asset statusOfValueForKey:self.videoName error:&error];
                
                if (status == AVKeyValueStatusLoaded)
                {
                    AVPlayerItem * item = [AVPlayerItem playerItemWithAsset:asset];
                    handler(item);

                }
                else
                {
                    DDLogError(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
                    handler(nil);
                }
            });
        }];
    }
    else
    {
        handler(nil);
    }
}

#pragma mark - Private
- (void)setup
{
    switch (self.index) {
        case 0:
            self.imageName = @"bg_page_1";
            self.videoName = @"guide_1";
            self.title     = NSLocalizedString(@"Простой способ\nвыбрать подходящий\nбукет", nil);
            break;
        case 1:
            self.imageName = @"bg_page_2";
            self.videoName = @"guide_2";
            self.title     = NSLocalizedString(@"Экспресс-доставка\nв любую точку мира\n24/7", nil);
            break;
        case 2:
            self.imageName = @"bg_page_3";
            self.videoName = @"guide_3";
            self.title     = NSLocalizedString(@"Отслеживайте\nсостояние заказа\nв реальном времени", nil);
            break;
        case 3:
            self.imageName = @"bg_page_5";
            self.videoName = @"guide_5";
            self.title     = NSLocalizedString(@"Держите\nвсе памятные даты\nв удобном календаре", nil);
            break;
        case 4:
            self.imageName = @"bg_page_4";
            self.videoName = @"guide_4";
            self.title     = NSLocalizedString(@"Персональные предложения\nи дисконтная карта\nвсегда с вами", nil);
            break;
    }
}

@end
