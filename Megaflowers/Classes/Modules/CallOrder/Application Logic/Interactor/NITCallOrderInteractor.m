//
//  NITCallOrderInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCallOrderInteractor.h"

@implementation NITCallOrderInteractor

- (void)callbackWithName:(NSString *)name phone:(NSString *)phone time:(NSString *)time
{
    weaken(self);
    [self.APIDataManager callbackWithName:name phone:phone time:time withHandler:^(NSError *error) {
        if (error)
        {
            [weakSelf.presenter callBackSendError:error.interfaceDescription];
        }
        else
        {
            [weakSelf.presenter callBackSendSuccess];
        }
    }];
}


@end
