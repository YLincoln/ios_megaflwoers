//
//  NITCallOrderInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCallOrderProtocols.h"

@interface NITCallOrderInteractor : NSObject <NITCallOrderInteractorInputProtocol>

@property (nonatomic, weak) id <NITCallOrderInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCallOrderAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCallOrderLocalDataManagerInputProtocol> localDataManager;

@end
