//
//  NITCallOrderAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCallOrderProtocols.h"

@interface NITCallOrderAPIDataManager : NSObject <NITCallOrderAPIDataManagerInputProtocol>

@end
