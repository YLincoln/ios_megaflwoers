//
//  NITCallOrderAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCallOrderAPIDataManager.h"
#import "SWGSalonApi.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITCallOrderAPIDataManager

- (void)callbackWithName:(NSString *)name phone:(NSString *)phone time:(NSString *)time withHandler:(void(^)(NSError *error))handler
{
    [HELPER startLoading];
    [[[SWGSalonApi alloc] initWithApiClient:[SWGApiClient sharedClient]] callbackPostWithName:name phone:phone time:time completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(error);
    }];
}


@end
