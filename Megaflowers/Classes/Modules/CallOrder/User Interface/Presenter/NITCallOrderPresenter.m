//
//  NITCallOrderPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCallOrderPresenter.h"
#import "NITCallOrderWireframe.h"

@implementation NITCallOrderPresenter

#pragma mark - Public
- (BOOL)remindAvialable
{
    if (self.time && [self.time timeIntervalSinceNow] >= (60*60))
    {
        return true;
    }
    
    self.needRemind = false;
    
    return false;
}

- (BOOL)orderAvialable
{
    if (self.time && [self.name length] > 0 && [self.phone length] > 0)
    {
        return true;
    }
    
    return false;
}

- (NSString *)formattedTime
{
    return [self.time dateStringByLocalFormat:@"EE, d LLLL HH:mm"];
}

- (void)orderAction
{
    if ([self orderAvialable])
    {
        [self.interactor callbackWithName:self.name phone:self.phone time:[self.time iso8601String]];
    }
    else
    {
        [self.view highlightEmptyFields];
    }
}

#pragma mark - Private
- (void)createLocalNotificationIfNeed
{
    if (self.needRemind)
    {
        UILocalNotification * notification = [[UILocalNotification alloc] init];
        notification.fireDate = [NSDate dateWithTimeInterval:60*60 sinceDate:self.time];
        notification.alertBody = NSLocalizedString(@"Заказ звонка через час!", nil);
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.soundName = UILocalNotificationDefaultSoundName;
        notification.applicationIconBadgeNumber = 1;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

#pragma mark - NITCallOrderInteractorOutputProtocol
- (void)callBackSendSuccess
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{
        [weakSelf.wireFrame backActionFrom:weakSelf.view];
    }];
    
    [HELPER showActionSheetFromView:self.view
                          withTitle:NSLocalizedString(@"Ваш звонок принят.", nil)
                      actionButtons:@[]
                       cancelButton:cancel];
}

- (void)callBackSendError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
    
    [HELPER showActionSheetFromView:self.view
                          withTitle:error
                      actionButtons:@[]
                       cancelButton:cancel];
}

@end
