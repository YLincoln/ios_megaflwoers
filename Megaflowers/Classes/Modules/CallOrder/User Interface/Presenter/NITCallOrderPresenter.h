//
//  NITCallOrderPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCallOrderProtocols.h"

@class NITCallOrderWireFrame;

@interface NITCallOrderPresenter : NITRootPresenter <NITCallOrderPresenterProtocol, NITCallOrderInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCallOrderViewProtocol> view;
@property (nonatomic, strong) id <NITCallOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCallOrderWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSDate * time;
@property (nonatomic) BOOL needRemind;

@end
