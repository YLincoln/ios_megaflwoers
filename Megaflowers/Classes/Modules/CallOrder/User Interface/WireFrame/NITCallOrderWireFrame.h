//
//  NITCallOrderWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCallOrderProtocols.h"
#import "NITCallOrderViewController.h"
#import "NITCallOrderLocalDataManager.h"
#import "NITCallOrderAPIDataManager.h"
#import "NITCallOrderInteractor.h"
#import "NITCallOrderPresenter.h"
#import "NITCallOrderWireframe.h"
#import "NITRootWireframe.h"

@interface NITCallOrderWireFrame : NITRootWireframe <NITCallOrderWireFrameProtocol>

@end
