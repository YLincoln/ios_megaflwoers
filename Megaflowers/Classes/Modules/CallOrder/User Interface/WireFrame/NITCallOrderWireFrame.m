//
//  NITCallOrderWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCallOrderWireFrame.h"

@implementation NITCallOrderWireFrame

+ (id)createNITCallOrderModule
{
    // Generating module components
    id <NITCallOrderPresenterProtocol, NITCallOrderInteractorOutputProtocol> presenter = [NITCallOrderPresenter new];
    id <NITCallOrderInteractorInputProtocol> interactor = [NITCallOrderInteractor new];
    id <NITCallOrderAPIDataManagerInputProtocol> APIDataManager = [NITCallOrderAPIDataManager new];
    id <NITCallOrderLocalDataManagerInputProtocol> localDataManager = [NITCallOrderLocalDataManager new];
    id <NITCallOrderWireFrameProtocol> wireFrame= [NITCallOrderWireFrame new];
    id <NITCallOrderViewProtocol> view = [(NITCallOrderWireFrame *)wireFrame createViewControllerWithKey:_s(NITCallOrderViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
        
    return view;
}

+ (void)presentNITCallOrderModuleFrom:(UIViewController *)fromViewController
{
    NITCallOrderViewController * view = [NITCallOrderWireFrame createNITCallOrderModule];
    [fromViewController.navigationController pushViewController:view animated:true];
    [TRACKER trackEvent:TrackerEventRequestCall];
}

- (void)backActionFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
