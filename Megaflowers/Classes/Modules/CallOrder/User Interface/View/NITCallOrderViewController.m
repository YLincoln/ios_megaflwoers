//
//  NITCallOrderViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCallOrderViewController.h"
#import "AKNumericFormatter.h"

@interface NITCallOrderViewController ()

@property (nonatomic) IBOutlet UITextField * nameField;
@property (nonatomic) IBOutlet PhoneTextField * phoneField;
@property (nonatomic) IBOutlet UITextField * timeField;
@property (nonatomic) IBOutlet UIButton * orderBtn;
@property (nonatomic) IBOutlet UIButton * remindBtn;
@property (nonatomic) IBOutlet UIButton * showTimeBtn;
@property (nonatomic) IBOutlet UIImageView * timeImg;
@property (nonatomic) IBOutlet UIDatePicker * datePicker;

@end

@implementation NITCallOrderViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = true;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.tabBarController.tabBar.hidden = false;
}

#pragma mark - IBAction
- (IBAction)remindAction:(id)sender
{
    self.presenter.needRemind = !self.presenter.needRemind;
    [self updateUI];
}

- (IBAction)callOrderAction:(id)sender
{
    [self.presenter orderAction];
}

- (IBAction)showTimeAction:(id)sender
{
    self.datePicker.hidden = !self.datePicker.hidden;
    self.datePicker.minimumDate = [NSDate date];
    self.datePicker.maximumDate = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    [self changeTime:nil];
    [self updateTimeIcon];
}

- (IBAction)changeTime:(id)sender
{
    self.presenter.time = self.datePicker.date;
    self.timeField.text = [self.presenter formattedTime];
    [self updateUI];
}

#pragma mark - Private
- (void)initUI
{
    [self setKeyboardActiv:true];
    
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Как к вам обращаться?", nil)
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Номер телефона", nil)
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.timeField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Выберите удобное время", nil)
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    [self.datePicker setValue:RGB(10, 88, 43) forKeyPath:@"textColor"];
    
    self.presenter.time = [NSDate date];
    self.timeField.text = [self.presenter formattedTime];
    
    if([USER isAutorize])
    {
        self.nameField.text = USER.name;
        self.phoneField.text = USER.phone;
        self.presenter.name = USER.name;
        self.presenter.phone = USER.phone;
    }
    
    [self updateUI];
}

- (void)updateUI
{    
    //self.orderBtn.enabled = [self.presenter orderAvialable];
    self.remindBtn.enabled = [self.presenter remindAvialable];
    [self.remindBtn setImage:[UIImage imageNamed:self.presenter.needRemind ? @"check" : @"check_ar"] forState:UIControlStateNormal];
}

- (void)updateTimeIcon
{
    self.timeImg.image = [UIImage imageNamed:self.datePicker.hidden ? @"ic_down_cn" :  @"ic_up_cn"];
}

- (void)updateModelByField:(UITextField *)textField
{
    EditedFieldType type = textField.tag;
    
    switch (type)
    {
        case EditedFieldTypeName:
            textField.text = [textField.text capitalizedString];
            self.presenter.name = textField.text;
            break;
            
        case EditedFieldTypePhone:
            self.presenter.phone = textField.text;
            break;
            
        default:
            break;
    }
    
    [self updateUI];
}

#pragma mark - NITCallOrderPresenterProtocol
- (void)highlightEmptyFields
{
    UIColor * color = RGB(217, 67, 67);
    if(!([self.nameField.text length] > 0))
    {
        self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Как к вам обращаться?", nil)
                                                                               attributes:@{NSForegroundColorAttributeName: color}];
    }
    if(!([self.phoneField.text length] > 0))
    {
        self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Номер телефона", nil)
                                                                                attributes:@{NSForegroundColorAttributeName: color}];
    }
    if(!([self.timeField.text length] > 0))
    {
        self.timeField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Выберите удобное время", nil)
                                                                               attributes:@{NSForegroundColorAttributeName: color}];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.datePicker.hidden = true;
    [self updateTimeIcon];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self updateModelByField:textField];
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateModelByField:textField];
}

@end
