//
//  NITCallOrderViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITCallOrderProtocols.h"

@interface NITCallOrderViewController : NITBaseViewController <NITCallOrderViewProtocol>

@property (nonatomic, strong) id <NITCallOrderPresenterProtocol> presenter;

@end
