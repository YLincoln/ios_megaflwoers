//
//  NITCallOrderProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/07/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, EditedFieldType) {
    EditedFieldTypeName     = 0,
    EditedFieldTypePhone    = 1,
    EditedFieldTypeTime     = 2
};

@protocol NITCallOrderInteractorOutputProtocol;
@protocol NITCallOrderInteractorInputProtocol;
@protocol NITCallOrderViewProtocol;
@protocol NITCallOrderPresenterProtocol;
@protocol NITCallOrderLocalDataManagerInputProtocol;
@protocol NITCallOrderAPIDataManagerInputProtocol;

@class NITCallOrderWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITCallOrderViewProtocol
@required
@property (nonatomic, strong) id <NITCallOrderPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)highlightEmptyFields;
@end

@protocol NITCallOrderWireFrameProtocol
@required
+ (void)presentNITCallOrderModuleFrom:(id)fromViewController;
- (void)backActionFrom:(id)fromViewController;
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@end

@protocol NITCallOrderPresenterProtocol
@required
@property (nonatomic, weak) id <NITCallOrderViewProtocol> view;
@property (nonatomic, strong) id <NITCallOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCallOrderWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSDate * time;
@property (nonatomic) BOOL needRemind;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (BOOL)remindAvialable;
- (BOOL)orderAvialable;
- (NSString *)formattedTime;
- (void)orderAction;
@end

@protocol NITCallOrderInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)callBackSendSuccess;
- (void)callBackSendError:(NSString *)error;
@end

@protocol NITCallOrderInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCallOrderInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCallOrderAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCallOrderLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)callbackWithName:(NSString *)name phone:(NSString *)phone time:(NSString *)time;
@end


@protocol NITCallOrderDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCallOrderAPIDataManagerInputProtocol <NITCallOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)callbackWithName:(NSString *)name phone:(NSString *)phone time:(NSString *)time withHandler:(void(^)(NSError *error))handler;
@end

@protocol NITCallOrderLocalDataManagerInputProtocol <NITCallOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
