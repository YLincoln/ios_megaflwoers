//
//  NITProductDetailsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsWireFrame.h"
#import "NITCatalogDirectoryViewController.h"
#import "MSAlertController.h"
#import "NITPhotoSliderController.h"
#import "NITBouquetInfoWireFrame.h"
#import "NITCallOrderWireFrame.h"
#import "NITAuthorizationWireFrame.h"
#import "NITProductDetailsNavigationController.h"
#import "NITProductDetailsPreview.h"

@implementation NITProductDetailsWireFrame

#pragma mark - Create
+ (id)createNITProductDetailsModuleWithView:(id <NITProductDetailsViewProtocol>)view bouquet:(id)bouquet
{
    // Generating module components
    id <NITProductDetailsPresenterProtocol, NITProductDetailsInteractorOutputProtocol> presenter = [NITProductDetailsPresenter new];
    id <NITProductDetailsInteractorInputProtocol> interactor = [NITProductDetailsInteractor new];
    id <NITProductDetailsAPIDataManagerInputProtocol> APIDataManager = [NITProductDetailsAPIDataManager new];
    id <NITProductDetailsLocalDataManagerInputProtocol> localDataManager = [NITProductDetailsLocalDataManager new];
    id <NITProductDetailsWireFrameProtocol> wireFrame= [NITProductDetailsWireFrame new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    interactor.data = [bouquet copy];
    interactor.currentData = [bouquet copy];
    interactor.presentData = [bouquet copy];
    
    return view;
}

+ (id)createNITProductDetailsView
{
    id <NITProductDetailsWireFrameProtocol> wireFrame = [NITProductDetailsWireFrame new];
    return [(NITProductDetailsWireFrame *)wireFrame createViewControllerWithKey:_s(NITProductDetailsViewController) storyboardType:StoryboardTypeCatalog];
}

+ (id)createNITProductDetailsPreviewView
{
    return [NITProductDetailsView view];
}

+ (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model selectType:(ProductSelectType)selectType from:(NITBaseViewController *)fromViewController
{
    NITProductDetailsViewController * view = [NITProductDetailsWireFrame createNITProductDetailsModuleWithView:[NITProductDetailsWireFrame createNITProductDetailsView] bouquet:model];
    view.title = model.title;
    view.presenter.selectType = selectType;
    view.backViewController = fromViewController;
    view.preview = [NITProductDetailsPreview viewWithModel:model];
    
    [view.view addSubview:view.preview];

    NITProductDetailsNavigationController * navVC = [[NITProductDetailsNavigationController alloc] initWithRootViewController:view];
    navVC.presenter = view.presenter;
    navVC.preferredContentSize = CGSizeMake(ViewWidth(view.preview), ViewHeight(view.preview) - 44);
    
    [navVC setNavigationBarHidden:true];
    
    switch (model.contentType) {
        case ProductContentTypeBouqets:
            [TRACKER trackEvent:TrackerEventPreviewBouquet parameters:@{@"id":model.identifier}];
            break;
        default:
            break;
    }
    
    return navVC;
}

#pragma mark - Product
+ (void)presentNITProductDetailsModuleFrom:(UIViewController *)fromViewController bouquet:(NITCatalogItemViewModel *)bouquet selectType:(ProductSelectType)selectType
{
    NITProductDetailsViewController * view = [NITProductDetailsWireFrame createNITProductDetailsModuleWithView:[NITProductDetailsWireFrame createNITProductDetailsView] bouquet:bouquet];
    view.title = bouquet.title;
    view.presenter.selectType = selectType;

    switch (bouquet.contentType) {
        case ProductContentTypeBouqets:
            [TRACKER trackEvent:TrackerEventOpenBouquet parameters:@{@"id":bouquet.identifier}];
            break;
            
        case ProductContentTypeAttach:
            [TRACKER trackEvent:TrackerEventOpenAttach parameters:@{@"id":bouquet.identifier}];
            break;
    }

    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITProductDetailsPreviewModuleWithBouquet:(NITCatalogItemViewModel *)bouquet isViewMode:(BOOL)isViewMode
{
    NITProductDetailsView * view = [NITProductDetailsWireFrame createNITProductDetailsModuleWithView:[NITProductDetailsWireFrame createNITProductDetailsPreviewView]
                                                                                             bouquet:bouquet];
    view.isViewMode = isViewMode;
    
    [view show];
    
    switch (bouquet.contentType) {
        case ProductContentTypeBouqets:
            [TRACKER trackEvent:TrackerEventPreviewBouquet parameters:@{@"id":bouquet.identifier}];
            break;
        default:
            break;
    }
}

#pragma mark - Back
- (void)backToRootFrom:(NITBaseViewController *)fromViewController
{
    if (fromViewController.backViewController)
    {
        [fromViewController dismissViewControllerAnimated:true completion:^{
            [fromViewController.backViewController.navigationController popToRootViewControllerAnimated:false];
        }];
    }
    else
    {
        [fromViewController.navigationController popToRootViewControllerAnimated:true];
    }
}

- (void)backFrom:(NITBaseViewController *)fromViewController
{
    if ([fromViewController isKindOfClass:[UIViewController class]])
    {
        if (fromViewController.backViewController)
        {
            [fromViewController dismissViewControllerAnimated:true completion:^{
                [fromViewController.backViewController.navigationController popViewControllerAnimated:true];
            }];
        }
        else
        {
            [fromViewController.navigationController popViewControllerAnimated:true];
        }
    }
    else if ([fromViewController isKindOfClass:[UIView class]])
    {
        UIView * fromView = (UIView *)fromViewController;
        [fromView removeFromSuperview];
    }
}

#pragma mark - Product Details
- (void)showDetailsByModel:(NITCatalogItemViewModel *)model from:(UIViewController *)fromViewController
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController bouquet:model selectType:ProductSelectTypeView];
}

- (void)selectBouquetByModel:(NITCatalogItemViewModel *)model from:(UIViewController *)fromViewController
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController bouquet:model selectType:ProductSelectTypeSelect];
}

#pragma mark - Components
- (void)showComponentsForBouquet:(NITCatalogItemViewModel *)bouquet andSKUIndex:(NSInteger)index from:(UIViewController *)fromViewController
{
    [NITBouquetInfoWireFrame presentNITBouquetInfoModuleFrom:fromViewController withBouquet:bouquet andSKUIndex:index];
}

#pragma mark - Image
- (void)showImages:(NSArray <NSString *> *)imagesPaths from:(UIViewController *)fromViewController
{
    NITPhotoSliderController * photoVC = [[NITPhotoSliderController alloc] initWithImagePaths:imagesPaths];
    [fromViewController presentViewController:photoVC animated:true completion:nil];
}

#pragma mark - Call
- (void)showCallOrderFrom:(UIViewController *)fromViewController
{
    [NITCallOrderWireFrame presentNITCallOrderModuleFrom:fromViewController];
}

#pragma mark - Register
- (void)showRegisterFrom:(UIViewController *)fromViewController
{
    [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:fromViewController];
}

@end
