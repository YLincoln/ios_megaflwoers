//
//  NITProductDetailsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProductDetailsProtocols.h"
#import "NITProductDetailsViewController.h"
#import "NITProductDetailsLocalDataManager.h"
#import "NITProductDetailsAPIDataManager.h"
#import "NITProductDetailsInteractor.h"
#import "NITProductDetailsPresenter.h"
#import "NITProductDetailsWireframe.h"
#import "NITRootWireframe.h"
#import "NITProductDetailsView.h"

@interface NITProductDetailsWireFrame : NITRootWireframe <NITProductDetailsWireFrameProtocol>

+ (id)createNITProductDetailsView;
+ (id)createNITProductDetailsModuleWithView:(id <NITProductDetailsViewProtocol>)view bouquet:(id)bouquet;

@end
