//
//  NITProductDetailsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsPresenter.h"
#import "NITProductDetailsWireframe.h"

#import "NITBasketInteractor.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITProductDetailsPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self initData];
}

#pragma mark - Data
- (void)initData
{
    id view = self.view;
    
    if ([view isKindOfClass:[UIViewController class]])
    {
        NSMutableArray * sectionTypes;
        weaken(self);
        
        switch (self.interactor.data.contentType) {
            case ProductContentTypeBouqets: {
                
                sectionTypes = [NSMutableArray new];
                
                if ([self.interactor.data.previewPath length] > 0)
                {
                    [sectionTypes addObject:[[NITProductSectionType alloc] initWithType:SectionTypeFoto]];
                }
                
                if ([self.interactor.data.videoPath length] > 0)
                {
                    [sectionTypes addObject:[[NITProductSectionType alloc] initWithType:SectionTypeVideo]];
                }
                
                if ([self.interactor.data.skus bk_select:^BOOL(NITCatalogItemSKUViewModel * obj) {
                    return [obj.image360Path length] > 0;
                }].count > 0)
                {
                    [sectionTypes addObject:[[NITProductSectionType alloc] initWithType:SectionType3D]];
                }
                
                if ([self.interactor.data.masterPath length] > 0)
                {
                    [sectionTypes addObject:[[NITProductSectionType alloc] initWithType:SectionTypeMaster]];
                }
                
                [self.interactor updateAttachDataWithHandler:^(NSArray<NITCatalogItemViewModel *> *models) {
                    [weakSelf.view reloadAttachData:models];
                }];
            }
                break;
                
            case ProductContentTypeAttach: {
                [self.view reloadAttachData:nil];
            }
                break;
        }
        
        [self.view reloadViewTypesData:sectionTypes];
        
        [self.interactor updateRecommendationsDataWithHandler:^(NSArray<NITCatalogItemViewModel *> *models) {
            [weakSelf.view reloadRecommendationsData:models];
        }];
    }
    
    [self.interactor updateDataBySelectType:self.selectType];
}

- (void)updateData
{
    [self.view reloadDataByModel:self.interactor.currentData];
}

- (void)updateDataByModel:(NITCatalogItemViewModel *)model
{
    self.interactor.currentData = model;
    [self.view reloadDataByModel:model];
}

- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model andSku:(NITCatalogItemSKUViewModel *)sku;
{
    if ([USER isAutorize])
    {
        [self.interactor changeFavoriteStateForBouquet:model andSku:sku];
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Регистрация", nil) actionBlock:^{
            [self.wireFrame showRegisterFrom:WINDOW.visibleController];
        }];
        
        [HELPER showActionSheetFromView:WINDOW.visibleController withTitle:NSLocalizedString(@"Данная функция приложения не доступна без регистрации", nil)  actionButtons:@[action1] cancelButton:cancel];
    }
}

- (void)updatePriceValue:(NSNumber *)value
{
    [self.view reloadPriceText:[self priceTitleByValue:value]];
}

- (BOOL)isEditedSKUByIndex:(NSInteger)index
{
    NITCatalogItemSKUViewModel * model = self.interactor.data.skus[index];
    NITCatalogItemSKUViewModel * currentModel = self.interactor.currentData.skus[index];
    currentModel.edited = ![model isEqualSKU:currentModel];
    
    return currentModel.edited;
}

- (BOOL)isEditedBusketBouquet
{
    if (self.interactor.presentData && self.interactor.currentData)
    {
        NITCatalogItemSKUViewModel * model = self.interactor.presentData.skus.firstObject;
        NITCatalogItemSKUViewModel * currentModel = self.interactor.currentData.skus.firstObject;
        currentModel.edited = ![model isEqualSKU:currentModel];
        
        return currentModel.edited;
    }
    else
    {
        return false;
    }
}

- (void)buyBouquet:(NITCatalogItemViewModel *)bouquet withSKUIndex:(NSInteger)index
{
    switch (self.selectType) {
        case ProductSelectTypeView: {
            [self.interactor saveProductModel:bouquet skuIndex:index];
            [[NSNotificationCenter defaultCenter] postNotificationName:kOpenbasketNotification object:nil];
        }
            break;
        case ProductSelectTypeSelect: {
            [self.interactor saveProductModel:bouquet skuIndex:index];
            [self.wireFrame backToRootFrom:self.view];
        }
            break;
        case ProductSelectTypeEdite: {
            [self.interactor saveEditedProductModel:bouquet skuInex:index];
        }
    }
}

- (void)downloadImageByPath:(NSString *)path withHandler:(void(^)(UIImage * image))handler
{
    [API imageByPath:path complition:^(UIImage *image) {
        handler(image);
    }];
}

#pragma mark - Action
- (void)callAction
{
    [self callActionFromView:self.view];
}

- (void)callActionFromView:(id)view
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Заказ звонка", nil) actionBlock:^{
        if (weakSelf.view) [weakSelf.wireFrame showCallOrderFrom:weakSelf.view];
        else [[NSNotificationCenter defaultCenter] postNotificationName:kOpenCallOrderPreviewNotification object:nil];

    }];
    
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Сделать заказ по телефону", nil) actionBlock:^{
        [PHONE callToPhoneNumber:USER.cityPhone];
        [TRACKER trackEvent:TrackerEventOrderByPhone];
    }];
    
    [HELPER showActionSheetFromView:WINDOW.visibleController withTitle:nil actionButtons:@[action1, action2] cancelButton:cancel];
}

- (NSArray<id<UIPreviewActionItem>> *)previewActionItems
{
    weaken(self);
    UIPreviewAction * action1 = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Быстрая покупка", nil)
                                                           style:UIPreviewActionStyleDefault
                                                         handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
                                                             UINavigationController * navVC = (UINavigationController *)previewViewController;
                                                             NITBaseViewController * vc = (NITBaseViewController *)navVC.topViewController;
                                                             [weakSelf callActionFromView:vc.backViewController];
                                                         }];
    
    
    NITCatalogItemSKUViewModel * sku = self.interactor.currentData.skus.firstObject;
    UIPreviewAction * action2 = [UIPreviewAction actionWithTitle:[self priceTitleByValue:sku.price].string
                                                           style:UIPreviewActionStyleDefault
                                                         handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
                                                             [weakSelf buyBouquet:weakSelf.interactor.currentData withSKUIndex:0];
    }];
    
    return @[action1, action2];
}

#pragma mark - Navigation
- (void)showDetailsByModel:(NITCatalogItemViewModel *)model
{
    [self.wireFrame showDetailsByModel:model from:self.view];
}

- (void)showImages:(NSArray<NSString *> *)imagesPaths
{
    [self.wireFrame showImages:imagesPaths from:self.view];
}

- (void)showComponentsForBouquet:(NITCatalogItemViewModel *)bouquet andSKUIndex:(NSInteger)index
{
    [self.wireFrame showComponentsForBouquet:bouquet andSKUIndex:index from:self.view];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoExitFullScreen) name:UIWindowDidBecomeHiddenNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)videoExitFullScreen
{
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
}

- (NSMutableAttributedString *)priceTitleByValue:(NSNumber *)value
{
    NSString * priceTitle;
    id view = self.view;
    
    if ([view isKindOfClass:[UIViewController class]])
    {
        priceTitle = [NSString stringWithFormat:@"%@%@ ₽", NSLocalizedString(@"Купить за ", nil), value];
    }
    else
    {
        priceTitle =  [NSString stringWithFormat:@"%@%@%@ ₽", NSLocalizedString(@"Купить за ", nil), (IS_IPHONE_5 || IS_IPHONE_4) ? @"\n" : @"", value];
    }
    
    NSMutableAttributedString * attrTitle = [[NSMutableAttributedString alloc] initWithString:priceTitle];
    
    NSRange range = [priceTitle rangeOfString:NSLocalizedString(@"Купить за ", nil)];
    [attrTitle addAttribute:NSFontAttributeName
                      value:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]
                      range:NSMakeRange(range.length, attrTitle.length - range.length)];
    
    [attrTitle addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:NSMakeRange(0, attrTitle.length)];
    
    return attrTitle;
}

#pragma mark - NITProductDetailsInteractorOutputProtocol
- (void)updateModel:(NITCatalogItemViewModel *)model
{
    [self.view reloadDataByModel:model];
}

- (void)showOpenProductErrorForModel:(NITCatalogItemViewModel *)model
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{
        [weakSelf.wireFrame backFrom:weakSelf.view];
    }];
    
    NSString * title;
    switch (model.contentType) {
        case ProductContentTypeBouqets:
            title = [NSString stringWithFormat:@"%@ «%@» %@ %@",
                     NSLocalizedString(@"Букета", nil),
                     model.title,
                     NSLocalizedString(@"нет в продаже в г.", nil),
                     USER.cityName];
            break;
            
        case ProductContentTypeAttach:
            title = [NSString stringWithFormat:@"«%@» %@ %@",
                     model.title,
                     NSLocalizedString(@"нет в продаже в г.", nil),
                     USER.cityName];
            break;
    }
    
    [HELPER showActionSheetFromView:WINDOW.visibleController withTitle:title actionButtons:nil cancelButton:cancel];
}

@end
