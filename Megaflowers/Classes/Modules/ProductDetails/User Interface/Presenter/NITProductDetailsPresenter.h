//
//  NITProductDetailsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProductDetailsProtocols.h"

@class NITProductDetailsWireFrame;

@interface NITProductDetailsPresenter : NITRootPresenter <NITProductDetailsPresenterProtocol, NITProductDetailsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITProductDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITProductDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITProductDetailsWireFrameProtocol> wireFrame;
@property (nonatomic) ProductSelectType selectType;

@end
