//
//  NITProductAttachments.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductAttachments.h"
#import "NITAttachView.h"

@interface NITProductAttachments ()
<
HTHorizontalSelectionListDelegate,
HTHorizontalSelectionListDataSource
>

@property (nonatomic) IBOutlet UIView * view;
@property (nonatomic) IBOutlet HTHorizontalSelectionList * attachmentsList;

@end

@implementation NITProductAttachments

#pragma mark - Lifecycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self prepareLayout];
        [self initializeSubviews];
        [self initUI];
    }
    
    return self;
}

- (void)prepareLayout
{
    self.frame = CGRectMake(ViewX(self), ViewY(self), ViewWidth(WINDOW), ViewHeight(self));
}

- (void)initializeSubviews
{
    [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    
    self.view.frame = self.bounds;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self addSubview:self.view];
}

#pragma mark - Custom Accessors
- (void)setItems:(NSArray *)items
{
    _items = items;
    [self.attachmentsList reloadData];
}

#pragma mark - Private
- (void)initUI
{
    self.attachmentsList.delegate = self;
    self.attachmentsList.dataSource = self;
    self.attachmentsList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.attachmentsList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.attachmentsList.bottomTrimHidden = true;
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [self.items count];
}

- (nullable UIView *)selectionList:(HTHorizontalSelectionList *)selectionList viewForItemWithIndex:(NSInteger)index
{
    NITAttachView * view = [NITAttachView viewByModel:self.items[index] andIndex:index];
    return view;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    [self.presenter showDetailsByModel:self.items[index]];
}

@end
