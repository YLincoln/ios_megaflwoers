//
//  NITProductAttachments.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsProtocols.h"

@interface NITProductAttachments : UIView

@property (nonatomic) id <NITProductDetailsPresenterProtocol> presenter;
@property (nonatomic) NSArray * items;

@end
