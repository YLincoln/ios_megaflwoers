//
//  NITProductDetailsView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/2/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsProtocols.h"

static NSString * kOpenProductPreviewNotification  = @"open_product_preview_notification";
static NSString * kCloseProductPreviewNotification = @"close_product_preview_notification";
static NSString * kOpenCallOrderPreviewNotification = @"open_call_order_preview_notification";

@interface NITProductDetailsView : UIView <NITProductDetailsViewProtocol>

@property (nonatomic, strong) id <NITProductDetailsPresenterProtocol> presenter;
@property (nonatomic) BOOL isViewMode;

+ (instancetype)view;

- (void)show;

@end
