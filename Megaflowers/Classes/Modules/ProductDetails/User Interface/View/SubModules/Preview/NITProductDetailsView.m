//
//  NITProductDetailsView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/2/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsView.h"
#import "UIImageView+AFNetworking.h"
#import "NITCatalogItemSKUViewModel.h"
#import "NITProductDetailsSKUView.h"
#import "UIImage+Blurring.h"

@interface NITProductDetailsView ()
<
HTHorizontalSelectionListDelegate,
HTHorizontalSelectionListDataSource
>

@property (nonatomic) IBOutlet UIImageView * bluredBg;
@property (nonatomic) IBOutlet UIImageView * imageView;
@property (nonatomic) IBOutlet UIView * blackView;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * likeBtn;
@property (nonatomic) IBOutlet UIButton * closeBtn;
@property (nonatomic) IBOutlet UIButton * fastBuyBtn;
@property (nonatomic) IBOutlet UIButton * buyBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * buyBtnH;
@property (nonatomic) IBOutlet HTHorizontalSelectionList * skuList;

@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic) NSNumber * currentPrice;
@property (nonatomic) NSInteger skuIndex;
@property (nonatomic) BOOL presented;

@end

@implementation NITProductDetailsView

+ (instancetype)view
{
    NITProductDetailsView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITProductDetailsView) owner:self options:nil] firstObject];
    
    [view initUI];

    return view;
}

- (void)setCurrentPrice:(NSNumber *)currentPrice
{
    _currentPrice = currentPrice;
    [self updatePriceByIndex:self.skuIndex];
}

- (void)setIsViewMode:(BOOL)isViewMode
{
    _isViewMode = isViewMode;
    [self updateBuyButtons];
}

- (void)setModel:(NITCatalogItemViewModel *)model
{
    _model = model;
    _skuIndex = 0;
    
    if (model.favoriteSkuID)
    {
        NITCatalogItemSKUViewModel * sku = [model.skus bk_select:^BOOL(NITCatalogItemSKUViewModel * obj) {
            return [obj.identifier isEqualToNumber:model.favoriteSkuID];
        }].firstObject;
        
        if (sku)
        {
            _skuIndex = [model.skus indexOfObject:sku];
        }
    }
    else if (model.defaultSku)
    {
        _skuIndex = [model.skus indexOfObject:model.defaultSku];
    }
    
    [self.skuList reloadData];
    [self updateUI];
    
    if (_skuIndex != 0)
    {
        [self changeCurrentSkuIndex:_skuIndex];
    }
}

#pragma mark - IBAction
- (IBAction)likeAction:(id)sender
{
    if (![USER isAutorize])
    {
        [self closeWithComplition:^{
            [self changeFavoriteState];
        }];
    }
    else
    {
        [self changeFavoriteState];
    }
}

- (IBAction)closeAction:(id)sender
{
    [self closeWithComplition:^{}];
}

- (IBAction)fastBuyAction:(id)sender
{
    weaken(self);
    [self closeWithComplition:^{
        [weakSelf.presenter callAction];
    }];
}

- (IBAction)buyAction:(id)sender
{
    weaken(self);
    [self closeWithComplition:^{
        [weakSelf.presenter buyBouquet:weakSelf.model withSKUIndex:weakSelf.skuList.selectedButtonIndex];
    }];
}

#pragma mark - Public
- (void)show
{
    self.alpha = 0;
    [WINDOW addSubview:self];
    [self.presenter initData];
}

#pragma mark - Private
- (void)initUI
{
    self.frame = WINDOW.frame;
    self.bluredBg.image = [[UIImage screenshot] gaussianBlurWithBias:1];
    
    // sku list
    self.skuList.delegate = self;
    self.skuList.dataSource = self;
    self.skuList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.skuList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.skuList.bottomTrimHidden = true;
    
    UIGestureRecognizer * tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeAction:)];
    tapper.cancelsTouchesInView = false;
    [self.blackView addGestureRecognizer:tapper];
    
    [self updateBuyButtons];
}

- (void)updateUI
{
    self.title.text = self.model.title;
    
    NITCatalogItemSKUViewModel * model = [self.model.skus firstObject];
    self.currentPrice = model.price ? : self.model.discountPrice;

    self.imageView.image = nil;
    NSString * imagePath = self.model.imagePaths[self.skuIndex];
    [self.imageView setImageWithURL:[NSURL URLWithString:imagePath]];
    
    [self updateFavoriteState];
}

- (void)showSelfIfNeed
{
    if (!self.presented)
    {
        self.presented = true;
        [UIView animateWithDuration:0.3f animations:^{
            self.alpha = 1.f;
        } completion:^(BOOL finished) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kOpenProductPreviewNotification object:nil];
        }];
    }
}

- (void)changeFavoriteState
{
    NSInteger index = self.skuList.selectedButtonIndex;
    
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        NITCatalogItemSKUViewModel * sku = self.model.skus[index];
        sku.favorite = !sku.favorite;
        [self.presenter changeFavoriteStateForBouquet:self.model andSku:sku];
        [self updateFavoriteState];
    }
}

- (void)updateFavoriteState
{
    self.likeBtn.hidden = self.model.contentType != ProductContentTypeBouqets;
    NSInteger index = self.skuList.selectedButtonIndex;
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        NITCatalogItemSKUViewModel * skuModel = self.model.skus[index];
        NSString * imgaeName = skuModel.favorite ? @"pink_heart" : @"green_heart";
        [self.likeBtn setImage:[UIImage imageNamed:imgaeName] forState:UIControlStateNormal];
    }
}

- (void)updatePriceByIndex:(NSInteger)index
{
    NSNumber * price = @0;
    
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        NITCatalogItemSKUViewModel * skuModel = self.model.skus[index];
        price = skuModel.discountPrice;
    }
    
    [self.presenter updatePriceValue:price];
}

- (void)updateBuyButtons
{
    self.buyBtnH.constant = self.isViewMode ? 0 : 45;
    [self layoutIfNeeded];
    
    self.buyBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.buyBtn.titleLabel.numberOfLines = 2;
    self.buyBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.fastBuyBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.fastBuyBtn.titleLabel.numberOfLines = 2;
    self.fastBuyBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)changeCurrentSkuIndex:(NSInteger)index
{
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        self.skuList.selectedButtonIndex = index;
        
        [self.skuList reloadData];
        [self updatePriceByIndex:index];
    }
}

- (void)closeWithComplition:(void(^)())complitionBlock
{
    [UIView animateWithDuration:0.3f animations:^{
        self.alpha = 0.f;
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kCloseProductPreviewNotification object:nil];
        [self removeFromSuperview];
        complitionBlock();
    }];
}

#pragma mark - NITProductDetailsViewProtocol
- (void)reloadDataByModel:(NITCatalogItemViewModel *)model
{
    [self showSelfIfNeed];
    self.model = model;
}

- (void)reloadPriceText:(NSAttributedString *)text
{
    [self.buyBtn setAttributedTitle:text forState:UIControlStateNormal];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [self.model.skus count];
}

- (nullable UIView *)selectionList:(HTHorizontalSelectionList *)selectionList viewForItemWithIndex:(NSInteger)index
{
    NITProductDetailsSKUView * view = [NITProductDetailsSKUView viewByModel:self.model.skus[index]
                                                                 tableWidth:ViewWidth(self.skuList)
                                                                   andState:self.skuList.selectedButtonIndex == index];
    return view;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    self.skuIndex = index;
    NITCatalogItemSKUViewModel * model = self.model.skus[index];
    self.currentPrice = model.discountPrice;
    [self.skuList reloadData];
    [self updateUI];
}

@end
