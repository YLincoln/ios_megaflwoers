//
//  NITProductSections.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductSections.h"
#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"
#import "JVYoutubePlayerView.h"
#import "NITProductDetailsImageCell.h"
#import "NITProductDetailsSKUView.h"
#import "NSArray+BlocksKit.h"
#import "NITGuideBouquetInfoView.h"
#import "NITGuideBouquetEditedView.h"
#import "NITGuideBouquetSwipeView.h"
#import "NITProductDetailsViewController.h"

@interface NITProductSections ()
<
UIScrollViewDelegate,
UICollectionViewDelegate,
UICollectionViewDataSource,
HTHorizontalSelectionListDelegate,
HTHorizontalSelectionListDataSource
>

@property (nonatomic) IBOutlet UIView * view;
@property (nonatomic) IBOutlet UIView * productSections;

// Image section
@property (nonatomic) IBOutlet UIView * imageSection;
@property (nonatomic) IBOutlet HTHorizontalSelectionList * skuList;
@property (nonatomic) IBOutlet UICollectionView * imageCollection;
@property (nonatomic) IBOutlet UIPageControl * pageControl;
@property (nonatomic) IBOutlet UIButton * likeBtn;
@property (nonatomic) IBOutlet UIButton * infoBtn;
@property (nonatomic) IBOutlet UILabel * editedLbl;
@property (nonatomic) IBOutlet UIImageView * editedImg;

@property (nonatomic) NSArray * skuItems;
@property (nonatomic) NSArray * imageItems;
@property (nonatomic) NSInteger skuIndex;

// Video section
@property (nonatomic) IBOutlet UIView * videoSection;
@property (nonatomic) IBOutlet JVYoutubePlayerView * videoView;
@property (nonatomic) IBOutlet UILabel * videoComment;

// 360 section
@property (nonatomic) IBOutlet UIView * section360;
@property (nonatomic) IBOutlet NITImageView360 * image360;

// Master class section
@property (nonatomic) IBOutlet UIView * masterClassSection;
@property (nonatomic) IBOutlet JVYoutubePlayerView * masterCalssVideoView;

//
@property (nonatomic) BOOL defaultSkuShowed;

@end

@implementation NITProductSections

#pragma mark - Lifecycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self prepareLayout];
        [self initializeSubviews];
        [self initUI];
    }
    
    return self;
}

- (void)prepareLayout
{
    self.frame = CGRectMake(ViewX(self), ViewY(self), ViewWidth(WINDOW), ViewHeight(self));
}

- (void)initializeSubviews
{
    [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    
    self.view.frame = self.bounds;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    [self addSubview:self.view];
}

#pragma mark - Custom Accessors
- (void)setModel:(NITCatalogItemViewModel *)model
{
    _model = model;
    [self updateData];
}

- (void)setSectionType:(SectionType)sectionType
{
    _sectionType = sectionType;
    [self updateSections];
}

#pragma mark - IBAction
- (IBAction)likeAction:(id)sender
{
    NSInteger index = self.skuList.selectedButtonIndex;
    
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        NITCatalogItemSKUViewModel * sku = self.model.skus[index];
        sku.favorite = !sku.favorite;
        [self.presenter changeFavoriteStateForBouquet:self.model andSku:sku];
        [self updateFavoriteState];
    }
}

- (IBAction)moreInfoAction:(id)sender
{
    self.defaultSkuShowed = true;
    [self.presenter showComponentsForBouquet:self.model andSKUIndex:self.skuList.selectedButtonIndex];
}

- (IBAction)changePage:(id)sender
{
    NSInteger index = self.pageControl.currentPage;
    
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        self.skuList.selectedButtonIndex = index;
        
        [self.skuList reloadData];
        [self updateEditedState];
        [self updatePriceByIndex:index];
        [self scrollToImageByIndex:index];
    }
}

#pragma mark - Public
- (void)setModel:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex
{
    _model = model;
    _skuIndex = skuIndex;
    
    if (model.favoriteSkuID)
    {
        NITCatalogItemSKUViewModel * sku = [model.skus bk_select:^BOOL(NITCatalogItemSKUViewModel * obj) {
            return [obj.identifier isEqualToNumber:model.favoriteSkuID];
        }].firstObject;
        
        if (sku)
        {
            _skuIndex = [model.skus indexOfObject:sku];
        }
    }
    else if (model.defaultSku && !self.defaultSkuShowed)
    {
        self.defaultSkuShowed = true;
        _skuIndex = [model.skus indexOfObject:model.defaultSku];
    }
    
    [self updateData];
    
    if (_skuIndex != skuIndex)
    {
        [self changeCurrentSkuIndex:_skuIndex];
    }
}

#pragma mark - Private
- (void)initUI
{
    self.skuList.delegate = self;
    self.skuList.dataSource = self;
    self.skuList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.skuList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.skuList.bottomTrimHidden = true;
    
    [self.imageCollection registerNib:[UINib nibWithNibName:_s(NITProductDetailsImageCell) bundle:nil] forCellWithReuseIdentifier:_s(NITProductDetailsImageCell)];
    UICollectionViewFlowLayout * flowLayout = (id)self.imageCollection.collectionViewLayout;
    flowLayout.itemSize = CGSizeMake(ViewWidth(self), ViewWidth(self) * 0.66 - 21);
    
    self.pageControl.pageIndicatorTintColor = RGBA(19, 95, 52, 0.1);
    self.pageControl.currentPageIndicatorTintColor = RGBA(19, 95, 52, 0.44);
    
    [self.infoBtn setTitle:NSLocalizedString(@"ИНФО", nil) forState:UIControlStateNormal];
}

- (void)updateData
{
    [self updateImageSection];
    [self updateVideoSection];
    [self update360Section];
    [self updateMasterClassSection];
    [self updatePriceByIndex:self.skuIndex];
    [self showGuideIfNeed];
}

- (void)updateSections
{
    self.imageSection.hidden        = self.sectionType != SectionTypeFoto;
    self.videoSection.hidden        = self.sectionType != SectionTypeVideo;
    self.section360.hidden          = self.sectionType != SectionType3D;
    self.masterClassSection.hidden  = self.sectionType != SectionTypeMaster;
    
    [self showGuideIfNeed];
}

- (void)changeCurrentSkuIndex:(NSInteger)index
{
    self.pageControl.currentPage = index;
    
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        self.skuList.selectedButtonIndex = index;
        
        [self.skuList reloadData];
        [self updateEditedState];
        [self updatePriceByIndex:index];
        [self scrollToImageByIndex:index];
        [self update360Section];
    }
}

- (void)updatePriceByIndex:(NSInteger)index
{
    NSNumber * price = @0;
    
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        NITCatalogItemSKUViewModel * skuModel = self.model.skus[index];
        price = skuModel.discountPrice;
    }
    
    [self updateFavoriteState];
    [self.presenter updatePriceValue:price];
    
    if ([self.delegate respondsToSelector:@selector(didChangeSkuIndex:)])
    {
        [self.delegate didChangeSkuIndex:index];
    }
}

- (void)showGuideIfNeed
{
    if (![WINDOW.visibleController isKindOfClass:[NITProductDetailsViewController class]] ||
        self.model.contentType != ProductContentTypeBouqets)
    {
        return;
    }
    
    if ([self forceTouchActive])
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showGuideIfNeed];
        });
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if ([WINDOW.visibleController isKindOfClass:[NITProductDetailsViewController class]])
            {
                switch (self.sectionType) {
                        
                    case SectionTypeFoto: {
                        
                        CGFloat offset = self.sectionsCount > 1 ? 44 : 0;
                        
                        if (self.model.edited)
                        {
                            if ([NITGuideBouquetEditedView notYetShown])
                            {
                                [NITGuideBouquetEditedView showWidthOverlayView:self.editedImg withYOffset:offset];
                            }
                        }
                        else
                        {
                            if ([NITGuideBouquetInfoView notYetShown])
                            {
                                [NITGuideBouquetInfoView showWidthOverlayView:self.infoBtn withYOffset:offset];
                            }
                        }
                    }
                        break;
                        
                    case SectionType3D: {
                        
                        if ([NITGuideBouquetSwipeView notYetShown])
                        {
                            [NITGuideBouquetSwipeView show];
                        }
                    }
                        break;
                        
                    default:
                        break;
                }
            }
        });
    }
}

- (BOOL)forceTouchActive
{
    return [WINDOW viewWithTag:kPreviewTag] ? true : false;
}

#pragma mark - Image section
- (void)updateImageSection
{
    [self.skuList reloadData];
    [self updateFavoriteState];
    [self updateEditedState];
    
    self.infoBtn.hidden = self.model.contentType != ProductContentTypeBouqets;

    self.imageItems = self.model.imagePaths;
    self.pageControl.numberOfPages = [self.imageItems count];
    [self.imageCollection reloadData];
}

- (void)updateFavoriteState
{
    self.likeBtn.hidden = self.model.contentType != ProductContentTypeBouqets;
    NSInteger index = self.skuList.selectedButtonIndex;
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        NITCatalogItemSKUViewModel * skuModel = self.model.skus[index];
        NSString * imgaeName = skuModel.favorite ? @"pink_heart" : @"green_heart";
        [self.likeBtn setImage:[UIImage imageNamed:imgaeName] forState:UIControlStateNormal];
    }
}

- (void)updateEditedState
{
    if (self.model.contentType != ProductContentTypeAttach)
    {
        //self.editedLbl.hidden = ![self.presenter isEditedSKUByIndex:self.skuList.selectedButtonIndex];
        self.editedImg.hidden = ![self.presenter isEditedSKUByIndex:self.skuList.selectedButtonIndex];
    }
}

- (void)scrollToImageByIndex:(NSInteger)index
{
    [self.imageCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                 atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                         animated:true];
}

#pragma mark - Video section
- (void)updateVideoSection
{
    if (self.model.videoPath)
    {
        [self.videoView loadPlayerWithVideoURL:self.model.videoPath];
        [self.videoComment setText:self.model.videoComment];
    }
}

#pragma mark - 360 section
- (void)update360Section
{
    NSInteger index = self.skuList.selectedButtonIndex;
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        weaken(self);
        self.image360.frameImage = nil;
        NITCatalogItemSKUViewModel * skuModel = self.model.skus[index];
        [self.presenter downloadImageByPath:skuModel.image360Frame withHandler:^(UIImage * frameImage){
            [weakSelf.presenter downloadImageByPath:skuModel.image360Path withHandler:^(UIImage * image){
                weakSelf.image360.imageFrameCount = image.size.width / frameImage.size.width;
                weakSelf.image360.frameImage = image;
            }];
        }];
    }
}

#pragma mark - Master class section
- (void)updateMasterClassSection
{
    if (self.model.masterPath)
    {
        [self.masterCalssVideoView loadPlayerWithVideoURL:self.model.videoPath];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.imageItems count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NITProductDetailsImageCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITProductDetailsImageCell) forIndexPath:indexPath];
    cell.imagePath = self.imageItems[indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.pageControl setCurrentPage:indexPath.row];
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * imagePath = self.imageItems[indexPath.row];
    
    if (imagePath)
    {
        [self.presenter showImages:@[imagePath]];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(ViewWidth(self.imageCollection), ViewHeight(self.imageCollection));
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger index = self.imageCollection.contentOffset.x / ViewWidth(self.imageCollection);
    
    self.pageControl.currentPage = index;
    self.skuList.selectedButtonIndex = index;
    
    [self.skuList reloadData];
    [self updatePriceByIndex:index];
    [self updateEditedState];
    [self update360Section];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [self.model.skus count];
}

- (nullable UIView *)selectionList:(HTHorizontalSelectionList *)selectionList viewForItemWithIndex:(NSInteger)index
{
    NITProductDetailsSKUView * view = [NITProductDetailsSKUView viewByModel:self.model.skus[index]
                                                                 tableWidth:ViewWidth(self.skuList)
                                                                   andState:self.skuList.selectedButtonIndex == index];
    return view;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    self.pageControl.currentPage = index;
    self.skuList.selectedButtonIndex = index;
    
    [self.skuList reloadData];
    [self updateEditedState];
    [self updatePriceByIndex:index];
    [self scrollToImageByIndex:index];
    [self update360Section];
}

@end
