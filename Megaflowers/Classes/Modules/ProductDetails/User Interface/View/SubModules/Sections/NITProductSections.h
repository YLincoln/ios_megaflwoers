//
//  NITProductSections.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsProtocols.h"
#import "NITProductSectionTypes.h"

@class NITCatalogItemViewModel;

@protocol NITProductSectionsDelegate <NSObject>

- (void)didChangeSkuIndex:(NSInteger)index;

@end

@interface NITProductSections : UIView

@property (nonatomic) id <NITProductDetailsPresenterProtocol> presenter;
@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic) SectionType sectionType;
@property (nonatomic) NSInteger sectionsCount;
@property (nonatomic, weak) id <NITProductSectionsDelegate> delegate;

- (void)setModel:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex;

@end
