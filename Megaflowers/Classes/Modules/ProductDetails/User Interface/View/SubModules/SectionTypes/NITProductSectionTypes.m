//
//  NITProductSectionTypes.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductSectionTypes.h"
#import "NITContentTypeView.h"

@interface NITProductSectionTypes ()
<
HTHorizontalSelectionListDelegate,
HTHorizontalSelectionListDataSource
>

@property (nonatomic) IBOutlet HTHorizontalSelectionList * typesList;

@end

@implementation NITProductSectionTypes

#pragma mark - Lifecycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self initUI];
    }
    
    return self;
}

#pragma mark - Custom Accessors
- (void)setItems:(NSArray *)items
{
    _items = items;
    [self.typesList reloadData];
}

#pragma mark - Private
- (void)initUI
{
    self.frame = CGRectMake(ViewX(self), ViewY(self), ViewWidth(WINDOW), ViewHeight(self));
    
    self.typesList = [HTHorizontalSelectionList new];
    self.typesList.frame = self.frame;
    self.typesList.delegate = self;
    self.typesList.dataSource = self;
    self.typesList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.typesList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.typesList.bottomTrimHidden = true;
    
    [self addSubview:self.typesList];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [self.items count];
}

- (nullable UIView *)selectionList:(HTHorizontalSelectionList *)selectionList viewForItemWithIndex:(NSInteger)index
{
    NITContentTypeView * view = [NITContentTypeView viewTitle:[self.items[index] title]
                                                   dataSource:self.items
                                                     andState:self.typesList.selectedButtonIndex == index];
    return view;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    [self.typesList reloadData];
    
    if ([self.delegate respondsToSelector:@selector(didChangeSectionType:)])
    {
        NITProductSectionType * item = self.items[index];
        [self.delegate didChangeSectionType:item.type];
    }
}


@end
