//
//  NITProductSectionTypes.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductSectionType.h"

@protocol NITProductSectionTypesDelegate <NSObject>

- (void)didChangeSectionType:(SectionType)type;

@end

@interface NITProductSectionTypes : UIView

@property (nonatomic) NSArray <NITProductSectionType *> * items;
@property (nonatomic, weak) id <NITProductSectionTypesDelegate> delegate;

@end
