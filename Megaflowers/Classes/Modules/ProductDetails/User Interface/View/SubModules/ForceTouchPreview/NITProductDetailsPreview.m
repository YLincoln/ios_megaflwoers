//
//  NITProductDetailsPreview.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/8/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsPreview.h"
#import "NITCatalogItemViewModel.h"
#import "UIImageView+AFNetworking.h"
#import "NITCatalogItemSKUViewModel.h"
#import "NITProductDetailsSKUView.h"

@interface NITProductDetailsPreview ()
<
HTHorizontalSelectionListDelegate,
HTHorizontalSelectionListDataSource
>

@property (nonatomic) IBOutlet UIImageView * imageView;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * likeBtn;
@property (nonatomic) IBOutlet HTHorizontalSelectionList * skuList;

@property (nonatomic) NITCatalogItemViewModel * model;

@end

@implementation NITProductDetailsPreview

+ (instancetype)viewWithModel:(NITCatalogItemViewModel *)model
{
    NITProductDetailsPreview * view = [[[NSBundle mainBundle] loadNibNamed:_s(self) owner:self options:nil] firstObject];
    
    [view initUI];
    [view setModel:model];
    
    return view;
}

- (void)setModel:(NITCatalogItemViewModel *)model
{
    _model = model;
   
    [self.skuList reloadData];
    [self updateUI];
}

#pragma mark - Private
- (void)initUI
{
    self.tag = kPreviewTag;
    
    // sku list
    self.skuList.delegate = self;
    self.skuList.dataSource = self;
    self.skuList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.skuList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.skuList.bottomTrimHidden = true;
    
    self.likeBtn.hidden = HELPER.forceTouchAvailable;
}

- (void)updateUI
{
    self.title.text = self.model.title;
    self.imageView.image = nil;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.model.previewPath]];
    
    [self updateFavoriteState];
}

- (void)updateFavoriteState
{
    self.likeBtn.hidden = self.model.contentType != ProductContentTypeBouqets || HELPER.forceTouchAvailable;
    NSInteger index = self.skuList.selectedButtonIndex;
    if ([self.model.skus count] > 0 && index < [self.model.skus count])
    {
        NITCatalogItemSKUViewModel * skuModel = self.model.skus[index];
        NSString * imgaeName = skuModel.favorite ? @"pink_heart" : @"green_heart";
        [self.likeBtn setImage:[UIImage imageNamed:imgaeName] forState:UIControlStateNormal];
    }
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [self.model.skus count];
}

- (nullable UIView *)selectionList:(HTHorizontalSelectionList *)selectionList viewForItemWithIndex:(NSInteger)index
{
    NITProductDetailsSKUView * view = [NITProductDetailsSKUView viewByModel:self.model.skus[index]
                                                                 tableWidth:ViewWidth(self.skuList)
                                                                   andState:self.skuList.selectedButtonIndex == index];
    return view;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    [self.skuList reloadData];
    [self updateFavoriteState];
}

@end
