//
//  NITProductDetailsPreview.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/8/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsProtocols.h"

@interface NITProductDetailsPreview : UIView

+ (instancetype)viewWithModel:(id)model;

@end
