//
//  NITProductRecommendations.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 1/20/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductRecommendations.h"
#import "NITRecommendationView.h"

@interface NITProductRecommendations ()
<
HTHorizontalSelectionListDelegate,
HTHorizontalSelectionListDataSource
>

@property (nonatomic) IBOutlet UIView * view;
@property (nonatomic) IBOutlet HTHorizontalSelectionList * recommendationsList;

@end

@implementation NITProductRecommendations

#pragma mark - Lifecycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self prepareLayout];
        [self initializeSubviews];
        [self initUI];
    }
    
    return self;
}

- (void)prepareLayout
{
    self.frame = CGRectMake(ViewX(self), ViewY(self), ViewWidth(WINDOW), ViewHeight(self));
}

- (void)initializeSubviews
{
    [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    
    self.view.frame = self.bounds;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self addSubview:self.view];
}

#pragma mark - Custom Accessors
- (void)setItems:(NSArray *)items
{
    _items = items;
    [self.recommendationsList reloadData];
}

#pragma mark - Private
- (void)initUI
{
    self.recommendationsList.delegate = self;
    self.recommendationsList.dataSource = self;
    self.recommendationsList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.recommendationsList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.recommendationsList.bottomTrimHidden = true;
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [self.items count];
}

- (nullable UIView *)selectionList:(HTHorizontalSelectionList *)selectionList viewForItemWithIndex:(NSInteger)index
{
    NITRecommendationView * view = [NITRecommendationView viewByModel:self.items[index]
                                                           tableWidth:ViewWidth(self.recommendationsList)
                                                             andIndex:index];
    return view;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    [self.presenter showDetailsByModel:self.items[index]];
}

@end
