//
//  NITProductDetailsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITProductDetailsProtocols.h"
#import "NITBaseViewController.h"
#import "NITProductDetailsPreview.h"

@interface NITProductDetailsViewController : NITBaseViewController <NITProductDetailsViewProtocol>

@property (nonatomic, strong) id <NITProductDetailsPresenterProtocol> presenter;
@property (nonatomic) NITProductDetailsPreview * preview;

- (void)hidePreview;

@end
