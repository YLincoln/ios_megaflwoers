//
//  NITProductDetailsNavigationController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/8/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsNavigationController.h"

@interface NITProductDetailsNavigationController () <UIPreviewActionItem>

@end

@implementation NITProductDetailsNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

#pragma mark - UIPreviewActionItem
- (NSArray<id<UIPreviewActionItem>> *)previewActionItems
{
    return [self.presenter previewActionItems];
}

@end
