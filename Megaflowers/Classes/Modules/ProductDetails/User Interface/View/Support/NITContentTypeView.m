//
//  NITContentTypeView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITContentTypeView.h"

@interface NITContentTypeView ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) BOOL selected;

@property (nonatomic) NSArray <NITProductSectionType *> * dataSource;

@end

@implementation NITContentTypeView

+ (instancetype)viewTitle:(NSString *)title dataSource:(NSArray <NITProductSectionType *> *)dataSource andState:(BOOL)selected
{
    NITContentTypeView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITContentTypeView) owner:self options:nil] firstObject];
    view.selected = selected;
    view.title.text = title;
    view.dataSource = dataSource;
    view.frame = [view calculateFrame];
    
    return view;
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self updateUI];
}

#pragma mark - Private
- (void)updateUI
{
    [self.title setFont:[UIFont systemFontOfSize:16 weight:self.selected ? UIFontWeightMedium : UIFontWeightLight]];
    [self.title setAlpha:self.selected ? 1.0 : 0.6];
}

- (CGRect)calculateFrame
{
    UILabel * lbl = [self copyLabel:self.title];
    
    CGFloat total = 0;
    for (NITProductSectionType * item in self.dataSource)
    {
        lbl.text = item.title;
        total += [lbl sizeOfMultiLineLabel].width + 10;
    }
    
    CGFloat koef = (ViewWidth(WINDOW) - total - 20) / [self.dataSource count];
    
    return CGRectMake(0, 0, [self.title sizeOfMultiLineLabel].width + koef, 44);
}

- (UILabel *)copyLabel:(UILabel *)label
{
    NSData * archivedData = [NSKeyedArchiver archivedDataWithRootObject:label];
    UILabel * copy = [NSKeyedUnarchiver unarchiveObjectWithData:archivedData];
    return copy;
}

@end
