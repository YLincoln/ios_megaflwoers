//
//  NITRecommendationView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRecommendationView.h"
#import "NITCatalogItemViewModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITRecommendationView ()

@property (nonatomic) IBOutlet UIImageView * imageView;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * price;
@property (nonatomic) IBOutlet UILabel * discontPrice;
@property (nonatomic) IBOutlet UIView * line;
@property (nonatomic) IBOutlet NSLayoutConstraint * priceW;
@property (nonatomic) IBOutlet NSLayoutConstraint * discontPriceL;

@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic) NSInteger index;

@end

@implementation NITRecommendationView

+ (instancetype)viewByModel:(NITCatalogItemViewModel *)model tableWidth:(CGFloat)tableWidth andIndex:(NSInteger)index
{
    NITRecommendationView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITRecommendationView) owner:self options:nil] firstObject];
    view.index = index;
    view.model = model;
    
    return view;
}

- (void)setModel:(NITCatalogItemViewModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.line.hidden = true; //self.index == 0;
    
    if ([self.model.price isEqualToNumber:self.model.discountPrice])
    {
        self.priceW.constant = 0;
        self.discontPriceL.constant = 0;
        self.discontPrice.textAlignment = NSTextAlignmentCenter;
    }
    else
    {
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:[self.model.price stringValue]];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                value:@2
                                range:NSMakeRange(0, [attributeString length])];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setAlignment:NSTextAlignmentRight];
        [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributeString length])];
        self.price.attributedText = attributeString;
        self.priceW.constant = [self.price sizeOfMultiLineLabel].width * 1.5;
        self.discontPriceL.constant = 15;
        self.discontPrice.textAlignment = NSTextAlignmentLeft;
    }

    
    self.discontPrice.text = [NSString stringWithFormat:@"%@ %@", self.model.discountPrice, NSLocalizedString(@"руб.", nil)];
    
    self.imageView.image = nil;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.model.previewPath]];
}

@end
