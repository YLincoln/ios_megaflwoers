//
//  NITAttachView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAttachView.h"
#import "NITCatalogItemViewModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITAttachView ()

@property (nonatomic) IBOutlet UIImageView * imageView;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * price;
@property (nonatomic) IBOutlet UIView * line;

@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic) NSInteger index;

@end

@implementation NITAttachView


+ (instancetype)viewByModel:(NITCatalogItemViewModel *)model andIndex:(NSUInteger)index
{
    NITAttachView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITAttachView) owner:self options:nil] firstObject];
    view.index = index;
    view.model = model;
    
    return view;
}

- (void)setModel:(NITCatalogItemViewModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text  = self.model.title;
    self.price.text  = [NSString stringWithFormat:@"%@ %@", self.model.discountPrice, NSLocalizedString(@"руб.", nil)];
    self.line.hidden = true; //self.index == 0;
    
    self.imageView.image = nil;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.model.previewPath]];
}


@end
