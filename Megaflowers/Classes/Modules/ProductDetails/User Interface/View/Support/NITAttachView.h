//
//  NITAttachView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITCatalogItemViewModel;

@interface NITAttachView : UIView

+ (instancetype)viewByModel:(NITCatalogItemViewModel *)model andIndex:(NSUInteger)index;

@end
