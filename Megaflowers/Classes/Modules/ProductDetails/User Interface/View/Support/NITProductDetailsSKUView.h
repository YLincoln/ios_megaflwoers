//
//  NITProductDetailsSKUView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/2/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITCatalogItemSKUViewModel;

@interface NITProductDetailsSKUView : UIView

@property (nonatomic) NITCatalogItemSKUViewModel * model;

+ (instancetype)viewByModel:(NITCatalogItemSKUViewModel *)model tableWidth:(CGFloat)tableWidth andState:(BOOL)selected;

@end
