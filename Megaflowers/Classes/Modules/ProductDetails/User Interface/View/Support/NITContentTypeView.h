//
//  NITContentTypeView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductSectionType.h"

@interface NITContentTypeView : UIView

+ (instancetype)viewTitle:(NSString *)title
               dataSource:(NSArray <NITProductSectionType *> *)dataSource
                 andState:(BOOL)selected;

@end
