//
//  NITProductDetailsSKUView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/2/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsSKUView.h"
#import "NITCatalogItemSKUViewModel.h"

@interface NITProductDetailsSKUView ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * sizeLbl;
@property (nonatomic) IBOutlet NSLayoutConstraint * titleW;

@property (nonatomic) BOOL selected;

@end

@implementation NITProductDetailsSKUView

+ (instancetype)viewByModel:(NITCatalogItemSKUViewModel *)model tableWidth:(CGFloat)tableWidth andState:(BOOL)selected
{
    NITProductDetailsSKUView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITProductDetailsSKUView) owner:self options:nil] firstObject];
    view.frame = CGRectMake(ViewX(view), ViewY(view), tableWidth/3 - 18, ViewHeight(view));
    view.selected = selected;
    view.model = model;
    
    return view;
}

- (void)setModel:(NITCatalogItemSKUViewModel *)model
{
    _model = model;
    [self updateData];
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self updateUI];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.titleW.constant = MIN(ViewWidth(self), [self.title sizeOfMultiLineLabel].width);
    
    if (self.model.width && self.model.height)
    {
        self.sizeLbl.text = [NSString stringWithFormat:@"%@-%@ %@", self.model.width, self.model.height, NSLocalizedString(@"см", nil)];
    }
    else if (self.model.width && !self.model.height)
    {
       self.sizeLbl.text = [NSString stringWithFormat:@"%@ %@", self.model.width, NSLocalizedString(@"см", nil)];
    }
    else if (!self.model.width && self.model.height)
    {
        self.sizeLbl.text = [NSString stringWithFormat:@"%@ %@", self.model.height, NSLocalizedString(@"см", nil)];
    }
    else
    {
        self.sizeLbl.text = @"";
    }
}

- (void)updateUI
{
    [self.title setFont:[UIFont systemFontOfSize:16 weight:self.selected ? UIFontWeightBold : UIFontWeightRegular]];
    [self.title setAlpha:self.selected ? 1.0 : 0.6];
    [self.sizeLbl setAlpha:self.selected ? 1.0 : 0.6];
}


@end
