//
//  NITRecommendationView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITCatalogItemViewModel;

@interface NITRecommendationView : UIView

+ (instancetype)viewByModel:(NITCatalogItemViewModel *)model tableWidth:(CGFloat)tableWidth andIndex:(NSInteger)index;

@end
