//
//  NITProductDetailsViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsViewController.h"
#import "NITProductSectionTypes.h"
#import "NITProductSections.h"
#import "NITProductAttachments.h"
#import "NITProductRecommendations.h"
#import "NITBouquetInfoViewController.h"
#import "NITProductDetailsImageCell.h"
#import "UIImageView+AFNetworking.h"
#import "NITProductDetailsSKUView.h"
#import "NITContentTypeView.h"
#import "NITAttachView.h"
#import "NITRecommendationView.h"
#import "JVYoutubePlayerView.h"

static NSInteger duration = .06;

@interface NITProductDetailsViewController ()
<
NITProductSectionTypesDelegate,
NITProductSectionsDelegate,
NITBouquetInfoViewControllerDelegate
>

@property (nonatomic) IBOutlet NITProductSectionTypes * sectionTypesView;
@property (nonatomic) IBOutlet NITProductSections * sectionsView;
@property (nonatomic) IBOutlet NITProductAttachments * attachmentsView;
@property (nonatomic) IBOutlet NITProductRecommendations * recommendationsView;
@property (nonatomic) IBOutlet UIButton * buyBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * buyBtnBottom;
@property (nonatomic) IBOutlet NSLayoutConstraint * sectionTypesH;
@property (nonatomic) IBOutlet NSLayoutConstraint * contentSectionH;
@property (nonatomic) IBOutlet NSLayoutConstraint * attachmentsSectionH;
@property (nonatomic) IBOutlet NSLayoutConstraint * recommendationsSectionH;

@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic) NSInteger skuIndex;

@end

@implementation NITProductDetailsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)callAction:(id)sender
{
    [self.presenter callAction];
}
- (IBAction)buyAction:(id)sender
{
    [self.presenter buyBouquet:self.model withSKUIndex:self.skuIndex];
    [self backAction:nil];
}

#pragma mark - Public
- (void)hidePreview
{
    if (self.preview)
    {
        [self.preview removeFromSuperview];
        self.preview = nil;
    }
}

#pragma mark - Private
- (void)initUI
{
    self.sectionTypesView.delegate = self;
    self.sectionsView.delegate = self;
    self.sectionsView.presenter = self.presenter;
    self.attachmentsView.presenter = self.presenter;
    self.recommendationsView.presenter = self.presenter;
    
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    switch (self.sectionsView.sectionType)
    {
        case SectionTypeFoto:   self.contentSectionH.constant = ViewWidth(self.view) * (IS_IPHONE_4 ? 0.66 : 1) + 52; break;
        case SectionTypeVideo:  self.contentSectionH.constant = ViewWidth(self.view) * 0.66 + 29;   break;
        case SectionType3D:     self.contentSectionH.constant = ViewWidth(self.view) / 1.7;         break;
        case SectionTypeMaster: self.contentSectionH.constant = ViewWidth(self.view) * 0.66;        break;
    }
    
    if (self.presenter.selectType == ProductSelectTypeEdite)
    {
        BOOL edited = [self.presenter isEditedBusketBouquet];
        self.buyBtnBottom.constant = edited ? 0 : - 57;
        self.buyBtn.hidden = !edited;
    }
    
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - NITProductDetailsPresenterProtocol
- (void)reloadDataByModel:(NITCatalogItemViewModel *)model
{
    self.model = model;
    
    [self.sectionsView setModel:self.model skuIndex:self.skuIndex];
    [self updateContentHeight];
}

- (void)reloadViewTypesData:(NSArray *)data
{
    self.sectionTypesH.constant = data.count > 1 ? 44 : 0;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
    
    self.sectionTypesView.hidden = self.sectionTypesH.constant == 0;
    self.sectionTypesView.items = data;
    self.sectionsView.sectionsCount = data.count;
}

- (void)reloadAttachData:(NSArray *)data
{
    self.attachmentsSectionH.constant = data.count > 0 ? 230 : 0;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
    
    self.attachmentsView.hidden = self.attachmentsSectionH.constant == 0;
    self.attachmentsView.items = data;
}

- (void)reloadRecommendationsData:(NSArray *)data
{
    self.recommendationsSectionH.constant = data.count > 0 ? 230 : 0;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];

    self.recommendationsView.hidden = self.recommendationsSectionH.constant == 0;
    self.recommendationsView.items = data;
}

- (void)reloadPriceText:(NSAttributedString *)text
{
    [self.buyBtn setAttributedTitle:text forState:UIControlStateNormal];
}

#pragma mark - NITBouquetInfoViewControllerDelegate
- (void)didHideDetailsWithModel:(NITCatalogItemViewModel *)model
{
    [self.presenter updateDataByModel:model];
}

#pragma mark - NITProductSectionTypesDelegate
- (void)didChangeSectionType:(SectionType)type
{
    self.sectionsView.sectionType = type;
    [self updateContentHeight];
}

#pragma mark - NITProductSectionsDelegate
- (void)didChangeSkuIndex:(NSInteger)index
{
    self.skuIndex = index;
}

@end
