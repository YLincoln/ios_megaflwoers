//
//  NITProductDetailsNavigationController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/8/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsProtocols.h"

@interface NITProductDetailsNavigationController : UINavigationController

@property (nonatomic, strong) id <NITProductDetailsPresenterProtocol> presenter;

@end
