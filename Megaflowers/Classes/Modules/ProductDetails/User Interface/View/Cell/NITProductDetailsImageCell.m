//
//  NITProductDetailsImageCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsImageCell.h"
#import "UIImageView+AFNetworking.h"

@interface NITProductDetailsImageCell ()

@property (nonatomic) IBOutlet UIImageView * imageView;

@end

@implementation NITProductDetailsImageCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setImagePath:(NSString *)imagePath
{
    _imagePath = imagePath;
    
    self.imageView.image = nil;
    [self.imageView setImageWithURL:[NSURL URLWithString:imagePath]];
}

@end
