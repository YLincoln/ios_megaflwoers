//
//  NITProductDetailsImageCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITProductDetailsImageCell : UICollectionViewCell

@property (nonatomic) NSString * imagePath;

@end
