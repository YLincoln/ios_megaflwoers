//
//  NITProductDetailsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProductDetailsProtocols.h"

@interface NITProductDetailsInteractor : NSObject <NITProductDetailsInteractorInputProtocol>

@property (nonatomic, weak) id <NITProductDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITProductDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITProductDetailsLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NITCatalogItemViewModel * data;
@property (nonatomic) NITCatalogItemViewModel * currentData;
@property (nonatomic) NITCatalogItemViewModel * presentData;


@end
