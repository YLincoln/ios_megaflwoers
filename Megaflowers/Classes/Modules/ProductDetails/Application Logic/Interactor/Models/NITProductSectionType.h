//
//  NITProductSectionType.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/10/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, SectionType) {
    SectionTypeFoto    = 0,
    SectionTypeVideo   = 1,
    SectionType3D      = 2,
    SectionTypeMaster  = 3
};

@interface NITProductSectionType : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) SectionType type;

- (instancetype)initWithType:(SectionType)type;

@end
