//
//  NITProductSectionType.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/10/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductSectionType.h"

@implementation NITProductSectionType

- (instancetype)initWithType:(SectionType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

- (NSString *)title
{
    switch (self.type) {
        case SectionTypeFoto:   return NSLocalizedString(@"Фото", nil);
        case SectionTypeVideo:  return NSLocalizedString(@"Видео", nil);
        case SectionType3D:     return NSLocalizedString(@"360", nil);
        case SectionTypeMaster: return NSLocalizedString(@"Мастер - класс", nil);
    }
}

@end
