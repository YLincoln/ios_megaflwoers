//
//  NITProductDetailsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsInteractor.h"
#import "NSArray+BlocksKit.h"

#import "NITCatalogItemComponentViewModel.h"

@implementation NITProductDetailsInteractor

- (void)updateDataBySelectType:(ProductSelectType)selectType
{
    switch (self.data.contentType) {
        case ProductContentTypeBouqets: {
            weaken(self);
            [self.APIDataManager getBouquetsByID:self.data.identifier withHandler:^(SWGBouquet *output, NSError * error) {
                
                if (output)
                {
                    NSNumber * favoriteSkuID = weakSelf.data.favoriteSkuID;
                    weakSelf.data = [weakSelf modelFromEntity:output];
                    weakSelf.data.favoriteSkuID = favoriteSkuID;
                    
                    if (selectType == ProductSelectTypeEdite)
                    {
                        weakSelf.data.skus = [weakSelf.data.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj) {
                            NITCatalogItemSKUViewModel * sku = weakSelf.currentData.skus.firstObject;
                            return [obj.identifier isEqualToNumber:sku.identifier];
                        }];
                    }
                    else
                    {
                        weakSelf.currentData = [weakSelf.data copy];
                    }
                    
                    [weakSelf.presenter updateModel:weakSelf.currentData];
                }
                else if (error.code != kCFURLErrorNotConnectedToInternet)
                {
                    [weakSelf.presenter showOpenProductErrorForModel:weakSelf.data];
                }
            }];
        }
            break;
            
        case ProductContentTypeAttach: {
            weaken(self);
            [self.APIDataManager getAttachByID:self.data.identifier withHandler:^(SWGAttach *output) {
                
                if (output)
                {
                    weakSelf.data = [weakSelf attachModelsFromEntities:@[output]].firstObject;
                    
                    if (selectType != ProductSelectTypeEdite)
                    {
                        weakSelf.currentData = [weakSelf.data copy];
                    }
                    
                    [weakSelf.presenter updateModel:weakSelf.currentData];
                }
                else
                {
                    [weakSelf.presenter showOpenProductErrorForModel:weakSelf.data];
                }
            }];
        }
            break;
    }
}

- (void)updateBasketDataWithHandler:(void (^)(NITCatalogItemViewModel * model))handler
{
    switch (self.data.contentType) {
        case ProductContentTypeBouqets: {
            weaken(self);
            [self.APIDataManager getBouquetsByID:self.data.identifier withHandler:^(SWGBouquet *output, NSError * error) {
                
                if (output)
                {
                    NSNumber * favoriteSkuID = weakSelf.data.favoriteSkuID;
                    weakSelf.data = [weakSelf modelFromEntity:output];
                    weakSelf.data.favoriteSkuID = favoriteSkuID;
                    weakSelf.currentData = [weakSelf.data copy];
                }
                
                handler(weakSelf.currentData);
                
            }];
        }
            break;
            
        case ProductContentTypeAttach: {
            weaken(self);
            [self.APIDataManager getAttachByID:self.data.identifier withHandler:^(SWGAttach *output) {
                
                if (output)
                {
                    weakSelf.data = [weakSelf attachModelsFromEntities:@[output]].firstObject;
                    weakSelf.currentData = [weakSelf.data copy];
                }
                
                handler(weakSelf.currentData);
            }];
        }
            break;
    }
}

- (void)updateAttachDataWithHandler:(void (^)(NSArray <NITCatalogItemViewModel *> * models))handler
{
    [self.APIDataManager getAttachByBouquetID:self.data.identifier withHandler:^(NSArray<SWGAttach> *output) {
        handler([self attachModelsFromEntities:output]);
    }];
}

- (void)updateRecommendationsDataWithHandler:(void (^)(NSArray <NITCatalogItemViewModel *> * models))handler
{
    [self.APIDataManager getRecommendationsWithHandler:^(NSArray<SWGBouquet> *output) {
        handler([self modelsFromEntities:output]);
    }];
}

- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model andSku:(NITCatalogItemSKUViewModel *)sku
{
    [self.localDataManager changeFavoriteForBouquet:model andSku:sku];
    [self.APIDataManager changeFavoriteState:!sku.favorite forBouquetID:sku.identifier];
}

- (void)saveProductModel:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex
{
    if (model && [model.skus count] > 0)
    {
        NITBasketProductModel * buc = [[NITBasketProductModel alloc] initWidthModel:model skuIndex:skuIndex];
        [self.localDataManager saveProductModel:buc];
    }
}

- (void)saveEditedProductModel:(NITCatalogItemViewModel *)model skuInex:(NSInteger)skuIndex
{
    if (model && [model.skus count] > 0)
    {
        NITBasketProductModel * buc = [[NITBasketProductModel alloc] initWidthModel:model skuIndex:skuIndex];
        [self.localDataManager saveEditedProductModel:buc];
    }
}

#pragma mark - Private
- (NITCatalogItemViewModel *)modelFromEntity:(SWGBouquet *)entity
{
    return [[NITCatalogItemViewModel alloc] initWithBouquet:entity];
}

- (NSArray <NITCatalogItemViewModel *> *)modelsFromEntities:(NSArray <SWGBouquet *> *)entities
{
    return [entities bk_map:^id(SWGBouquet * obj) {
        return [[NITCatalogItemViewModel alloc] initWithBouquet:obj];
    }];
}

- (NSArray <NITCatalogItemViewModel *> *)attachModelsFromEntities:(NSArray <SWGAttach *> *)entities
{
    return [entities bk_map:^id(SWGAttach * obj) {
        return [[NITCatalogItemViewModel alloc] initWithAttach:obj];
    }];
}

@end
