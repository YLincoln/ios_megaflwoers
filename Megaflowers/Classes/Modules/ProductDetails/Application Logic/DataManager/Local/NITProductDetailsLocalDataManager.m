//
//  NITProductDetailsLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsLocalDataManager.h"

@implementation NITProductDetailsLocalDataManager

- (void)changeFavoriteForBouquet:(NITCatalogItemViewModel *)bouquet andSku:(NITCatalogItemSKUViewModel *)sku
{
    BOOL favorite = !sku.favorite;
    
    if (favorite)
    {
        [NITFavoriteBouquet createFavoriteBouquetByModel:bouquet andSkuModel:sku];
        [TRACKER trackEvent:TrackerEventLikeBouquet parameters:@{@"id":sku.identifier}];
    }
    else
    {
        [NITFavoriteBouquet deleteBouquetBySkuID:sku.identifier];
    }
}

- (void)saveProductModel:(NITBasketProductModel *)model
{
    if (model)
    {
        switch (model.contentType) {
            case ProductContentTypeBouqets:
                [NITBusketBouquet addBouquet:model];
                break;
                
            case ProductContentTypeAttach:
                [NITBusketAttachment addAttachment:model];
                break;
        }
    }
}

- (void)saveEditedProductModel:(NITBasketProductModel *)model
{
    if (model)
    {
        switch (model.contentType) {
            case ProductContentTypeBouqets:
                [NITBusketBouquet updateBouquet:model];
                break;
                
            case ProductContentTypeAttach:
                [NITBusketAttachment updateAttachment:model];
                break;
        }
    }
}

@end
