//
//  NITProductDetailsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsAPIDataManager.h"
#import "SWGBouquetApi.h"
#import "SWGUserApi.h"
#import "SWGAttachApi.h"
#import "SWGBasketApi.h"
#import "NITCatalogItemComponentViewModel.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITProductDetailsAPIDataManager

- (void)getBouquetsByID:(NSNumber *)bouquetID withHandler:(void(^)(SWGBouquet * output, NSError * error))handler
{
    [HELPER startLoading];
    [[[SWGBouquetApi alloc] initWithApiClient:[SWGApiClient sharedClient]] bouquetIdGetWithId:bouquetID
                                                                                         lang:HELPER.lang
                                                                                         site:API.site
                                                                                         city:USER.cityID
                                                                            completionHandler:^(SWGBouquet *output, NSError *error) {
                                                                                [HELPER stopLoading];
                                                                                if (error)
                                                                                {
                                                                                    [HELPER logError:error method:METHOD_NAME];
                                                                                }
                                                                                
                                                                                handler(output, error);
                                                                            }];
}

- (void)getAttachByID:(NSNumber *)attachID withHandler:(void(^)(SWGAttach * output))handler
{
    [HELPER startLoading];
    [[[SWGAttachApi alloc] initWithApiClient:[SWGApiClient sharedClient]] attachIdGetWithId:attachID lang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(SWGAttach *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        
        handler(output);
    }];
}

- (void)getAttachByBouquetID:(NSNumber *)bouquetID withHandler:(void(^)(NSArray<SWGAttach *> *output))handler
{
    [HELPER startLoading];
    [[[SWGBouquetApi alloc] initWithApiClient:[SWGApiClient sharedClient]] bouquetIdAdditionsGetWithId:bouquetID
                                                                                                  lang:HELPER.lang
                                                                                                  site:API.site
                                                                                                  city:USER.cityID
                                                                                     completionHandler:^(NSArray<SWGAttach> *output, NSError *error) {
                                                                                         [HELPER stopLoading];
                                                                                         if (error)
                                                                                         {
                                                                                             [HELPER logError:error method:METHOD_NAME];
                                                                                         }
                                                                                         
                                                                                         handler(output);
                                                                                     }];
}

- (void)getRecommendationsWithHandler:(void(^)(NSArray<SWGBouquet *> *output))handler
{
    [HELPER startLoading];
    [[[SWGBouquetApi alloc] initWithApiClient:[SWGApiClient sharedClient]] bouquetRecommendationsGetWithLang:HELPER.lang
                                                                                                        site:API.site
                                                                                                        city:USER.cityID
                                                                                           completionHandler:^(NSArray<SWGBouquet> *output, NSError *error) {
                                                                                               [HELPER stopLoading];
                                                                                               if (error)
                                                                                               {
                                                                                                   [HELPER logError:error method:METHOD_NAME];
                                                                                               }
                                                                                               handler(output);
                                                                                           }];
}

- (void)changeFavoriteState:(BOOL)state forBouquetID:(NSNumber *)bouquetID
{
    if (USER.isAutorize)
    {
        if (!state)
        {
            [HELPER startLoading];
            [[SWGUserApi new] userBouquetPostWithUser:@([USER.identifier integerValue]) bouquet:bouquetID completionHandler:^(NSError *error) {
                [HELPER stopLoading];
                if (error)
                {
                    [HELPER logError:error method:METHOD_NAME];
                }
            }];
        }
        else
        {
            [HELPER startLoading];
            [[SWGUserApi new]  userBouquetIdDeleteWithId:bouquetID completionHandler:^(NSError *error) {
                [HELPER stopLoading];
                if (error)
                {
                    [HELPER logError:error method:METHOD_NAME];
                }
            }];
        }
    }
}

@end
