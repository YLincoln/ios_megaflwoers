//
//  NITProductDetailsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "SWGBouquet.h"
#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"
#import "SWGAttach.h"
#import "NITBasketProductModel.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"
#import "NITFavoriteBouquet.h"
#import "NITCatalogDirectoryProtocols.h"
#import "NITProductSectionType.h"
#import "SWGBasket.h"

static NSInteger const kPreviewTag = 222222;

typedef CF_ENUM (NSUInteger, ProductType) {
    ProductTypeBouquet  = 0,
    ProductTypeAttach   = 1
};

@protocol NITProductDetailsInteractorOutputProtocol;
@protocol NITProductDetailsInteractorInputProtocol;
@protocol NITProductDetailsViewProtocol;
@protocol NITProductDetailsPresenterProtocol;
@protocol NITProductDetailsLocalDataManagerInputProtocol;
@protocol NITProductDetailsAPIDataManagerInputProtocol;

@class NITProductDetailsWireFrame;

@protocol NITProductDetailsViewProtocol
@required
@property (nonatomic, strong) id <NITProductDetailsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITCatalogItemViewModel *)model;
@optional
- (void)reloadViewTypesData:(NSArray *)data;
- (void)reloadAttachData:(NSArray *)data;
- (void)reloadRecommendationsData:(NSArray *)data;
- (void)reloadPriceText:(NSAttributedString *)text;
@end

@protocol NITProductDetailsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model selectType:(ProductSelectType)selectType from:(id)fromView;
+ (void)presentNITProductDetailsModuleFrom:(id)fromView bouquet:(NITCatalogItemViewModel *)bouquet selectType:(ProductSelectType)selectType;
+ (void)presentNITProductDetailsPreviewModuleWithBouquet:(NITCatalogItemViewModel *)bouquet isViewMode:(BOOL)isViewMode;
- (void)showDetailsByModel:(NITCatalogItemViewModel *)model from:(id)fromView;
- (void)showImages:(NSArray <NSString *> *)imagesPaths from:(id)fromView;
- (void)showComponentsForBouquet:(NITCatalogItemViewModel *)bouquet andSKUIndex:(NSInteger)index from:(id)fromView;
- (void)showCallOrderFrom:(id)fromView;
- (void)showRegisterFrom:(id)fromView;
- (void)backToRootFrom:(id)fromView;
- (void)backFrom:(id)fromView;
@end

@protocol NITProductDetailsPresenterProtocol
@required
@property (nonatomic, weak) id <NITProductDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITProductDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITProductDetailsWireFrameProtocol> wireFrame;
@property (nonatomic) ProductSelectType selectType;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updateData;
- (void)updateDataByModel:(NITCatalogItemViewModel *)model;
- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model andSku:(NITCatalogItemSKUViewModel *)sku;
- (void)updatePriceValue:(NSNumber *)value;
- (BOOL)isEditedSKUByIndex:(NSInteger)index;
- (BOOL)isEditedBusketBouquet;
- (void)callAction;
- (void)showDetailsByModel:(NITCatalogItemViewModel *)model;
- (void)showImages:(NSArray <NSString *> *)imagesPaths;
- (void)showComponentsForBouquet:(NITCatalogItemViewModel *)bouquet andSKUIndex:(NSInteger)index;
- (void)buyBouquet:(NITCatalogItemViewModel *)bouquet withSKUIndex:(NSInteger)index;
- (void)downloadImageByPath:(NSString *)path withHandler:(void(^)(UIImage * image))handler;
- (NSArray<id<UIPreviewActionItem>> *)previewActionItems;
@end

@protocol NITProductDetailsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateModel:(NITCatalogItemViewModel *)model;
- (void)showOpenProductErrorForModel:(NITCatalogItemViewModel *)model;;
@end

@protocol NITProductDetailsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITProductDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITProductDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITProductDetailsLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NITCatalogItemViewModel * data;
@property (nonatomic) NITCatalogItemViewModel * currentData;
@property (nonatomic) NITCatalogItemViewModel * presentData;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)updateDataBySelectType:(ProductSelectType)selectType;
- (void)updateAttachDataWithHandler:(void (^)(NSArray <NITCatalogItemViewModel *> * models))handler;
- (void)updateRecommendationsDataWithHandler:(void (^)(NSArray <NITCatalogItemViewModel *> * models))handler;
- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model andSku:(NITCatalogItemSKUViewModel *)sku;
- (void)saveProductModel:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex;
- (void)saveEditedProductModel:(NITCatalogItemViewModel *)model skuInex:(NSInteger)skuIndex;
@end


@protocol NITProductDetailsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITProductDetailsAPIDataManagerInputProtocol <NITProductDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getBouquetsByID:(NSNumber *)bouquetID withHandler:(void(^)(SWGBouquet * output, NSError * error))handler;
- (void)getAttachByID:(NSNumber *)attachID withHandler:(void(^)(SWGAttach * output))handler;
- (void)getAttachByBouquetID:(NSNumber *)bouquetID withHandler:(void(^)(NSArray<SWGAttach *> *output))handler;
- (void)getRecommendationsWithHandler:(void(^)(NSArray<SWGBouquet *> *output))handler;
- (void)changeFavoriteState:(BOOL)state forBouquetID:(NSNumber *)bouquetID;
@end

@protocol NITProductDetailsLocalDataManagerInputProtocol <NITProductDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)changeFavoriteForBouquet:(NITCatalogItemViewModel *)bouquet andSku:(NITCatalogItemSKUViewModel *)sku;
- (void)saveProductModel:(NITBasketProductModel *)model;
- (void)saveEditedProductModel:(NITBasketProductModel *)model;
@end
