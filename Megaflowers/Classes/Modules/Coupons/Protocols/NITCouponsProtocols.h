//
//  NITCouponsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponModel.h"
#import "SWGUserCoupon.h"
#import "NITCouponSection.h"

@protocol NITCouponsInteractorOutputProtocol;
@protocol NITCouponsInteractorInputProtocol;
@protocol NITCouponsViewProtocol;
@protocol NITCouponsPresenterProtocol;
@protocol NITCouponsLocalDataManagerInputProtocol;
@protocol NITCouponsAPIDataManagerInputProtocol;

@class NITCouponsWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITCouponsViewProtocol
@required
@property (nonatomic, strong) id <NITCouponsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadCouponsBySectionModels:(NSArray <NITCouponSection *> *)models;
- (void)reloadAddCouponSection;
@end

@protocol NITCouponsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCouponsModuleFrom:(id)fromView;
- (void)showScanCouponFrom:(id)fromView withHandler:(void(^)(NSString * code))handler;
- (void)backActionFrom:(id)fromView;
@end

@protocol NITCouponsPresenterProtocol
@required
@property (nonatomic, weak) id <NITCouponsViewProtocol> view;
@property (nonatomic, strong) id <NITCouponsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCouponsWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)scanQRCode;
- (void)addNewCoupon:(NSString *)text;
- (void)addCouponToBasket:(NITCouponModel *)coupon;
@end

@protocol NITCouponsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCouponModels:(NSArray <NITCouponModel *> *)models;
- (void)couponAddedSuccess;
- (void)couponAddError:(NSString *)error;
@end

@protocol NITCouponsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCouponsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCouponsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCouponsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getCoupons;
- (void)addNewCoupon:(NSString *)text;
- (void)addCouponToBasket:(NITCouponModel *)coupon;
@end


@protocol NITCouponsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCouponsAPIDataManagerInputProtocol <NITCouponsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserCouponsWithHandler:(void (^)(NSArray<SWGUserCoupon> *result))handler;
- (void)addNewCoupon:(NSString *)text withHandler:(void (^)(NSError *error))handler;
@end

@protocol NITCouponsLocalDataManagerInputProtocol <NITCouponsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)addCouponToBasket:(NITCouponModel *)coupon;
@end
