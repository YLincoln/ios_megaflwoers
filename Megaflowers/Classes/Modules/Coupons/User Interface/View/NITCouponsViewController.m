//
//  NITCouponsViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponsViewController.h"
#import "NITCouponCell.h"
#import "NITCouponAddCell.h"
#import "NITPersonalDiscontCell.h"

@interface NITCouponsViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITCouponCellDelegate,
NITCouponAddCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITCouponSection *> * sections;

@end

@implementation NITCouponsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter updateData];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Купоны", nil);
    
    [self setKeyboardActiv:true];
    [self.tableView registerNibArray:@[_s(NITCouponCell), _s(NITCouponAddCell), _s(NITPersonalDiscontCell)]];
    [self.tableView setTableFooterView:[UIView new]];
}

#pragma mark - NITCouponsPresenterProtocol
- (void)reloadCouponsBySectionModels:(NSArray<NITCouponSection *> *)models
{
    self.sections = models;
    [self.tableView reloadData];
}

- (void)reloadAddCouponSection
{
    NITCouponAddCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [cell resetState];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sections[section] models].count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.sections[indexPath.section] cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCouponSection * sectionModel = self.sections[indexPath.section];
    NITCouponBaseCell * cell = [tableView dequeueReusableCellWithIdentifier:sectionModel.cellId forIndexPath:indexPath];
    [cell setModel:sectionModel.models[indexPath.row] delegate:self];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCouponSection * sectionModel = self.sections[indexPath.section];
    [self.presenter addCouponToBasket:sectionModel.models[indexPath.row]];
}

#pragma mark - NITCouponAddCellDelegate
- (void)didAddCoupon:(NSString *)text
{
    [self.presenter addNewCoupon:text];
}

- (void)didOpenScanQRCode
{
    [self.presenter scanQRCode];
}

#pragma mark - NITCouponCellDelegate
- (void)didAddCouponToBasket:(NITCouponModel *)coupon
{
    [self.presenter addCouponToBasket:coupon];
    [self.tableView reloadData];
}

@end
