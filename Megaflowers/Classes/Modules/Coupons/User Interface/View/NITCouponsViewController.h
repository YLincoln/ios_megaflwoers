//
//  NITCouponsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITCouponsProtocols.h"

@interface NITCouponsViewController : NITBaseViewController <NITCouponsViewProtocol>

@property (nonatomic, strong) id <NITCouponsPresenterProtocol> presenter;

@end
