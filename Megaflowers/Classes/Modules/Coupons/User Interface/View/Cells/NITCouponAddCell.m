//
//  NITCouponAddCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponAddCell.h"

@interface NITCouponAddCell ()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * addCouponBtn;
@property (nonatomic) IBOutlet UIButton * scanQrBtn;
@property (nonatomic) IBOutlet UITextField * textField;

@end

@implementation NITCouponAddCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

#pragma mark - IBAction
- (IBAction)addCouponAction:(id)sender
{
    self.textField.hidden = false;
    self.addCouponBtn.hidden = true;
    self.title.hidden = true;
    
    [self.textField becomeFirstResponder];
}

- (IBAction)scanQrAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didOpenScanQRCode)])
    {
        [self.delegate didOpenScanQRCode];
    }
}

#pragma mark - Public
- (void)resetState
{
    self.textField.text = @"";
    self.textField.hidden = true;
    self.addCouponBtn.hidden = false;
    self.title.hidden = false;
}

- (void)setModel:(id)model delegate:(id)delegate
{
    self.delegate = delegate;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.text length] > 0)
    {
        if ([self.delegate respondsToSelector:@selector(didAddCoupon:)])
        {
            [self.delegate didAddCoupon:textField.text];
        }
    }
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.textField.text length] > 0)
    {
        if ([self.delegate respondsToSelector:@selector(didAddCoupon:)])
        {
            [self.delegate didAddCoupon:textField.text];
        }
    }
    else
    {
        [self resetState];
    }
}

@end
