//
//  NITCouponCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponBaseCell.h"

@class NITCouponModel;

@protocol NITCouponCellDelegate <NSObject>

- (void)didAddCouponToBasket:(NITCouponModel *)coupon;

@end

@interface NITCouponCell : NITCouponBaseCell

@property (nonatomic) NITCouponModel * model;
@property (nonatomic, weak) id <NITCouponCellDelegate> delegate;

@end
