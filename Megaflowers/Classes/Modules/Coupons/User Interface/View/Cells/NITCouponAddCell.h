//
//  NITCouponAddCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponBaseCell.h"

@protocol NITCouponAddCellDelegate <NSObject>

- (void)didOpenScanQRCode;
- (void)didAddCoupon:(NSString *)text;

@end

@interface NITCouponAddCell : NITCouponBaseCell

@property (nonatomic, weak) id <NITCouponAddCellDelegate> delegate;
- (void)resetState;

@end
