//
//  NITCouponCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponCell.h"
#import "NITCouponModel.h"

@interface NITCouponCell ()

@property (nonatomic) IBOutlet UILabel * date;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * detail;
@property (nonatomic) IBOutlet UIButton * basketBtn;

@end

@implementation NITCouponCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)setModel:(id)model delegate:(id)delegate
{
    self.model = model;
    self.delegate = delegate;
}

- (void)setModel:(NITCouponModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)addToBasketAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didAddCouponToBasket:)])
    {
        [self.delegate didAddCouponToBasket:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.date.text = self.model.dateEnd;
    self.title.text = self.model.title;
    self.detail.text = self.model.detail;
    self.basketBtn.enabled = !self.model.inBasket;
    
    NSString * imageName = self.model.inBasket ? @"ic_in_basket_active" : @"ic_in_basket";
    [self.basketBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}

@end
