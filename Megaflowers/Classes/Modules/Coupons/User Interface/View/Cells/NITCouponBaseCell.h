//
//  NITCouponBaseCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/10/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITCouponBaseCell : UITableViewCell

- (void)setModel:(id)model delegate:(id)delegate;

@end
