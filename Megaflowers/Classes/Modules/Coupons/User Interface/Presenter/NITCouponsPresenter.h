//
//  NITCouponsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCouponsProtocols.h"

@class NITCouponsWireFrame;

@interface NITCouponsPresenter : NITRootPresenter <NITCouponsPresenterProtocol, NITCouponsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCouponsViewProtocol> view;
@property (nonatomic, strong) id <NITCouponsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCouponsWireFrameProtocol> wireFrame;

@end
