//
//  NITCouponsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponsPresenter.h"
#import "NITCouponsWireframe.h"

@implementation NITCouponsPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - NITCouponsPresenterProtocol
- (void)updateData
{
    [self.interactor getCoupons];
}

- (void)addNewCoupon:(NSString *)text
{
    [self.interactor addNewCoupon:text];
}

- (void)addCouponToBasket:(NITCouponModel *)coupon
{
    if ([coupon isKindOfClass:[NITCouponModel class]])
    {
        [self.interactor addCouponToBasket:coupon];
        [self.wireFrame backActionFrom:self.view];
    }
}

- (void)scanQRCode
{
    weaken(self);
    [self.wireFrame showScanCouponFrom:self.view withHandler:^(NSString *code) {
        [weakSelf addNewCoupon:code];
    }];
}

#pragma mark - NITCouponsInteractorOutputProtocol
- (void)updateCouponModels:(NSArray<NITCouponModel *> *)models
{
    NSArray * sections = @[
                           [[NITCouponSection alloc] initWithType:CouponSectionTypeAdd models:@[@true]],
                           [[NITCouponSection alloc] initWithType:CouponSectionTypeView models:models],
                           [[NITCouponSection alloc] initWithType:CouponSectionTypeInfo models:nil /*USER.birthday ? nil : @[@true]*/]
                           ];
    
    [self.view reloadCouponsBySectionModels:sections];
}

- (void)couponAddedSuccess
{
    [self updateData];
    [self.view reloadAddCouponSection];
}

- (void)couponAddError:(NSString *)error
{
    if (error.length)
    {
        [self showError:error fromView:self.view];
    }
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveCouponNotification object:nil];
}

@end
