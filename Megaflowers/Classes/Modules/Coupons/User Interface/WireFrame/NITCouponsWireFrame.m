//
//  NITCouponsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponsWireFrame.h"
#import "NITBarcodeScannerWireFrame.h"

@implementation NITCouponsWireFrame

+ (id)createNITCouponsModule
{
    // Generating module components
    id <NITCouponsPresenterProtocol, NITCouponsInteractorOutputProtocol> presenter = [NITCouponsPresenter new];
    id <NITCouponsInteractorInputProtocol> interactor = [NITCouponsInteractor new];
    id <NITCouponsAPIDataManagerInputProtocol> APIDataManager = [NITCouponsAPIDataManager new];
    id <NITCouponsLocalDataManagerInputProtocol> localDataManager = [NITCouponsLocalDataManager new];
    id <NITCouponsWireFrameProtocol> wireFrame= [NITCouponsWireFrame new];
    id <NITCouponsViewProtocol> view = [(NITCouponsWireFrame *)wireFrame createViewControllerWithKey:_s(NITCouponsViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITCouponsModuleFrom:(UIViewController *)fromViewController
{
    NITCouponsViewController * view = [NITCouponsWireFrame createNITCouponsModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showScanCouponFrom:(UIViewController *)fromViewController withHandler:(void(^)(NSString * code))handler
{
    [NITBarcodeScannerWireFrame presentNITBarcodeScannerModuleFrom:fromViewController
                                                             title:NSLocalizedString(@"Наведите на купон.\nСканирование начнется автоматически!", nil)
                                                           handler:handler];
}

- (void)backActionFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
