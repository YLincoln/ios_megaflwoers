//
//  NITCouponsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponsProtocols.h"
#import "NITCouponsViewController.h"
#import "NITCouponsLocalDataManager.h"
#import "NITCouponsAPIDataManager.h"
#import "NITCouponsInteractor.h"
#import "NITCouponsPresenter.h"
#import "NITCouponsWireframe.h"
#import "NITRootWireframe.h"

@interface NITCouponsWireFrame : NITRootWireframe <NITCouponsWireFrameProtocol>

@end
