//
//  NITCouponsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponsInteractor.h"
#import "NSArray+BlocksKit.h"

@implementation NITCouponsInteractor
- (void)getCoupons
{
    weaken(self);
    [self.APIDataManager getUserCouponsWithHandler:^(NSArray<SWGUserCoupon> *result) {
        [weakSelf.presenter updateCouponModels:[NITCouponModel modelsFromEntities:result]];
    }];
}

- (void)addNewCoupon:(NSString *)text
{
    weaken(self);
    [self.APIDataManager addNewCoupon:text withHandler:^(NSError *error) {
        
        if (error)
        {
            [weakSelf.presenter couponAddError:error.interfaceDescription];
        }
        else
        {
            [weakSelf.presenter couponAddedSuccess];
        }
    }];
}

- (void)addCouponToBasket:(NITCouponModel *)coupon
{
    [self.localDataManager addCouponToBasket:coupon];
}

@end
