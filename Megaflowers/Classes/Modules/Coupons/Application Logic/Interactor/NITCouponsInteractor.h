//
//  NITCouponsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCouponsProtocols.h"

@interface NITCouponsInteractor : NSObject <NITCouponsInteractorInputProtocol>

@property (nonatomic, weak) id <NITCouponsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCouponsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCouponsLocalDataManagerInputProtocol> localDataManager;

@end
