//
//  NITCouponModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponModel.h"
#import "NITCoupon.h"
#import "SWGUserCoupon.h"
#import "SWGBasketCoupon.h"

@implementation NITCouponModel

- (instancetype)initWidthCoupon:(NITCoupon *)coupon
{
    if (self = [super init])
    {
        self.identifier = @(coupon.identifier);
        self.type = @(coupon.type);
        self.value = @(coupon.value);
        self.updatedAt = coupon.updatedAt;
        self.dateStart = coupon.dateStart;
        self.dateEnd = coupon.dateEnd;
        self.title = coupon.title;
        self.detail = [coupon.detail stringByStrippingHTML];
    }
    
    return self;
}

- (instancetype)initWidthUserCoupon:(SWGUserCoupon *)coupon
{
    if (self = [super init])
    {
        self.identifier = coupon._id;
        self.type = coupon.type;
        self.value = coupon.value;
        self.updatedAt = coupon.updatedAt;
        self.dateStart = coupon.dateStart;
        self.dateEnd = coupon.dateEnd;
        self.title = coupon.name;
        self.detail = [coupon._description stringByStrippingHTML];
        self.code = coupon.code;
    }
    
    return self;
}

- (instancetype)initWidthBasketCoupon:(SWGBasketCoupon *)coupon
{
    if (self = [super init])
    {
        self.identifier = coupon._id;
        self.type = @(coupon.type.integerValue);
        self.value = coupon.value;
        self.dateStart = coupon.dateStart;
        self.dateEnd = coupon.dateEnd;
        self.updatedAt = coupon.updatedAt;
        self.title = coupon.name;
        self.detail = [coupon._description stringByStrippingHTML];
        self.code = coupon.code;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (BOOL)inBasket
{
    return [NITCoupon currentCoupon].identifier == self.identifier.integerValue;
}

#pragma mark - Public
+ (NSArray <NITCouponModel *> *)modelsFromEntities:(NSArray <SWGUserCoupon *> *)entities
{
    return [entities bk_map:^id(SWGUserCoupon * obj) {
        return [[NITCouponModel alloc] initWidthUserCoupon:obj];
    }];
}

@end
