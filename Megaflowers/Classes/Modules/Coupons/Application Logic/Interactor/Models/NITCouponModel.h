//
//  NITCouponModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITCoupon, SWGUserCoupon, SWGBasketCoupon;

typedef CF_ENUM (NSUInteger, CouponType) {
    CouponTypePercent   = 0,
    CouponTypeSum       = 1,
};

@interface NITCouponModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSNumber * type;
@property (nonatomic) NSNumber * value;
@property (nonatomic) NSString * updatedAt;
@property (nonatomic) NSString * dateStart;
@property (nonatomic) NSString * dateEnd;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * detail;
@property (nonatomic) NSString * code;
@property (nonatomic) BOOL inBasket;

- (instancetype)initWidthCoupon:(NITCoupon *)coupon;
- (instancetype)initWidthUserCoupon:(SWGUserCoupon *)coupon;
- (instancetype)initWidthBasketCoupon:(SWGBasketCoupon *)coupon;
+ (NSArray <NITCouponModel *> *)modelsFromEntities:(NSArray <SWGUserCoupon *> *)entities;

@end
