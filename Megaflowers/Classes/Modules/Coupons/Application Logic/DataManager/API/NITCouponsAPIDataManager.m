//
//  NITCouponsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponsAPIDataManager.h"
#import "SWGUserApi.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITCouponsAPIDataManager

- (void)getUserCouponsWithHandler:(void (^)(NSArray<SWGUserCoupon> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userCouponGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserCoupon> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

- (void)addNewCoupon:(NSString *)text withHandler:(void (^)(NSError *error))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userCouponPostWithCode:text completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler(error);
    }];
}

@end
