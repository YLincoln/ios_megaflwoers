//
//  NITCouponsLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCouponsLocalDataManager.h"
#import "NITCoupon.h"

@implementation NITCouponsLocalDataManager

- (void)addCouponToBasket:(NITCouponModel *)coupon
{
    [NITCoupon addToBusketCoupon:coupon];
}

@end
