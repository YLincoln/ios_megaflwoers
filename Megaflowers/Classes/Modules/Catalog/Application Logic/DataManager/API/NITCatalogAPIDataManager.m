//
//  NITCatalogAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogAPIDataManager.h"
#import "SWGBouquetSectionApi.h"
#import "SWGBannerApi.h"
#import "SWGBouquetApi.h"
#import "SWGUserApi.h"
#import "SWGSearchApi.h"

@implementation NITCatalogAPIDataManager

#pragma mark - NITCatalogAPIDataManagerInputProtocol
- (void)getCategoriesWithHandler:(void (^)(NSArray<SWGBouquetSection*> *result))handler
{
    [HELPER startLoading];
    [[SWGBouquetSectionApi new] bouquetSectionGetWithLang:HELPER.lang
                                                     site:API.site
                                                     city:USER.cityID
                                                   parent:nil
                                        completionHandler:^(NSArray<SWGBouquetSection> *output, NSError *error) {
                                            [HELPER stopLoading];
                                            if (error) [HELPER logError:error method:METHOD_NAME];
                                            handler(output);
                                        }];
}

- (void)getStocksWithHandler:(void (^)(NSArray<SWGBanner*> *result))handler
{
    [HELPER startLoading];
    [[SWGBannerApi new] bannerGetWithLang:HELPER.lang
                                     site:API.site
                                     city:USER.cityID
                        completionHandler:^(NSArray<SWGBanner> *output, NSError *error) {
                            [HELPER stopLoading];
                            if (error) [HELPER logError:error method:METHOD_NAME];
                            handler(output);
                        }];
}

- (void)getPopularSearchesWithHandler:(void(^)(NSArray<SWGPopular *> * output))handler
{
    [HELPER startLoading];
    NSNumber * userId = USER.identifier.length > 0 ? @(USER.identifier.integerValue) : nil;
    [[SWGSearchApi new] searchPopularGetWithLang:HELPER.lang site:API.site city:USER.cityID userId:userId q:nil completionHandler:^(NSArray<SWGPopular> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        handler(output);
    }];
}

- (void)getUserPopularSearchesWithHandler:(void(^)(NSArray<SWGPopular *> * output))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userSearchPopularGetWithLang:HELPER.lang site:API.site city:USER.cityID q:nil completionHandler:^(NSArray<SWGPopular> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        handler(output);
    }];
}

- (void)searchCatalogItemsByString:(NSString *)searchString withHandler:(void(^)(SWGSearch * output))handler
{
    [HELPER startLoading];
    NSNumber * userId = USER.identifier.length > 0 ? @(USER.identifier.integerValue) : nil;
    [[SWGSearchApi new] searchGetWithLang:HELPER.lang site:API.site city:USER.cityID q:searchString userId:userId completionHandler:^(SWGSearch *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        handler(output);
    }];
}

@end
