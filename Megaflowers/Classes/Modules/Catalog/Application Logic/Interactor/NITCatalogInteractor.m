//
//  NITCatalogInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogInteractor.h"
#import "NSArray+BlocksKit.h"
#import "SWGBouquet.h"
#import "SWGSearch.h"
#import "SWGAttach.h"


@implementation NITCatalogInteractor

#pragma mark - NITCatalogInteractorInputProtocol
- (void)getCategoriesData
{
    weaken(self);
    [self.APIDataManager getCategoriesWithHandler:^(NSArray<SWGBouquetSection *> *result) {
        [weakSelf.presenter updateCategoriesDataByModels:[NITCategoryViewModel categoryModelsFromEntities:result]];
    }];
}

- (void)getStocksData
{
    weaken(self);
    [self.APIDataManager getStocksWithHandler:^(NSArray<SWGBanner *> *result) {
        [weakSelf.presenter updateStocksDataByModels:[NITStockViewModel stoksModelsFromEntities:result]];
    }];
}

- (void)getPopularSearces
{
    [self.APIDataManager getPopularSearchesWithHandler:^(NSArray<SWGPopular *> *output){
        
        if (USER.accessToken.length > 0)
        {
            [self.APIDataManager getUserPopularSearchesWithHandler:^(NSArray<SWGPopular *> *outputUser){
                NSArray * entities = [self excludeDuplicatesFromEntities:[output arrayByAddingObjectsFromArray:outputUser]];
                [self.presenter updatePopularSearchesData:[self popularSringsFromEntities:entities]];

            }];
        }
        else
        {
            [self.presenter updatePopularSearchesData:[self popularSringsFromEntities:output]];
        }
        
    }];
}

- (void)searchCatalogItemsByString:(NSString *)searchString
{
    if (searchString && searchString.length > 0)
    {
        weaken(self);
        [self.APIDataManager searchCatalogItemsByString:searchString withHandler:^(SWGSearch *output) {
        
            NSMutableArray * result = [NSMutableArray new];
            
            [result addObjectsFromArray:[NITCatalogItemViewModel bouqetModelsFromEntities:output.bouquets]];
            [result addObjectsFromArray:[NITCatalogItemViewModel attachModelsFromEntities:output.attaches]];
            
            [weakSelf.presenter updateSearchDataByModels:result];
        }];
    }
    else
    {
        [self.presenter updateSearchDataByModels:nil];
    }
}

#pragma mark - Private
- (NSArray <NSString *> *)popularSringsFromEntities:(NSArray <SWGPopular *> *)entities
{
    NSMutableArray * sortedModels = [entities mutableCopy];
    [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"count" ascending:false]]];
    
    return [sortedModels bk_map:^id(SWGPopular * obj) {
        return obj.text;
    }];
}

- (NSArray<SWGPopular *> *)excludeDuplicatesFromEntities:(NSArray<SWGPopular *> *)entities
{
    NSSet * set = [NSSet setWithArray:[self popularSringsFromEntities:entities]];
    
    return [[set allObjects] bk_map:^id(NSString * string) {
        
        NSMutableArray * sortedArray = [[entities bk_select:^BOOL(SWGPopular *obj) {
            return [obj.text isEqualToString:string];
        }] mutableCopy];
        
        [sortedArray sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"count" ascending:false]]];

        return sortedArray.firstObject;
    }];
}

@end
