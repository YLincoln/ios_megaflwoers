//
//  NITCatalogInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogProtocols.h"

@interface NITCatalogInteractor : NSObject <NITCatalogInteractorInputProtocol>

@property (nonatomic, weak) id <NITCatalogInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogLocalDataManagerInputProtocol> localDataManager;

@end
