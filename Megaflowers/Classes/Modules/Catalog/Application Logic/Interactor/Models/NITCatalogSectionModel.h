//
//  NITCatalogSectionModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef CF_ENUM (NSUInteger, CatalogSectionType) {
    CatalogSectionTypeStock     = 0,
    CatalogSectionTypeCategory  = 1
};

@interface NITCatalogSectionModel : NSObject

@property (nonatomic) CatalogSectionType type;
@property (nonatomic) NSArray * models;
@property (nonatomic) NSString * cellID;
@property (nonatomic) CGFloat rowHeight;

- (instancetype)initWithType:(CatalogSectionType)type;

@end
