//
//  NITCategoryViewModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCategoryViewModel.h"
#import "SWGBouquetSection.h"

@implementation NITCategoryViewModel

- (instancetype) initWithCategory:(SWGBouquetSection *)category
{
    if (self = [super init])
    {
        self.identifier = category._id;
        self.iconPath = [[(NSDictionary *)category.image allValues] firstObject];
        self.title = category.name;
    }
    
    return self;
}

+ (NSArray <NITCategoryViewModel *> *)categoryModelsFromEntities:(NSArray <SWGBouquetSection *> *)entities
{
    return [entities bk_map:^id(SWGBouquetSection * obj) {
        return [[NITCategoryViewModel alloc] initWithCategory:obj];
    }];
}

@end
