//
//  NITStockViewModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGBanner;

@interface NITStockViewModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * imagePath;
@property (nonatomic) NSString * title;

- (instancetype)initWithStock:(SWGBanner *)stock;
+ (NSArray <NITStockViewModel *> *)stoksModelsFromEntities:(NSArray <SWGBanner *> *)entities;

@end
