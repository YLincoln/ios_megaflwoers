//
//  NITStockViewModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITStockViewModel.h"
#import "SWGBanner.h"

@implementation NITStockViewModel

#pragma nark - Public
- (instancetype)initWithStock:(SWGBanner *)stock
{
    if ([super init])
    {
        self.identifier = stock.discountId;
        self.imagePath = [self imagePathFrom:stock];
        self.title = stock.name;
    }
    
    return self;
}

+ (NSArray <NITStockViewModel *> *)stoksModelsFromEntities:(NSArray <SWGBanner *> *)entities
{
    return [entities bk_map:^id(SWGBanner * obj) {
        return [[NITStockViewModel alloc] initWithStock:obj];
    }];
}

#pragma mark - Private
- (NSString *)imagePathFrom:(SWGBanner *)stock
{
    NSDictionary * imageInfo = (NSDictionary *)stock.image;
    return imageInfo[@"detail"][API.imageKey] ? : @"";
}

@end
