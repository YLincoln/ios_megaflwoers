//
//  NITCategoryViewModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGBouquetSection;

@interface NITCategoryViewModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * iconPath;

- (instancetype) initWithCategory:(SWGBouquetSection *)category;
+ (NSArray <NITCategoryViewModel *> *)categoryModelsFromEntities:(NSArray <SWGBouquetSection *> *)entities;

@end
