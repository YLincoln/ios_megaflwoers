//
//  NITCatalogProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITStockViewModel.h"
#import "NITCategoryViewModel.h"
#import "SWGBouquetSection.h"
#import "SWGBanner.h"
#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"
#import "SWGSearch.h"
#import "SWGPopular.h"
#import "NITFavoriteBouquet.h"
#import "NITBasketProductModel.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"
#import "NITCatalogSectionModel.h"

@protocol NITCatalogInteractorOutputProtocol;
@protocol NITCatalogInteractorInputProtocol;
@protocol NITCatalogViewProtocol;
@protocol NITCatalogPresenterProtocol;
@protocol NITCatalogLocalDataManagerInputProtocol;
@protocol NITCatalogAPIDataManagerInputProtocol;

@class NITCatalogWireFrame;

@protocol NITCatalogViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadSectionByModels:(NSArray <NITCatalogSectionModel *> *)models;
- (void)reloadSearchDataByModels:(NSArray<NITCatalogItemViewModel *> *)models;
- (void)reloadPopularSearchesData:(NSArray<NSString *> *)dataArray;
@end

@protocol NITCatalogWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITCatalogModule;
+ (void)presentNITCatalogModuleFrom:(id)fromView;
- (void)showSetupCitiesFrom:(id)fromView;
- (void)showCitiesFrom:(id)fromView;
- (void)showCatalogDirectoryFrom:(id)fromView withTitle:(NSString *)title parentId:(NSNumber *)parentId;
- (void)showStocksFrom:(id)fromView withTitle:(NSString *)title discountId:(NSNumber *)discountId;
- (void)showCallOrderFrom:(id)fromView;
- (void)showBasketFrom:(id)fromView;
@end

@protocol NITCatalogPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)iniData;
- (void)checkForceTouchAction;
- (void)updatePopularSearchesData;
- (void)searchCatalogItemsByString:(NSString *)searchString;
- (void)selectCityAction;
- (void)callToHotLineAction;
- (void)selectCategotyModel:(NITCategoryViewModel *)model;
- (void)selectStockModel:(NITStockViewModel *)model;
@end

@protocol NITCatalogInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCategoriesDataByModels:(NSArray<NITCategoryViewModel *> *)models;
- (void)updateStocksDataByModels:(NSArray<NITStockViewModel *> *)models;
- (void)updateSearchDataByModels:(NSArray<NITCatalogItemViewModel *> *)models;
- (void)updatePopularSearchesData:(NSArray<NSString *> *)dataArray;
@end

@protocol NITCatalogInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getCategoriesData;
- (void)getStocksData;
- (void)getPopularSearces;
- (void)searchCatalogItemsByString:(NSString *)searchString;
@end


@protocol NITCatalogDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogAPIDataManagerInputProtocol <NITCatalogDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getCategoriesWithHandler:(void (^)(NSArray<SWGBouquetSection *> * result))handler;
- (void)getStocksWithHandler:(void (^)(NSArray<SWGBanner *> * result))handler;
- (void)searchCatalogItemsByString:(NSString *)searchString withHandler:(void(^)(SWGSearch * output))handler;
- (void)getPopularSearchesWithHandler:(void(^)(NSArray<SWGPopular *> * output))handler;
- (void)getUserPopularSearchesWithHandler:(void(^)(NSArray<SWGPopular *> * output))handler;
@end

@protocol NITCatalogLocalDataManagerInputProtocol <NITCatalogDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */

@end
