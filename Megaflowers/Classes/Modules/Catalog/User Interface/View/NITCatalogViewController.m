//
//  NITCatalogViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogViewController.h"
#import "NITGuideAccountView.h"
#import "NITCatalogItemsView.h"
#import "NITCatalogTableViewCell.h"
#import "NITPopularSearchesView.h"
#import "NITCategoryCell.h"
#import "NITCatalogSliderTableCell.h"

@interface NITCatalogViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate,
TOSearchBarDelegate,
NITCatalogSliderCellDelegate,
NITPopularSearchesViewDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet NITPopularSearchesView  * popularSearchesView;
@property (nonatomic) IBOutlet NITCatalogItemsView * searchResultsView;
@property (nonatomic) IBOutlet UILabel * catalogTitle;
@property (nonatomic) IBOutlet NSLayoutConstraint * catalogTitleW;

@property (nonatomic) NSArray <NITCatalogSectionModel *> * sections;

@end

@implementation NITCatalogViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter iniData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
    [self showGuideIfNeed];
    [self.presenter checkForceTouchAction];
    [self updateUI];
}

#pragma mark - IBAction
- (IBAction)searchAction:(id)sender
{
    [self showSearchBarWithAnimations:^{
        self.searchResultsView.hidden = false;
        self.popularSearchesView.hidden = false;
    }];
}

- (IBAction)callAction:(id)sender
{
    [self.presenter callToHotLineAction];
}

- (IBAction)selectCityAction:(id)sender
{
    [self.presenter selectCityAction];
}

#pragma mark - Private
- (void)initUI
{
    [self setKeyboardActiv:true];
    
    // table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNibArray:@[_s(NITCategoryCell), _s(NITCatalogSliderTableCell)]];
    
    // Search
    [self initSearchBar];
    [self.popularSearchesView setDelegate:self];
    [self.searchBar setDelegate:self];
}

- (void)updateUI
{
    self.catalogTitle.text = USER.cityName;
    self.catalogTitleW.constant = MIN([self.catalogTitle sizeOfMultiLineLabel].width, ViewWidth(WINDOW) * 0.57);
    [self.searchResultsView reloadView];
}

- (void)showGuideIfNeed
{
    if ([NITGuideAccountView notYetShown])
    {
        if (self.sections.count > 0 && [WINDOW.visibleController isEqual:self])
        {
            [NITGuideAccountView showWidthOverlayRect:[WINDOW rectForTabbarItemByIndex:2]];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showGuideIfNeed];
            });
        }
    }
}

#pragma mark - NITCatalogPresenterProtocol
- (void)reloadSectionByModels:(NSArray<NITCatalogSectionModel *> *)models
{
    self.sections = models;
    [self.tableView reloadData];
}

- (void)reloadPopularSearchesData:(NSArray<NSString *> *)dataArray
{
    [self.popularSearchesView reloadDataByArray:dataArray];
}

- (void)reloadSearchDataByModels:(NSArray<NITCatalogItemViewModel *> *)models
{
    [self.searchResultsView updateViewMode:USER.viewType];
    [self.searchResultsView reloadCatalogItemsByModels:models delegate:nil];
    
    if ([self.searchBar.text length] > 0)
    {
        self.noResultsView.hidden = models.count > 0;
    }
    else
    {
        self.noResultsView.hidden = true;
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NITCatalogSectionModel * sectionModel = self.sections[section];
    return [sectionModel.models count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogSectionModel * sectionModel = self.sections[indexPath.section];
    return [sectionModel rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogSectionModel * sectionModel = self.sections[indexPath.section];
    NITCatalogTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[sectionModel cellID] forIndexPath:indexPath];
    [cell setModel:sectionModel.models[indexPath.row] delegate:self];

    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogSectionModel * sectionModel = self.sections[indexPath.section];
    [self.presenter selectCategotyModel:sectionModel.models[indexPath.row]];
    [self searchBarShouldReturn:self.searchBar];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 56, 0, 0)];
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.isTop)
    {
        [scrollView setContentOffset:CGPointMake(0, 0)];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [self.searchBar endEditing:true];
    [self enableSearchBarCanelButton];
}

#pragma mark - TOSearchBarDelegate
- (void)searchBarCancelButtonClicked:(TOSearchBar *)searchBar
{
    [searchBar setText:@""];
    [self.presenter searchCatalogItemsByString:searchBar.text];
    
    [self hideSearchBarWithAnimations:^{
        self.popularSearchesView.hidden = true;
        self.searchResultsView.hidden = true;
        [self.searchResultsView reloadCatalogItemsByModels:nil delegate:nil];
    }];
}

- (BOOL)searchBarShouldClear:(TOSearchBar *)searchBar
{
    [searchBar setText:@""];
    self.popularSearchesView.hidden = false;
    
    return true;
}

- (void)searchBar:(TOSearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.popularSearchesView.hidden = searchBar.text.length > 0;
    if (searchBar.text.length == 0)
    {
        [self.presenter searchCatalogItemsByString:searchBar.text];
    }
}

- (BOOL)searchBarShouldReturn:(TOSearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self.presenter searchCatalogItemsByString:searchBar.text];
    [self.popularSearchesView setHidden:searchBar.text.length > 0];
    [self enableSearchBarCanelButton];
    
    return true;
}

#pragma mark - NITPopularSearchesViewDelegate
- (void)didSelectPopularSearchString:(NSString *)searchString
{
    [self.searchBar setText:searchString];
    [self.presenter searchCatalogItemsByString:self.searchBar.text];
    [self.popularSearchesView setHidden:self.searchBar.text.length > 0];
    [self.searchBar endEditing:true];
    [self enableSearchBarCanelButton];
}

#pragma mark - NITCatalogSliderCellDelegate
- (void)didSelectSliderModel:(NITStockViewModel *)model
{
    [self.presenter selectStockModel:model];
}

@end
