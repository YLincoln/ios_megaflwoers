//
//  NITCatalogViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogProtocols.h"
#import "NITSearchViewController.h"

@interface NITCatalogViewController : NITSearchViewController <NITCatalogViewProtocol>

@property (nonatomic, strong) id <NITCatalogPresenterProtocol> presenter;

@end
