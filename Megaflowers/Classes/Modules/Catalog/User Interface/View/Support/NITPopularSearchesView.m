//
//  NITPopularSearchesView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPopularSearchesView.h"
#import "NITPopularSearchesCell.h"

@interface NITPopularSearchesView ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NSString *> * items;

@end

@implementation NITPopularSearchesView

#pragma mark - Life cycle
- (void)viewDidLoad
{
    [self initUI];
}

#pragma mark - Public
- (void)reloadDataByArray:(NSArray *)array
{
    self.items = array;
    [self.tableView reloadData];
}

#pragma mark - Private
- (void)initUI
{
    [self setKeyboardActiv:true];
    [self.tableView registerNibArray:@[_s(NITPopularSearchesCell)]];
    [self.tableView setTableFooterView:[UIView new]];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * sectionView = [[[NSBundle mainBundle] loadNibNamed:@"NITSearchEventHeader" owner:self options:nil] firstObject];
    UILabel * title = [sectionView viewWithTag:101];
    title.text = NSLocalizedString(@"Популярные запросы", nil);
    
    return sectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPopularSearchesCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITPopularSearchesCell) forIndexPath:indexPath];
    UILabel * title = [cell viewWithTag:101];
    title.text = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectPopularSearchString:)])
    {
        [self.delegate didSelectPopularSearchString:self.items[indexPath.row]];
    }
}

@end
