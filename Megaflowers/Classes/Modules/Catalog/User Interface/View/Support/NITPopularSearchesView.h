//
//  NITPopularSearchesView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "XibView.h"

@protocol  NITPopularSearchesViewDelegate <NSObject>

- (void)didSelectPopularSearchString:(NSString *)searchString;

@end

@interface NITPopularSearchesView : XibView

@property (nonatomic, weak) id <NITPopularSearchesViewDelegate> delegate;

- (void)reloadDataByArray:(NSArray *)array;

@end
