//
//  NITCatalogSliderTableCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogTableViewCell.h"
#import "NITCatalogSliderCell.h"

@class NITStockViewModel;

@interface NITCatalogSliderTableCell : NITCatalogTableViewCell

@property (nonatomic) NSArray <NITStockViewModel *> * sliderItems;
@property (nonatomic, weak) id <NITCatalogSliderCellDelegate> delegate;

@end
