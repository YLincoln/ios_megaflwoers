//
//  NITCatalogSliderTableCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogSliderTableCell.h"
#import "NITCatalogSliderCell.h"
#import "NITStockViewModel.h"
#import "NITAutoScrollCollectionView.h"
#import "NITCatalogSectionModel.h"

static NSInteger const sliderInterval = 5; // for auto scroll

@interface NITCatalogSliderTableCell ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UIScrollViewDelegate,
NITCatalogSliderCellDelegate
>

@property (nonatomic) IBOutlet NITAutoScrollCollectionView * slideCollection;
@property (nonatomic) IBOutlet UIPageControl * pageControl;

@end

@implementation NITCatalogSliderTableCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initUI];
}

#pragma mark - Public
- (void)setModel:(NITCatalogSectionModel *)model delegate:(id)delegate
{
    self.delegate = delegate;
    self.sliderItems = model.models;
}

#pragma mark - Custom accessors
- (void)setSliderItems:(NSArray<NITStockViewModel *> *)sliderItems
{
    _sliderItems = sliderItems;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)pageControlAction:(id)sender
{
    [self.slideCollection setAutoScrollTimeInterval:0];
    [self.slideCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:[self.pageControl currentPage] inSection:0]
                                 atScrollPosition:UICollectionViewScrollPositionNone
                                         animated:true];
}

#pragma mark - Private
- (void)initUI
{
    [self.slideCollection registerNib:[UINib nibWithNibName:_s(NITCatalogSliderCell) bundle:nil] forCellWithReuseIdentifier:_s(NITCatalogSliderCell)];
    UICollectionViewFlowLayout * flowLayout = (id)self.slideCollection.collectionViewLayout;
    flowLayout.itemSize = CGSizeMake(ViewWidth(WINDOW), ViewHeight(self));
    
    self.slideCollection.autoScrollTimeInterval = sliderInterval;
    self.slideCollection.stopAutoScrollAfterSwipe = true;
    
    [self.pageControl setCurrentPageIndicatorTintColor:RGB(125, 166, 142)];
}

- (void)updateData
{
    self.slideCollection.cellCount = self.sliderItems.count;
    self.pageControl.numberOfPages = self.sliderItems.count;
    [self.slideCollection reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.sliderItems count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogSliderCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITCatalogSliderCell) forIndexPath:indexPath];
    cell.model = self.sliderItems[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.pageControl setCurrentPage:indexPath.row];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.pageControl.currentPage = self.slideCollection.contentOffset.x / ViewWidth(self.slideCollection);
}

#pragma mark - NITCatalogSliderCellDelegate
- (void)didSelectSliderModel:(NITStockViewModel *)model
{
    if ([self.delegate respondsToSelector:@selector(didSelectSliderModel:)])
    {
        [self.delegate didSelectSliderModel:model];
    }
}

@end
