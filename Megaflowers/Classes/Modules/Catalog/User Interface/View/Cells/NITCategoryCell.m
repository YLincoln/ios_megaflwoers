//
//  NITCategoryCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCategoryCell.h"
#import "NITCategoryViewModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITCategoryCell ()

@property (nonatomic) IBOutlet UIImageView * icon;
@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITCategoryCell

#pragma mark - Public
- (void)setModel:(id)model delegate:(id)delegate
{
    self.model = model;
}

#pragma mark - Custom accessors
- (void)setModel:(NITCategoryViewModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.icon.image = nil;
    [self.icon setImageWithURL:[NSURL URLWithString:self.model.iconPath]];
}

@end
