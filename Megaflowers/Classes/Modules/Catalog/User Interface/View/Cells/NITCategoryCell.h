//
//  NITCategoryCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogTableViewCell.h"

@class NITCategoryViewModel;

@interface NITCategoryCell : NITCatalogTableViewCell

@property (nonatomic) NITCategoryViewModel * model;

@end
