//
//  NITCatalogSliderCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogSliderCell.h"
#import "NITStockViewModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITCatalogSliderCell ()

@property (nonatomic) IBOutlet UIImageView * imageView;

@end

@implementation NITCatalogSliderCell

- (void)setModel:(NITStockViewModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didSelectSliderModel:)])
    {
        [self.delegate didSelectSliderModel:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.imageView.image = nil;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.model.imagePath]];
}

@end
