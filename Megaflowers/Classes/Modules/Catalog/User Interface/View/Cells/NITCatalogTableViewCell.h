//
//  NITCatalogTableViewCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITCatalogTableViewCell : UITableViewCell

- (void)setModel:(id)model delegate:(id)delegate;

@end
