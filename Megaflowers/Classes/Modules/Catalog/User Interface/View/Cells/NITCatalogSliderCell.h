//
//  NITCatalogSliderCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITStockViewModel;

@protocol NITCatalogSliderCellDelegate <NSObject>
@optional
- (void)didSelectSliderModel:(NITStockViewModel *)model;

@end

@interface NITCatalogSliderCell : UICollectionViewCell

@property (nonatomic) NITStockViewModel * model;
@property (nonatomic, weak) id <NITCatalogSliderCellDelegate> delegate;

@end
