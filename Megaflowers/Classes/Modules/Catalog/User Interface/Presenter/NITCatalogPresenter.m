//
//  NITCatalogPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogPresenter.h"
#import "NITCatalogWireframe.h"
#import "NITCitiesWireFrame.h"
#import "NSArray+BlocksKit.h"

@interface NITCatalogPresenter ()

@property (nonatomic) NSArray <NITCatalogSectionModel *> * sections;

@end

@implementation NITCatalogPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Customs accessors
- (NSArray<NITCatalogSectionModel *> *)sections
{
    if (_sections.count != 2)
    {
        NITCatalogSectionModel * stocks = [[NITCatalogSectionModel alloc] initWithType:CatalogSectionTypeStock];
        NITCatalogSectionModel * categories = [[NITCatalogSectionModel alloc] initWithType:CatalogSectionTypeCategory];
        _sections = @[stocks, categories];
    }
    
    return _sections;
}

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - NITCatalogPresenterProtocol
- (void)iniData
{
    [self checkUserCity];
    [self updateData];
}

- (void)checkForceTouchAction
{
    if (HELPER.interfaceLoaded)
    {
        if ([Shortcut isCreatedShortcut])
        {
            if ([Shortcut isAvailableShortcutByTag:@"Fast"])
            {
                [PHONE callToPhoneNumber:USER.cityPhone];
                [TRACKER trackEvent:TrackerEventOrderByPhone];
                [Shortcut deleteShortcut];
            }
            else if ([Shortcut isAvailableShortcutByTag:@"Catalog"])
            {
                [Shortcut deleteShortcut];
            }
        }
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self checkForceTouchAction];
        });
    }
}

- (void)updatePopularSearchesData
{
    [self.interactor getPopularSearces];
}

- (void)searchCatalogItemsByString:(NSString *)searchString
{
    [self.interactor searchCatalogItemsByString:searchString];
}

- (void)selectCityAction
{
    [self.wireFrame showCitiesFrom:self.view];
}

- (void)selectCategotyModel:(NITCategoryViewModel *)model
{
    [TRACKER trackEvent:TrackerEventSelectCategory parameters:@{@"id":model.identifier}];
    [self.wireFrame showCatalogDirectoryFrom:self.view withTitle:model.title parentId:model.identifier];
}

- (void)selectStockModel:(NITCategoryViewModel *)model
{
    if (model.identifier)
    {
        [self.wireFrame showStocksFrom:self.view withTitle:model.title discountId:model.identifier];
    }
}

- (void)callToHotLineAction
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Заказ звонка", nil) actionBlock:^{
        [self.wireFrame showCallOrderFrom:self.view];
    }];
    
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Сделать заказ по телефону", nil) actionBlock:^{
        [PHONE callToPhoneNumber:USER.cityPhone];
        [TRACKER trackEvent:TrackerEventOrderByPhone];
    }];

    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1,action2] cancelButton:cancel];
}

#pragma mark - NITCatalogInteractorOutputProtocol
- (void)updateCategoriesDataByModels:(NSArray<NITCategoryViewModel *> *)models
{
    NITCatalogSectionModel * categoriesSection = self.sections[CatalogSectionTypeCategory];
    categoriesSection.models = models;
    [self.view reloadSectionByModels:self.sections];
}

- (void)updateStocksDataByModels:(NSArray<NITStockViewModel *> *)models
{
    NITCatalogSectionModel * stocksSection = self.sections[CatalogSectionTypeStock];
    
    if (models.count > 0)
    {
        NITCatalogSectionModel * stocks = [[NITCatalogSectionModel alloc] initWithType:CatalogSectionTypeStock];
        stocks.models = models;
        stocksSection.models = @[stocks];
    }
    else
    {
        stocksSection.models = nil;
    }
    
    [self.view reloadSectionByModels:self.sections];
}

- (void)updateSearchDataByModels:(NSArray<NITCatalogItemViewModel *> *)models
{
    [self.view reloadSearchDataByModels:models];
}

- (void)updatePopularSearchesData:(NSArray<NSString *> *)dataArray
{
    [self.view reloadPopularSearchesData:dataArray];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kChangeCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showBasket) name:kOpenbasketNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePopularSearchesData) name:kUpdateSearchesResults object:nil];
}

- (void)updateData
{
    if ([USER.cityID integerValue] > 0)
    {
        [self.interactor getCategoriesData];
        [self.interactor getStocksData];
        [self.interactor getPopularSearces];
    }
}

- (void)checkUserCity
{
    if ([USER.cityID integerValue] == 0 || [USER.countryID integerValue] == 0)
    {
        [self.wireFrame showSetupCitiesFrom:self.view];
    }
}

- (void)showBasket
{
    [self.wireFrame showBasketFrom:self.view];
    [[NSNotificationCenter defaultCenter] postNotificationName:kSetRootBasketNotification object:nil];
}

@end
