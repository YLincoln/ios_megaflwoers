//
//  NITCatalogPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogProtocols.h"

@class NITCatalogWireFrame;

@interface NITCatalogPresenter : NITRootPresenter <NITCatalogPresenterProtocol, NITCatalogInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogWireFrameProtocol> wireFrame;

@end
