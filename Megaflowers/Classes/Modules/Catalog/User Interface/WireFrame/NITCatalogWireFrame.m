//
//  NITCatalogWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogWireFrame.h"
#import "NITCitiesWireFrame.h"
#import "NITCatalogDirectoryWireFrame.h"
#import "NITCallOrderWireFrame.h"
#import "NITProductDetailsWireFrame.h"
#import "NITAuthorizationWireFrame.h"

@implementation NITCatalogWireFrame

#pragma mark - NITCatalogWireFrameProtocol
+ (id)createNITCatalogModule
{
    // Generating module components
    id <NITCatalogPresenterProtocol, NITCatalogInteractorOutputProtocol> presenter = [NITCatalogPresenter new];
    id <NITCatalogInteractorInputProtocol> interactor = [NITCatalogInteractor new];
    id <NITCatalogAPIDataManagerInputProtocol> APIDataManager = [NITCatalogAPIDataManager new];
    id <NITCatalogLocalDataManagerInputProtocol> localDataManager = [NITCatalogLocalDataManager new];
    id <NITCatalogWireFrameProtocol> wireFrame= [NITCatalogWireFrame new];
    id <NITCatalogViewProtocol> view = [(NITCatalogWireFrame *)wireFrame createViewControllerWithKey:_s(NITCatalogViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITCatalogModuleFrom:(UIViewController *)fromViewController
{
    NITCatalogViewController * view = [NITCatalogWireFrame createNITCatalogModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showSetupCitiesFrom:(UIViewController *)fromViewController
{
    [NITCitiesWireFrame presentNITCitiesModuleFrom:fromViewController withType:CityChangeTypeFirst];
}

- (void)showCitiesFrom:(UIViewController *)fromViewController
{
    [NITCitiesWireFrame presentNITCitiesModuleFrom:fromViewController withType:CityChangeTypeGlobal];
}

- (void)showCatalogDirectoryFrom:(UIViewController *)fromViewController withTitle:(NSString *)title parentId:(NSNumber *)parentId
{
    [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:fromViewController withTitle:title parentID:parentId];
}

- (void)showStocksFrom:(UIViewController *)fromViewController withTitle:(NSString *)title discountId:(NSNumber *)discountId
{
    [NITCatalogDirectoryWireFrame presentNITStockDirectoryModuleFrom:fromViewController withTitle:title discountId:discountId];
}

- (void)showCallOrderFrom:(UIViewController *)fromViewController
{
    [NITCallOrderWireFrame presentNITCallOrderModuleFrom:fromViewController];
}

- (void)showBasketFrom:(UIViewController *)fromView
{
    [fromView.tabBarController setSelectedIndex:1];
}

@end
