//
//  NITCatalogWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogProtocols.h"
#import "NITCatalogViewController.h"
#import "NITCatalogLocalDataManager.h"
#import "NITCatalogAPIDataManager.h"
#import "NITCatalogInteractor.h"
#import "NITCatalogPresenter.h"
#import "NITCatalogWireframe.h"
#import "NITRootWireframe.h"

@interface NITCatalogWireFrame : NITRootWireframe <NITCatalogWireFrameProtocol>

@end
