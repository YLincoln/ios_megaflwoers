//
//  NITBouquetInfoProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemComponentViewModel.h"
#import "NITCatalogItemSKUViewModel.h"

@protocol NITBouquetInfoInteractorOutputProtocol;
@protocol NITBouquetInfoInteractorInputProtocol;
@protocol NITBouquetInfoViewProtocol;
@protocol NITBouquetInfoPresenterProtocol;
@protocol NITBouquetInfoLocalDataManagerInputProtocol;
@protocol NITBouquetInfoAPIDataManagerInputProtocol;

@class NITBouquetInfoWireFrame;

@protocol NITBouquetInfoViewProtocol
@required
@property (nonatomic, strong) id <NITBouquetInfoPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITCatalogItemViewModel *)model
           baseComponents:(NSArray <NITCatalogItemComponentViewModel *> *)baseComponents
               components:(NSArray <NITCatalogItemComponentViewModel *> *)components;
- (void)reloadSaveButtonState:(BOOL)enable;
@end

@protocol NITBouquetInfoWireFrameProtocol
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@required
+ (void)presentNITBouquetInfoModuleFrom:(id)fromViewController withBouquet:(NITCatalogItemViewModel *)bouquet andSKUIndex:(NSInteger)index;
- (void)dissmisView:(id)viewController withModel:(NITCatalogItemViewModel *)model;
@end

@protocol NITBouquetInfoPresenterProtocol
@required
@property (nonatomic, weak) id <NITBouquetInfoViewProtocol> view;
@property (nonatomic, strong) id <NITBouquetInfoInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBouquetInfoWireFrameProtocol> wireFrame;
@property (nonatomic) NSInteger skuIndex;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updateData;
- (void)resetData;
- (void)updatePrice;
- (void)save;
- (void)cancel;
- (BOOL)isEditedSKU;
- (void)removeComponent:(NITCatalogItemComponentViewModel *)model;
- (void)saveProductState;
@end

@protocol NITBouquetInfoInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITBouquetInfoInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITBouquetInfoInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBouquetInfoAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBouquetInfoLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NITCatalogItemViewModel * data;
@property (nonatomic) NITCatalogItemViewModel * presentData;
@property (nonatomic) NITCatalogItemViewModel * currentData;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)updateDataWithHandler:(void(^)(NITCatalogItemViewModel * model))handler;
- (void)updatePriceByIndex:(NSInteger)index withHandler:(void (^)(NSString * error))handler;
@end


@protocol NITBouquetInfoDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITBouquetInfoAPIDataManagerInputProtocol <NITBouquetInfoDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getBouquetsByBouquetID:(NSNumber *)bouquetID withHandler:(void(^)(SWGBouquet * output))handler;
- (void)getAttachByID:(NSNumber *)attachID withHandler:(void(^)(SWGAttach * output))handler;
- (void)getPriceByBouquetID:(NSNumber *)bouquetID components:(NSArray *)components withHandler:(void (^)(NSNumber * result, NSError *error))handler;
@end

@protocol NITBouquetInfoLocalDataManagerInputProtocol <NITBouquetInfoDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
