//
//  NITBouquetInfoAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBouquetInfoAPIDataManager.h"
#import "SWGBouquetApi.h"
#import "SWGAttachApi.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITBouquetInfoAPIDataManager

- (void)getBouquetsByBouquetID:(NSNumber *)bouquetID withHandler:(void(^)(SWGBouquet * output))handler
{
    if (bouquetID)
    {
        [HELPER startLoading];
        [[[SWGBouquetApi alloc] initWithApiClient:[SWGApiClient sharedClient]] bouquetIdGetWithId:bouquetID
                                                                                             lang:HELPER.lang
                                                                                             site:API.site
                                                                                             city:USER.cityID
                                                                                completionHandler:^(SWGBouquet *output, NSError *error) {
                                                                                    [HELPER stopLoading];
                                                                                    if (error)
                                                                                    {
                                                                                        [HELPER logError:error method:METHOD_NAME];
                                                                                    }
                                                                                    
                                                                                    handler(output);
                                                                                }];
    }
}

- (void)getAttachByID:(NSNumber *)attachID withHandler:(void(^)(SWGAttach * output))handler
{
    [HELPER startLoading];
    [[[SWGAttachApi alloc] initWithApiClient:[SWGApiClient sharedClient]] attachIdGetWithId:attachID lang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(SWGAttach *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        
        handler(output);
    }];
}

- (void)getPriceByBouquetID:(NSNumber *)bouquetID components:(NSArray *)components withHandler:(void (^)(NSNumber * result, NSError *error))handler
{
    NSString * componentsStr = [components componentsJoinedByString:@","];
    [HELPER startLoading];
    [[SWGBouquetApi new] bouquetPriceGetWithComponents:componentsStr
                                                  site:API.site
                                                  city:USER.cityID
                                                  lang:HELPER.lang
                                               bouquet:bouquetID
                                     completionHandler:^(NSNumber *output, NSError *error) {
                                         [HELPER stopLoading];
                                         if (error)
                                         {
                                             [HELPER logError:error method:METHOD_NAME];
                                         }
                                         
                                         handler(output ? : @(0), error);
                                     }];
}

@end
