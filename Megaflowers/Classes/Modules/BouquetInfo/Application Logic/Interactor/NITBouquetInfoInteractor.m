//
//  NITBouquetInfoInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBouquetInfoInteractor.h"
#import "NSArray+BlocksKit.h"

@implementation NITBouquetInfoInteractor

- (void)updateDataWithHandler:(void (^)(NITCatalogItemViewModel *))handler
{
    switch (self.data.contentType) {
        case ProductContentTypeBouqets: {
            weaken(self);
            [self.APIDataManager getBouquetsByBouquetID:self.data.identifier withHandler:^(SWGBouquet *output) {
                
                weakSelf.data = [weakSelf modelFromEntity:output];
                
                if (weakSelf.currentData.skus.count != weakSelf.data.skus.count)
                {
                    weakSelf.data.skus = [weakSelf.data.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj) {
                        NITCatalogItemSKUViewModel * sku = weakSelf.currentData.skus.firstObject;
                        return [obj.identifier isEqualToNumber:sku.identifier];
                    }];
                }
                
                handler(weakSelf.currentData);
            }];
        }
            break;
            
        case ProductContentTypeAttach: {
            weaken(self);
            [self.APIDataManager getAttachByID:self.data.identifier withHandler:^(SWGAttach *output) {
                NSParameterAssert(output);
                weakSelf.data = [weakSelf attachModelsFromEntities:@[output]].firstObject;
                handler(weakSelf.currentData);
            }];
        }
            break;
    }
}

- (void)updatePriceByIndex:(NSInteger)index withHandler:(void (^)(NSString *error))handler
{
    NITCatalogItemSKUViewModel * sku = self.currentData.skus[index];
    NSMutableArray * components = [NSMutableArray new];
    for (NITCatalogItemComponentViewModel * component in  sku.components)
    {
        NSString * str = [NSString stringWithFormat:@"%@-%@-%@", component.identifier, component.color, component.count];
        [components addObject:str];
    }
    
    [self.APIDataManager getPriceByBouquetID:self.currentData.identifier components:components withHandler:^(NSNumber *result, NSError *error) {
        
        sku.price = result;
        sku.discountPrice = result;
        
        handler(error.interfaceDescription);
    }];
}

#pragma mark - Private
- (NITCatalogItemViewModel *)modelFromEntity:(SWGBouquet *)entity
{
    return [[NITCatalogItemViewModel alloc] initWithBouquet:entity];
}

- (NSArray <NITCatalogItemViewModel *> *)attachModelsFromEntities:(NSArray <SWGAttach *> *)entities
{
    return [entities bk_map:^id(SWGAttach * obj) {
        return [[NITCatalogItemViewModel alloc] initWithAttach:obj];
    }];
}

@end
