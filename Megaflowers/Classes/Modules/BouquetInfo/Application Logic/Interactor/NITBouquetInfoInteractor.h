//
//  NITBouquetInfoInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBouquetInfoProtocols.h"

@interface NITBouquetInfoInteractor : NSObject <NITBouquetInfoInteractorInputProtocol>

@property (nonatomic, weak) id <NITBouquetInfoInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBouquetInfoAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBouquetInfoLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NITCatalogItemViewModel * data;
@property (nonatomic) NITCatalogItemViewModel * presentData;
@property (nonatomic) NITCatalogItemViewModel * currentData;

@end
