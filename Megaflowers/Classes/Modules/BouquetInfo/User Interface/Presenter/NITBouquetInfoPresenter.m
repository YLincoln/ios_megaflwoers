//
//  NITBouquetInfoPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBouquetInfoPresenter.h"
#import "NITBouquetInfoWireframe.h"
#import "NSArray+BlocksKit.h"

@interface NITBouquetInfoPresenter ()

@property (nonatomic) NITCatalogItemViewModel * restoreModel;

@end

@implementation NITBouquetInfoPresenter

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self initData];
}

#pragma mark - Data
- (void)initData
{
    weaken(self);
    [self.interactor updateDataWithHandler:^(NITCatalogItemViewModel *model){
        [weakSelf updateData];
        [weakSelf updatePrice];
    }];
}

- (void)updateData
{
    NITCatalogItemSKUViewModel * sku = self.interactor.currentData.skus[self.skuIndex];
    NITCatalogItemSKUViewModel * baseSku = self.interactor.data.skus[self.skuIndex];
    [self.view reloadDataByModel:self.interactor.currentData baseComponents:baseSku.components components:sku.components];
}

- (void)resetData
{
    NITCatalogItemViewModel * model = [self.interactor.data copy];
    self.interactor.currentData.skus = [self.interactor.currentData.skus bk_map:^id(NITCatalogItemSKUViewModel * obj) {
        
        NITCatalogItemSKUViewModel * sku = [model.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj2) {
            return [obj2.identifier isEqualToNumber:obj.identifier];
        }].firstObject;
        
        if (sku)
        {
            obj.components = sku.components;
        }
        
        return obj;
    }];

    [self updatePrice];
}

- (void)updatePrice
{
    [self.view reloadSaveButtonState:false];
    
    weaken(self);
    [self.interactor updatePriceByIndex:self.skuIndex withHandler:^(NSString *error) {
        
        if (error.length > 0 && self.restoreModel)
        {
            self.interactor.currentData = self.restoreModel;
        }
        
        [weakSelf updateData];
        
        if (error.length > 0)
        {
            [weakSelf showError:error fromView:self.view];
        }
        else
        {
            [weakSelf.view reloadSaveButtonState:true];
        }
        
        [weakSelf.view reloadSaveButtonState:true];
    }];
}

- (BOOL)isEditedSKU
{
    NITCatalogItemSKUViewModel * model = self.interactor.data.skus[self.skuIndex];
    NITCatalogItemSKUViewModel * currentModel = self.interactor.currentData.skus[self.skuIndex];
    
    return ![model isEqualSKU:currentModel];
}

- (void)removeComponent:(NITCatalogItemComponentViewModel *)model
{
    NITCatalogItemSKUViewModel * sku = self.interactor.currentData.skus[self.skuIndex];
    NSMutableArray * components = [NSMutableArray arrayWithArray:sku.components];
    [components removeObject:model];

    NSMutableArray * skus = [NSMutableArray arrayWithArray:self.interactor.currentData.skus];
    [skus removeObjectAtIndex:self.skuIndex];
    
    sku.components = components;
    [skus insertObject:sku atIndex:self.skuIndex];
    
    self.interactor.currentData.skus = skus;
    
    [self updateData];
}

- (void)saveProductState
{
    self.restoreModel = [self.interactor.currentData copy];
}

#pragma mark - Navigation
- (void)save
{
    NITCatalogItemViewModel * model = self.interactor.currentData;
    [self.wireFrame dissmisView:self.view withModel:model];
    
    if (model.edited)
    {
        [TRACKER trackEvent:TrackerEventBouquetEdite parameters:@{@"id":model.identifier}];
    }
}

- (void)cancel
{
    [self.wireFrame dissmisView:self.view withModel:self.interactor.presentData];
}

@end
