//
//  NITBouquetInfoPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBouquetInfoProtocols.h"

@class NITBouquetInfoWireFrame;

@interface NITBouquetInfoPresenter : NITRootPresenter <NITBouquetInfoPresenterProtocol, NITBouquetInfoInteractorOutputProtocol>

@property (nonatomic, weak) id <NITBouquetInfoViewProtocol> view;
@property (nonatomic, strong) id <NITBouquetInfoInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBouquetInfoWireFrameProtocol> wireFrame;
@property (nonatomic) NSInteger skuIndex;

@end
