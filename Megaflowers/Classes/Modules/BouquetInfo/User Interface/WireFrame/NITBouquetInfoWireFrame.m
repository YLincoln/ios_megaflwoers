//
//  NITBouquetInfoWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBouquetInfoWireFrame.h"
#import "NITBouquetInfoViewController.h"

@implementation NITBouquetInfoWireFrame

+ (id)createNITBouquetInfoModuleWithBouquet:(NITCatalogItemViewModel *)bouquet andSKUIndex:(NSInteger)index
{
    // Generating module components
    id <NITBouquetInfoPresenterProtocol, NITBouquetInfoInteractorOutputProtocol> presenter = [NITBouquetInfoPresenter new];
    id <NITBouquetInfoInteractorInputProtocol> interactor = [NITBouquetInfoInteractor new];
    id <NITBouquetInfoAPIDataManagerInputProtocol> APIDataManager = [NITBouquetInfoAPIDataManager new];
    id <NITBouquetInfoLocalDataManagerInputProtocol> localDataManager = [NITBouquetInfoLocalDataManager new];
    id <NITBouquetInfoWireFrameProtocol> wireFrame= [NITBouquetInfoWireFrame new];
    id <NITBouquetInfoViewProtocol> view = [(NITBouquetInfoWireFrame *)wireFrame createViewControllerWithKey:_s(NITBouquetInfoViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    presenter.skuIndex = index;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    interactor.data = [bouquet copy];
    interactor.presentData = [bouquet copy];
    interactor.currentData = bouquet;
    
    return view;
}

+ (void)presentNITBouquetInfoModuleFrom:(UIViewController *)fromViewController withBouquet:(NITCatalogItemViewModel *)bouquet andSKUIndex:(NSInteger)index
{
    NITBouquetInfoViewController * view = [NITBouquetInfoWireFrame createNITBouquetInfoModuleWithBouquet:bouquet andSKUIndex:index];
    view.delegate = (id)fromViewController;
    [fromViewController presentViewController:view animated:true completion:^{}];
}

- (void)dissmisView:(NITBouquetInfoViewController *)viewController withModel:(NITCatalogItemViewModel *)model
{
    [viewController dismissViewControllerAnimated:true completion:^{
        if ([viewController.delegate respondsToSelector:@selector(didHideDetailsWithModel:)]) {
            [viewController.delegate didHideDetailsWithModel:model];
        }
    }];
}

@end
