//
//  NITBouquetInfoWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBouquetInfoProtocols.h"
#import "NITBouquetInfoViewController.h"
#import "NITBouquetInfoLocalDataManager.h"
#import "NITBouquetInfoAPIDataManager.h"
#import "NITBouquetInfoInteractor.h"
#import "NITBouquetInfoPresenter.h"
#import "NITBouquetInfoWireframe.h"
#import "NITRootWireframe.h"

@interface NITBouquetInfoWireFrame : NITRootWireframe <NITBouquetInfoWireFrameProtocol>

@end
