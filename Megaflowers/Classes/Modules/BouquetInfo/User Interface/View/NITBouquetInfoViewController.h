//
//  NITBouquetInfoViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBouquetInfoProtocols.h"
#import "NITBaseViewController.h"

@protocol NITBouquetInfoViewControllerDelegate <NSObject>

- (void)didHideDetailsWithModel:(NITCatalogItemViewModel *)model;

@end

@interface NITBouquetInfoViewController : NITBaseViewController <NITBouquetInfoViewProtocol>

@property (nonatomic, strong) id <NITBouquetInfoPresenterProtocol> presenter;
@property (nonatomic, weak) id <NITBouquetInfoViewControllerDelegate> delegate;

@end
