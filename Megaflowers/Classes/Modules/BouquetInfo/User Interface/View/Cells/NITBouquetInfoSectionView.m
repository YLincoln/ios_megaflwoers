//
//  NITBouquetInfoSectionView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
// 116

#import "NITBouquetInfoSectionView.h"

@interface NITBouquetInfoSectionView ()

@property (nonatomic) IBOutlet UILabel * text;
@property (nonatomic) IBOutlet UIButton * resetBtn;

@end

@implementation NITBouquetInfoSectionView

+ (instancetype)viewWithText:(NSString *)text
{
    NITBouquetInfoSectionView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITBouquetInfoSectionView) owner:self options:nil] firstObject];
    [view.text setText:text];
    [view setFrame:[view calculateFrame]];
    
    return view;
}

- (void)updateEditedState:(BOOL)isEdited
{
    self.resetBtn.hidden = !isEdited;
}

#pragma mark - IBAction
- (IBAction)resetAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didResetAction)])
    {
        [self.delegate didResetAction];
    }
}

#pragma mark - Private
- (CGRect)calculateFrame
{
    return CGRectMake(0, 0, ViewWidth(WINDOW), [self.text heightOfMultiLineLabel] + 116);
}

@end
