//
//  NITBouquetComponentCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/5/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITCatalogItemComponentViewModel;

@protocol NITBouquetComponentCellDelegate <NSObject>

- (void)didUpdatePrice;
- (void)didDeleteComponent:(NITCatalogItemComponentViewModel *)model;
- (void)didUpdateDataModel;

@end

@interface NITBouquetComponentCell : UITableViewCell

@property (nonatomic) NITCatalogItemComponentViewModel * model;
@property (nonatomic) NITCatalogItemComponentViewModel * baseModel;
@property (nonatomic, weak) id <NITBouquetComponentCellDelegate> delegate;

@end
