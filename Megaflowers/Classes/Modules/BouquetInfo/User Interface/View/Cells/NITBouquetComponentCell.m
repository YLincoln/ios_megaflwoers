//
//  NITBouquetComponentCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/5/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBouquetComponentCell.h"
#import "NITCatalogItemComponentViewModel.h"

@interface NITBouquetComponentCell ()

@property (weak, nonatomic) IBOutlet UILabel * title;
@property (weak, nonatomic) IBOutlet UILabel * count;

@end

@implementation NITBouquetComponentCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITCatalogItemComponentViewModel *)model
{
    _model = model;
    [self updateData];
}

- (void)setBaseModel:(NITCatalogItemComponentViewModel *)baseModel
{
    _baseModel = baseModel;
    [self updateTitleColor];
}

#pragma mark - IBAction
- (IBAction)minusAction:(id)sender
{
    NSUInteger count = [self.model.count integerValue];
    
    if (count > 1)
    {
        count--;
        
        if ([self.delegate respondsToSelector:@selector(didUpdateDataModel)])
        {
            [self.delegate didUpdateDataModel];
        }
        
        self.model.count = @(count);
        [self updateCounter];
        
        if ([self.delegate respondsToSelector:@selector(didUpdatePrice)])
        {
            [self.delegate didUpdatePrice];
        }
    }
    else if (count == 1 && !self.model.required)
    {
        if ([self.delegate respondsToSelector:@selector(didUpdateDataModel)])
        {
            [self.delegate didUpdateDataModel];
        }
        
        if ([self.delegate respondsToSelector:@selector(didDeleteComponent:)])
        {
            [self.delegate didDeleteComponent:self.model];
        }
    }
}

- (IBAction)plusAction:(id)sender
{
    NSUInteger count = [self.model.count integerValue];
    count++;
    
    if ([self.delegate respondsToSelector:@selector(didUpdateDataModel)])
    {
        [self.delegate didUpdateDataModel];
    }
    
    self.model.count = @(count);
    [self updateCounter];
    
    if ([self.delegate respondsToSelector:@selector(didUpdatePrice)])
    {
        [self.delegate didUpdatePrice];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    [self updateCounter];
}

- (void)updateCounter
{
    self.count.text = [self.model.count stringValue];
    [self updateTitleColor];
}

- (void)updateTitleColor
{
    if ([self.model isEqualComponent:self.baseModel])
    {
        self.title.textColor = RGB(58, 58, 58);
        self.title.font = [UIFont systemFontOfSize:14 weight:UIFontWeightLight];
    }
    else
    {
        self.title.textColor =  RGB(32, 101, 61);
        self.title.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    }
}

@end
