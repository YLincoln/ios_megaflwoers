//
//  NITBouquetInfoSectionView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@protocol NITBouquetInfoSectionViewDelegate <NSObject>

- (void)didResetAction;

@end

@interface NITBouquetInfoSectionView : UIView

@property (nonatomic) id <NITBouquetInfoSectionViewDelegate> delegate;

+ (instancetype)viewWithText:(NSString *)text;
- (void)updateEditedState:(BOOL)isEdited;

@end
