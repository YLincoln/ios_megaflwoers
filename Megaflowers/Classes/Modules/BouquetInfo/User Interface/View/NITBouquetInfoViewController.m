//
//  NITBouquetInfoViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBouquetInfoViewController.h"
#import "NITBouquetInfoSectionView.h"
#import "NITBouquetComponentCell.h"

@interface NITBouquetInfoViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITBouquetInfoSectionViewDelegate,
NITBouquetComponentCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UILabel * priceLbl;
@property (nonatomic) IBOutlet UIButton * saveBtn;

@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic) NSArray <NITCatalogItemComponentViewModel *> * components;
@property (nonatomic) NSArray <NITCatalogItemComponentViewModel *> * baseComponents;
@property (nonatomic) NSNumber * currentPrice;
@property (nonatomic) NITBouquetInfoSectionView * headerView;

@end

@implementation NITBouquetInfoViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

- (void)setCurrentPrice:(NSNumber *)currentPrice
{
    _currentPrice = currentPrice;
    [self updatePrice];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.presenter cancel];
}

- (IBAction)saveAction:(id)sender
{
    [self.presenter save];
}

#pragma mark - Private
- (void)initUI
{
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITBouquetComponentCell) bundle:nil] forCellReuseIdentifier:_s(NITBouquetComponentCell)];
}

- (void)updatePrice
{
    self.priceLbl.text = [NSString stringWithFormat:@"%@ ₽", self.currentPrice];
}

#pragma mark - NITBouquetInfoPresenterProtocol
- (void)reloadDataByModel:(NITCatalogItemViewModel *)model
           baseComponents:(NSArray <NITCatalogItemComponentViewModel *> *)baseComponents
               components:(NSArray <NITCatalogItemComponentViewModel *> *)components
{
    self.model = model;
    self.components = components;
    self.baseComponents = baseComponents;
    
    NITCatalogItemSKUViewModel * mod = self.model.skus[self.presenter.skuIndex];
    self.currentPrice = mod.price;
    
    self.headerView = [NITBouquetInfoSectionView viewWithText:model.detail];
    self.headerView.delegate = self;
    [self.headerView updateEditedState:[self.presenter isEditedSKU]];
    [self.tableView setTableHeaderView:self.headerView];
    
    [self.tableView reloadData];
}

- (void)reloadSaveButtonState:(BOOL)enable
{
    [self.saveBtn setEnabled:enable];
    [self.tableView setUserInteractionEnabled:enable];
    /*[self.saveBtn setBackgroundColor:enable ? RGB(10, 88, 43) : [UIColor lightGrayColor]];*/
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.components count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITBouquetComponentCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITBouquetComponentCell) forIndexPath:indexPath];
    cell.model = self.components[indexPath.row];
    cell.baseModel = self.baseComponents[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - NITBouquetInfoSectionViewDelegate
- (void)didResetAction
{
    [self.presenter resetData];
}

#pragma mark - NITBouquetComponentCellDelegate
- (void)didUpdatePrice
{
    [self.presenter updatePrice];
    [self.headerView updateEditedState:[self.presenter isEditedSKU]];
}

- (void)didDeleteComponent:(NITCatalogItemComponentViewModel *)model
{
    [self.presenter removeComponent:model];
    [self.presenter updatePrice];
}

- (void)didUpdateDataModel
{
    [self.presenter saveProductState];
}

@end
