//
//  NITProfileProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProfileSectionModel.h"

@protocol NITProfileInteractorOutputProtocol;
@protocol NITProfileInteractorInputProtocol;
@protocol NITProfileViewProtocol;
@protocol NITProfilePresenterProtocol;
@protocol NITProfileLocalDataManagerInputProtocol;
@protocol NITProfileAPIDataManagerInputProtocol;

@class NITProfileWireFrame;

typedef CF_ENUM (NSUInteger, ProfileState) {
    ProfileStateUnlogged    = 0,
    ProfileStateAuthorized  = 1
};

@protocol NITProfileViewProtocol
@required
@property (nonatomic, strong) id <NITProfilePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)updateViewBySate:(ProfileState)state;
- (void)reloadProfileModels:(NSArray <NITProfileSectionModel *> *)models;
@end

@protocol NITProfileWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITProfileModule;
+ (void)presentNITProfileModuleFrom:(id)fromView;
- (void)showAuthorizationFrom:(id)fromView;
- (void)showPersonalDataFrom:(id)fromView;
- (void)showCalendarFrom:(id)fromView;
- (void)showFavoriteBouquetsFrom:(id)fromView;
- (void)showFavoriteSalonsFrom:(id)fromView;
- (void)showMyOrdersFrom:(id)fromView;
- (void)showDiscontCardFrom:(id)fromView;
- (void)showPushSettingsFrom:(id)fromView;
- (void)showFeedbackFrom:(id)fromView;
- (void)showGuideFrom:(id)fromView;
@end

@protocol NITProfilePresenterProtocol
@required
@property (nonatomic, weak) id <NITProfileViewProtocol> view;
@property (nonatomic, strong) id <NITProfileInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITProfileWireFrameProtocol> wireFrame;
@property (nonatomic) ProfileState state;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)checkLoginState;
- (void)authorizeUser;
- (void)showSectionForModel:(NITProfileSectionModel *)model;
- (void)showControllerFrom3DTouch;
- (void)showAuthorizationNotificationIfNeed;
@end

@protocol NITProfileInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITProfileInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITProfileInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITProfileAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITProfileLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)checkUnloggedUserOrderWithHandler:(void(^)(BOOL haveOrder))handler;
@end


@protocol NITProfileDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITProfileAPIDataManagerInputProtocol <NITProfileDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITProfileLocalDataManagerInputProtocol <NITProfileDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
