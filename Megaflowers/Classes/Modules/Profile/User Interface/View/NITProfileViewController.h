//
//  NITProfileViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITProfileProtocols.h"

@interface NITProfileViewController : NITBaseViewController <NITProfileViewProtocol>

@property (nonatomic, strong) id <NITProfilePresenterProtocol> presenter;

@end
