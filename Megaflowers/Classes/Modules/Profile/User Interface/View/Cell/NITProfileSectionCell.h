//
//  NITProfileSectionCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITProfileSectionModel;

@interface NITProfileSectionCell : UITableViewCell

@property (nonatomic) NITProfileSectionModel * model;

@end
