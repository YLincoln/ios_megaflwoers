//
//  NITProfileSectionCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProfileSectionCell.h"
#import "NITProfileSectionModel.h"

@interface NITProfileSectionCell ()

@property (nonatomic) IBOutlet UIImageView * icon;
@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITProfileSectionCell

- (void)setModel:(NITProfileSectionModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    //self.userInteractionEnabled = self.model.enabled;
    self.title.text = self.model.name;
    self.title.textColor = self.model.enabled ? [UIColor blackColor] : RGB(162, 182, 168);
    self.icon.image = [UIImage imageNamed:self.model.iconName];
    self.icon.highlighted = self.model.enabled;
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:self.model.enabled ? @"ic_right" : @"ic_lock"]];
}

@end
