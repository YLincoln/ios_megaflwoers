//
//  NITProfileViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProfileViewController.h"
#import "NITProfileSectionCell.h"

@interface NITProfileViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UIBarButtonItem * autorizeBtn;
@property (nonatomic) IBOutlet UITableView * tableView;

@property (nonatomic) NSArray <NITProfileSectionModel *> * items;

@end

@implementation NITProfileViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter showControllerFrom3DTouch];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.presenter checkLoginState];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - IBAction
- (IBAction)autorizeAction:(id)sender
{
    [self.presenter authorizeUser];
}

#pragma mark - Private
- (void)initUI
{    
    self.title = NSLocalizedString(@"Аккаунт", nil);
    
    if ([[self.navigationController viewControllers].firstObject isKindOfClass:[NITProfileViewController class]])
    {
        self.navigationItem.hidesBackButton = true;
    }
    
    // table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITProfileSectionCell) bundle:nil] forCellReuseIdentifier:_s(NITProfileSectionCell)];
}

#pragma mark - NITProfilePresenterProtocol
- (void)updateViewBySate:(ProfileState)state
{
    switch (state) {
        case ProfileStateAuthorized:
            self.navigationItem.rightBarButtonItem = nil;
            break;
        case ProfileStateUnlogged:
            self.navigationItem.rightBarButtonItem = self.autorizeBtn;
            break;
        default:
            break;
    }
}

- (void)reloadProfileModels:(NSArray <NITProfileSectionModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITProfileSectionCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITProfileSectionCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.items[indexPath.row].enabled)
    {
        [self.presenter showSectionForModel:self.items[indexPath.row]];
    }
    else
    {
        [self.presenter showAuthorizationNotificationIfNeed];
    }
}

@end
