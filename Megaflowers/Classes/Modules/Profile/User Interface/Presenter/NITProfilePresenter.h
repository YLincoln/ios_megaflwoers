//
//  NITProfilePresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProfileProtocols.h"

@class NITProfileWireFrame;

@interface NITProfilePresenter : NITRootPresenter <NITProfilePresenterProtocol, NITProfileInteractorOutputProtocol>

@property (nonatomic, weak) id <NITProfileViewProtocol> view;
@property (nonatomic, strong) id <NITProfileInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITProfileWireFrameProtocol> wireFrame;
@property (nonatomic) ProfileState state;
@property (nonatomic) NSTimer * authorizationTimer;

@end
