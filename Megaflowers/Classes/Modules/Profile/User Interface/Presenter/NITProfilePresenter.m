//
//  NITProfilePresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProfilePresenter.h"
#import "NITProfileWireframe.h"
#import "NITNotificationView.h"
#import "NITDiscontCardWireFrame.h"
#import "NITTabBarWireFrame.h"

@implementation NITProfilePresenter

- (void)checkLoginState
{
    self.state = USER.isAutorize ? ProfileStateAuthorized : ProfileStateUnlogged;
    [self.view updateViewBySate:self.state];
    
    switch (self.state) {
        case ProfileStateAuthorized:
            [self initData];
            break;
        case ProfileStateUnlogged:
            [self initUnloggedData];
            break;
        default:
            break;
    }
}

- (void)initData
{
    NSArray * items = @[
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeDisscont state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeCalendar state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeFavorite state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeShops state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypePurchaseHistory state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypePersonalData state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypePush state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeGuide state:true],
                        [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeFeedback state:true],
                        ];
    
    [self.view reloadProfileModels:items];
}

- (void)initUnloggedData
{
    [self.interactor checkUnloggedUserOrderWithHandler:^(BOOL haveOrder) {
        NSArray * items = @[
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypePush state:true],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeGuide state:true],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeFeedback state:true],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypePurchaseHistory state:haveOrder],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeDisscont state:false],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeCalendar state:false],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeFavorite state:false],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypeShops state:false],
                            [[NITProfileSectionModel alloc] initWithType:ProfileSectionTypePersonalData state:false]
                            ];
        
        [self.view reloadProfileModels:items];
    }];
}

#pragma mark - Navigation
- (void)authorizeUser
{
    [self.wireFrame showAuthorizationFrom:self.view];
}

- (void)showSectionForModel:(NITProfileSectionModel *)model
{
    switch (model.type) {
        case ProfileSectionTypeDisscont:
            [self.wireFrame showDiscontCardFrom:self.view];
            break;
        case ProfileSectionTypeCalendar:
            [self.wireFrame showCalendarFrom:self.view];
            break;
        case ProfileSectionTypeFavorite:
            [self.wireFrame showFavoriteBouquetsFrom:self.view];
            break;
        case ProfileSectionTypeShops:
            [self.wireFrame showFavoriteSalonsFrom:self.view];
            break;
        case ProfileSectionTypePurchaseHistory:
            [self.wireFrame showMyOrdersFrom:self.view];
            break;
        case ProfileSectionTypePersonalData:
            [self.wireFrame showPersonalDataFrom:self.view];
            break;
        case ProfileSectionTypePush:
            [self.wireFrame showPushSettingsFrom:self.view];
            break;
        case ProfileSectionTypeGuide:
            [self.wireFrame showGuideFrom:self.view];
            break;
        case ProfileSectionTypeFeedback:
            [self.wireFrame showFeedbackFrom:self.view];
            break;
    }
}

#pragma mark - Authorization notification
- (void)showAuthorizationNotificationIfNeed
{
    weaken(self);
    if(!self.authorizationTimer)
    {
        [self startAuthorizationTimer];
        [NITNotificationView showWithText:NSLocalizedString(@"Зарегистрируйтесь. Вам доступен не весь функционал", nil) tapHandler:^{
            [weakSelf authorizeUser];
        }];
    }
}

- (void)startAuthorizationTimer
{
    self.authorizationTimer = [NSTimer scheduledTimerWithTimeInterval:6.0f
                                                               target:self
                                                             selector:@selector(stopAuthorizationTimer)
                                                             userInfo:nil
                                                              repeats:NO];
}

- (void)stopAuthorizationTimer
{
    if (self.authorizationTimer)
    {
        [self.authorizationTimer invalidate];
        self.authorizationTimer = nil;
    }
}

#pragma mark - 3D Touch

- (void)showControllerFrom3DTouch
{
    if([Shortcut isCreatedShortcut])
    {
        if([Shortcut isAvailableShortcutByTag:@"Status"])           [self.wireFrame showMyOrdersFrom:self.view];
        if([Shortcut isAvailableShortcutByTag:@"Room"])             [self.wireFrame showPersonalDataFrom:self.view];
        if([Shortcut isAvailableShortcutByTag:@"OrdersComplited"])  [self.wireFrame showMyOrdersFrom:self.view];
        if([Shortcut isAvailableShortcutByTag:@"OrderActive"])      [self.wireFrame showMyOrdersFrom:self.view];
        if([Shortcut isAvailableShortcutByTag:@"OrderPickup"])      [self.wireFrame showMyOrdersFrom:self.view];
    }
}

@end
