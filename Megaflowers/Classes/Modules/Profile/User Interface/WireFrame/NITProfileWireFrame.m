//
//  NITProfileWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProfileWireFrame.h"
#import "NITCalendarWireFrame.h"
#import "NITPersonalDataWireFrame.h"
#import "NITFavoriteBouquetsWireFrame.h"
#import "NITFavoriteSalonsWireFrame.h"
#import "NITOrdersListWireFrame.h"
#import "NITDiscontCardWireFrame.h"
#import "NITPushSettingsWireFrame.h"
#import "NITFeedbackWireFrame.h"
#import "NITAuthorizationWireFrame.h"
#import "NITGuideWireFrame.h"

@implementation NITProfileWireFrame

+ (id)createNITProfileModule
{
    // Generating module components
    id <NITProfilePresenterProtocol, NITProfileInteractorOutputProtocol> presenter = [NITProfilePresenter new];
    id <NITProfileInteractorInputProtocol> interactor = [NITProfileInteractor new];
    id <NITProfileAPIDataManagerInputProtocol> APIDataManager = [NITProfileAPIDataManager new];
    id <NITProfileLocalDataManagerInputProtocol> localDataManager = [NITProfileLocalDataManager new];
    id <NITProfileWireFrameProtocol> wireFrame= [NITProfileWireFrame new];
    id <NITProfileViewProtocol> view = [(NITProfileWireFrame *)wireFrame createViewControllerWithKey:_s(NITProfileViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITProfileModuleFrom:(UIViewController *)fromViewController
{
    NITProfileViewController * view = [NITProfileWireFrame createNITProfileModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showPersonalDataFrom:(UIViewController *)fromView
{
    [NITPersonalDataWireFrame presentNITPersonalDataModuleFrom:fromView];
}

- (void)showCalendarFrom:(UIViewController *)fromView
{
    [NITCalendarWireFrame presentNITCalendarModuleFrom:fromView];
}

- (void)showFavoriteBouquetsFrom:(UIViewController *)fromView
{
    [NITFavoriteBouquetsWireFrame presentNITFavoriteBouquetsModuleFrom:fromView];
}

- (void)showFavoriteSalonsFrom:(UIViewController *)fromViewController
{
    [NITFavoriteSalonsWireFrame presentNITFavoriteSalonsModuleFrom:fromViewController];
}

- (void)showMyOrdersFrom:(UIViewController *)fromViewController
{
    [NITOrdersListWireFrame presentNITOrdersListModuleFrom:fromViewController];
}

- (void)showDiscontCardFrom:(UIViewController *)fromViewController
{
    [NITDiscontCardWireFrame presentNITDiscontCardModuleFrom:fromViewController];
}

- (void)showPushSettingsFrom:(UIViewController *)fromViewController
{
    [NITPushSettingsWireFrame presentNITPushSettingsModuleFrom:fromViewController];
}

- (void)showFeedbackFrom:(UIViewController *)fromViewController
{
    [NITFeedbackWireFrame presentNITFeedbackModuleFrom:fromViewController];
}

- (void)showAuthorizationFrom:(UIViewController *)fromViewController
{
    [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:fromViewController];
}

- (void)showGuideFrom:(UIViewController *)fromViewController
{
    [NITGuideWireFrame presentNITGuideModuleFrom:fromViewController widthState:GuideStateView];
}

@end
