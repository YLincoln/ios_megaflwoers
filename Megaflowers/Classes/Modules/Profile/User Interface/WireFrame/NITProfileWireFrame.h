//
//  NITProfileWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProfileProtocols.h"
#import "NITProfileViewController.h"
#import "NITProfileLocalDataManager.h"
#import "NITProfileAPIDataManager.h"
#import "NITProfileInteractor.h"
#import "NITProfilePresenter.h"
#import "NITProfileWireframe.h"
#import "NITRootWireframe.h"

@interface NITProfileWireFrame : NITRootWireframe <NITProfileWireFrameProtocol>

@end
