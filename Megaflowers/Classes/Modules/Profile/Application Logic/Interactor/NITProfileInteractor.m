//
//  NITProfileInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProfileInteractor.h"
#import "NITOrderDetailsAPIDataManager.h"
#import "NITOrder.h"
#import "NITMyOrderModel.h"

@implementation NITProfileInteractor

- (void)checkUnloggedUserOrderWithHandler:(void(^)(BOOL haveOrder))handler
{
    if (USER.lastOrderID)
    {
        [USER checkAutorizationWithHandler:^(BOOL success) {
            if (success)
            {
                NITOrderDetailsAPIDataManager * api = [NITOrderDetailsAPIDataManager new];
                [api getUserOrderByID:USER.lastOrderID withHandler:^(SWGOrder *result) {
                    if (result)
                    {
                        NITMyOrderModel * model = [[NITMyOrderModel alloc] initWithOrder:result];
                        if (!model.complited)
                        {
                            [NITOrder addOrders:@[result]];
                            if (handler) handler(true);
                        }
                        else
                        {
                            if (handler) handler(false);
                        }
                    }
                    else
                    {
                        if (handler) handler(false);
                    }
                }];
            }
            else
            {
                if (handler) handler(false);
            }
        }];
    }
    else
    {
        if (handler) handler(false);
    }
}

@end
