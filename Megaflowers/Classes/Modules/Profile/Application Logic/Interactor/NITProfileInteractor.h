//
//  NITProfileInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProfileProtocols.h"

@interface NITProfileInteractor : NSObject <NITProfileInteractorInputProtocol>

@property (nonatomic, weak) id <NITProfileInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITProfileAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITProfileLocalDataManagerInputProtocol> localDataManager;

@end
