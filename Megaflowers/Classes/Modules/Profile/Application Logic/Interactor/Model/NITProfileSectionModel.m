//
//  NITProfileSectionModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITProfileSectionModel.h"

@implementation NITProfileSectionModel

- (instancetype)initWithType:(ProfileSectionType)type state:(BOOL)state
{
    if (self = [super init])
    {
        self.type = type;
        self.enabled = state;
    }
    
    return self;
}

- (void)setType:(ProfileSectionType)type
{
    _type = type;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    switch (self.type) {
        case ProfileSectionTypeDisscont:
            self.name = NSLocalizedString(@"Дисконт, купоны и скидки", nil);
            self.iconName = @"ic_discount";
            break;
            
        case ProfileSectionTypeCalendar:
            self.name = NSLocalizedString(@"Календарь событий", nil);
            self.iconName = @"ic_calendar";
            break;
        case ProfileSectionTypeFavorite:
            self.name = NSLocalizedString(@"Избранное", nil);
            self.iconName = @"ic_liked";
            break;
        case ProfileSectionTypeShops:
            self.name = NSLocalizedString(@"Любимые магазины", nil);
            self.iconName = @"ic_love";
            break;
        case ProfileSectionTypePurchaseHistory:
            self.name = NSLocalizedString(@"История покупок", nil);
            self.iconName = @"ic_latertime";
            break;
        case ProfileSectionTypePersonalData:
            self.name = NSLocalizedString(@"Личные данные", nil);
            self.iconName = @"ic_data";
            break;
        case ProfileSectionTypePush:
            self.name = NSLocalizedString(@"Push-уведомления", nil);
            self.iconName = @"ic_push";
            break;
        case ProfileSectionTypeGuide:
            self.name = NSLocalizedString(@"Гид по приложению", nil);
            self.iconName = @"ic_guide";
            break;
        case ProfileSectionTypeFeedback:
            self.name = NSLocalizedString(@"Обратная связь", nil);
            self.iconName = @"ic_feedback";
            break;
    }
}

@end
