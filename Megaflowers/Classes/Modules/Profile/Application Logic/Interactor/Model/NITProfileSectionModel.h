//
//  NITProfileSectionModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, ProfileSectionType) {
    ProfileSectionTypeDisscont          = 0,
    ProfileSectionTypeCalendar          = 1,
    ProfileSectionTypeFavorite          = 2,
    ProfileSectionTypeShops             = 3,
    ProfileSectionTypePurchaseHistory   = 4,
    ProfileSectionTypePersonalData      = 5,
    ProfileSectionTypePush              = 6,
    ProfileSectionTypeGuide             = 7,
    ProfileSectionTypeFeedback          = 8
};

@interface NITProfileSectionModel : NSObject

@property (nonatomic) ProfileSectionType type;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * iconName;
@property (nonatomic) BOOL enabled;

- (instancetype)initWithType:(ProfileSectionType)type state:(BOOL)state;

@end
