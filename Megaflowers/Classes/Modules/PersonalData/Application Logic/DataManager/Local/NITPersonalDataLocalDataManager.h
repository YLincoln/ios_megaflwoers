//
//  NITPersonalDataLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPersonalDataProtocols.h"

@interface NITPersonalDataLocalDataManager : NSObject <NITPersonalDataLocalDataManagerInputProtocol>

@end
