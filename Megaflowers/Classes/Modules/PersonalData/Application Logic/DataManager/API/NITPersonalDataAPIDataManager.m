//
//  NITPersonalDataAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataAPIDataManager.h"
#import "SWGUserApi.h"

@implementation NITPersonalDataAPIDataManager

- (void)putUserData:(NSDictionary *)userData withHandler:(void (^)(NSError *))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userPutWithName:userData[keyPath(USER.name)]
                               gender:userData[keyPath(USER.gender)]
                                 city:userData[keyPath(USER.cityID)]
                              address:userData[keyPath(USER.address)]
                             birthday:userData[keyPath(USER.birthday)]
                    completionHandler:^(NSError *error) {
                        [HELPER stopLoading];
                        if (error)
                        {
                            [HELPER logError:error method:METHOD_NAME];
                        }
                        handler(error);
                    }];
}

- (void)postUserEmail:(NSString *)email withHandler:(void (^)(NSError *))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEmailPostWithEmail:email completionHandler:^(NSError *error) {
                        [HELPER stopLoading];
                        if (error)
                        {
                            [HELPER logError:error method:METHOD_NAME];
                        }
                        handler(error);
                    }];
}

- (void)getEmailsWithHandler:(void (^)(NSArray<SWGUserEmail *> *output, NSError *))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEmailGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserEmail> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output, error);
    }];
}

- (void)deleteUserEmail:(NSNumber *)emailId withHandler:(void (^)(NSError *))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEmailIdDeleteWithId:emailId completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(error);
    }];
}

@end
