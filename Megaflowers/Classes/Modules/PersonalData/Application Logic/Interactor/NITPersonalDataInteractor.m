//
//  NITPersonalDataInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataInteractor.h"

@implementation NITPersonalDataInteractor

- (void)saveUserDataOnServer:(NSDictionary *)userData withHandler:(void (^)(BOOL, NSString *))handler
{
    [self.APIDataManager putUserData:userData withHandler:^(NSError *error) {
        
        if (error)
        {
            handler(false, error.interfaceDescription);
        }
        else
        {
            handler(true, nil);
        }
    }];
}

- (void)saveUserEmailOnServer:(NSString *)email withHandler:(void (^)(BOOL, NSString *))handler
{
    weaken(self);
    [self.APIDataManager postUserEmail:email withHandler:^(NSError * error) {
        
        if (error)
        {
            handler(false, error.interfaceDescription);
        }
        else
        {
            [weakSelf.APIDataManager getEmailsWithHandler:^(NSArray<SWGUserEmail *> *output, NSError *error) {
                
                if (output.count > 0)
                {
                    dispatch_group_t group = dispatch_group_create();
                    
                    for (SWGUserEmail * emailModel in output)
                    {
                        dispatch_group_enter(group);
                        if (![emailModel.value isEqualToString:email])
                        {
                            [weakSelf.APIDataManager deleteUserEmail:emailModel._id withHandler:^(NSError *error) {
                                dispatch_group_leave(group);
                            }];
                        }
                        else
                        {
                            dispatch_group_leave(group);
                        }
                    }
                    
                    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
                        handler(true, nil);
                    });
                }
                else
                {
                    handler(true, nil);
                }
            }];
        }
        
    }];
}

@end
