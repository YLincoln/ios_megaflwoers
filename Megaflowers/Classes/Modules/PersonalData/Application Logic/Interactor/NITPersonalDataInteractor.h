//
//  NITPersonalDataInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPersonalDataProtocols.h"

@interface NITPersonalDataInteractor : NSObject <NITPersonalDataInteractorInputProtocol>

@property (nonatomic, weak) id <NITPersonalDataInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPersonalDataAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPersonalDataLocalDataManagerInputProtocol> localDataManager;

@end
