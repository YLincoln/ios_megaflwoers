//
//  NITPersonalDataModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataModel.h"
#import "NITPersonalDataCell.h"
#import "NITPersonalDataDateCell.h"
#import "NITPersonalDataDateCell.h"
#import "NITPersonalGenderCell.h"
#import "AKNumericFormatter.h"
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"

static NSString * birthdayFormat = @"dd MMMM yyyy";

@implementation NITPersonalDataModel

- (instancetype)initWithType:(PersonalDataFieldType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

- (void)setType:(PersonalDataFieldType)type
{
    _type = type;
    [self updateData];
}

- (BOOL)isCorrect
{
    switch (self.type)
    {
        case PersonalDataFieldTypePassword:
            return self.value.length > 0;
            
        case PersonalDataFieldTypeEmail:
            return [self isValidEmail];
            
        case PersonalDataFieldTypePhone:
            return [self isValidPhone];
            
        default:
            return true;
    }
    
    return true;
}

- (CGFloat)cellHeight
{
    switch (self.type) {
        case PersonalDataFieldTypeBirthday:
            return 56 + (self.isBirthdayEdit ? 216 : 0);
        default:
            return 56;
    }
}

- (NSString *)cellID
{
    switch (self.type) {
        case PersonalDataFieldTypeBirthday:
            return _s(NITPersonalDataDateCell);
        case PersonalDataFieldTypeGender:
            return _s(NITPersonalGenderCell);
        default:
            return _s(NITPersonalDataCell);
    }
}

- (void)setValue:(NSString *)value
{
    _value = value;
    [self checkEditedState];
}

- (NSDate *)birthday
{
    switch (self.type) {
        case PersonalDataFieldTypeBirthday: return [NSDate dateFromString:self.value format:birthdayFormat];
        default:                            return nil;
    }

}

#pragma mark - Private
- (void)updateData
{
    switch (self.type) {
        case PersonalDataFieldTypeName:
            self.title = NSLocalizedString(@"Имя", nil);
            self.value = USER.name;
            break;

        case PersonalDataFieldTypePhone:
            self.title = NSLocalizedString(@"Телефон", nil);
            self.value = USER.phone;
            break;
            
        case PersonalDataFieldTypeEmail:
            self.title = NSLocalizedString(@"E-mail", nil);
            self.value = USER.email;
            break;
            
        case PersonalDataFieldTypePassword:
            self.title = NSLocalizedString(@"Сменить пароль", nil);
            self.value = self.title;//USER.password;
            break;
            
        case PersonalDataFieldTypeBirthday:
            self.title = NSLocalizedString(@"Дата рождения", nil);
            self.value = [USER.birthday dateStringByLocalFormat:birthdayFormat];
            break;
            
        case PersonalDataFieldTypeGender:
            self.title = NSLocalizedString(@"Пол", nil);
            self.value = @(USER.gender).stringValue;
    }
}

- (NSString * )phoneFormat:(NSString *)phone
{
    return [AKNumericFormatter formatString:phone
                                  usingMask:[@"+" stringByAppendingString:@"* (***) ***-****"]
                       placeholderCharacter:'*'
                                       mode:AKNumericFormatterMixed];
}

- (void)checkEditedState
{
    switch (self.type) {
        case PersonalDataFieldTypeName:
            if(self.value.length == 0 && USER.name.length == 0) {
                self.isEdited = false;
            } else {
                self.isEdited = ![self.value isEqualToString:USER.name];
            }
            break;
            
        case PersonalDataFieldTypePhone:
        {
            NSString * userPhone = [self phoneFormat:USER.phone];
            NSString * selfValue = [self phoneFormat:self.value];
            
            if(self.value.length == 0 && USER.phone.length == 0) {
                self.isEdited = false;
            } else {
                self.isEdited = ![selfValue isEqualToString:userPhone];
            }
        }
            break;
            
        case PersonalDataFieldTypeEmail:
            if(self.value.length == 0 && USER.email.length == 0) {
                self.isEdited = false;
            } else {
                self.isEdited = ![self.value isEqualToString:USER.email];
            }
            break;
            
        case PersonalDataFieldTypePassword:
            self.isEdited = false;
            break;
            
        case PersonalDataFieldTypeBirthday:
            if(self.value.length == 0 && USER.birthday == nil){
                self.isEdited = false;
            } else {
                self.isEdited = ![[NSDate dateFromString:self.value format:@"dd-MM-yyyy"] isEqualToDate:USER.birthday];
            }
            break;
            
        case PersonalDataFieldTypeGender:
            if(self.value.length == 0 && (USER.gender == GenderUnknow || @(USER.gender).stringValue.length == 0)) {
                self.isEdited = false;
            } else {
                self.isEdited = ![self.value isEqualToString:@(USER.gender).stringValue];
            }
            break;
    }
}

- (BOOL)isValidEmail
{
    if (self.value.length == 0)
    {
        return false;
    }
    
    NSString * emailid = self.value;
    NSString * emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate * emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailid];
}

- (BOOL)isValidPhone
{
    NSError * anError = nil;
    NBPhoneNumberUtil * phoneUtil = [NBPhoneNumberUtil new];
    NBPhoneNumber * myNumber = [phoneUtil parse:self.value
                                  defaultRegion:@"RU"
                                          error:&anError];
    
    return [phoneUtil isValidNumber:myNumber];
}

@end
