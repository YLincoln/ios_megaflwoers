//
//  NITPersonalDataModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef CF_ENUM (NSUInteger, PersonalDataFieldType) {
    PersonalDataFieldTypeName       = 0,
    PersonalDataFieldTypeBirthday   = 1,
    PersonalDataFieldTypePhone      = 2,
    PersonalDataFieldTypeEmail      = 3,
    PersonalDataFieldTypePassword   = 4,
    PersonalDataFieldTypeGender     = 5
};

@interface NITPersonalDataModel : NSObject

@property (nonatomic) PersonalDataFieldType type;
@property (nonatomic) NSString * value;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * cellID;
@property (nonatomic) NSDate * birthday;
@property (nonatomic) CGFloat cellHeight;
@property (nonatomic) BOOL selected;
@property (nonatomic) BOOL isHighlighted;
@property (nonatomic) BOOL isCorrect;
@property (nonatomic) BOOL isEdited;
@property (nonatomic) BOOL isBirthdayEdit;

- (instancetype)initWithType:(PersonalDataFieldType)type;

@end
