//
//  NITPersonalDataProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataModel.h"
#import "SWGUserEmail.h"

@protocol NITPersonalDataInteractorOutputProtocol;
@protocol NITPersonalDataInteractorInputProtocol;
@protocol NITPersonalDataViewProtocol;
@protocol NITPersonalDataPresenterProtocol;
@protocol NITPersonalDataLocalDataManagerInputProtocol;
@protocol NITPersonalDataAPIDataManagerInputProtocol;

@class NITPersonalDataWireFrame;

@protocol NITPersonalDataViewProtocol
@required
@property (nonatomic, strong) id <NITPersonalDataPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByFieldModels:(NSArray<NITPersonalDataModel *> *)models;
- (void)reloadEditedSate:(BOOL)edited;
@end

@protocol NITPersonalDataWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITPersonalDataModuleFrom:(id)fromView;
- (void)backActionFrom:(id)fromView;
- (void)showChangePasswordFrom:(id)fromView withDelegate:(id)delegate;
- (void)showCheckSMSFrom:(id)fromView withDelegate:(id)delegate phone:(NSString *)phone;
@end

@protocol NITPersonalDataPresenterProtocol
@required
@property (nonatomic, weak) id <NITPersonalDataViewProtocol> view;
@property (nonatomic, strong) id <NITPersonalDataInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPersonalDataWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)changePassword;
- (void)checkEditedState;
- (void)logout;
- (void)savePersonalData;
- (void)showControllerFrom3DTouch;
@end

@protocol NITPersonalDataInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITPersonalDataInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPersonalDataInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPersonalDataAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPersonalDataLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)saveUserDataOnServer:(NSDictionary *)userData withHandler:(void(^)(BOOL success, NSString *error))handler;
- (void)saveUserEmailOnServer:(NSString *)email withHandler:(void(^)(BOOL success, NSString *error))handler;
@end


@protocol NITPersonalDataDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITPersonalDataAPIDataManagerInputProtocol <NITPersonalDataDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)putUserData:(NSDictionary *)userData withHandler:(void(^)(NSError *error))handler;
- (void)postUserEmail:(NSString *)email withHandler:(void (^)(NSError *))handler;
- (void)getEmailsWithHandler:(void (^)(NSArray<SWGUserEmail *> *output, NSError *))handler;
- (void)deleteUserEmail:(NSNumber *)emailId withHandler:(void (^)(NSError *))handler;
@end

@protocol NITPersonalDataLocalDataManagerInputProtocol <NITPersonalDataDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
