//
//  NITPersonalDataCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataCell.h"
#import "NITPersonalDataModel.h"
#import "AKNumericFormatter.h"

@interface NITPersonalDataCell () <UITextFieldDelegate>

@property (nonatomic) IBOutlet PhoneTextField * textField;

@end

@implementation NITPersonalDataCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)setModel:(NITPersonalDataModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.textField.formatEnabled = false;
    self.textField.textColor = self.model.isHighlighted ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.model.title
                                                                           attributes:@{NSForegroundColorAttributeName:
                                                                                            self.model.isHighlighted ? RGB(217, 67, 67) : RGB(164, 183, 171)}];
    
    switch (self.model.type) {
        case PersonalDataFieldTypePhone: {
            self.textField.enabled = true;
            self.accessoryView = nil;
            self.textField.keyboardType = UIKeyboardTypeNumberPad;
            self.textField.secureTextEntry = false;
            self.textField.formatEnabled = true;
        }
            break;
        case PersonalDataFieldTypeEmail: {
            self.textField.enabled = true;
            self.accessoryView = nil;
            self.textField.keyboardType = UIKeyboardTypeEmailAddress;
            self.textField.secureTextEntry = false;
        }
            break;
        case PersonalDataFieldTypePassword: {
            self.textField.enabled = false;
            self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_right"]];
            self.textField.text = self.model.title;
            }
            break;
        default:
            self.textField.enabled = true;
            self.accessoryView = nil;
            self.textField.keyboardType = UIKeyboardTypeDefault;
            self.textField.secureTextEntry = false;
            break;
    }
    
    self.textField.text = self.model.value;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.textField.textColor = RGB(10, 88, 43);
    
    self.model.isHighlighted = false;
    
    switch (self.model.type) {
        case PersonalDataFieldTypeName: {
            textField.text = [textField.text capitalizedString];
        }
            break;
        default:
            break;
    }

    
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(didStartEditPersonalData)])
    {
        [self.delegate didStartEditPersonalData];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.model.value = textField.text;
    
    if ([self.delegate respondsToSelector:@selector(didEndEditPersonalData)])
    {
        [self.delegate didEndEditPersonalData];
    }
}

@end
