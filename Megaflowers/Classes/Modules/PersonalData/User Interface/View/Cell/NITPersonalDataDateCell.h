//
//  NITPersonalDataDateCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITPersonalDataModel;

@protocol NITPersonalDataDateCellDelegate <NSObject>

- (void)didSelectDate;
- (void)didChangeDate;

@end

@interface NITPersonalDataDateCell : UITableViewCell

@property (nonatomic) NITPersonalDataModel * model;
@property (nonatomic, weak) id <NITPersonalDataDateCellDelegate> delegate;

@end
