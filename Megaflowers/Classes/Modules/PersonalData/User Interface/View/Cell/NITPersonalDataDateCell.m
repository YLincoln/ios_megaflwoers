//
//  NITPersonalDataDateCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataDateCell.h"
#import "NITPersonalDataModel.h"
#import "CMPopTipView.h"

@interface NITPersonalDataDateCell ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet NSLayoutConstraint * titleW;
@property (nonatomic) IBOutlet UIButton * questionBtn;
@property (nonatomic) IBOutlet UIButton * selectBtn;
@property (nonatomic) IBOutlet UIImageView * selectIcon;
@property (nonatomic) IBOutlet UIDatePicker * datePicker;
@property (nonatomic) IBOutlet NSLayoutConstraint * datePickerH;

@end

@implementation NITPersonalDataDateCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initUI];
}

- (void)setModel:(NITPersonalDataModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)dateChangeAction:(id)sender
{
    self.model.value = [self.datePicker.date dateStringByLocalFormat:@"dd MMMM yyyy"];
    self.title.text = self.model.value;
    self.title.textColor = RGB(10, 88, 43);
    self.titleW.constant = [self.title sizeOfMultiLineLabel].width;
    self.questionBtn.hidden = true;
    
    if ([self.delegate respondsToSelector:@selector(didChangeDate)])
    {
        [self.delegate didChangeDate];
    }
}

- (IBAction)selectAction:(id)sender
{
    self.model.isBirthdayEdit = !self.model.isBirthdayEdit;
    [self updateDateIcon];
    
    if ([self.delegate respondsToSelector:@selector(didSelectDate)])
    {
        [self.delegate didSelectDate];
    }
}

- (IBAction)questionAction:(id)sender
{
    CMPopTipView * popTipView  = [[CMPopTipView alloc] initWithCustomView:[[[NSBundle mainBundle] loadNibNamed:@"NITBirthdayHintView" owner:self options:nil] firstObject]];

    popTipView.preferredPointDirection = PointDirectionDown;
    popTipView.hasGradientBackground = false;
    popTipView.hasShadow = true;
    popTipView.backgroundColor = [UIColor whiteColor];
    popTipView.borderColor = [UIColor whiteColor];
    popTipView.textColor = RGB(36, 40, 37);
    popTipView.animation = true;
    popTipView.has3DStyle = false;
    popTipView.dismissTapAnywhere = true;
    
    [popTipView presentPointingAtView:self.questionBtn inView:self.superview animated:true];
}

#pragma mark - Private
- (void)initUI
{
    [self.datePicker setValue:RGB(10, 88, 43) forKeyPath:@"textColor"];
    self.datePicker.hidden = true;
}

- (void)updateData
{
    if ([self.model.value length] == 0 )
    {
        self.title.textColor = RGB(164, 183, 171);
        self.title.text = self.model.title;
    }
    else
    {
        self.title.textColor = RGB(10, 88, 43);
        self.title.text = self.model.value;
    }
    
    self.questionBtn.hidden = true; //![self.title.text isEqualToString:self.model.title];
    
    self.titleW.constant = [self.title sizeOfMultiLineLabel].width;
    self.datePickerH.constant = self.model.isBirthdayEdit ? 216 : 0;
    self.datePicker.hidden = !self.model.isBirthdayEdit;
    
    [self layoutIfNeeded];
    [self updateDateIcon];
}

- (void)updateDateIcon
{
    self.selectIcon.image = [UIImage imageNamed:self.model.isBirthdayEdit ? @"ic_up_cn" : @"ic_down_cn"];
}

@end
