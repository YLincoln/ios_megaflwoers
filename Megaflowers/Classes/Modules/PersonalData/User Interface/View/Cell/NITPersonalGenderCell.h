//
//  NITPersonalGenderCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITPersonalDataModel;

@protocol NITPersonalGenderCellDelegate <NSObject>

- (void)didChangeGender;

@end

@interface NITPersonalGenderCell : UITableViewCell

@property (nonatomic) NITPersonalDataModel * model;
@property (nonatomic, weak) id <NITPersonalGenderCellDelegate> delegate;

@end
