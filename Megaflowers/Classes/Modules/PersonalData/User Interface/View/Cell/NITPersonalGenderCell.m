//
//  NITPersonalGenderCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalGenderCell.h"
#import "NITPersonalDataModel.h"
#import "AKNumericFormatter.h"
#import "UITextField+AKNumericFormatter.h"

@interface NITPersonalGenderCell ()

@property (nonatomic) IBOutlet UITextField * textField;
@property (nonatomic) IBOutlet UIButton * maleBtn;
@property (nonatomic) IBOutlet UIButton * femaleBtn;

@end

@implementation NITPersonalGenderCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITPersonalDataModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - 
- (IBAction)selectFameleAction:(id)sender
{
    self.model.value = @(GenderFamale).stringValue;
    [self updateState];
    
    if ([self.delegate respondsToSelector:@selector(didChangeGender)])
    {
        [self.delegate didChangeGender];
    }
}

- (IBAction)selectMaleAction:(id)sender
{
    self.model.value = @(GenderMale).stringValue;
    [self updateState];
    
    if ([self.delegate respondsToSelector:@selector(didChangeGender)])
    {
        [self.delegate didChangeGender];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.textField.textColor = self.model.isHighlighted ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.model.title
                                                                           attributes:@{NSForegroundColorAttributeName:
                                                                                            self.model.isHighlighted ? RGB(217, 67, 67) : RGB(164, 183, 171)}];
    
    self.textField.enabled = true;
    self.accessoryView = nil;
    
    [self updateState];
}

- (void)updateState
{
    [self.maleBtn setImage:[UIImage imageNamed:[self.model.value integerValue] == GenderMale ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    [self.femaleBtn setImage:[UIImage imageNamed:[self.model.value integerValue] == GenderFamale ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    
    if ([self.model.value integerValue] != GenderUnknow)
    {
        self.textField.text = self.model.title;
    }
}

@end
