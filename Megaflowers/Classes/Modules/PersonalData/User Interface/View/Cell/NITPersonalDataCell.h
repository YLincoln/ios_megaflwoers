//
//  NITPersonalDataCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITPersonalDataModel;

@protocol NITPersonalDataCellDelegate <NSObject>

- (void)didStartEditPersonalData;
- (void)didEndEditPersonalData;

@end

@interface NITPersonalDataCell : UITableViewCell

@property (nonatomic) NITPersonalDataModel * model;
@property (nonatomic, weak) id <NITPersonalDataCellDelegate> delegate;

@end
