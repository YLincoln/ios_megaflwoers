//
//  NITPersonalDataViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITPersonalDataProtocols.h"

@interface NITPersonalDataViewController : NITBaseViewController <NITPersonalDataViewProtocol>

@property (nonatomic, strong) id <NITPersonalDataPresenterProtocol> presenter;

@end
