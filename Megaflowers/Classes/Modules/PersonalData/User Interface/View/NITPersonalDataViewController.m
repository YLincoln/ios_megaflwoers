//
//  NITPersonalDataViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataViewController.h"
#import "NITPersonalDataCell.h"
#import "NITPersonalDataDateCell.h"
#import "NITPersonalGenderCell.h"

@interface NITPersonalDataViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITPersonalDataCellDelegate,
NITPersonalDataDateCellDelegate,
NITPersonalGenderCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIButton * saveBtn;
@property (nonatomic) IBOutlet UIBarButtonItem * logoutBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint *saveBtnH;

@property (nonatomic) NSArray <NITPersonalDataModel *> * items;

@end

@implementation NITPersonalDataViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
    
    [self.presenter showControllerFrom3DTouch];
}

#pragma mark - IBAction
- (IBAction)logoutAction:(id)sender
{
    [self.presenter logout];
}

- (IBAction)saveAction:(id)sender
{
    [self.presenter savePersonalData];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Личные данные", nil);
    self.navigationItem.rightBarButtonItem = nil;
    
    [self setKeyboardActiv:true];
    
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITPersonalDataCell) bundle:nil] forCellReuseIdentifier:_s(NITPersonalDataCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITPersonalDataDateCell) bundle:nil] forCellReuseIdentifier:_s(NITPersonalDataDateCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITPersonalGenderCell) bundle:nil] forCellReuseIdentifier:_s(NITPersonalGenderCell)];
    [self.tableView setTableFooterView:[UIView new]];
    
    self.navigationItem.rightBarButtonItem = [USER isAutorize] ? self.logoutBtn : nil;
}

#pragma mark - NITPersonalDataPresenterProtocol
- (void)reloadDataByFieldModels:(NSArray<NITPersonalDataModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

- (void)reloadEditedSate:(BOOL)edited
{
    self.saveBtnH.constant = edited ? 57 : 0;
    [self.view layoutIfNeeded];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPersonalDataModel * model = self.items[indexPath.row];
    return model.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPersonalDataModel * model = self.items[indexPath.row];
    NITPersonalDataCell * cell = [tableView dequeueReusableCellWithIdentifier:model.cellID forIndexPath:indexPath];
    cell.model = model;
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPersonalDataModel * model = self.items[indexPath.row];
    if (model.type == PersonalDataFieldTypePassword)
    {
        [self.presenter changePassword];
    }
}

#pragma mark - NITPersonalDataCellDelegate
- (void)didStartEditPersonalData
{
    NITPersonalDataModel * model = self.items[PersonalDataFieldTypeBirthday];
    model.isBirthdayEdit = false;
    [self didSelectDate];
}

- (void)didEndEditPersonalData
{
    [self.presenter checkEditedState];
}

#pragma mark - NITPersonalDataDateCellDelegate
- (void)didSelectDate
{
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:PersonalDataFieldTypeBirthday inSection:0]]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)didChangeDate
{
    [self.presenter checkEditedState];
}

#pragma mark - NITPersonalGenderCellDelegate
- (void)didChangeGender
{
    [self.presenter checkEditedState];
}

@end
