//
//  NITPersonalDataPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPersonalDataProtocols.h"

@class NITPersonalDataWireFrame;

@interface NITPersonalDataPresenter : NITRootPresenter <NITPersonalDataPresenterProtocol, NITPersonalDataInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPersonalDataViewProtocol> view;
@property (nonatomic, strong) id <NITPersonalDataInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPersonalDataWireFrameProtocol> wireFrame;

@end
