//
//  NITPersonalDataPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataPresenter.h"
#import "NITPersonalDataWireframe.h"
#import "NITChangePasswordProtocols.h"
#import "NITCheckSMSProtocols.h"

@interface NITPersonalDataPresenter ()
<
NITChangePasswordDelegate,
NITCheckSMSDelegate
>

@property (nonatomic) NITPersonalDataModel * genderModel;
@property (nonatomic) NITPersonalDataModel * nameModel;
@property (nonatomic) NITPersonalDataModel * birthdayModel;
@property (nonatomic) NITPersonalDataModel * phoneModel;
@property (nonatomic) NITPersonalDataModel * emailModel;
@property (nonatomic) NITPersonalDataModel * passwordModel;

@property (nonatomic) NSArray <NITPersonalDataModel *> * models;

@end

@implementation NITPersonalDataPresenter

- (void)initData
{
    self.genderModel        = [[NITPersonalDataModel alloc] initWithType:PersonalDataFieldTypeGender];
    self.nameModel          = [[NITPersonalDataModel alloc] initWithType:PersonalDataFieldTypeName];
    self.emailModel         = [[NITPersonalDataModel alloc] initWithType:PersonalDataFieldTypeEmail];
    self.birthdayModel      = [[NITPersonalDataModel alloc] initWithType:PersonalDataFieldTypeBirthday];
    self.phoneModel         = [[NITPersonalDataModel alloc] initWithType:PersonalDataFieldTypePhone];
    self.passwordModel      = USER.password.length > 0 ? [[NITPersonalDataModel alloc] initWithType:PersonalDataFieldTypePassword] : nil;
    
    NSMutableArray * models = [NSMutableArray arrayWithArray:@[self.nameModel, self.birthdayModel, self.phoneModel, self.emailModel]];
    
    if (self.passwordModel)
    {
        [models addObject:self.passwordModel];
    }
    
    [models addObject:self.genderModel];
    
    self.models = models;
    [self.view reloadDataByFieldModels:self.models];
}

- (void)checkEditedState
{
    [self.view reloadEditedSate:[self isEditedProfile]];
}

- (void)logout
{
    [USER logout];
    
    [self.wireFrame backActionFrom:self.view];
}

- (void)savePersonalData
{
    if ([self saveAvailable])
    {
        if (self.phoneModel.isEdited)
        {
            [self chechPhoneBySms];
        }
        else
        {
            weaken(self);
            [self saveDataWithHandler:^(BOOL success) {
                if (success)
                {
                    [weakSelf.wireFrame backActionFrom:weakSelf.view];
                }
            }];
        }
    }
    else
    {
        [self.view reloadDataByFieldModels:self.models];
    }
}

- (void)chechPhoneBySms
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ok", nil) actionBlock:^{
        [weakSelf.wireFrame showCheckSMSFrom:self.view withDelegate:self phone:self.phoneModel.value];
    }];
    
    [HELPER showActionSheetFromView:self.view
                          withTitle:NSLocalizedString(@"При смене номера телефона необходимо подтвердить новый номер", nil)
                      actionButtons:@[action1] cancelButton:cancel];
}

- (void)changePassword
{
    [self.wireFrame showChangePasswordFrom:self.view withDelegate:self];
}

#pragma mark - Private
- (BOOL)isEditedProfile
{
    if ([self.genderModel isEdited])
    {
        return true;
    }
    
    for (NITPersonalDataModel * model in self.models)
    {
        if ([model isEdited])
        {
            return true;
        }
    }
    
    return false;
}

- (BOOL)saveAvailable
{
    BOOL available = true;
    
    if (!self.nameModel.isCorrect)
    {
        self.nameModel.isHighlighted = true;
        available = false;
    }
    
    if (!self.birthdayModel.isCorrect)
    {
        self.birthdayModel.isHighlighted = true;
        available = false;
    }
    
    if (self.phoneModel.isEdited && !self.phoneModel.isCorrect)
    {
        self.phoneModel.isHighlighted = true;
        available = false;
    }
    
    if (!self.emailModel.isCorrect)
    {
        self.emailModel.isHighlighted = true;
        available = false;
    }
    
    return available;
}

- (void)saveDataWithHandler:(void(^)(BOOL success))handler
{
    NSMutableDictionary * userData = [NSMutableDictionary new];

    if (self.nameModel.value)           [userData setObject:self.nameModel.value forKey:keyPath(USER.name)];
    if (self.genderModel.value)         [userData setObject:@(self.genderModel.value.integerValue) forKey:keyPath(USER.gender)];
    if (self.birthdayModel.birthday)    [userData setObject:[self.birthdayModel.birthday serverString] forKey:keyPath(USER.birthday)];
    if (USER.cityID)                    [userData setObject:USER.cityID forKey:keyPath(USER.cityID)];
    
    weaken(self);
    [self.interactor saveUserDataOnServer:userData withHandler:^(BOOL success, NSString *error) {
        
        if (success)
        {
            USER.gender     = [weakSelf.genderModel.value integerValue];
            USER.name       = weakSelf.nameModel.value;
            USER.birthday   = weakSelf.birthdayModel.birthday;
            USER.phone      = weakSelf.phoneModel.value;
            USER.password   = weakSelf.passwordModel.value;
            
            if (self.emailModel.value.length > 0 && ![self.emailModel.value isEqualToString:USER.email])
            {
                [self.interactor saveUserEmailOnServer:self.emailModel.value withHandler:^(BOOL success, NSString *error) {
                    
                    if (success)
                    {
                        USER.email = self.emailModel.value;
                        handler(true);
                    }
                    else
                    {
                        [weakSelf showAlertError:error];
                        handler(false);
                    }
                }];
            }
            else
            {
                handler(true);
            }
        }
        else
        {
            [weakSelf showAlertError:error];
            handler(false);
        }
    }];
}

#pragma mark - NITChangePasswordDelegate
- (void)didChangePassword:(NSString *)password
{
    self.passwordModel.value = password;
    [self.view reloadEditedSate:[self isEditedProfile]];
}

#pragma mark - NITCheckSMSDelegate
- (void)didCheckSMS:(BOOL)success
{
    if (success)
    {
        weaken(self);
        [self saveDataWithHandler:^(BOOL success) {
            if (success)
            {
                [weakSelf.wireFrame backActionFrom:weakSelf.view];
            }
        }];
    }
    else
    {
        self.phoneModel.isHighlighted = true;
        [self.view reloadDataByFieldModels:self.models];
    }
}

#pragma mark - 3D Touch
- (void)showControllerFrom3DTouch
{
    if([Shortcut isCreatedShortcut])
    {
        if([Shortcut isAvailableShortcutByTag:@"Room"])
        {
            [Shortcut deleteShortcut];
        }
    }
}

#pragma mark - Private
- (void)showAlertError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ok", nil) actionBlock:^{}];
    [HELPER showActionSheetFromView:self.view withTitle:error actionButtons:@[] cancelButton:cancel];
}

@end
