//
//  NITPersonalDataWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataWireFrame.h"
#import "NITChangePasswordWireFrame.h"
#import "NITCheckSMSWireFrame.h"

@implementation NITPersonalDataWireFrame

+ (id)createNITPersonalDataModule
{
    // Generating module components
    id <NITPersonalDataPresenterProtocol, NITPersonalDataInteractorOutputProtocol> presenter = [NITPersonalDataPresenter new];
    id <NITPersonalDataInteractorInputProtocol> interactor = [NITPersonalDataInteractor new];
    id <NITPersonalDataAPIDataManagerInputProtocol> APIDataManager = [NITPersonalDataAPIDataManager new];
    id <NITPersonalDataLocalDataManagerInputProtocol> localDataManager = [NITPersonalDataLocalDataManager new];
    id <NITPersonalDataWireFrameProtocol> wireFrame= [NITPersonalDataWireFrame new];
    id <NITPersonalDataViewProtocol> view = [(NITPersonalDataWireFrame *)wireFrame createViewControllerWithKey:_s(NITPersonalDataViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITPersonalDataModuleFrom:(UIViewController *)fromViewController
{
    NITPersonalDataViewController * view = [NITPersonalDataWireFrame createNITPersonalDataModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromView
{
    [fromView.navigationController popViewControllerAnimated:true];
}

- (void)showChangePasswordFrom:(UIViewController *)fromView withDelegate:(id)delegate
{
    [NITChangePasswordWireFrame presentNITChangePasswordModuleFrom:fromView withDelegate:delegate];
}

- (void)showCheckSMSFrom:(UIViewController *)fromView withDelegate:(id)delegate phone:(NSString *)phone
{
    [NITCheckSMSWireFrame presentNITCheckSMSModuleFrom:fromView
                                          withDelegate:delegate
                                                  mode:SMSRecoveryModeActivateAccount
                                                 phone:phone
                                                 token:nil];
}

@end
