//
//  NITPersonalDataWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/25/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPersonalDataProtocols.h"
#import "NITPersonalDataViewController.h"
#import "NITPersonalDataLocalDataManager.h"
#import "NITPersonalDataAPIDataManager.h"
#import "NITPersonalDataInteractor.h"
#import "NITPersonalDataPresenter.h"
#import "NITPersonalDataWireframe.h"
#import "NITRootWireframe.h"

@interface NITPersonalDataWireFrame : NITRootWireframe <NITPersonalDataWireFrameProtocol>

@end
