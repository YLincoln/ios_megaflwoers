//
//  NITCreateEventAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateEventAPIDataManager.h"
#import "SWGUserApi.h"

@implementation NITCreateEventAPIDataManager

- (void)getUserEventsWithHandler:(void(^)(NSArray <SWGUserEvent> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEventGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserEvent> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

- (void)getUserEventByID:(NSNumber *)eventID withHandler:(void(^)(SWGUserEvent *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEventIdGetWithLang:HELPER.lang site:API.site city:USER.cityID _id:eventID completionHandler:^(NSArray<SWGUserEvent> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output.firstObject);
    }];
}

- (void)createByModel:(NITNewEventModel *)model withHandler:(void(^)(SWGUserEvent *result))handler
{
    weaken(self);
    [HELPER startLoading];
    [[SWGUserApi new] userEventPostWithName:model.title
                                       date:model.date
                                   periodId:@(model.repeat)
                                   remindId:@0
                                       text:model.contact
                                      photo:model.imageUrl
                                     typeId:@(NITEventTypeUser)
                          completionHandler:^(NSNumber *output, NSError *error) {
                              [HELPER stopLoading];
                              if (error)
                              {
                                  [HELPER logError:error method:METHOD_NAME];
                                  handler(nil);
                              }
                              else
                              {
                                  [weakSelf getUserEventByID:output withHandler:handler];
                              }
                          }];
}

- (void)editeByModel:(NITNewEventModel *)model withHandler:(void (^)(SWGUserEvent *))handler
{
    weaken(self);
    [HELPER startLoading];
    NSNumber * eventID = @([model.identifier integerValue]);
    [[SWGUserApi new] userEventIdPutWithId:eventID
                                      date:model.date
                                  periodId:@(model.repeat)
                                  remindId:@0
                                      name:model.title
                                      text:model.contact
                         completionHandler:^(NSError *error) {
                             
                             [HELPER stopLoading];
                             
                             if (error)
                             {
                                 [HELPER logError:error method:METHOD_NAME];
                                 handler(nil);
                             }
                             else
                             {
                                 if (model.imageUrl)
                                 {
                                     [weakSelf updateEventPhoto:model.imageUrl byEventID:eventID withHandler:^{
                                         [weakSelf getUserEventByID:eventID withHandler:handler];
                                     }];
                                 }
                                 else
                                 {
                                     [weakSelf getUserEventByID:eventID withHandler:handler];
                                 }
                             }
                         }];
}

- (void)updateEventPhoto:(NSURL *)url byEventID:(NSNumber *)eventID withHandler:(void (^)())handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEventIdUpdatePhotoPostWithId:eventID photo:url completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        handler();
    }];
}

@end
