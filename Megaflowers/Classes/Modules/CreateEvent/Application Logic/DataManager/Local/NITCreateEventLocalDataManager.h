//
//  NITCreateEventLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCreateEventProtocols.h"

@interface NITCreateEventLocalDataManager : NSObject <NITCreateEventLocalDataManagerInputProtocol>

@end
