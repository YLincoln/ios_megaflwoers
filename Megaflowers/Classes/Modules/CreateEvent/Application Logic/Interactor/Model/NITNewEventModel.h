//
//  NITNewEventModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITNewEventModel : NSObject

@property (nonatomic) NSString * identifier;
@property (nonatomic) UIImage * placeholder;
@property (nonatomic) NSURL * imageUrl;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * contact;
@property (nonatomic) NSDate * date;
@property (nonatomic) NSString * repeatTitle;
@property (nonatomic) NITEventRepeatType repeat;
@property (nonatomic) BOOL local;

@property (nonatomic) BOOL titleHighlight;
@property (nonatomic) BOOL contactHighlight;
@property (nonatomic) BOOL dateHighlight;
@property (nonatomic) BOOL isCorrect;

@end
