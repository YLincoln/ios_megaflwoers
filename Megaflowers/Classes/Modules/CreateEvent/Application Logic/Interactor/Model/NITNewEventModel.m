//
//  NITNewEventModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITNewEventModel.h"

@implementation NITNewEventModel

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.repeat = NITEventRepeatTypeDisabled;
        self.local = true;
    }
    return self;
}

- (NSString *)repeatTitle
{
    switch (self.repeat) {
        case NITEventRepeatTypeNever:
            return NSLocalizedString(@"Никогда", nil);
        case NITEventRepeatTypeEveryMonth:
            return NSLocalizedString(@"Каждый месяц", nil);
        case NITEventRepeatTypeEveryYear:
            return NSLocalizedString(@"Каждый год", nil);
        default:
            return nil;
    }
}

- (BOOL)isCorrect
{
    BOOL correct = true;
    
    if ([self.title length] == 0)
    {
        self.titleHighlight = true;
        correct = false;
    }
    
    if ([self.contact length] == 0)
    {
        self.contactHighlight = true;
        correct = false;
    }
    
    if (self.date == nil)
    {
        self.dateHighlight = true;
        correct = false;
    }
    
    return correct;
}

@end
