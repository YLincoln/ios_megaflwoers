//
//  NITCreateEventInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCreateEventProtocols.h"

@interface NITCreateEventInteractor : NSObject <NITCreateEventInteractorInputProtocol>

@property (nonatomic, weak) id <NITCreateEventInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCreateEventAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCreateEventLocalDataManagerInputProtocol> localDataManager;

@end
