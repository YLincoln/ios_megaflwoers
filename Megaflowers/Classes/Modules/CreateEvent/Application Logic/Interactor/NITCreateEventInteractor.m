//
//  NITCreateEventInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateEventInteractor.h"

@implementation NITCreateEventInteractor

- (void)createActionByModel:(NITNewEventModel *)model
{
    weaken(self);
    [self.APIDataManager createByModel:model withHandler:^(SWGUserEvent *result) {
        if (result)
        {            
            [NITEvent syncServerEvents:@[result]];
            [weakSelf.presenter saveEventSuccess:true];
        }
        else
        {
            [weakSelf.presenter saveEventSuccess:false];
        }
    }];
}

- (void)saveActionByModel:(NITNewEventModel *)model
{
    if (model.local)
    {
        [self.localDataManager editeByModel:model];
        [self.presenter saveEventSuccess:true];
    }
    else
    {
        weaken(self);
        [self.APIDataManager editeByModel:model withHandler:^(SWGUserEvent *result) {
            if (result)
            {
                [NITEvent syncServerEvents:@[result]];
                [weakSelf.presenter saveEventSuccess:true];
            }
            else
            {
                [weakSelf.presenter saveEventSuccess:false];
            }
        }];
    }
}

@end
