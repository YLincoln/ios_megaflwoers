//
//  NITCreateEventPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateEventPresenter.h"
#import "NITCreateEventWireframe.h"
#import "APAddressBook.h"
#import "APContact.h"
#import "NSArray+BlocksKit.h"
#import "UIImage+Saving.h"
#import "UIImage+Resizing.h"

@interface NITCreateEventPresenter ()

@property (nonatomic) NITNewEventModel * model;

@end

@implementation NITCreateEventPresenter

- (void)initData
{
    self.model = [NITNewEventModel new];
    
    switch (self.eventMode) {
        case EventModeCreate:
            self.model.date = self.createdDate;
            break;
        case EventModeEdit:
            self.model.identifier = self.editedEvent.identifier;
            self.model.imageUrl = self.editedEvent.imageURL;
            self.model.title = self.editedEvent.typeTitle;
            self.model.contact = self.editedEvent.title;
            self.model.date = self.editedEvent.startDate;
            self.model.repeat = self.editedEvent.repeatType;
            self.model.local = self.editedEvent.local;
            self.model.placeholder = self.editedEvent.placeholder;
            
            [self.view reloadEditedState:false];
            break;
    }

    [self.view reloadDataByModel:self.model];
}

- (void)checkEditedState
{
    if (self.eventMode == EventModeCreate)
    {
        return;
    }
    
    BOOL edited = false;
    
    if (![self.model.imageUrl.absoluteString isEqualToString:self.editedEvent.imageURL.absoluteString])
    {
        edited = true;
        [self.view reloadEditedState:edited];
        return;
    }
    
    if (![self.model.title isEqualToString:self.editedEvent.typeTitle])
    {
        edited = true;
        [self.view reloadEditedState:edited];
        return;
    }
    
    if (![self.model.contact isEqualToString:self.editedEvent.title])
    {
        edited = true;
        [self.view reloadEditedState:edited];
        return;
    }
    
    if (![self.model.date isEqualToDate:self.editedEvent.startDate])
    {
        edited = true;
        [self.view reloadEditedState:edited];
        return;
    }
    
    if (self.model.repeat != self.editedEvent.repeatType)
    {
        edited = true;
        [self.view reloadEditedState:edited];
        return;
    }
}

- (NSString *)formattedDate
{
    NSString * format = self.model.date.year == 4 ? @"d LLLL" : @"d LLLL yyyy";
    return [self.model.date dateStringByLocalFormat:format];
}

- (void)selectContact
{
    __block BOOL access = [APAddressBook access] == APAddressBookAccessGranted;
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    
    if (!access)
    {
        APAddressBook * addressBook = [[APAddressBook alloc] init];
        [addressBook requestAccessOnQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) completion:^(BOOL granted, NSError * _Nullable error) {
            access = granted;
            dispatch_group_leave(group);
        }];
    }
    else
    {
        dispatch_group_leave(group);
    }
    
    weaken(self);
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        [weakSelf.wireFrame selectPhoneNumberFrom:weakSelf.view withHandler:^(NSString * phoneNumber) {
            
            APAddressBook * addressBook = [[APAddressBook alloc] init];
            addressBook.fieldsMask = APContactFieldDefault | APContactFieldThumbnail | APContactFieldBirthday;
            [addressBook loadContacts:^(NSArray<APContact *> * _Nullable contacts, NSError * _Nullable error) {
                
                APContact * contact = [[contacts bk_select:^BOOL(APContact * obj) {
                    return [[obj.phones bk_map:^id(APPhone * obj) {
                        return obj.number;
                    }] bk_select:^BOOL(NSString * obj) {
                        return [obj isEqualToString:phoneNumber];
                    }].count > 0;
                    
                }] firstObject];
                
                weakSelf.model.contact = contact.name.compositeName;
                
                if (contact.thumbnail)
                {
                    NSString * key = [NSString stringWithFormat:@"%@_%@",
                                      contact.recordID.stringValue,
                                      [contact.name.compositeName stringByReplacingOccurrencesOfString:@" " withString:@""]];
                    weakSelf.model.imageUrl = [NSURL URLWithString:[HELPER saveImage:contact.thumbnail forKey:key]];
                }
                
                if (contact.birthday)
                {
                    weakSelf.model.date = contact.birthday;
                    weakSelf.model.title = NSLocalizedString(@"День Рождения", nil);
                }
                
                [weakSelf.view reloadDataByModel:weakSelf.model];
            }];
        }];
    });
}

- (void)selectPhoto
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSString * action1Title = NSLocalizedString(@"Выбрать из галереи", nil);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:action1Title actionBlock:^{
        [weakSelf.wireFrame selectPhotoFromView:weakSelf.view withHandler:^(UIImage *image, NSURL *url) {
            UIImage * scaledImage = [[image imageFixOrientation] scaleToFitSize:CGSizeMake(800, 800)];
            weakSelf.model.imageUrl = [HELPER saveImage:scaledImage byAssetUrl:url];
            [weakSelf.view reloadDataByModel:weakSelf.model];
        }];
    }];
    
    NSString * action2Title = NSLocalizedString(@"Сделать фото", nil);
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:action2Title actionBlock:^{
        [weakSelf.wireFrame takePhotoFromView:weakSelf.view withHandler:^(UIImage *image, NSURL *url) {
            
            UIImage * scaledImage = [[image imageFixOrientation] scaleToFitSize:CGSizeMake(800, 800)];
            weakSelf.model.imageUrl = [HELPER saveImage:scaledImage byAssetUrl:url];
            [weakSelf.view reloadDataByModel:weakSelf.model];
        }];
    }];

    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1,action2] cancelButton:cancel];
}

- (void)selectRepaet
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSString * action1Title = NSLocalizedString(@"Никогда", nil);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:action1Title actionBlock:^{
        weakSelf.model.repeat = NITEventRepeatTypeNever;
        [weakSelf.view reloadDataByModel:weakSelf.model];
    }];
    
    NSString * action2Title = NSLocalizedString(@"Каждый месяц", nil);
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:action2Title actionBlock:^{
        weakSelf.model.repeat = NITEventRepeatTypeEveryMonth;
        [weakSelf.view reloadDataByModel:weakSelf.model];
    }];
    
    NSString * action3Title = NSLocalizedString(@"Каждый год", nil);
    NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:action3Title actionBlock:^{
        weakSelf.model.repeat = NITEventRepeatTypeEveryYear;
        [weakSelf.view reloadDataByModel:weakSelf.model];
    }];

    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1,action2,action3] cancelButton:cancel];
}

- (void)createAction
{
    if ([self.model isCorrect])
    {
        if (self.model.repeat == NITEventRepeatTypeDisabled)
        {
            self.model.repeat = NITEventRepeatTypeNever;
        }
        
        [TRACKER trackEvent:TrackerEventCalendarEventCreate];
        [self.interactor createActionByModel:self.model];
    }
    else
    {
        [self.view reloadDataByModel:self.model];
    }
}

- (void)saveAction
{
    if ([self.model isCorrect])
    {
        [self.interactor saveActionByModel:self.model];
    }
    else
    {
        [self.view reloadDataByModel:self.model];
    }
}

#pragma mark - NITCreateEventInteractorOutputProtocol
- (void)saveEventSuccess:(BOOL)success
{
    [self.wireFrame backActionFrom:self.view];
}

@end
