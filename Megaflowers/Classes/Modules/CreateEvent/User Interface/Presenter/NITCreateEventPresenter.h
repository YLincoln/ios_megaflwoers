//
//  NITCreateEventPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCreateEventProtocols.h"

@class NITCreateEventWireFrame;

@interface NITCreateEventPresenter : NITRootPresenter <NITCreateEventPresenterProtocol, NITCreateEventInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCreateEventViewProtocol> view;
@property (nonatomic, strong) id <NITCreateEventInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCreateEventWireFrameProtocol> wireFrame;
@property (nonatomic) NITEventModel * editedEvent;
@property (nonatomic) EventMode eventMode;
@property (nonatomic) NSDate * createdDate;

@end
