//
//  NITCreateEventWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateEventProtocols.h"
#import "NITCreateEventViewController.h"
#import "NITCreateEventLocalDataManager.h"
#import "NITCreateEventAPIDataManager.h"
#import "NITCreateEventInteractor.h"
#import "NITCreateEventPresenter.h"
#import "NITCreateEventWireframe.h"
#import "NITRootWireframe.h"

@interface NITCreateEventWireFrame : NITRootWireframe <NITCreateEventWireFrameProtocol>

@property (nonatomic) SelectContactBlock contactBlock;
@property (nonatomic) SelectPhotoBlock photoBlock;

@end
