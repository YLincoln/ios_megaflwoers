//
//  NITCreateEventWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateEventWireFrame.h"
#import <ContactsUI/ContactsUI.h>
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>

@interface NITCreateEventWireFrame ()
<
CNContactPickerDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
ABPeoplePickerNavigationControllerDelegate
>
{
    UIImagePickerController * _imagePickerController;
}

@end

@implementation NITCreateEventWireFrame

+ (id)createNITCreateEventModule
{
    // Generating module components
    id <NITCreateEventPresenterProtocol, NITCreateEventInteractorOutputProtocol> presenter = [NITCreateEventPresenter new];
    id <NITCreateEventInteractorInputProtocol> interactor = [NITCreateEventInteractor new];
    id <NITCreateEventAPIDataManagerInputProtocol> APIDataManager = [NITCreateEventAPIDataManager new];
    id <NITCreateEventLocalDataManagerInputProtocol> localDataManager = [NITCreateEventLocalDataManager new];
    id <NITCreateEventWireFrameProtocol> wireFrame= [NITCreateEventWireFrame new];
    id <NITCreateEventViewProtocol> view = [(NITCreateEventWireFrame *)wireFrame createViewControllerWithKey:_s(NITCreateEventViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITCreateEventModuleFrom:(UIViewController *)fromViewController withDate:(NSDate *)date
{
    NITCreateEventViewController * view = [NITCreateEventWireFrame createNITCreateEventModule];
    view.presenter.eventMode = EventModeCreate;
    view.presenter.createdDate = date;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITEditEventModuleFrom:(UIViewController *)fromViewController withEvent:(NITEventModel *)model
{
    NITCreateEventViewController * view = [NITCreateEventWireFrame createNITCreateEventModule];
    view.presenter.editedEvent = model;
    view.presenter.eventMode = EventModeEdit;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromView
{
    [fromView.navigationController popViewControllerAnimated:true];
}

#pragma mark - Photo
- (void)selectPhotoFromView:(UIViewController *)fromView withHandler:(SelectPhotoBlock)handler
{
    self.photoBlock = handler;
    [self showImagePickerControllerWithSouceType:UIImagePickerControllerSourceTypePhotoLibrary fromView:fromView];
}

- (void)takePhotoFromView:(UIViewController *)fromView withHandler:(SelectPhotoBlock)handler
{
    self.photoBlock = handler;
    [self showImagePickerControllerWithSouceType:UIImagePickerControllerSourceTypeCamera fromView:fromView];
}

#pragma mark - ImagePickerControllerDataSouce
- (void)showImagePickerControllerWithSouceType:(UIImagePickerControllerSourceType)type fromView:(UIViewController *)fromView
{
    if ([UIImagePickerController isSourceTypeAvailable:type])
    {
        if (!_imagePickerController)
        {
            _imagePickerController = [[UIImagePickerController alloc] init];
            _imagePickerController.delegate = self;
            _imagePickerController.navigationBar.translucent = false;
        }
        
        _imagePickerController.sourceType = type;
        
        [fromView presentViewController:_imagePickerController animated:true completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"imagePickerError", nil)
                                    message:nil
                                   delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:@"OK", nil]
         show];
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:true completion:NULL];
    if (self.photoBlock) self.photoBlock([info objectForKey:UIImagePickerControllerOriginalImage], [info objectForKey:UIImagePickerControllerReferenceURL]);
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:true completion:NULL];
}

#pragma mark - Adress book
- (void)selectPhoneNumberFrom:(UIViewController *)fromViewController withHandler:(SelectContactBlock)handler
{
    self.contactBlock = handler;
    
    if (IOS_8)
    {
        ABPeoplePickerNavigationController * peoplePicker = [ABPeoplePickerNavigationController new];
        peoplePicker.displayedProperties = @[@(kABPersonPhoneProperty)];
        peoplePicker.peoplePickerDelegate = self;
        [fromViewController.navigationController presentViewController:peoplePicker animated:true completion:nil];
    }
    else
    {
        CNContactPickerViewController * contactPicker = [[CNContactPickerViewController alloc] init];
        contactPicker.delegate = self;
        
        NSArray * propertyKeys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactOrganizationNameKey, CNContactPostalAddressesKey, CNContactImageDataKey, CNContactThumbnailImageDataKey, CNContactImageDataAvailableKey, CNContactUrlAddressesKey];
        
        contactPicker.displayedPropertyKeys = propertyKeys;
        
        [fromViewController presentViewController:contactPicker animated:true completion:nil];
    }
}

#pragma mark CNContactPickerDelegate methods
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSArray <CNLabeledValue<CNPhoneNumber *> *> * phoneNumbers = contact.phoneNumbers;
    CNLabeledValue<CNPhoneNumber *> * firstPhone = [phoneNumbers firstObject];
    NSString * phone = [firstPhone.value stringValue];
    
    [picker dismissViewControllerAnimated:true completion:^{
        
        if (self.contactBlock)
        {
            self.contactBlock(phone);
        }
    }];
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    NSString * phone = [contactProperty.value stringValue];
    
    [picker dismissViewControllerAnimated:true completion:^{
        
        if (self.contactBlock)
        {
            self.contactBlock(phone);
        }
    }];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
    [peoplePicker dismissViewControllerAnimated:YES completion:^{}];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    NSString * phone;
    if (property == kABPersonPhoneProperty)
    {
        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, property);
        if (ABMultiValueGetCount(phoneNumbers) > 0)
        {
            CFIndex index = ABMultiValueGetIndexForIdentifier(phoneNumbers, identifier);
            phone = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, index);
        }
    }
    
    if (self.contactBlock)
    {
        self.contactBlock(phone);
    }
}

@end
