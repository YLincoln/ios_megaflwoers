//
//  NITCreateEventViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateEventViewController.h"
#import "AKNumericFormatter.h"
#import "UIImageView+AFNetworking.h"

@interface NITCreateEventViewController ()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UIScrollView * scrollView;
@property (nonatomic) IBOutlet UIImageView * eventImage;
@property (nonatomic) IBOutlet UILabel * eventImageText;
@property (nonatomic) IBOutlet UITextField * nameField;
@property (nonatomic) IBOutlet UITextField * contactField;
@property (nonatomic) IBOutlet UITextField * dateField;
@property (nonatomic) IBOutlet UIImageView * dateIcon;
@property (nonatomic) IBOutlet NSLayoutConstraint * datePickerH;
@property (nonatomic) IBOutlet UIDatePicker * datePicker;
@property (nonatomic) IBOutlet UITextField * repeatField;
@property (nonatomic) IBOutlet UIBarButtonItem * saveBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * createBtnH;

@property (nonatomic) NITNewEventModel * model;

@end

@implementation NITCreateEventViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)selectFotoAction:(id)sender
{
    [self closeSelectDateIfNeed];
    [self.presenter selectPhoto];
}

- (IBAction)selectDateAction:(id)sender
{
    self.datePicker.hidden = !self.datePicker.hidden;
    self.datePickerH.constant = self.datePicker.hidden ? 0 : 216;

    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self dateChangeAction:nil];
    [self updateDateIcon];

    [self.scrollView scrollRectToVisible:CGRectMake(self.scrollView.contentSize.width - 1, self.scrollView.contentSize.height - 1, 1, 1) animated:true];
}

- (IBAction)dateChangeAction:(id)sender
{
    self.model.dateHighlight = false;
    self.dateField.textColor = RGB(10, 88, 43);
    self.model.date = self.datePicker.date;
    self.dateField.text = [self.presenter formattedDate];
    [self.presenter checkEditedState];
}

- (IBAction)selectRepeatAction:(id)sender
{
    [self closeSelectDateIfNeed];
    [self.presenter selectRepaet];
}

- (IBAction)selectContactAction:(id)sender
{
    [self closeSelectDateIfNeed];
    [self.presenter selectContact];
}

- (IBAction)createAction:(id)sender
{
    [self closeSelectDateIfNeed];
    [self.presenter createAction];
}

- (IBAction)saveAction:(id)sender
{
    [self closeSelectDateIfNeed];
    [self.presenter saveAction];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Новое событие", nil);
    
    [self setKeyboardActiv:true];
    
    self.eventImage.layer.borderWidth = 1;
    self.eventImage.layer.borderColor = RGB(201, 205, 203).CGColor;
    
    [self.datePicker setValue:RGB(10, 88, 43) forKeyPath:@"textColor"];
    
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                              attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    self.dateField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.dateField.placeholder
                                                                          attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    self.contactField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.contactField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    self.repeatField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.repeatField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    switch (self.presenter.eventMode) {
        case EventModeCreate:
            break;
        case EventModeEdit:
            self.createBtnH.constant = 0;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
            break;
    }
}

- (void)updateDateIcon
{
    self.dateIcon.image = [UIImage imageNamed:self.datePicker.hidden ? @"ic_down_cn" :  @"ic_up_cn"];
}

- (void)updateModelByField:(UITextField *)textField
{
    textField.text = [textField.text capitalizedString];
    
    switch (textField.tag) {
        case 101:
            self.model.title = textField.text;
            break;
        case 102:
            self.model.contact = textField.text;
            break;
    }
    
    [self.presenter checkEditedState];
}

- (void)closeSelectDateIfNeed
{
    if (!self.datePicker.hidden)
    {
        [self selectDateAction:nil];
    }
}

#pragma mark - NITCreateEventPresenterProtocol
- (void)reloadDataByModel:(NITNewEventModel *)model
{
    self.model = model;

    weaken(self);
    [self.eventImage setImageWithURLRequest:[NSURLRequest requestWithURL:self.model.imageUrl]
                           placeholderImage:self.model.placeholder
                                    success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                        if (image)
                                        {
                                            weakSelf.eventImage.image = image;
                                        }
                                        weakSelf.eventImageText.hidden = true;
                                    }
                                    failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                        weakSelf.eventImageText.hidden = false;
                                    }];
    
    self.nameField.text = self.model.title;
    self.contactField.text = self.model.contact;
    self.dateField.text = [self.presenter formattedDate];
    
    if (model.repeat != NITEventRepeatTypeDisabled)
    {
        self.repeatField.text = self.model.repeatTitle;
    }

    self.nameField.textColor = self.model.titleHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    UIColor * nameColor = self.model.titleHighlight ? RGB(217, 67, 67) : RGB(164, 183, 171);
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:nameColor}];
    
    self.contactField.textColor = self.model.contactHighlight ?  RGB(217, 67, 67) : RGB(10, 88, 43);
    UIColor * contactColor = self.model.contactHighlight ? RGB(217, 67, 67) : RGB(164, 183, 171);
    self.contactField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.contactField.placeholder
                                                                              attributes:@{NSForegroundColorAttributeName:contactColor}];
    
    self.dateField.textColor = self.model.dateHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    UIColor * dateColor = self.model.dateHighlight ? RGB(217, 67, 67) : RGB(164, 183, 171);
    self.dateField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.dateField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:dateColor}];
    
    [self.presenter checkEditedState];
}

- (void)reloadEditedState:(BOOL)edited
{
    self.navigationItem.rightBarButtonItem = edited ? self.saveBtn : nil;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    textField.textColor = RGB(10, 88, 43);
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder
                                                                      attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self closeSelectDateIfNeed];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateModelByField:textField];
}

@end
