//
//  NITCreateEventViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITCreateEventProtocols.h"

@interface NITCreateEventViewController : NITBaseViewController <NITCreateEventViewProtocol>

@property (nonatomic, strong) id <NITCreateEventPresenterProtocol> presenter;

@end
