//
//  NITCreateEventProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/30/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITNewEventModel.h"
#import "NITEventModel.h"
#import "SWGUserEvent.h"

@protocol NITCreateEventInteractorOutputProtocol;
@protocol NITCreateEventInteractorInputProtocol;
@protocol NITCreateEventViewProtocol;
@protocol NITCreateEventPresenterProtocol;
@protocol NITCreateEventLocalDataManagerInputProtocol;
@protocol NITCreateEventAPIDataManagerInputProtocol;

@class NITCreateEventWireFrame;

typedef CF_ENUM (NSUInteger, EventMode) {
    EventModeCreate = 0,
    EventModeEdit   = 1
};

typedef void (^SelectContactBlock) (NSString * phoneNumber);
typedef void (^SelectPhotoBlock) (UIImage * image, NSURL * url);

@protocol NITCreateEventViewProtocol
@required
@property (nonatomic, strong) id <NITCreateEventPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITNewEventModel *)model;
- (void)reloadEditedState:(BOOL)edited;
@end

@protocol NITCreateEventWireFrameProtocol
@property (nonatomic) SelectContactBlock contactBlock;
@property (nonatomic) SelectPhotoBlock photoBlock;
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCreateEventModuleFrom:(id)fromView withDate:(NSDate *)date;
+ (void)presentNITEditEventModuleFrom:(id)fromView withEvent:(NITEventModel *)model;
- (void)backActionFrom:(id)fromView;
- (void)selectPhoneNumberFrom:(id)fromView withHandler:(SelectContactBlock)handler;
- (void)selectPhotoFromView:(id)fromView withHandler:(SelectPhotoBlock)handler;
- (void)takePhotoFromView:(id)fromView withHandler:(SelectPhotoBlock)handler;
@end

@protocol NITCreateEventPresenterProtocol
@required
@property (nonatomic, weak) id <NITCreateEventViewProtocol> view;
@property (nonatomic, strong) id <NITCreateEventInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCreateEventWireFrameProtocol> wireFrame;
@property (nonatomic) NITEventModel * editedEvent;
@property (nonatomic) EventMode eventMode;
@property (nonatomic) NSDate * createdDate;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (NSString *)formattedDate;
- (void)selectContact;
- (void)selectPhoto;
- (void)selectRepaet;
- (void)createAction;
- (void)saveAction;
- (void)checkEditedState;
@end

@protocol NITCreateEventInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)saveEventSuccess:(BOOL)success;
@end

@protocol NITCreateEventInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCreateEventInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCreateEventAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCreateEventLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)createActionByModel:(NITNewEventModel *)model;
- (void)saveActionByModel:(NITNewEventModel *)model;
@end


@protocol NITCreateEventDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCreateEventAPIDataManagerInputProtocol <NITCreateEventDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserEventsWithHandler:(void(^)(NSArray <SWGUserEvent> *result))handler;
- (void)createByModel:(NITNewEventModel *)model withHandler:(void(^)(SWGUserEvent *result))handler;
- (void)editeByModel:(NITNewEventModel *)model withHandler:(void(^)(SWGUserEvent *result))handler;
@end

@protocol NITCreateEventLocalDataManagerInputProtocol <NITCreateEventDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)editeByModel:(NITNewEventModel *)model;
@end
