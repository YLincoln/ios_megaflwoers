//
//  NITCatalogDirectoryProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemViewModel.h"
#import "SWGBouquetSection.h"
#import "SWGBouquet.h"
#import "SWGAttach.h"
#import "NITCatalogFilterViewModel.h"
#import "NITCategoryViewModel.h"
#import "NITBasketProductModel.h"
#import "NITBusketBouquet.h"
#import "NITFavoriteBouquet.h"
#import "NITBusketAttachment.h"
#import "NITCoupon.h"
#import "NITCatalogItemSKUViewModel.h"

typedef CF_ENUM (NSUInteger, ProductSelectType) {
    ProductSelectTypeView   = 0,
    ProductSelectTypeSelect = 1,
    ProductSelectTypeEdite = 2,
};

typedef CF_ENUM (NSUInteger, SortType) {
    SortTypeNone            = -1,
    SortTypePopular         = 0,
    SortTypePriceAscending  = 1,
    SortTypePriceDescending = 2
};

@protocol NITCatalogDirectoryInteractorOutputProtocol;
@protocol NITCatalogDirectoryInteractorInputProtocol;
@protocol NITCatalogDirectoryViewProtocol;
@protocol NITCatalogDirectoryPresenterProtocol;
@protocol NITCatalogDirectoryLocalDataManagerInputProtocol;
@protocol NITCatalogDirectoryAPIDataManagerInputProtocol;

@class NITCatalogDirectoryWireFrame;

@protocol NITCatalogDirectoryViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogDirectoryPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadView;
- (void)reloadDataByModels:(NSArray<NITCatalogItemViewModel *> *)models;
- (void)insertDataByModels:(NSArray<NITCatalogItemViewModel *> *)models;
- (void)reloadDataBySubCategoryModels:(NSArray<NITCategoryViewModel *> *)models;
- (void)reloadFilterdDataByModels:(NSArray<NITCatalogFilterViewModel *> *)models;
- (void)reloadSortTitle:(NSString *)title;
@end

@protocol NITCatalogDirectoryWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITStockDirectoryModuleFrom:(id)fromView withTitle:(NSString *)title discountId:(NSNumber *)discountId;
+ (void)presentNITCatalogDirectoryModuleFrom:(id)fromView withTitle:(NSString *)title parentID:(NSNumber *)parentID;
+ (void)presentNITAttachDirectoryModuleFrom:(id)fromView withTitle:(NSString *)title parentID:(NSNumber *)parentID;
+ (void)selectNITCatalogDirectoryFrom:(UIViewController *)fromView withTitle:(NSString *)title parentID:(NSNumber *)parentID;
+ (void)selectNITAttachModuleFrom:(UIViewController *)fromView withTitle:(NSString *)title parentID:(NSNumber *)parentID;
- (void)backActionFrom:(id)fromView;
- (void)showFiltersFrom:(id)fromView parentID:(NSNumber *)parentID;
- (void)showImages:(NSArray <NSString *> *)imagesPaths from:(id)fromView;
- (void)showCallOrderFrom:(id)fromView;
- (void)showRegisterFrom:(id)fromView;
- (void)showBasketFrom:(id)fromView;
- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model selectType:(ProductSelectType)selectType from:(id)fromView;
@end

@protocol NITCatalogDirectoryPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogDirectoryViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogDirectoryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogDirectoryWireFrameProtocol> wireFrame;
@property (nonatomic) ProductContentType contentType;
@property (nonatomic) ProductSelectType selectType;
@property (nonatomic) NSNumber * discountId;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)showFilters;
- (void)initData;
- (void)updateData;
- (void)updateDataBySubCategoryModel:(NITCategoryViewModel *)model;
- (void)callAction;
- (void)sortAction;
- (void)showImages:(NSArray <NSString *> *)imagesPaths;
- (void)viewWillAppear;
- (void)viewWillDisappear;
- (void)openCallOrder;
- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model;
- (void)checkLoadNextDataByOffset:(NSInteger)offset;
@end

@protocol NITCatalogDirectoryInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITCatalogDirectoryInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogDirectoryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogDirectoryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogDirectoryLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NSNumber * parentID;
@property (nonatomic) NSNumber * currentParentID;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)initDataWidthDiscountId:(NSNumber *)discountID withHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *, NSString *))handler;
- (void)initDataWidthContentType:(ProductContentType)contentType
                     withHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *models, NSArray<NITCategoryViewModel *> *sections, NSString * error))handler;
- (void)updateDataWithSortType:(SortType)type
                   contentType:(ProductContentType)contentType
                    andHandler:(void (^)(NSArray<NITCatalogItemViewModel *> * models, NSArray<NITCategoryViewModel *> * sections, NSString * error))handler;
- (void)updateDataByModel:(NITCategoryViewModel *)model
              contentType:(ProductContentType)contentType
                 sortType:(SortType)type
               andHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *models, NSArray<NITCategoryViewModel *> *sections, NSString * error))handler;
- (void)loadNextBouqetsWithOffset:(NSNumber *)offset andHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *models))handler;
- (NSArray <NITCatalogFilterViewModel *> *)getFiltersData;
- (NSArray <NITCatalogItemViewModel *> *)sortDataBySortType:(SortType)type;
@end


@protocol NITCatalogDirectoryDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogDirectoryAPIDataManagerInputProtocol <NITCatalogDirectoryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getStocssById:(NSNumber *)stockId withHandler:(void (^)(NSArray<SWGBouquet *> *, NSError *))handler;
- (void)getSubCategoriesByParentID:(NSNumber *)parentID withHandler:(void (^)(NSArray<SWGBouquetSection*> *result))handler;
- (void)getBouquetsByParentID:(NSNumber *)parentID sort:(NSNumber *)sort offset:(NSNumber *)offset withHandler:(void(^)(NSArray<SWGBouquet *> * output, NSError * error))handler;
- (void)getAttachByBouquetID:(NSNumber *)bouquetID withHandler:(void(^)(NSArray<SWGAttach *> *output, NSError * error))handler;
- (void)getAttachesByParentID:(NSNumber *)parentID withHandler:(void(^)(NSArray<SWGAttach *> *output, NSError * error))handler;
@end

@protocol NITCatalogDirectoryLocalDataManagerInputProtocol <NITCatalogDirectoryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITCatalogFilterViewModel *> *)getFiltersData;
@end
