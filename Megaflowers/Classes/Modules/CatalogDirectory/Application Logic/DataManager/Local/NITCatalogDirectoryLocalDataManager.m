//
//  NITCatalogDirectoryLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryLocalDataManager.h"

@implementation NITCatalogDirectoryLocalDataManager

- (NSArray <NITCatalogFilterViewModel *> *)getFiltersData
{
    NSDictionary * filtersInfo = [FILTERS allActiveFilters];
    NSMutableArray * items = [NSMutableArray new];
    
    for (NSNumber * key in filtersInfo)
    {
        [items addObject:[[NITCatalogFilterViewModel alloc] initWithID:key title:filtersInfo[key]]];
    }
    
    return items;
}

@end
