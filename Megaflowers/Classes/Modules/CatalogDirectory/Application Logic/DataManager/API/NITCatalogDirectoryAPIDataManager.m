//
//  NITCatalogDirectoryAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryAPIDataManager.h"
#import "SWGBouquetSectionApi.h"
#import "SWGBouquetApi.h"
#import "SWGUserApi.h"
#import "SWGAttachApi.h"
#import "SWGDiscountApi.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;
static const NSInteger limit = 20;

@implementation NITCatalogDirectoryAPIDataManager

#pragma mark - NITCatalogDirectoryAPIDataManagerInputProtocol
- (void)getStocssById:(NSNumber *)stockId withHandler:(void (^)(NSArray<SWGBouquet *> *, NSError *))handler
{
    [HELPER startLoading];
    [[SWGDiscountApi new] discountIdGetWithId:stockId lang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(SWGDiscount *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler(output.bouquets, error);
    }];
}

- (void)getBouquetsByParentID:(NSNumber *)parentID sort:(NSNumber *)sort offset:(NSNumber *)offset withHandler:(void (^)(NSArray<SWGBouquet *> *, NSError *))handler
{
    [HELPER startLoading];
    [[SWGBouquetApi new] bouquetGetWithLang:HELPER.lang
                                       site:API.site
                                       city:USER.cityID
                                     parent:parentID
                                    filters:[FILTERS activeFilters]
                                      price:[FILTERS activePriceFilters]
                                     offset:offset
                                      limit:@(limit)
                                       sort:sort
                          completionHandler:^(NSArray<SWGBouquet> *output, NSError *error) {
                              [HELPER stopLoading];
                              if (error) [HELPER logError:error method:METHOD_NAME];
                              if (handler) handler(output, error);
                          }];
}

- (void)getSubCategoriesByParentID:(NSNumber *)parentID withHandler:(void (^)(NSArray<SWGBouquetSection*> *result))handler
{
    [HELPER startLoading];
    [[SWGBouquetSectionApi new] bouquetSectionGetWithLang:HELPER.lang
                                                     site:API.site
                                                     city:USER.cityID
                                                   parent:parentID
                                        completionHandler:^(NSArray<SWGBouquetSection> *output, NSError *error) {
                                            [HELPER stopLoading];
                                            if (error) [HELPER logError:error method:METHOD_NAME];
                                            if (handler) handler(output);
                                        }];
}

- (void)getAttachByBouquetID:(NSNumber *)bouquetID withHandler:(void (^)(NSArray<SWGAttach *> *, NSError *))handler
{
    [HELPER startLoading];
    [[SWGBouquetApi new] bouquetIdAdditionsGetWithId:bouquetID
                                                lang:HELPER.lang
                                                site:API.site
                                                city:USER.cityID
                                   completionHandler:^(NSArray<SWGAttach> *output, NSError *error) {
                                       [HELPER stopLoading];
                                       if (error) [HELPER logError:error method:METHOD_NAME];
                                       if (handler) handler(output, error);
                                   }];
}

- (void)getAttachesByParentID:(NSNumber *)parentID withHandler:(void (^)(NSArray<SWGAttach *> *, NSError *))handler
{
    [HELPER startLoading];
    [[SWGAttachApi new] attachGetWithLang:HELPER.lang
                                     site:API.site
                                     city:USER.cityID
                                   parent:parentID
                        completionHandler:^(NSArray<SWGAttach> *output, NSError *error) {
                            [HELPER stopLoading];
                            if (error) [HELPER logError:error method:METHOD_NAME];
                            if (handler) handler(output, error);
                        }];
}

@end
