//
//  NITCatalogDirectoryInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITProductDetailsAPIDataManager.h"

@interface NITCatalogDirectoryInteractor ()

@property (nonatomic) NSArray * data;
@property (nonatomic) NSArray * sectionsData;
@property (nonatomic) SortType sortType;

@end

@implementation NITCatalogDirectoryInteractor

#pragma mark - Custom accessors
- (void)setParentID:(NSNumber *)parentID
{
    _parentID = parentID;
    self.currentParentID = parentID;
}

#pragma mark - NITCatalogDirectoryInteractorInputProtocol
- (void)initDataWidthDiscountId:(NSNumber *)discountID withHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *, NSString *))handler
{
    weaken(self);
    self.sortType = SortTypePopular;
    [self.APIDataManager getStocssById:discountID withHandler:^(NSArray<SWGBouquet *> *output, NSError *error) {
        weakSelf.data = [NITCatalogItemViewModel bouqetModelsFromEntities:output];
        handler([weakSelf sortingData], error.interfaceDescription);
    }];

}

- (void)initDataWidthContentType:(ProductContentType)contentType withHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *, NSArray<NITCategoryViewModel *> *, NSString *))handler
{
    weaken(self);
    self.sortType = SortTypePopular;
    switch (contentType) {
        case ProductContentTypeBouqets: {
            [self.APIDataManager getSubCategoriesByParentID:self.parentID withHandler:^(NSArray<SWGBouquetSection *> *result) {
                
                if (result.count > 0)
                {
                    SWGBouquetSection * section = result.firstObject;
                    weakSelf.currentParentID = section._id;
                    weakSelf.sectionsData = [NITCategoryViewModel categoryModelsFromEntities:result];
                }
                
                [weakSelf.APIDataManager getBouquetsByParentID:weakSelf.currentParentID sort:[self sortKey] offset:@0 withHandler:^(NSArray<SWGBouquet *> *output, NSError * error) {
                    weakSelf.data = [NITCatalogItemViewModel bouqetModelsFromEntities:output];
                    handler([weakSelf sortingData], weakSelf.sectionsData, error.interfaceDescription);
                }];
            }];
        } break;
            
        case ProductContentTypeAttach: {
            [self.APIDataManager getAttachesByParentID:self.currentParentID withHandler:^(NSArray<SWGAttach *> *output, NSError *error) {
                weakSelf.data = [NITCatalogItemViewModel attachModelsFromEntities:output];
                handler([weakSelf sortingData], nil, error.interfaceDescription);
            }];
        } break;
    }
}

- (void)updateDataWithSortType:(SortType)type contentType:(ProductContentType)contentType andHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *, NSArray<NITCategoryViewModel *> *, NSString *))handler
{
    [self updateDataByModel:nil contentType:contentType sortType:type andHandler:handler];
}

- (void)updateDataByModel:(NITCategoryViewModel *)model contentType:(ProductContentType)contentType sortType:(SortType)type andHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *, NSArray<NITCategoryViewModel *> *, NSString *))handler
{
    if (type != SortTypeNone) self.sortType = type;
    if (model) self.currentParentID = model.identifier;
    
    weaken(self);
    switch (contentType) {
        case ProductContentTypeBouqets: {
            [weakSelf.APIDataManager getBouquetsByParentID:weakSelf.currentParentID sort:[self sortKey] offset:@0 withHandler:^(NSArray<SWGBouquet *> *output, NSError *error) {
                weakSelf.data = [NITCatalogItemViewModel bouqetModelsFromEntities:output];
                handler([weakSelf sortingData], weakSelf.sectionsData, error.interfaceDescription);
            }];
        } break;
            
        case ProductContentTypeAttach: {
            [self.APIDataManager getAttachesByParentID:self.currentParentID withHandler:^(NSArray<SWGAttach *> *output, NSError *error) {
                weakSelf.data = [NITCatalogItemViewModel attachModelsFromEntities:output];
                handler([weakSelf sortingData], nil, error.interfaceDescription);
            }];
        } break;
    }
}

- (void)loadNextBouqetsWithOffset:(NSNumber *)offset andHandler:(void (^)(NSArray<NITCatalogItemViewModel *> *))handler
{
    weaken(self);
    [self.APIDataManager getBouquetsByParentID:weakSelf.currentParentID sort:[self sortKey] offset:offset withHandler:^(NSArray<SWGBouquet *> *output, NSError *error) {
        
        if (output.count > 0)
        {
            NSArray * loadedData = [NITCatalogItemViewModel bouqetModelsFromEntities:output];
            NSMutableArray * allData = [NSMutableArray arrayWithArray:weakSelf.data];
            [allData addObjectsFromArray:loadedData];
            weakSelf.data = allData;
            handler(loadedData);
        }
        else
        {
            handler(nil);
        }
    }];
}

- (NSArray <NITCatalogFilterViewModel *> *)getFiltersData
{
    return [self.localDataManager getFiltersData];
}

- (NSArray <NITCatalogItemViewModel *> *)sortDataBySortType:(SortType)type
{
    self.sortType = type;
    return [self sortingData];
}

#pragma mark - Private
- (NSArray <NITCatalogItemViewModel *> *)sortingData
{
    NSString * sortKey;
    BOOL acceding;
    
    switch (self.sortType) {
            
        case SortTypePopular:
        case SortTypeNone: {
            sortKey = keyPath(NITCatalogItemViewModel.popularity);
            acceding = false;
        } break;
            
        case SortTypePriceAscending: {
            sortKey = keyPath(NITCatalogItemViewModel.price);
            acceding = true;
        } break;
            
        case SortTypePriceDescending: {
            sortKey = keyPath(NITCatalogItemViewModel.price);
            acceding = false;
        } break;
    }
    
    NSMutableArray * sortedModels = [self.data mutableCopy];
    [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:acceding]]];
    
    return sortedModels;
}

- (NSNumber *)sortKey
{
    switch (self.sortType) {
        case SortTypeNone:
        case SortTypePopular:           return nil;
        case SortTypePriceAscending:    return @4;
        case SortTypePriceDescending:   return @3;
    }
}

@end
