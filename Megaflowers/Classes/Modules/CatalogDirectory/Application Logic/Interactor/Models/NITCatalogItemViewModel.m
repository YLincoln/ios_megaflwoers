//
//  NITCatalogItemViewModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"
#import "SWGBouquet.h"
#import "SWGAttach.h"
#import "NITFavoriteBouquet.h"
#import "SWGBouquetSku.h"
#import "NSArray+BlocksKit.h"
#import <objc/runtime.h>
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"
#import "SWGBasketBouquet.h"

static NSString * const kYoutubeLink = @"https://www.youtube.com/watch?v=";

@implementation NITCatalogItemViewModel

#pragma mark - Life cycle
- (instancetype)initWithBouquet:(SWGBouquet *)bouquet
{
    if (self = [super init])
    {
        self.identifier     = bouquet._id;
        self.contentType    = ProductContentTypeBouqets;
        self.title          = bouquet.name;
        self.popularity     = bouquet.countOrders;
        self.detail         = [bouquet.detail stringByStrippingHTML];
        self.price          = [(SWGBouquetSku *)[self minimalPriceSKUForProduct:bouquet byKey:keyPath(SWGBouquetSkus.sku)] price];
        self.discountPrice  = [(SWGBouquetSku *)[self minimalPriceSKUForProduct:bouquet byKey:keyPath(SWGBouquetSkus.sku)] discountPrice];
        
        SWGBouquetSku * presentSKU = [self presentSKUFromProduct:bouquet];
            
        self.videoPath      = presentSKU.video.length > 0 ? [kYoutubeLink stringByAppendingString:presentSKU.video] : @"";
        self.videoComment   = presentSKU.videoComment;
        self.masterPath     = presentSKU.masterClass.length > 0 ? [kYoutubeLink stringByAppendingString:presentSKU.masterClass] : @"";
        self.imagePath      = [self imagePathByKey:keyPath(sku.image) fromSku:presentSKU];
        self.previewPath    = [self previewPathByKey:keyPath(sku.image) fromSku:presentSKU];
        self.offset         = presentSKU.offset.integerValue < 0 ? 0 : presentSKU.offset.integerValue;
        
        if ([bouquet respondsToSelector:@selector(defaultSkuId)])
        {
            self.defaultSkuId = bouquet.defaultSkuId;
        }
        
        if ([bouquet respondsToSelector:@selector(active)])
        {
            self.enabled = bouquet.active.boolValue;
        }
        
        if (bouquet.label)
        {
            self.iconPath = [bouquet.label valueForKey:API.imageKey];
        }

        self.skus = [bouquet.sku bk_map:^id(SWGBouquetSku *obj) {
            return [[NITCatalogItemSKUViewModel alloc] initWithBouquetSKU:obj];
        }];
        
        self.imagePaths = [bouquet.sku bk_map:^id(SWGBouquetSku *obj) {
            return [self imagePathByKey:keyPath(sku.image) fromSku:obj];
        }];
    }
    
    return self;
}

- (instancetype)initWidthFavoriteBouquet:(NITFavoriteBouquet *)bouquet
{
    if (self = [super init])
    {
        self.identifier         = @([bouquet.bouquetID integerValue]);
        self.favoriteSkuID      = @([bouquet.identifier integerValue]);
        self.price              = @(bouquet.price);
        self.discountPrice      = @(bouquet.discountPrice);
        self.title              = bouquet.bouquetName;
        self.imagePath          = bouquet.imagePath;
        self.previewPath        = bouquet.previewPath;
        self.favoriteSkuName    = bouquet.name;
        self.favoriteSkuSize    = bouquet.presentSize;
        self.enabled            = bouquet.active;
    }

    return self;
}

- (instancetype)initWidthSWGBasketBouquet:(SWGBasketBouquet *)basketBouquet bouquet:(SWGBouquet *)bouquet
{
    if (self = [self initWithBouquet:bouquet])
    {
        NITCatalogItemSKUViewModel * sku = [[NITCatalogItemSKUViewModel alloc] initWithBasketBouquetSKU:basketBouquet];
        NITCatalogItemSKUViewModel * originalSku = [self.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj) {
            return [obj.identifier isEqualToNumber:sku.identifier];
        }].firstObject;
        
        if (originalSku)
        {
            sku.edited = ![sku isEqualSKU:originalSku];
        }
        
        self.skus = @[sku];
    }
    
    return self;
}

- (instancetype)initWithAttach:(SWGAttach *)attach
{
    if (self = [super init])
    {
        self.identifier     = attach._id;
        self.contentType    = ProductContentTypeAttach;
        self.title          = attach.name;
        self.detail         = [attach.detail stringByStrippingHTML];
        self.price          = [(SWGAttachSku *)[self minimalPriceSKUForProduct:attach byKey:keyPath(SWGAttachSku.sku)] price];
        self.discountPrice  = [(SWGAttachSku *)[self minimalPriceSKUForProduct:attach byKey:keyPath(SWGAttachSku.sku)] discountPrice];
        
        if ([attach respondsToSelector:@selector(defaultSkuId)])
        {
            self.defaultSkuId   = attach.defaultSkuId;
        }
        
        SWGAttachSku * presentSKU = [self presentSKUFromProduct:attach];
        
        self.imagePath      = [self imagePathByKey:keyPath(sku.image1) fromSku:presentSKU];
        self.previewPath    = [self previewPathByKey:keyPath(sku.image1) fromSku:presentSKU];
        self.offset         = presentSKU.offset.integerValue < 0 ? 0 : presentSKU.offset.integerValue;
        
        self.skus = [attach.sku bk_map:^id(SWGAttachSku *obj) {
            return [[NITCatalogItemSKUViewModel alloc] initWithAttachSKU:obj];
        }];
        
        self.imagePaths = [attach.sku bk_map:^id(SWGAttachSku *obj) {
            return [self imagePathByKey:keyPath(sku.image1) fromSku:obj];
        }];
    }
    
    return self;
}

- (id)copy
{
    NITCatalogItemViewModel * model = [NITCatalogItemViewModel new];
    
    for (NSString * key in [self allPropertyNames])
    {
        if ([key isEqualToString:keyPath(self.skus)] ||
            [key isEqualToString:keyPath(self.imagePaths)])
        {
            continue;
        }
        
        id value = [self valueForKey:key];
        if (value) [model setValue:value forKey:key];
    }
    
    model.imagePaths = [self.imagePaths copy];
    model.skus = [self.skus bk_map:^id(NITCatalogItemSKUViewModel *obj) {
        return [obj copy];
    }];
    
    return model;
}

#pragma mark - Custom accessors
- (BOOL)favorite
{
    return [self.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj) {
        return obj.favorite == true;
    }].count > 0;
}

- (BOOL)edited
{
    return [self.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj) {
        return obj.edited == true;
    }].count > 0;
}

- (NITCatalogItemSKUViewModel *)defaultSku
{
    NITCatalogItemSKUViewModel * sku = [self.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj) {
        return self.defaultSkuId ? [obj.identifier isEqualToNumber:self.defaultSkuId] : false;
    }].firstObject;
    
    return sku ? sku : [self minimalPriceSKUForProduct:self byKey:keyPath(NITCatalogItemSKUViewModel.skus)];
}

- (CatalogItemsViewMode)CatalogItemsViewMode
{
    return USER.viewType;
}

- (BOOL)inBasket
{
    switch (self.contentType) {
        case ProductContentTypeBouqets:
            return [NITBusketBouquet bouquetByID:self.identifier.stringValue] != nil;
            
        case ProductContentTypeAttach:
            return [NITBusketAttachment attachmentByID:self.identifier.stringValue] != nil;
    }
}

#pragma mark - Public
+ (NSArray <NITCatalogItemViewModel *> *)bouqetModelsFromEntities:(NSArray <SWGBouquet *> *)entities
{
    return [entities bk_map:^id(SWGBouquet * obj) {
        return [[NITCatalogItemViewModel alloc] initWithBouquet:obj];
    }];
}

+ (NSArray <NITCatalogItemViewModel *> *)attachModelsFromEntities:(NSArray <SWGAttach *> *)entities
{
    return [entities bk_map:^id(SWGAttach * obj) {
        return [[NITCatalogItemViewModel alloc] initWithAttach:obj];
    }];
}

#pragma mark - Private
- (id)presentSKUFromProduct:(id)product
{
    NSArray * skus = [product valueForKey:keyPath(SWGBouquetSkus.sku)];
    id sku = [skus bk_select:^BOOL(id obj) {
        if ([product respondsToSelector:@selector(defaultSkuId)] && [product valueForKey:keyPath(SWGBouquetSkus.defaultSkuId)])
        {
            return [[obj valueForKey:keyPath(SWGBouquetSkus._id)] isEqualToNumber:[product valueForKey:keyPath(SWGBouquetSkus.defaultSkuId)]];
        }
        else
        {
            return false;
        }
    }].firstObject;
    
    return sku ? sku : [self minimalPriceSKUForProduct:product byKey:keyPath(SWGBouquetSkus.sku)];
}

- (id)minimalPriceSKUForProduct:(id)product byKey:(NSString *)key
{
    NSMutableArray * sortedSKUS = [[product valueForKey:key] mutableCopy];
    [sortedSKUS sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:keyPath(SWGBouquetSkus.discountPrice) ascending:true]]];
    
    return sortedSKUS.firstObject;
}

- (NSString *)imagePathByKey:(NSString *)key fromSku:(id)sku
{
    NSDictionary * dict = [(NSDictionary *)sku valueForKey:key];
    return dict[@"detail"][API.imageKey] ? : @"";
}

- (NSString *)previewPathByKey:(NSString *)key fromSku:(id)sku
{
    NSDictionary * dict = [(NSDictionary *)sku valueForKey:key];
    return dict[@"preview"][API.imageKey] ? : @"";
}

@end
