//
//  NITCatalogFilterViewModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@interface NITCatalogFilterViewModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NSNumber * identifier;

- (instancetype)initWithID:(NSNumber *)identifier title:(NSString *)title;

- (void)removeFilter;

@end
