//
//  NITCatalogFilterViewModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogFilterViewModel.h"

@implementation NITCatalogFilterViewModel

- (instancetype)initWithID:(NSNumber *)identifier title:(NSString *)title
{
    if (self = [super init])
    {
        self.identifier = identifier;
        self.title = title;
    }
    
    return self;
}

- (void)removeFilter
{
    [FILTERS removeFilterByID:self.identifier];
}

@end
