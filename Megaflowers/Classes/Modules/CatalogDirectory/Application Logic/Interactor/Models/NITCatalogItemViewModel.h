//
//  NITCatalogItemViewModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGBouquet, SWGAttach, NITCatalogItemSKUViewModel, NITFavoriteBouquet, SWGBasketBouquet;

typedef CF_ENUM (NSUInteger, ProductContentType) {
    ProductContentTypeBouqets = 0,
    ProductContentTypeAttach  = 1
};

@interface NITCatalogItemViewModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSNumber * defaultSkuId;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * detail;
@property (nonatomic) NSString * previewPath;
@property (nonatomic) NSString * imagePath;
@property (nonatomic) NSString * videoPath;
@property (nonatomic) NSString * videoComment;
@property (nonatomic) NSString * masterPath;
@property (nonatomic) NSString * panoramaImagePath;
@property (nonatomic) NSString * iconPath;
@property (nonatomic) NSNumber * price;
@property (nonatomic) NSNumber * discountPrice;
@property (nonatomic) NSNumber * popularity;
@property (nonatomic) NSNumber * favoriteSkuID;
@property (nonatomic) NSString * favoriteSkuName;
@property (nonatomic) NSString * favoriteSkuSize;
@property (nonatomic) NSInteger selectType;
@property (nonatomic) NSInteger offset;
@property (nonatomic) ProductContentType contentType;
@property (nonatomic) CatalogItemsViewMode CatalogItemsViewMode;
@property (nonatomic) BOOL enabled;
@property (nonatomic) BOOL favorite;
@property (nonatomic) BOOL edited;
@property (nonatomic) BOOL inBasket;
@property (nonatomic) NITCatalogItemSKUViewModel * defaultSku;

@property (nonatomic) NSArray <NITCatalogItemSKUViewModel *> * skus;
@property (nonatomic) NSArray <NSString *> * imagePaths;

- (instancetype)initWithBouquet:(SWGBouquet *)bouquet;
- (instancetype)initWithAttach:(SWGAttach *)attach;
- (instancetype)initWidthFavoriteBouquet:(NITFavoriteBouquet *)bouquet;
- (instancetype)initWidthSWGBasketBouquet:(SWGBasketBouquet *)basketBouquet bouquet:(SWGBouquet *)bouquet;

+ (NSArray <NITCatalogItemViewModel *> *)bouqetModelsFromEntities:(NSArray <SWGBouquet *> *)entities;
+ (NSArray <NITCatalogItemViewModel *> *)attachModelsFromEntities:(NSArray <SWGAttach *> *)entities;

@end
