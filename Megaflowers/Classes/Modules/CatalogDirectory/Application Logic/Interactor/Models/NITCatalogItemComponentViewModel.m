//
//  NITCatalogItemComponentViewModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemComponentViewModel.h"
#import "SWGBouquetComponents.h"
#import "SWGBasketBouquetComponent.h"

@implementation NITCatalogItemComponentViewModel

#pragma mark - Life cycle
- (instancetype)initWithBouquetComponents:(SWGBouquetComponents *)components
{
    if (self = [super init])
    {
        self.identifier = components.component._id;
        self.title      = components.component.name;
        self.type       = components.component.type;
        self.count      = components.count;
        self.color      = components.color._id;
        self.text       = [components.component._description stringByStrippingHTML];
        self.required   = [components.component.required boolValue];
    }
    
    return self;
}

- (instancetype)initWithBasketBouquetComponents:(SWGBasketBouquetComponent *)component
{
    if (self = [super init])
    {
        self.identifier = component._id;
        self.title      = component.name;
        self.count      = component.count;
        self.color      = component.color._id;
    }
    
    return self;
}

- (id)copy
{
    NITCatalogItemComponentViewModel * model = [NITCatalogItemComponentViewModel new];
    
    for (NSString * key in [self allPropertyNames])
    {
        id value = [self valueForKey:key];
        if (value) [model setValue:value forKey:key];
    }

    return model;
}

#pragma mark - Public
- (BOOL)isEqualComponent:(NITCatalogItemComponentViewModel *)component
{
    if (![self.count isKindOfClass:[NSNumber class]] ||
        ![component.count isKindOfClass:[NSNumber class]] ||
        ![self.count isEqualToNumber:component.count])
    {
        return false;
    }
    
    return true;
}

@end
