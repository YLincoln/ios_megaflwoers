//
//  NITCatalogItemSKUViewModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/2/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGBouquetSku, SWGAttachSku, NITCatalogItemComponentViewModel, SWGBasketBouquet;

@interface NITCatalogItemSKUViewModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * db_id;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * imagePath;
@property (nonatomic) NSString * previewPath;
@property (nonatomic) NSNumber * width;
@property (nonatomic) NSNumber * height;
@property (nonatomic) NSNumber * price;
@property (nonatomic) NSNumber * discountPrice;
@property (nonatomic) NSNumber * count;
@property (nonatomic) NSString * presentName;
@property (nonatomic) BOOL favorite;
@property (nonatomic) BOOL edited;
@property (nonatomic) NSArray <NITCatalogItemComponentViewModel *> * components;
@property (nonatomic) NSString * image360Path;
@property (nonatomic) NSString * image360Frame;

- (instancetype)initWithBouquetSKU:(SWGBouquetSku *)bouqueSKU;
- (instancetype)initWithAttachSKU:(SWGAttachSku *)attachSKU;
- (instancetype)initWithBasketBouquetSKU:(SWGBasketBouquet *)bouquetSKU;
- (BOOL)isEqualSKU:(NITCatalogItemSKUViewModel *)bouqueSKU;

@end
