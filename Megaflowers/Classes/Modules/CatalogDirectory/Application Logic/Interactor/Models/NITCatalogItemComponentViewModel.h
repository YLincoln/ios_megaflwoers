//
//  NITCatalogItemComponentViewModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGBouquetComponents, SWGBasketBouquetComponent;

@interface NITCatalogItemComponentViewModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * type;
@property (nonatomic) NSString * text;
@property (nonatomic) NSNumber * color;
@property (nonatomic) NSNumber * count;
@property (nonatomic) BOOL required;

- (instancetype)initWithBouquetComponents:(SWGBouquetComponents *)components;
- (instancetype)initWithBasketBouquetComponents:(SWGBasketBouquetComponent *)component;
- (BOOL)isEqualComponent:(NITCatalogItemComponentViewModel *)component;

@end
