//
//  NITCatalogItemSKUViewModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/2/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemSKUViewModel.h"
#import "SWGBouquetSku.h"
#import "NITCatalogItemComponentViewModel.h"
#import "SWGAttachSku.h"
#import "NSArray+BlocksKit.h"
#import "NITFavoriteBouquet.h"
#import "SWGBasketBouquet.h"
#import "SWGBasketBouquetComponent.h"

@implementation NITCatalogItemSKUViewModel

#pragma mark - Life cycle
- (instancetype)initWithBouquetSKU:(SWGBouquetSku *)bouqueSKU
{
    if (self = [super init])
    {
        self.identifier     = bouqueSKU._id;
        self.title          = bouqueSKU.name;
        self.width          = bouqueSKU.width;
        self.height         = bouqueSKU.height;
        self.count          = bouqueSKU.count;
        self.price          = bouqueSKU.price;
        self.discountPrice  = bouqueSKU.discountPrice;
        self.imagePath      = [self imagePathByKey:keyPath(sku.image) fromSku:bouqueSKU];
        self.previewPath    = [self previewPathByKey:keyPath(sku.image) fromSku:bouqueSKU];
        self.image360Path   = bouqueSKU.axis;
        self.image360Frame  = bouqueSKU.axisPlug;
        
        self.components = [bouqueSKU.components bk_map:^id(SWGBouquetComponents *obj) {
            return [[NITCatalogItemComponentViewModel alloc] initWithBouquetComponents:obj];
        }];
    }
    
    return self;
}

- (instancetype)initWithAttachSKU:(SWGAttachSku *)attachSKU
{
    if (self = [super init])
    {
        self.identifier     = attachSKU._id;
        self.title          = attachSKU.name;
        self.count          = attachSKU.count;
        self.price          = attachSKU.price;
        self.discountPrice  = attachSKU.discountPrice;
        self.imagePath      = [self imagePathByKey:keyPath(sku.image1) fromSku:attachSKU];
        self.previewPath    = [self previewPathByKey:keyPath(sku.image1) fromSku:attachSKU];
    }
    
    return self;
}

- (instancetype)initWithBasketBouquetSKU:(SWGBasketBouquet *)bouquetSKU
{
    if (self = [super init])
    {
        self.identifier     = bouquetSKU.bouquetSkuId;
        self.title          = bouquetSKU.skuName;
        self.count          = bouquetSKU.count;
        self.price          = bouquetSKU.price;
        self.discountPrice  = bouquetSKU.price;
        self.imagePath      = [self imagePathByKey:keyPath(sku.image) fromSku:bouquetSKU.image];
        self.previewPath    = [self previewPathByKey:keyPath(sku.image) fromSku:bouquetSKU.image];
        
        self.components = [bouquetSKU.components bk_map:^id(SWGBasketBouquetComponent *obj) {
            return [[NITCatalogItemComponentViewModel alloc] initWithBasketBouquetComponents:obj];
        }];
    }
    
    return self;
}

- (id)copy
{
    NITCatalogItemSKUViewModel * model = [NITCatalogItemSKUViewModel new];
    
    for (NSString * key in [self allPropertyNames])
    {
        if ([key isEqualToString:keyPath(self.components)]) continue;
        id value = [self valueForKey:key];
        if (value) [model setValue:value forKey:key];
    }
    
    model.components = [self.components bk_map:^id(NITCatalogItemComponentViewModel *obj) {
        return [obj copy];
    }];
    
    return model;
}

#pragma mark - Custom accessors
- (BOOL)favorite
{
    return [NITFavoriteBouquet isFavoriteSkuID:self.identifier];
}

- (NSString *)presentName
{
    if (self.width.integerValue > 0 && self.height.integerValue > 0)
    {
        return [NSString stringWithFormat:@"%@ (%@-%@ %@)", self.title, self.width, self.height, NSLocalizedString(@"см", nil)];
    }
    else if (self.width.integerValue > 0 && self.height.integerValue == 0)
    {
        return [NSString stringWithFormat:@"%@ (%@ %@)", self.title, self.width, NSLocalizedString(@"см", nil)];
    }
    else if (self.width.integerValue == 0 && self.height.integerValue > 0)
    {
        return [NSString stringWithFormat:@"%@ (%@ %@)", self.title, self.height, NSLocalizedString(@"см", nil)];
    }
    else
    {
        return self.title;
    }
}

#pragma mark - Public
- (BOOL)isEqualSKU:(NITCatalogItemSKUViewModel *)bouqueSKU
{
    BOOL isEqual = true;
    
    for (int i = 0; i < self.components.count; i++)
    {
        NITCatalogItemComponentViewModel * comp1 = self.components[i];
        if (bouqueSKU.components.count > 0 && i <= bouqueSKU.components.count - 1)
        {
            NITCatalogItemComponentViewModel * comp2 = bouqueSKU.components[i];
            
            if (![comp1.count isKindOfClass:[NSNumber class]] ||
                ![comp2.count isKindOfClass:[NSNumber class]] ||
                ![comp1.count isEqualToNumber:comp2.count])
            {
                isEqual = false;
                break;
            }
        }
    }
    
    return isEqual;
}

#pragma mark - Private
- (NSString *)imagePathByKey:(NSString *)key fromSku:(id)sku
{
    if ([sku isKindOfClass:[SWGBasketAttachImage class]])
    {
        NSDictionary * dict = [sku valueForKey:@"detail"];
        return dict[API.imageKey] ? : @"";
    }
    else
    {
        NSDictionary * dict = [(NSDictionary *)sku valueForKey:key];
        return dict[@"detail"][API.imageKey] ? : @"";
    }
}

- (NSString *)previewPathByKey:(NSString *)key fromSku:(id)sku
{
    if ([sku isKindOfClass:[SWGBasketAttachImage class]])
    {
        NSDictionary * dict = [sku valueForKey:@"preview"];
        return dict[API.imageKey] ? : @"";
    }
    else
    {
        NSDictionary * dict = [(NSDictionary *)sku valueForKey:key];
        return dict[@"preview"][API.imageKey] ? : @"";
    }
}

@end
