//
//  NITCatalogDirectoryInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogDirectoryProtocols.h"

@interface NITCatalogDirectoryInteractor : NSObject <NITCatalogDirectoryInteractorInputProtocol>

@property (nonatomic, weak) id <NITCatalogDirectoryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogDirectoryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogDirectoryLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NSNumber * parentID;
@property (nonatomic) NSNumber * currentParentID;

@end
