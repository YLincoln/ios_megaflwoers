//
//  NITCatalogDirectoryPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryPresenter.h"
#import "NITCatalogDirectoryWireframe.h"
#import "NITFiltersInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITProductDetailsView.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITCatalogDirectoryPresenter ()

@property (nonatomic) BOOL loading;

@end

@implementation NITCatalogDirectoryPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - NITCatalogDirectoryPresenterProtocol
// Data
- (void)initData
{
    weaken(self);
    if (self.discountId)
    {
        [self.interactor initDataWidthDiscountId:self.discountId withHandler:^(NSArray<NITCatalogItemViewModel *> *models, NSString *error) {
            [weakSelf.view reloadDataByModels:[weakSelf updateSelectTypeForData:models]];
            [weakSelf.view reloadDataBySubCategoryModels:nil];
            
            if (self.contentType == ProductTypeBouquet)
            {
               [weakSelf.view reloadFilterdDataByModels:[weakSelf.interactor getFiltersData]];
            }
            
            if (error.length > 0)
            {
                [self showError:error fromView:self.view];
            }
        }];
    }
    else
    {
        [self.interactor initDataWidthContentType:self.contentType withHandler:^(NSArray<NITCatalogItemViewModel *> *models, NSArray<NITCategoryViewModel *> *sections, NSString *error) {
            [weakSelf.view reloadDataByModels:[weakSelf updateSelectTypeForData:models]];
            [weakSelf.view reloadDataBySubCategoryModels:sections];
            
            if (self.contentType == ProductTypeBouquet)
            {
                [weakSelf.view reloadFilterdDataByModels:[weakSelf.interactor getFiltersData]];
            }
            
            if (error.length > 0)
            {
                [self showError:error fromView:self.view];
            }
        }];
    }
}

- (void)updateData
{
    [self updateDataBySubCategoryModel:nil];
}

- (void)updateDataBySubCategoryModel:(NITCategoryViewModel *)model
{
    weaken(self);
    [self.interactor updateDataByModel:model contentType:self.contentType sortType:SortTypeNone andHandler:^(NSArray<NITCatalogItemViewModel *> *models, NSArray<NITCategoryViewModel *> *sections, NSString *error) {
        [weakSelf.view reloadDataByModels:[weakSelf updateSelectTypeForData:models]];
        if (error.length > 0)
        {
            [self showError:error fromView:self.view];
        }
    }];
    
    if (self.contentType == ProductTypeBouquet)
    {
        [weakSelf.view reloadFilterdDataByModels:[weakSelf.interactor getFiltersData]];
    }
}

- (void)checkLoadNextDataByOffset:(NSInteger)offset
{
    if (self.discountId == nil && self.contentType == ProductTypeBouquet && !self.loading && offset >= 20)
    {
        self.loading = true;
        [self.interactor loadNextBouqetsWithOffset:@(offset) andHandler:^(NSArray<NITCatalogItemViewModel *> *models) {
            [self.view insertDataByModels:[self updateSelectTypeForData:models]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.loading = false;
            });
        }];
    }
}

// View delegate
- (void)viewWillAppear
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveFavoriteBouquetNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveBusketBouquetNotification object:nil];
}

- (void)viewWillDisappear
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveFavoriteBouquetNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveBusketBouquetNotification object:nil];
}

- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model
{
    return [self.wireFrame createProductDetailsByModel:model selectType:self.selectType from:self.view];
}

// Action
- (void)sortDataByType:(SortType)type
{
    if (self.discountId || self.contentType == ProductTypeAttach)
    {
        [self.view reloadDataByModels:[self updateSelectTypeForData:[self.interactor sortDataBySortType:type]]];
    }
    else
    {
        weaken(self);
        [self.interactor updateDataByModel:nil contentType:self.contentType sortType:type andHandler:^(NSArray<NITCatalogItemViewModel *> *models, NSArray<NITCategoryViewModel *> *sections, NSString *error) {
            [weakSelf.view reloadDataByModels:[weakSelf updateSelectTypeForData:models]];
            if (error.length > 0)
            {
                [self showError:error fromView:self.view];
            }
        }];
    }
}

- (void)showFilters
{
    [self.wireFrame showFiltersFrom:self.view parentID:self.interactor.parentID];
}

- (void)showImages:(NSArray<NSString *> *)imagesPaths
{
    [self.wireFrame showImages:imagesPaths from:self.view];
}

- (void)openCallOrder
{
    [self.wireFrame showCallOrderFrom:self.view];
}

- (void)callAction
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Заказ звонка", nil) actionBlock:^{
        [weakSelf.wireFrame showCallOrderFrom:self.view];
    }];
    
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Сделать заказ по телефону", nil) actionBlock:^{
        [PHONE callToPhoneNumber:USER.cityPhone];
        [TRACKER trackEvent:TrackerEventOrderByPhone];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1, action2] cancelButton:cancel];
}

- (void)sortAction
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSString * action1Title = NSLocalizedString(@"По популярности", nil);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:action1Title actionBlock:^{
        [weakSelf sortDataByType:SortTypePopular];
        [weakSelf.view reloadSortTitle:action1Title];
        [TRACKER trackEvent:TrackerEventSort parameters:@{@"field":@"popularity"}];
    }];
    
    NSString * action2Title = NSLocalizedString(@"По возрастанию цены", nil);
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:action2Title actionBlock:^{
        [weakSelf sortDataByType:SortTypePriceAscending];
        [weakSelf.view reloadSortTitle:action2Title];
        [TRACKER trackEvent:TrackerEventSort parameters:@{@"field":@"price ascending"}];
    }];
    
    NSString * action3Title = NSLocalizedString(@"По убыванию цены", nil);
    NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:action3Title actionBlock:^{
        [weakSelf sortDataByType:SortTypePriceDescending];
        [weakSelf.view reloadSortTitle:action3Title];
        [TRACKER trackEvent:TrackerEventSort parameters:@{@"field":@"price descending"}];
    }];

    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1,action2,action3] cancelButton:cancel];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kChangeFiltersNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewWillDisappear) name:kOpenProductPreviewNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewWillAppear) name:kCloseProductPreviewNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCallOrder) name:kOpenCallOrderPreviewNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showBasket) name:kOpenbasketNotification object:nil];
}

- (void)showBasket
{
    [self.wireFrame showBasketFrom:self.view];
    [[NSNotificationCenter defaultCenter] postNotificationName:kSetRootBasketNotification object:nil];
}

- (NSArray *)updateSelectTypeForData:(NSArray *)data
{
    return [[data mutableCopy] bk_map:^id(NITCatalogItemViewModel *obj) {
        obj.selectType = self.selectType;
        return obj;
    }];
}

@end
