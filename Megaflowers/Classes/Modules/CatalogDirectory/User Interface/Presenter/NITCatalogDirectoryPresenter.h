//
//  NITCatalogDirectoryPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogDirectoryProtocols.h"

@class NITCatalogDirectoryWireFrame;

@interface NITCatalogDirectoryPresenter : NITRootPresenter <NITCatalogDirectoryPresenterProtocol, NITCatalogDirectoryInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogDirectoryViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogDirectoryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogDirectoryWireFrameProtocol> wireFrame;
@property (nonatomic) ProductContentType contentType;
@property (nonatomic) ProductSelectType selectType;
@property (nonatomic) NSNumber * discountId;

@end
