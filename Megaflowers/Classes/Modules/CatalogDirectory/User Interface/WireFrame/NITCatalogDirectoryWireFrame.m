//
//  NITCatalogDirectoryWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryWireFrame.h"
#import "NITFiltersWireFrame.h"
#import "NITPhotoSliderController.h"
#import "NITProductDetailsWireFrame.h"
#import "NITCallOrderWireFrame.h"
#import "NITAuthorizationWireFrame.h"

@implementation NITCatalogDirectoryWireFrame

#pragma mark - Create
+ (id)createNITCatalogDirectoryModuleWithParentID:(NSNumber *)parentID
{
    id <NITCatalogDirectoryPresenterProtocol, NITCatalogDirectoryInteractorOutputProtocol> presenter = [NITCatalogDirectoryPresenter new];
    id <NITCatalogDirectoryInteractorInputProtocol> interactor = [NITCatalogDirectoryInteractor new];
    id <NITCatalogDirectoryAPIDataManagerInputProtocol> APIDataManager = [NITCatalogDirectoryAPIDataManager new];
    id <NITCatalogDirectoryLocalDataManagerInputProtocol> localDataManager = [NITCatalogDirectoryLocalDataManager new];
    id <NITCatalogDirectoryWireFrameProtocol> wireFrame= [NITCatalogDirectoryWireFrame new];
    id <NITCatalogDirectoryViewProtocol> view = [(NITCatalogDirectoryWireFrame *)wireFrame createViewControllerWithKey:_s(NITCatalogDirectoryViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    interactor.parentID = parentID;
    
    return view;
}

#pragma mark - Discounts
+ (void)presentNITStockDirectoryModuleFrom:(UIViewController *)fromViewController withTitle:(NSString *)title discountId:(NSNumber *)discountId
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModuleWithParentID:nil];
    view.title = title;
    view.presenter.contentType = ProductContentTypeBouqets;
    view.presenter.selectType = ProductSelectTypeView;
    view.presenter.discountId = discountId;
    [fromViewController.navigationController pushViewController:view animated:true];
}

#pragma mark - Bouquets
+ (void)presentNITCatalogDirectoryModuleFrom:(UIViewController *)fromViewController withTitle:(NSString *)title parentID:(NSNumber *)parentID
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModuleWithParentID:parentID];
    view.title = title;
    view.presenter.contentType = ProductContentTypeBouqets;
    view.presenter.selectType = ProductSelectTypeView;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)selectNITCatalogDirectoryFrom:(UIViewController *)fromViewController withTitle:(NSString *)title parentID:(NSNumber *)parentID
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModuleWithParentID:parentID];
    view.title = title;
    view.presenter.contentType = ProductContentTypeBouqets;
    view.presenter.selectType = ProductSelectTypeSelect;
    [fromViewController.navigationController pushViewController:view animated:true];
}

#pragma mark - Attach
+ (void)presentNITAttachDirectoryModuleFrom:(UIViewController *)fromViewController withTitle:(NSString *)title parentID:(NSNumber *)parentID
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModuleWithParentID:parentID];
    view.title = title;
    view.presenter.contentType = ProductContentTypeAttach;
    view.presenter.selectType = ProductSelectTypeView;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)selectNITAttachModuleFrom:(UIViewController *)fromViewController withTitle:(NSString *)title parentID:(NSNumber *)parentID
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModuleWithParentID:parentID];
    view.title = title;
    view.presenter.contentType = ProductContentTypeAttach;
    view.presenter.selectType = ProductSelectTypeSelect;
    [fromViewController.navigationController pushViewController:view animated:true];
}

#pragma mark - Back
- (void)backActionFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

#pragma mark - Filters
- (void)showFiltersFrom:(UIViewController *)fromViewController parentID:(NSNumber *)parentID
{
    [NITFiltersWireFrame presentNITFiltersModuleFrom:fromViewController withParentID:parentID];
}

#pragma mark - Image
- (void)showImages:(NSArray <NSString *> *)imagesPaths from:(UIViewController *)fromViewController
{
    NITPhotoSliderController * photoVC = [[NITPhotoSliderController alloc] initWithImagePaths:imagesPaths];
    [fromViewController presentViewController:photoVC animated:true completion:nil];
}

#pragma mark - Details
- (void)showDetailsPreviewByModel:(NITCatalogItemViewModel *)model isViewMode:(BOOL)isViewMode
{
    [NITProductDetailsWireFrame presentNITProductDetailsPreviewModuleWithBouquet:model isViewMode:isViewMode];
}

- (void)showDetailsByModel:(NITCatalogItemViewModel *)model from:(UIViewController *)fromViewController
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController bouquet:model selectType:ProductSelectTypeView];
}

- (void)selectDetailsByModel:(NITCatalogItemViewModel *)model from:(UIViewController *)fromViewController
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController bouquet:model selectType:ProductSelectTypeSelect];
}

- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model selectType:(ProductSelectType)selectType from:(UIViewController *)fromViewController
{
    return [NITProductDetailsWireFrame createProductDetailsByModel:model selectType:selectType from:fromViewController];
}

#pragma mark - Call
- (void)showCallOrderFrom:(UIViewController *)fromViewController
{
    [NITCallOrderWireFrame presentNITCallOrderModuleFrom:fromViewController];
}

#pragma mark - Register
- (void)showRegisterFrom:(UIViewController *)fromViewController
{
    [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:fromViewController];
}

#pragma mark - Basket
- (void)showBasketFrom:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:1];
}

@end
