//
//  NITCatalogDirectoryWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogDirectoryProtocols.h"
#import "NITCatalogDirectoryViewController.h"
#import "NITCatalogDirectoryLocalDataManager.h"
#import "NITCatalogDirectoryAPIDataManager.h"
#import "NITCatalogDirectoryInteractor.h"
#import "NITCatalogDirectoryPresenter.h"
#import "NITCatalogDirectoryWireframe.h"
#import "NITRootWireframe.h"

@interface NITCatalogDirectoryWireFrame : NITRootWireframe <NITCatalogDirectoryWireFrameProtocol>

@end
