//
//  NITCatalogDirectoryViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryViewController.h"
#import "NITCatalogDirectoryCell.h"
#import "NITCatalogDirectoryFilterView.h"
#import "NITCatalogDirectoryCell.h"
#import "NITProductDetailsViewController.h"
#import "NITGuideFiltersView.h"
#import "NITGuideBouquetPreviewView.h"
#import "NITCatalogItemsView.h"

@interface NITCatalogDirectoryViewController ()
<
UIViewControllerPreviewingDelegate,
HTHorizontalSelectionListDelegate,
HTHorizontalSelectionListDataSource,
NITCatalogItemsViewDelegate
>

@property (nonatomic) IBOutlet NITCatalogItemsView * catalogItemsView;
@property (nonatomic) IBOutlet HTHorizontalSelectionList * subCategoryList;

@property (nonatomic) IBOutlet UIScrollView * filtersView;
@property (nonatomic) IBOutlet UIButton * filterBtn;

@property (nonatomic) IBOutlet UILabel * sortTitle;
@property (nonatomic) IBOutlet UIBarButtonItem * changeModeBtn;
@property (nonatomic) IBOutlet UIView * noResultsView;

@property (nonatomic) IBOutlet NSLayoutConstraint * sortTitleW;
@property (nonatomic) IBOutlet NSLayoutConstraint * subCategoryListH;
@property (nonatomic) IBOutlet NSLayoutConstraint * filtersViewH;
@property (nonatomic) IBOutlet NSLayoutConstraint * top;
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray * topInsetConstraints;
@property (nonatomic) IBOutletCollection(UIView) NSArray * topInsetElements;

@property (nonatomic) CatalogItemsViewMode catalogItemsViewMode;
@property (nonatomic) NSArray <NITCategoryViewModel *> * subCategoryItems;
@property (nonatomic) NSArray <NITCatalogFilterViewModel *> * filterItems;
@property (nonatomic) CGFloat topInset;
@property (nonatomic) CGFloat lastYOffset;
@property (nonatomic) BOOL userScrolled;

@end

@implementation NITCatalogDirectoryViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.presenter viewWillAppear];
    [self reloadView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.presenter viewWillDisappear];
}

#pragma mark - Custom accessors
- (void)setCatalogItemsViewMode:(CatalogItemsViewMode)viewMode
{
    _catalogItemsViewMode = viewMode;
    USER.viewType = viewMode;
}

#pragma mark - IBAction
- (IBAction)showFilters:(id)sender
{
    [self.presenter showFilters];
}

- (IBAction)sortAction:(id)sender
{
    [self.presenter sortAction];
}

- (IBAction)callAction:(id)sender
{
    [self.presenter callAction];
}

- (IBAction)changeViewModeAction:(id)sender
{
    switch (self.catalogItemsViewMode) {
        case CatalogItemsViewModeList:      self.catalogItemsViewMode = CatalogItemsViewModeSection;    break;
        case CatalogItemsViewModeSection:   self.catalogItemsViewMode = CatalogItemsViewModeList;       break;
    }

    [self reloadViewMode];
}

#pragma mark - Private
- (void)initUI
{
    // sub category list
    self.subCategoryList.delegate = self;
    self.subCategoryList.dataSource = self;
    self.subCategoryList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeNoBounce;
    self.subCategoryList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.subCategoryList.bottomTrimHidden = true;
    self.subCategoryList.selectionIndicatorColor = RGB(10, 88, 43);
    self.subCategoryList.backgroundColor = RGB(240, 245, 242);
    
    [self.subCategoryList setTitleColor:RGB(10, 88, 43) forState:UIControlStateNormal];
    [self.subCategoryList setTitleFont:[UIFont systemFontOfSize:14 weight:UIFontWeightLight] forState:UIControlStateNormal];
    [self.subCategoryList setTitleFont:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium] forState:UIControlStateSelected];
    
    [self.filterBtn setHidden:self.presenter.contentType != ProductTypeBouquet];
    
    // Register for 3D Touch Previewing if available
    if ([HELPER forceTouchAvailable])
    {
        [self registerForPreviewingWithDelegate:self sourceView:self.catalogItemsView.collectionView];
    }
    
    // View mode
    self.catalogItemsViewMode = USER.viewType;
    [self reloadViewMode];
}

- (void)reloadViewMode
{
    self.userScrolled = false;
    [self.changeModeBtn setImage:[UIImage imageNamed:self.catalogItemsViewMode == CatalogItemsViewModeList ? @"ic_gallery" : @"ic_gallery_pl"]];
    [self.catalogItemsView updateViewMode:self.catalogItemsViewMode];
}

- (void)updateFilters
{
    self.filtersViewH.constant = self.filterItems.count > 0 ? 40 : 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self updateTopInset];
    
    CGFloat x = 0;
    [self.filtersView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (NITCatalogFilterViewModel * model in self.filterItems)
    {
        NITCatalogDirectoryFilterView * view = [NITCatalogDirectoryFilterView viewByModel:model andPositionX:x];
        [self.filtersView addSubview:view];
        x += ViewWidth(view);
    }
    
    self.filtersView.contentSize = CGSizeMake(x, ViewHeight(self.filtersView));
}

- (void)updateTopElementsVisibilyty:(BOOL)show
{
    [self.view layoutIfNeeded];
    
    self.top.constant = show ? 0 : - self.topInset;
    [UIView animateWithDuration:0.6 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)updateTopInset
{
    self.topInset = [[self.topInsetConstraints valueForKeyPath:@"@sum.constant"] floatValue];
    [self setScrollViewTopInset:self.topInset];
}

- (void)setScrollViewTopInset:(CGFloat)inset
{
    [self.catalogItemsView.collectionView setContentInset:UIEdgeInsetsMake(inset, 0, 0, 0)];
    
    if (self.catalogItemsView.collectionView.contentOffset.y <= inset && !self.userScrolled)
    {
        [self.catalogItemsView.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:true];
    }

    [self updateTopElementsVisibilyty:inset > 0];
}

- (void)hideOrShowTopBarWhenScroll:(UIScrollView *)scrollView
{
    CGFloat y = scrollView.contentOffset.y;
    CGFloat height = scrollView.contentSize.height;
    
    if (height > ViewHeight(scrollView) && y > 0 && y < (height - ViewHeight(scrollView)))
    {
        if (self.lastYOffset > y)
        {
            if (scrollView.contentInset.top == 0)
            {
                [self setScrollViewTopInset:self.topInset];
            }
        }
        else if (self.lastYOffset < y)
        {
            if (scrollView.contentInset.top > 0)
            {
                [self setScrollViewTopInset:0];
            }
        }
    }
    
    self.lastYOffset = y;
}

- (void)showGuideIfNeed
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if ([NITGuideFiltersView notYetShown])
        {
            [NITGuideFiltersView showWidthOverlayView:self.filterBtn withHandler:^{
                
                if ([NITGuideBouquetPreviewView notYetShown])
                {
                    [NITGuideBouquetPreviewView show];
                }
            }];
        }
    });
}

#pragma mark - NITFiltersPresenterProtocol
- (void)reloadView
{
    [self.catalogItemsView reloadView];
}

- (void)reloadDataByModels:(NSArray<NITCatalogItemViewModel *> *)models
{
    [self.noResultsView setHidden:models.count > 0];
    [self.catalogItemsView reloadCatalogItemsByModels:models delegate:self];
    [self showGuideIfNeed];
}

- (void)insertDataByModels:(NSArray<NITCatalogItemViewModel *> *)models
{
    [self.catalogItemsView insertDataByModels:models lastYOffset:self.lastYOffset];
}

- (void)reloadDataBySubCategoryModels:(NSArray<NITCategoryViewModel *> *)models
{
    self.subCategoryItems = models;
    
    BOOL show = (models != nil && models.count > 0);
    self.subCategoryListH.constant = show ? 43 : 0;
    [self.view layoutIfNeeded];
    
    [self updateTopInset];
    
    if (show)
    {
        [self.subCategoryList reloadData];
        [self.subCategoryList setSelectedButtonIndex:0];
    }
}

- (void)reloadFilterdDataByModels:(NSArray<NITCatalogFilterViewModel *> *)models
{
    self.filterItems = models;
    [self updateFilters];
}

- (void)reloadSortTitle:(NSString *)title
{
    self.sortTitle.text = title;
    self.sortTitleW.constant = [self.sortTitle sizeOfMultiLineLabel].width;
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [self.subCategoryItems count];
}

- (nullable NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index
{
    NITCategoryViewModel * model = self.subCategoryItems[index];
    return model.title;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    [self.presenter updateDataBySubCategoryModel:self.subCategoryItems[index]];
    [self.subCategoryList reloadData];
}

#pragma mark - NITCatalogItemsViewDelegate
- (void)catalogTableWillBeginDecelerating:(UIScrollView *)catalogTable
{
    self.userScrolled = true;
}

- (void)catalogTableDidScroll:(UIScrollView *)catalogTable
{
    [self hideOrShowTopBarWhenScroll:catalogTable];
    
    if (catalogTable.isBottom)
    {
        [self.presenter checkLoadNextDataByOffset:[self.catalogItemsView.items count]];
    }
}

#pragma mark - UIViewControllerPreviewingDelegate
- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location
{
    NITCatalogItemViewModel * model = self.catalogItemsView.items[[self.catalogItemsView.collectionView indexPathForItemAtPoint:location].row];
    previewingContext.sourceRect = CGRectZero;
    
    return [self.presenter createProductDetailsByModel:model];
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit
{
    UINavigationController * navVC = (UINavigationController *)viewControllerToCommit;
    [navVC setNavigationBarHidden:false];
    
    NITProductDetailsViewController * vc = (NITProductDetailsViewController *)navVC.topViewController;
    [vc hidePreview];
    
    [self showDetailViewController:viewControllerToCommit sender:self];
}

@end
