//
//  NITCatalogDirectoryFilterView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITCatalogFilterViewModel;

@interface NITCatalogDirectoryFilterView : UIView

@property (nonatomic) NITCatalogFilterViewModel * model;

+ (instancetype)viewByModel:(NITCatalogFilterViewModel *)model andPositionX:(CGFloat)positionX;

@end
