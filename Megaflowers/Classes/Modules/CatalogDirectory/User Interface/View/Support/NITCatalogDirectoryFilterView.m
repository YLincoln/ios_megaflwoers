//
//  NITCatalogDirectoryFilterView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryFilterView.h"
#import "NITCatalogFilterViewModel.h"

@interface NITCatalogDirectoryFilterView ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton *closeBtn;

@end

@implementation NITCatalogDirectoryFilterView

+ (instancetype)viewByModel:(NITCatalogFilterViewModel *)model andPositionX:(CGFloat)positionX
{
    NITCatalogDirectoryFilterView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITCatalogDirectoryFilterView) owner:self options:nil] firstObject];
    view.model = model;
    view.frame = [view rectByPositionX:positionX];

    return view;
}

- (void)setModel:(NITCatalogFilterViewModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [UIView animateWithDuration:0.5f
                          delay:0.f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [self.model removeFilter];
                     }];
}

#pragma mark - Private
- (void)updateData
{
    self.tag = [self.model.identifier integerValue];
    self.title.text = self.model.title;
}

- (CGRect)rectByPositionX:(CGFloat)positionX
{
    return CGRectMake(positionX, 0, [self.title sizeOfMultiLineLabel].width + 33, 40);
}

@end
