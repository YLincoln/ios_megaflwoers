//
//  NITCatalogDirectoryViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogDirectoryProtocols.h"
#import "NITBaseViewController.h"

@interface NITCatalogDirectoryViewController : NITBaseViewController <NITCatalogDirectoryViewProtocol>

@property (nonatomic, strong) id <NITCatalogDirectoryPresenterProtocol> presenter;

@end
