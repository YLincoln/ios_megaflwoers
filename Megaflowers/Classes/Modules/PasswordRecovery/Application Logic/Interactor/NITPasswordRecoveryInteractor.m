//
//  NITPasswordRecoveryInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPasswordRecoveryInteractor.h"

@implementation NITPasswordRecoveryInteractor

- (void)recoveryPasswordByPhone:(NSString *)phone withHandler:(void (^)(BOOL success, NSString *token, NSString *error))handler
{
    [self.APIDataManager recoveryByPhone:phone withHandler:^(NSString *token, NSString *error) {
        handler(token ? true : false, token, error);
    }];
}

@end
