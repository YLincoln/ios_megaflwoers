//
//  NITPasswordRecoveryInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPasswordRecoveryProtocols.h"

@interface NITPasswordRecoveryInteractor : NSObject <NITPasswordRecoveryInteractorInputProtocol>

@property (nonatomic, weak) id <NITPasswordRecoveryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPasswordRecoveryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPasswordRecoveryLocalDataManagerInputProtocol> localDataManager;

@end
