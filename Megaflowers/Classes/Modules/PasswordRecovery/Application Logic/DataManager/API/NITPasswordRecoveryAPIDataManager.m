//
//  NITPasswordRecoveryAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPasswordRecoveryAPIDataManager.h"
#import "SWGAccountApi.h"

@implementation NITPasswordRecoveryAPIDataManager

- (void)recoveryByPhone:(NSString *)phone withHandler:(void(^)(NSString * token, NSString * error))handler
{
    [HELPER startLoading];
    [[[SWGAccountApi alloc] initWithApiClient:[SWGApiClient sharedClient]] accountRecoveryPhonePostWithPhone:phone lang:HELPER.lang site:API.site completionHandler:^(NSString *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:THIS_METHOD];
        }
        handler(output, error.interfaceDescription);
    }];
}

@end
