//
//  NITPasswordRecoveryProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITPasswordRecoveryInteractorOutputProtocol;
@protocol NITPasswordRecoveryInteractorInputProtocol;
@protocol NITPasswordRecoveryViewProtocol;
@protocol NITPasswordRecoveryPresenterProtocol;
@protocol NITPasswordRecoveryLocalDataManagerInputProtocol;
@protocol NITPasswordRecoveryAPIDataManagerInputProtocol;

@class NITPasswordRecoveryWireFrame;

@protocol NITPasswordRecoveryViewProtocol
@required
@property (nonatomic, strong) id <NITPasswordRecoveryPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadPhone:(NSString *)phone;
@end

@protocol NITPasswordRecoveryWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITPasswordRecoveryModuleFrom:(id)fromView phone:(NSString *)phone;
- (void)backActionFrom:(id)fromView;
- (void)showCheckSMSFrom:(id)fromView withDelegate:(id)delegate phone:(NSString *)phone token:(NSString *)token;
@end

@protocol NITPasswordRecoveryPresenterProtocol
@required
@property (nonatomic, weak) id <NITPasswordRecoveryViewProtocol> view;
@property (nonatomic, strong) id <NITPasswordRecoveryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPasswordRecoveryWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * phone;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)passwordRecoveryByPhone:(NSString *)phone;
@end

@protocol NITPasswordRecoveryInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITPasswordRecoveryInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPasswordRecoveryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPasswordRecoveryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPasswordRecoveryLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)recoveryPasswordByPhone:(NSString *)phone withHandler:(void (^)(BOOL success, NSString * token, NSString * error))handler;
@end


@protocol NITPasswordRecoveryDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITPasswordRecoveryAPIDataManagerInputProtocol <NITPasswordRecoveryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)recoveryByPhone:(NSString *)phone withHandler:(void(^)(NSString * token, NSString * error))handler;
@end

@protocol NITPasswordRecoveryLocalDataManagerInputProtocol <NITPasswordRecoveryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
