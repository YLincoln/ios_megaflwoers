//
//  NITPasswordRecoveryWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPasswordRecoveryWireFrame.h"
#import "NITCheckSMSWireFrame.h"

@implementation NITPasswordRecoveryWireFrame

+ (id)createNITPasswordRecoveryModule
{
    // Generating module components
    id <NITPasswordRecoveryPresenterProtocol, NITPasswordRecoveryInteractorOutputProtocol> presenter = [NITPasswordRecoveryPresenter new];
    id <NITPasswordRecoveryInteractorInputProtocol> interactor = [NITPasswordRecoveryInteractor new];
    id <NITPasswordRecoveryAPIDataManagerInputProtocol> APIDataManager = [NITPasswordRecoveryAPIDataManager new];
    id <NITPasswordRecoveryLocalDataManagerInputProtocol> localDataManager = [NITPasswordRecoveryLocalDataManager new];
    id <NITPasswordRecoveryWireFrameProtocol> wireFrame= [NITPasswordRecoveryWireFrame new];
    id <NITPasswordRecoveryViewProtocol> view = [(NITPasswordRecoveryWireFrame *)wireFrame createViewControllerWithKey:_s(NITPasswordRecoveryViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;

    return view;
}

#pragma mark - NITPasswordRecoveryWireFrameProtocol
+ (void)presentNITPasswordRecoveryModuleFrom:(UIViewController *)fromViewController phone:(NSString *)phone
{
    NITPasswordRecoveryViewController * view = [NITPasswordRecoveryWireFrame createNITPasswordRecoveryModule];
    view.presenter.phone = phone;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromView
{
    [fromView.navigationController popViewControllerAnimated:true];
}

- (void)showCheckSMSFrom:(UIViewController *)fromViewController withDelegate:(id)delegate phone:(NSString *)phone token:(NSString *)token
{
    [NITCheckSMSWireFrame presentNITCheckSMSModuleFrom:fromViewController
                                          withDelegate:delegate
                                                  mode:SMSRecoveryModeResetPassword
                                                 phone:phone
                                                 token:token];
}

@end
