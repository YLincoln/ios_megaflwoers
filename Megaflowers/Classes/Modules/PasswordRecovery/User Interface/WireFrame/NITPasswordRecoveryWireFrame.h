//
//  NITPasswordRecoveryWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPasswordRecoveryProtocols.h"
#import "NITPasswordRecoveryViewController.h"
#import "NITPasswordRecoveryLocalDataManager.h"
#import "NITPasswordRecoveryAPIDataManager.h"
#import "NITPasswordRecoveryInteractor.h"
#import "NITPasswordRecoveryPresenter.h"
#import "NITPasswordRecoveryWireframe.h"
#import "NITRootWireframe.h"

@interface NITPasswordRecoveryWireFrame : NITRootWireframe <NITPasswordRecoveryWireFrameProtocol>

@end
