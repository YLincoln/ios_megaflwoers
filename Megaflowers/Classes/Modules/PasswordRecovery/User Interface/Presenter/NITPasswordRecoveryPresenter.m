//
//  NITPasswordRecoveryPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPasswordRecoveryPresenter.h"
#import "NITPasswordRecoveryWireframe.h"

@implementation NITPasswordRecoveryPresenter

#pragma mark - NITPasswordRecoveryPresenterProtocol
- (void)initData
{
    if (self.phone.length > 0)
    {
        [self.view reloadPhone:self.phone];
    }
}

- (void)passwordRecoveryByPhone:(NSString *)phone
{
    if (phone.length > 0)
    {
        weaken(self);
        [self.interactor recoveryPasswordByPhone:phone withHandler:^(BOOL success, NSString *token, NSString *error) {
            
            if (success)
            {
                USER.phone = phone;
                [weakSelf.wireFrame showCheckSMSFrom:weakSelf.view withDelegate:weakSelf phone:phone token:token];
            }
            else
            {
                [self showError:error fromView:self.view];
            }
        }];
    }
    else
    {
        [self showError:NSLocalizedString(@"Заполните номер телефона!", nil) fromView:self.view];
    }
}


@end
