//
//  NITPasswordRecoveryPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPasswordRecoveryProtocols.h"

@class NITPasswordRecoveryWireFrame;

@interface NITPasswordRecoveryPresenter : NITRootPresenter <NITPasswordRecoveryPresenterProtocol, NITPasswordRecoveryInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPasswordRecoveryViewProtocol> view;
@property (nonatomic, strong) id <NITPasswordRecoveryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPasswordRecoveryWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * phone;

@end
