//
//  NITPasswordRecoveryViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITPasswordRecoveryProtocols.h"

@interface NITPasswordRecoveryViewController : NITBaseViewController <NITPasswordRecoveryViewProtocol>

@property (nonatomic, strong) id <NITPasswordRecoveryPresenterProtocol> presenter;

@end
