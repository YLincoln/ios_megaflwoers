//
//  NITPasswordRecoveryViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/24/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPasswordRecoveryViewController.h"
#import "AKNumericFormatter.h"

@interface NITPasswordRecoveryViewController ()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet PhoneTextField * phoneField;

@end

@implementation NITPasswordRecoveryViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)passwordRecoveryAction:(id)sender
{
    [self.presenter passwordRecoveryByPhone:self.phoneField.text];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Восстановление пароля", nil);
    
    [self setKeyboardActiv:true];
    
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

#pragma mark - NITPasswordRecoveryPresenterProtocol
- (void)reloadPhone:(NSString *)phone
{
    self.phoneField.text = phone;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

@end
