//
//  NITСouponsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITСouponsInteractorOutputProtocol;
@protocol NITСouponsInteractorInputProtocol;
@protocol NITСouponsViewProtocol;
@protocol NITСouponsPresenterProtocol;
@protocol NITСouponsLocalDataManagerInputProtocol;
@protocol NITСouponsAPIDataManagerInputProtocol;

@class NITСouponsWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITСouponsViewProtocol
@required
@property (nonatomic, strong) id <NITСouponsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITСouponsWireFrameProtocol
@required
+ (void)presentNITСouponsModuleFrom:(id)fromView;
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@end

@protocol NITСouponsPresenterProtocol
@required
@property (nonatomic, weak) id <NITСouponsViewProtocol> view;
@property (nonatomic, strong) id <NITСouponsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITСouponsWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
@end

@protocol NITСouponsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITСouponsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITСouponsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITСouponsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITСouponsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITСouponsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITСouponsAPIDataManagerInputProtocol <NITСouponsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITСouponsLocalDataManagerInputProtocol <NITСouponsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
