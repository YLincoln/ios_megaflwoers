//
//  NITСouponsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITСouponsProtocols.h"

@class NITСouponsWireFrame;

@interface NITСouponsPresenter : NSObject <NITСouponsPresenterProtocol, NITСouponsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITСouponsViewProtocol> view;
@property (nonatomic, strong) id <NITСouponsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITСouponsWireFrameProtocol> wireFrame;

@end
