//
//  NITСouponsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITСouponsProtocols.h"

@interface NITСouponsViewController : UIViewController <NITСouponsViewProtocol>

@property (nonatomic, strong) id <NITСouponsPresenterProtocol> presenter;

@end
