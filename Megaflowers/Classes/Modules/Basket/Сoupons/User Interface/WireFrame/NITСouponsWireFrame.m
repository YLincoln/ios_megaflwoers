//
//  NITСouponsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITСouponsWireFrame.h"

@implementation NITСouponsWireFrame

+ (void)presentNITСouponsModuleFrom:(UIViewController*)fromViewController {

    // Generating module components
    id <NITСouponsViewProtocol> view = [[NITСouponsViewController alloc] init];
    id <NITСouponsPresenterProtocol, NITСouponsInteractorOutputProtocol> presenter = [NITСouponsPresenter new];
    id <NITСouponsInteractorInputProtocol> interactor = [NITСouponsInteractor new];
    id <NITСouponsAPIDataManagerInputProtocol> APIDataManager = [NITСouponsAPIDataManager new];
    id <NITСouponsLocalDataManagerInputProtocol> localDataManager = [NITСouponsLocalDataManager new];
    id <NITСouponsWireFrameProtocol> wireFrame= [NITСouponsWireFrame new];

    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;

    //TODO: ViewController presentation
}

@end
