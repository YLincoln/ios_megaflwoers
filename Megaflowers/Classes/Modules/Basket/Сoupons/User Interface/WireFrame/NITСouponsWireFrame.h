//
//  NITСouponsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/08/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITСouponsProtocols.h"
#import "NITСouponsViewController.h"
#import "NITСouponsLocalDataManager.h"
#import "NITСouponsAPIDataManager.h"
#import "NITСouponsInteractor.h"
#import "NITСouponsPresenter.h"
#import "NITСouponsWireframe.h"
#import <UIKit/UIKit.h>

@interface NITСouponsWireFrame : NSObject <NITСouponsWireFrameProtocol>

@end
