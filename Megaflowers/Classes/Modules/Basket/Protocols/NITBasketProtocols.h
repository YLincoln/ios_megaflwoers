//
//  NITBasketProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketProductModel.h"
#import "NITBasketProductAddModel.h"
#import "NITCouponModel.h"
#import "NITCoupon.h"
#import "NITDeliveryMethodModel.h"
#import "NITBusketAttachment.h"
#import "NITBusketBouquet.h"
#import "NITBasketSectionModel.h"
#import "SWGOrder.h"

@protocol NITBasketInteractorOutputProtocol;
@protocol NITBasketInteractorInputProtocol;
@protocol NITBasketViewProtocol;
@protocol NITBasketPresenterProtocol;
@protocol NITBasketLocalDataManagerInputProtocol;
@protocol NITBasketAPIDataManagerInputProtocol;

@class NITBasketWireFrame;

typedef CF_ENUM (NSUInteger, OrderButtonState) {
    OrderButtonStateAvailable       = 0,
    OrderButtonStateNotAvailable    = 1,
    OrderButtonStateInactive        = 2,
    OrderButtonStateDisable         = 3
};

@protocol NITBasketViewProtocol
@required
@property (nonatomic, strong) id <NITBasketPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray<NITBasketSectionModel *> *)models availabilityOrderSection:(BOOL)available;
- (void)updateTotalPrice:(NSNumber *)price;
- (void)updateOrderButtonState:(OrderButtonState)state;
- (void)reloadTableAvailability:(BOOL)available;
@end

@protocol NITBasketWireFrameProtocol
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@required
+ (id)createNITBasketModule;
+ (void)presentNITBasketModuleFrom:(id)fromView;
- (void)showSelectBouqetFrom:(id)fromView;
- (void)showSelectAttachFrom:(id)fromView;
- (void)showSelectCouponFrom:(id)fromView;
- (void)showOrderModuleFrom:(id)fromView withDeliveryType:(DeliveryMethodType)deliveryType;
- (void)showRegisterFrom:(id)fromView;
- (void)showProductDetailsFrom:(id)fromView forModel:(NITCatalogItemViewModel *)model;
- (void)setRootController:(id)fromView;
@end

@protocol NITBasketPresenterProtocol
@required
@property (nonatomic, weak) id <NITBasketViewProtocol> view;
@property (nonatomic, strong) id <NITBasketInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBasketWireFrameProtocol> wireFrame;
@property (nonatomic) NSArray <NITBasketSectionModel *> * sections;
@property (nonatomic) OrderButtonState orderButtonState;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updateData;
- (void)cleanBasket;
- (void)orderAction;
- (void)selectNewCoupon;
- (void)selectNewBouqet;
- (void)selectNewAttach;
- (void)removeProduct:(id)product;
- (void)updateCountForProduct:(NITBasketProductModel *)product;
- (CGFloat)deliveryHeightWithState:(BOOL)enabled forTable:(UITableView *)table;
- (void)showControllerFrom3DTouch;
- (void)editeProductModel:(NITBasketProductModel *)model byType:(BasketTableSectionType)type;
@end

@protocol NITBasketInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITBasketInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITBasketInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBasketAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBasketLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (NSArray<NITBasketProductModel *> *)getBouqetsDataModels;
- (NSArray<NITBasketProductModel *> *)getAttachDataModels;
- (NSArray<NITCouponModel *> *)getCouponsDataModels;
- (void)cleanBasket;
- (void)deleteBouquet:(NITBasketProductModel *)model;
- (void)deleteAttachment:(NITBasketProductModel *)model;
- (void)deleteCoupon:(NITCouponModel *)model;
- (void)updateCountForProduct:(NITBasketProductModel *)product;
- (void)getBouquetModelByID:(NSNumber *)identifier andHandler:(void (^)(NITCatalogItemViewModel * model))handler;
- (void)getAttachmentModelByID:(NSNumber *)identifier andHandler:(void (^)(NITCatalogItemViewModel * model))handler;
- (void)checkSalonsWithHandler:(void(^)(BOOL isSalonsAvailable))handler;
- (void)getCurrentOrderByID:(NSNumber *)orderId withHandler:(void(^)(SWGOrder *order))handler;
- (void)createPreOrderIfNeedWithHandler:(void(^)())handler;
@end


@protocol NITBasketDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITBasketAPIDataManagerInputProtocol <NITBasketDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITBasketLocalDataManagerInputProtocol <NITBasketDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NITCoupon *)getCoupon;
- (void)deleteCoupon;
- (void)deleteCoupons;
- (NSArray <NITBusketBouquet *> *)allBouquets;
- (void)deleteBouquets;
- (void)deleteBouquetByModel:(NITBasketProductModel *)model;
- (void)updateCountForProduct:(NITBasketProductModel *)model;
- (NSArray <NITBusketAttachment *> *)allAttachments;
- (void)deleteAttachments;
- (void)deleteAttachmentByModel:(NITBasketProductModel *)model;


@end
