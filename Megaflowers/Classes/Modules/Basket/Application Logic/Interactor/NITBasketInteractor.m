//
//  NITBasketInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITSalonsAPIDataManager.h"
#import "NITProductDetailsAPIDataManager.h"
#import "NSArray+BlocksKit.h"
#import "NITOrderAPIDataManager.h"
#import "NITOrderDetailsAPIDataManager.h"

@interface NITBasketInteractor ()

@end

@implementation NITBasketInteractor

- (NSArray<NITBasketProductModel *> *)getBouqetsDataModels
{
    NSArray * items = [self.localDataManager allBouquets];
    return [self bouquetsModelsFromNITBusketBouquets:items];
}

- (NSArray<NITBasketProductModel *> *)getAttachDataModels
{
    NSArray * items = [self.localDataManager allAttachments];
    return [self attachmentsModelsFromNITBusketAttachments:items];
}

- (NSArray<NITCouponModel *> *)getCouponsDataModels
{
    NITCoupon * coupon = [self.localDataManager getCoupon];
    
    if (coupon)
    {
        return @[[self couponModelFromNITCoupon:coupon]];
    }

    return nil;
}

- (void)cleanBasket
{
    [self.localDataManager deleteCoupons];
    [self.localDataManager deleteBouquets];
    [self.localDataManager deleteAttachments];
}

- (void)deleteBouquet:(NITBasketProductModel *)model
{
    [self.localDataManager deleteBouquetByModel:model];
}

- (void)updateCountForProduct:(NITBasketProductModel *)product
{
    [self.localDataManager updateCountForProduct:product];
}

- (void)deleteAttachment:(NITBasketProductModel *)model
{
    [self.localDataManager deleteAttachmentByModel:model];
}

- (void)deleteCoupon:(NITCouponModel *)model
{
    [self.localDataManager deleteCoupon];
}

- (void)checkSalonsWithHandler:(void(^)(BOOL isSalonsAvailable))handler
{
    NITSalonsAPIDataManager * salonsApi = [[NITSalonsAPIDataManager alloc] init];
    [salonsApi getUserSalonsByCityId:USER.cityID withHandler:^(NSArray<SWGSalon> *result){
        handler(result.count > 0);
    }];
}

- (void)getBouquetModelByID:(NSNumber *)identifier andHandler:(void (^)(NITCatalogItemViewModel * model))handler
{
    NITProductDetailsAPIDataManager * productDirectoryAPIDataManager = [[NITProductDetailsAPIDataManager alloc] init];
    [productDirectoryAPIDataManager getBouquetsByID:identifier withHandler:^(SWGBouquet *output, NSError * error) {
        
        NITCatalogItemViewModel * bouquetModel;
        if (output) bouquetModel = [self bouquetModelFromSWGBouquet:output];
        
        handler(bouquetModel);
    }];
}

- (void)getAttachmentModelByID:(NSNumber *)identifier andHandler:(void (^)(NITCatalogItemViewModel * model))handler
{
    NITProductDetailsAPIDataManager * productDirectoryAPIDataManager = [[NITProductDetailsAPIDataManager alloc] init];
    [productDirectoryAPIDataManager getAttachByID:identifier withHandler:^(SWGAttach *output) {
        
        NITCatalogItemViewModel * attachModel;
        if (output) attachModel = [self attachModelFromSWGAttach:output];

         handler(attachModel);
     }];
}

- (void)getCurrentOrderByID:(NSNumber *)orderId withHandler:(void(^)(SWGOrder *order))handler
{
    [[NITOrderDetailsAPIDataManager new] getUserOrderByID:orderId withHandler:^(SWGOrder *result) {
        if (handler) handler(result);
    }];
}

- (void)createPreOrderIfNeedWithHandler:(void(^)())handler
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        
        if (success)
        {
            if (USER.orderID.integerValue == 0)
            {
                [[NITOrderAPIDataManager new] sendPreOrderByBasket:[API createBasket] withHandler:^(SWGInlineResponse201 *output, NSError *error) {
                    
                    if (!error) USER.orderID = output._id;
                    if (handler) handler();
                }];
            }
            else
            {
                [[NITOrderDetailsAPIDataManager new] getUserOrderByID:USER.orderID withHandler:^(SWGOrder *result) {
                    
                    if (result)
                    {
                        SWGOrder * order = [SWGOrder new];
                        order._id       = result._id;
                        order.orderHash = result.orderHash;
                        order.num       = @([result.num integerValue]);
                        order.basket    = (id)[API createBasket];
                        
                        [[NITOrderAPIDataManager new] sendOrderwithID:order._id order:(SWGRequestOrder *)order withHandler:^(BOOL success, NSError *error) {
                            if (handler) handler(success, error.interfaceDescription);
                        }];
                    }
                    else
                    {
                        USER.orderID = nil;
                        [self createPreOrderIfNeedWithHandler:handler];
                    }
                }];
            }
        }
        else
        {
            if (handler) handler();
        }
    }];
}

#pragma mark - Private
- (NITCouponModel *)couponModelFromNITCoupon:(NITCoupon *)coupon
{
    return [[NITCouponModel alloc] initWidthCoupon:coupon];
}

- (NSArray <NITBasketProductModel *> *)bouquetsModelsFromNITBusketBouquets:(NSArray <NITBusketBouquet *> *)bouquets
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBusketBouquet * bouquet in bouquets)
    {
        [result addObjectsFromArray:[NITBasketProductModel modelsFromBouquet:bouquet]];
    }
    
    return result;
}

- (NSArray <NITBasketProductModel *> *)attachmentsModelsFromNITBusketAttachments:(NSArray <NITBusketAttachment *> *)attachments
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBusketAttachment * attachment in attachments)
    {
        [result addObjectsFromArray:[NITBasketProductModel modelsFromAttachment:attachment]];
    }
    
    return result;
}

- (NITCatalogItemViewModel *)bouquetModelFromSWGBouquet:(SWGBouquet *)bouquet
{
    return [[NITCatalogItemViewModel alloc] initWithBouquet:bouquet];
}

- (NITCatalogItemViewModel *)attachModelFromSWGAttach:(SWGAttach *)attachment
{
    return [[NITCatalogItemViewModel alloc] initWithAttach:attachment];
}

@end
