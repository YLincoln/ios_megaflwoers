//
//  NITBasketProductAddModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketProductAddModel.h"

@implementation NITBasketProductAddModel

- (instancetype)initWithType:(BasketProductAddType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

- (void)setType:(BasketProductAddType)type
{
    _type = type;
    
    switch (type)
    {
        case BasketProductAddTypeBouqet:
            self.title = NSLocalizedString(@"Добавить букет", nil);
            break;
        case BasketProductAddTypeAttach:
            self.title = NSLocalizedString(@"Добавить дополнение", nil);
            break;
        case BasketProductAddTypeCoupon:
            self.title = NSLocalizedString(@"Добавить купон", nil);
            break;
    }
}

@end
