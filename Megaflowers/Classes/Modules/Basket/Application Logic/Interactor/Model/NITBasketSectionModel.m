//
//  NITBasketSectionModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/11/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketSectionModel.h"
#import "NITBasketProductCell.h"
#import "NITBasketCouponCell.h"
#import "NITBasketProductAddCell.h"
#import "NITDeliveryMethodCell.h"

@implementation NITBasketSectionModel

- (instancetype)initWithType:(BasketTableSectionType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (CGFloat)rowHeight
{
    switch (self.type)
    {
        case BasketTableSectionTypeBouqets:
        case BasketTableSectionTypeAttach:
            return 200;
        case BasketTableSectionTypeCoupons:
            return 134;
        case BasketTableSectionTypeButtons:
            return 57;
        case BasketTableSectionTypeDelivery:
            return _rowHeight;
    }
}

- (NSString *)cellID
{
    switch (self.type)
    {
        case BasketTableSectionTypeBouqets:
        case BasketTableSectionTypeAttach:
            return _s(NITBasketProductCell);
        case BasketTableSectionTypeCoupons:
            return _s(NITBasketCouponCell);
        case BasketTableSectionTypeButtons:
            return _s(NITBasketProductAddCell);
        case BasketTableSectionTypeDelivery:
            return _s(NITDeliveryMethodCell);
    }
}

#pragma mark - Public
+ (NITBasketSectionModel *)sectionByModel:(id)model fromSections:(NSArray *)sections
{
    for (NITBasketSectionModel * section in sections)
    {
        if ([section.models containsObject:model])
        {
            return section;
        }
    }
    
    return BasketTableSectionTypeBouqets;
}

+ (BOOL)productsIsNotEmptyFromSections:(NSArray *)sections
{
    return [sections[BasketTableSectionTypeBouqets] models].count > 0 || [sections[BasketTableSectionTypeAttach] models].count > 0;
}

- (void)addModel:(id)model
{
    NSMutableArray * array = [NSMutableArray arrayWithArray:self.models];
    [array addObject:model];
    self.models = array;
}

- (void)removeModel:(id)model
{
    NSMutableArray * array = [NSMutableArray arrayWithArray:self.models];
    [array removeObject:model];
    self.models = array;
}

@end
