//
//  NITDeliveryMethodModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDeliveryMethodModel.h"

@implementation NITDeliveryMethodModel

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _type = [NITOrderModel new].deliveryType;
        _pickupAvailable = true;
    }
    return self;
}

- (NSNumber *)price
{
    switch (self.type)
    {
        case DeliveryMethodTypeCourier: return [NITOrderModel new].deliveryPrice;
        case DeliveryMethodTypePickup:  return @(0);
    }
}

- (void)setType:(DeliveryMethodType)type
{
    _type = type;
    
    NITOrderModel * order = [NITOrderModel new];
    
    switch (type)
    {
        case DeliveryMethodTypeCourier:
        {
            order.deliveryType = 0;
            break;
        }
        case DeliveryMethodTypePickup:
        {
            order.deliveryType = 1;
            break;
        }
    }
    
    order.receiverAddress = @"";
}

@end
