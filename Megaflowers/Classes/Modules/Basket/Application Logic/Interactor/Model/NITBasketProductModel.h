//
//  NITBasketProductModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemViewModel.h"

@class NITBusketBouquet, NITBusketAttachment, SWGBasketBouquet, SWGBasketAttach;

static NSString const * kComponentIDkey     = @"identifier";
static NSString const * kComponentNamekey   = @"name";
static NSString const * kComponentCountkey  = @"count";
static NSString const * kComponentColorkey  = @"color";

@interface NITBasketProductModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * skuIdentifier;
@property (nonatomic) NSString * oldSkuIdentifier;
@property (nonatomic) NSNumber * baseSkuId;
@property (nonatomic) NSString * skuName;
@property (nonatomic) NSString * skuImagePath;
@property (nonatomic) NSNumber * skuWidth;
@property (nonatomic) NSNumber * skuHeight;
@property (nonatomic) NSNumber * skuPrice;
@property (nonatomic) NSNumber * skuCount;
@property (nonatomic) NSArray <NSDictionary *> * components; // {identifier : "number", name : "string", count : "number", color : "number"}
@property (nonatomic) NSString * skuPresentName;
@property (nonatomic) ProductContentType contentType;
@property (nonatomic) BOOL edited;
@property (nonatomic) BOOL inBasket;

- (instancetype)initWidthModel:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex;
- (instancetype)initWidthModel:(NITCatalogItemViewModel *)model;
+ (NSArray *)modelsFromBouquet:(NITBusketBouquet *)bouquet;
+ (NSArray *)modelsFromAttachment:(NITBusketAttachment *)attachment;
+ (NITBasketProductModel *)modelBySWGBouquet:(SWGBasketBouquet *)bouquet;
+ (NITBasketProductModel *)modelBySWGAttaches:(SWGBasketAttach *)attach;

@end
