//
//  NITBasketProductModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketProductModel.h"
#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"
#import "NITCatalogItemComponentViewModel.h"
#import "SWGBasketBouquet.h"
#import "SWGBasketAttach.h"
#import "NITBusketBouquetSku.h"

@implementation NITBasketProductModel

- (NSString *)skuPresentName
{
    if (self.skuWidth.integerValue > 0 && self.skuHeight.integerValue > 0)
    {
        return [NSString stringWithFormat:@"%@ (%@-%@ %@)", self.skuName, self.skuWidth, self.skuHeight, NSLocalizedString(@"см", nil)];
    }
    else if (self.skuWidth.integerValue > 0 && self.skuHeight.integerValue == 0)
    {
        return [NSString stringWithFormat:@"%@ (%@ %@)", self.skuName, self.skuWidth, NSLocalizedString(@"см", nil)];
    }
    else if (self.skuWidth.integerValue == 0 && self.skuHeight.integerValue > 0)
    {
        return [NSString stringWithFormat:@"%@ (%@ %@)", self.skuName, self.skuHeight, NSLocalizedString(@"см", nil)];
    }
    else
    {
        return self.skuName;
    }
}

- (BOOL)inBasket
{
    switch (self.contentType) {
        case ProductContentTypeBouqets:
            return [NITBusketBouquet bouquetByID:self.identifier.stringValue] != nil;
            
        case ProductContentTypeAttach:
            return [NITBusketAttachment attachmentByID:self.identifier.stringValue] != nil;
    }
}

- (instancetype)initWidthModel:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex
{
    if (self = [super init])
    {
        self.identifier = model.identifier ? : @0;
        self.name = model.title ? : @"";
        self.contentType = model.contentType;
        
        NITCatalogItemSKUViewModel * sku;
        if (model.skus.count > 0 && skuIndex < model.skus.count)
        {
            sku = model.skus[skuIndex];
        }
        else
        {
            sku = [NITCatalogItemSKUViewModel new];
        }
        
        self.oldSkuIdentifier = sku.db_id;
        self.baseSkuId = sku.identifier ? : @0;
        self.skuName = sku.title ? : @"";
        self.skuImagePath = sku.imagePath ? : @"";
        self.skuWidth = sku.width ? : @0;
        self.skuHeight = sku.height ? : @0;
        self.skuPrice = sku.discountPrice ? : @0;
        self.skuCount = @1;
        
        NSMutableArray * components = [NSMutableArray new];
        for (NITCatalogItemComponentViewModel * comp in sku.components)
        {
            NSMutableDictionary * info = [[NSMutableDictionary alloc] initWithDictionary:@{kComponentNamekey : comp.title ? : @"",
                                                                                           kComponentCountkey : comp.count ? : @0}];
            if ([comp.identifier isKindOfClass:[NSNumber class]])
            {
                [info setObject:comp.identifier.stringValue forKey:kComponentIDkey];
            }
            else
            {
                [info setObject:comp.identifier forKey:kComponentIDkey];
            }
            
            if ([comp.color isKindOfClass:[NSNumber class]])
            {
                [info setObject:comp.color.stringValue forKey:kComponentColorkey];
            }
            else
            {
                [info setObject:comp.color forKey:kComponentColorkey];
            }
            
            [components addObject:info];
        }
        
        self.components = components;
        
        if (model.contentType == ProductContentTypeBouqets && sku.edited)
        {
            self.skuIdentifier = [NITBusketBouquetSku editedSkuIDByComponents:components];
            self.edited = true;
        }
        else
        {
            self.skuIdentifier = self.baseSkuId.stringValue;
            self.edited = false;
        }
    }
    
    return self;
}

- (instancetype)initWidthModel:(NITCatalogItemViewModel *)model
{
    if (self = [super init])
    {
        self.identifier = model.identifier ? : @0;
        self.name = model.title ? : @"";
        self.contentType = model.contentType;
        
        NITCatalogItemSKUViewModel * sku = model.skus.firstObject;

        self.oldSkuIdentifier = sku.db_id;
        self.baseSkuId = sku.identifier ? : @0;
        self.skuName = sku.title ? : @"";
        self.skuImagePath = sku.imagePath ? : @"";
        self.skuWidth = sku.width ? : @0;
        self.skuHeight = sku.height ? : @0;
        self.skuPrice = sku.discountPrice ? : @0;
        self.skuCount = sku.count;
        
        NSMutableArray * components = [NSMutableArray new];
        for (NITCatalogItemComponentViewModel * comp in sku.components)
        {
            NSMutableDictionary * info = [[NSMutableDictionary alloc] initWithDictionary:@{kComponentNamekey : comp.title ? : @"",
                                                                                           kComponentCountkey : comp.count ? : @0}];
            if ([comp.identifier isKindOfClass:[NSNumber class]])
            {
                [info setObject:comp.identifier.stringValue forKey:kComponentIDkey];
            }
            else
            {
                [info setObject:comp.identifier forKey:kComponentIDkey];
            }
            
            if ([comp.color isKindOfClass:[NSNumber class]])
            {
                [info setObject:comp.color.stringValue forKey:kComponentColorkey];
            }
            else
            {
                [info setObject:comp.color forKey:kComponentColorkey];
            }
            
            [components addObject:info];
        }
        
        self.components = components;
        
        if (model.contentType == ProductContentTypeBouqets && sku.edited)
        {
            self.skuIdentifier = [NITBusketBouquetSku editedSkuIDByComponents:components];
            self.edited = true;
        }
        else
        {
            self.skuIdentifier = self.baseSkuId.stringValue;
            self.edited = false;
        }
    }
    
    return self;
}

+ (NSArray *)modelsFromBouquet:(NITBusketBouquet *)bouquet
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBusketBouquetSku * sku in bouquet.sku)
    {
        NITBasketProductModel * model = [NITBasketProductModel new];
        
        model.contentType = ProductContentTypeBouqets;
        model.identifier = @([bouquet.identifier integerValue]);
        model.name = bouquet.name;
        model.baseSkuId = @([sku.base_id integerValue]);
        model.skuIdentifier = sku.identifier;
        model.edited = ![model.baseSkuId.stringValue isEqualToString:model.skuIdentifier];
        model.skuName = sku.name;
        model.skuImagePath = sku.imagePath;
        model.skuWidth = @(sku.width);
        model.skuHeight = @(sku.height);
        model.skuPrice = @(sku.price);
        model.skuCount = @(sku.count);

        NSMutableArray * components = [NSMutableArray new];
        for (NITBusketBouquetComponent * comp in sku.components)
        {
            [components addObject:@{kComponentIDkey : comp.identifier,
                                    kComponentNamekey : comp.name,
                                    kComponentColorkey : comp.color,
                                    kComponentCountkey : @(comp.count)}];
        }
        
        model.components = components;

        [result addObject:model];
    }
    
    return result;
}

+ (NSArray *)modelsFromAttachment:(NITBusketAttachment *)attachment
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBusketAttachmentSku * sku in attachment.sku)
    {
        NITBasketProductModel * model = [NITBasketProductModel new];
        
        model.contentType = ProductContentTypeAttach;
        model.identifier = @([attachment.identifier integerValue]);
        model.name = attachment.name;
        model.skuIdentifier = sku.identifier;
        model.baseSkuId = @([sku.identifier integerValue]);
        model.skuName = sku.name;
        model.skuImagePath = sku.imagePath;
        model.skuWidth = @(sku.width);
        model.skuHeight = @(sku.height);
        model.skuPrice = @(sku.price);
        model.skuCount = @(sku.count);
        model.edited = false;
        
        NSMutableArray * components = [NSMutableArray new];
        for (NITBusketAttachmentComponent * comp in sku.components)
        {
            [components addObject:@{kComponentIDkey : comp.identifier,
                                    kComponentNamekey : comp.name,
                                    kComponentColorkey : comp.color,
                                    kComponentCountkey : @(comp.count)}];
        }
        
        model.components = components;
        
        [result addObject:model];
    }
    
    return result;
}

+ (NITBasketProductModel *)modelBySWGBouquet:(SWGBasketBouquet *)bouquet
{
    NITBasketProductModel * model = [NITBasketProductModel new];
    
    model.contentType = ProductContentTypeBouqets;
    model.identifier = bouquet.bouquetId;
    model.skuIdentifier = bouquet.bouquetSkuId.stringValue;
    model.baseSkuId = bouquet.bouquetSkuId;
    model.skuName = bouquet.skuName ? : @"";
    model.name = bouquet.name ? : @"";
    model.skuPrice = bouquet.price ? : @(0);
    model.skuCount = bouquet.count;
    model.edited = bouquet.isChanged.boolValue;
    
    NSDictionary * dict = (NSDictionary *)bouquet.image.detail;
    model.skuImagePath = dict[API.imageKey] ? : @"";
    
    NSMutableArray * components = [NSMutableArray new];
    for (SWGBasketBouquetComponent * component in bouquet.components)
    {
        
        [components addObject:@{kComponentIDkey : component._id.stringValue,
                                kComponentNamekey : component.name,
                                kComponentColorkey : component.color._id.stringValue,
                                kComponentCountkey : component.count}];
    }
    
    model.components = components;
    
    if (model.contentType == ProductContentTypeBouqets && model.edited)
    {
        model.skuIdentifier = [NITBusketBouquetSku editedSkuIDByComponents:components];
    }
    else
    {
        model.skuIdentifier = bouquet.bouquetSkuId.stringValue;
    }
    
    model.oldSkuIdentifier = model.skuIdentifier;
    
    return model;
}

+ (NITBasketProductModel *)modelBySWGAttaches:(SWGBasketAttach *)attach
{
    NITBasketProductModel * model = [NITBasketProductModel new];
    
    model.contentType = ProductContentTypeAttach;
    model.identifier = attach.attachId;
    model.skuIdentifier = attach.attachSkuId.stringValue;
    model.baseSkuId = attach.attachSkuId;
    model.skuName = attach.name ? : @"";
    model.skuPrice = attach.price ? : @(0);
    model.skuCount = attach.count;
    model.skuImagePath = [[attach.image detail] valueForKey:API.imageKey];;
    
    return model;
}

@end
