//
//  NITBasketSectionModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/11/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, BasketTableSectionType) {
    BasketTableSectionTypeBouqets   = 0,
    BasketTableSectionTypeAttach    = 1,
    BasketTableSectionTypeCoupons   = 2,
    BasketTableSectionTypeButtons   = 3,
    BasketTableSectionTypeDelivery  = 4
};

@interface NITBasketSectionModel : NSObject

@property (nonatomic) BasketTableSectionType type;
@property (nonatomic) NSArray * models;
@property (nonatomic) NSString * cellID;
@property (nonatomic) CGFloat rowHeight;

- (instancetype)initWithType:(BasketTableSectionType)type;
- (void)addModel:(id)model;
- (void)removeModel:(id)model;
+ (NITBasketSectionModel *)sectionByModel:(id)model fromSections:(NSArray *)sections;
+ (BOOL)productsIsNotEmptyFromSections:(NSArray *)sections;

@end
