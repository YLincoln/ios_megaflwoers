//
//  NITDeliveryMethodModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderModel.h"

typedef CF_ENUM (NSUInteger, DeliveryMethodType) {
    DeliveryMethodTypeCourier   = 0,
    DeliveryMethodTypePickup    = 1
};

@interface NITDeliveryMethodModel : NSObject

@property (nonatomic)  DeliveryMethodType type;
@property (nonatomic)  NSNumber * price;
@property (nonatomic)  BOOL pickupAvailable;

@end
