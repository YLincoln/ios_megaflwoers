//
//  NITBasketProductAddModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef CF_ENUM (NSUInteger, BasketProductAddType) {
    BasketProductAddTypeBouqet  = 0,
    BasketProductAddTypeAttach  = 1,
    BasketProductAddTypeCoupon  = 2
};

@interface NITBasketProductAddModel : NSObject

@property (nonatomic) BasketProductAddType type;
@property (nonatomic) NSString * title;

- (instancetype)initWithType:(BasketProductAddType)type;

@end
