//
//  NITBasketInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBasketProtocols.h"

@interface NITBasketInteractor : NSObject <NITBasketInteractorInputProtocol>

@property (nonatomic, weak) id <NITBasketInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBasketAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBasketLocalDataManagerInputProtocol> localDataManager;

@end
