//
//  NITBasketLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketLocalDataManager.h"


@implementation NITBasketLocalDataManager

#pragma mark - Coupon
- (NITCoupon *)getCoupon
{
    return [NITCoupon currentCoupon];
}

- (void)deleteCoupon
{
    [NITCoupon deleteCurrentCoupon];
}

- (void)deleteCoupons
{
    [NITCoupon deleteAllCoupons];
}

#pragma mark - Bouquets
- (NSArray <NITBusketBouquet *> *)allBouquets
{
    return [NITBusketBouquet allBouquets];
}

- (void)deleteBouquets
{
    [NITBusketBouquet deleteAllBouquets];
}

- (void)deleteBouquetByModel:(NITBasketProductModel *)model
{
    NITBusketBouquet * bouquet = [NITBusketBouquet bouquetByID:model.identifier.stringValue];
    
    if (bouquet)
    {
        [bouquet deleteBouquetSkuByID:model.skuIdentifier];
    }
}

- (void)updateCountForProduct:(NITBasketProductModel *)model
{
    switch (model.contentType) {
        case ProductContentTypeBouqets: {
            [NITBusketBouquetSku updateCount:[model.skuCount integerValue] bySkuID:model.skuIdentifier];
        }
            break;
            
        case ProductContentTypeAttach: {
            [NITBusketAttachmentSku updateCount:[model.skuCount integerValue] bySkuID:model.skuIdentifier];
        }
            break;
    }
}

#pragma mark - Attachments
- (NSArray <NITBusketAttachment *> *)allAttachments
{
    return [NITBusketAttachment allAttachments];
}

- (void)deleteAttachments
{
    [NITBusketAttachment deleteAllAttachments];
}

- (void)deleteAttachmentByModel:(NITBasketProductModel *)model
{
    NITBusketAttachment * attachment = [NITBusketAttachment attachmentByID:model.identifier.stringValue];
    
    if (attachment)
    {
        [attachment deleteAttachmentSkuByID:model.skuIdentifier];
    }
}

@end
