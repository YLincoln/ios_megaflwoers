//
//  NITBasketLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBasketProtocols.h"

@interface NITBasketLocalDataManager : NSObject <NITBasketLocalDataManagerInputProtocol>

@end
