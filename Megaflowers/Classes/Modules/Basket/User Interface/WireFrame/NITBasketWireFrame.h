//
//  NITBasketWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBasketProtocols.h"
#import "NITBasketViewController.h"
#import "NITBasketLocalDataManager.h"
#import "NITBasketAPIDataManager.h"
#import "NITBasketInteractor.h"
#import "NITBasketPresenter.h"
#import "NITBasketWireframe.h"
#import "NITRootWireframe.h"

@interface NITBasketWireFrame : NITRootWireframe <NITBasketWireFrameProtocol>

@end
