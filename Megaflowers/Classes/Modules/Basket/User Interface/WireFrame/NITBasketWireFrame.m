//
//  NITBasketWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketWireFrame.h"
#import "NITCouponsWireFrame.h"
#import "NITCatalogDirectoryWireFrame.h"
#import "NITProductDetailsWireFrame.h"
#import "NITOrderWireFrame.h"
#import "NITAuthorizationWireFrame.h"

#import "NITProductDetailsWireFrame.h"

@implementation NITBasketWireFrame

+ (id)createNITBasketModule
{
    // Generating module components
    id <NITBasketPresenterProtocol, NITBasketInteractorOutputProtocol> presenter = [NITBasketPresenter new];
    id <NITBasketInteractorInputProtocol> interactor = [NITBasketInteractor new];
    id <NITBasketAPIDataManagerInputProtocol> APIDataManager = [NITBasketAPIDataManager new];
    id <NITBasketLocalDataManagerInputProtocol> localDataManager = [NITBasketLocalDataManager new];
    id <NITBasketWireFrameProtocol> wireFrame= [NITBasketWireFrame new];
    id <NITBasketViewProtocol> view = [(NITBasketWireFrame *)wireFrame createViewControllerWithKey:_s(NITBasketViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITBasketModuleFrom:(UIViewController *)fromViewController
{
    NITBasketViewController * view = [NITBasketWireFrame createNITBasketModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showSelectBouqetFrom:(UIViewController *)fromView
{
    [NITCatalogDirectoryWireFrame selectNITCatalogDirectoryFrom:fromView withTitle:NSLocalizedString(@"Букеты", nil) parentID:nil];
}

- (void)showSelectAttachFrom:(UIViewController *)fromView
{
    [NITCatalogDirectoryWireFrame selectNITAttachModuleFrom:fromView withTitle:NSLocalizedString(@"Дополнения к букету", nil) parentID:nil];
}

- (void)showSelectCouponFrom:(UIViewController *)fromView
{
    [NITCouponsWireFrame presentNITCouponsModuleFrom:fromView];
}

- (void)showOrderModuleFrom:(UIViewController *)fromView withDeliveryType:(DeliveryMethodType)deliveryType
{
    [NITOrderWireFrame presentNITOrderModuleFrom:fromView withDeliveryType:deliveryType];
}

- (void)showRegisterFrom:(UIViewController *)fromViewController
{
    [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:fromViewController];
}

- (void)showProductDetailsFrom:(UIViewController *)fromViewController forModel:(NITCatalogItemViewModel *)model
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController bouquet:model selectType:ProductSelectTypeEdite];
}

- (void)setRootController:(UIViewController *)fromView
{
    [fromView.navigationController popToRootViewControllerAnimated:NO];
}

@end
