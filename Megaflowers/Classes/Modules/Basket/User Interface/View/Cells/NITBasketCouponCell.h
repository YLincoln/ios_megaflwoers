//
//  NITBasketCouponCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketTableViewCell.h"
#import "MGSwipeTableCell.h"

@class NITCouponModel;

@protocol NITBasketCouponCellDelegate <NSObject>

- (void)didSelectCoupon:(NITCouponModel *)coupon;
- (void)didNeedDeleteCoupon:(NITCouponModel *)coupon;

@end

@interface NITBasketCouponCell : MGSwipeTableCell

@property (nonatomic) NITCouponModel * model;
@property (nonatomic, weak) id <NITBasketCouponCellDelegate> cellDelegate;

@end
