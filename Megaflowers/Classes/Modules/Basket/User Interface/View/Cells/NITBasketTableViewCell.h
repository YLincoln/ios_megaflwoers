//
//  NITBasketTableViewCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/11/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "MGSwipeTableCell.h"

@interface NITBasketTableViewCell : MGSwipeTableCell

- (void)setModel:(id)model delegate:(id)delegate;

@end
