//
//  NITBasketTableViewCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 4/11/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketTableViewCell.h"

@implementation NITBasketTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
