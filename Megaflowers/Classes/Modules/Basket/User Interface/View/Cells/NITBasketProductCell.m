//
//  NITBasketProductCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketProductCell.h"
#import "NITBasketProductModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITBasketProductCell ()

@property (nonatomic) IBOutlet UIImageView * image;
@property (nonatomic) IBOutlet UILabel * name;
@property (nonatomic) IBOutlet UILabel * skuName;
@property (nonatomic) IBOutlet UILabel * price;
@property (nonatomic) IBOutlet UILabel * count;
@property (nonatomic) IBOutlet UIButton * minusBtn;
@property (nonatomic) IBOutlet UIButton * plusBtn;
@property (nonatomic) IBOutlet UIImageView * editeImg;

@end

@implementation NITBasketProductCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        
    }
    
    return self;
}

- (void)setModel:(NITBasketProductModel *)model delegate:(id)delegate
{
    self.model = model;
    self.cellDelegate = delegate;
}

- (void)setModel:(NITBasketProductModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)minusAction:(id)sender
{
    NSUInteger count = [self.model.skuCount integerValue];
    
    if (count > 1)
    {
        count--;
        self.model.skuCount = @(count);
        [self updateCounter];
        
        if ([self.cellDelegate respondsToSelector:@selector(didUpdateCountForProduct:)])
        {
            [self.cellDelegate didUpdateCountForProduct:self.model];
        }
    }
    else if (count == 1)
    {
        [self showSwipe:MGSwipeDirectionRightToLeft animated:true];
        /*if ([self.cellDelegate respondsToSelector:@selector(didNeedDeleteProduct:)])
        {
            [self.cellDelegate didNeedDeleteProduct:self.model];
        }*/
    }
}

- (IBAction)plusAction:(id)sender
{
    NSUInteger count = [self.model.skuCount integerValue];
    count++;
    self.model.skuCount = @(count);
    [self updateCounter];
    
    if ([self.cellDelegate respondsToSelector:@selector(didUpdateCountForProduct:)])
    {
        [self.cellDelegate didUpdateCountForProduct:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.name.text = self.model.name;
    self.skuName.text = self.model.skuPresentName;
    self.price.text = [NSString stringWithFormat:@"%@ ₽", self.model.skuPrice];
    
    self.image.image = nil;
    [self.image setImageWithURL:[NSURL URLWithString:self.model.skuImagePath]];
    
    [self updateCounter];
    
    weaken(self);
    MGSwipeButton * deleteBtn = [MGSwipeButton buttonWithTitle:NSLocalizedString(@"Удалить", nil)
                                               backgroundColor:RGB(236, 106, 106)
                                                      callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
                                                          
                                                          if ([weakSelf.cellDelegate respondsToSelector:@selector(didNeedDeleteProduct:)])
                                                          {
                                                              [weakSelf.cellDelegate didNeedDeleteProduct:weakSelf.model];
                                                          }
                                                          
                                                          return false;
                                                      }];
    
    self.rightButtons = @[deleteBtn];
    self.rightSwipeSettings.transition = MGSwipeTransitionBorder;
    self.rightExpansion.buttonIndex = 0;
    self.rightExpansion.fillOnTrigger = true;
    
    self.editeImg.hidden = !self.model.edited;
}

- (void)updateCounter
{
    self.count.text = [self.model.skuCount stringValue];
}


@end
