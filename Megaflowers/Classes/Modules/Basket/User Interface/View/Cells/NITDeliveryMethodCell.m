//
//  NITDeliveryMethodCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITDeliveryMethodCell.h"
#import "NITDeliveryMethodModel.h"

@interface NITDeliveryMethodCell ()

@property (nonatomic) IBOutlet UILabel * courierLbl;
@property (nonatomic) IBOutlet UILabel * pickupLbl;
@property (nonatomic) IBOutlet UIButton * courierBtn;
@property (nonatomic) IBOutlet UIButton * pickupBtn;

@end

@implementation NITDeliveryMethodCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITDeliveryMethodModel *)model delegate:(id)delegate
{
    self.model = model;
    self.delegate = delegate;
}


- (void)setModel:(NITDeliveryMethodModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)courierTapped:(id)sender
{
    self.model.type = DeliveryMethodTypeCourier;
    [self updateData];
    
    if ([self.delegate respondsToSelector:@selector(didChangeDeliveryMethod)])
    {
        [self.delegate didChangeDeliveryMethod];
    }
}

- (IBAction)pickupTapped:(id)sender
{
    self.model.type = DeliveryMethodTypePickup;
    [self updateData];
    
    if ([self.delegate respondsToSelector:@selector(didChangeDeliveryMethod)])
    {
        [self.delegate didChangeDeliveryMethod];
    }
}

#pragma mark - Private
- (void)updateData
{
    NSString * courierBtnImg;
    NSString * pickupBtnImg;
    
    switch (self.model.type)
    {
        case DeliveryMethodTypeCourier:
            courierBtnImg = @"check";
            pickupBtnImg = @"check_ar";
            break;
        case DeliveryMethodTypePickup:
            courierBtnImg = @"check_ar";
            pickupBtnImg = @"check";
            break;
    }
    
    NSNumber * price = [NITOrderModel new].deliveryPrice;
    if (price.integerValue > 0)
    {
        self.courierLbl.text = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"Курьер", nil), [NITOrderModel new].deliveryPrice, NSLocalizedString(@"руб.", nil)];
    }
    else
    {
        self.courierLbl.text = NSLocalizedString(@"Курьер", nil);
    }
    
    [self.courierBtn setImage:[UIImage imageNamed:courierBtnImg] forState:UIControlStateNormal];
    [self.pickupBtn setImage:[UIImage imageNamed:pickupBtnImg] forState:UIControlStateNormal];
    
    [self.pickupBtn setHidden:!self.model.pickupAvailable];
    [self.pickupLbl setHidden:!self.model.pickupAvailable];
}

@end
