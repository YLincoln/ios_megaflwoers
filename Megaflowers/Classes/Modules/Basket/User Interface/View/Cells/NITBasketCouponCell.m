//
//  NITBasketCouponCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketCouponCell.h"
#import "NITCouponModel.h"

@interface NITBasketCouponCell ()

@property (nonatomic) IBOutlet UILabel * date;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * detail;
@property (nonatomic) IBOutlet UIButton * basketBtn;

@end

@implementation NITBasketCouponCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {

    }
    
    return self;
}

- (void)setModel:(NITCouponModel *)model delegate:(id)delegate
{
    self.model = model;
    self.cellDelegate = delegate;
}

- (void)setModel:(NITCouponModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectCouponAction:(id)sender
{
    if ([self.cellDelegate respondsToSelector:@selector(didSelectCoupon:)])
    {
        [self.cellDelegate didSelectCoupon:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.date.text = self.model.dateEnd;
    self.title.text = self.model.title;
    self.detail.text = self.model.detail;
    
    weaken(self);
    MGSwipeButton * deleteBtn = [MGSwipeButton buttonWithTitle:NSLocalizedString(@"Удалить", nil)
                                               backgroundColor:RGB(236, 106, 106)
                                                      callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
                                                          
                                                          if ([weakSelf.cellDelegate respondsToSelector:@selector(didNeedDeleteCoupon:)])
                                                          {
                                                              [weakSelf.cellDelegate didNeedDeleteCoupon:weakSelf.model];
                                                          }
                                                          
                                                          return false;
                                                      }];
    
    self.rightButtons = @[deleteBtn];
    self.rightSwipeSettings.transition = MGSwipeTransitionBorder;
    self.rightExpansion.buttonIndex = 0;
    self.rightExpansion.fillOnTrigger = true;
}

@end
