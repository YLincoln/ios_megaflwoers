//
//  NITDeliveryMethodCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/10/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketTableViewCell.h"

@class NITDeliveryMethodModel;

@protocol NITDeliveryMethodCellDelegate <NSObject>

- (void)didChangeDeliveryMethod;

@end

@interface NITDeliveryMethodCell : UITableViewCell

@property (nonatomic) NITDeliveryMethodModel * model;
@property (nonatomic, weak) id <NITDeliveryMethodCellDelegate> delegate;

@end
