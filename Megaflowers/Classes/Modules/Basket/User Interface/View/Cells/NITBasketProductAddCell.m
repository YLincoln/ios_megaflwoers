//
//  NITBasketProductAddCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketProductAddCell.h"
#import "NITBasketProductAddModel.h"

@interface NITBasketProductAddCell ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * addBtn;

@end

@implementation NITBasketProductAddCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITBasketProductAddModel *)model delegate:(id)delegate
{
    self.model = model;
    self.delegate = delegate;
}

- (void)setModel:(NITBasketProductAddModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)addAction:(id)sender
{
    switch (self.model.type)
    {
        case BasketProductAddTypeBouqet:
            if ([self.delegate respondsToSelector:@selector(didAddBouqet)])
            {
                [self.delegate didAddBouqet];
            }
            break;
        case BasketProductAddTypeAttach:
            if ([self.delegate respondsToSelector:@selector(didAddAttach)])
            {
                [self.delegate didAddAttach];
            }
            break;
        case BasketProductAddTypeCoupon:
            if ([self.delegate respondsToSelector:@selector(didAddCoupon)])
            {
                [self.delegate didAddCoupon];
            }
            break;
    }
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
}

@end
