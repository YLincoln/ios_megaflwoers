//
//  NITBasketProductAddCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketTableViewCell.h"

@class NITBasketProductAddModel;

@protocol NITBasketProductAddCellDelegate <NSObject>

- (void)didAddBouqet;
- (void)didAddAttach;
- (void)didAddCoupon;

@end

@interface NITBasketProductAddCell : UITableViewCell

@property (nonatomic) NITBasketProductAddModel * model;
@property (nonatomic, weak) id <NITBasketProductAddCellDelegate> delegate;

@end
