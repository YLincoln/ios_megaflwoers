//
//  NITBasketProductCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/9/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketTableViewCell.h"
#import "MGSwipeTableCell.h"

@class NITBasketProductModel;

@protocol NITBasketProductCellDelegate <NSObject>

- (void)didUpdateCountForProduct:(NITBasketProductModel *)product;
- (void)didNeedDeleteProduct:(NITBasketProductModel *)product;

@end

@interface NITBasketProductCell : MGSwipeTableCell

@property (nonatomic) NITBasketProductModel * model;
@property (nonatomic, weak) id <NITBasketProductCellDelegate> cellDelegate;

@end
