//
//  NITBasketViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketViewController.h"
#import "NITCouponsWireFrame.h"
#import "NITBasketProductCell.h"
#import "NITBasketCouponCell.h"
#import "NITBasketProductAddCell.h"
#import "NITDeliveryMethodCell.h"
#import "NITOrderModel.h"
#import "NITBasketTableViewCell.h"

@interface NITBasketViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITBasketProductCellDelegate,
NITBasketCouponCellDelegate,
NITBasketProductAddCellDelegate,
NITDeliveryMethodCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIButton * orderBtn;
@property (nonatomic) IBOutlet UILabel * totalSum;
@property (nonatomic) IBOutlet UIView * orderSection;
@property (nonatomic) IBOutlet UIBarButtonItem * cleanBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * orderSectionBottom;

@property (nonatomic) NSArray <NITBasketSectionModel *> * sections;

@end

@implementation NITBasketViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
    [self.presenter showControllerFrom3DTouch];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:false];
    [self.orderBtn setEnabled:true];
}

#pragma mark - IBAction
- (IBAction)cleanAction:(id)sender
{
    [self.presenter cleanBasket];
}

- (IBAction)orderAction:(id)sender
{
    [self.orderBtn setEnabled:false];
    [self.presenter orderAction];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Корзина", nil);
    
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITBasketProductCell) bundle:nil] forCellReuseIdentifier:_s(NITBasketProductCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITBasketCouponCell) bundle:nil] forCellReuseIdentifier:_s(NITBasketCouponCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITBasketProductAddCell) bundle:nil] forCellReuseIdentifier:_s(NITBasketProductAddCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITDeliveryMethodCell) bundle:nil] forCellReuseIdentifier:_s(NITDeliveryMethodCell)];
    [self.tableView setTableFooterView:[UIView new]];
    
    self.orderBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.orderBtn.titleLabel.numberOfLines = 2;
    self.orderBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.orderSection.hidden = true;
}

- (void)reloadTableSection:(NSInteger)section
{
    [self.tableView beginUpdates];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

#pragma mark - NITBasketPresenterProtocol
- (void)updateTotalPrice:(NSNumber *)price
{
    self.totalSum.text = [NSString stringWithFormat:@"%@ ₽", price];
    
    NSString * priceTitle = [NSString stringWithFormat:@"%@\n%@ ₽", NSLocalizedString(@"Оформить заказ", nil), price];
    
    NSMutableAttributedString * attrTitle = [[NSMutableAttributedString alloc] initWithString:priceTitle];
    
    NSRange range = [priceTitle rangeOfString:NSLocalizedString(@"Оформить заказ", nil)];
    [attrTitle addAttribute:NSFontAttributeName
                      value:[UIFont systemFontOfSize:20 weight:UIFontWeightRegular]
                      range:NSMakeRange(range.location, range.length)];
    
    [attrTitle addAttribute:NSFontAttributeName
                      value:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]
                      range:NSMakeRange(range.length, attrTitle.length - range.length)];
    
    [attrTitle addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:NSMakeRange(0, attrTitle.length)];
    
    [self.orderBtn setAttributedTitle:attrTitle forState:UIControlStateNormal];
    
    self.orderSection.hidden = false;
}

- (void)updateOrderButtonState:(OrderButtonState)state
{
    switch (state) {
        case OrderButtonStateAvailable:
            [self.orderBtn setEnabled:true];
            [self.orderBtn setHidden:false];
            [self.orderBtn setBackgroundColor:RGB(10, 88, 43)];
            break;
            
        case OrderButtonStateNotAvailable:
            [self.orderBtn setEnabled:false];
            [self.orderBtn setHidden:false];
            [self.orderBtn setBackgroundColor:RGB(10, 88, 43)];
            break;
            
        case OrderButtonStateInactive:
            [self.orderBtn setEnabled:true];
            [self.orderBtn setHidden:false];
            [self.orderBtn setBackgroundColor:[UIColor lightGrayColor]];
            break;
        case OrderButtonStateDisable:
            [self.orderBtn setEnabled:false];
            [self.orderBtn setHidden:true];
            break;
    }
}

- (void)reloadDataByModels:(NSArray<NITBasketSectionModel *> *)models availabilityOrderSection:(BOOL)available
{
    CGPoint contentOffset = [self.tableView contentOffset];
    
    self.sections = models;
    [self.tableView reloadData];
    
    self.cleanBtn.enabled = available;
    self.orderSection.hidden = !available;
    
    self.orderSectionBottom.constant = available ? 0 : - ViewHeight(self.orderSection);
    [self.view layoutIfNeeded];
    
    NITBasketSectionModel * sectionModel = models[BasketTableSectionTypeDelivery];
    sectionModel.rowHeight = [self.presenter deliveryHeightWithState:available forTable:self.tableView];
    [self reloadTableSection:BasketTableSectionTypeDelivery];
    
    if ([self.tableView contentSize].height > (ViewHeight(self.tableView) - ViewHeight(self.orderBtn)))
    {
        [self.tableView setContentOffset:contentOffset animated:false];
    }
}

- (void)reloadTableAvailability:(BOOL)available
{
    [self.tableView setUserInteractionEnabled:available];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NITBasketSectionModel * sectionModel = self.sections[section];
    return [sectionModel.models count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITBasketSectionModel * sectionModel = self.sections[indexPath.section];
    return [sectionModel rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITBasketSectionModel * sectionModel = self.sections[indexPath.section];
    NITBasketTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:sectionModel.cellID forIndexPath:indexPath];
    [cell setModel:sectionModel.models[indexPath.row] delegate:self];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == BasketTableSectionTypeDelivery && [cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, ViewWidth(self.view))];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITBasketSectionModel * sectionModel = self.sections[indexPath.section];
    if (sectionModel.type == BasketTableSectionTypeBouqets || sectionModel.type == BasketTableSectionTypeAttach)
    {
        [self.presenter editeProductModel:sectionModel.models[indexPath.row] byType:sectionModel.type];
    }
}

#pragma mark - NITBasketProductCellDelegate
- (void)didNeedDeleteProduct:(NITBasketProductModel *)product
{
    [self.presenter removeProduct:product]; 
}

- (void)didUpdateCountForProduct:(NITBasketProductModel *)product
{
    [self.presenter updateCountForProduct:product];
}

#pragma mark - NITBasketCouponCellDelegate
- (void)didSelectCoupon:(NITCouponModel *)coupon
{
    [self.presenter selectNewCoupon];
}

- (void)didNeedDeleteCoupon:(NITCouponModel *)coupon
{
    [self.presenter removeProduct:coupon];
}

#pragma mark - NITBasketProductAddCellDelegate
- (void)didAddBouqet
{
    [self.presenter selectNewBouqet];
}

- (void)didAddAttach
{
    [self.presenter selectNewAttach];
}

- (void)didAddCoupon
{
    [self.presenter selectNewCoupon];
}

#pragma mark - NITDeliveryMethodCellDelegate
- (void)didChangeDeliveryMethod
{
    [self.presenter updateData];
}

@end
