//
//  NITBasketViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITBasketProtocols.h"

@interface NITBasketViewController : NITBaseViewController <NITBasketViewProtocol>

@property (nonatomic, strong) id <NITBasketPresenterProtocol> presenter;

@end
