//
//  NITBasketPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBasketProtocols.h"

@class NITBasketWireFrame;

@interface NITBasketPresenter : NITRootPresenter <NITBasketPresenterProtocol, NITBasketInteractorOutputProtocol>

@property (nonatomic, weak) id <NITBasketViewProtocol> view;
@property (nonatomic, strong) id <NITBasketInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBasketWireFrameProtocol> wireFrame;
@property (nonatomic) NSArray <NITBasketSectionModel *> * sections;
@property (nonatomic) OrderButtonState orderButtonState;

@end
