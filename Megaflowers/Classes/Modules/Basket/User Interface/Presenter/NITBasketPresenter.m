//
//  NITBasketPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketPresenter.h"
#import "NITBasketWireframe.h"
#import "NITCatalogItemSKUViewModel.h"
#import "NITCatalogItemComponentViewModel.h"
#import "NSArray+BlocksKit.h"
#import "NITSalonsWireFrame.h"
#import "SWGBasket.h"

@implementation NITBasketPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - NITBasketPresenterProtocol
- (void)initData
{
    self.orderButtonState = OrderButtonStateNotAvailable;
    [self.view updateOrderButtonState:self.orderButtonState];
    [self updateData];
}

- (void)cleanBasket
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Удалить", nil) actionBlock:^{
        weakSelf.orderButtonState = OrderButtonStateDisable;
        [weakSelf.interactor cleanBasket];
        [weakSelf updateData];
    }];

    [HELPER showActionSheetFromView:self.view
                          withTitle:NSLocalizedString(@"Вы действительно хотите удалить содержимое корзины?", nil)
                      actionButtons:@[action]
                       cancelButton:cancel];
}

- (void)orderAction
{
    switch (self.orderButtonState) {
            
        case OrderButtonStateInactive: {
            
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"OK", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:self.view
                                  withTitle:NSLocalizedString(@"Для оформления заказа\nподключитесь к интернету.\nЦена букетов в режиме офлайн может отличаться\nот текущей стоимости.", nil)
                              actionButtons:nil
                               cancelButton:cancel];
            
        } break;
            
        default: {
    
            NSArray * bouqets = [self.sections[BasketTableSectionTypeBouqets] models];
            if ([bouqets count] == 0)
            {
                NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ok", nil) actionBlock:^{}];
                [HELPER showActionSheetFromView:self.view
                                      withTitle:NSLocalizedString(@"Для завершения покупки добавьте в корзину букет!", nil)
                                  actionButtons:nil
                                   cancelButton:cancel];
            }
            else
            {
                weaken(self);
                [self.interactor createPreOrderIfNeedWithHandler:^{
                    NITDeliveryMethodModel * model = [weakSelf.sections[BasketTableSectionTypeDelivery] models].firstObject;
                    [weakSelf.wireFrame showOrderModuleFrom:weakSelf.view withDeliveryType:model.type];
                }];
            }
            
        } break;
    }
}

- (void)updateCountForProduct:(NITBasketProductModel *)product
{
    [self.interactor updateCountForProduct:product];
}

- (void)removeProduct:(id)product
{
    NITBasketSectionModel * section = [NITBasketSectionModel sectionByModel:product fromSections:self.sections];
    NSString * title;
    
    switch (section.type)
    {
        case BasketTableSectionTypeBouqets:
            title = NSLocalizedString(@"Вы действительно хотите удалить букет?", nil);
            break;
        case BasketTableSectionTypeAttach:
            title = NSLocalizedString(@"Вы действительно хотите удалить доп. к букету?", nil);
            break;
        case BasketTableSectionTypeCoupons:
            title = NSLocalizedString(@"Вы действительно хотите удалить купон?", nil);
            break;
        default:
            break;
    }
    
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{
        BOOL available = [NITBasketSectionModel productsIsNotEmptyFromSections:weakSelf.sections];
        [weakSelf.view reloadDataByModels:weakSelf.sections availabilityOrderSection:available];
    }];
    
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Удалить", nil) actionBlock:^{
        
        [section removeModel:product];
        
        BOOL available = [NITBasketSectionModel productsIsNotEmptyFromSections:weakSelf.sections];
        
        [weakSelf.view updateOrderButtonState:available ? weakSelf.orderButtonState : OrderButtonStateDisable];
        [weakSelf.view reloadDataByModels:weakSelf.sections availabilityOrderSection:available];
        
        switch (section.type)
        {
            case BasketTableSectionTypeBouqets:
                [weakSelf.interactor deleteBouquet:product];
                break;
            case BasketTableSectionTypeAttach:
                [weakSelf.interactor deleteAttachment:product];
                break;
            case BasketTableSectionTypeCoupons: {
                [weakSelf.interactor deleteCoupon:product];
            }
                break;
            default:
                break;
        }
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:title actionButtons:@[action1] cancelButton:cancel];
}

- (CGFloat)deliveryHeightWithState:(BOOL)enabled forTable:(UITableView *)table
{
    CGFloat h = 0;
    
    if (enabled)
    {
        for (int i = 0; i < BasketTableSectionTypeDelivery; i++)
        {
            h += [table rectForSection:i].size.height;
        }
        
        if (ViewHeight(table) > h) h = MAX(57, ViewHeight(table) - h);
        else                       h = 57;
        
        table.scrollEnabled = !(h > 57);
    }

    return h;
}

#pragma mark - Navigation
- (void)selectNewCoupon
{
    if ([USER isAutorize])
    {
        [self.wireFrame showSelectCouponFrom:self.view];
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        
        weaken(self);
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Регистрация", nil) actionBlock:^{
            [weakSelf.wireFrame showRegisterFrom:weakSelf.view];
        }];
        
        [HELPER showActionSheetFromView:self.view
                              withTitle:NSLocalizedString(@"Данная функция приложения не доступна без регистрации", nil)
                          actionButtons:@[action1]
                           cancelButton:cancel];
    }
}

- (void)selectNewBouqet
{
    [self.wireFrame showSelectBouqetFrom:self.view];
}

- (void)selectNewAttach
{
    [self.wireFrame showSelectAttachFrom:self.view];
}

- (void)editeProductModel:(NITBasketProductModel *)model byType:(BasketTableSectionType)type
{
    weaken(self);
    switch (type) {
        case BasketTableSectionTypeBouqets: {
            [self.interactor getBouquetModelByID:model.identifier andHandler:^(NITCatalogItemViewModel *originalModel) {
                if (originalModel)
                {
                    NITCatalogItemViewModel * updatedModel = [weakSelf updateOriginalModel:originalModel byBasketModel:model];
                    if (updatedModel) [weakSelf.wireFrame showProductDetailsFrom:weakSelf.view forModel:updatedModel];
                }
            }];
        } break;
            
        case BasketTableSectionTypeAttach: {
            [self.interactor getAttachmentModelByID:model.identifier andHandler:^(NITCatalogItemViewModel *originalModel) {
                if (originalModel) [weakSelf.wireFrame showProductDetailsFrom:weakSelf.view forModel:originalModel];
            }];
        } break;
            
        default:
            break;
    }
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kChangeCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setRootControler) name:kSetRootBasketNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(basketStartValidation) name:kStartBasketValidation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(basketStopValidation:) name:kStopBasketValidation object:nil];
}

- (void)setRootControler
{
    [self.wireFrame setRootController:self.view];
}

- (void)basketStartValidation
{
    self.orderButtonState = OrderButtonStateNotAvailable;
    [self.view updateOrderButtonState:self.orderButtonState];
    [self.view reloadTableAvailability:false];
    [self reloadDataWithHandler:^{}];
}

- (void)basketStopValidation:(NSNotification *)notification
{
    NSDictionary * info = notification.object;
    NSNumber * price = info[@"price"];
    self.orderButtonState = [info[@"state"] integerValue];
    
    [self reloadDataWithHandler:^{
        
        if (price)
        {
            [self.view updateTotalPrice:price];
        }
        else
        {
            [self updatePriceLocal];
        }
    }];
}

- (void)reloadDataWithHandler:(void(^)())handler
{
    NITBasketSectionModel * bouqetsSection  = [[NITBasketSectionModel alloc] initWithType:BasketTableSectionTypeBouqets];
    NITBasketSectionModel * attachSection   = [[NITBasketSectionModel alloc] initWithType:BasketTableSectionTypeAttach];
    NITBasketSectionModel * couponsSection  = [[NITBasketSectionModel alloc] initWithType:BasketTableSectionTypeCoupons];
    NITBasketSectionModel * buttonsSection  = [[NITBasketSectionModel alloc] initWithType:BasketTableSectionTypeButtons];
    NITBasketSectionModel * deliverySection = [[NITBasketSectionModel alloc] initWithType:BasketTableSectionTypeDelivery];
    
    bouqetsSection.models = [self.interactor getBouqetsDataModels];
    attachSection.models  = [self.interactor getAttachDataModels];
    couponsSection.models = [self.interactor getCouponsDataModels];
    buttonsSection.models = [self buttonsDataWithCouponState:couponsSection.models.count > 0];
    
    [self updateDeliveryDataWithHandler:^(NSArray<NITDeliveryMethodModel *> *models) {
        
        deliverySection.models = models;
        
        self.sections = @[bouqetsSection, attachSection, couponsSection, buttonsSection, deliverySection];
        BOOL available = [NITBasketSectionModel productsIsNotEmptyFromSections:self.sections];
        
        [self.view updateOrderButtonState:self.orderButtonState];
        [self.view reloadDataByModels:self.sections availabilityOrderSection:available];
        [self.view reloadTableAvailability:true];
        
        if (handler) handler();
    }];
}

- (void)updatePriceLocal
{
    CGFloat totalPrice = 0;
    
    NSArray * bouqets = [self.sections[BasketTableSectionTypeBouqets] models];
    NSArray * attachments = [self.sections[BasketTableSectionTypeAttach] models];
    
    if ([bouqets count] > 0 || [attachments count] > 0)
    {
        for (NITBasketProductModel * model in bouqets)
        {
            totalPrice += [model.skuPrice floatValue] * [model.skuCount floatValue];
        }
        
        for (NITBasketProductModel * model in attachments)
        {
            totalPrice += [model.skuPrice floatValue] * [model.skuCount floatValue];
        }
        
        NSArray * deliveryData = [self.sections[BasketTableSectionTypeDelivery] models];
        for (NITDeliveryMethodModel * model in deliveryData)
        {
            totalPrice += [model.price floatValue];
        }
        
        NSArray * coupons = [self.sections[BasketTableSectionTypeCoupons] models];
        for (NITCouponModel * model in coupons)
        {
            if ([model.value floatValue])
            {
                switch ([model.type integerValue]) {
                    case CouponTypePercent: totalPrice -= totalPrice / 100 * [model.value floatValue];  break;
                    case CouponTypeSum:     totalPrice -= [model.value floatValue];                     break;
                }
            }
            
            break;
        }
        
        if (USER.discount)
        {
            totalPrice -= totalPrice / 100 * [USER.discount.value floatValue];
        }
    }
    
    [self.view updateTotalPrice:@(totalPrice)];
}

- (void)updateData
{
    [API validateBasket];
}

- (NSMutableArray <NITBasketProductAddModel *> *)buttonsDataWithCouponState:(BOOL)couponSelected
{
    NSMutableArray <NITBasketProductAddModel *> * models = [NSMutableArray new];
    
    [models addObject:[[NITBasketProductAddModel alloc] initWithType:BasketProductAddTypeBouqet]];
    [models addObject:[[NITBasketProductAddModel alloc] initWithType:BasketProductAddTypeAttach]];
    
    if (!couponSelected)
    {
        [models addObject:[[NITBasketProductAddModel alloc] initWithType:BasketProductAddTypeCoupon]];
    }
    
    return models;
}

- (void)updateDeliveryDataWithHandler:(void(^)(NSArray <NITDeliveryMethodModel *> *models))handler
{
    [self.interactor checkSalonsWithHandler:^(BOOL pickupAvailable) {
        
        NITDeliveryMethodModel * model = [NITDeliveryMethodModel new];
        if (!pickupAvailable) model.type = DeliveryMethodTypeCourier;
        model.pickupAvailable = pickupAvailable;
        
        handler(@[model]);
    }];
}

- (NITCatalogItemViewModel *)updateOriginalModel:(NITCatalogItemViewModel *)originalModel byBasketModel:(NITBasketProductModel *)basketModel
{
    NITCatalogItemSKUViewModel * originalSku = [originalModel.skus bk_select:^BOOL(NITCatalogItemSKUViewModel *obj) {
        return [obj.identifier isEqualToNumber:basketModel.baseSkuId];
    }].firstObject;
    
    if (originalSku)
    {
        originalSku.identifier = basketModel.baseSkuId;
        originalSku.db_id = basketModel.skuIdentifier;
        originalSku.price = basketModel.skuPrice;
        
        NSMutableArray * componenmts = [NSMutableArray new];
        
        for (NSDictionary * dict in basketModel.components)
        {
            NITCatalogItemComponentViewModel * comp = [NITCatalogItemComponentViewModel new];
            comp.identifier = dict[@"identifier"];
            comp.title = dict[@"name"];
            comp.count = dict[@"count"];
            comp.color = dict[@"color"];
            
            [componenmts addObject:comp];
        }
        
        originalSku.components = componenmts;
        
        NSInteger index = [originalModel.skus indexOfObject:originalSku];
        NSString * imagePath = originalModel.imagePaths[index];
        originalModel.imagePaths = @[imagePath];
        originalModel.skus = @[originalSku];
        
        return originalModel;
    }
    
    return nil;
}

#pragma mark - 3D Touch
- (void)showControllerFrom3DTouch
{
    if ([Shortcut isCreatedShortcut])
    {
        if([Shortcut isAvailableShortcutByTag:@"Basket"])
        {
            [Shortcut deleteShortcut];
        }
    }
}

@end
