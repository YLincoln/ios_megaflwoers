//
//  NITPaymentFinalProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITPaymentFinalInteractorOutputProtocol;
@protocol NITPaymentFinalInteractorInputProtocol;
@protocol NITPaymentFinalViewProtocol;
@protocol NITPaymentFinalPresenterProtocol;
@protocol NITPaymentFinalLocalDataManagerInputProtocol;
@protocol NITPaymentFinalAPIDataManagerInputProtocol;

@class NITPaymentFinalWireFrame;

@protocol NITPaymentFinalViewProtocol
@required
@property (nonatomic, strong) id <NITPaymentFinalPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITPaymentFinalWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITPaymentFinalModuleFrom:(id)fromView withPaymentState:(BOOL)succes;
- (void)doneFrom:(id)fromView;
- (void)openMyOrders:(id)fromView;
- (void)chooseDifferentPaymentMethod:(id)fromView;
@end

@protocol NITPaymentFinalPresenterProtocol
@required
@property (nonatomic, weak) id <NITPaymentFinalViewProtocol> view;
@property (nonatomic, strong) id <NITPaymentFinalInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPaymentFinalWireFrameProtocol> wireFrame;
@property (nonatomic) BOOL paymentSucces;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)done;
- (void)openMyOrders;
- (void)chooseDifferentPaymentMethod;
@end

@protocol NITPaymentFinalInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITPaymentFinalInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPaymentFinalInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPaymentFinalAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPaymentFinalLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)cleanBusket;
@end


@protocol NITPaymentFinalDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITPaymentFinalAPIDataManagerInputProtocol <NITPaymentFinalDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITPaymentFinalLocalDataManagerInputProtocol <NITPaymentFinalDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)deleteCoupon;
- (void)deleteBouquets;
- (void)deleteAttachments;
@end
