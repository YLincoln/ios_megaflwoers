//
//  NITPaymentFinalInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPaymentFinalProtocols.h"

@interface NITPaymentFinalInteractor : NSObject <NITPaymentFinalInteractorInputProtocol>

@property (nonatomic, weak) id <NITPaymentFinalInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPaymentFinalAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPaymentFinalLocalDataManagerInputProtocol> localDataManager;

@end
