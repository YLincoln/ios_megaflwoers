//
//  NITPaymentFinalInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentFinalInteractor.h"

@implementation NITPaymentFinalInteractor

- (void)cleanBusket
{
    [self.localDataManager deleteCoupon];
    [self.localDataManager deleteBouquets];
    [self.localDataManager deleteAttachments];
}

@end
