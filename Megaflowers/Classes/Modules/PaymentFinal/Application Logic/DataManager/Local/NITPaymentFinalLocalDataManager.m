//
//  NITPaymentFinalLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentFinalLocalDataManager.h"
#import "NITCoupon.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"

@implementation NITPaymentFinalLocalDataManager

- (void)deleteCoupon
{
    [NITCoupon deleteAllCoupons];
}

- (void)deleteBouquets
{
    [NITBusketBouquet deleteAllBouquets];
}

- (void)deleteAttachments
{
    [NITBusketAttachment deleteAllAttachments];
}

@end
