//
//  NITPaymentFinalViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITPaymentFinalProtocols.h"

@interface NITPaymentFinalViewController : NITBaseViewController <NITPaymentFinalViewProtocol>

@property (nonatomic, strong) id <NITPaymentFinalPresenterProtocol> presenter;

@end
