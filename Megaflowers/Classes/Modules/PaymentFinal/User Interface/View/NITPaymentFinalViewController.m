//
//  NITPaymentFinalViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentFinalViewController.h"

@interface NITPaymentFinalViewController ()

@property (nonatomic) IBOutlet UIView * successSection;
@property (nonatomic) IBOutlet UIView * failSection;

@end

@implementation NITPaymentFinalViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)doneAction:(id)sender
{
    [self.presenter done];
}

- (IBAction)openMyOrdersAction:(id)sender
{
    [self.presenter openMyOrders];
}

- (IBAction)chooseDifferentPaymentMethodAction:(id)sender
{
    [self.presenter chooseDifferentPaymentMethod];
}

#pragma mark - Private
- (void)initUI
{
    self.successSection.hidden = !self.presenter.paymentSucces;
    self.failSection.hidden = self.presenter.paymentSucces;
}

@end
