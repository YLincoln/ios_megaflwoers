//
//  NITPaymentFinalWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentFinalWireFrame.h"
#import "NITOrdersListWireFrame.h"
#import "NITAuthorizationWireFrame.h"

@interface NITPaymentFinalWireFrame ()

@property (nonatomic) UINavigationController * navController;

@end

@implementation NITPaymentFinalWireFrame

+ (id)createNITPaymentFinalModule
{
    // Generating module components
    id <NITPaymentFinalPresenterProtocol, NITPaymentFinalInteractorOutputProtocol> presenter = [NITPaymentFinalPresenter new];
    id <NITPaymentFinalInteractorInputProtocol> interactor = [NITPaymentFinalInteractor new];
    id <NITPaymentFinalAPIDataManagerInputProtocol> APIDataManager = [NITPaymentFinalAPIDataManager new];
    id <NITPaymentFinalLocalDataManagerInputProtocol> localDataManager = [NITPaymentFinalLocalDataManager new];
    id <NITPaymentFinalWireFrameProtocol> wireFrame= [NITPaymentFinalWireFrame new];
    id <NITPaymentFinalViewProtocol> view = [(NITPaymentFinalWireFrame *)wireFrame createViewControllerWithKey:_s(NITPaymentFinalViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITPaymentFinalModuleFrom:(UIViewController *)fromViewController withPaymentState:(BOOL)succes
{
    NITPaymentFinalViewController * vc = [NITPaymentFinalWireFrame createNITPaymentFinalModule];
    vc.presenter.paymentSucces = succes;
    
    NITPaymentFinalWireFrame * wireFrame = (NITPaymentFinalWireFrame *)vc.presenter.wireFrame;
    wireFrame.navController = fromViewController.navigationController;

    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

- (void)doneFrom:(UIViewController *)fromViewController
{
    [self.navController popToRootViewControllerAnimated:false];
    [fromViewController dismissViewControllerAnimated:true completion:^{}];
}

- (void)openMyOrders:(UIViewController *)fromViewController
{
    [self.navController popToRootViewControllerAnimated:true];
    
//    if (USER.isAutorize)
//    {
        [NITOrdersListWireFrame presentNITOrdersListModuleFrom:self.navController.topViewController];
//    }
//    else
//    {
//        [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:self.navController.topViewController];
//    }
    
    [fromViewController dismissViewControllerAnimated:true completion:^{}];
}

- (void)chooseDifferentPaymentMethod:(UIViewController *)fromViewController
{
    [fromViewController dismissViewControllerAnimated:true completion:^{}];
}

@end
