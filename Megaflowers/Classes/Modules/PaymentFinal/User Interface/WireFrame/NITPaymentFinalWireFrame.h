//
//  NITPaymentFinalWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentFinalProtocols.h"
#import "NITPaymentFinalViewController.h"
#import "NITPaymentFinalLocalDataManager.h"
#import "NITPaymentFinalAPIDataManager.h"
#import "NITPaymentFinalInteractor.h"
#import "NITPaymentFinalPresenter.h"
#import "NITPaymentFinalWireframe.h"
#import "NITRootWireframe.h"

@interface NITPaymentFinalWireFrame : NITRootWireframe <NITPaymentFinalWireFrameProtocol>

@end
