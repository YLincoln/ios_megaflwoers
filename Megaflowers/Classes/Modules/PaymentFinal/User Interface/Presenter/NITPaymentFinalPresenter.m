//
//  NITPaymentFinalPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentFinalPresenter.h"
#import "NITPaymentFinalWireframe.h"

@implementation NITPaymentFinalPresenter

- (void)initData
{
    if (self.paymentSucces)
    {
        [self.interactor cleanBusket];
        [self rateAppIfNeed];
    }
}

- (void)done
{
    [self.wireFrame doneFrom:self.view];
}

- (void)openMyOrders
{
    [self.wireFrame openMyOrders:self.view];
}

- (void)chooseDifferentPaymentMethod
{
    [self.wireFrame chooseDifferentPaymentMethod:self.view];
}

#pragma mark - Private
- (void)rateAppIfNeed
{
    NSUInteger interval = - [USER.showRateAppDate timeIntervalSinceNow];
    if (USER.appRate == 0 && interval > 60*60*24*2)
    {
        [HELPER showRateAppActionSheetWithTitle:NSLocalizedString(@"Пожалуйста, оцените\nприложение Megaflowers!", nil)
                                   successBlock:^(CGFloat rateValue) {
                                       USER.appRate = rateValue;
                                       
                                       NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
                                       
                                       NITActionButton * action = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Перейти в Appstore", nil) actionBlock:^{
                                           [[UIApplication sharedApplication] openURL:APP_RATE_URL];
                                       }];
                                       
                                       [HELPER showActionSheetFromView:WINDOW.visibleController
                                                             withTitle:NSLocalizedString(@"Спасибо за вашу оценку!\nБудем также благодарны,\nесли вы оставите отзыв\nв Appstore", nil)
                                                         actionButtons:@[action]
                                                          cancelButton:cancel];
                                   }
                                    cancelBlock:^{
                                        USER.showRateAppDate = [NSDate date];
                                    }];
    }
}

@end
