//
//  NITPaymentFinalPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPaymentFinalProtocols.h"

@class NITPaymentFinalWireFrame;

@interface NITPaymentFinalPresenter : NITRootPresenter <NITPaymentFinalPresenterProtocol, NITPaymentFinalInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPaymentFinalViewProtocol> view;
@property (nonatomic, strong) id <NITPaymentFinalInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPaymentFinalWireFrameProtocol> wireFrame;
@property (nonatomic) BOOL paymentSucces;

@end
