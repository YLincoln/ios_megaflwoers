//
//  NITAuthorizationWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAuthorizationProtocols.h"
#import "NITAuthorizationViewController.h"
#import "NITAuthorizationLocalDataManager.h"
#import "NITAuthorizationAPIDataManager.h"
#import "NITAuthorizationInteractor.h"
#import "NITAuthorizationPresenter.h"
#import "NITAuthorizationWireframe.h"
#import "NITRootWireframe.h"

@interface NITAuthorizationWireFrame : NITRootWireframe <NITAuthorizationWireFrameProtocol>

@end
