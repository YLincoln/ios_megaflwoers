//
//  NITAuthorizationWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAuthorizationWireFrame.h"
#import "NITRegisterWireFrame.h"
#import "NITLoginWireFrame.h"

@implementation NITAuthorizationWireFrame

#pragma mark - NITAuthorizationWireFrameProtocol
+ (id)createNITAuthorizationModule
{
    // Generating module components
    id <NITAuthorizationPresenterProtocol, NITAuthorizationInteractorOutputProtocol> presenter = [NITAuthorizationPresenter new];
    id <NITAuthorizationInteractorInputProtocol> interactor = [NITAuthorizationInteractor new];
    id <NITAuthorizationAPIDataManagerInputProtocol> APIDataManager = [NITAuthorizationAPIDataManager new];
    id <NITAuthorizationLocalDataManagerInputProtocol> localDataManager = [NITAuthorizationLocalDataManager new];
    id <NITAuthorizationWireFrameProtocol> wireFrame= [NITAuthorizationWireFrame new];
    id <NITAuthorizationViewProtocol> view = [(NITAuthorizationWireFrame *)wireFrame createViewControllerWithKey:_s(NITAuthorizationViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITAuthorizationModuleFrom:(UIViewController *)fromViewController
{
    NITAuthorizationViewController * vc = [NITAuthorizationWireFrame createNITAuthorizationModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)openRegisterFrom:(UIViewController *)fromViewController
{
    [NITRegisterWireFrame presentNITRegisterModuleFrom:fromViewController];
}

- (void)openLoginFrom:(UIViewController *)fromViewController
{
    [NITLoginWireFrame presentNITLoginModuleFrom:fromViewController];
}

- (void)backFrom:(UIViewController *)fromViewController
{
    NSMutableArray * stack =  [NSMutableArray arrayWithArray:[fromViewController.navigationController viewControllers]];
    if (stack.count > 1)
    {
        [stack removeLastObject];
        fromViewController.navigationController.viewControllers = stack;
    }
    
    [fromViewController.navigationController popViewControllerAnimated:false];
}

@end
