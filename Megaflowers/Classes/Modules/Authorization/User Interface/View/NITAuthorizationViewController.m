//
//  NITAuthorizationViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAuthorizationViewController.h"

@interface NITAuthorizationViewController ()

@property (nonatomic) IBOutlet UIButton * fbBtn;
@property (nonatomic) IBOutlet UIButton * vkBtn;
@property (nonatomic) IBOutlet UIButton * mailBtn;
@property (nonatomic) IBOutlet UIButton * okBtn;
@property (nonatomic) IBOutlet UIButton * loginBtn;
@property (nonatomic) IBOutlet UIButton * registerBtn;

@end

@implementation NITAuthorizationViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - IBAction
- (IBAction)fbTapped:(id)sender
{
    [self.presenter fbLogin];
}

- (IBAction)vkTapped:(id)sender
{
    [self.presenter vkLogin];
}

- (IBAction)okTapped:(id)sender
{
    [self.presenter okLogin];
}

- (IBAction)loginAction:(id)sender
{
    [self.presenter loginUser];
}

- (IBAction)registerAction:(id)sender
{
    [self.presenter registerUser];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Авторизация", nil);
}

@end
