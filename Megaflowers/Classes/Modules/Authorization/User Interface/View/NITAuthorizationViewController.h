//
//  NITAuthorizationViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITAuthorizationProtocols.h"

@interface NITAuthorizationViewController : NITBaseViewController <NITAuthorizationViewProtocol>

@property (nonatomic, strong) id <NITAuthorizationPresenterProtocol> presenter;

@end
