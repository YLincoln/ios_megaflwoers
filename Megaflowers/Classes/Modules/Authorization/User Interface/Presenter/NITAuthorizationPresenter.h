//
//  NITAuthorizationPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITAuthorizationProtocols.h"

@class NITAuthorizationWireFrame;

@interface NITAuthorizationPresenter : NITRootPresenter <NITAuthorizationPresenterProtocol, NITAuthorizationInteractorOutputProtocol>

@property (nonatomic, weak) id <NITAuthorizationViewProtocol> view;
@property (nonatomic, strong) id <NITAuthorizationInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITAuthorizationWireFrameProtocol> wireFrame;

@end
