//
//  NITAuthorizationPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAuthorizationPresenter.h"
#import "NITAuthorizationWireframe.h"

@implementation NITAuthorizationPresenter

#pragma mark - NITAuthorizationPresenterProtocol
- (void)loginUser
{
    [self.wireFrame openLoginFrom:self.view];
}

- (void)registerUser
{
    [self.wireFrame openRegisterFrom:self.view];
}

- (void)fbLogin
{
    weaken(self);
    [SOCIAL setViewController:self.view];
    [SOCIAL fbLoginWithHandler:^(BOOL success, id token, NSError *error) {
        
        if (success)
        {
            [weakSelf authorizeWithServiceName:fbService serviceToken:token email:nil];
        }
        else if (error)
        {
            [weakSelf showError:error.localizedDescription fromView:weakSelf.view];
        }
    }];
}

- (void)vkLogin
{
    weaken(self);
    [SOCIAL setViewController:self.view];
    [SOCIAL vkLoginWithHandler:^(BOOL success, NSDictionary *data, NSError *error) {
        
        if (success)
        {
            [weakSelf authorizeWithServiceName:vkService serviceToken:data[@"token"] email:data[@"email"]];
        }
        else if (error)
        {
            [weakSelf showError:error.localizedDescription fromView:weakSelf.view];
        }
    }];
}

- (void)okLogin
{
    weaken(self);
    [SOCIAL setViewController:self.view];
    [SOCIAL okLoginWithHandler:^(BOOL success, id token, NSError *error) {
        
        if (success)
        {
            [weakSelf authorizeWithServiceName:okService serviceToken:token email:nil];
        }
        else if (error)
        {
            [weakSelf showError:error.localizedDescription fromView:weakSelf.view];
        }
    }];
}

#pragma mark - Private
- (void)authorizeWithServiceName:(NSString *)serviceName serviceToken:(NSString *)token email:(NSString *)email
{
    weaken(self);
    [self.interactor authorizeWithServiceName:serviceName serviceToken:token email:nil withHandler:^(BOOL success, NSString *error) {
        if (success)
        {
            [weakSelf.wireFrame backFrom:weakSelf.view];
        }
        else
        {
            [weakSelf showError:error fromView:self.view];
        }
    }];
}

@end
