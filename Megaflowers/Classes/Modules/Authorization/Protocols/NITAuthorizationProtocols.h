//
//  NITAuthorizationProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITAuthorizationInteractorOutputProtocol;
@protocol NITAuthorizationInteractorInputProtocol;
@protocol NITAuthorizationViewProtocol;
@protocol NITAuthorizationPresenterProtocol;
@protocol NITAuthorizationLocalDataManagerInputProtocol;
@protocol NITAuthorizationAPIDataManagerInputProtocol;

@class NITAuthorizationWireFrame;

static NSString * const fbService = @"facebook";
static NSString * const vkService = @"vkontakte";
static NSString * const okService = @"odnoklassniki";

@protocol NITAuthorizationViewProtocol
@required
@property (nonatomic, strong) id <NITAuthorizationPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITAuthorizationWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITAuthorizationModule;
+ (void)presentNITAuthorizationModuleFrom:(id)fromView;
- (void)openLoginFrom:(id)fromView;
- (void)openRegisterFrom:(id)fromView;
- (void)backFrom:(id)fromView;
@end

@protocol NITAuthorizationPresenterProtocol
@required
@property (nonatomic, weak) id <NITAuthorizationViewProtocol> view;
@property (nonatomic, strong) id <NITAuthorizationInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITAuthorizationWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)fbLogin;
- (void)vkLogin;
- (void)okLogin;
- (void)loginUser;
- (void)registerUser;
@end

@protocol NITAuthorizationInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITAuthorizationInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITAuthorizationInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITAuthorizationAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITAuthorizationLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)authorizeWithServiceName:(NSString *)serviceName serviceToken:(NSString *)serviceToken email:(NSString *)email withHandler:(void(^)(BOOL success, NSString * error))handler;
@end


@protocol NITAuthorizationDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITAuthorizationAPIDataManagerInputProtocol <NITAuthorizationDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)authorizeWithServiceName:(NSString *)serviceName serviceToken:(NSString *)serviceToken email:(NSString *)email withHandler:(void(^)(NSError *error, NSString * token))handler;
@end

@protocol NITAuthorizationLocalDataManagerInputProtocol <NITAuthorizationDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
