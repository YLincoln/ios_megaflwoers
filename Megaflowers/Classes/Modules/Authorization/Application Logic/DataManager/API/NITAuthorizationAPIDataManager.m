//
//  NITAuthorizationAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAuthorizationAPIDataManager.h"
#import "SWGAccountApi.h"

@implementation NITAuthorizationAPIDataManager

#pragma mark - NITAuthorizationAPIDataManagerInputProtocol
- (void)authorizeWithServiceName:(NSString *)serviceName serviceToken:(NSString *)serviceToken email:email withHandler:(void (^)(NSError *error, NSString *token))handler
{
    [HELPER startLoading];
    [[SWGAccountApi new] loginPostWithPhone:nil
                                      email:email
                                serviceName:serviceName
                               serviceToken:serviceToken
                                   password:nil
                          completionHandler:^(NSString *output, NSError *error) {
                              [HELPER stopLoading];
                              if (error)
                              {
                                  [HELPER logError:error method:METHOD_NAME];
                                  handler(error, nil);
                              }
                              else
                              {
                                  handler(nil, output);
                              }
                          }];
}

@end
