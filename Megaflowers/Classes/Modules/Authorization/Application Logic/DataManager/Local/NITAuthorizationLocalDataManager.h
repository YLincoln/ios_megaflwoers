//
//  NITAuthorizationLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITAuthorizationProtocols.h"

@interface NITAuthorizationLocalDataManager : NSObject <NITAuthorizationLocalDataManagerInputProtocol>

@end
