//
//  NITAuthorizationInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAuthorizationInteractor.h"
#import "NITLoginInteractor.h"
#import "NITLoginAPIDataManager.h"

@implementation NITAuthorizationInteractor

#pragma mark - NITAuthorizationInteractorInputProtocol
- (void)authorizeWithServiceName:(NSString *)serviceName serviceToken:(NSString *)serviceToken email:(NSString *)email withHandler:(void (^)(BOOL success, NSString *error))handler
{
    [self.APIDataManager authorizeWithServiceName:serviceName serviceToken:serviceToken email:email withHandler:^(NSError *error, NSString *token) {
        
        if (token.length > 0 && !error)
        {
            [USER setAccessToken:token];
            [TRACKER trackEvent:TrackerEventSocialAuthorization parameters:@{@"type":serviceName}];
            
            NITLoginInteractor * loginInteractor = [NITLoginInteractor new];
            loginInteractor.APIDataManager = [NITLoginAPIDataManager new];
            [loginInteractor syncUserDataWithHandler:^(BOOL success, NSError *error) {
                handler(true, nil);
            }];
        }
        else
        {
            handler(false, error.interfaceDescription);
        }
    }];
}

@end
