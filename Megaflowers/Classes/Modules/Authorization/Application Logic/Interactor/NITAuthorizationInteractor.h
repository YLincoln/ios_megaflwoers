//
//  NITAuthorizationInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITAuthorizationProtocols.h"

@interface NITAuthorizationInteractor : NSObject <NITAuthorizationInteractorInputProtocol>

@property (nonatomic, weak) id <NITAuthorizationInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITAuthorizationAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITAuthorizationLocalDataManagerInputProtocol> localDataManager;

@end
