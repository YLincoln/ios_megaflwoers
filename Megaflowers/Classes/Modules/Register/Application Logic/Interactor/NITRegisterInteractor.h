//
//  NITRegisterInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegisterProtocols.h"

@interface NITRegisterInteractor : NSObject <NITRegisterInteractorInputProtocol>

@property (nonatomic, weak) id <NITRegisterInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRegisterAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRegisterLocalDataManagerInputProtocol> localDataManager;

@end
