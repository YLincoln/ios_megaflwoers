//
//  NITRegisterModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, RegisterFieldType) {
    RegisterFieldTypeName           = 0,
    RegisterFieldTypePhone          = 1,
    RegisterFieldTypeEmail          = 2,
    RegisterFieldTypePassword       = 3,
    RegisterFieldTypePasswordCheck  = 4,
    RegisterFieldTypeAgreement      = 5
};

@interface NITRegisterModel : NSObject

@property (nonatomic) RegisterFieldType type;
@property (nonatomic) NSString * value;
@property (nonatomic) NSString * title;
@property (nonatomic) BOOL selected;
@property (nonatomic) BOOL isHighlighted;
@property (nonatomic) BOOL isCorrect;

- (instancetype)initWithType:(RegisterFieldType)type;

@end
