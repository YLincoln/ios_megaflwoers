//
//  NITRegisterModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterModel.h"
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"

@implementation NITRegisterModel

- (instancetype)initWithType:(RegisterFieldType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

- (void)setType:(RegisterFieldType)type
{
    _type = type;
    [self updateData];
}

- (BOOL)isCorrect
{
    switch (self.type)
    {
        case RegisterFieldTypeName:
        case RegisterFieldTypePassword:
        case RegisterFieldTypePasswordCheck:
            return self.value.length > 0;
            
        case RegisterFieldTypePhone:
            return [PHONE isValidPhoneNumber:self.value];
            
        case RegisterFieldTypeEmail:
            return [self isValidEmail];

        case RegisterFieldTypeAgreement:
            return self.selected;
    }
    
    return true;
}

#pragma mark - Private
- (void)updateData
{
    switch (self.type) {
        case RegisterFieldTypeName:
            self.title = NSLocalizedString(@"Как вас зовут?", nil);
            break;
            
        case RegisterFieldTypePhone:
            self.title = NSLocalizedString(@"Телефон", nil);
            break;
            
        case RegisterFieldTypeEmail:
            self.title = NSLocalizedString(@"E-mail", nil);
            break;
            
        case RegisterFieldTypePassword:
            self.title = NSLocalizedString(@"Пароль", nil);
            break;
            
        case RegisterFieldTypePasswordCheck:
            self.title = NSLocalizedString(@"Повтор пароля", nil);
            break;
            
        case RegisterFieldTypeAgreement:
            self.title = @"";
            break;
            
    }
}

- (BOOL)isValidEmail
{
    if (self.value.length == 0)
    {
        return false;
    }
    
    NSString * emailid = self.value;
    NSString * emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate * emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailid];
}

@end
