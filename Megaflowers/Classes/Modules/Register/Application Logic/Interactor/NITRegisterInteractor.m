//
//  NITRegisterInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterInteractor.h"
#import "NITLoginInteractor.h"
#import "NITLoginAPIDataManager.h"

@implementation NITRegisterInteractor

- (void)registerNewUserWithName:(NSString *)name phone:(NSString *)phone email:(NSString *)email password:(NSString *)password withHandler:(void (^)(BOOL, NSError *))handler
{
    NSString * phoneNumber = [self checkPhone:phone];
    [self.APIDataManager registerNewUserWithName:name phone:phoneNumber email:email password:password withHandler:^(NSString *token, NSError *error) {

        if (token.length > 0 && !error)
        {
            USER.accessToken = token;
            USER.name = name;
            USER.phone = phoneNumber;
            USER.email = email;
            USER.password = password;
            
            [TRACKER trackEvent:TrackerEventRegister];
            
            handler(true, nil);
        }
        else
        {
            switch (error.swaggerCode) {
                case 400: // validation error
                    handler(false, error);
                    break;
                    
                case 403: // user exist but need activate
                    if (token.length > 0) USER.accessToken = token;
                    handler(true, error);
                    break;
                    
                default:
                    handler(false, error);
                    break;
            }
        }
    }];
}

- (void)loginUserByPhone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(BOOL success))handler
{
    NSString * phoneNumber = [self checkPhone:phone];
    NITLoginInteractor * loginInteractor = [NITLoginInteractor new];
    loginInteractor.APIDataManager = [NITLoginAPIDataManager new];
    [loginInteractor loginByEmail:nil phone:phoneNumber password:password withHandler:^(BOOL success, NSError *error) {
        handler(success);
    }];
}

- (void)syncUserDataWithHandler:(void(^)(BOOL success))handler
{
    NITLoginInteractor * loginInteractor = [NITLoginInteractor new];
    loginInteractor.APIDataManager = [NITLoginAPIDataManager new];
    [loginInteractor syncUserDataWithHandler:^(BOOL success, NSError *error) {
        handler(success);
    }];
}

#pragma mark - Private
- (NSString *)checkPhone:(NSString *)phone
{
    return [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
}

@end
