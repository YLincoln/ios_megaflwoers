//
//  NITRegisterAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterAPIDataManager.h"
#import "SWGUserApi.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITRegisterAPIDataManager

- (void)registerNewUserWithName:(NSString *)name phone:(NSString *)phone email:(NSString *)email password:(NSString *)password withHandler:(void (^)(NSString *, NSError *))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userPostWithSite:API.site
                                 phone:[phone stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                  name:[name stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                 email:[email stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                              password:[password stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                  code:nil
                     completionHandler:^(NSString *output, NSError *error) {
                         [HELPER stopLoading];
                         if (error) [HELPER logError:error method:METHOD_NAME];
                         if (handler) handler(output, error);
                     }];
}

@end
