//
//  NITRegisterLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegisterProtocols.h"

@interface NITRegisterLocalDataManager : NSObject <NITRegisterLocalDataManagerInputProtocol>

@end
