//
//  NITRegisterViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITRegisterProtocols.h"

@interface NITRegisterViewController : NITBaseViewController <NITRegisterViewProtocol>

@property (nonatomic, strong) id <NITRegisterPresenterProtocol> presenter;

@end
