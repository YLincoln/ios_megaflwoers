//
//  NITRegisterViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterViewController.h"
#import "NITRegisterFieldCell.h"

@interface NITRegisterViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet PhoneTextField * phoneField;
@property (nonatomic) IBOutlet UIButton * agreementBtn;
@property (nonatomic) IBOutlet UIButton * registerBtn;
@property (nonatomic) IBOutlet UILabel * agreementLbl;
@property (nonatomic) IBOutlet UIButton * showAgreementBtn;

@property (nonatomic) NSArray <NITRegisterModel *> * items;
@property (nonatomic) NITRegisterModel * phoneItem;
@property (nonatomic) NITRegisterModel * agreementItem;

@end

@implementation NITRegisterViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:true];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.tabBarController.tabBar setHidden:false];
}

#pragma mark - IBAction
- (IBAction)agreementTapped:(id)sender
{
    self.agreementItem.selected = !self.agreementItem.selected;
    [self updateAgreement];
}

- (IBAction)showAgreementAction:(id)sender
{
    [self.presenter showAgreement];
}

- (IBAction)registerAction:(id)sender
{
    [self.presenter registerAction];
}

- (IBAction)dismissAction:(id)sender
{
    [self.presenter dismissAction];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Создание аккаунта", nil);
    
    [self setKeyboardActiv:true];
    
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITRegisterFieldCell) bundle:nil] forCellReuseIdentifier:_s(NITRegisterFieldCell)];
    [self.tableView setTableFooterView:[UIView new]];
    
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    
    switch (self.presenter.presentMode) {
        case RegisterPresentModeModal: {
            [self.navigationItem setHidesBackButton:true];
            UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Отмена", nil)
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(dismissAction:)];
            item.tintColor = RGB(10, 88, 43);
            self.navigationItem.leftBarButtonItem = item;
        } break;
            
        case RegisterPresentModePush:
            [self.navigationItem setHidesBackButton:false];
            break;
    }
    
    [self.view layoutIfNeeded];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.phoneField becomeFirstResponder];
    });
}

- (void)updateAgreement
{
    if (self.agreementItem.isHighlighted && !self.agreementItem.selected)
    {
        [self.agreementBtn setImage:[UIImage imageNamed:@"around"] forState:UIControlStateNormal];
        [self.agreementLbl setTextColor:RGB(217, 67, 67)];
    }
    else
    {
        [self.agreementBtn setImage:[UIImage imageNamed:self.agreementItem.selected ? @"check" : @"check_ar"] forState:UIControlStateNormal];
        [self.agreementLbl setTextColor:RGB(10, 88, 43)];
    }
}

- (void)updatePhone
{
    self.phoneField.text = self.phoneItem.value;
    self.phoneField.textColor = self.phoneItem.isHighlighted ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.phoneField.returnKeyType = UIReturnKeyNext;
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneItem.title
                                                                            attributes:@{NSForegroundColorAttributeName: self.phoneItem.isHighlighted ? RGB(217, 67, 67) : RGB(164, 183, 171)}];
}

#pragma mark - NITRegisterPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITRegisterModel *> *)models
{
    for (NITRegisterModel * model in models)
    {
        switch (model.type) {
            case RegisterFieldTypePhone:
                self.phoneItem = model;
                [self updatePhone];
                break;
            case RegisterFieldTypeAgreement:
                self.agreementItem = model;
                [self updateAgreement];
                break;
            default:
                break;
        }
    }
}

/*- (void)reloadDataByFieldModels:(NSArray<NITRegisterModel *> *)models andAgreementModel:(NITRegisterModel *)agreementModel
{
    self.agreementItem = agreementModel;
    [self updateAgreement];
    
    self.items = models;
    [self.tableView reloadData];
}*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITRegisterFieldCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITRegisterFieldCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.textField.tag = 200 + indexPath.row;
    
    return cell;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.phoneField.textColor = RGB(10, 88, 43);
    self.phoneItem.isHighlighted = false;
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.phoneItem.value = textField.text;
}

@end
