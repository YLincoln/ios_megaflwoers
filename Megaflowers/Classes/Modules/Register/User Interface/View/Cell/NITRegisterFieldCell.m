//
//  NITRegisterFieldCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterFieldCell.h"
#import "NITRegisterModel.h"
#import "AKNumericFormatter.h"

@interface NITRegisterFieldCell () <UITextFieldDelegate>

@end

@implementation NITRegisterFieldCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITRegisterModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.textField.formatEnabled = false;
    self.textField.textColor = self.model.isHighlighted ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.textField.returnKeyType = UIReturnKeyNext;
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.model.title
                                                                           attributes:@{NSForegroundColorAttributeName: self.model.isHighlighted ? RGB(217, 67, 67) : RGB(164, 183, 171)}];
    
    switch (self.model.type) {
        case RegisterFieldTypePhone: {
            self.textField.keyboardType = UIKeyboardTypeDecimalPad;
            self.textField.secureTextEntry = false;
            self.textField.formatEnabled = false;
        }
            break;
        case RegisterFieldTypeEmail: {
            self.textField.keyboardType = UIKeyboardTypeEmailAddress;
            self.textField.secureTextEntry = false;
        }
            break;
        case RegisterFieldTypePassword: {
            self.textField.keyboardType = UIKeyboardTypeDefault;
            self.textField.secureTextEntry = true;
        }
        case RegisterFieldTypePasswordCheck: {
            self.textField.keyboardType = UIKeyboardTypeDefault;
            self.textField.secureTextEntry = true;
            self.textField.returnKeyType = UIReturnKeyDone;
        }
            break;
        default:
            self.textField.keyboardType = UIKeyboardTypeDefault;
            self.textField.secureTextEntry = false;
            break;
    }
    
    self.textField.text = self.model.value;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder * nextResponder = [self.superview viewWithTag:nextTag];
    
    if (nextResponder)
    {
        [nextResponder becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.textField.textColor = RGB(10, 88, 43);
    self.model.isHighlighted = false;
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.model.value = textField.text;
}

@end
