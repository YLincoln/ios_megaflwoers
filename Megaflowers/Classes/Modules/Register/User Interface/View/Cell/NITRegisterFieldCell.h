//
//  NITRegisterFieldCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITRegisterModel;

@interface NITRegisterFieldCell : UITableViewCell

@property (nonatomic) IBOutlet PhoneTextField * textField;
@property (nonatomic) NITRegisterModel * model;

@end
