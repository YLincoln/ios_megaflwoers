//
//  NITRegisterWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegisterProtocols.h"
#import "NITRegisterViewController.h"
#import "NITRegisterLocalDataManager.h"
#import "NITRegisterAPIDataManager.h"
#import "NITRegisterInteractor.h"
#import "NITRegisterPresenter.h"
#import "NITRegisterWireframe.h"
#import "NITRootWireframe.h"

@interface NITRegisterWireFrame : NITRootWireframe <NITRegisterWireFrameProtocol>

@property (nonatomic) RegisterHandler handler;

@end
