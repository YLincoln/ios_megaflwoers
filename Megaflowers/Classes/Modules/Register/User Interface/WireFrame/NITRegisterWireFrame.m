//
//  NITRegisterWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterWireFrame.h"
#import "NITCheckSMSWireFrame.h"
#import "NITUserAgreementWireFrame.h"
#import "NITPasswordRecoveryWireFrame.h"

@implementation NITRegisterWireFrame

+ (id)createNITRegisterModule
{
    // Generating module components
    id <NITRegisterPresenterProtocol, NITRegisterInteractorOutputProtocol> presenter = [NITRegisterPresenter new];
    id <NITRegisterInteractorInputProtocol> interactor = [NITRegisterInteractor new];
    id <NITRegisterAPIDataManagerInputProtocol> APIDataManager = [NITRegisterAPIDataManager new];
    id <NITRegisterLocalDataManagerInputProtocol> localDataManager = [NITRegisterLocalDataManager new];
    id <NITRegisterWireFrameProtocol> wireFrame= [NITRegisterWireFrame new];
    id <NITRegisterViewProtocol> view = [(NITRegisterWireFrame *)wireFrame createViewControllerWithKey:_s(NITRegisterViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITRegisterWireFrameProtocol
+ (void)presentNITRegisterModule
{
    NITRegisterViewController * view = [NITRegisterWireFrame createNITRegisterModule];
    [WINDOW.visibleController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITRegisterModuleFrom:(UIViewController *)fromViewController
{
    NITRegisterViewController * view = [NITRegisterWireFrame createNITRegisterModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentModalNITRegisterModule:(UIViewController *)fromViewController withHandler:(void(^)())handler
{
    NITRegisterViewController * view = [NITRegisterWireFrame createNITRegisterModule];
    view.presenter.presentMode = RegisterPresentModeModal;
    view.presenter.wireFrame.handler = handler;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:view];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

- (void)backActionFrom:(UIViewController *)fromView
{
    [fromView.navigationController popToRootViewControllerAnimated:true];
}

- (void)showAgreementFrom:(UIViewController *)fromView withDelegate:(id)delegate
{
    [NITUserAgreementWireFrame presentNITUserAgreementModuleFrom:fromView withDelegate:delegate];
}

- (void)showCheckSMSFrom:(UIViewController *)fromView withDelegate:(id)delegate phone:(NSString *)phone
{
    [NITCheckSMSWireFrame presentNITCheckSMSModuleFrom:fromView
                                          withDelegate:delegate
                                                  mode:SMSRecoveryModeActivateAccount
                                                 phone:phone
                                                 token:nil];
}

- (void)showRestorePasswordFrom:(UIViewController *)fromViewController phone:(NSString *)phone
{
    [NITPasswordRecoveryWireFrame presentNITPasswordRecoveryModuleFrom:fromViewController phone:phone];
}

- (void)dismissView:(UIViewController *)fromViewController
{
    [fromViewController dismissViewControllerAnimated:true completion:^{
        if (self.handler) self.handler();
    }];
}

@end
