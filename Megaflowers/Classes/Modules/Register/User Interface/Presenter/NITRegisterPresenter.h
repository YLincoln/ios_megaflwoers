//
//  NITRegisterPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegisterProtocols.h"

@class NITRegisterWireFrame;

@interface NITRegisterPresenter : NITRootPresenter <NITRegisterPresenterProtocol, NITRegisterInteractorOutputProtocol>

@property (nonatomic, weak) id <NITRegisterViewProtocol> view;
@property (nonatomic, strong) id <NITRegisterInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRegisterWireFrameProtocol> wireFrame;
@property (nonatomic) RegisterPresentMode presentMode;

@end
