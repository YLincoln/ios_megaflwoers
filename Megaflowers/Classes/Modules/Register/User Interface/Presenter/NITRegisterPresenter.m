//
//  NITRegisterPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterPresenter.h"
#import "NITRegisterWireframe.h"
#import "NITUserAgreementProtocols.h"
#import "NITCheckSMSProtocols.h"

@interface NITRegisterPresenter ()
<
NITUserAgreementDelegate,
NITCheckSMSDelegate
>

/*@property (nonatomic) NITRegisterModel * nameModel;
@property (nonatomic) NITRegisterModel * emailModel;
@property (nonatomic) NITRegisterModel * passwordModel;
@property (nonatomic) NITRegisterModel * passwordCheckModel;*/

@property (nonatomic) NITRegisterModel * phoneModel;
@property (nonatomic) NITRegisterModel * agreementModel;

@property (nonatomic) NSArray <NITRegisterModel *> * models;

@end

@implementation NITRegisterPresenter

#pragma mark - NITRegisterPresenterProtocol
- (void)initData
{
    /*self.nameModel = [[NITRegisterModel alloc] initWithType:RegisterFieldTypeName];
    self.emailModel = [[NITRegisterModel alloc] initWithType:RegisterFieldTypeEmail];
    self.passwordModel = [[NITRegisterModel alloc] initWithType:RegisterFieldTypePassword];
    self.passwordCheckModel = [[NITRegisterModel alloc] initWithType:RegisterFieldTypePasswordCheck];*/
    
    self.phoneModel = [[NITRegisterModel alloc] initWithType:RegisterFieldTypePhone];
    self.agreementModel = [[NITRegisterModel alloc] initWithType:RegisterFieldTypeAgreement];
    self.models = @[self.phoneModel, self.agreementModel];
    
    [self.view reloadDataByModels:self.models];
}

- (void)registerAction
{
    if ([self registerAvailable])
    {
        weaken(self);
        [self.interactor registerNewUserWithName:nil //self.nameModel.value
                                           phone:self.phoneModel.value
                                           email:nil //self.emailModel.value
                                        password:nil //self.passwordModel.value
                                     withHandler:^(BOOL success, NSError *error) {
                                         
                                         if (success)
                                         {
                                             [weakSelf.wireFrame showCheckSMSFrom:weakSelf.view withDelegate:weakSelf phone:weakSelf.phoneModel.value];
                                         }
                                         else
                                         {
                                             if (error.swaggerCode == 400)
                                             {
                                                 NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
                                                 
                                                 weaken(self);
                                                 NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Восстановить пароль", nil) actionBlock:^{
                                                     [weakSelf.wireFrame showRestorePasswordFrom:self.view phone:weakSelf.phoneModel.value];
                                                 }];
                                                 
                                                 [HELPER showActionSheetFromView:self.view withTitle:error.interfaceDescription actionButtons:@[action1] cancelButton:cancel];
                                             }
                                             else
                                             {
                                                 [weakSelf showRegisterError:error.interfaceDescription];
                                             }
                                         }
                                     }];
    }
    else
    {
        [self.view reloadDataByModels:self.models];
    }
}

- (void)dismissAction
{
    [self closeAction];
}

- (void)showAgreement
{
    [self.wireFrame showAgreementFrom:self.view withDelegate:self];
}

#pragma mark - Private
- (void)closeAction
{
    switch (self.presentMode) {
        case RegisterPresentModePush: {
            [self.wireFrame backActionFrom:self.view];
        } break;
        case RegisterPresentModeModal: {
            [self.wireFrame dismissView:self.view];
        } break;
    }
}

- (void)showRegisterError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Повторить", nil) actionBlock:^{
        [weakSelf registerAction];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:error actionButtons:@[action1] cancelButton:cancel];
}

- (BOOL)registerAvailable
{
    BOOL available = true;
    
    if (!self.agreementModel.isCorrect)
    {
        self.agreementModel.isHighlighted = true;
        available = false;
    }
    
    if (!self.phoneModel.isCorrect)
    {
        self.phoneModel.isHighlighted = true;
        available = false;
    }
    
    /*if (!self.nameModel.isCorrect)
    {
        self.nameModel.isHighlighted = true;
        available = false;
    }
    
    if (!self.emailModel.isCorrect)
    {
        self.emailModel.isHighlighted = true;
        available = false;
    }
    
    if (!self.passwordModel.isCorrect)
    {
        self.passwordModel.isHighlighted = true;
        available = false;
    }
    
    if (!self.passwordCheckModel.isCorrect)
    {
        self.passwordCheckModel.isHighlighted = true;
        available = false;
    }
    
    if (![self.passwordModel.value isEqualToString:self.passwordCheckModel.value])
    {
        self.passwordCheckModel.isHighlighted = true;
        available = false;
    }*/
    
    return available;
}

#pragma mark - NITUserAgreementDelegate
- (void)didAcceptAgreement
{
    self.agreementModel.selected = true;
    self.agreementModel.isHighlighted = false;
    
    [self.view reloadDataByModels:self.models];
}

#pragma mark - NITCheckSMSDelegate
- (void)didCheckSMS:(BOOL)success
{
    if (success)
    {
        weaken(self);
        /*[self.interactor loginUserByPhone:self.phoneModel.value password:self.passwordModel.value withHandler:^(BOOL success) {
            [weakSelf.wireFrame backActionFrom:weakSelf.view];
        }];*/
        [self.interactor syncUserDataWithHandler:^(BOOL success) {
            [weakSelf closeAction];
        }];
    }
    else
    {
        self.phoneModel.isHighlighted = true;
        [self.view reloadDataByModels:self.models];
    }
}

@end
