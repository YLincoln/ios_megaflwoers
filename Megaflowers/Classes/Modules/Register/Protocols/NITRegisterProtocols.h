//
//  NITRegisterProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRegisterModel.h"
#import "SWGUser.h"

@protocol NITRegisterInteractorOutputProtocol;
@protocol NITRegisterInteractorInputProtocol;
@protocol NITRegisterViewProtocol;
@protocol NITRegisterPresenterProtocol;
@protocol NITRegisterLocalDataManagerInputProtocol;
@protocol NITRegisterAPIDataManagerInputProtocol;

@class NITRegisterWireFrame;

typedef CF_ENUM (NSUInteger, RegisterPresentMode) {
    RegisterPresentModePush    = 0,
    RegisterPresentModeModal   = 1
};

typedef void (^RegisterHandler) ();

@protocol NITRegisterViewProtocol
@required
@property (nonatomic, strong) id <NITRegisterPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITRegisterModel *> *)models;
/*- (void)reloadDataByFieldModels:(NSArray <NITRegisterModel *> *)models andAgreementModel:(NITRegisterModel *)agreementModel;*/
@end

@protocol NITRegisterWireFrameProtocol
@property (nonatomic) RegisterHandler handler;
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITRegisterModuleFrom:(id)fromView;
+ (void)presentModalNITRegisterModule:(id)fromView withHandler:(void(^)())handler;
+ (void)presentNITRegisterModule;
- (void)showAgreementFrom:(id)fromView withDelegate:(id)delegate;
- (void)backActionFrom:(id)fromView;
- (void)dismissView:(id)fromView;
- (void)showCheckSMSFrom:(id)fromView withDelegate:(id)delegate phone:(NSString *)phone;
- (void)showRestorePasswordFrom:(id)fromView phone:(NSString *)phone;
@end

@protocol NITRegisterPresenterProtocol
@required
@property (nonatomic, weak) id <NITRegisterViewProtocol> view;
@property (nonatomic, strong) id <NITRegisterInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRegisterWireFrameProtocol> wireFrame;
@property (nonatomic) RegisterPresentMode presentMode;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)showAgreement;
- (void)registerAction;
- (void)dismissAction;
@end

@protocol NITRegisterInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITRegisterInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITRegisterInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRegisterAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRegisterLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)registerNewUserWithName:(NSString *)name
                          phone:(NSString *)phone
                          email:(NSString *)emeil
                       password:(NSString *)password
                    withHandler:(void(^)(BOOL success, NSError * error))handler;
- (void)loginUserByPhone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(BOOL success))handler;
- (void)syncUserDataWithHandler:(void(^)(BOOL success))handler;
@end


@protocol NITRegisterDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITRegisterAPIDataManagerInputProtocol <NITRegisterDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)registerNewUserWithName:(NSString *)name
                          phone:(NSString *)phone
                          email:(NSString *)emeil
                       password:(NSString *)password
                    withHandler:(void(^)(NSString *token, NSError * error))handler;
@end

@protocol NITRegisterLocalDataManagerInputProtocol <NITRegisterDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
