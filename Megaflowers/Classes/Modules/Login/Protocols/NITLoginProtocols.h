//
//  NITLoginProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "SWGUser.h"
#import "SWGUserEmail.h"
#import "SWGUserPhone.h"
#import "SWGBouquet.h"
#import "SWGUserCoupon.h"
#import "SWGUserEvent.h"
#import "SWGSalon.h"
#import "SWGUserBouquet.h"
#import "SWGOrder.h"
#import "SWGEvent.h"

@protocol NITLoginInteractorOutputProtocol;
@protocol NITLoginInteractorInputProtocol;
@protocol NITLoginViewProtocol;
@protocol NITLoginPresenterProtocol;
@protocol NITLoginLocalDataManagerInputProtocol;
@protocol NITLoginAPIDataManagerInputProtocol;

@class NITLoginWireFrame;

typedef CF_ENUM (NSUInteger, LoginState) {
    LoginStateNormal = 0,
    LoginStateError  = 1
};

typedef CF_ENUM (NSUInteger, LoginPresentMode) {
    LoginPresentModePush    = 0,
    LoginPresentModeModal   = 1
};

typedef void (^LoginHandler) ();

@protocol NITLoginViewProtocol
@required
@property (nonatomic, strong) id <NITLoginPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadState:(LoginState)state;
@end

@protocol NITLoginWireFrameProtocol
@property (nonatomic) LoginHandler handler;
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITLoginModuleFrom:(id)fromView;
+ (void)presentModalNITLoginModule:(id)fromView withHandler:(void(^)())handler;
+ (void)presentNITLoginModule;
- (void)backActionFrom:(id)fromView;
- (void)dismissView:(id)fromView;
- (void)showRestorePasswordFrom:(id)fromView;
@end

@protocol NITLoginPresenterProtocol
@required
@property (nonatomic, weak) id <NITLoginViewProtocol> view;
@property (nonatomic, strong) id <NITLoginInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITLoginWireFrameProtocol> wireFrame;
@property (nonatomic) LoginPresentMode presentMode;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)loginWithName:(NSString *)name password:(NSString *)password;
- (void)restorePassword;
- (void)fbLogin;
- (void)vkLogin;
- (void)okLogin;
- (void)dismissAction;
@end

@protocol NITLoginInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITLoginInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITLoginInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITLoginAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITLoginLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)loginByEmail:(NSString *)email phone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(BOOL success, NSError * error))handler;
- (void)syncUserDataWithHandler:(void(^)(BOOL success, NSError * error))handler;
@end


@protocol NITLoginDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITLoginAPIDataManagerInputProtocol <NITLoginDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)loginByEmail:(NSString *)email phone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(NSString *token, NSError * error))handler;
- (void)getUserDataWithHandler:(void(^)(SWGUser *result))handler;
- (void)getUserEmailsWithHandler:(void(^)(NSArray <SWGUserEmail> *result))handler;
- (void)getUserPhonesWithHandler:(void(^)(NSArray <SWGUserPhone> *result))handler;
- (void)getUserBouquetsWithHandler:(void(^)(NSArray <SWGBouquet> *result))handler;
- (void)getUserSkuWithHandler:(void(^)(NSArray <SWGUserBouquet> *result))handler;
- (void)getUserCouponsWithHandler:(void(^)(NSArray <SWGUserCoupon> *result))handler;
- (void)getUserEventsWithHandler:(void(^)(NSArray <SWGUserEvent> *result))handler;
- (void)getEventsWithHandler:(void(^)(NSArray <SWGEvent> *result))handler;
- (void)getUserSalonsWithHandler:(void(^)(NSArray <SWGSalon> *result))handler;
- (void)getUserOrdersWithHandler:(void(^)(NSArray <SWGOrder> *result))handler;
@end

@protocol NITLoginLocalDataManagerInputProtocol <NITLoginDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
