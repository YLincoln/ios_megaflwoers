//
//  NITLoginAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITLoginAPIDataManager.h"
#import "SWGUserApi.h"
#import "SWGAccountApi.h"
#import "SWGOrderApi.h"
#import "SWGEventApi.h"

@implementation NITLoginAPIDataManager

- (void)loginByEmail:(NSString *)email phone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(NSString *token, NSError * error))handler
{
    [HELPER startLoading];
    [[SWGAccountApi new] loginPostWithPhone:phone email:email serviceName:nil serviceToken:nil password:password completionHandler:^(NSString *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
            handler(nil, error);
        }
        else
        {
            handler(output, nil);
        }
    }];
}

// user data
- (void)getUserDataWithHandler:(void(^)(SWGUser *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(SWGUser *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

// user emeil
- (void)getUserEmailsWithHandler:(void(^)(NSArray <SWGUserEmail> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEmailGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserEmail> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

// user phone
- (void)getUserPhonesWithHandler:(void(^)(NSArray <SWGUserPhone> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userPhoneGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserPhone> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

// user bouquets
- (void)getUserBouquetsWithHandler:(void(^)(NSArray <SWGBouquet> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userBouquetFullGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGBouquet> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

- (void)getUserSkuWithHandler:(void(^)(NSArray <SWGUserBouquet> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userBouquetGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserBouquet> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

// user coupons
- (void)getUserCouponsWithHandler:(void(^)(NSArray <SWGUserCoupon> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userCouponGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserCoupon> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

// user events
- (void)getUserEventsWithHandler:(void(^)(NSArray <SWGUserEvent> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userEventGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGUserEvent> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

- (void)getEventsWithHandler:(void(^)(NSArray <SWGEvent> *result))handler
{
    [HELPER startLoading];
    [[SWGEventApi new] eventGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGEvent> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        
        handler(output);
    }];
}

// user salons
- (void)getUserSalonsWithHandler:(void(^)(NSArray <SWGSalon> *result))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userSalonGetWithLang:HELPER.lang site:API.site city:USER.cityID completionHandler:^(NSArray<SWGSalon> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

// user orders
- (void)getUserOrdersWithHandler:(void(^)(NSArray <SWGOrder> *result))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderGetWithLang:HELPER.lang site:API.site city:USER.cityID status:nil completionHandler:^(NSArray<SWGOrder> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler((id)[output bk_select:^BOOL(SWGOrder *obj) {
            return obj.statusId.integerValue > 1;
        }]);
    }];
}

@end
