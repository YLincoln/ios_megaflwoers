//
//  NITLoginInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITLoginProtocols.h"

@interface NITLoginInteractor : NSObject <NITLoginInteractorInputProtocol>

@property (nonatomic, weak) id <NITLoginInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITLoginAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITLoginLocalDataManagerInputProtocol> localDataManager;

@end
