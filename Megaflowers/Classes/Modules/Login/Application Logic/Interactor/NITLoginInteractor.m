//
//  NITLoginInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITLoginInteractor.h"
#import "NITFavoriteBouquet.h"
#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"
#import "NITCoupon.h"
#import "NITFavoriteSalon.h"
#import "NITAddressModel.h"
#import "NSArray+BlocksKit.h"
#import "NITOrder.h"
#import "NITPushSettingsInteractor.h"
#import "NITPushSettingsAPIDataManager.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITLoginInteractor

- (void)loginByEmail:(NSString *)email phone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(BOOL success, NSError * error))handler
{
    [self.APIDataManager loginByEmail:email phone:phone password:password withHandler:^(NSString *token, NSError *error) {

        if (token)
        {
            USER.accessToken = token;
            USER.password = password;
            [self syncUserDataWithHandler:handler];
        }
        else
        {
            handler(false, error);
        }
    }];
}

- (void)syncUserDataWithHandler:(void(^)(BOOL success, NSError * error))handler
{
    [self.APIDataManager getUserDataWithHandler:^(SWGUser *result) {
        
        if (result)
        {
            USER.name       = result.name;
            USER.identifier = [result._id stringValue];
            USER.address    = result.address;
            USER.bonus      = result.bonus;
            USER.gender     = [result.gender integerValue];
            USER.birthday   = [NSDate dateFromServerString:result.birthday];
            
            if (result.city)
            {
                USER.cityID   = result.city._id;
                USER.cityName = result.city.name;
            }
            
            if (result.discount)
            {
                USER.discount = [UserDiscount discontFromModel:result.discount];
            }
            
            [self.APIDataManager getUserEmailsWithHandler:^(NSArray<SWGUserEmail> *result) {
                
                SWGUserEmail * email = result.firstObject;
                if (email)
                {
                    USER.email = email.value;
                }
            }];
            
            [self.APIDataManager getUserPhonesWithHandler:^(NSArray<SWGUserPhone> *result) {
                
                SWGUserPhone * phone = result.firstObject;
                if (phone)
                {
                    USER.phone = phone.value;
                }
            }];
            
            [self.APIDataManager getUserSkuWithHandler:^(NSArray<SWGUserBouquet> *result) {
                
                NSArray * skuIDs = [result bk_map:^id(SWGUserBouquet *obj) {
                    return obj._id;
                }];
                
                [self.APIDataManager getUserBouquetsWithHandler:^(NSArray<SWGBouquet> *result) {
                    
                    NSArray * models = [self bouquetModelsFromEntities:result];
                    for (NITCatalogItemViewModel * model in models)
                    {
                        for (NITCatalogItemSKUViewModel * sku in model.skus)
                        {
                            if ([skuIDs containsObject:sku.identifier])
                            {
                                [NITFavoriteBouquet createFavoriteBouquetByModel:model andSkuModel:sku];
                            }
                        }
                    }
                }];
            }];
            
            [self.APIDataManager getUserEventsWithHandler:^(NSArray<SWGUserEvent> *result) {
                [NITEvent syncServerEvents:result];
                [self.APIDataManager getEventsWithHandler:^(NSArray<SWGEvent> *result) {
                    [NITEvent syncServerEvents:[self userEventsFromEntities:result]];
                }];
            }];
            
            [self.APIDataManager getUserSalonsWithHandler:^(NSArray<SWGSalon> *result) {
                [NITFavoriteSalon addSalons:[self salonModelsFromEntities:result]];
            }];
            
            [self.APIDataManager getUserOrdersWithHandler:^(NSArray *result) {
                [NITOrder addOrders:result];
            }];
            
            NITPushSettingsInteractor * pushInteractor = [NITPushSettingsInteractor new];
            pushInteractor.APIDataManager = [NITPushSettingsAPIDataManager new];
            [pushInteractor getPushSettings];
            
            
            handler(true, nil);
        }
        else
        {
            handler(false, nil);
        }
        
    }];
}

#pragma mark - Private
- (NSArray <NITCatalogItemViewModel *> *)bouquetModelsFromEntities:(NSArray <SWGBouquet *> *)entities
{
    return [entities bk_map:^id(SWGBouquet * obj) {
        return [[NITCatalogItemViewModel alloc] initWithBouquet:obj];
    }];
}

- (NSArray <NITAddressModel *> *)salonModelsFromEntities:(NSArray <SWGSalon *> *)entities
{
    return [entities bk_map:^id(SWGSalon * obj) {
        return [[NITAddressModel alloc] initWidthSalon:obj];
    }];
}

- (NSArray <SWGUserEvent *> *)userEventsFromEntities:(NSArray <SWGEvent *> *)entities
{
    return [entities bk_map:^id(SWGEvent * obj) {
        
        SWGUserEvent * event = [SWGUserEvent new];
        
        event._id = obj._id;
        event.name = obj.name;
        event.date = obj.date;
        event.text = obj.text;
        event.periodId = @([obj.periodId integerValue]);
        event.remindId = @([obj.remindId integerValue]);
        event.typeId   = obj.typeId;
        event.photo    = obj.photo;
        
        return event;
    }];
}

@end
