//
//  NITLoginPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITLoginPresenter.h"
#import "NITLoginWireframe.h"
#import "NITAuthorizationProtocols.h"
#import "NITAuthorizationWireFrame.h"

@implementation NITLoginPresenter

- (void)initData
{
    [self.view reloadState:LoginStateNormal];
}

- (void)loginWithName:(NSString *)name password:(NSString *)password
{
    if (name.length > 0 && password.length > 0)
    {
        NSString * phone;
        NSString * email;
        
        if ([self isPhoneLogin:name])
        {
            phone = name;
        }
        else if ([self isEmailLogin:name])
        {
            email = name;
        }
        
        if (phone == nil && email == nil)
        {
            [self.view reloadState:LoginStateError];
        }
        else
        {
            weaken(self);
            [self.interactor loginByEmail:email phone:phone password:password withHandler:^(BOOL success, NSError *error) {

                if (success)
                {
                    [TRACKER trackEvent:TrackerEventLogin];
                    [weakSelf closeAction];
                }
                else
                {
                    [weakSelf showLoginError:error.interfaceDescription];
                    [weakSelf.view reloadState:LoginStateError];
                }
            }];
        }
    }
    else
    {
        [self.view reloadState:LoginStateError];
    }
}

- (void)restorePassword
{
    [self.wireFrame showRestorePasswordFrom:self.view];
}

- (void)fbLogin
{
    weaken(self);
    [SOCIAL setViewController:(UIViewController *)self.view];
    [SOCIAL fbLoginWithHandler:^(BOOL success, id token, NSError *error) {
        
        if (success)
        {
            [weakSelf.authorizationInteractor authorizeWithServiceName:fbService serviceToken:token email:nil withHandler:^(BOOL success, NSString *error) {
                if (success)
                {
                    [weakSelf closeAction];
                }
                else
                {
                    [weakSelf showLoginError:error];
                }
            }];
        }
        else if (error)
        {
            [weakSelf showLoginError:error.localizedDescription];
        }
    }];
}

- (void)vkLogin
{
    weaken(self);
    [SOCIAL setViewController:(UIViewController *)self.view];
    [SOCIAL vkLoginWithHandler:^(BOOL success, NSDictionary *data, NSError *error) {
        
        if (success)
        {
            [weakSelf.authorizationInteractor authorizeWithServiceName:vkService serviceToken:data[@"token"] email:data[@"email"] withHandler:^(BOOL success, NSString *error) {
                if (success)
                {
                    [weakSelf closeAction];
                }
                else
                {
                    [weakSelf showLoginError:error];
                }
            }];
        }
        else if (error)
        {
            [weakSelf showLoginError:error.localizedDescription];
        }
    }];
}

- (void)okLogin
{
    weaken(self);
    [SOCIAL setViewController:(UIViewController *)self.view];
    [SOCIAL okLoginWithHandler:^(BOOL success, id token, NSError *error) {
        
        if (success)
        {
            [weakSelf.authorizationInteractor authorizeWithServiceName:okService serviceToken:token email:nil withHandler:^(BOOL success, NSString *error) {
                if (success)
                {
                    [weakSelf closeAction];
                }
                else
                {
                    [weakSelf showLoginError:error];
                }
            }];
        }
        else if (error)
        {
            [weakSelf showLoginError:error.localizedDescription];
        }
    }];
}

- (void)dismissAction
{
    [self closeAction];
}

#pragma mark - Private
- (BOOL)isPhoneLogin:(NSString *)login
{
    NSCharacterSet * alphanumericSet = [NSCharacterSet characterSetWithCharactersInString:@"+()1234567890"];
    NSString * result = [login stringByTrimmingCharactersInSet:alphanumericSet];
    
    return [result isEqualToString:@""];
}

- (BOOL)isEmailLogin:(NSString *)login
{
    NSString * emailid = login;
    NSString * emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate * emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailid];
}

- (void)closeAction
{
    switch (self.presentMode) {
        case LoginPresentModePush: {
            [self.wireFrame backActionFrom:self.view];
        } break;
        case LoginPresentModeModal: {
            [self.wireFrame dismissView:self.view];
        } break;
    }
}

- (void)showLoginError:(NSString *)error
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ok", nil) actionBlock:^{
        [weakSelf.view reloadState:LoginStateNormal];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:error actionButtons:@[] cancelButton:cancel];
}

- (NITAuthorizationInteractor *)authorizationInteractor
{
    NITAuthorizationInteractor * interactor = [NITAuthorizationInteractor new];
    interactor.APIDataManager = [NITAuthorizationAPIDataManager new];

    return interactor;
}

@end
