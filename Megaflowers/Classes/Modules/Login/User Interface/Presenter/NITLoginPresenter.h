//
//  NITLoginPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITLoginProtocols.h"

@class NITLoginWireFrame;

@interface NITLoginPresenter : NITRootPresenter <NITLoginPresenterProtocol, NITLoginInteractorOutputProtocol>

@property (nonatomic, weak) id <NITLoginViewProtocol> view;
@property (nonatomic, strong) id <NITLoginInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITLoginWireFrameProtocol> wireFrame;
@property (nonatomic) LoginPresentMode presentMode;

@end
