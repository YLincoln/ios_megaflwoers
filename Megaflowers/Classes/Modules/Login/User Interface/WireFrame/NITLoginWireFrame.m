//
//  NITLoginWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITLoginWireFrame.h"
#import "NITPasswordRecoveryWireFrame.h"

@implementation NITLoginWireFrame

+ (id)createNITLoginModule
{
    // Generating module components
    id <NITLoginPresenterProtocol, NITLoginInteractorOutputProtocol> presenter = [NITLoginPresenter new];
    id <NITLoginInteractorInputProtocol> interactor = [NITLoginInteractor new];
    id <NITLoginAPIDataManagerInputProtocol> APIDataManager = [NITLoginAPIDataManager new];
    id <NITLoginLocalDataManagerInputProtocol> localDataManager = [NITLoginLocalDataManager new];
    id <NITLoginWireFrameProtocol> wireFrame= [NITLoginWireFrame new];
    id <NITLoginViewProtocol> view = [(NITLoginWireFrame *)wireFrame createViewControllerWithKey:_s(NITLoginViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITLoginModuleFrom:(UIViewController *)fromViewController
{
    NITLoginViewController * view = [NITLoginWireFrame createNITLoginModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentModalNITLoginModule:(UIViewController *)fromViewController withHandler:(void(^)())handler
{
    NITLoginViewController * view = [NITLoginWireFrame createNITLoginModule];
    view.presenter.presentMode = LoginPresentModeModal;
    view.presenter.wireFrame.handler = handler;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:view];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

+ (void)presentNITLoginModule
{
    NITLoginViewController * view = [NITLoginWireFrame createNITLoginModule];
    [WINDOW.visibleController.navigationController pushViewController:view animated:true];
}

- (void)showRestorePasswordFrom:(UIViewController *)fromViewController
{
    [NITPasswordRecoveryWireFrame presentNITPasswordRecoveryModuleFrom:fromViewController phone:nil];
}

- (void)backActionFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popToRootViewControllerAnimated:true];
}

- (void)dismissView:(UIViewController *)fromViewController
{
    [fromViewController dismissViewControllerAnimated:true completion:^{
        if (self.handler) self.handler();
    }];
}

@end
