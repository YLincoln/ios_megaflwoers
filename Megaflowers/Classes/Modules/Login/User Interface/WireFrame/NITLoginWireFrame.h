//
//  NITLoginWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITLoginProtocols.h"
#import "NITLoginViewController.h"
#import "NITLoginLocalDataManager.h"
#import "NITLoginAPIDataManager.h"
#import "NITLoginInteractor.h"
#import "NITLoginPresenter.h"
#import "NITLoginWireframe.h"
#import "NITRootWireframe.h"

@interface NITLoginWireFrame : NITRootWireframe <NITLoginWireFrameProtocol>

@property (nonatomic) LoginHandler handler;

@end
