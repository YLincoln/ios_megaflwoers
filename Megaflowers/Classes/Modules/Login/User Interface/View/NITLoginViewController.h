//
//  NITLoginViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITLoginProtocols.h"

@interface NITLoginViewController : NITBaseViewController <NITLoginViewProtocol>

@property (nonatomic, strong) id <NITLoginPresenterProtocol> presenter;

@end
