//
//  NITLoginViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITLoginViewController.h"
#import "UITextField+AKNumericFormatter.h"

@interface NITLoginViewController ()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UITextField * loginField;
@property (nonatomic) IBOutlet UITextField * passwordField;
@property (nonatomic) IBOutlet UIButton * loginBtn;

@end

@implementation NITLoginViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:true];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.tabBarController.tabBar setHidden:false];
}

#pragma mark - IBAction
- (IBAction)restorePasswordAction:(id)sender
{
    [self.presenter restorePassword];
}

- (IBAction)loginAction:(UIButton *)sender
{
    sender.enabled = false;
    [self.presenter loginWithName:self.loginField.text password:self.passwordField.text];
}

- (IBAction)fbTapped:(id)sender
{
    [self.presenter fbLogin];
}

- (IBAction)vkTapped:(id)sender
{
    [self.presenter vkLogin];
}

- (IBAction)okTapped:(id)sender
{
    [self.presenter okLogin];
}

- (IBAction)dismissAction:(id)sender
{
    [self.presenter dismissAction];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Аккаунт", nil);
    
    [self setKeyboardActiv:true];
    
    self.loginField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.loginField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.passwordField.placeholder
                                                                               attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    switch (self.presenter.presentMode) {
        case LoginPresentModeModal: {
            [self.navigationItem setHidesBackButton:true];
            UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Отмена", nil)
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(dismissAction:)];
            item.tintColor = RGB(10, 88, 43);
            self.navigationItem.leftBarButtonItem = item;
        } break;
            
        case LoginPresentModePush:
            [self.navigationItem setHidesBackButton:false];
            break;
    }
    
    #if DEBUG
    self.loginField.text = @"api@mega.ru";
    self.passwordField.text = @"megapassword";
    #endif
}

#pragma mark - NITLoginPresenterProtocol
- (void)reloadState:(LoginState)state
{
    self.loginBtn.enabled = true;
    
    switch (state) {
        case LoginStateNormal: {
            self.loginField.textColor = RGB(10, 88, 43);
            self.passwordField.textColor = RGB(10, 88, 43);
        } break;
            
        case LoginStateError: {
            self.loginField.textColor = RGB(217, 67, 67);
            self.passwordField.textColor = RGB(217, 67, 67);
        } break;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder * nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder)
    {
        [nextResponder becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return false;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.loginField.textColor = RGB(10, 88, 43);
    self.passwordField.textColor = RGB(10, 88, 43);
    
    return true;
}

@end
