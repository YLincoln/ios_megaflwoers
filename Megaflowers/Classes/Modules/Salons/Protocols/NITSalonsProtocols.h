//
//  NITSalonsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAddressModel.h"
#import "NITAddressSectionModel.h"
#import "SWGSalon.h"
#import "NITCityModel.h"
#import "NITCityCellModel.h"
#import "NITSalonMarker.h"
#import "HSClusterMapView.h"

@protocol NITSalonsInteractorOutputProtocol;
@protocol NITSalonsInteractorInputProtocol;
@protocol NITSalonsViewProtocol;
@protocol NITSalonsPresenterProtocol;
@protocol NITSalonsLocalDataManagerInputProtocol;
@protocol NITSalonsAPIDataManagerInputProtocol;

@class NITSalonsWireFrame;

typedef CF_ENUM (NSUInteger, SalonsScreenMode) {
    SalonsScreenModeView        = 0,
    SalonsScreenModeSelect      = 1,
    SalonsScreenModeModalMap    = 2
};

typedef CF_ENUM (NSUInteger, SalonsSectionType) {
    SalonsSectionTypeAddress    = 0,
    SalonsSectionTypeMap        = 1
};

typedef CF_ENUM (NSUInteger, SalonsSortType) {
    SalonsSortTypeAll     = 0,
    SalonsSortTypeWorkNow = 1
};

@protocol NITSalonsDelegate <NSObject>
- (void)didSelectAddress:(NITAddressModel *)address;
@end

@protocol NITSalonsViewProtocol
@required
@property (nonatomic, strong) id <NITSalonsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadView;
- (void)reloadSectionByModels:(NSArray <NITAddressSectionModel *> *)models;
- (void)reloadMapByMarkers:(NSArray<NITSalonMarker *> *)markers;
- (void)reloadCityTitle:(NSString *)title;
- (void)reloadSortTitle:(NSString *)title;
- (void)didShowMarker:(NITSalonMarker *)marker;
@end

@protocol NITSalonsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITSalonsModule;
+ (void)presentNITSalonsModuleFrom:(id)fromView;
+ (void)presentNITSalonsPreviewModuleFrom:(id)fromView model:(NITAddressModel *)model;
+ (void)selectAddressFrom:(id)fromView withDelegate:(id)delegate;
- (void)selectCityFrom:(id)fromView currentCityId:(NSNumber *)cityId withHandler:(void(^)(NITCityCellModel * model))handler;
- (void)showUserRegistrationFrom:(id)fromView;
- (void)dismissView:(id)view;
@end

@protocol NITSalonsPresenterProtocol
@required
@property (nonatomic, weak) id <NITSalonsViewProtocol> view;
@property (nonatomic, strong) id <NITSalonsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITSalonsWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITSalonsDelegate> delegate;
@property (nonatomic) SalonsScreenMode viewMode;
@property (nonatomic) SalonsSectionType sectionType;
@property (nonatomic) NITAddressModel * favoriteSalon;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initDataWithMap:(HSClusterMapView *)map;
- (void)updateData;
- (void)checkForceTouchAction;
- (void)searchSalonsByText:(NSString *)text;
- (void)callToSalonByPhone:(NSString *)phone;
- (void)changeFavoriteStateForSalon:(NITAddressModel *)model;
- (void)changeCity;
- (void)sortSalons;
- (void)selectSalonAddressByModel:(NITAddressModel *)model;
- (void)createRouteByModel:(NITAddressModel *)model;
- (void)dismissView;
- (void)selectSalonModel:(NITAddressModel *)model zoomed:(BOOL)zoomed;
@end

@protocol NITSalonsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateSalonsModels:(NSArray <NITAddressSectionModel *> *)models;
@end

@protocol NITSalonsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITSalonsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITSalonsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITSalonsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getSalonsBySortType:(SalonsSortType)sortType cityId:(NSNumber *)cityId;
- (void)sortSalonsByType:(SalonsSortType)sortType;
- (void)searchSalonsByText:(NSString *)text;
- (void)changeFavoriteStateForSalon:(NITAddressModel *)model;
@end


@protocol NITSalonsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITSalonsAPIDataManagerInputProtocol <NITSalonsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserSalonsByCityId:(NSNumber *)cityId withHandler:(void(^)(NSArray<SWGSalon> *result))handler;
- (void)changeFavoriteState:(BOOL)state forSalonId:(NSNumber *)salonId;
@end

@protocol NITSalonsLocalDataManagerInputProtocol <NITSalonsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)changeFavoriteStateForSalon:(NITAddressModel *)salon;
@end
