//
//  NITSalonsLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsLocalDataManager.h"

@implementation NITSalonsLocalDataManager

- (void)changeFavoriteStateForSalon:(NITAddressModel *)salon
{
    if (salon.isFavorite)
    {
        [NITFavoriteSalon addSalons:@[salon]];
        [TRACKER trackEvent:TrackerEventLikeSalon parameters:@{@"id":salon.identifier}];
    }
    else
    {
        [NITFavoriteSalon deleteSalonByID:salon.identifier];
    }
}

@end
