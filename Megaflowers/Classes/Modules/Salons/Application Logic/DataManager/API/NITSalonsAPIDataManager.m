//
//  NITSalonsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsAPIDataManager.h"
#import "SWGSalonApi.h"
#import "SWGUserApi.h"

@implementation NITSalonsAPIDataManager

- (void)getUserSalonsByCityId:(NSNumber *)cityId withHandler:(void (^)(NSArray<SWGSalon> *))handler
{
    [HELPER startLoading];
    [[SWGSalonApi new] salonGetWithLang:HELPER.lang site:API.site city:cityId completionHandler:^(NSArray<SWGSalon> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

- (void)changeFavoriteState:(BOOL)state forSalonId:(NSNumber *)salonId
{
    if (USER.isAutorize)
    {
        if (state)
        {
            [HELPER startLoading];
            [[SWGUserApi new] userSalonPostWithSite:API.site salonId:salonId completionHandler:^(NSError *error) {
                [HELPER stopLoading];
                if (error) [HELPER logError:error method:METHOD_NAME];
            }];
        }
        else
        {
            [HELPER startLoading];
            [[SWGUserApi new] userSalonIdDeleteWithId:salonId site:API.site completionHandler:^(NSError *error) {
                [HELPER stopLoading];
                if (error) [HELPER logError:error method:METHOD_NAME];
            }];
        }
    }
}

@end
