//
//  NITAddressSectionModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAddressSectionModel.h"
#import "NITAddressCellSection.h"

@implementation NITAddressSectionModel

- (CGFloat)height
{
    return 30;
}

- (NITAddressCellSection *)headerView
{
    return [NITAddressCellSection viewWidthTitle:self.name];
}

@end
