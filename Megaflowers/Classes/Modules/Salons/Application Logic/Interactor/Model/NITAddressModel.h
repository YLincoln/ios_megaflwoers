//
//  NITAddressModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalon.h"

@class SWGSalon;

@interface NITAddressModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * address;
@property (nonatomic) NSString * schedule;
@property (nonatomic) NSString * phone;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic) CLLocation * location;
@property (nonatomic) CGFloat distance;
@property (nonatomic) BOOL selected;

- (instancetype)initWidthSalon:(SWGSalon *)salon;
- (instancetype)initWidthFavoriteSalon:(NITFavoriteSalon *)salon;
+ (NSArray <NITAddressModel *> *)salonModelsFromEntities:(NSArray <SWGSalon *> *)entities;

@end
