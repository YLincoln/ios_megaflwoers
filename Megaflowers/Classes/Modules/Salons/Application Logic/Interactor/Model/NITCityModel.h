//
//  NITCityModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/3/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITStorageManager.h"

@interface NITCityModel : NITStorageManager

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * phone;

- (void)clean;

@end
