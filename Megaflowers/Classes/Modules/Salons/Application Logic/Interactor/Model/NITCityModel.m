//
//  NITCityModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/3/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCityModel.h"

static NSString * const kSalonCityId    = @"kSalonCityId";
static NSString * const kSalonCityName  = @"kSalonCityName";
static NSString * const kSalonCityPhone = @"kSalonCityPhone";

@implementation NITCityModel

- (instancetype)init
{
    if (self = [super init])
    {
        if (self.identifier.integerValue == 0)
        {
            self.identifier = USER.cityID;
            self.name = USER.cityName;
            self.phone = USER.cityPhone;
        }
    }
    
    return self;
}

- (void)clean
{
    self.identifier = nil;
    self.name = nil;
    self.phone = nil;
}

#pragma mark - Custom accessors
- (NSNumber *)identifier
{
    return @([self Integer:kSalonCityId]);
}

- (void)setIdentifier:(NSNumber *)identifier
{
    [self setInteger:kSalonCityId value:identifier.integerValue];
}

- (NSString *)name
{
    return [self CriptoString:kSalonCityName];
}

- (void)setName:(NSString *)name
{
    [self setCriptoString:kSalonCityName value:name];
}

- (NSString *)phone
{
    return [self CriptoString:kSalonCityPhone];
}

- (void)setPhone:(NSString *)phone
{
    [self setCriptoString:kSalonCityPhone value:phone];
}

@end
