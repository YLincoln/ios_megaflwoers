//
//  NITSalonMarker.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/26/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonMarker.h"
#import "NITAddressModel.h"

@implementation NITSalonMarker

- (instancetype)initWithModel:(NITAddressModel *)model
{
    if (self = [super init])
    {
        self.model = model;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (void)setModel:(NITAddressModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.position = self.model.location.coordinate;
    self.userData = self.model;
    self.icon     = [UIImage imageNamed:@"metka"];
}

@end
