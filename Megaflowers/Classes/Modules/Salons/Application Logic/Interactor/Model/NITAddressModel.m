//
//  NITAddressModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAddressModel.h"
#import "SWGSalon.h"

@implementation NITAddressModel

- (instancetype)initWidthSalon:(SWGSalon *)salon
{
    if (self = [super init])
    {
        self.identifier = salon._id;
        self.address = [NSString stringWithFormat:@"%@ %@, %@ %@", salon.streetType, salon.street, salon.buildingType, salon.building];
        self.schedule = salon.scheduleWeekday;
        self.isFavorite = [NITFavoriteSalon isFavoriteSalonID:self.identifier];
        
        if (salon.phones.count > 0)
        {
            SWGSalonPhones * phone = salon.phones.firstObject;
            self.phone = phone.value;
        }
        else
        {
            self.phone = USER.cityPhone;
        }
        
        CLLocation * location;
        if (salon.lat.length > 0 && salon.lon.length > 0)
        {
            location = [[CLLocation alloc] initWithLatitude:[salon.lat doubleValue] longitude:[salon.lon doubleValue]];
        }
        
        self.location = location;
        
    }
    
    return self;
}

- (instancetype)initWidthFavoriteSalon:(NITFavoriteSalon *)salon
{
    if (self = [super init])
    {
        self.identifier = @([salon.identifier integerValue]);
        self.address = salon.address;
        self.schedule = salon.schedule;
        self.phone = salon.phone;
        self.location = salon.location;
        self.isFavorite = [NITFavoriteSalon isFavoriteSalonID:self.identifier];;
    }
    
    return self;
}

+ (NSArray <NITAddressModel *> *)salonModelsFromEntities:(NSArray <SWGSalon *> *)entities
{
    return [entities bk_map:^id(SWGSalon * obj) {
        return [[NITAddressModel alloc] initWidthSalon:obj];
    }];
}

- (void)setIsFavorite:(BOOL)isFavorite
{
    if ([USER isAutorize])
    {
        _isFavorite = isFavorite;
    }
}

- (CGFloat)distance
{
    CLLocation * currentLocation = [LOCATION_MANAGER currentLocation];
    
    if (currentLocation && self.location)
    {
        CLLocationDistance meters = [currentLocation distanceFromLocation:self.location];
        return meters;
    }
    else
    {
        return 0;
    }
}

@end
