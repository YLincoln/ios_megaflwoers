//
//  NITSalonMarker.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/26/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class NITAddressModel;

@interface NITSalonMarker : GMSMarker

@property (nonatomic) NITAddressModel * model;

- (instancetype)initWithModel:(NITAddressModel *)model;

@end
