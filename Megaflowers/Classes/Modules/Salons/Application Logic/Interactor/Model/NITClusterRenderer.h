//
//  NITClusterRenderer.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/28/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "HSClusterRenderer.h"

@interface NITClusterRenderer : NSObject <HSClusterRenderer>

@end
