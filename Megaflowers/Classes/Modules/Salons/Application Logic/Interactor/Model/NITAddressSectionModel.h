//
//  NITAddressSectionModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITAddressModel, NITAddressCellSection;

@interface NITAddressSectionModel : NSObject

@property (nonatomic) NSString * name;
@property (nonatomic) NSArray <NITAddressModel *> * items;
@property (nonatomic) CGFloat height;
@property (nonatomic) NITAddressCellSection * headerView;

@end
