//
//  NITSalonsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITSalonsProtocols.h"

@interface NITSalonsInteractor : NSObject <NITSalonsInteractorInputProtocol>

@property (nonatomic, weak) id <NITSalonsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITSalonsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITSalonsLocalDataManagerInputProtocol> localDataManager;

@end
