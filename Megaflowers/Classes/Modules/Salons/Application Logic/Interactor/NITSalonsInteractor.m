//
//  NITSalonsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsInteractor.h"
#import "NSArray+BlocksKit.h"

@interface NITSalonsInteractor ()

@property (nonatomic) NSArray <NITAddressModel *> * data;
@property (nonatomic) NSArray <NITAddressSectionModel *> * dataSection;
@property (nonatomic) SalonsSortType sortType;

@end

@implementation NITSalonsInteractor

- (void)getSalonsBySortType:(SalonsSortType)sortType cityId:(NSNumber *)cityId
{
    self.sortType = sortType;
    
    weaken(self);
    [self.APIDataManager getUserSalonsByCityId:cityId withHandler:^(NSArray<SWGSalon> *result) {
        weakSelf.data = [weakSelf modelsFromEntities:result];

        if (weakSelf.data) [weakSelf.presenter updateSalonsModels:[weakSelf createSectionsByModels:weakSelf.data]];
        else if (weakSelf.dataSection)
        {
            NSMutableArray <NITAddressModel *> * arr = [[NSMutableArray alloc] init];
            for (NITAddressSectionModel * citySection in weakSelf.dataSection)
            {
                for(NITAddressModel * city in citySection.items)
                {
                    [arr addObject:city];
                }
            }
            weakSelf.data = [arr mutableCopy];
            [weakSelf.presenter updateSalonsModels:[weakSelf createSectionsByModels:weakSelf.data]];
        }
    }];
    
}

- (void)searchSalonsByText:(NSString *)text
{
    if (text && text.length > 0)
    {
        NSPredicate * resultPredicate = [NSPredicate predicateWithFormat:@"(address contains[cd] %@)", text];
        NSArray * items =  [self.data filteredArrayUsingPredicate:resultPredicate];
        
        [self.presenter updateSalonsModels:[self createSectionsByModels:items]];
    }
    else
    {
        [self.presenter updateSalonsModels:[self createSectionsByModels:self.data]];
    }
}

- (void)changeFavoriteStateForSalon:(NITAddressModel *)model
{
    [self.localDataManager changeFavoriteStateForSalon:model];
    [self.APIDataManager changeFavoriteState:model.isFavorite forSalonId:model.identifier];
}

- (void)sortSalonsByType:(SalonsSortType)sortType
{
    [self.presenter updateSalonsModels:[self createSectionsByModels:self.data]];
}

#pragma mark - Private
- (NSArray <NITAddressModel *> *)modelsFromEntities:(NSArray <SWGSalon *> *)entities
{
    return [entities bk_map:^id(SWGSalon * obj) {
        return [[NITAddressModel alloc] initWidthSalon:obj];
    }];
}

- (NSArray <NITAddressSectionModel *> *)createSectionsByModels:(NSArray <NITAddressModel *> *)models
{
    NSMutableArray * sections = [NSMutableArray new];
    NSArray * salons = [models copy];
    
    if (self.sortType == SalonsSortTypeWorkNow)
    {
        // filter salons by time
    }
    
    // 10 km
    NSArray * arr1 = [salons bk_select:^BOOL(NITAddressModel * obj) {
        return obj.distance <= 10000;
    }];
    
    if (arr1.count > 0)
    {
        NITAddressSectionModel * sec1 = [NITAddressSectionModel new];
        sec1.name = NSLocalizedString(@"Не дальше 10 км", nil);
        sec1.items = arr1;
        [sections addObject:sec1];
    }
    
    // 15 km
    NSArray * arr2 = [salons bk_select:^BOOL(NITAddressModel * obj) {
        return obj.distance > 10000 && obj.distance <= 15000;
    }];
    
    if (arr2.count > 0)
    {
        NITAddressSectionModel * sec2 = [NITAddressSectionModel new];
        sec2.name = NSLocalizedString(@"Не дальше 15 км", nil);
        sec2.items = arr2;
        [sections addObject:sec2];
    }
    
    // other
    NSArray * arr3 = [salons bk_select:^BOOL(NITAddressModel * obj) {
        return obj.distance > 15000;
    }];
    
    if (arr3.count > 0)
    {
        NITAddressSectionModel * sec3 = [NITAddressSectionModel new];
        sec3.name = NSLocalizedString(@"Дальше 15 км", nil);
        sec3.items = arr3;
        [sections addObject:sec3];
    }
    
    return sections;
}

@end
