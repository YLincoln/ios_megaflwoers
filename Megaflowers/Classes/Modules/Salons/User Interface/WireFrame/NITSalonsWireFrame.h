//
//  NITSalonsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITSalonsProtocols.h"
#import "NITSalonsViewController.h"
#import "NITSalonsLocalDataManager.h"
#import "NITSalonsAPIDataManager.h"
#import "NITSalonsInteractor.h"
#import "NITSalonsPresenter.h"
#import "NITSalonsWireframe.h"
#import "NITRootWireframe.h"

@interface NITSalonsWireFrame : NITRootWireframe <NITSalonsWireFrameProtocol>

+ (id)createNITSalonsModule;

@end
