//
//  NITSalonsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsWireFrame.h"
#import "NITCitiesWireFrame.h"
#import "NITAuthorizationWireFrame.h"

@implementation NITSalonsWireFrame

+ (id)createNITSalonsModule
{
    // Generating module components
    id <NITSalonsPresenterProtocol, NITSalonsInteractorOutputProtocol> presenter = [NITSalonsPresenter new];
    id <NITSalonsInteractorInputProtocol> interactor = [NITSalonsInteractor new];
    id <NITSalonsAPIDataManagerInputProtocol> APIDataManager = [NITSalonsAPIDataManager new];
    id <NITSalonsLocalDataManagerInputProtocol> localDataManager = [NITSalonsLocalDataManager new];
    id <NITSalonsWireFrameProtocol> wireFrame= [NITSalonsWireFrame new];
    id <NITSalonsViewProtocol> view = [(NITSalonsWireFrame *)wireFrame createViewControllerWithKey:_s(NITSalonsViewController) storyboardType:StoryboardTypeSalons];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITSalonsModuleFrom:(UIViewController *)fromViewController
{
    NITSalonsViewController * view = [NITSalonsWireFrame createNITSalonsModule];
    view.presenter.viewMode = SalonsScreenModeView;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)selectAddressFrom:(UIViewController *)fromViewController withDelegate:(id)delegate
{
    NITSalonsViewController * view = [NITSalonsWireFrame createNITSalonsModule];
    view.presenter.viewMode = SalonsScreenModeSelect;
    view.presenter.delegate = delegate;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITSalonsPreviewModuleFrom:(UIViewController *)fromViewController model:(NITAddressModel *)model
{
    NITSalonsViewController * view = [NITSalonsWireFrame createNITSalonsModule];
    view.presenter.viewMode = SalonsScreenModeModalMap;
    view.presenter.favoriteSalon = model;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:view];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

- (void)selectCityFrom:(UIViewController *)fromViewController currentCityId:(NSNumber *)cityId withHandler:(void (^)(NITCityCellModel *))handler
{
    [NITCitiesWireFrame presentNITCitiesModuleFrom:fromViewController currentCityId:cityId type:CityChangeTypeLocal andHandler:handler];
}

- (void)dismissView:(UIViewController *)viewController
{
    [viewController dismissViewControllerAnimated:true completion:^{}];
}

- (void)showUserRegistrationFrom:(UIViewController *)fromViewController
{
    [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:fromViewController];
}


@end
