//
//  NITAddressCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAddressCell.h"
#import "NITAddressModel.h"

@interface NITAddressCell ()

@property (nonatomic) IBOutlet UILabel * address;
@property (nonatomic) IBOutlet UILabel * schedule;
@property (nonatomic) IBOutlet UILabel * phone;
@property (nonatomic) IBOutlet UIButton * likeBtn;
@property (nonatomic) IBOutlet UIButton * selectBtn;
@property (nonatomic) IBOutlet UIButton * centerSelectBtn;

@end

@implementation NITAddressCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITAddressModel *)model
{
    _model = model;
    [self updateData];
}

- (void)setViewMode:(SalonsScreenMode)viewMode
{
    _viewMode = viewMode;
    [self updateSelectBtn];
    [self updateFavoriteState];
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    [self updateSelectBtn];
}

#pragma mark - IBAction
- (IBAction)likeAction:(id)sender
{
    self.model.isFavorite = !self.model.isFavorite;
    [self updateFavoriteState];
    
    if ([self.delegate respondsToSelector:@selector(didChangeFavoriteStateForSalon:)])
    {
        [self.delegate didChangeFavoriteStateForSalon:self.model];
    }
}

- (IBAction)detailAction:(id)sender
{
    if (self.viewMode == SalonsScreenModeView)
    {
        if ([self.delegate respondsToSelector:@selector(didShowAddress:)])
        {
            [self.delegate didShowAddress:self.model];
        }
    }
}

- (IBAction)callAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didCallToPhone:)])
    {
        [self.delegate didCallToPhone:self.model.phone];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.address.text = self.model.address;
    self.schedule.text = self.model.schedule;
    
    [self updatePhone];
    [self updateFavoriteState];
}

- (void)updateFavoriteState
{
    NSString * imgaeName = self.model.isFavorite ? @"pink_heart" : @"green_heart";
    [self.likeBtn setImage:[UIImage imageNamed:imgaeName] forState:UIControlStateNormal];
    self.likeBtn.hidden = self.viewMode == SalonsScreenModeSelect;
}

- (void)updateSelectBtn
{
    self.selectBtn.hidden = self.viewMode != SalonsScreenModeView;
    self.centerSelectBtn.hidden = self.viewMode != SalonsScreenModeSelect;
    
    /*if (self.viewMode == SalonsScreenModeSelect)
    {
        [self.selectBtn setImage:self.selected ? [UIImage imageNamed:@"ic_check"] : nil forState:UIControlStateNormal];
    }*/
}

- (void)updatePhone
{
    NSMutableAttributedString * attrPhone = [[NSMutableAttributedString alloc] initWithString:self.model.phone];
    
    [attrPhone addAttribute:NSFontAttributeName
                      value:[UIFont systemFontOfSize:14 weight:UIFontWeightRegular]
                      range:NSMakeRange(0, attrPhone.length)];
    
    [attrPhone addAttribute:NSUnderlineStyleAttributeName
                      value:@(1)
                      range:NSMakeRange(0, attrPhone.length)];
    
    [attrPhone addAttribute:NSForegroundColorAttributeName
                      value:RGB(10, 88, 43)
                      range:NSMakeRange(0, attrPhone.length)];
    
    self.phone.attributedText = attrPhone;
}

@end
