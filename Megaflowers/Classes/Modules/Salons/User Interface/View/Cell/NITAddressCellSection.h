//
//  NITAddressCellSection.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITAddressCellSection : UIView

+ (instancetype)viewWidthTitle:(NSString *)title;

@end
