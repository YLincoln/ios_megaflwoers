//
//  NITAddressCellSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAddressCellSection.h"

@interface NITAddressCellSection ()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITAddressCellSection

+ (instancetype)viewWidthTitle:(NSString *)title
{
    NITAddressCellSection * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITAddressCellSection) owner:self options:nil] firstObject];
    view.title.text = title;
    view.frame = CGRectMake(0, 0, ViewWidth(WINDOW), ViewHeight(view));
    
    return view;
}

@end
