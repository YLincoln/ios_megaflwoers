//
//  NITAddressCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsPresenter.h"
@class NITAddressModel;

@protocol NITAddressCellDelegate <NSObject>

- (void)didCallToPhone:(NSString *)phone;
- (void)didShowAddress:(NITAddressModel *)model;
- (void)didChangeFavoriteStateForSalon:(NITAddressModel *)model;

@end

@interface NITAddressCell : UITableViewCell

@property (nonatomic) NITAddressModel * model;
@property (nonatomic) SalonsScreenMode viewMode;
@property (nonatomic) BOOL isSelected;
@property (nonatomic, weak) id <NITAddressCellDelegate> delegate;

@end
