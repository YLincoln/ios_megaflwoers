//
//  NITSalonsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITGMSMapViewController.h"
#import "NITSalonsProtocols.h"

@interface NITSalonsViewController : NITGMSMapViewController <NITSalonsViewProtocol>

@property (nonatomic, strong) id <NITSalonsPresenterProtocol> presenter;

@end
