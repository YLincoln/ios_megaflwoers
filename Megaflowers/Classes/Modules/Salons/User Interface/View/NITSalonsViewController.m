//
//  NITSalonsViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsViewController.h"
#import "NITAddressCellSection.h"
#import "NITAddressCell.h"
#import "NITMarkerInfoView.h"
#import <CoreLocation/CoreLocation.h>

@interface NITSalonsViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
TOSearchBarDelegate,
NITAddressCellDelegate,
NITMarkerInfoViewDelegate
>

@property (nonatomic) IBOutlet UISegmentedControl * segmentControl;
@property (nonatomic) IBOutlet UIView * mapSection;
@property (nonatomic) IBOutlet UIView * tableSection;
@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UILabel * cityTitle;
@property (nonatomic) IBOutlet UILabel * sortTitle;
@property (nonatomic) IBOutlet NSLayoutConstraint * sortTitleW;
@property (nonatomic) IBOutlet NSLayoutConstraint * segmentViewH;

@property (nonatomic) NSArray <NITAddressSectionModel *> * sections;

@end

@implementation NITSalonsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initDataWithMap:self.mapView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.presenter checkForceTouchAction];
}

#pragma mark - IBAction
- (IBAction)changeViewSection:(id)sender
{
    self.presenter.sectionType = self.segmentControl.selectedSegmentIndex;
    [self reloadView];
}

- (IBAction)searchSalonsAction:(id)sender
{
    [self showSearchBarWithAnimations:^{}];
}

- (IBAction)changeCityAction:(id)sender
{
    [self.presenter changeCity];
}

- (IBAction)selectSortAction:(id)sender
{
    [self.presenter sortSalons];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Салоны", nil);
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNibArray:@[_s(NITAddressCell)]];
    [self setKeyboardActiv:true];
    [self reloadView];
    
    switch (self.presenter.viewMode) {
            
        case SalonsScreenModeView: {
            self.navigationItem.leftBarButtonItem = nil;
        } break;
            
        case SalonsScreenModeSelect: {
            [self.tableView setTableHeaderView:nil];
        } break;
            
        case SalonsScreenModeModalMap: {
            self.navigationItem.rightBarButtonItems = nil;
            self.navigationItem.title = NSLocalizedString(@"Любимые магазины", nil);
            self.segmentControl.hidden = true;
            self.segmentViewH.constant = 0;
            
            UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Отмена", nil)
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self.presenter
                                                                     action:@selector(dismissView)];
            item.tintColor = RGB(10, 88, 43);
            self.navigationItem.leftBarButtonItem = item;
            
        } break;
    }
    
    [self initSearchBar];
    [self.searchBar setDelegate:self];
    [self.view layoutIfNeeded];
}

#pragma mark - NITSalonsPresenterProtocol
- (void)reloadView
{
    self.segmentControl.selectedSegmentIndex = self.presenter.sectionType;
    self.tableSection.hidden = self.presenter.sectionType != SalonsSectionTypeAddress;
    self.mapSection.hidden = self.presenter.sectionType != SalonsSectionTypeMap;
    
    [self.tableView reloadData];
}

- (void)reloadSectionByModels:(NSArray<NITAddressSectionModel *> *)models
{
    self.sections = models;
    [self.tableView reloadData];
}

- (void)reloadMapByMarkers:(NSArray<NITSalonMarker *> *)markers
{
    [self.mapView clear];
    [self.mapView addMarkers:markers];
}

- (void)reloadCityTitle:(NSString *)title
{
    NSMutableAttributedString * attrTitle = [[NSMutableAttributedString alloc] initWithString:title];
    
    [attrTitle addAttribute:NSFontAttributeName
                      value:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]
                      range:NSMakeRange(0, attrTitle.length)];
    
    [attrTitle addAttribute:NSUnderlineStyleAttributeName
                      value:@(1)
                      range:NSMakeRange(0, attrTitle.length)];
    
    [attrTitle addAttribute:NSForegroundColorAttributeName
                      value:RGB(10, 88, 43)
                      range:NSMakeRange(0, attrTitle.length)];
    
    self.cityTitle.attributedText = attrTitle;
}

- (void)reloadSortTitle:(NSString *)title
{
    self.sortTitle.text = title;
    self.sortTitleW.constant = [self.sortTitle sizeOfMultiLineLabel].width;
    [self.view layoutIfNeeded];
}

- (void)didShowMarker:(NITSalonMarker *)marker
{
    [self showMarkerInfoWindowForMarker:marker];
}

#pragma mark - NITGMSMapViewController
- (UIView *)mapViewMarkerInfoWindow:(GMSMarker *)marker
{
    NITMarkerInfoView * markerInfo = [NITMarkerInfoView viewWidthModel:self.currentlyTappedMarker.userData];
    markerInfo.viewMode = self.presenter.viewMode;
    markerInfo.delegate = self;
    
    return markerInfo;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NITAddressSectionModel * model = self.sections[section];
    return [model.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NITAddressSectionModel * model = self.sections[section];
    return model.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NITAddressSectionModel * model = self.sections[section];
    return [model headerView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITAddressCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITAddressCell) forIndexPath:indexPath];
    NITAddressSectionModel * section = self.sections[indexPath.section];
    
    cell.model = section.items[indexPath.row];
    cell.delegate = self;
    cell.viewMode = self.presenter.viewMode;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITAddressSectionModel * section = self.sections[indexPath.section];
    [self.presenter selectSalonModel:section.items[indexPath.row] zoomed:true];
}

#pragma mark - TOSearchBarDelegate
- (void)searchBarCancelButtonClicked:(TOSearchBar *)searchBar
{
    [searchBar setText:@""];
    [self.presenter searchSalonsByText:searchBar.text];
    [self hideSearchBarWithAnimations:^{}];
}

- (BOOL)searchBarShouldClear:(TOSearchBar *)searchBar
{
    [searchBar setText:@""];
    [self.presenter searchSalonsByText:searchBar.text];
    
    return true;
}

- (void)searchBar:(TOSearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.presenter searchSalonsByText:searchBar.text];
}
- (BOOL)searchBarShouldReturn:(TOSearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self.presenter searchSalonsByText:searchBar.text];
    
    return true;
}

#pragma mark - NITAddressCellDelegate
- (void)didShowAddress:(NITAddressModel *)model
{
    [self.presenter selectSalonModel:model zoomed:true];
}

- (void)didCallToPhone:(NSString *)phone
{
    [self.presenter callToSalonByPhone:phone];
}

- (void)didChangeFavoriteStateForSalon:(NITAddressModel *)model
{
    [self.presenter changeFavoriteStateForSalon:model];
}

#pragma mark - NITMarkerInfoViewDelegate
- (void)didCallToPhoneFromMap:(NSString *)phone
{
    [self.presenter callToSalonByPhone:phone];
}

- (void)didCreateRouteByModel:(NITAddressModel *)model
{
    [self hideMarkerInfoWindow];
    [self.presenter createRouteByModel:model];
}

- (void)didSelectModel:(NITAddressModel *)model
{
    [self.presenter selectSalonAddressByModel:model];
    [self backAction:nil];
}

@end
