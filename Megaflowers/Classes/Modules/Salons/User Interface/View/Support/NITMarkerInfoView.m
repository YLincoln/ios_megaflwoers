//
//  NITMarkerInfoView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/22/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITMarkerInfoView.h"
#import "NITAddressModel.h"

@interface NITMarkerInfoView ()

@property (nonatomic) IBOutlet UIView * mainView;
@property (nonatomic) IBOutlet UIView * triangle;
@property (nonatomic) IBOutlet UILabel * address;
@property (nonatomic) IBOutlet UILabel * schedule;
@property (nonatomic) IBOutlet UILabel * phone;
@property (nonatomic) IBOutlet UIButton * routeBtn;

@end

@implementation NITMarkerInfoView

+ (instancetype)viewWidthModel:(NITAddressModel *)model
{
    NITMarkerInfoView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITMarkerInfoView) owner:self options:nil] firstObject];
    view.model = model;
    [view initUI];
    
    return view;
}

- (void)setModel:(NITAddressModel *)model
{
    _model = model;
    if (model)
    {
       [self updateData];
    }
}

- (void)setViewMode:(SalonsScreenMode)viewMode
{
    _viewMode = viewMode;
    [self updateButton];
}

#pragma mark - IBAction
- (IBAction)createRouteAction:(id)sender
{
    switch (self.viewMode)
    {
        case SalonsScreenModeView:
        case SalonsScreenModeModalMap:
            if ([self.delegate respondsToSelector:@selector(didCreateRouteByModel:)])
            {
                [self.delegate didCreateRouteByModel:self.model];
            }
            break;
            
        case SalonsScreenModeSelect:
            if ([self.delegate respondsToSelector:@selector(didSelectModel:)])
            {
                [self.delegate didSelectModel:self.model];
            }
            break;
    }
}

- (IBAction)callAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didCallToPhoneFromMap:)])
    {
        [self.delegate didCallToPhoneFromMap:self.model.phone];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.address.text = self.model.address;
    self.schedule.text = self.model.schedule;
    
    [self updatePhone];
    [self updateButton];
}

- (void)updatePhone
{
    NSMutableAttributedString * attrPhone = [[NSMutableAttributedString alloc] initWithString:self.model.phone];
    
    [attrPhone addAttribute:NSFontAttributeName
                      value:[UIFont systemFontOfSize:14 weight:UIFontWeightRegular]
                      range:NSMakeRange(0, attrPhone.length)];
    
    [attrPhone addAttribute:NSUnderlineStyleAttributeName
                      value:@(1)
                      range:NSMakeRange(0, attrPhone.length)];
    
    [attrPhone addAttribute:NSForegroundColorAttributeName
                      value:RGB(10, 88, 43)
                      range:NSMakeRange(0, attrPhone.length)];
    
    self.phone.attributedText = attrPhone;
}

- (void)updateButton
{
    switch (self.viewMode)
    {
        case SalonsScreenModeView:
        case SalonsScreenModeModalMap:
            [self.routeBtn setTitle:NSLocalizedString(@"Маршрут", nil) forState:UIControlStateNormal];
            break;
            
        case SalonsScreenModeSelect:
            [self.routeBtn setTitle:NSLocalizedString(@"Выбрать", nil) forState:UIControlStateNormal];
            break;
    }
}

- (void)initUI
{
    // main
    UIBezierPath * maskPath = [UIBezierPath bezierPathWithRoundedRect:self.mainView.bounds
                                                    byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                                          cornerRadii:CGSizeMake(10, 10)];
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.mainView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.mainView.layer.mask = maskLayer;
    
    // triangle
    CGFloat layerHeight = ViewHeight(self.triangle);
    CGFloat layerWidth = ViewWidth(self.triangle);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(0, 0)];
    [bezierPath addLineToPoint:CGPointMake(layerWidth / 2 - 17, 0)];
    [bezierPath addLineToPoint:CGPointMake(layerWidth / 2, layerHeight)];
    [bezierPath addLineToPoint:CGPointMake(layerWidth / 2 + 17, 0)];
    [bezierPath closePath];
    
    // Mask to Path
    CAShapeLayer * shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = bezierPath.CGPath;
    self.triangle.layer.mask = shapeLayer;
    
    //Address label
    self.address.lineBreakMode = NSLineBreakByWordWrapping;
}

@end
