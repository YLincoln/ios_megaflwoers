//
//  NITMarkerInfoView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/22/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsPresenter.h"

@class NITAddressModel;

@protocol NITMarkerInfoViewDelegate <NSObject>

- (void)didCallToPhoneFromMap:(NSString *)phone;
- (void)didCreateRouteByModel:(NITAddressModel *)model;
- (void)didSelectModel:(NITAddressModel *)model;

@end

@interface NITMarkerInfoView : UIView

@property (nonatomic) NITAddressModel * model;
@property (nonatomic) SalonsScreenMode viewMode;
@property (nonatomic, weak) id <NITMarkerInfoViewDelegate> delegate;

+ (instancetype)viewWidthModel:(NITAddressModel *)model;

@end
