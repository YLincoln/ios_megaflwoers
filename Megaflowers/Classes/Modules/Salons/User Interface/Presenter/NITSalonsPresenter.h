//
//  NITSalonsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITSalonsProtocols.h"

@class NITSalonsWireFrame;

@interface NITSalonsPresenter : NITRootPresenter <NITSalonsPresenterProtocol, NITSalonsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITSalonsViewProtocol> view;
@property (nonatomic, strong) id <NITSalonsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITSalonsWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITSalonsDelegate> delegate;
@property (nonatomic) SalonsScreenMode viewMode;
@property (nonatomic) SalonsSectionType sectionType;
@property (nonatomic) NITAddressModel * favoriteSalon;

@end
