//
//  NITSalonsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSalonsPresenter.h"
#import "NITSalonsWireframe.h"
#import "NITCitiesWireFrame.h"
#import "NITOrdersListWireFrame.h"

@interface NITSalonsPresenter ()

@property (nonatomic) HSClusterMapView * map;
@property (nonatomic) GMSPolyline * rectangle;
@property (nonatomic) SalonsSortType sortType;
@property (nonatomic) NITCityModel * city;
@property (nonatomic) NSArray <NITAddressModel *> * salons;
@property (nonatomic) BOOL needShowSalonOnMap;

@end

@interface NITSalonsPresenter ()

@property (nonatomic) BOOL dataLoaded;

@end

@implementation NITSalonsPresenter

#pragma mark - Custom accessors
- (NITCityModel *)city
{
    if (!_city || !_city.identifier || !_city.name)
    {
        _city = [NITCityModel new];
    }
    
    return _city;
}

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - NITSalonsPresenterProtocol
// Data
- (void)initDataWithMap:(HSClusterMapView *)map
{
    self.map = map;
    [self addObservers];
    [self updateData];
}

- (void)updateData
{
    self.dataLoaded = false;
    
    [self.view reloadSortTitle:[self filterTitle]];
    [LOCATION_MANAGER updateLocationWithHandler:^{
        
        switch (self.viewMode) {
            
            case SalonsScreenModeModalMap: {
                [self setSectionType:SalonsSectionTypeMap];
                [self.view reloadView];
                if (self.favoriteSalon)
                {
                    NITAddressSectionModel * sec = [NITAddressSectionModel new];
                    sec.items = @[self.favoriteSalon];
                    [self updateSalonsModels:@[sec]];
                }
            } break;
                
            case SalonsScreenModeSelect: {
                [self.view reloadCityTitle:USER.cityName];
                [self.interactor getSalonsBySortType:self.sortType cityId:USER.cityID];
            } break;
                
            case SalonsScreenModeView: {
                [self.view reloadCityTitle:self.city.name];
                [self.interactor getSalonsBySortType:self.sortType cityId:self.city.identifier];
            } break;
        }
    }];
}

// Force Touch
- (void)checkForceTouchAction
{
    if (self.dataLoaded)
    {
        if ([Shortcut isCreatedShortcut])
        {
            if ([Shortcut isAvailableShortcutByTag:@"Salons"])
            {
                // show salons list
                [Shortcut deleteShortcut];
                [self setSectionType:SalonsSectionTypeAddress];
            }
            else if([Shortcut isAvailableShortcutByTag:@"SalonsWay"])
            {
                // show nearby salon on map
                [Shortcut deleteShortcut];
                [self setSectionType:SalonsSectionTypeMap];
                [self showNearbySalon];
            }
            
            [self.view reloadView];
        }
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self checkForceTouchAction];
        });
    }
}

// Action
- (void)dismissView
{
    [self.wireFrame dismissView:self.view];
}

- (void)searchSalonsByText:(NSString *)text
{
    [self.interactor searchSalonsByText:text];
}

- (void)callToSalonByPhone:(NSString *)phone
{
    [PHONE callToPhoneNumber:phone];
}

- (void)selectSalonAddressByModel:(NITAddressModel *)model
{
    if ([self.delegate respondsToSelector:@selector(didSelectAddress:)])
    {
        [self.delegate didSelectAddress:model];
    }
    
    [TRACKER trackEvent:TrackerEventPickupSalon parameters:@{@"id":model.identifier}];
}

- (void)changeFavoriteStateForSalon:(NITAddressModel *)model
{
    if ([USER isAutorize])
    {
        [self.interactor changeFavoriteStateForSalon:model];
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        
        weaken(self);
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Регистрация", nil) actionBlock:^{
            [weakSelf.wireFrame showUserRegistrationFrom:self.view];
        }];
        
        [HELPER showActionSheetFromView:self.view
                              withTitle:NSLocalizedString(@"Данная функция приложения не доступна без регистрации", nil)
                          actionButtons:@[action1]
                           cancelButton:cancel];
    }
}

- (void)sortSalons
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSString * action1Title = NSLocalizedString(@"Все магазины", nil);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:action1Title actionBlock:^{
        weakSelf.sortType = SalonsSortTypeAll;
        [weakSelf updateData];
    }];
    
    NSString * action2Title = NSLocalizedString(@"Работают сейчас", nil);
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:action2Title actionBlock:^{
        weakSelf.sortType = SalonsSortTypeWorkNow;
        [weakSelf updateData];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1,action2] cancelButton:cancel];
}

- (void)changeCity
{
    weaken(self);
    [self.wireFrame selectCityFrom:self.view currentCityId:self.city.identifier withHandler:^(NITCityCellModel *model) {

        weakSelf.city.identifier = model.identifier;
        weakSelf.city.name = model.title;
        weakSelf.city.phone = model.phone;
        
        [weakSelf updateData];
    }];
}

// Map
- (void)selectSalonModel:(NITAddressModel *)model zoomed:(BOOL)zoomed
{
    NITAddressModel * selectedModel = [self.salons bk_select:^BOOL(NITAddressModel *obj) {
        return obj.selected;
    }].firstObject;
    
    [self setSectionType:SalonsSectionTypeMap];
    [self.view reloadView];
    
    if (selectedModel && [selectedModel.identifier isEqualToNumber:model.identifier])
    {
        return;
    }
    
    [self.salons bk_each:^(NITAddressModel *obj) {
        obj.selected = model && [obj.identifier isEqualToNumber:model.identifier];
    }];
    
    if (model)
    {
        [self moveCameraToLocation:model.location zoomed:zoomed];
    }
    
    NSArray * markers = [self.salons bk_map:^id(NITAddressModel *obj) {
        return [[NITSalonMarker alloc] initWithModel:obj];
    }];
    
    [self.view reloadMapByMarkers:markers];
    
    NITSalonMarker * marker = [markers bk_select:^BOOL(NITSalonMarker *obj) {
        return obj.model.selected == true;
    }].firstObject;
    
    [self.view didShowMarker:marker];
}

- (void)createRouteByModel:(NITAddressModel *)model
{
    if ([LOCATION_MANAGER currentLocation])
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        
        NSMutableArray * actions = [NSMutableArray new];
        
        weaken(self);
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Маршрут в приложении", nil) actionBlock:^{
            [LOCATION_MANAGER googleFetchPolylineToDestination:model.location completionHandler:^(GMSPolyline * rectangle) {
                
                weakSelf.rectangle.map = nil;
                weakSelf.rectangle = rectangle;
                weakSelf.rectangle.strokeWidth = 2.f;
                weakSelf.rectangle.strokeColor = RGB(10, 88, 43);
                weakSelf.rectangle.map = self.map;
                
                GMSCoordinateBounds * bounds = [[GMSCoordinateBounds alloc] initWithPath:weakSelf.rectangle.path];
                GMSCameraUpdate * update = [GMSCameraUpdate fitBounds:bounds withPadding:15.5];
                [self.map moveCamera:update];
                [self.map animateToZoom:self.map.camera.zoom * 0.97];
            }];
        }];
        
        [actions addObject:action1];
        
        // apple
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://maps.apple.com/"]])
        {
            NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Maps", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action2];
        }
        
        // google
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]])
        {
            NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Google Maps", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action3];
        }
        
        // yandex
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"yandexmaps://maps.yandex.ru/"]])
        {
            NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Yandex Maps", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"yandexmaps://build_route_on_map/?lat_from=%f&lon_from=%f&lat_to=%f&lon_to=%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action3];
        }
        
        [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:actions cancelButton:cancel];
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Перейти к настройкам?", nil) actionBlock:^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        
        [HELPER showActionSheetFromView:self.view
                              withTitle:NSLocalizedString(@"Для проложения маршрута необходимо включить доступ к геолокации!", nil)
                          actionButtons:@[action1]
                           cancelButton:cancel];
    }
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCity) name:kChangeCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveFavoriteSalonNotification object:nil];
}

- (NSString *)filterTitle
{
    switch (self.sortType) {
        case SalonsSortTypeAll:       return NSLocalizedString(@"Все магазины", nil);
        case SalonsSortTypeWorkNow:   return NSLocalizedString(@"Работают сейчас", nil);
    }
}

- (void)updateCity
{
    self.city = [NITCityModel new];
    [self updateData];
}

// Map
- (void)moveCameraToLocation:(CLLocation *)location zoomed:(BOOL)zoomed
{
    GMSCameraPosition * cameraPosition = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                     longitude:location.coordinate.longitude
                                                                          zoom:zoomed ? 15.5 : self.map.camera.zoom];
    
    [self.map animateToCameraPosition:cameraPosition];
}

- (void)showNearbySalon
{
    NITCitiesInteractor * citiesInteractor = [NITCitiesInteractor new];
    citiesInteractor.APIDataManager = [NITCitiesAPIDataManager new];
    [citiesInteractor updateDataWithHandler:^(NSArray<NITCityCellModel *> * models) {
        
        if ([LOCATION_MANAGER currentLocation])
        {
            NSMutableArray * sortedModels = [[models bk_select:^BOOL(NITCityCellModel *obj) {
                return obj.location != nil;
            }] mutableCopy];
            [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:keyPath(NITCityCellModel.distance) ascending:true]]];
            
            NITCityCellModel * city = sortedModels.firstObject;
            self.needShowSalonOnMap = true;
            
            if (![city.identifier isEqualToNumber:self.city.identifier])
            {
                NITCityModel * cityModel = [NITCityModel new];
                cityModel.identifier = city.identifier;
                cityModel.name = city.title;
                cityModel.phone = city.phone;
                
                self.city = cityModel;
                [self updateData];
            }
            else
            {
                [self showNearbySalonOnMapIfNeed];
            }
        }
        else
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
            NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Настройки", nil) actionBlock:^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            
            NSString * title = NSLocalizedString(@"Чтобы определить ближайший салон нужно включить доступ к определению геолокации", nil);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [HELPER showActionSheetFromView:WINDOW.visibleController withTitle:title actionButtons:@[action1] cancelButton:cancel];
            });
        }
    }];
}

- (void)showNearbySalonOnMapIfNeed
{
    if (self.needShowSalonOnMap)
    {
        self.needShowSalonOnMap = false;
        NSMutableArray * sortedModels = [[self.salons bk_select:^BOOL(NITAddressModel *obj) {
            return obj.location != nil;
        }] mutableCopy];
        
        [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:keyPath(NITAddressModel.distance) ascending:true]]];
        [self setSectionType:SalonsSectionTypeMap];
        [self selectSalonModel:sortedModels.firstObject zoomed:true];
    }
}

- (GMSCameraUpdate *)cameraUpdateByModels:(NSArray<NITAddressSectionModel *> *)models
{
    GMSMutablePath * rect = [GMSMutablePath path];
    for (NITAddressSectionModel * section in models)
    {
        [section.items bk_each:^(NITAddressModel *obj) {
            [rect addCoordinate:CLLocationCoordinate2DMake(obj.location.coordinate.latitude, obj.location.coordinate.longitude)];
        }];
    }
    
    GMSCameraUpdate * update;
    
    if (rect.count == 1)
    {
        CLLocationCoordinate2D coordinate = [rect coordinateAtIndex:0];
        GMSCameraPosition * camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
                                                                 longitude:coordinate.longitude
                                                                      zoom:15.5];
        update = [GMSCameraUpdate setCamera:camera];
    }
    else
    {
        // Create the polygon, and assign it to the map.
        GMSPolyline * polyline = [GMSPolyline polylineWithPath:rect];
        GMSCoordinateBounds * bounds = [[GMSCoordinateBounds alloc] initWithPath:polyline.path];
        update = [GMSCameraUpdate fitBounds:bounds withPadding:15.5];
    }
    
    return update;
}

#pragma mark - NITSalonsInteractorOutputProtocol
- (void)updateSalonsModels:(NSArray<NITAddressSectionModel *> *)models
{
    self.dataLoaded = true;
    
    NSMutableArray * salons = [NSMutableArray new];
    for (NITAddressSectionModel * section in models)
    {
        [section.items bk_each:^(id obj) {
            [salons addObject:obj];
        }];
    }
    
    self.salons = salons;
    
    NSArray * markers = [salons bk_map:^id(NITAddressModel *obj) {
        return [[NITSalonMarker alloc] initWithModel:obj];
    }];
    
    [self.map moveCamera:[self cameraUpdateByModels:models]];
    [self.view reloadSectionByModels:models];
    [self.view reloadMapByMarkers:markers];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showNearbySalonOnMapIfNeed];
    });
}



@end
