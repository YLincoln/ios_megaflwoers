//
//  NITMyOrdersListCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/6/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrdersListProtocols.h"
#import "MGSwipeTableCell.h"

@protocol NITMyOrdersListCellDelegate <NSObject>

- (void)didLeaveFeedbackForOrder:(NITMyOrderModel *)model;
- (void)didRemoveComplitedOrder:(NITMyOrderModel *)model;

@end

@interface NITMyOrdersListCell : MGSwipeTableCell

@property (nonatomic) NITMyOrderModel * model;
@property (nonatomic) OrdersSectionType sectionType;
@property (nonatomic, weak) id <NITMyOrdersListCellDelegate> cellDelegate;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
