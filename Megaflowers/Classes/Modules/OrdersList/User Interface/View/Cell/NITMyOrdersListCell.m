//
//  NITMyOrdersListCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/6/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITMyOrdersListCell.h"
#import "UIImageView+AFNetworking.h"

@interface NITMyOrdersListCell ()

@property (nonatomic) IBOutlet UIView * containerView;
@property (nonatomic) IBOutlet UIImageView * icon;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * city;
@property (nonatomic) IBOutlet UILabel * name;
@property (nonatomic) IBOutlet UIView * paymentView;
@property (nonatomic) IBOutlet UIView * feedbackView;
@property (nonatomic) IBOutlet UIImageView * paymentIcon;
@property (nonatomic) IBOutlet UILabel * paymentText;

@end

@implementation NITMyOrdersListCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self.contentView addSubview:self.containerView];
    }
    
    return self;
}

- (void)setSectionType:(OrdersSectionType)sectionType
{
    _sectionType = sectionType;
    [self updateSectionType];
    [self updateSwipeButtonsIfNeed];
}

- (void)setModel:(NITMyOrderModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)feedbackAction:(id)sender
{
    if ([self.cellDelegate respondsToSelector:@selector(didLeaveFeedbackForOrder:)])
    {
        [self.cellDelegate didLeaveFeedbackForOrder:self.model];
    }
}

- (IBAction)removeAction:(id)sender
{
    if ([self.cellDelegate respondsToSelector:@selector(didRemoveComplitedOrder:)])
    {
        [self.cellDelegate didRemoveComplitedOrder:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.icon.image = nil;
    [self.icon setImageWithURL:[NSURL URLWithString:self.model.imagePath]];
    
    self.title.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Заказ №", nil), self.model.num];
    self.city.text = self.model.address ? : @"";
    self.name.text = self.model.receiverName ? : @"";
    
    [self updatePaymentView];
}

- (void)updateSectionType
{
    self.paymentView.hidden = self.sectionType != OrdersSectionTypeCurrent;
    self.feedbackView.hidden = self.sectionType != OrdersSectionTypeComplited;
}

- (void)updatePaymentView
{
    switch (self.model.paymentStatus) {
        case OrdersPaymentStatusNotPaid:
            self.paymentIcon.image = [UIImage imageNamed:@"ic_not_paid"];
            self.paymentText.text = NSLocalizedString(@"Не оплачен", nil);
            self.paymentText.textColor = RGB(227, 104, 104);
            break;
            
        case OrdersPaymentStatusPartPaid:
            self.paymentIcon.image = [UIImage imageNamed:@"ic_partially"];
            self.paymentText.text = NSLocalizedString(@"Заказ оплачен частично", nil);
            self.paymentText.textColor = RGB(10, 88, 43);
            break;
            
        case OrdersPaymentStatusPaid:
        case OrdersPaymentStatusOverpaid:
            self.paymentIcon.image = [UIImage imageNamed:@"ic_check"];
            self.paymentText.text = NSLocalizedString(@"Оплачен", nil);
            self.paymentText.textColor = RGB(10, 88, 43);
            break;
    }
}

- (void)updateSwipeButtonsIfNeed
{
    if (self.sectionType == OrdersSectionTypeComplited)
    {
        weaken(self);
        MGSwipeButton * deleteBtn = [MGSwipeButton buttonWithTitle:NSLocalizedString(@"Удалить", nil)
                                                   backgroundColor:RGB(236, 106, 106)
                                                          callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
            [weakSelf removeAction:nil];
            return false;
        }];
        
        self.rightButtons = @[deleteBtn];
        self.rightSwipeSettings.transition = MGSwipeTransitionBorder;
        self.rightExpansion.buttonIndex = 0;
        self.rightExpansion.fillOnTrigger = true;
    }
}

@end
