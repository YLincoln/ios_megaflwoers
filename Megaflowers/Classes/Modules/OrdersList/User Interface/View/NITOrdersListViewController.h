//
//  NITOrdersListViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITOrdersListProtocols.h"

@interface NITOrdersListViewController : NITBaseViewController <NITOrdersListViewProtocol>

@property (nonatomic, strong) id <NITOrdersListPresenterProtocol> presenter;

@end
