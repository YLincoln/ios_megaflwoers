//
//  NITOrdersListViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrdersListViewController.h"
#import "NITMyOrdersListCell.h"
#import "MGSwipeButton.h"

@interface NITOrdersListViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITMyOrdersListCellDelegate
>

@property (nonatomic) IBOutlet UISegmentedControl * segmentControl;
@property (nonatomic) IBOutlet UITableView * currentOrdersTable;
@property (nonatomic) IBOutlet UITableView * completedOrdersTable;
@property (nonatomic) IBOutlet UIView * emptyView;
@property (nonatomic) IBOutlet UILabel * emptyViewTitle;
@property (nonatomic) IBOutlet UILabel * emptyViewMessage;
@property (nonatomic) IBOutlet UILabel * sortTitle;
@property (nonatomic) IBOutlet NSLayoutConstraint * sortTitleW;

@property (nonatomic) NSArray <NITMyOrderModel *> * currentItems;
@property (nonatomic) NSArray <NITMyOrderModel *> * completedItems;

@end

@implementation NITOrdersListViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.presenter initData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSInteger index = [self.presenter showControllerFrom3DTouch:self.currentItems];
    self.presenter.sectionType = index;
    [self.segmentControl setSelectedSegmentIndex:index];
    [self updateSectionType];
}

#pragma mark - IBAction
- (IBAction)changedViewSection:(id)sender
{
    self.presenter.sectionType = self.segmentControl.selectedSegmentIndex;
    [self updateSectionType];
    [self updateEmptyView];
}

- (IBAction)sortAction:(id)sender
{
    [self.presenter sortComplitedOrders];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Мои заказы", nil);
    
    // table view
    [self.currentOrdersTable setTableFooterView:[UIView new]];
    [self.completedOrdersTable setTableFooterView:[UIView new]];
    [self.currentOrdersTable registerNib:[UINib nibWithNibName:_s(NITMyOrdersListCell) bundle:nil] forCellReuseIdentifier:_s(NITMyOrdersListCell)];
    [self.completedOrdersTable registerNib:[UINib nibWithNibName:_s(NITMyOrdersListCell) bundle:nil] forCellReuseIdentifier:_s(NITMyOrdersListCell)];
    
    [self updateSectionType];
}

- (void)updateSectionType
{
    self.currentOrdersTable.hidden = self.presenter.sectionType != OrdersSectionTypeCurrent;
    self.completedOrdersTable.hidden = self.presenter.sectionType != OrdersSectionTypeComplited;
}

- (void)updateEmptyView
{
    switch (self.presenter.sectionType) {
        case OrdersSectionTypeCurrent: {
            self.emptyView.hidden = self.currentItems.count > 0;
            self.emptyViewTitle.text = self.completedItems.count == 0 ? NSLocalizedString(@"Текущих заказов нет", nil) : NSLocalizedString(@"Все заказы завершены", nil);
            self.emptyViewMessage.text = NSLocalizedString(@"Оформите новые заказы и они появятся здесь", nil);
        } break;
            
        case OrdersSectionTypeComplited: {
            self.emptyView.hidden = self.completedItems.count > 0;
            self.emptyViewTitle.text = NSLocalizedString(@"Завершенных заказов нет", nil);
            self.emptyViewMessage.text = NSLocalizedString(@"Здесь будет хранится информация о завершенных заказах", nil);
        } break;
    }
}

#pragma mark - NITOrdersListPresenterProtocol
- (void)reloadCurrentOrdersByModels:(NSArray<NITMyOrderModel *> *)models
{
    self.currentItems = models;
    [self updateSectionType];
    [self updateEmptyView];
    [self.currentOrdersTable reloadData];
}

- (void)reloadComplitedOrdersByModels:(NSArray<NITMyOrderModel *> *)models
{
    self.completedItems = models;
    [self updateSectionType];
    [self updateEmptyView];
    [self.completedOrdersTable reloadData];
}

- (void)reloadSortTitle:(NSString *)title
{
    self.sortTitle.text = title;
    self.sortTitleW.constant = [self.sortTitle sizeOfMultiLineLabel].width;
    [self.view layoutIfNeeded];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableView == self.currentOrdersTable ? self.currentItems.count : self.completedItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITMyOrdersListCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITMyOrdersListCell) forIndexPath:indexPath];
    cell.model = tableView == self.currentOrdersTable ? self.currentItems[indexPath.row] : self.completedItems[indexPath.row];
    cell.sectionType = tableView == self.currentOrdersTable ? OrdersSectionTypeCurrent : OrdersSectionTypeComplited;
    cell.cellDelegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITMyOrderModel * model = tableView == self.currentOrdersTable ? self.currentItems[indexPath.row] : self.completedItems[indexPath.row];
    [self.presenter showOrder:model];
}

#pragma mark - NITMyOrdersListCellDelegate
- (void)didLeaveFeedbackForOrder:(NITMyOrderModel *)model
{
    [self.presenter leaveFeedbackForOrder:model];
}

- (void)didRemoveComplitedOrder:(NITMyOrderModel *)model
{
    [self.presenter removeComplitedOrder:model];
}

@end
