//
//  NITOrdersListPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrdersListProtocols.h"

@class NITOrdersListWireFrame;

@interface NITOrdersListPresenter : NITRootPresenter <NITOrdersListPresenterProtocol, NITOrdersListInteractorOutputProtocol>

@property (nonatomic, weak) id <NITOrdersListViewProtocol> view;
@property (nonatomic, strong) id <NITOrdersListInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITOrdersListWireFrameProtocol> wireFrame;
@property (nonatomic) OrdersSectionType sectionType;

@end
