//
//  NITOrdersListPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrdersListPresenter.h"
#import "NITOrdersListWireframe.h"

@implementation NITOrdersListPresenter

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - NITOrdersListPresenterProtocol
- (void)initData
{
    [self updateData];
    [self.view reloadSortTitle:NSLocalizedString(@"По дате", nil)];
}

- (void)updateData
{
    [self.interactor getUserOrders];
}

- (void)sortComplitedOrders
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSString * action1Title = NSLocalizedString(@"По дате", nil);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:action1Title actionBlock:^{
        [weakSelf.interactor sortComplitedOrdersByType:OrdersSortTypeDate];
        [weakSelf.view reloadSortTitle:action1Title];
    }];
    
    NSString * action2Title = NSLocalizedString(@"По имени", nil);
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:action2Title actionBlock:^{
        [weakSelf.interactor sortComplitedOrdersByType:OrdersSortTypeName];
        [weakSelf.view reloadSortTitle:action2Title];
    }];
    
    NSString * action3Title = NSLocalizedString(@"По городу доставки", nil);
    NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:action3Title actionBlock:^{
        [weakSelf.interactor sortComplitedOrdersByType:OrdersSortTypeCity];
        [weakSelf.view reloadSortTitle:action3Title];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:@[action1,action2,action3] cancelButton:cancel];
}

- (void)removeComplitedOrder:(NITMyOrderModel *)model
{
    weaken(self);
    [self.interactor removeComplitedOrder:model withHandler:^(NSString *error) {
        if (error)
        {
            [weakSelf showError:error];
        }
    }];
}

- (void)showOrder:(NITMyOrderModel *)model
{
    [self.wireFrame showOrder:model from:self.view];
}

- (void)leaveFeedbackForOrder:(NITMyOrderModel *)model
{
    [self.wireFrame showLeaveFeedbackForOrder:model from:self.view];
}

#pragma mark - Private
- (void)showError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Oк", nil) actionBlock:^{}];
    [HELPER showActionSheetFromView:self.view withTitle:error actionButtons:nil cancelButton:cancel];
}

#pragma mark - NITOrdersListInteractorOutputProtocol
- (void)updateCurrentOrdersByModels:(NSArray<NITMyOrderModel *> *)models
{
    [self.view reloadCurrentOrdersByModels:models];
}

- (void)updateComplitedOrdersByModels:(NSArray<NITMyOrderModel *> *)models
{
    [self.view reloadComplitedOrdersByModels:models];
}

#pragma mark - 3D Touch
- (NSInteger)showControllerFrom3DTouch:(NSArray <NITMyOrderModel *> *)currentItems
{
    if([Shortcut isCreatedShortcut])
    {
        if([Shortcut isAvailableShortcutByTag:@"Status"])
        {
            NITMyOrderModel * model;
            for(NITMyOrderModel * order in currentItems)
            {
                if(order.processStatus == OrdersProcessStatusNone || order.processStatus == OrdersProcessStatusOrder || order.processStatus == OrdersProcessStatusBouquet)
                    model = order;
                break;
            }
            if(model)
            {
                [self showOrder:model];
                [Shortcut deleteShortcut];
            }
        }
        if([Shortcut isAvailableShortcutByTag:@"OrderActive"])
        {
            NITMyOrderModel * model;
            for(NITMyOrderModel * order in currentItems)
            {
                if(order.processStatus == OrdersProcessStatusCourier)
                    model = order;
                break;
            }
            if(model)
            {
                [self showOrder:model];
                [Shortcut deleteShortcut];
            }
        }
        if([Shortcut isAvailableShortcutByTag:@"OrdersComplited"])
        {
            [Shortcut deleteShortcut];
            return OrdersSectionTypeComplited;
        }
        if([Shortcut isAvailableShortcutByTag:@"OrderPickup"])
        {
            NITMyOrderModel * model;
            for(NITMyOrderModel * order in currentItems)
            {
                if(order.isPickup)
                    model = order;
                break;
            }
            if(model)
            {
                [self showOrder:model];
                [Shortcut deleteShortcut];
            }
        }
    }
    return self.sectionType;
}

@end
