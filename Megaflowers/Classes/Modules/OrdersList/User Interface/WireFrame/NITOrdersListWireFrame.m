//
//  NITOrdersListWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrdersListWireFrame.h"
#import "NITOrderDetailsWireFrame.h"
#import "NITRateDeliveryWireFrame.h"

@implementation NITOrdersListWireFrame

+ (id)createNITOrdersListModule
{
    // Generating module components
    id <NITOrdersListPresenterProtocol, NITOrdersListInteractorOutputProtocol> presenter = [NITOrdersListPresenter new];
    id <NITOrdersListInteractorInputProtocol> interactor = [NITOrdersListInteractor new];
    id <NITOrdersListAPIDataManagerInputProtocol> APIDataManager = [NITOrdersListAPIDataManager new];
    id <NITOrdersListLocalDataManagerInputProtocol> localDataManager = [NITOrdersListLocalDataManager new];
    id <NITOrdersListWireFrameProtocol> wireFrame= [NITOrdersListWireFrame new];
    id <NITOrdersListViewProtocol> view = [(NITOrdersListWireFrame *)wireFrame createViewControllerWithKey:_s(NITOrdersListViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITOrdersListModuleFrom:(UIViewController *)fromViewController
{
    NITOrdersListViewController * view = [NITOrdersListWireFrame createNITOrdersListModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showLeaveFeedbackForOrder:(NITMyOrderModel *)model from:(UIViewController *)fromViewController
{
    [NITRateDeliveryWireFrame presentNITRateDeliveryModuleFrom:fromViewController orderId:model.identifier];
}

- (void)showOrder:(NITMyOrderModel *)model from:(UIViewController *)fromViewController
{
    [NITOrderDetailsWireFrame presentNITOrderDetailsModuleFrom:fromViewController withModel:model];
}

@end
