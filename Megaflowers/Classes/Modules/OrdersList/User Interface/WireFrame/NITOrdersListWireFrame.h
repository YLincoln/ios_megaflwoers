//
//  NITOrdersListWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrdersListProtocols.h"
#import "NITOrdersListViewController.h"
#import "NITOrdersListLocalDataManager.h"
#import "NITOrdersListAPIDataManager.h"
#import "NITOrdersListInteractor.h"
#import "NITOrdersListPresenter.h"
#import "NITOrdersListWireframe.h"
#import "NITRootWireframe.h"

@interface NITOrdersListWireFrame : NITRootWireframe <NITOrdersListWireFrameProtocol>

@end
