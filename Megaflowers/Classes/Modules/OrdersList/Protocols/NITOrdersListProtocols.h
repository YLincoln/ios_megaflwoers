//
//  NITOrdersListProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITMyOrderModel.h"
#import "NITOrder.h"
#import "SWGOrder.h"

@protocol NITOrdersListInteractorOutputProtocol;
@protocol NITOrdersListInteractorInputProtocol;
@protocol NITOrdersListViewProtocol;
@protocol NITOrdersListPresenterProtocol;
@protocol NITOrdersListLocalDataManagerInputProtocol;
@protocol NITOrdersListAPIDataManagerInputProtocol;

typedef CF_ENUM (NSUInteger, OrdersSectionType) {
    OrdersSectionTypeCurrent    = 0,
    OrdersSectionTypeComplited  = 1
};

typedef CF_ENUM (NSUInteger, OrdersSortType) {
    OrdersSortTypeDate  = 0,
    OrdersSortTypeName  = 1,
    OrdersSortTypeCity  = 3
};

@class NITOrdersListWireFrame;

@protocol NITOrdersListViewProtocol
@required
@property (nonatomic, strong) id <NITOrdersListPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadCurrentOrdersByModels:(NSArray <NITMyOrderModel *> *)models;
- (void)reloadComplitedOrdersByModels:(NSArray <NITMyOrderModel *> *)models;
- (void)reloadSortTitle:(NSString *)title;
@end

@protocol NITOrdersListWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITOrdersListModuleFrom:(id)fromView;
- (void)showOrder:(NITMyOrderModel *)model from:(id)fromView;
- (void)showLeaveFeedbackForOrder:(NITMyOrderModel *)model from:(id)fromView;
@end

@protocol NITOrdersListPresenterProtocol
@required
@property (nonatomic, weak) id <NITOrdersListViewProtocol> view;
@property (nonatomic, strong) id <NITOrdersListInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITOrdersListWireFrameProtocol> wireFrame;
@property (nonatomic) OrdersSectionType sectionType;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updateData;
- (void)sortComplitedOrders;
- (void)removeComplitedOrder:(NITMyOrderModel *)model;
- (void)leaveFeedbackForOrder:(NITMyOrderModel *)model;
- (void)showOrder:(NITMyOrderModel *)model;
- (NSInteger)showControllerFrom3DTouch:(NSArray <NITMyOrderModel *> *)currentItems;
@end

@protocol NITOrdersListInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCurrentOrdersByModels:(NSArray <NITMyOrderModel *> *)models;
- (void)updateComplitedOrdersByModels:(NSArray <NITMyOrderModel *> *)models;
@end

@protocol NITOrdersListInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITOrdersListInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITOrdersListAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITOrdersListLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getUserOrders;
- (void)sortComplitedOrdersByType:(OrdersSortType)type;
- (void)removeComplitedOrder:(NITMyOrderModel *)model withHandler:(void(^)(NSString * error))handler;

@end


@protocol NITOrdersListDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITOrdersListAPIDataManagerInputProtocol <NITOrdersListDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserOrdersWithHandler:(void(^)(NSArray <SWGOrder> *result))handler;
- (void)deleteOrderByID:(NSNumber *)orderID withHandler:(void(^)(NSError *error))handler;
@end

@protocol NITOrdersListLocalDataManagerInputProtocol <NITOrdersListDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
