//
//  NITOrdersListAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrdersListAPIDataManager.h"
#import "SWGOrderApi.h"

@implementation NITOrdersListAPIDataManager

- (void)getUserOrdersWithHandler:(void(^)(NSArray <SWGOrder> *result))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderGetWithLang:HELPER.lang site:API.site city:USER.cityID status:nil completionHandler:^(NSArray<SWGOrder> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler((id)[output bk_select:^BOOL(SWGOrder *obj) {
            return obj.statusId.integerValue > 1;
        }]);
    }];
}

- (void)deleteOrderByID:(NSNumber *)orderID withHandler:(void(^)(NSError *error))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderIdDeleteWithId:orderID completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(error);
    }];
}

@end
