//
//  NITOrdersListInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrdersListInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITCatalogItemViewModel.h"
#import "SWGBouquet.h"
#import "SWGAttach.h"
#import "NITBasketProductModel.h"
#import "NITOrderDetailsAPIDataManager.h"

@interface NITOrdersListInteractor ()

@property (nonatomic) NSArray <NITMyOrderModel *> * currentData;
@property (nonatomic) NSArray <NITMyOrderModel *> * complitedData;
@property (nonatomic) OrdersSortType sortType;

@end

@implementation NITOrdersListInteractor

- (void)getUserOrders
{
    if ([USER isAutorize])
    {
        weaken(self);
        [self.APIDataManager getUserOrdersWithHandler:^(NSArray<SWGOrder> *result) {
            NSArray * orders = [weakSelf orderModelsFromEntities:result];
            [weakSelf updateCurrentOrdersByModels:orders];
            [weakSelf updateComplitedOrdersByModels:orders];
        }];
    }
    else if (USER.lastOrderID)
    {
        [USER checkAutorizationWithHandler:^(BOOL success) {
            if (success)
            {
                weaken(self);
                NITOrderDetailsAPIDataManager * api = [NITOrderDetailsAPIDataManager new];
                [api getUserOrderByID:USER.lastOrderID withHandler:^(SWGOrder *result) {
                    if (result)
                    {
                        NITMyOrderModel * model = [[NITMyOrderModel alloc] initWithOrder:result];
                        [weakSelf updateCurrentOrdersByModels:@[model]];
                        [weakSelf updateComplitedOrdersByModels:@[model]];
                    }
                }];
            }
        }];
    }
}

- (void)updateCurrentOrdersByModels:(NSArray <NITMyOrderModel *> *)models
{
    self.currentData = [models bk_select:^BOOL(NITMyOrderModel *obj) {
        return obj.complited == false;
    }];
    
    [self.presenter updateCurrentOrdersByModels:self.currentData];
}

- (void)updateComplitedOrdersByModels:(NSArray <NITMyOrderModel *> *)models
{
    self.complitedData = [models bk_select:^BOOL(NITMyOrderModel *obj) {
        return obj.complited == true;
    }];
    
    [self.presenter updateComplitedOrdersByModels:[self sortedComplitedOrders]];
}

- (void)sortComplitedOrdersByType:(OrdersSortType)type
{
    [self.presenter updateComplitedOrdersByModels:[self sortedComplitedOrders]];
}

- (void)removeComplitedOrder:(NITMyOrderModel *)model withHandler:(void(^)(NSString * error))handler
{
    weaken(self);
    [self.APIDataManager deleteOrderByID:model.identifier withHandler:^(NSError *error) {
        if (error)
        {
            handler(error.interfaceDescription);
        }
        else
        {
            weakSelf.complitedData = [weakSelf.complitedData bk_select:^BOOL(NITMyOrderModel *obj) {
                return ![obj.identifier isEqualToNumber:model.identifier];
            }];
            [weakSelf.presenter updateComplitedOrdersByModels:[weakSelf sortedComplitedOrders]];
            
            handler(nil);
        }
    }];
}

#pragma mark - Private
- (NSArray <NITMyOrderModel *> *)sortedComplitedOrders
{
    NSString * sortKey;
    BOOL ascending;
    
    switch (self.sortType) {
        case OrdersSortTypeDate:
            sortKey = keyPath(NITMyOrderModel.date);
            ascending = false;
            break;
        case OrdersSortTypeName:
            sortKey = keyPath(NITMyOrderModel.userName);
            ascending = true;
            break;
        case OrdersSortTypeCity:
            sortKey = keyPath(NITMyOrderModel.cityName);
            ascending = true;
            break;
    }
    
    NSMutableArray * sortedModels = [self.complitedData mutableCopy];
    [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:ascending]]];
    
    return sortedModels;
}

- (NSArray <NITMyOrderModel *> *)orderModelsFromEntities:(NSArray <SWGOrder *> *)entities
{
    NSMutableArray * sortedModels = [[entities bk_map:^id(SWGOrder * obj) {
        return [[NITMyOrderModel alloc] initWithOrder:obj];
    }] mutableCopy];
    [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:keyPath(NITMyOrderModel.identifier) ascending:false]]];
    
    return sortedModels;
}

- (NSArray <NITCatalogItemViewModel *> *)modelsFromEntities:(NSArray <SWGBouquet *> *)entities
{
    return [entities bk_map:^id(SWGBouquet * obj) {
        return [[NITCatalogItemViewModel alloc] initWithBouquet:obj];
    }];
}

- (NSArray <NITCatalogItemViewModel *> *)attachModelsFromEntities:(NSArray <SWGAttach *> *)entities
{
    return [entities bk_map:^id(SWGAttach * obj) {
        return [[NITCatalogItemViewModel alloc] initWithAttach:obj];
    }];
}

@end
