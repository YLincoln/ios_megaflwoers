//
//  NITMyOrderModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/6/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITMyOrderModel.h"
#import "NITBasketProductModel.h"
#import "NITOrder.h"
#import "SWGOrder.h"
#import "SWGBasket.h"
#import "SWGBouquet.h"
#import "NSArray+BlocksKit.h"

@implementation NITMyOrderModel

- (instancetype)initWithOrder:(SWGOrder *)model
{
    if (self = [super init])
    {
        SWGBasket * basket = (SWGBasket *)model.basket;
        
        self.identifier         = model._id;
        self.orderHash          = model.orderHash;
        self.num                = model.num;
        self.address            = model.receiverAddress;
        self.receiverName       = model.isPickup.boolValue ? model.senderName : model.receiverName;
        self.date               = [NSDate dateFromServerString:model.deliveryDate];
        self.deliveryDate       = [NSDate dateFromServerString:model.deliveryDate];
        self.deliveryTime       = model.deliveryTime;
        self.imagePath          = [self imagePathFromBasket:basket];
        self.price              = basket.price;
        self.sumRemain          = model.sumRemain;
        self.paymentStatus      = [self paymentStatusByID:model.statusId];
        self.processStatus      = [self processStatusByID:model.statusId];
        self.complited          = [self complitedByStatus:model.statusId];
        self.items              = [self itemsFromBasket:basket];
        self.isPickup           = model.isPickup.boolValue;
    }
    
    return self;
}

- (NSArray *)itemsFromBasket:(SWGBasket *)basket
{
    if (!basket)
    {
        return nil;
    }
    
    NSMutableArray * items = [NSMutableArray new];
    
    [items addObjectsFromArray:[basket.bouquets bk_map:^id(SWGBasketBouquet *obj) {
        return [NITBasketProductModel modelBySWGBouquet:obj];
    }]];
    
    [items addObjectsFromArray:[basket.attaches bk_map:^id(SWGBasketAttach *obj) {
        return [NITBasketProductModel modelBySWGAttaches:obj];
    }]];
    
    return items;
}

- (BOOL)complitedByStatus:(NSNumber *)statusID
{
    switch (statusID.integerValue)
    {
        case 12:                  // Отменён
        case 15: return true;     // Выполнен
        default: return false;
    }
}

- (OrdersProcessStatus)processStatusByID:(NSNumber *)statusID
{
    switch (statusID.integerValue)
    {
        case 1:                                         // Предзаказ
        case 12: return OrdersProcessStatusNone;        // Отменён клиентом
        case 2:                                         // Оформлен
        case 3:  return OrdersProcessStatusOrder;       // Оплачен
        case 4:                                         // Передан в работу
        case 5:                                         // Передан другому партнёру
        case 6:                                         // Передан нескольким партнёрам
        case 7:                                         // Принят партнёром
        case 8:                                         // Контроль выполнения
        case 9:                                         // Замена
        case 13:                                        // Отменён партнёром
        case 14:                                        // Проблема решена
        case 17: return OrdersProcessStatusBouquet;     // В работе, имеет проблему
        case 10:                                        // Передан курьеру
        case 11:                                        // Доставлен
        case 15:                                        // Закрыт
        case 16: return OrdersProcessStatusCourier;     // Доставлен, имеет проблему
        default: return OrdersProcessStatusNone;
    }
}

- (OrdersPaymentStatus)paymentStatusByID:(NSNumber *)statusID
{
    if (self.sumRemain.floatValue == 0)
    {
        return OrdersPaymentStatusPaid;
    }
    else if (![self.price isEqualToNumber:self.sumRemain] && self.sumRemain.floatValue > 0)
    {
        return OrdersPaymentStatusPartPaid;
    }
    else if (self.sumRemain.floatValue < 0)
    {
        return OrdersPaymentStatusOverpaid;
    }
    else
    {
        return OrdersPaymentStatusNotPaid;
    }
}

- (NSString *)imagePathFromBasket:(SWGBasket *)basket
{
    if (basket && basket.bouquets.count > 0)
    {
        SWGBasketBouquet * bouquet = basket.bouquets.firstObject;
        SWGBasketAttachImage * image = bouquet.image;
        return [image.detail valueForKey:API.imageKey];
    }
    else
    {
        return @"";
    }
}

@end
