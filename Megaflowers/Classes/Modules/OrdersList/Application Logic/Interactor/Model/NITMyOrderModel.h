//
//  NITMyOrderModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/6/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITBasketProductModel, NITOrder, SWGOrder, NITBiilModel;

typedef CF_ENUM (NSUInteger, OrdersPaymentStatus) {
    OrdersPaymentStatusNotPaid  = 0,
    OrdersPaymentStatusPartPaid = 1,
    OrdersPaymentStatusPaid     = 2,
    OrdersPaymentStatusOverpaid = 3
};

typedef CF_ENUM (NSUInteger, OrdersProcessStatus) {
    OrdersProcessStatusNone     = 0,
    OrdersProcessStatusOrder    = 1,
    OrdersProcessStatusBouquet  = 2,
    OrdersProcessStatusCourier  = 3
};

@interface NITMyOrderModel : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * orderHash;
@property (nonatomic) NSNumber * num;
@property (nonatomic) NSNumber * price;
@property (nonatomic) NSNumber * sumRemain;
@property (nonatomic) NSString * address;
@property (nonatomic) NSString * receiverName;
@property (nonatomic) NSString * imagePath;
@property (nonatomic) NSDate * date;
@property (nonatomic) NSDate * deliveryDate;
@property (nonatomic) NSNumber * deliveryTime;
@property (nonatomic) OrdersPaymentStatus paymentStatus;
@property (nonatomic) OrdersProcessStatus processStatus;
@property (nonatomic) BOOL complited;
@property (nonatomic) NSArray <NITBasketProductModel *> * items;
@property (nonatomic) BOOL isPickup;
@property (nonatomic) NSArray <NITBiilModel *> * bills;
@property (nonatomic) BOOL paymentTypeCash;

- (instancetype)initWithOrder:(SWGOrder *)model;

@end
