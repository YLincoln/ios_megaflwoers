//
//  NITOrdersListInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/06/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrdersListProtocols.h"

@interface NITOrdersListInteractor : NSObject <NITOrdersListInteractorInputProtocol>

@property (nonatomic, weak) id <NITOrdersListInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITOrdersListAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITOrdersListLocalDataManagerInputProtocol> localDataManager;

@end
