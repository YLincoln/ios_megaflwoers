//
//  NITFiltersAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFiltersAPIDataManager.h"
#import "SWGFilterApi.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITFiltersAPIDataManager

- (void)getFiltersByParentID:(NSNumber *)parentID withHandler:(void(^)(NSArray<SWGFilter> * result, SWGInlineResponse2001Price * price))handler
{
    [HELPER startLoading];
    [[[SWGFilterApi alloc] initWithApiClient:[SWGApiClient sharedClient]] filterGetWithLang:HELPER.lang
                                                                                       site:API.site
                                                                                       city:USER.cityID
                                                                                     parent:parentID filters:nil
                                                                                      price:nil
                                                                          completionHandler:^(SWGInlineResponse2001 *output, NSError *error) {
                                                                              [HELPER stopLoading];
                                                                              if (error)
                                                                              {
                                                                                  [HELPER logError:error method:METHOD_NAME];
                                                                              }
                                                                              handler(output.filters, output.price);
                                                                          }];
}

@end
