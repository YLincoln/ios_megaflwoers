//
//  NITRadioFilterSectionModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRadioFilterSectionModel.h"
#import "NITFilterCellModel.h"
#import "NITRadioFilterCell.h"

@implementation NITRadioFilterSectionModel

- (instancetype)initWithFilter:(SWGFilter *)filter
{
    if (self = [super init])
    {
        [self updateDataByFilter:filter];
    }
    
    return self;
}

- (NSString *)cellID
{
    return _s(NITRadioFilterCell);
}

- (CGFloat)cellHeight
{
    return [self.filterCellModels count] * 56;
}

- (CGFloat)headerHeight
{
    return 30;
}

#pragma mark - Data
- (void)updateDataByFilter:(SWGFilter *)filter
{
    self.title = filter.name;
    self.position = filter.position.integerValue;

    NSMutableArray * items = [NSMutableArray new];
    for (SWGFilterValues * filterValue in filter.values)
    {
        [items addObject:[[NITFilterCellModel alloc] initWithFilterValue:filterValue]];
    }
    
    self.filterCellModels = items;
}

@end
