//
//  NITRangeFilterSectionModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRangeFilterSectionModel.h"
#import "NITRangeFilterCell.h"
#import "SWGInlineResponse2001Price.h"

@implementation NITRangeFilterSectionModel

- (instancetype)initWithPriceFilter:(SWGInlineResponse2001Price *)filter
{
    if (self = [super init])
    {
        [self updateDataByFilter:filter];
    }
    
    return self;
}

- (NSString *)cellID
{
    return _s(NITRangeFilterCell);
}

- (CGFloat)cellHeight
{
    return 114;
}

- (CGFloat)headerHeight
{
    return 0;
}

- (void)setPrice:(PriceValue)price
{
    _price = price;
    self.textValue = [FILTERS priceStringFromFilterValue:self.price];
    [self updateSelectedState];
    
    if ([self.delegate respondsToSelector:@selector(didChangePriceFilterValue:)])
    {
        [self.delegate didChangePriceFilterValue:price];
    }
}

#pragma mark - Data
- (void)updateDataByFilter:(SWGInlineResponse2001Price *)filter
{
    self.title = filter.name;
    self.identifier = self.identifier;
    self.priceBorder = PriceValueMake([filter.values.min integerValue], [filter.values.max integerValue]);
    self.price = [FILTERS activePriceFilters] ? [FILTERS activePriceValues] : self.priceBorder;
}

- (void)updateSelectedState
{
    if (self.price.min == self.priceBorder.min &&
        self.price.max == self.priceBorder.max)
    {
        self.selected = false;
    }
    else
    {
        self.selected = true;
    }
}

@end
