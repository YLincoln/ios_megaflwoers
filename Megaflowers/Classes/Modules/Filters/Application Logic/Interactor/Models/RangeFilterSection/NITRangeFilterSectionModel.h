//
//  NITRangeFilterSectionModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterSectionModel.h"

@class SWGInlineResponse200Price;

@protocol NITRangeFilterSectionModelDelegate <NSObject>

- (void)didChangePriceFilterValue:(PriceValue)priceValue;

@end

@interface NITRangeFilterSectionModel : NITFilterSectionModel

@property (nonatomic) NSNumber * identifier;

@property (nonatomic) PriceValue priceBorder;
@property (nonatomic) PriceValue price;
@property (nonatomic) BOOL selected;

@property (nonatomic, weak) id <NITRangeFilterSectionModelDelegate> delegate;

- (instancetype)initWithPriceFilter:(SWGInlineResponse200Price *)filter;

@end
