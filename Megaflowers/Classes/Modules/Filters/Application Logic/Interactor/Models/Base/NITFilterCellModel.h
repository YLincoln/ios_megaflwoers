//
//  NITFilterCellModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "SWGFilterValues.h"

@interface NITFilterCellModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NSNumber * identifier;
@property (nonatomic) UIColor * color;
@property (nonatomic) NSArray <UIColor *> * gradientColors;
@property (nonatomic) NSArray <NSNumber *> * gradientLocations;
@property (nonatomic) BOOL selected;

- (instancetype)initWithFilterValue:(SWGFilterValues *)filterValue;

@end
