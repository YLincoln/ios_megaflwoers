//
//  NITFilterSectionModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "SWGFilter.h"
#import "SWGFilterValues.h"

@class NITFilterCellModel, NITFilterHeaderView;

@interface NITFilterSectionModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NSString * textValue;
@property (nonatomic) NSString * cellID;
@property (nonatomic) CGFloat cellHeight;
@property (nonatomic) CGFloat headerHeight;
@property (nonatomic) NSInteger position;
@property (nonatomic) NSArray <NITFilterCellModel *> * filterCellModels;

- (instancetype)initWithFilter:(SWGFilter *)filter;

- (NITFilterHeaderView *)headerView;

@end
