//
//  NITFilterCellModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCellModel.h"
#import "NSString+Extension.h"
#import "UIColor+HEX.h"

@implementation NITFilterCellModel

- (instancetype)initWithFilterValue:(SWGFilterValues *)filterValue
{
    if (self = [super init])
    {
        self.identifier = filterValue._id;
        self.title = filterValue.name;
        self.selected = [FILTERS isActiveFilterID:self.identifier];
        
        [self updateColorIfNeedByString:filterValue.color];
    }
    
    return self;
}

- (void)updateColorIfNeedByString:(NSString *)hexString
{
    if ([hexString length] > 0)
    {
        NSRange range = [hexString rangeOfString:@","];
        switch (range.location)
        {
            case NSNotFound:
            {
                self.color = [UIColor colorFromHexString:hexString];
            }
                break;
                
            default:
            {
                NSArray * array = [hexString objectFromJSONString] ? : [[self testGradient] objectFromJSONString];
                
                if (array)
                {
                    NSMutableArray * colors = [NSMutableArray new];
                    NSMutableArray * locations = [NSMutableArray new];
                    for (NSDictionary * dict in array)
                    {
                        NSString * key = dict.allKeys.firstObject;
                        UIColor * color = [UIColor colorFromHexString:dict[key]];
                        if (color)
                        {
                            [colors addObject:(id)color.CGColor];
                            [locations addObject:@([key integerValue]/100)];
                        }
                    }
                    self.gradientColors = colors;
                    self.gradientLocations = locations;
                }
            }
                break;
        }
    }
}

#warning test data
- (NSString *)testGradient
{
    return @"[{\"0\":\"#a647ff\"},{\"35\":\"#308cc1\"},{\"65\":\"#ff8eeb\"},{\"100\":\"#37a008\"}]";
}

@end
