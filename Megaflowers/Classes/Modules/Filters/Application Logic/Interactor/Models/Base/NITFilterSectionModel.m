//
//  NITFilterSectionModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterSectionModel.h"
#import "NITFilterHeaderView.h"

@implementation NITFilterSectionModel

- (instancetype)initWithFilter:(SWGFilter *)filter
{
    if (self = [super init])
    {
        self.title = filter.name;
        self.position = filter.position.integerValue;
    }
    
    return self;
}

- (CGFloat)headerHeight
{
    return 30;
}

- (NITFilterHeaderView *)headerView
{
    return [NITFilterHeaderView headerBySection:self];
}

@end
