//
//  NITFiltersInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFiltersInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITBooleanFilterSectionModel.h"
#import "NITCheckboxFilterSectionModel.h"
#import "NITColorFilterSectionModel.h"
#import "NITRadioFilterSectionModel.h"
#import "NITFilterCellModel.h"
#import "NITRangeFilterSectionModel.h"

@interface NITFiltersInteractor ()

@property (nonatomic) NSArray * filters;

@end

@implementation NITFiltersInteractor

- (void)updateDataWithHandler:(void (^)(NSArray<NITFilterSectionModel *> *))handler
{
    weaken(self);
    [self.APIDataManager getFiltersByParentID:self.parentID withHandler:^(NSArray<SWGFilter> *result, SWGInlineResponse2001Price *price) {
        weakSelf.filters = [weakSelf modelsFromEntities:result price:price];
        handler(weakSelf.filters);
    }];
}

- (void)saveFilters
{
    [FILTERS resetFilters];

    for (NITFilterSectionModel * model in self.filters)
    {
        if ([model isKindOfClass:[NITRangeFilterSectionModel class]] )
        {
            NITRangeFilterSectionModel * priceModel = (NITRangeFilterSectionModel *)model;
            if (priceModel.selected)
            {
                [FILTERS setPriceFilter:priceModel.price];
            }
        }
        else
        {
            for (NITFilterCellModel * value in model.filterCellModels)
            {
                if (value.selected)
                {
                    [FILTERS setFilterID:value.identifier withName:value.title];
                }
            }
        }
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeFiltersNotification object:nil];
}

#pragma mark - Private
- (NSArray <NITFilterSectionModel *> *)modelsFromEntities:(NSArray <SWGFilter *> *)entities price:(SWGInlineResponse2001Price *)price
{
    NSMutableArray * items = [NSMutableArray arrayWithArray:[entities bk_map:^id(SWGFilter * obj) {
        FilterType type = (FilterType)[obj.type integerValue];
        switch (type)
        {
                case FilterTypeRadio:
                return [[NITRadioFilterSectionModel alloc] initWithFilter:obj];
                case FilterTypeCheckbox:
                return [[NITCheckboxFilterSectionModel alloc] initWithFilter:obj];
                case FilterTypeBoolean:
                return [[NITBooleanFilterSectionModel alloc] initWithFilter:obj];
                case FilterTypeRange:
                return [[NITRangeFilterSectionModel alloc] initWithFilter:obj];
                case FilterTypeColor:
                return [[NITColorFilterSectionModel alloc] initWithFilter:obj];
        }
    }]];
    
    if (price)
    {
        [items insertObject:[[NITRangeFilterSectionModel alloc] initWithPriceFilter:price] atIndex:items.count > 0 ? 1 : 0];
    }
    
    [items sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:true]]];
    
    return items;
}

@end
