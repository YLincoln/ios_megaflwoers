//
//  NITFiltersInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFiltersProtocols.h"

@interface NITFiltersInteractor : NSObject <NITFiltersInteractorInputProtocol>

@property (nonatomic, weak) id <NITFiltersInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFiltersAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFiltersLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NSNumber * parentID;

@end
