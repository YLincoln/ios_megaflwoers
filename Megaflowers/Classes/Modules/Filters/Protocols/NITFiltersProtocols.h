//
//  NITFiltersProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "SWGFilter.h"
#import "NITFilterSectionModel.h"
#import "SWGInlineResponse2001Price.h"

@protocol NITFiltersInteractorOutputProtocol;
@protocol NITFiltersInteractorInputProtocol;
@protocol NITFiltersViewProtocol;
@protocol NITFiltersPresenterProtocol;
@protocol NITFiltersLocalDataManagerInputProtocol;
@protocol NITFiltersAPIDataManagerInputProtocol;

@class NITFiltersWireFrame;

@protocol NITFiltersViewProtocol
@required
@property (nonatomic, strong) id <NITFiltersPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray<NITFilterSectionModel *> *)models;
@end

@protocol NITFiltersWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITFiltersModuleFrom:(id)fromView withParentID:(NSNumber *)parentID;
- (void)backActionFrom:(id)fromViewController;
@end

@protocol NITFiltersPresenterProtocol
@required
@property (nonatomic, weak) id <NITFiltersViewProtocol> view;
@property (nonatomic, strong) id <NITFiltersInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFiltersWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)saveFilters;
- (void)backAction;
@end

@protocol NITFiltersInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITFiltersInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITFiltersInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFiltersAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFiltersLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic) NSNumber * parentID;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)updateDataWithHandler:(void (^)(NSArray<NITFilterSectionModel *> * models))handler;
- (void)saveFilters;
@end


@protocol NITFiltersDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITFiltersAPIDataManagerInputProtocol <NITFiltersDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getFiltersByParentID:(NSNumber *)parentID withHandler:(void(^)(NSArray<SWGFilter> * result, SWGInlineResponse2001Price * price))handler;
@end

@protocol NITFiltersLocalDataManagerInputProtocol <NITFiltersDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
