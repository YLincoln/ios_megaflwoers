//
//  NITFiltersViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFiltersViewController.h"
#import "NITFilterSectionModel.h"
#import "NITFilterCell.h"
#import "NITFilterHeaderView.h"

@interface NITFiltersViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITFilterSectionModel *> * items;

@end

@implementation NITFiltersViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter updateData];
}

#pragma mark - IBAction
- (IBAction)showFilters:(id)sender
{
    [self.presenter saveFilters];
    [self.presenter backAction];
}

#pragma mark - Private
- (void)initUI
{
    [self setTitle:NSLocalizedString(@"Фильтры", nil)];
}

#pragma mark - NITFiltersPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITFilterSectionModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NITFilterSectionModel * model = self.items[section];
    return model.headerHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITFilterSectionModel * model = self.items[indexPath.section];
    return model.cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NITFilterSectionModel * model = self.items[section];
    return [model headerView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITFilterSectionModel * model = self.items[indexPath.section];
    NITFilterCell * cell = [tableView dequeueReusableCellWithIdentifier:model.cellID];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:model.cellID bundle:nil] forCellReuseIdentifier:model.cellID];
        cell = [tableView dequeueReusableCellWithIdentifier:model.cellID forIndexPath:indexPath];
    }
    
    [cell setCellModel:model withDelegate:self];
    
    return cell;
}

@end
