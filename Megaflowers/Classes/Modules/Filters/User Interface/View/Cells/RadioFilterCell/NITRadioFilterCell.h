//
//  NITRadioFilterCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCell.h"

@class NITRadioFilterSectionModel;

@interface NITRadioFilterCell : NITFilterCell

@property (nonatomic) NITRadioFilterSectionModel * model;

@end
