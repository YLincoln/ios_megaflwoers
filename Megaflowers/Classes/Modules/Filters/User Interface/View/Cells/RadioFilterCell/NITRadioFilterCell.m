//
//  NITRadioFilterCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRadioFilterCell.h"
#import "NITRadioFilterSectionModel.h"
#import "NITRadioFilterValueCell.h"

@interface NITRadioFilterCell ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITFilterCellModel *> * items;

@end

@implementation NITRadioFilterCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initUI];
}

- (void)setCellModel:(id)model withDelegate:(id)delegate
{
    _model = model;
    [self updateData];
}

- (void)setModel:(NITRadioFilterSectionModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)initUI
{
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITRadioFilterValueCell) bundle:nil] forCellReuseIdentifier:_s(NITRadioFilterValueCell)];
    [self.tableView setTableFooterView:[UIView new]];
}


- (void)updateData
{
    self.items = self.model.filterCellModels;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITRadioFilterValueCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITRadioFilterValueCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

@end
