//
//  NITFilterHeaderView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITFilterSectionModel;

@interface NITFilterHeaderView : UIView

+ (instancetype)headerBySection:(NITFilterSectionModel *)section;

@end
