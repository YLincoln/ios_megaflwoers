//
//  NITFilterHeaderView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterHeaderView.h"
#import "NITFilterSectionModel.h"
#import "NITRangeFilterSectionModel.h"

@interface NITFilterHeaderView () <NITRangeFilterSectionModelDelegate>

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * textValue;
@property (nonatomic) NITFilterSectionModel * model;

@end

@implementation NITFilterHeaderView

+ (instancetype)headerBySection:(NITFilterSectionModel *)section
{
    NITFilterHeaderView * sectionView = [[[NSBundle mainBundle] loadNibNamed:_s(NITFilterHeaderView) owner:self options:nil] firstObject];
    sectionView.title.text = section.title;
    sectionView.textValue.text = section.textValue;
    sectionView.model = section;
    
    if ([section isKindOfClass:[NITRangeFilterSectionModel class]])
    {
        NITRangeFilterSectionModel * model = (NITRangeFilterSectionModel *)section;
        model.delegate = sectionView;
    }
    
    return sectionView;
}

#pragma mark - NITRangeFilterSectionModelDelegate
- (void)didChangePriceFilterValue:(PriceValue)priceValue
{
    self.textValue.text = self.model.textValue;
}

@end
