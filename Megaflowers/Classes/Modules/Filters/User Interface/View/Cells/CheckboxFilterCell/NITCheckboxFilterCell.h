//
//  NITCheckboxFilterCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCell.h"

@class NITCheckboxFilterSectionModel;

@interface NITCheckboxFilterCell : NITFilterCell

@property (nonatomic) NITCheckboxFilterSectionModel * model;

@end
