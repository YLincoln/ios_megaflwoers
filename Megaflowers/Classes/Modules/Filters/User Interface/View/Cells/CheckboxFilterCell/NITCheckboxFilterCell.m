//
//  NITCheckboxFilterCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckboxFilterCell.h"
#import "NITCheckboxFilterValueCell.h"
#import "NITCheckboxFilterSectionModel.h"

@interface NITCheckboxFilterCell ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITFilterCellModel *> * items;

@end

@implementation NITCheckboxFilterCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initUI];
}

- (void)setCellModel:(id)model withDelegate:(id)delegate
{
    _model = model;
    [self updateData];
}

- (void)setModel:(NITCheckboxFilterSectionModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)initUI
{
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCheckboxFilterValueCell) bundle:nil] forCellReuseIdentifier:_s(NITCheckboxFilterValueCell)];
    [self.tableView setTableFooterView:[UIView new]];
}


- (void)updateData
{
    self.items = self.model.filterCellModels;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCheckboxFilterValueCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCheckboxFilterValueCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

@end

