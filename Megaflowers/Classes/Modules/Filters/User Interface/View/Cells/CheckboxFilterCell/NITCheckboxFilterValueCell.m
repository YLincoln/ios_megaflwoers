//
//  NITCheckboxFilterValueCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckboxFilterValueCell.h"

@interface NITCheckboxFilterValueCell ()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITCheckboxFilterValueCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITFilterCellModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    self.model.selected = !self.model.selected;
    [self updateCheckmark];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    [self updateCheckmark];
}

- (void)updateCheckmark
{
    self.accessoryType  = self.model.selected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}

@end
