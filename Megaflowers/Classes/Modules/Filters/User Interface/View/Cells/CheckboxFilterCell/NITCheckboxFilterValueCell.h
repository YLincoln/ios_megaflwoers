//
//  NITCheckboxFilterValueCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCellModel.h"

@interface NITCheckboxFilterValueCell : UITableViewCell

@property (nonatomic) NITFilterCellModel * model;

@end
