//
//  NITFilterCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterSectionModel.h"

@interface NITFilterCell : UITableViewCell

- (void)setCellModel:(id)model withDelegate:(id)delegate;

@end
