//
//  NITBooleanFilterCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCell.h"

@class NITBooleanFilterSectionModel;

@interface NITBooleanFilterCell : NITFilterCell

@property (nonatomic) NITBooleanFilterSectionModel * model;

@end
