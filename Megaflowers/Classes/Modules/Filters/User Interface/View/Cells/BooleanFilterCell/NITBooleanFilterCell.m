//
//  NITBooleanFilterCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBooleanFilterCell.h"
#import "NITBooleanFilterValueCell.h"
#import "NITBooleanFilterSectionModel.h"

@interface NITBooleanFilterCell ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITFilterCellModel *> * items;

@end

@implementation NITBooleanFilterCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initUI];
}

- (void)setCellModel:(id)model withDelegate:(id)delegate
{
    _model = model;
    [self updateData];
}

- (void)setModel:(NITBooleanFilterSectionModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)initUI
{
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITBooleanFilterValueCell) bundle:nil] forCellReuseIdentifier:_s(NITBooleanFilterValueCell)];
    [self.tableView setTableFooterView:[UIView new]];
}


- (void)updateData
{
    self.items = self.model.filterCellModels;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITBooleanFilterValueCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITBooleanFilterValueCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}


@end
