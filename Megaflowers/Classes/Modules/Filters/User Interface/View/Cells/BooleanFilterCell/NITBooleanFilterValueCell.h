//
//  NITBooleanFilterValueCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCellModel.h"

@interface NITBooleanFilterValueCell : UITableViewCell

@property (nonatomic) NITFilterCellModel * model;

@end
