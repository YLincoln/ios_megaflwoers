//
//  NITBooleanFilterValueCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/27/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBooleanFilterValueCell.h"

@interface NITBooleanFilterValueCell ()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITBooleanFilterValueCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITFilterCellModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    [self deselectAllCells];
    
    self.model.selected = !self.model.selected;
    self.accessoryType  = self.model.selected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    
    if (self.model.selected)
    {
        [self deselectAllCells];
        [self setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [self setAccessoryType:UITableViewCellAccessoryNone];
    }
}

- (void)deselectAllCells
{
    UITableView * mainTable = (UITableView *)self.superview.superview;
    NSArray * list = [mainTable visibleCells];
    for (NITBooleanFilterValueCell * cell in list)
    {
        if ([cell isKindOfClass:[NITBooleanFilterValueCell class]] && ![cell isEqual:self])
        {
            cell.model.selected = false;
            cell.model = cell.model;
        }
    }
}

@end
