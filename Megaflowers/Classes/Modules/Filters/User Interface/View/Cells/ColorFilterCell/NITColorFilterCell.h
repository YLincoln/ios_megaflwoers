//
//  NITColorFilterCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCell.h"

@class NITColorFilterSectionModel;

@interface NITColorFilterCell : NITFilterCell

@property (nonatomic) NITColorFilterSectionModel * model;

@end
