//
//  NITColorFilterValueCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITColorFilterValueCell.h"
#import "NITFilterCellModel.h"
#import "NSArray+BlocksKit.h"

@interface NITColorFilterValueCell ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIView * colorView;

@end

@implementation NITColorFilterValueCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITFilterCellModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    [self deselectAllCells];
    
    self.model.selected = !self.model.selected;
    self.accessoryType  = self.model.selected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    
    if ([self.model.gradientColors count] > 0)
    {
        CAGradientLayer * gradient = [CAGradientLayer layer];
        gradient.frame      = self.colorView.bounds;
        gradient.startPoint = CGPointZero;
        gradient.endPoint   = CGPointMake(1, 1);
        gradient.colors     = self.model.gradientColors;
        gradient.locations  = self.model.gradientLocations;
        
        [self.colorView.layer addSublayer:gradient];
    }
    else
    {
        if([self.model.color isEqual:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]])
        {
            self.colorView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
        }
        self.colorView.backgroundColor = self.model.color;
    }

    if (self.model.selected)
    {
        [self deselectAllCells];
        [self setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [self setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    //self.colorView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
}

- (void)deselectAllCells
{
    UITableView * mainTable = (UITableView *)self.superview.superview;
    NSArray * list = [mainTable visibleCells];
    for (NITColorFilterValueCell * cell in list)
    {
        if ([cell isKindOfClass:[NITColorFilterValueCell class]] && ![cell isEqual:self])
        {
            cell.model.selected = false;
            cell.model = cell.model;
        }
    }
}

@end
