//
//  NITRangeFilterCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRangeFilterCell.h"
#import "TTRangeSlider.h"
#import "NITRangeFilterSectionModel.h"

@interface NITRangeFilterCell ()
<
TTRangeSliderDelegate,
UITextFieldDelegate
>

@property (nonatomic) IBOutlet TTRangeSlider * rangeSlider;
@property (nonatomic) IBOutlet UITextField * minField;
@property (nonatomic) IBOutlet UITextField * maxField;

@property (nonatomic) NSInteger maxlenth;

@end

@implementation NITRangeFilterCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initUI];
    [self registerForKeyboardNotifications];
}

- (void)dealloc
{
    [self unregisterForKeyboardNotifications];
}

- (void)setCellModel:(NITRangeFilterSectionModel *)model withDelegate:(id)delegate
{
    self.model = model;
}

- (void)setModel:(NITRangeFilterSectionModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)initUI
{
    self.rangeSlider.handleImage = [UIImage imageNamed:@"slider"];
    self.rangeSlider.handleColor = [UIColor clearColor];
    self.rangeSlider.tintColor = RGB(229, 229, 229);
    self.rangeSlider.tintColorBetweenHandles = RGB(229, 229, 229);
    
    [self.minField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.maxField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)updateData
{
    self.rangeSlider.minValue = self.model.priceBorder.min;
    self.rangeSlider.maxValue = self.model.priceBorder.max;
    
    self.rangeSlider.selectedMinimum = self.model.price.min;
    self.rangeSlider.selectedMaximum = self.model.price.max;
    
    self.minField.text = @(self.model.price.min).stringValue;
    self.maxField.text = @(self.model.price.max).stringValue;
    
    self.maxlenth = [self.maxField.text length];
}

- (void)updateModelByField:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 101: // min
            //self.rangeSlider.selectedMinimum = [textField.text integerValue];
            if([textField.text integerValue] < self.model.priceBorder.min)
            {
                self.rangeSlider.selectedMinimum = self.model.priceBorder.min;
            }
            else if (([textField.text integerValue] > self.model.price.max) || ([textField.text integerValue] > self.model.priceBorder.max))
            {
                self.rangeSlider.selectedMinimum = self.model.price.max;
            }
            else
            {
                self.rangeSlider.selectedMinimum = [textField.text integerValue];
            }
            break;
            
        case 102: // max
            //self.rangeSlider.selectedMaximum = [textField.text integerValue];
            if([textField.text integerValue] > self.model.priceBorder.max)
            {
                self.rangeSlider.selectedMaximum = self.model.priceBorder.max;
            }
            else if (([textField.text integerValue] < self.model.price.min) || ([textField.text integerValue] < self.model.priceBorder.min))
            {
                self.rangeSlider.selectedMaximum = self.model.price.min;
            }
            else
            {
                self.rangeSlider.selectedMaximum = [textField.text integerValue];
            }
            break;
            
        default:
            break;
    }

    self.model.price = PriceValueMake(self.rangeSlider.selectedMinimum, self.rangeSlider.selectedMaximum);
}

#pragma mark - TTRangeSliderDelegate
- (void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum
{
    self.minField.text = @((int)selectedMinimum).stringValue;
    self.maxField.text = @((int)selectedMaximum).stringValue;
    self.model.price = PriceValueMake(selectedMinimum, selectedMaximum);
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= self.maxlenth && range.length == 0)
    {
        return false;
    }
    
    return true;
}

- (void)textFieldDidChange:(UITextField *)textField
{
    [self updateModelByField:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.minField.text = [NSString stringWithFormat:@"%ld", (NSInteger)self.rangeSlider.selectedMinimum];
    self.maxField.text = [NSString stringWithFormat:@"%ld", (NSInteger)self.rangeSlider.selectedMaximum];
    self.model.price = PriceValueMake(self.rangeSlider.selectedMinimum, self.rangeSlider.selectedMaximum);
    
    [self updateModelByField:textField];
}

#pragma mark - Keyboard
- (void)registerForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(keyboardWasShown:)
                   name:UIKeyboardWillShowNotification object:nil];
    
    [center addObserver:self
               selector:@selector(keyboardWillBeHidden:)
                   name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    UITableView * table = (UITableView *)self.superview.superview;
    table.scrollEnabled = false;
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UITableView * table = (UITableView *)self.superview.superview;
    table.scrollEnabled = true;
}

@end
