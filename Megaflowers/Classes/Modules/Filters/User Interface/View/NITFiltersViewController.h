//
//  NITFiltersViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITFiltersProtocols.h"
#import "NITBaseViewController.h"

@interface NITFiltersViewController : NITBaseViewController <NITFiltersViewProtocol>

@property (nonatomic, strong) id <NITFiltersPresenterProtocol> presenter;

@end
