//
//  NITFiltersPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFiltersPresenter.h"
#import "NITFiltersWireframe.h"

@implementation NITFiltersPresenter

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self updateData];
}

#pragma mark - Data
- (void)updateData
{
    [self.interactor updateDataWithHandler:^(NSArray<NITFilterSectionModel *> *models) {
        [self.view reloadDataByModels:models];
    }];
}

- (void)saveFilters
{
    [self.interactor saveFilters];
}

#pragma mark - Navigation
- (void)backAction
{
    [self.wireFrame backActionFrom:(NITFiltersViewController *)self.view];
}

@end
