//
//  NITFiltersPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFiltersProtocols.h"

@class NITFiltersWireFrame;

@interface NITFiltersPresenter : NITRootPresenter <NITFiltersPresenterProtocol, NITFiltersInteractorOutputProtocol>

@property (nonatomic, weak) id <NITFiltersViewProtocol> view;
@property (nonatomic, strong) id <NITFiltersInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFiltersWireFrameProtocol> wireFrame;

@end
