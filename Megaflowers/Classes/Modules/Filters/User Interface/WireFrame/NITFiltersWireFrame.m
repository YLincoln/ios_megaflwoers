//
//  NITFiltersWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFiltersWireFrame.h"

@implementation NITFiltersWireFrame

+ (id)createNITFiltersModuleWithParentID:(NSNumber *)parentID
{
    // Generating module components
    id <NITFiltersPresenterProtocol, NITFiltersInteractorOutputProtocol> presenter = [NITFiltersPresenter new];
    id <NITFiltersInteractorInputProtocol> interactor = [NITFiltersInteractor new];
    id <NITFiltersAPIDataManagerInputProtocol> APIDataManager = [NITFiltersAPIDataManager new];
    id <NITFiltersLocalDataManagerInputProtocol> localDataManager = [NITFiltersLocalDataManager new];
    id <NITFiltersWireFrameProtocol> wireFrame= [NITFiltersWireFrame new];
    id <NITFiltersViewProtocol> view = [(NITFiltersWireFrame *)wireFrame createViewControllerWithKey:_s(NITFiltersViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITFiltersModuleFrom:(UIViewController *)fromViewController withParentID:(NSNumber *)parentID
{
    NITFiltersViewController * view = [NITFiltersWireFrame createNITFiltersModuleWithParentID:parentID];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
