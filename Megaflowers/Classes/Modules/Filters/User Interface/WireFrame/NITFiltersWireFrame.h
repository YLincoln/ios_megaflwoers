//
//  NITFiltersWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFiltersProtocols.h"
#import "NITFiltersViewController.h"
#import "NITFiltersLocalDataManager.h"
#import "NITFiltersAPIDataManager.h"
#import "NITFiltersInteractor.h"
#import "NITFiltersPresenter.h"
#import "NITFiltersWireframe.h"
#import "NITRootWireframe.h"

@interface NITFiltersWireFrame : NITRootWireframe <NITFiltersWireFrameProtocol>

@end
