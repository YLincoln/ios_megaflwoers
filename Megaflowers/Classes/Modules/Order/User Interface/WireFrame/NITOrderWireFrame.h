//
//  NITOrderWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderProtocols.h"
#import "NITOrderViewController.h"
#import "NITOrderLocalDataManager.h"
#import "NITOrderAPIDataManager.h"
#import "NITOrderInteractor.h"
#import "NITOrderPresenter.h"
#import "NITOrderWireframe.h"
#import "NITRootWireframe.h"

@interface NITOrderWireFrame : NITRootWireframe <NITOrderWireFrameProtocol>

@property (nonatomic) SelectContactBlock contactBlock;

@end
