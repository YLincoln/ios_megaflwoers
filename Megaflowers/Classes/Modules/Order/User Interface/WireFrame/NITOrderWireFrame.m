//
//  NITOrderWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderWireFrame.h"
#import <ContactsUI/ContactsUI.h>
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>
#import "NITSalonsWireFrame.h"
#import "NITPaymentOrderWireFrame.h"

@interface NITOrderWireFrame () <CNContactPickerDelegate, ABPeoplePickerNavigationControllerDelegate>

@end

@implementation NITOrderWireFrame

+ (id)createNITOrderModuleWithDeliveryType:(DeliveryMethodType)deliveryType
{
    // Generating module components
    id <NITOrderPresenterProtocol, NITOrderInteractorOutputProtocol> presenter = [NITOrderPresenter new];
    id <NITOrderInteractorInputProtocol> interactor = [NITOrderInteractor new];
    id <NITOrderAPIDataManagerInputProtocol> APIDataManager = [NITOrderAPIDataManager new];
    id <NITOrderLocalDataManagerInputProtocol> localDataManager = [NITOrderLocalDataManager new];
    id <NITOrderWireFrameProtocol> wireFrame= [NITOrderWireFrame new];
    id <NITOrderViewProtocol> view = [(NITOrderWireFrame *)wireFrame createViewControllerWithKey:_s(NITOrderViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    presenter.deliveryType = deliveryType;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITOrderModuleFrom:(UIViewController *)fromViewController withDeliveryType:(DeliveryMethodType)deliveryType
{
    NITOrderViewController * view = [NITOrderWireFrame createNITOrderModuleWithDeliveryType:deliveryType];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

- (void)selectSalonAddressFrom:(id)fromView withDelegate:(id)delegate
{
    [NITSalonsWireFrame selectAddressFrom:fromView withDelegate:delegate];
}

- (void)showPaymentOrderFrom:(UIViewController *)fromViewController forOrderID:(NSNumber *)orderID
{
    [NITPaymentOrderWireFrame presentNITPaymentOrderModuleFrom:fromViewController forOrderID:orderID];
}

- (NSArray *)createPickupSections
{
    NITOrderPostcardSection * postcardSection = [self createViewControllerWithKey:_s(NITOrderPostcardSection) storyboardType:StoryboardTypeBasket];
    postcardSection.title = NSLocalizedString(@"Открытка", nil);
    
    NITOrderDeliverySection * orderSection = [self createViewControllerWithKey:_s(NITOrderDeliverySection) storyboardType:StoryboardTypeBasket];
    orderSection.title = NSLocalizedString(@"Доставка", nil);
    
    NITOrderAboutSection * aboutSection = [self createViewControllerWithKey:_s(NITOrderAboutSection) storyboardType:StoryboardTypeBasket];
    aboutSection.title = NSLocalizedString(@"О себе", nil);

    return @[postcardSection, orderSection, aboutSection];
}

- (NSArray *)createCourierSections
{
    NITOrderPostcardSection * postcardSection = [self createViewControllerWithKey:_s(NITOrderPostcardSection) storyboardType:StoryboardTypeBasket];
    postcardSection.title = NSLocalizedString(@"Открытка", nil);
    
    NITOrderWhomSection * whomSection = [self createViewControllerWithKey:_s(NITOrderWhomSection) storyboardType:StoryboardTypeBasket];
    whomSection.title = NSLocalizedString(@"Кому", nil);
    
    NITOrderWhenSection * whenSection = [self createViewControllerWithKey:_s(NITOrderWhenSection) storyboardType:StoryboardTypeBasket];
    whenSection.title = NSLocalizedString(@"Когда", nil);
    
    NITOrderFromSection * fromSection = [self createViewControllerWithKey:_s(NITOrderFromSection) storyboardType:StoryboardTypeBasket];
    fromSection.title = NSLocalizedString(@"От кого", nil);
    
    return @[postcardSection, whomSection, whenSection, fromSection];
}

#pragma mark - Adress book
- (void)selectPhoneNumberFrom:(UIViewController *)fromViewController withHandler:(SelectContactBlock)handler
{
    self.contactBlock = handler;
    
    if (IOS_8)
    {
        ABPeoplePickerNavigationController * peoplePicker = [ABPeoplePickerNavigationController new];
        peoplePicker.displayedProperties = @[@(kABPersonPhoneProperty)];
        peoplePicker.peoplePickerDelegate = self;
        [fromViewController.navigationController presentViewController:peoplePicker animated:true completion:nil];
    }
    else
    {
        CNContactPickerViewController * contactPicker = [[CNContactPickerViewController alloc] init];
        contactPicker.delegate = self;
        
        NSArray * propertyKeys = @[CNContactPhoneNumbersKey, CNContactGivenNameKey, CNContactFamilyNameKey, CNContactOrganizationNameKey, CNContactPostalAddressesKey];
        NSPredicate * enablePredicate = [NSPredicate predicateWithFormat:@"(phoneNumbers.@count > 0)"];
        NSPredicate * contactSelectionPredicate = [NSPredicate predicateWithFormat:@"phoneNumbers.@count == 1"];
        
        contactPicker.displayedPropertyKeys = propertyKeys;
        contactPicker.predicateForEnablingContact = enablePredicate;
        contactPicker.predicateForSelectionOfContact = contactSelectionPredicate;
        
        [fromViewController presentViewController:contactPicker animated:true completion:nil];
    }
}

#pragma mark CNContactPickerDelegate methods
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSArray <CNLabeledValue<CNPhoneNumber *> *> * phoneNumbers = contact.phoneNumbers;
    CNLabeledValue<CNPhoneNumber *> * firstPhone = [phoneNumbers firstObject];
    NSString * phone = [firstPhone.value stringValue];
    
    [picker dismissViewControllerAnimated:true completion:^{
        
        if (self.contactBlock)
        {
            self.contactBlock(phone);
        }
    }];
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty
{
    NSString * phone = [contactProperty.value stringValue];

    [picker dismissViewControllerAnimated:true completion:^{
        
        if (self.contactBlock)
        {
            self.contactBlock(phone);
        }
    }];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
    [peoplePicker dismissViewControllerAnimated:YES completion:^{}];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    NSString * phone;
    if (property == kABPersonPhoneProperty)
    {
        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, property);
        if (ABMultiValueGetCount(phoneNumbers) > 0)
        {
            CFIndex index = ABMultiValueGetIndexForIdentifier(phoneNumbers, identifier);
            phone = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, index);
        }
    }
    
    if (self.contactBlock)
    {
        self.contactBlock(phone);
    }
}

@end
