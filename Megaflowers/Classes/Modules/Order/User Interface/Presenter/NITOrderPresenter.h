//
//  NITOrderPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrderProtocols.h"

@class NITOrderWireFrame;

@interface NITOrderPresenter : NITRootPresenter <NITOrderPresenterProtocol, NITOrderInteractorOutputProtocol>

@property (nonatomic, weak) id <NITOrderViewProtocol> view;
@property (nonatomic, strong) id <NITOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITOrderWireFrameProtocol> wireFrame;
@property (nonatomic) DeliveryMethodType deliveryType;
@property (nonatomic) NITOrderModel * model;

@end
