//
//  NITOrderPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderPresenter.h"
#import "NITOrderWireframe.h"
#import "APAddressBook.h"
#import "APContact.h"
#import "NSArray+BlocksKit.h"
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>

@implementation NITOrderPresenter

#pragma mark - Data
- (void)initData
{
    self.model = [NITOrderModel new];
    
    if ([USER isAutorize])
    {
        if (USER.name)   self.model.senderName  = USER.name;
        if (USER.phone)  self.model.senderPhone = USER.phone;
        if (USER.email)  self.model.senderEmail = USER.email;
    }

    NSArray * sections;
    
    switch (self.deliveryType)
    {
        case DeliveryMethodTypeCourier:
            sections = [self.wireFrame createCourierSections];
            break;
            
        case DeliveryMethodTypePickup:
            sections = [self.wireFrame createPickupSections];
            break;
    }

    NSArray <NSDate *> * dates = [self getDates];
    for (NITOrderBaseSection * section in sections)
    {
        section.dates = dates;
        section.model = self.model;
        section.parent = (NITOrderViewController *)self.view;
    }
    
    [self.view loadOrderSections:sections];
}

- (void)showPaymentOrder
{
    weaken(self);
    [self.interactor createNewOrderWithHandler:^(BOOL success, NSString *error, NSNumber *orderID) {
        
        if (success)
        {
           [weakSelf.wireFrame showPaymentOrderFrom:weakSelf.view forOrderID:orderID];
        }
        else
        {
            [weakSelf showCreateOrderError:error];
        }
    }];
}

- (void)showCreateOrderError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Повторить", nil) actionBlock:^{
        [weakSelf showPaymentOrder];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:error actionButtons:@[action1] cancelButton:cancel];
}

- (NSMutableArray <NSDate *> *)getDates
{
    NSMutableArray <NSDate *> * dates = [NSMutableArray new];
    NSDate * currentDate = [NSDate date];
    
    while (currentDate.year < [NSDate date].year + 2)
    {
        [dates addObject:currentDate];
        currentDate = [currentDate dateByAddingDays:1];
    }

    return dates;
}

- (void)validateOrderProperties:(NSArray <NSString *> *)properties withHandler:(void(^)(BOOL success, NSError * error))handler
{
    [HELPER logString:[NSString stringWithFormat:@"Validate order fields: %@", properties]];
    [self.interactor validateOrderProperties:properties withHandler:^(BOOL success, NSError *error) {
        
        if (handler) handler(success, error);
        if (error) [HELPER logString:[NSString stringWithFormat:@"❗️Order validation error: %@", error.errorFields]];
    }];
}

#pragma mark - NITOrderPostcardSection
- (void)updatePostcardDataWithHandler:(void (^)(NSArray<NITPostcardItem *> *))handler
{
     [self.interactor updatePostcardDataWithHandler:^(NSArray<NITPostcardItem *> *models) {
         handler(models);
     }];
}

#pragma mark - NITOrderDeliverySection
- (void)selectSalonAddressFrom:(id)fromView
{
    [self.wireFrame selectSalonAddressFrom:self.view withDelegate:fromView];
}

#pragma mark - NITOrderWhenSection
- (void)updateIntervalsForDate:(NSDate *)date withHandler:(void (^)(NSArray<NITIntervalItem *> *))handler
{
    [self.interactor updateOrderTimeListForDate:date ? : [NSDate date] withHandler:^(NSArray<NITIntervalItem *> *result, NSString *error) {
        handler(result);
        if (error.length > 0)
        {
            [self showError:error fromView:self.view];
        }
    }];
}

- (NSInteger)currentIndexForDate:(NSDate *)date fromArray:(NSArray *)array
{
    NSDate * item = [array bk_select:^BOOL(NSDate *obj) {
        return [obj isEqualToDay:date];
    }].firstObject;
    
    return [array indexOfObject:item];
}

- (NSInteger)currentIndexForInterval:(NITIntervalItem *)interval fromArray:(NSArray *)array
{
    NITIntervalItem * item = [array bk_select:^BOOL(NITIntervalItem *obj) {
        return [obj.identifier isEqualToNumber:interval.identifier];
    }].firstObject;
    
    return [array indexOfObject:item];
}

#pragma mark - NITOrderWhomSection
- (void)selectPhoneNumberWithHandler:(void(^)(NSString * phone, NSString * name, NSString * address))handler
{
    __block BOOL access = [APAddressBook access] == APAddressBookAccessGranted;
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    
    if (!access)
    {
        APAddressBook * addressBook = [[APAddressBook alloc] init];
        [addressBook requestAccessOnQueue:dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) completion:^(BOOL granted, NSError * _Nullable error) {
            access = granted;
            dispatch_group_leave(group);
        }];
    }
    else
    {
        dispatch_group_leave(group);
    }
    
    weaken(self);
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        [weakSelf.wireFrame selectPhoneNumberFrom:self.view withHandler:^(NSString * phoneNumber) {
            
            APAddressBook * addressBook = [[APAddressBook alloc] init];
            addressBook.fieldsMask = APContactFieldAll;
            [addressBook loadContacts:^(NSArray<APContact *> * _Nullable contacts, NSError * _Nullable error) {
                
                APContact * contact = [[contacts bk_select:^BOOL(APContact * obj) {
                    return [[obj.phones bk_map:^id(APPhone * obj) {
                        return obj.number;
                    }] bk_select:^BOOL(NSString * obj) {
                        return [obj isEqualToString:phoneNumber];
                    }].count > 0;
                    
                }] firstObject];
                
                NSString * phone = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
                NSString * name = contact.name.compositeName;
                NSString * addressString = @"";
                
                APAddress * address = contact.addresses.firstObject;
                if (address)
                {
                    if (address.street.length > 0)
                    {
                        addressString = [addressString stringByAppendingString:addressString.length == 0 ? @"" : @", "];
                        addressString = [addressString stringByAppendingString:address.street];
                    }
                    
                    if (address.city.length > 0)
                    {
                        addressString = [addressString stringByAppendingString:addressString.length == 0 ? @"" : @", "];
                        addressString = [addressString stringByAppendingString:address.city];
                    }
                    
                    if (address.country.length > 0)
                    {
                        addressString = [addressString stringByAppendingString:addressString.length == 0 ? @"" : @", "];
                        addressString = [addressString stringByAppendingString:address.country];
                    }
                    
                    addressString = [addressString stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
                }
                
                handler(phone, name, addressString);
            }];
        }];
    });
}

@end
