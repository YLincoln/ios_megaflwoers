//
//  NITOrderViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderViewController.h"
#import "NITPostcardCell.h"
#import "NITPlaceholderTextView.h"
#import "CAPSPageMenu.h"
#import "NITOrderBaseSection.h"

@interface NITOrderViewController ()

@property (nonatomic) IBOutlet UIView *containerView;
@property (nonatomic) CAPSPageMenu * pageMenu;
@property (nonatomic) NSArray * sections;

@end

@implementation NITOrderViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:true];
}

#pragma mark - IBAction
- (IBAction)nextAction:(id)sender
{
    NSInteger currentIndex = self.pageMenu.currentPageIndex;
    
    NITOrderBaseSection * section = self.sections[currentIndex];
    
    weaken(self);
    [section validateSectionWithHandler:^(BOOL success) {
       
        if (success)
        {
            if (currentIndex < weakSelf.pageMenu.controllerArray.count - 1)
            {
                [weakSelf.pageMenu moveToPage:currentIndex + 1];
                weakSelf.pageMenu.maxEnabledIndex = currentIndex + 1;
            }
            else
            {
                [weakSelf.presenter showPaymentOrder];
            }
        }
    }];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Оформление заказа", nil);
}

#pragma mark - NITOrderPresenterProtocol
- (void)loadOrderSections:(NSArray *)section
{
    self.sections = section;
    self.pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:section frame:self.containerView.bounds options:nil];

    [self.pageMenu.controllerScrollView setScrollEnabled:false];
    [self.containerView addSubview:self.pageMenu.view];
}

@end
