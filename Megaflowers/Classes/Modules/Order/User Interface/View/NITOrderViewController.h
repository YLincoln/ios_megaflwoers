//
//  NITOrderViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITOrderProtocols.h"

@interface NITOrderViewController : NITBaseViewController <NITOrderViewProtocol>

@property (nonatomic, strong) id <NITOrderPresenterProtocol> presenter;

@end
