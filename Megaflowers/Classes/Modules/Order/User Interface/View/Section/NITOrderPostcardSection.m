//
//  NITOrderPostcardSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderPostcardSection.h"
#import "NITPlaceholderTextView.h"
#import "NITPostcardCell.h"
#import "NITPostcardItem.h"
#import "NITOrderViewController.h"

@interface NITOrderPostcardSection ()
<
UITextViewDelegate,
UICollectionViewDelegate,
UICollectionViewDataSource
>

@property (nonatomic) IBOutlet UICollectionView * collectionView;
@property (nonatomic) IBOutlet NITPlaceholderTextView * congratulationTextView;
@property (nonatomic) IBOutlet UIButton * postcardBtn;

@property (nonatomic) NSArray <NITPostcardItem *> * postcardItems;

@end

@implementation NITOrderPostcardSection

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

#pragma mark - IBAction
- (IBAction)postcardSelect:(id)sender
{
    self.model.postcardEnabled = !self.model.postcardEnabled;
    [self updatePostcardSection];
}

#pragma mark - Public
- (void)validateSectionWithHandler:(void (^)(BOOL success))handler
{
    NSArray * properties = @[keyPath(SWGOrder.postcardText)];
    [self.parent.presenter validateOrderProperties:properties withHandler:^(BOOL success, NSError *error) {
        [self highlightFieldsIfNeed:error];
        if (handler) handler(success);
    }];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Открытка", nil);
    
    [self setKeyboardActiv:true];
    
    // collection view
    [self.collectionView registerNib:[UINib nibWithNibName:_s(NITPostcardCell) bundle:nil] forCellWithReuseIdentifier:_s(NITPostcardCell)];
    self.collectionView.panGestureRecognizer.cancelsTouchesInView = true;
    
    [self updateUI];
    [self updatePostcardSection];
}

- (void)updateUI
{
    self.congratulationTextView.text = self.model.postcardText;
    
    for (int i = 0; i < self.postcardItems.count; i++)
    {
        NITPostcardItem * item = self.postcardItems[i];
        if (self.model.postcardID && [item.identifier isEqualToNumber:self.model.postcardID])
        {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]
                                        atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                                animated:false];
            break;
        }
    }
}

- (void)highlightFieldsIfNeed:(NSError *)error
{
    if (error)
    {
        BOOL needShowError = false;
        
        for (NSString * key in error.errorFields)
        {
            if ([key isEqualToString:@"postcard_text"])
            {
                self.congratulationTextView.textColor = RGB(217, 67, 67);
                self.congratulationTextView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.congratulationTextView.placeholder
                                                                                                    attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.postcardText.length > 0)
                {
                    needShowError = true;
                }
            }
        }
        
        if (needShowError)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:self.parent withTitle:error.interfaceDescription actionButtons:nil cancelButton:cancel];
        }
    }
}

- (void)updatePostcardSection
{
    [self.postcardBtn setImage:[UIImage imageNamed:self.model.postcardEnabled ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    
    self.collectionView.alpha = !self.model.postcardEnabled ? 1.0 : 0.6;
    self.collectionView.userInteractionEnabled = !self.model.postcardEnabled;
    self.congratulationTextView.userInteractionEnabled = !self.model.postcardEnabled;
}

- (void)initData
{
    weaken(self);
    [self.parent.presenter updatePostcardDataWithHandler:^(NSArray<NITPostcardItem *> *models) {
        
        weakSelf.postcardItems = models;
        
        [weakSelf.collectionView reloadData];
        [weakSelf updateUI];
    }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.postcardItems count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NITPostcardCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITPostcardCell) forIndexPath:indexPath];
    cell.model = self.postcardItems[indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(NITPostcardCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.count = [self.postcardItems count];
    cell.index = indexPath.row;
    
    NITPostcardItem * item = self.postcardItems[indexPath.row];
    self.model.postcardID = item.identifier;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(ViewWidth(self.view), ViewHeight(self.collectionView));
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView;
{
    self.model.postcardText = textView.text;
    self.congratulationTextView.textColor = RGB(10, 88, 43);
    self.congratulationTextView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.congratulationTextView.placeholder
                                                                                        attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

@end
