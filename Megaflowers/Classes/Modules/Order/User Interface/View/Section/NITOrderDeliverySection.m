//
//  NITOrderDeliverySection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderDeliverySection.h"
#import "NITOrderViewController.h"
#import "NITSalonsProtocols.h"
#import "NSArray+BlocksKit.h"

@interface NITOrderDeliverySection ()
<
NITSalonsDelegate
>

@property (nonatomic) IBOutlet UILabel * deliveryAddress;
@property (nonatomic) IBOutlet UITextField * deliveryDate;
@property (nonatomic) IBOutlet UITextField * deliveryTime;
@property (nonatomic) IBOutlet UIButton * selectAddressBtn;
@property (nonatomic) IBOutlet UIButton * selectDateBtn;
@property (nonatomic) IBOutlet UIButton * selectTimeBtn;
@property (nonatomic) IBOutlet UIImageView * dateImg;
@property (nonatomic) IBOutlet UIImageView * timeImg;
@property (nonatomic) IBOutlet UIPickerView * datePicker;
@property (nonatomic) IBOutlet UIPickerView * timePicker;
@property (nonatomic) IBOutlet NSLayoutConstraint * datePickerH;
@property (nonatomic) IBOutlet NSLayoutConstraint * timePickerH;

@property (nonatomic) NSArray <NITIntervalItem *> * intervalItems;

@end

@implementation NITOrderDeliverySection

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - Public
- (void)validateSectionWithHandler:(void (^)(BOOL success))handler
{
    NSArray * properties = @[keyPath(SWGOrder.receiverAddress),
                             keyPath(SWGOrder.deliveryDate),
                             keyPath(SWGOrder.deliveryTime),
                             keyPath(SWGOrder.deliverySalonId)];
    
    [self.parent.presenter validateOrderProperties:properties withHandler:^(BOOL success, NSError *error) {
        [self highlightFieldsIfNeed:error];
        if (handler) handler(success);
    }];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Доставка", nil);
    
    self.deliveryAddress.text = NSLocalizedString(@"Адрес салона доставки", nil);
    self.deliveryAddress.textColor = RGB(164, 183, 171);
    self.deliveryDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.deliveryDate.placeholder
                                                                              attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.deliveryTime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.deliveryTime.placeholder
                                                                              attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    [self.datePicker setValue:RGB(10, 88, 43) forKeyPath:@"textColor"];
    [self.timePicker setValue:RGB(10, 88, 43) forKeyPath:@"textColor"];
    
    [self updateUI];
}

- (void)updateUI
{
    if (self.model.receiverAddress.length > 0)
    {
        self.deliveryAddress.text = self.model.receiverAddress;
        self.deliveryAddress.textColor = RGB(10, 88,43);
    }
    else
    {
        self.deliveryAddress.text = NSLocalizedString(@"Адрес салона доставки", nil);
        self.deliveryAddress.textColor = RGB(164, 183, 171);
    }
    
    self.deliveryDate.text = [self.model.deliveryDate presentString];
    self.deliveryTime.text = [self.model.deliveryTime title];
}

- (void)highlightFieldsIfNeed:(NSError *)error
{
    if (error)
    {
        BOOL needShowError = false;
        
        for (NSString * key in error.errorFields)
        {
            if ([key isEqualToString:@"receiver_address"] ||
                [key isEqualToString:@"delivery_salon_id"])
            {
                self.deliveryAddress.textColor = RGB(217, 67, 67);
                if (self.model.receiverAddress.length > 0)
                {
                    needShowError = true;
                }
            }
            else if ([key isEqualToString:@"delivery_date"])
            {
                self.deliveryDate.textColor = RGB(217, 67, 67);
                self.deliveryDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.deliveryDate.placeholder
                                                                                          attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.deliveryDate)
                {
                    needShowError = true;
                }
            }
            else if ([key isEqualToString:@"delivery_time"])
            {
                self.deliveryTime.textColor = RGB(217, 67, 67);
                self.deliveryTime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.deliveryDate.placeholder
                                                                                          attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.deliveryTime)
                {
                    needShowError = true;
                }
            }
        }
        
        if (needShowError)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:self.parent withTitle:error.interfaceDescription actionButtons:nil cancelButton:cancel];
        }
    }
}

#pragma mark - IBAction
- (IBAction)selectAddressAction:(id)sender
{
    self.timePicker.hidden = true;
    self.datePicker.hidden = true;
    
    self.timePickerH.constant = 0;
    self.datePickerH.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self updateTimeIcons];
    [self.parent.presenter selectSalonAddressFrom:self];
}

- (IBAction)selectDateAction:(id)sender
{
    self.timePicker.hidden = true;
    self.datePicker.hidden = !self.datePicker.hidden;
    
    self.datePickerH.constant = self.datePicker.hidden ? 0 : 216;
    self.timePickerH.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    NSDate * selectedDate = self.model.deliveryDate ? self.model.deliveryDate : [self.dates firstObject];
    NSInteger selectedIndex = [self.parent.presenter currentIndexForDate:selectedDate fromArray:self.dates];
    
    [self.datePicker selectRow:selectedIndex inComponent:0 animated:true];
    [self updateDateByValue:selectedDate];
    [self updateTimeByValue:nil];
    [self updateTimeIcons];
}

- (IBAction)selectTimeAction:(id)sender
{
    if (!self.datePicker.hidden)
    {
        [self selectDateAction:nil];
    }
    
    if (!self.timePicker.hidden)
    {
        [self selectTime];
    }
    else
    {
        [self reloadIntervalsWitComplition:^{
            if (self.intervalItems.count > 0)
            {
                [self selectTime];
            }
        }];
    }
}

#pragma mark - Private
- (void)updateTimeIcons
{
    self.dateImg.image = [UIImage imageNamed:self.datePicker.hidden ? @"ic_down_cn" :  @"ic_up_cn"];
    self.timeImg.image = [UIImage imageNamed:self.timePicker.hidden ? @"ic_down_cn" :  @"ic_up_cn"];
}

- (void)reloadIntervalsWitComplition:(void(^)())complition
{
    weaken(self);
    [self.parent.presenter updateIntervalsForDate:self.model.deliveryDate withHandler:^(NSArray<NITIntervalItem *> *items) {
        weakSelf.intervalItems = items;
        [weakSelf.timePicker reloadAllComponents];
        if (complition) complition();
    }];
}

- (void)updateDateByValue:(NSDate *)value
{
    self.model.deliveryDate = value;
    self.deliveryDate.text =  value ? [value presentString] : @"";
}

- (void)updateTimeByValue:(NITIntervalItem *)value
{
    self.model.deliveryTime = value;
    self.deliveryTime.text = value.title;
}

- (void)selectTime
{
    self.datePicker.hidden = true;
    self.timePicker.hidden = !self.timePicker.hidden;
    
    self.timePickerH.constant = self.timePicker.hidden ? 0 : 216;
    self.datePickerH.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    NITIntervalItem * selectedInterval = self.model.deliveryTime ? self.model.deliveryTime : self.intervalItems.firstObject;
    NSInteger selectedIndex = [self.parent.presenter currentIndexForInterval:selectedInterval fromArray:self.intervalItems];
    
    [self.timePicker selectRow:selectedIndex inComponent:0 animated:true];
    [self updateTimeByValue:selectedInterval];
    [self updateTimeIcons];
}

#pragma mark - NITSalonsDelegate
- (void)didSelectAddress:(NITAddressModel *)model
{
    self.model.deliverySalonId = model.identifier;
    self.model.receiverAddress = model.address;
    self.deliveryAddress.text = model.address;
    self.deliveryAddress.textColor = RGB(10, 88,43);
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.timePicker])
    {
        return self.intervalItems.count;
    }
    else
    {
        return self.dates.count;
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([thePickerView isEqual:self.timePicker])
    {
        [self updateTimeByValue:self.intervalItems[row]];
    
        if (!self.model.deliveryDate)
        {
            [self updateDateByValue:[NSDate date]];
        }
    }
    else
    {
        [self updateDateByValue:self.dates[row]];
        [self updateTimeByValue:nil];
    }
}

- (UIView *)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)rview
{
    UILabel * label = [UILabel new];
    label.textColor = RGB(10, 88,43);
    label.textAlignment = NSTextAlignmentCenter;
    
    if ([thePickerView isEqual:self.timePicker])
    {
        label.text = [self.intervalItems[row] title];
    }
    else
    {
        label.text = [self.dates[row] presentString];
    }
    
    return label;
}

@end
