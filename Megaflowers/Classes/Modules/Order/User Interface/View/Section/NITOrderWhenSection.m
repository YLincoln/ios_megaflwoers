//
//  NITOrderWhenSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderWhenSection.h"
#import "NITOrderViewController.h"

@interface NITOrderWhenSection ()

@property (nonatomic) IBOutlet UITextField * whenDate;
@property (nonatomic) IBOutlet UITextField * whenTime;
@property (nonatomic) IBOutlet UIButton * selectDateBtn;
@property (nonatomic) IBOutlet UIButton * selectTimeBtn;
@property (nonatomic) IBOutlet UIImageView * dateImg;
@property (nonatomic) IBOutlet UIImageView * timeImg;
@property (nonatomic) IBOutlet UIPickerView * datePicker;
@property (nonatomic) IBOutlet UIPickerView * timePicker;
@property (nonatomic) IBOutlet UIButton * agreeWithRecipientBtn;
@property (nonatomic) IBOutlet UIButton * makePhotoRecipientAtTimeOfDeliveryBtn;
@property (nonatomic) IBOutlet UIButton * canLeaveBunchRelativesBtn;
@property (nonatomic) IBOutlet UIButton * deliverAnonymouslyBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * datePickerH;
@property (nonatomic) IBOutlet NSLayoutConstraint * timePickerH;

@property (nonatomic) NSArray <NITIntervalItem *> * intervalItems;

@end

@implementation NITOrderWhenSection

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - Public
- (void)validateSectionWithHandler:(void (^)(BOOL success))handler
{
    NSArray * properties = @[keyPath(SWGOrder.deliveryDate),
                             keyPath(SWGOrder.deliveryTime),
                             keyPath(SWGOrder.takePhoto),
                             keyPath(SWGOrder.leaveNeighbors),
                             keyPath(SWGOrder.notifyAboutDelivery)];
    
    [self.parent.presenter validateOrderProperties:properties withHandler:^(BOOL success, NSError *error) {
        [self highlightFieldsIfNeed:error];
        if (handler) handler(success);
    }];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Когда", nil);
    
    self.whenDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.whenDate.placeholder
                                                                              attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.whenTime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.whenTime.placeholder
                                                                              attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    [self.datePicker setValue:RGB(10, 88, 43) forKeyPath:@"textColor"];
    [self.timePicker setValue:RGB(10, 88, 43) forKeyPath:@"textColor"];
    
    [self updateUI];
    [self reloadIntervals];
}

- (void)updateUI
{
    self.whenDate.text = [self.model.deliveryDate presentString];
    self.whenTime.text = [self.model.deliveryTime title];
    
    [self updateButtons];
}

#pragma mark - IBAction
- (IBAction)selectDateAction:(id)sender
{
    self.timePicker.hidden = true;
    self.datePicker.hidden = !self.datePicker.hidden;
    
    self.datePickerH.constant = self.datePicker.hidden ? 0 : 216;
    self.timePickerH.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    NSDate * selectedDate = self.model.deliveryDate ? self.model.deliveryDate : [self.dates firstObject];
    NSInteger selectedIndex = [self.parent.presenter currentIndexForDate:selectedDate fromArray:self.dates];
    
    [self.datePicker selectRow:selectedIndex inComponent:0 animated:true];
    [self updateDateByValue:selectedDate];
    [self updateTimeByValue:nil];
    [self reloadIntervals];
    [self updateTimeIcons];
}

- (IBAction)selectTimeAction:(id)sender
{    
    self.datePicker.hidden = true;
    self.timePicker.hidden = !self.timePicker.hidden;
    
    self.timePickerH.constant = self.timePicker.hidden ? 0 : 216;
    self.datePickerH.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];

    NITIntervalItem * selectedInterval = self.model.deliveryTime ? self.model.deliveryTime : self.intervalItems.firstObject;
    NSInteger selectedIndex = [self.parent.presenter currentIndexForInterval:selectedInterval fromArray:self.intervalItems];
    
    [self.timePicker selectRow:selectedIndex inComponent:0 animated:true];
    [self updateTimeByValue:selectedInterval];
    [self updateTimeIcons];
}

- (IBAction)agreeWithRecipientAction:(id)sender
{
    self.model.agreeWithRecipient = !self.model.agreeWithRecipient;
    [self updateButtons];
}

- (IBAction)makePhotoRecipientAtTimeOfDeliveryAction:(id)sender
{
    self.model.takePhoto = !self.model.takePhoto;
    [self updateButtons];
}

- (IBAction)canLeaveBunchRelativesAction:(id)sender
{
    self.model.leaveNeighbors = !self.model.leaveNeighbors;
    [self updateButtons];
}

- (IBAction)deliverAnonymouslyAction:(id)sender
{
    self.model.deliverAnonymously = !self.model.deliverAnonymously;
    [self updateButtons];
}


#pragma mark - Private
- (void)updateTimeIcons
{
    self.dateImg.image = [UIImage imageNamed:self.datePicker.hidden ? @"ic_down_cn" :  @"ic_up_cn"];
    self.timeImg.image = [UIImage imageNamed:self.timePicker.hidden ? @"ic_down_cn" :  @"ic_up_cn"];
}

- (void)updateButtons
{
    [self.agreeWithRecipientBtn setImage:[UIImage imageNamed:self.model.agreeWithRecipient ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    [self.makePhotoRecipientAtTimeOfDeliveryBtn setImage:[UIImage imageNamed:self.model.takePhoto ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    [self.canLeaveBunchRelativesBtn setImage:[UIImage imageNamed:self.model.leaveNeighbors ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    [self.deliverAnonymouslyBtn setImage:[UIImage imageNamed:self.model.deliverAnonymously ? @"check" : @"check_ar"] forState:UIControlStateNormal];
}

- (void)reloadIntervals
{
    weaken(self);
    [self.parent.presenter updateIntervalsForDate:self.model.deliveryDate withHandler:^(NSArray<NITIntervalItem *> *items) {
        weakSelf.intervalItems = items;
        [weakSelf.timePicker reloadAllComponents];
    }];
}

- (void)updateDateByValue:(NSDate *)value
{
    self.model.deliveryDate = value;
    self.whenDate.text =  value ? [value presentString] : @"";
    self.whenTime.textColor = RGB(10, 88,43);
    self.whenTime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.whenTime.placeholder
                                                                          attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

- (void)updateTimeByValue:(NITIntervalItem *)value
{
    self.model.deliveryTime = value;
    self.whenTime.text = value.title;
    self.whenTime.textColor = RGB(10, 88,43);
    self.whenTime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.whenTime.placeholder
                                                                          attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

- (void)highlightFieldsIfNeed:(NSError *)error
{
    if (error)
    {
        BOOL needShowError = false;
        
        for (NSString * key in error.errorFields)
        {
            if ([key isEqualToString:@"delivery_date"])
            {
                self.whenDate.textColor = RGB(217, 67, 67);
                self.whenDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.whenDate.placeholder
                                                                                      attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.deliveryDate)
                {
                    needShowError = true;
                }
            }
            else if ([key isEqualToString:@"delivery_time"])
            {
                self.whenTime.textColor = RGB(217, 67, 67);
                self.whenTime.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.whenTime.placeholder
                                                                                      attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.deliveryTime)
                {
                    needShowError = true;
                }
            }
        }
        
        if (needShowError)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:self.parent withTitle:error.interfaceDescription actionButtons:nil cancelButton:cancel];
        }
    }
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.timePicker])
    {
        return self.intervalItems.count;
    }
    else
    {
        return self.dates.count;
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([thePickerView isEqual:self.timePicker])
    {
        [self updateTimeByValue:self.intervalItems[row]];
        
        if (!self.model.deliveryDate)
        {
            [self updateDateByValue:[NSDate date]];
        }
    }
    else
    {
        [self updateDateByValue:self.dates[row]];
        [self updateTimeByValue:nil];
    }
}

- (UIView *)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)rview
{
    UILabel * label = [UILabel new];
    label.textColor = RGB(10, 88,43);
    label.textAlignment = NSTextAlignmentCenter;
    
    if ([thePickerView isEqual:self.timePicker])
    {
        label.text = [self.intervalItems[row] title];
    }
    else
    {
        label.text = [self.dates[row] presentString];
    }
    
    return label;
}

@end
