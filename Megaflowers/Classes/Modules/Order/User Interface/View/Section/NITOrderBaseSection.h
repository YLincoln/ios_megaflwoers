//
//  NITOrderBaseSection.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITOrderViewController;

#import "NITBaseViewController.h"
#import "NITOrderModel.h"
#import "SWGOrder.h"

@interface NITOrderBaseSection : NITBaseViewController

@property (nonatomic) NITOrderModel * model;
@property (nonatomic) NITOrderViewController * parent;
@property (nonatomic) NSArray <NSDate *> * dates;

- (void)validateSectionWithHandler:(void(^)(BOOL success))handler;

@end
