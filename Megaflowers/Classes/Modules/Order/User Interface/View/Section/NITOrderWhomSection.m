//
//  NITOrderWhomSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderWhomSection.h"
#import "NITOrderViewController.h"
#import "AKNumericFormatter.h"
#import "NITGuideContactView.h"

@interface NITOrderWhomSection () <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic) IBOutlet UITextView * addressView;
@property (nonatomic) IBOutlet UITextField * nameField;
@property (nonatomic) IBOutlet PhoneTextField * phoneField;
@property (nonatomic) IBOutlet UIButton * knowOnlyRecipientPhoneBtn;
@property (nonatomic) IBOutlet UIButton * deliverWithoutRingingRecipientBtn;
@property (nonatomic) IBOutlet UIButton * deliverMeBtn;
@property (nonatomic) IBOutlet UIButton * selectContactBtn;
@property (nonatomic) BOOL nameEdited;
@property (nonatomic) BOOL addressEdited;

@end

@implementation NITOrderWhomSection

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - Public
- (void)validateSectionWithHandler:(void (^)(BOOL success))handler
{
    NSArray * properties = @[keyPath(SWGOrder.receiverName),
                             keyPath(SWGOrder.receiverPhone),
                             keyPath(SWGOrder.receiverAddress),
                             keyPath(SWGOrder.knowOnlyPhone),
                             keyPath(SWGOrder.dontCallReceiver),
                             keyPath(SWGOrder.forMe)];
    
    [self.parent.presenter validateOrderProperties:properties withHandler:^(BOOL success, NSError *error) {
        [self highlightFieldsIfNeed:error];
        if (handler) handler(success);
    }];
}

#pragma mark - IBAction
- (IBAction)selectPhoneAction:(id)sender
{
    [self.parent.presenter selectPhoneNumberWithHandler:^(NSString *phone, NSString *name, NSString *address) {
        
        self.model.receiverPhone = [@"+" stringByAppendingString:phone];
        
        if (!self.nameEdited && name.length > 0)
        {
            self.model.receiverName = name;
        }
        
        if (!self.addressEdited && address.length > 0)
        {
            self.model.receiverAddress = address;
        }
        
        [self updateUI];
    }];
}

- (IBAction)knowOnlyRecipientPhoneAction:(id)sender
{
    self.model.knowOnlyPhone = !self.model.knowOnlyPhone;
    [self updateButtons];
}

- (IBAction)deliverWithoutRingingRecipientAction:(id)sender
{
    self.model.dontCallReceiver = !self.model.dontCallReceiver;
    [self updateButtons];
}

- (IBAction)deliverMeAction:(id)sender
{
    self.model.forMe = !self.model.forMe;
    [self updateButtons];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Кому", nil);
    
    [self setKeyboardActiv:true];
    
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    
    self.addressView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.addressView.placeholder
                                                                             attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.addressView.delegate = self;
    
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];

    [self updateUI];
    [self showGuideIfNeed];
}

- (void)updateUI
{
    self.nameField.text = self.model.receiverName;
    self.addressView.text = self.model.receiverAddress;
    [self adjustContentSize:self.addressView];
    self.phoneField.text = self.model.receiverPhone;
    
    [self updateButtons];
}

- (void)updateButtons
{
    [self.knowOnlyRecipientPhoneBtn setImage:[UIImage imageNamed:self.model.knowOnlyPhone ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    [self.deliverWithoutRingingRecipientBtn setImage:[UIImage imageNamed:self.model.dontCallReceiver ? @"check" : @"check_ar"] forState:UIControlStateNormal];
    [self.deliverMeBtn setImage:[UIImage imageNamed:self.model.forMe ? @"check" : @"check_ar"] forState:UIControlStateNormal];
}

- (void)updateModelByField:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 101: // name
            textField.text = [textField.text capitalizedString];
            self.model.receiverName = textField.text;
            self.nameEdited = textField.text.length > 0;
            break;
            
        case 102: // address
            self.model.receiverAddress = textField.text;
            self.addressEdited = textField.text.length > 0;
            break;
            
        case 103: // phone
        {
            self.model.receiverPhone = textField.text;
        }
            break;
            
        default:
            break;
    }
    
    textField.textColor = RGB(10, 88, 43);
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder
                                                                      attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

- (void)highlightFieldsIfNeed:(NSError *)error
{
    if (error)
    {
        BOOL needShowError = false;
        
        for (NSString * key in error.errorFields)
        {
            if ([key isEqualToString:@"receiver_name"])
            {
                self.nameField.textColor = RGB(217, 67, 67);
                self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                                       attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.receiverName.length > 0)
                {
                    needShowError = true;
                }
            }
            else if ([key isEqualToString:@"receiver_phone"])
            {
                self.phoneField.textColor = RGB(217, 67, 67);
                self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                                        attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.receiverPhone.length > 0)
                {
                    needShowError = true;
                }
            }
            else if ([key isEqualToString:@"receiver_address"])
            {
                self.addressView.textColor = RGB(217, 67, 67);
                self.addressView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.addressView.placeholder
                                                                                         attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.receiverAddress.length > 0)
                {
                    needShowError = true;
                }
            }
        }
        
        if (needShowError)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:self.parent withTitle:error.interfaceDescription actionButtons:nil cancelButton:cancel];
        }
    }
}

- (void)showGuideIfNeed
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([NITGuideContactView notYetShown])
        {
            [NITGuideContactView showWidthOverlayView:self.selectContactBtn];
        } 
    });
}

-(void)adjustContentSize:(UITextView*)tv
{
    CGFloat deadSpace = ([tv bounds].size.height - [tv contentSize].height);
    CGFloat inset = MAX(0, deadSpace/2.0);
    tv.contentInset = UIEdgeInsetsMake(inset, tv.contentInset.left, inset, tv.contentInset.right);
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self updateModelByField:textField];
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateModelByField:textField];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self adjustContentSize:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.model.receiverAddress = textView.text;
    self.addressEdited = textView.text.length > 0;
}

- (void)textViewDidChange:(UITextView *)textView;
{
    textView.textColor = RGB(10, 88, 43);
    textView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textView.placeholder
                                                                     attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

@end
