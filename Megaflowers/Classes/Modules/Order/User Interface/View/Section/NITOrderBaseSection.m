//
//  NITOrderBaseSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderBaseSection.h"

@interface NITOrderBaseSection ()

@end

@implementation NITOrderBaseSection

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:true];
}

- (void)validateSectionWithHandler:(void(^)(BOOL success))handler
{

}


@end
