//
//  NITOrderAboutSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/17/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderAboutSection.h"
#import "NITOrderViewController.h"
#import "AKNumericFormatter.h"

@interface NITOrderAboutSection () <UITextFieldDelegate>

@property (nonatomic) IBOutlet UITextField * nameField;
@property (nonatomic) IBOutlet PhoneTextField * phoneField;
@property (nonatomic) IBOutlet UITextField * emailField;

@end

@implementation NITOrderAboutSection

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - Public
- (void)validateSectionWithHandler:(void (^)(BOOL success))handler
{
    NSArray * properties = @[keyPath(SWGOrder.senderName),
                             keyPath(SWGOrder.senderPhone),
                             keyPath(SWGOrder.senderEmail)];
    
    [self.parent.presenter validateOrderProperties:properties withHandler:^(BOOL success, NSError *error) {
        [self highlightFieldsIfNeed:error];
        if (handler) handler(success);
    }];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"О себе", nil);
    
    [self setKeyboardActiv:true];
    
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.emailField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName: RGB(164, 183, 171)}];

    [self updateUI];
}

- (void)updateUI
{
    self.nameField.text = self.model.senderName;
    self.phoneField.text = self.model.senderPhone;
    self.emailField.text = self.model.senderEmail;
}

- (void)updateModelByField:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 101: // name
            textField.text = [textField.text capitalizedString];
            self.model.senderName = textField.text;
            break;
            
        case 102: // phone
            self.model.senderPhone = textField.text;
            break;
            
        case 103: // email
            self.model.senderEmail = textField.text;
            break;
            
        default:
            break;
    }
    
    textField.textColor = RGB(10, 88, 43);
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder
                                                                      attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    
    [self updateUI];
}

- (void)highlightFieldsIfNeed:(NSError *)error
{
    if (error)
    {
        BOOL needShowError = false;
        
        for (NSString * key in error.errorFields)
        {
            if ([key isEqualToString:@"sender_name"])
            {
                self.nameField.textColor = RGB(217, 67, 67);
                self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                                       attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.senderName.length > 0)
                {
                    needShowError = true;
                }
            }
            else if ([key isEqualToString:@"sender_phone"])
            {
                self.phoneField.textColor = RGB(217, 67, 67);
                self.phoneField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneField.placeholder
                                                                                        attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.senderPhone.length > 0)
                {
                    needShowError = true;
                }
            }
            else if ([key isEqualToString:@"sender_email"])
            {
                self.emailField.textColor = RGB(217, 67, 67);
                self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.emailField.placeholder
                                                                                        attributes:@{NSForegroundColorAttributeName:RGB(217, 67, 67)}];
                if (self.model.senderEmail.length > 0)
                {
                    needShowError = true;
                }
            }
        }

        if (needShowError)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
            [HELPER showActionSheetFromView:self.parent withTitle:error.interfaceDescription actionButtons:nil cancelButton:cancel];
        }
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self updateModelByField:textField];
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateModelByField:textField];
}

@end
