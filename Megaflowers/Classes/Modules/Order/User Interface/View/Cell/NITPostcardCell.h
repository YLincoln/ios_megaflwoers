//
//  NITPostcardCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITPostcardItem;

@interface NITPostcardCell : UICollectionViewCell

@property (nonatomic) NITPostcardItem * model;
@property (nonatomic) NSInteger index;
@property (nonatomic) NSInteger count;

@end
