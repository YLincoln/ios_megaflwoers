//
//  NITPostcardCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPostcardCell.h"
#import "NITPostcardItem.h"
#import "UIImageView+AFNetworking.h"

@interface NITPostcardCell ()

@property (nonatomic) IBOutlet UIImageView * imageView;
@property (nonatomic) IBOutlet UIButton * backBtn;
@property (nonatomic) IBOutlet UIButton * nextBtn;
@property (nonatomic) IBOutlet UILabel * textLbl;

@end

@implementation NITPostcardCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

#pragma mark - Custom accessors
- (void)setIndex:(NSInteger)index
{
    _index = index;
    [self updateButtons];
}

- (void)setCount:(NSInteger)count
{
    _count = count;
    [self updateButtons];
}

- (void)setModel:(NITPostcardItem *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)backAction:(id)sender
{
    if (self.index > 0)
    {
        self.index--;
        [self scrollAction];
    }
}

- (IBAction)nextAction:(id)sender
{
    if (self.index < self.count)
    {
        self.index++;
        [self scrollAction];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.textLbl.hidden = true;//!self.model.isFree;
    self.imageView.image = nil;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.model.imagePath]];
}

- (void)updateButtons
{
    self.backBtn.hidden = self.index == 0;
    self.nextBtn.hidden = self.index == (self.count - 1);
}

- (void)scrollAction
{
    UICollectionView * collection = (UICollectionView *)self.superview;
    [collection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.index inSection:0]
                       atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                               animated:true];
}

@end
