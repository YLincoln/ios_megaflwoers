//
//  NITOrderProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderModel.h"
#import "NITPostcardItem.h"
#import "NITOrderBaseSection.h"
#import "NITOrderPostcardSection.h"
#import "NITOrderDeliverySection.h"
#import "NITOrderAboutSection.h"
#import "NITDeliveryMethodModel.h"
#import "NITOrderWhomSection.h"
#import "NITOrderWhenSection.h"
#import "NITOrderFromSection.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"
#import "SWGAttach.h"
#import "SWGBasket.h"
#import "SWGBasketRequest.h"
#import "SWGOrder.h"
#import "SWGBasketBouquetComponent.h"
#import "SWGInlineResponse201.h"
#import "SWGOrderTimeList.h"
#import "NITIntervalItem.h"
#import "SWGRequestOrder.h"
#import "SWGBill.h"

@protocol NITOrderInteractorOutputProtocol;
@protocol NITOrderInteractorInputProtocol;
@protocol NITOrderViewProtocol;
@protocol NITOrderPresenterProtocol;
@protocol NITOrderLocalDataManagerInputProtocol;
@protocol NITOrderAPIDataManagerInputProtocol;

@class NITOrderWireFrame;

typedef void (^SelectContactBlock) (NSString * phoneNumber);

@protocol NITOrderViewProtocol
@required
@property (nonatomic, strong) id <NITOrderPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)loadOrderSections:(NSArray *)section;
@end

@protocol NITOrderWireFrameProtocol
@property (nonatomic) SelectContactBlock contactBlock;
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITOrderModuleFrom:(id)fromView withDeliveryType:(DeliveryMethodType)deliveryType;
- (void)backActionFrom:(id)fromViewController;
- (void)showPaymentOrderFrom:(id)fromView forOrderID:(NSNumber *)orderID;
- (NSArray *)createPickupSections;
- (NSArray *)createCourierSections;
- (void)selectPhoneNumberFrom:(id)fromView withHandler:(SelectContactBlock)handler;
- (void)selectSalonAddressFrom:(id)fromView withDelegate:(id)delegate;
@end

@protocol NITOrderPresenterProtocol
@required
@property (nonatomic, weak) id <NITOrderViewProtocol> view;
@property (nonatomic, strong) id <NITOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITOrderWireFrameProtocol> wireFrame;
@property (nonatomic) DeliveryMethodType deliveryType;
@property (nonatomic) NITOrderModel * model;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updatePostcardDataWithHandler:(void(^)(NSArray<NITPostcardItem *> *models))handler;
- (void)updateIntervalsForDate:(NSDate *)date withHandler:(void (^)(NSArray<NITIntervalItem *> *))handler;
- (void)selectSalonAddressFrom:(id)fromView;
- (void)selectPhoneNumberWithHandler:(void(^)(NSString * phone, NSString * name, NSString * address))handler;
- (void)showPaymentOrder;
- (NSInteger)currentIndexForDate:(NSDate *)date fromArray:(NSArray *)array;
- (NSInteger)currentIndexForInterval:(NITIntervalItem *)interval fromArray:(NSArray *)array;
- (void)validateOrderProperties:(NSArray <NSString *> *)properties withHandler:(void(^)(BOOL success, NSError * error))handler;
@end

@protocol NITOrderInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITOrderInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITOrderInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITOrderAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITOrderLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)updatePostcardDataWithHandler:(void (^)(NSArray<NITPostcardItem *> * models))handler;
- (void)updateOrderTimeListForDate:(NSDate *)date withHandler:(void (^)(NSArray<NITIntervalItem *> * result, NSString * error))handler;
- (void)createNewOrderWithHandler:(void(^)(BOOL success, NSString *error, NSNumber *orderID))handler;
- (void)validateOrderProperties:(NSArray *)properties withHandler:(void(^)(BOOL success, NSError *error))handler;
@end


@protocol NITOrderDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITOrderAPIDataManagerInputProtocol <NITOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getPostcardByParendID:(NSNumber *)parent withHandler:(void(^)(NSArray<SWGAttach *> * output))handler;
- (void)getOrderTimeListForDate:(NSDate *)date bouquetSkusString:(NSString *)bouquetSkusString withHandler:(void(^)(NSArray<SWGOrderTimeList *> * output, NSError *error))handler;
- (void)sendPreOrderByBasket:(SWGBasketRequest *)basket withHandler:(void (^)(SWGInlineResponse201* output, NSError* error))handler;
- (void)sendOrderwithID:(NSNumber *)orderID order:(SWGRequestOrder *)order withHandler:(void (^)(BOOL success, NSError *error))handler;
@end

@protocol NITOrderLocalDataManagerInputProtocol <NITOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITBusketBouquet *> *)allBouquets;
- (NSArray <NITBusketAttachment *> *)allAttachments;
- (void)cleanBasket;
- (void)saveOrder:(SWGOrder *)order;
@end
