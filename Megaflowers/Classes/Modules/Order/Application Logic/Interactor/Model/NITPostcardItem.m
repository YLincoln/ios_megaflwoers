//
//  NITPostcardItem.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPostcardItem.h"
#import "SWGAttach.h"
#import "SWGAttachSku.h"

@implementation NITPostcardItem

- (instancetype)initWithAttach:(SWGAttach *)attach
{
    if (self = [super init])
    {
        self.identifier = attach._id;
        self.title = attach.name;
        self.price = [self getFirsthAttachSKU:attach].price;
        self.isFree = self.price.integerValue == 0;

        NSMutableArray * images = [NSMutableArray new];
        for (SWGAttachSku * sku in attach.sku)
        {
            NSString * imagePath = [self imagePathAttachFromSKU:sku] ? : @"";
            [images addObject:imagePath];
            
            if ([self.imagePath length] == 0)
            {
                self.imagePath = imagePath;
                break;
            }
        }
    }
    
    return self;
}

- (SWGAttachSku *)getFirsthAttachSKU:(SWGAttach *)attach
{
    NSMutableArray * sortedSKUS = [attach.sku mutableCopy];
    [sortedSKUS sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:keyPath(SWGBouquetSkus.discountPrice) ascending:true]]];
    
    return sortedSKUS.firstObject;
}

- (NSString *)imagePathAttachFromSKU:(SWGAttachSku *)sku
{
    NSDictionary * imageInfo = (NSDictionary *)sku.image1;
    return imageInfo[@"detail"][API.imageKey];
}

@end
