//
//  NITIntervalItem.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/15/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class SWGOrderTimeList;

@interface NITIntervalItem : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * title;

- (instancetype)initWithModel:(SWGOrderTimeList *)model;

@end
