//
//  NITOrderModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderModel.h"
#import "NITIntervalItem.h"

static NSString * const kPostcardID                         = @"kPostcardID";
static NSString * const kPostcardEnabled                    = @"kPostcardEnabled";
static NSString * const kPostcardText                       = @"kPostcardText";
static NSString * const kDeliveryDate                       = @"kDeliveryDate";
static NSString * const kDeliveryTimeId                     = @"kDeliveryTimeId";
static NSString * const kDeliveryTimeTitle                  = @"kDeliveryTimeTitle";
static NSString * const kSenderName                         = @"kSenderName";
static NSString * const kSenderPhone                        = @"kSenderPhone";
static NSString * const kSenderEmail                        = @"kSenderEmail";
static NSString * const kReceiverName                       = @"kReceiverName";
static NSString * const kReceiverPhone                      = @"kReceiverPhone";
static NSString * const kReceiverAddress                    = @"kReceiverAddress";
static NSString * const kKnowOnlyPhone                      = @"kKnowOnlyPhone";
static NSString * const kDontCallReceiver                   = @"kDontCallReceiver";
static NSString * const kForMe                              = @"kForMe";
static NSString * const kAgreeWithRecipient                 = @"kAgreeWithRecipient";
static NSString * const kTakePhoto                          = @"kTakePhoto";
static NSString * const kLeaveNeighbors                     = @"kLeaveNeighbors";
static NSString * const kDeliverAnonymously                 = @"kDeliverAnonymously";
static NSString * const kCallMeBackToConfirmOrder           = @"kCallMeBackToConfirmOrder";
static NSString * const kAgreeSendMyPersonalData            = @"kAgreeSendMyPersonalData";
static NSString * const kDeliveryType                       = @"kDeliveryType";
static NSString * const kDeliveryPrice                      = @"kDeliveryPrice";
static NSString * const kDeliverySalonId                    = @"kDeliverySalonId";

@implementation NITOrderModel

- (NSNumber *)postcardID
{
    return @([self Integer:kPostcardID]);
}

- (void)setPostcardID:(NSNumber *)postcardID
{
    [self setInteger:kPostcardID value:[postcardID integerValue]];
}

- (BOOL)postcardEnabled
{
    return @([self Integer:kPostcardEnabled]).boolValue;
}

- (void)setPostcardEnabled:(BOOL)postcardEnabled
{
    [self setInteger:kPostcardEnabled value:[@(postcardEnabled) integerValue]];
}

- (NSString *)postcardText
{
    return [self CriptoString:kPostcardText];
}

- (void)setPostcardText:(NSString *)postcardText
{
    [self setCriptoString:kPostcardText value:postcardText];
}

- (NSDate *)deliveryDate
{
    NSDate * date = [NSDate dateFromString:[self CriptoString:kDeliveryDate] format:serverFormat];
    
    if ([date isLessOrEqualToDate:[NSDate date]])
    {
        date = [NSDate date];
        [self setDeliveryDate:date];
    }
    
    return date;
}

- (void)setDeliveryDate:(NSDate *)deliveryDate
{
    [self setCriptoString:kDeliveryDate value:deliveryDate.serverString];
}

- (NITIntervalItem *)deliveryTime
{
    if (self.deliveryDate)
    {
        NITIntervalItem * item = [NITIntervalItem new];
        item.identifier = @([self Integer:kDeliveryTimeId]);
        item.title = [self CriptoString:kDeliveryTimeTitle];
        
        if (item.title.length > 0)
        {
            return item;
        }
    }

    return nil;
}

- (void)setDeliveryTime:(NITIntervalItem *)deliveryTime
{
    [self setInteger:kDeliveryTimeId value:deliveryTime.identifier.integerValue];
    [self setCriptoString:kDeliveryTimeTitle value:deliveryTime.title];
}

- (NSString *)senderName
{
    return [self CriptoString:kSenderName];
}

- (void)setSenderName:(NSString *)senderName
{
    [self setCriptoString:kSenderName value:senderName];
}

- (NSString *)senderPhone
{
    return [self CriptoString:kSenderPhone];
}

- (void)setSenderPhone:(NSString *)senderPhone
{
    [self setCriptoString:kSenderPhone value:senderPhone];
}

- (NSString *)senderEmail
{
    return [self CriptoString:kSenderEmail];
}

- (void)setSenderEmail:(NSString *)senderEmail
{
    [self setCriptoString:kSenderEmail value:senderEmail];
}

- (NSString *)receiverName
{
    return [self CriptoString:kReceiverName];
}

- (void)setReceiverName:(NSString *)receiverName
{
    [self setCriptoString:kReceiverName value:receiverName];
}

- (NSString *)receiverPhone
{
    return [self CriptoString:kReceiverPhone];
}

- (void)setReceiverPhone:(NSString *)receiverPhone
{
    [self setCriptoString:kReceiverPhone value:receiverPhone];
}

- (NSString *)receiverAddress
{
    return [self CriptoString:kReceiverAddress];
}

- (void)setReceiverAddress:(NSString *)receiverAddress
{
    [self setCriptoString:kReceiverAddress value:receiverAddress];
}

- (BOOL)knowOnlyPhone
{
    return @([self Integer:kKnowOnlyPhone]).boolValue;
}

- (void)setKnowOnlyPhone:(BOOL)knowOnlyPhone
{
    [self setInteger:kKnowOnlyPhone value:[@(knowOnlyPhone) integerValue]];
}

- (BOOL)dontCallReceiver
{
    return @([self Integer:kDontCallReceiver]).boolValue;
}

- (void)setDontCallReceiver:(BOOL)dontCallReceiver
{
    [self setInteger:kDontCallReceiver value:[@(dontCallReceiver) integerValue]];
}

- (BOOL)forMe
{
    return @([self Integer:kForMe]).boolValue;
}

- (void)setForMe:(BOOL)forMe
{
    [self setInteger:kForMe value:[@(forMe) integerValue]];
}

- (BOOL)agreeWithRecipient
{
    return @([self Integer:kAgreeWithRecipient]).boolValue;
}

- (void)setAgreeWithRecipient:(BOOL)agreeWithRecipient
{
    [self setInteger:kAgreeWithRecipient value:[@(agreeWithRecipient) integerValue]];
}

- (BOOL)takePhoto
{
    return @([self Integer:kTakePhoto]).boolValue;
}

- (void)setTakePhoto:(BOOL)takePhoto
{
    [self setInteger:kTakePhoto value:[@(takePhoto) integerValue]];
}

- (BOOL)leaveNeighbors
{
    return @([self Integer:kLeaveNeighbors]).boolValue;
}

- (void)setLeaveNeighbors:(BOOL)leaveNeighbors
{
    [self setInteger:kLeaveNeighbors value:[@(leaveNeighbors) integerValue]];
}

- (BOOL)deliverAnonymously
{
    return @([self Integer:kDeliverAnonymously]).boolValue;
}

- (void)setDeliverAnonymously:(BOOL)deliverAnonymously
{
    [self setInteger:kDeliverAnonymously value:[@(deliverAnonymously) integerValue]];
}

- (BOOL)callMeBackToConfirmOrder
{
    return @([self Integer:kCallMeBackToConfirmOrder]).boolValue;
}

- (void)setCallMeBackToConfirmOrder:(BOOL)callMeBackToConfirmOrder
{
    [self setInteger:kCallMeBackToConfirmOrder value:[@(callMeBackToConfirmOrder) integerValue]];
}

- (BOOL)agreeSendMyPersonalData
{
    return @([self Integer:kAgreeSendMyPersonalData]).boolValue;
}

- (void)setAgreeSendMyPersonalData:(BOOL)agreeSendMyPersonalData
{
    [self setInteger:kAgreeSendMyPersonalData value:[@(agreeSendMyPersonalData) integerValue]];
}

- (NSUInteger)deliveryType
{
    return ([self Integer:kDeliveryType]);
}

- (void)setDeliveryType:(NSUInteger)deliveryType
{
    [self setInteger:kDeliveryType value:deliveryType];
}

- (NSNumber *)deliveryPrice
{
    return @([self Integer:kDeliveryPrice]);
}

- (void)setDeliveryPrice:(NSNumber *)deliveryPrice
{
    [self setInteger:kDeliveryPrice value:deliveryPrice.integerValue];
}

- (NSNumber *)deliverySalonId
{
    return @([self Integer:kDeliverySalonId]);
}

- (void)setDeliverySalonId:(NSNumber *)deliverySalonId
{
    [self setInteger:kDeliverySalonId value:deliverySalonId.integerValue];
}

- (void)clean
{
    self.postcardID = nil;
    self.postcardEnabled = false;
    self.postcardText = nil;
    self.deliveryDate = nil;
    self.deliveryTime = nil;
    self.senderName = nil;
    self.senderPhone = nil;
    self.senderEmail = nil;
    self.receiverName = nil;
    self.receiverPhone = nil;
    self.receiverAddress = nil;
    self.knowOnlyPhone = false;
    self.dontCallReceiver = false;
    self.forMe = false;
    self.agreeWithRecipient = false;
    self.takePhoto = false;
    self.leaveNeighbors = false;
    self.deliverAnonymously = false;
    self.callMeBackToConfirmOrder = false;
    self.agreeSendMyPersonalData = false;
    self.deliveryType = 0;
}

@end
