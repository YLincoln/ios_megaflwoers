//
//  NITIntervalItem.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 2/15/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITIntervalItem.h"
#import "SWGOrderTimeList.h"

@implementation NITIntervalItem

- (instancetype)initWithModel:(SWGOrderTimeList *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.title = model.option;
    }
    
    return self;
}

@end
