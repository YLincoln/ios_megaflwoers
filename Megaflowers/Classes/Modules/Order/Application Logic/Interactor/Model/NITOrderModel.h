//
//  NITOrderModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITIntervalItem, NITBiilModel;

@interface NITOrderModel : NITStorageManager

@property (nonatomic) NSString * senderName;
@property (nonatomic) NSString * senderPhone;
@property (nonatomic) NSString * senderEmail;
@property (nonatomic) NSString * receiverName;
@property (nonatomic) NSString * receiverPhone;
@property (nonatomic) NSString * receiverAddress;
@property (nonatomic) NSDate * deliveryDate;
@property (nonatomic) NITIntervalItem * deliveryTime;
@property (nonatomic) BOOL takePhoto;
@property (nonatomic) BOOL leaveNeighbors;
@property (nonatomic) BOOL dontCallReceiver;
@property (nonatomic) BOOL knowOnlyPhone;
@property (nonatomic) BOOL forMe;
@property (nonatomic) NSString * postcardText;
@property (nonatomic) NSNumber * postcardID;
@property (nonatomic) BOOL postcardEnabled;
@property (nonatomic) NSUInteger deliveryType;
@property (nonatomic) NSNumber * deliveryPrice;
@property (nonatomic) BOOL agreeSendMyPersonalData;
@property (nonatomic) BOOL callMeBackToConfirmOrder;
@property (nonatomic) BOOL deliverAnonymously;
@property (nonatomic) BOOL agreeWithRecipient;
@property (nonatomic) NSNumber * deliverySalonId;

- (void)clean;

@end
