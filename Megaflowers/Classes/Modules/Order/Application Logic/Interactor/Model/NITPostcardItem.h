//
//  NITPostcardItem.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGAttach;

@interface NITPostcardItem : NSObject

@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * title;
@property (nonatomic) NSString * imagePath;
@property (nonatomic) NSNumber * price;
@property (nonatomic) BOOL isFree;

- (instancetype)initWithAttach:(SWGAttach *)attach;

@end
