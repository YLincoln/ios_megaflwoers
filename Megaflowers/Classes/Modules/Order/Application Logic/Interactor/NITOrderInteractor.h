//
//  NITOrderInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrderProtocols.h"

@interface NITOrderInteractor : NSObject <NITOrderInteractorInputProtocol>

@property (nonatomic, weak) id <NITOrderInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITOrderAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITOrderLocalDataManagerInputProtocol> localDataManager;

@end
