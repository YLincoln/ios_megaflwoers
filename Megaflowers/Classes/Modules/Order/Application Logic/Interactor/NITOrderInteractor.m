//
//  NITOrderInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITBasketProductModel.h"
#import "NITOrderDetailsWireFrame.h"

static NSInteger freePostcardID = 13;

@implementation NITOrderInteractor

- (void)updatePostcardDataWithHandler:(void (^)(NSArray<NITPostcardItem *> *))handler
{
    weaken(self);
    [self.APIDataManager getPostcardByParendID:@(freePostcardID) withHandler:^(NSArray<SWGAttach *> *output) {
        handler([weakSelf modelsFromEntities:output]);
    }];
}

- (void)updateOrderTimeListForDate:(NSDate *)date withHandler:(void (^)(NSArray<NITIntervalItem *> *, NSString *))handler
{
    SWGBasketRequest * basket = [API createBasket];
    NSArray * skusArray = [basket.bouquets bk_map:^id(SWGBasketRequestBouquets *obj) {
        return obj.skuId.stringValue;
    }];
    NSString * bouquetSkusString = [skusArray componentsJoinedByString:@","];
    
    weaken(self);
    [self.APIDataManager getOrderTimeListForDate:date bouquetSkusString:bouquetSkusString withHandler:^(NSArray<SWGOrderTimeList *> *output, NSError *error) {
        handler([weakSelf intervalModelsFromEntities:output], error.interfaceDescription);
    }];
}

- (void)createNewOrderWithHandler:(void(^)(BOOL success, NSString *error, NSNumber *orderID))handler
{
    SWGBasketRequest * basket = [API createBasket];
    
    if (USER.orderID.integerValue == 0)
    {
        [self sendPreOrderByBasket:basket withHandler:handler];
    }
    else
    {
        weaken(self);
        NITOrderDetailsAPIDataManager * orderApi = [NITOrderDetailsAPIDataManager new];
        [orderApi getUserOrderByID:USER.orderID withHandler:^(SWGOrder *result) {
            
            if (result)
            {
                SWGInlineResponse201 * output = [SWGInlineResponse201 new];
                output._id = result._id;
                output.orderHash = result.orderHash;
                output.num = [NSString stringWithFormat:@"%@", result.num];
                
                SWGRequestOrder * order = [weakSelf createOrderFromResponse:output basket:basket];
                [weakSelf sendOrder:order withHandler:handler];
            }
            else
            {
                USER.orderID = nil;
                [weakSelf sendPreOrderByBasket:basket withHandler:handler];
            }
        }];
    }
}

- (void)validateOrderProperties:(NSArray *)properties withHandler:(void(^)(BOOL success, NSError *error))handler
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        
        if (success)
        {
            if (USER.orderID.integerValue == 0)
            {
                weaken(self);
                [self.APIDataManager sendPreOrderByBasket:[API createBasket] withHandler:^(SWGInlineResponse201 *output, NSError *error) {
                    
                    if (error)
                    {
                        if (handler) handler(false, error);
                    }
                    else
                    {
                        USER.orderID = output._id;
                        [weakSelf validateOrderProperties:properties withHandler:handler];
                    }
                }];
            }
            else
            {
                NITOrderDetailsAPIDataManager * orderApi = [NITOrderDetailsAPIDataManager new];
                [orderApi getUserOrderByID:USER.orderID withHandler:^(SWGOrder *result) {
                    
                    if (result)
                    {
                        SWGOrder * order = [SWGOrder new];
                        NITOrderModel * model = [NITOrderModel new];
                        
                        BOOL isPickup = model.deliveryType == DeliveryMethodTypePickup;
                        
                        order._id       = result._id;
                        order.orderHash = result.orderHash;
                        order.num       = @([result.num integerValue]);
                        order.isPickup  = @(isPickup);
                        
                        for (NSString * key in properties)
                        {
                            if ([key isEqualToString:keyPath(order.senderCityId)])
                            {
                                order.senderCityId = USER.cityID ? : (id)@"";
                            }
                            else if ([key isEqualToString:keyPath(order.receiverName)])
                            {
                                order.receiverName = isPickup ? model.senderName ? : @"" : model.receiverName ? : @"" ;
                            }
                            else if ([key isEqualToString:keyPath(order.receiverPhone)])
                            {
                                order.receiverPhone = isPickup ? model.senderPhone ? : @"" : model.receiverPhone ? : @"";
                            }
                            else if ([key isEqualToString:keyPath(order.deliveryDate)])
                            {
                                order.deliveryDate = [model.deliveryDate serverString] ? : @"";
                            }
                            else if ([key isEqualToString:keyPath(order.deliveryTime)])
                            {
                                order.deliveryTime = model.deliveryTime.identifier ? : (id)@"";
                            }
                            else if ([key isEqualToString:keyPath(order.takePhoto)])
                            {
                                order.takePhoto = @(model.takePhoto);
                            }
                            else if ([key isEqualToString:keyPath(order.leaveNeighbors)])
                            {
                                order.leaveNeighbors = @(model.leaveNeighbors);
                            }
                            else if ([key isEqualToString:keyPath(order.dontCallReceiver)])
                            {
                                order.dontCallReceiver = @(model.dontCallReceiver);
                            }
                            else if ([key isEqualToString:keyPath(order.dontDisturbSender)])
                            {
                                order.dontDisturbSender = @(!model.callMeBackToConfirmOrder);
                            }
                            else if ([key isEqualToString:keyPath(order.notifyAboutDelivery)])
                            {
                                order.notifyAboutDelivery = @(!model.deliverAnonymously);
                            }
                            else if ([key isEqualToString:keyPath(order.knowOnlyPhone)])
                            {
                                order.knowOnlyPhone = @(model.knowOnlyPhone);
                            }
                            else if ([key isEqualToString:keyPath(order.forMe)])
                            {
                                order.forMe = @(model.forMe);
                            }
                            else if ([key isEqualToString:keyPath(order.isPickup)])
                            {
                                order.isPickup = @(isPickup);
                            }
                            else if ([key isEqualToString:keyPath(order.deliverySalonId)])
                            {
                                order.deliverySalonId = isPickup ? model.deliverySalonId : nil;
                            }
                            else
                            {
                                [order setValue:[model valueForKey:key] ? : @""forKey:key];
                            }
                        }
                        
                        [self.APIDataManager sendOrderwithID:order._id order:(SWGRequestOrder *)order withHandler:^(BOOL success, NSError *error) {
                            if (handler) handler(success, error);
                        }];
                        
                    }
                    else
                    {
                        USER.orderID = nil;
                        [self validateOrderProperties:properties withHandler:handler];
                    }
                }];
            }
        }
        else
        {
            if (handler) handler(false, [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"Ошибка соединения", nil)}]);
        }
    }];
}

#pragma mark - Private
- (void)sendPreOrderByBasket:(SWGBasketRequest *)basket withHandler:(void(^)(BOOL success, NSString *error, NSNumber *orderID))handler
{
    weaken(self);
    [self.APIDataManager sendPreOrderByBasket:basket withHandler:^(SWGInlineResponse201 *output, NSError *error) {
        
        if (error)
        {
            handler(false, error.interfaceDescription, output._id);
        }
        else
        {
            USER.orderID = output._id;
            SWGRequestOrder * order = [weakSelf createOrderFromResponse:output basket:basket];
            [self sendOrder:order withHandler:handler];
        }
    }];
}

- (void)sendOrder:(SWGRequestOrder *)order withHandler:(void(^)(BOOL success, NSString *error, NSNumber *orderID))handler
{
    weaken(self);
    [self.APIDataManager sendOrderwithID:order._id order:order withHandler:^(BOOL success, NSError *error){
        
        if (success && !error)
        {
            NITOrderDetailsAPIDataManager * orderAPI = [NITOrderDetailsAPIDataManager new];
            [orderAPI getUserOrderByID:order._id withHandler:^(SWGOrder *result) {
               
                [weakSelf.localDataManager saveOrder:order];
                
                handler(success, error.interfaceDescription, order._id);
                
                SWGBasketRequest * basket = order.basket;
                [TRACKER trackEvent:TrackerEventOrderCompleting parameters:@{@"is_pickup":order.isPickup, @"sum":result.basket.price}];
                if (basket.coupon)
                {
                    [TRACKER trackEvent:TrackerEventCouponeUse parameters:@{@"id":basket.coupon}];
                }
            }];
        }
        else
        {
            handler(success, error.interfaceDescription, order._id);
        }
    }];
}

- (SWGRequestOrder *)createOrderFromResponse:(SWGInlineResponse201 *)response basket:(SWGBasketRequest *)basket
{
    SWGOrder * order = [SWGOrder new];
    NITOrderModel * model = [NITOrderModel new];
    
    BOOL isPickup = model.deliveryType == DeliveryMethodTypePickup;
    
    order._id                   = response._id;
    order.orderHash             = response.orderHash;
    order.num                   = @([response.num integerValue]);
    order.basket                = (id)basket;
    
    order.senderName            = model.senderName;
    order.senderPhone           = model.senderPhone;
    order.senderEmail           = model.senderEmail;
    order.senderCityId          = USER.cityID;
    order.senderAddress         = nil; // ?
    order.senderInfo            = nil; // ?
    
    order.receiverName          = isPickup ? model.senderName : model.receiverName;
    order.receiverPhone         = isPickup ? model.senderPhone : model.receiverPhone;
    order.receiverAddress       = model.receiverAddress;
    order.receiverCityId        = nil; // ?
    order.receiverInfo          = nil; // ?
    
    order.deliveryDate          = [model.deliveryDate serverString];
    order.deliveryTime          = model.deliveryTime.identifier;
    
    order.takePhoto             = @(model.takePhoto);
    order.instaPhoto            = nil; // ?
    order.emailPhoto            = nil; // ?
    
    order.remindMonth           = nil; // ?
    order.remindYear            = nil; // ?
    
    order.leaveNeighbors        = @(model.leaveNeighbors);
    order.dontCallReceiver      = @(model.dontCallReceiver);
    order.dontDisturbSender     = @(!model.callMeBackToConfirmOrder);
    order.notifyAboutDelivery   = @(!model.deliverAnonymously);
    order.knowOnlyPhone         = @(model.knowOnlyPhone);
    
    order.asap                  = nil; // ?
    order.letRecipientKnown     = nil; // ?
    order.forMe                 = @(model.forMe);
    order.postcardText          = model.postcardText;
    order.isPickup              = @(isPickup);
    order.deliverySalonId       = isPickup ? model.deliverySalonId : nil;
    
    return (id)order;
}

- (NSArray <NITPostcardItem *> *)modelsFromEntities:(NSArray <SWGAttach *> *)entities
{
    return [entities bk_map:^id(SWGAttach * obj) {
        return [[NITPostcardItem alloc] initWithAttach:obj];
    }];
}

- (NSArray <NITIntervalItem *> *)intervalModelsFromEntities:(NSArray <SWGOrderTimeList *> *)entities
{
    return [entities bk_map:^id(SWGOrderTimeList * obj) {
        return [[NITIntervalItem alloc] initWithModel:obj];
    }];
}

- (NSArray <NITBasketProductModel *> *)basketBouquetsModelsFromEntities:(NSArray <NITBusketBouquet *> *)entities
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBusketBouquet * bouquet in entities)
    {
        [result addObjectsFromArray:[NITBasketProductModel modelsFromBouquet:bouquet]];
    }
    
    return result;
}

- (NSArray <NITBasketProductModel *> *)basketAttachesModelsFromEntities:(NSArray <NITBusketAttachment *> *)entities
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBusketAttachment * bouquet in entities)
    {
        [result addObjectsFromArray:[NITBasketProductModel modelsFromAttachment:bouquet]];
    }
    
    return result;
}

- (NSArray <SWGBasketRequestBouquets *> *)basketRequestBouquetsFromEntities:(NSArray <NITBasketProductModel *> *)entities
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBasketProductModel * model in entities)
    {
        SWGBasketRequestBouquets * bouquet = [SWGBasketRequestBouquets new];
        
        bouquet.skuId = model.baseSkuId;
        bouquet.count = model.skuCount;
        
        NSMutableArray * components = [NSMutableArray new];
        for (NSDictionary * dict in model.components)
        {
            SWGBasketRequestComponents * component = [SWGBasketRequestComponents new];
            component._id = dict[kComponentIDkey];
            component.colorId = dict[kComponentColorkey];
            component.count = dict[kComponentCountkey];
            
            [components addObject:component];
        }
        
        bouquet.components = (id)components;
        
        [result addObject:bouquet];
    }
    
    return result;
}

- (NSArray <SWGBasketRequestAttaches *> *)basketRequestAttachesFromEntities:(NSArray <NITBasketProductModel *> *)entities
{
    NSMutableArray * result = [NSMutableArray new];
    for (NITBasketProductModel * model in entities)
    {
        SWGBasketRequestAttaches * attachment = [SWGBasketRequestAttaches new];
        
        attachment.skuId = model.baseSkuId;
        attachment.count = model.skuCount;
        
        [result addObject:attachment];
    }
    
    return result;
}

@end
