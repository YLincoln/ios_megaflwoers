//
//  NITOrderAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITOrderProtocols.h"

@interface NITOrderAPIDataManager : NSObject <NITOrderAPIDataManagerInputProtocol>

@end
