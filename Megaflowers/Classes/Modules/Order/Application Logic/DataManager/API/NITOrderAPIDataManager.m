//
//  NITOrderAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderAPIDataManager.h"
#import "SWGAttachApi.h"
#import "SWGOrderApi.h"
#import "SWGPaymentsApi.h"

@implementation NITOrderAPIDataManager

- (void)getPostcardByParendID:(NSNumber *)parent withHandler:(void(^)(NSArray<SWGAttach *> * output))handler
{
    [HELPER startLoading];
    [[[SWGAttachApi alloc] initWithApiClient:[SWGApiClient sharedClient]] attachGetWithLang:HELPER.lang site:API.site city:USER.cityID parent:parent completionHandler:^(NSArray<SWGAttach *> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output);
    }];
}

- (void)getOrderTimeListForDate:(NSDate *)date bouquetSkusString:(NSString *)bouquetSkusString withHandler:(void (^)(NSArray<SWGOrderTimeList *> *output, NSError * error))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderTimeListGetWithLang:HELPER.lang
                                           site:API.site
                                           city:USER.cityID
                                           date:[date serverString]
                                         skuIds:bouquetSkusString
                                           full:@(false)
                              completionHandler:^(NSArray<SWGOrderTimeList> *output, NSError *error) {
                                  [HELPER stopLoading];
                                  if (error)
                                  {
                                      [HELPER logError:error method:METHOD_NAME];
                                  }
                                  handler(output, error);
                              }];
}

- (void)sendPreOrderByBasket:(SWGBasketRequest *)basket withHandler:(void (^)(SWGInlineResponse201* output, NSError* error))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderPostWithCity:USER.cityID site:API.site lang:HELPER.lang basket:basket completionHandler:^(SWGInlineResponse201 *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        handler(output, error);
    }];
}

- (void)sendOrderwithID:(NSNumber *)orderID order:(SWGRequestOrder *)order withHandler:(void (^)(BOOL success, NSError *error))handler
{
    [HELPER startLoading];
    [[SWGOrderApi new] orderIdPatchWithId:orderID order:order completionHandler:^(NSError *error) {
        
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
            handler(false, error);
        }
        else
        {
            handler(true, error);
        }
    }];
}

@end
