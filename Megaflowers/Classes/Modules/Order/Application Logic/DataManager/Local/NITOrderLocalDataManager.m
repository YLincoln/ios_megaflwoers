//
//  NITOrderLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/16/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderLocalDataManager.h"
#import "NITOrder.h"
#import "NITCoupon.h"

@implementation NITOrderLocalDataManager

- (NSArray <NITBusketBouquet *> *)allBouquets
{
    return [NITBusketBouquet allBouquets];
}

- (NSArray <NITBusketAttachment *> *)allAttachments
{
    return [NITBusketAttachment allAttachments];
}

- (void)cleanBasket
{
    [NITBusketBouquet deleteAllBouquets];
    [NITBusketAttachment deleteAllAttachments];
    [NITCoupon deleteAllCoupons];
}

- (void)saveOrder:(SWGOrder *)order
{
    if (order)
    {
       [NITOrder addOrders:@[order]];
    }
}

@end
