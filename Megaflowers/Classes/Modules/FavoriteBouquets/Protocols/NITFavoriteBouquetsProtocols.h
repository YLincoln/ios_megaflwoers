//
//  NITFavoriteBouquetsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemViewModel.h"
#import "NITFavoriteBouquet.h"
#import "NITCatalogDirectoryProtocols.h"

@protocol NITFavoriteBouquetsInteractorOutputProtocol;
@protocol NITFavoriteBouquetsInteractorInputProtocol;
@protocol NITFavoriteBouquetsViewProtocol;
@protocol NITFavoriteBouquetsPresenterProtocol;
@protocol NITFavoriteBouquetsLocalDataManagerInputProtocol;
@protocol NITFavoriteBouquetsAPIDataManagerInputProtocol;

@class NITFavoriteBouquetsWireFrame;

@protocol NITFavoriteBouquetsViewProtocol
@required
@property (nonatomic, strong) id <NITFavoriteBouquetsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITCatalogItemViewModel *> *)models;
@end

@protocol NITFavoriteBouquetsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITFavoriteBouquetsModuleFrom:(id)fromView;
- (void)showDetailsByModel:(NITCatalogItemViewModel *)model from:(id)fromView;
- (void)showDetailsPreviewByModel:(NITCatalogItemViewModel *)model isViewMode:(BOOL)isViewMode;
- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model selectType:(ProductSelectType)selectType from:(id)fromView;
- (void)showCallOrderFrom:(id)fromView;
@end

@protocol NITFavoriteBouquetsPresenterProtocol
@required
@property (nonatomic, weak) id <NITFavoriteBouquetsViewProtocol> view;
@property (nonatomic, strong) id <NITFavoriteBouquetsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFavoriteBouquetsWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)showDetailsByModel:(NITCatalogItemViewModel *)model;
- (void)showDetailsPreviewByModel:(NITCatalogItemViewModel *)model;
- (void)removeFavoriteBouquet:(NITCatalogItemViewModel *)model;
- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model;
@end

@protocol NITFavoriteBouquetsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDataByModels:(NSArray <NITCatalogItemViewModel *> *)models;
@end

@protocol NITFavoriteBouquetsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITFavoriteBouquetsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFavoriteBouquetsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFavoriteBouquetsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)loadData;
- (void)updateData;
- (void)removeFavoriteBouquet:(NITCatalogItemViewModel *)model;
@end


@protocol NITFavoriteBouquetsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
- (void)removeFavoriteBouquetByID:(NSNumber *)bouquetID;
@end

@protocol NITFavoriteBouquetsAPIDataManagerInputProtocol <NITFavoriteBouquetsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getBouquetByID:(NSNumber *)bouquetID withHandler:(void(^)(SWGBouquet * output))handler;
@end

@protocol NITFavoriteBouquetsLocalDataManagerInputProtocol <NITFavoriteBouquetsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITFavoriteBouquet *> *)getFavoriteBouquets;
- (void)removeFavoriteBouquetByID:(NSNumber *)bouquetID;
@end
