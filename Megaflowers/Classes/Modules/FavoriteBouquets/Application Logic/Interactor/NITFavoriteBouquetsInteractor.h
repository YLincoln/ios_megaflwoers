//
//  NITFavoriteBouquetsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteBouquetsProtocols.h"

@interface NITFavoriteBouquetsInteractor : NSObject <NITFavoriteBouquetsInteractorInputProtocol>

@property (nonatomic, weak) id <NITFavoriteBouquetsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFavoriteBouquetsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFavoriteBouquetsLocalDataManagerInputProtocol> localDataManager;

@end
