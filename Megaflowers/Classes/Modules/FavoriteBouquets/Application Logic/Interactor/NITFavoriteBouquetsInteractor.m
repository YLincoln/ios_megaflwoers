//
//  NITFavoriteBouquetsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsInteractor.h"
#import "NSArray+BlocksKit.h"
#import "SWGBouquet.h"
#import "NITLoginAPIDataManager.h"

@implementation NITFavoriteBouquetsInteractor

- (void)loadData
{
    NITLoginAPIDataManager * api = [NITLoginAPIDataManager new];
    [api getUserSkuWithHandler:^(NSArray<SWGUserBouquet> *result) {
        
        NSArray * skuIDs = [result bk_map:^id(SWGUserBouquet *obj) {
            return obj._id;
        }];
        
        [api getUserBouquetsWithHandler:^(NSArray<SWGBouquet> *result) {
            
            NSArray * models = [NITCatalogItemViewModel bouqetModelsFromEntities:result];
            for (NITCatalogItemViewModel * model in models)
            {
                for (NITCatalogItemSKUViewModel * sku in model.skus)
                {
                    if ([skuIDs containsObject:sku.identifier])
                    {
                        [NITFavoriteBouquet createFavoriteBouquetByModel:model andSkuModel:sku];
                    }
                }
            }
        }];
    }];
}

- (void)updateData
{
    NSArray <NITCatalogItemViewModel *> * favoriteBouquets = [self bouquetsModelsFromEntities:[self.localDataManager getFavoriteBouquets]];
    [self.presenter updateDataByModels:favoriteBouquets];
}

- (void)removeFavoriteBouquet:(NITCatalogItemViewModel *)model
{
    [self.localDataManager removeFavoriteBouquetByID:model.favoriteSkuID];
    [self.APIDataManager removeFavoriteBouquetByID:model.favoriteSkuID];
    [self updateData];
}

#pragma mark - Private
- (NSArray <NITCatalogItemViewModel *> *)bouquetsModelsFromEntities:(NSArray <NITFavoriteBouquet *> *)entities
{
    return [entities bk_map:^id(NITFavoriteBouquet * obj) {
        return [[NITCatalogItemViewModel alloc] initWidthFavoriteBouquet:obj];
    }];
}

- (NSArray <NITCatalogItemViewModel *> *)modelsFromEntities:(NSArray <SWGBouquet *> *)entities
{
    return [entities bk_map:^id(SWGBouquet * obj) {
        return [[NITCatalogItemViewModel alloc] initWithBouquet:obj];
    }];
}

@end
