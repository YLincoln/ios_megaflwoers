//
//  NITFavoriteBouquetsLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteBouquetsProtocols.h"

@interface NITFavoriteBouquetsLocalDataManager : NSObject <NITFavoriteBouquetsLocalDataManagerInputProtocol>

@end
