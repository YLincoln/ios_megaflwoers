//
//  NITFavoriteBouquetsLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsLocalDataManager.h"

@implementation NITFavoriteBouquetsLocalDataManager

- (NSArray <NITFavoriteBouquet *> *)getFavoriteBouquets
{
    return [NITFavoriteBouquet allBouquets];
}

- (void)removeFavoriteBouquetByID:(NSNumber *)bouquetID
{
    [NITFavoriteBouquet deleteBouquetBySkuID:bouquetID];
}

@end
