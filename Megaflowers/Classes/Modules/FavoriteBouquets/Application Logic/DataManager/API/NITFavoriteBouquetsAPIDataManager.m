//
//  NITFavoriteBouquetsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsAPIDataManager.h"
#import "SWGUserApi.h"
#import "SWGBouquetApi.h"

@implementation NITFavoriteBouquetsAPIDataManager

- (void)getBouquetByID:(NSNumber *)bouquetID withHandler:(void(^)(SWGBouquet * output))handler
{
    [HELPER startLoading];
    [[[SWGBouquetApi alloc] initWithApiClient:[SWGApiClient sharedClient]] bouquetIdGetWithId:bouquetID
                                             lang:HELPER.lang
                                             site:API.site
                                             city:USER.cityID
                                completionHandler:^(SWGBouquet *output, NSError *error) {
                                    [HELPER stopLoading];
                                    if (error)
                                    {
                                        [HELPER logError:error method:METHOD_NAME];
                                    }

                                    handler(output);
                                }];
}


- (void)removeFavoriteBouquetByID:(NSNumber *)bouquetID
{
    [HELPER startLoading];
    [[SWGUserApi new] userBouquetIdDeleteWithId:bouquetID completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
    }];
}

@end
