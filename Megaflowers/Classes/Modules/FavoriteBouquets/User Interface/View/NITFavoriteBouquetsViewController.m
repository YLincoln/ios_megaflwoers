//
//  NITFavoriteBouquetsViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsViewController.h"
#import "NITFavoriteBouquetsCell.h"
#import "NITProductDetailsViewController.h"

@interface NITFavoriteBouquetsViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UIViewControllerPreviewingDelegate,
NITFavoriteBouquetsCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIView * emptyView;
@property (nonatomic) NSArray <NITCatalogItemViewModel *> * items;

@end

@implementation NITFavoriteBouquetsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Избранное", nil);
    
    // table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITFavoriteBouquetsCell) bundle:nil] forCellReuseIdentifier:_s(NITFavoriteBouquetsCell)];
    
    // Register for 3D Touch Previewing if available
    if ([HELPER forceTouchAvailable])
    {
        [self registerForPreviewingWithDelegate:self sourceView:self.tableView];
    }
}

- (void)updateUI
{
    [self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - NITFavoriteBouquetsPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITCatalogItemViewModel *> *)models
{
    self.items = models;
    self.emptyView.hidden = models.count > 0;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITFavoriteBouquetsCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITFavoriteBouquetsCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.presenter showDetailsByModel:self.items[indexPath.row]];
}

#pragma pragma - NITFavoriteBouquetsCellDelegate
- (void)didChangeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model
{
    [self.presenter removeFavoriteBouquet:model];
    [self updateUI];
}

- (void)didShowPreviewForBouquet:(NITCatalogItemViewModel *)model
{
    [self.presenter showDetailsPreviewByModel:model];
}

#pragma mark - UIViewControllerPreviewingDelegate
- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location
{
    NITCatalogItemViewModel * model = self.items[[self.tableView indexPathForRowAtPoint:location].row];
    previewingContext.sourceRect = CGRectZero;
    
    return [self.presenter createProductDetailsByModel:model];
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit
{
    UINavigationController * navVC = (UINavigationController *)viewControllerToCommit;
    [navVC setNavigationBarHidden:false];
    
    NITProductDetailsViewController * vc = (NITProductDetailsViewController *)navVC.topViewController;
    [vc hidePreview];
    
    [self showDetailViewController:viewControllerToCommit sender:self];
}

@end
