//
//  NITFavoriteBouquetsCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITCatalogItemViewModel;

@protocol NITFavoriteBouquetsCellDelegate <NSObject>

- (void)didChangeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model;
- (void)didShowPreviewForBouquet:(NITCatalogItemViewModel *)model;

@end

@interface NITFavoriteBouquetsCell : UITableViewCell

@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic, weak) id <NITFavoriteBouquetsCellDelegate> delegate;

@end
