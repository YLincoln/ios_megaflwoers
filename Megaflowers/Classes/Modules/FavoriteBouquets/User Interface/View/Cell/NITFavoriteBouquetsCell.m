//
//  NITFavoriteBouquetsCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsCell.h"
#import "NITCatalogItemViewModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITFavoriteBouquetsCell ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * price;
@property (nonatomic) IBOutlet UIButton * favoriteBtn;
@property (nonatomic) IBOutlet UIImageView * image;
@property (nonatomic) IBOutlet UILabel * skuName;
@property (nonatomic) IBOutlet UILabel * skuSize;

@end

@implementation NITFavoriteBouquetsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [self addGestureRecognizer:longPress];
}

- (void)setModel:(NITCatalogItemViewModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)favoriteAction:(id)sender
{
    self.model.favorite = !self.model.favorite;
    
    if ([self.delegate respondsToSelector:@selector(didChangeFavoriteStateForBouquet:)])
    {
        [self.delegate didChangeFavoriteStateForBouquet:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    if (self.model.enabled)
    {
        self.title.text = self.model.title;
        self.title.textColor = RGBA(58, 58, 58, 1.0);
        self.price.textColor = RGBA(58, 58, 58, 1.0);
        self.image.alpha = 1.0;
    }
    else
    {
        self.title.text = NSLocalizedString(@"Букет снят с продажи", nil);
        self.title.textColor = RGBA(58, 58, 58, 0.5);
        self.price.textColor = RGBA(58, 58, 58, 0.2);
        self.image.alpha = 0.6;
    }
    
    self.skuName.text = self.model.favoriteSkuName;
    self.skuSize.text = self.model.favoriteSkuSize;
    
    self.image.image = nil;
    [self.image setImageWithURL:[NSURL URLWithString:self.model.previewPath]];
    
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSString * priceString = [self.model.price stringValue];//[[formatter stringFromNumber:self.model.price] stringByReplacingOccurrencesOfString:@"," withString:@" "];
    NSString * discountPriceString = [self.model.discountPrice stringValue];//[[formatter stringFromNumber:self.model.discountPrice] stringByReplacingOccurrencesOfString:@"," withString:@" "];
    
    if ([self.model.price integerValue] != [self.model.discountPrice integerValue])
    {
        NSString * title = [NSString stringWithFormat:@"%@ %@ %@", priceString, discountPriceString, NSLocalizedString(@"руб.", nil)];
        NSMutableAttributedString * attrText = [[NSMutableAttributedString alloc] initWithString:title];
        
        [attrText addAttribute:NSBaselineOffsetAttributeName
                         value:@(0)
                         range:NSMakeRange(0, priceString.length)];
        
        [attrText addAttribute:NSStrikethroughStyleAttributeName
                         value:@(2)
                         range:NSMakeRange(0, priceString.length)];
        
        NSInteger length = priceString.length + 1;
        [attrText addAttribute:NSForegroundColorAttributeName
                         value:RGB(254, 148, 205)
                         range:NSMakeRange(length, attrText.length - length)];
        
        self.price.attributedText = attrText;
    }
    else
    {
        self.price.text = [NSString stringWithFormat:@"%@ %@", discountPriceString, NSLocalizedString(@"руб.", nil)];
    }
}

- (void)longPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan)
    {
        if ([self.delegate respondsToSelector:@selector(didShowPreviewForBouquet:)])
        {
            [self.delegate didShowPreviewForBouquet:self.model];
        }
    }
}

@end
