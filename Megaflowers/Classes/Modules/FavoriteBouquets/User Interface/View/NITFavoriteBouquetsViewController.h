//
//  NITFavoriteBouquetsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITFavoriteBouquetsProtocols.h"

@interface NITFavoriteBouquetsViewController : NITBaseViewController <NITFavoriteBouquetsViewProtocol>

@property (nonatomic, strong) id <NITFavoriteBouquetsPresenterProtocol> presenter;

@end
