//
//  NITFavoriteBouquetsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteBouquetsProtocols.h"

@class NITFavoriteBouquetsWireFrame;

@interface NITFavoriteBouquetsPresenter : NITRootPresenter <NITFavoriteBouquetsPresenterProtocol, NITFavoriteBouquetsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITFavoriteBouquetsViewProtocol> view;
@property (nonatomic, strong) id <NITFavoriteBouquetsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFavoriteBouquetsWireFrameProtocol> wireFrame;

@end
