//
//  NITFavoriteBouquetsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsPresenter.h"
#import "NITFavoriteBouquetsWireframe.h"
#import "NITProductDetailsWireFrame.h"

@implementation NITFavoriteBouquetsPresenter

#pragma mark - Data
- (void)initData
{
    [self.interactor updateData];
    [self addObservers];
    [self.interactor loadData];
}

- (void)updateData
{
    [self.interactor updateData];
}

- (void)removeFavoriteBouquet:(NITCatalogItemViewModel *)model
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Удалить", nil) actionBlock:^{
        [weakSelf.interactor removeFavoriteBouquet:model];
    }];
    
    NSString * title = [NSString stringWithFormat:@"%@ %@ %@",
                        NSLocalizedString(@"Букет", nil),
                        model.title,
                        NSLocalizedString(@"будет удален из избранного", nil)];
    
    [HELPER showActionSheetFromView:self.view withTitle:title actionButtons:@[action1] cancelButton:cancel];
}

#pragma mark - Navigation
- (void)showDetailsByModel:(NITCatalogItemViewModel *)model
{
    if (model.enabled)
    {
        [self.wireFrame showDetailsByModel:model from:self.view];
    }
}

- (void)showDetailsPreviewByModel:(NITCatalogItemViewModel *)model
{
    [self.wireFrame showDetailsPreviewByModel:model isViewMode:false];
}

- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model
{
    return [self.wireFrame createProductDetailsByModel:model selectType:ProductSelectTypeView from:self.view];
}

#pragma mark - NITFavoriteBouquetsInteractorOutputProtocol
- (void)updateDataByModels:(NSArray<NITCatalogItemViewModel *> *)models
{
    [self.view reloadDataByModels:models];
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveFavoriteBouquetNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCallOrder) name:kOpenCallOrderPreviewNotification object:nil];
}

#pragma mark - Private

- (void)openCallOrder
{
    [self.wireFrame showCallOrderFrom:self.view];
}

@end
