//
//  NITFavoriteBouquetsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsWireFrame.h"
#import "NITProductDetailsWireFrame.h"
#import "NITCallOrderWireFrame.h"

@implementation NITFavoriteBouquetsWireFrame

+ (id)createNITFavoriteBouquetsModule
{
    // Generating module components
    id <NITFavoriteBouquetsPresenterProtocol, NITFavoriteBouquetsInteractorOutputProtocol> presenter = [NITFavoriteBouquetsPresenter new];
    id <NITFavoriteBouquetsInteractorInputProtocol> interactor = [NITFavoriteBouquetsInteractor new];
    id <NITFavoriteBouquetsAPIDataManagerInputProtocol> APIDataManager = [NITFavoriteBouquetsAPIDataManager new];
    id <NITFavoriteBouquetsLocalDataManagerInputProtocol> localDataManager = [NITFavoriteBouquetsLocalDataManager new];
    id <NITFavoriteBouquetsWireFrameProtocol> wireFrame= [NITFavoriteBouquetsWireFrame new];
    id <NITFavoriteBouquetsViewProtocol> view = [(NITFavoriteBouquetsWireFrame *)wireFrame createViewControllerWithKey:_s(NITFavoriteBouquetsViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITFavoriteBouquetsModuleFrom:(UIViewController *)fromViewController
{
    NITFavoriteBouquetsViewController * view = [NITFavoriteBouquetsWireFrame createNITFavoriteBouquetsModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showDetailsByModel:(NITCatalogItemViewModel *)model from:(UIViewController *)fromViewController
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController bouquet:model selectType:ProductSelectTypeView];
}

- (void)showDetailsPreviewByModel:(NITCatalogItemViewModel *)model isViewMode:(BOOL)isViewMode
{
    [NITProductDetailsWireFrame presentNITProductDetailsPreviewModuleWithBouquet:model isViewMode:isViewMode];
}

- (id)createProductDetailsByModel:(NITCatalogItemViewModel *)model selectType:(ProductSelectType)selectType from:(UIViewController *)fromViewController
{
    return [NITProductDetailsWireFrame createProductDetailsByModel:model selectType:selectType from:fromViewController];
}

- (void)showCallOrderFrom:(UIViewController *)fromViewController
{
    [NITCallOrderWireFrame presentNITCallOrderModuleFrom:fromViewController];
}

@end
