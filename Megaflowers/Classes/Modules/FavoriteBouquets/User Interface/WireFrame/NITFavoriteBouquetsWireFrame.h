//
//  NITFavoriteBouquetsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/01/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteBouquetsProtocols.h"
#import "NITFavoriteBouquetsViewController.h"
#import "NITFavoriteBouquetsLocalDataManager.h"
#import "NITFavoriteBouquetsAPIDataManager.h"
#import "NITFavoriteBouquetsInteractor.h"
#import "NITFavoriteBouquetsPresenter.h"
#import "NITFavoriteBouquetsWireframe.h"
#import "NITRootWireframe.h"

@interface NITFavoriteBouquetsWireFrame : NITRootWireframe <NITFavoriteBouquetsWireFrameProtocol>

@end
