//
//  NITFeedbackAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackProtocols.h"

@interface NITFeedbackAPIDataManager : NSObject <NITFeedbackAPIDataManagerInputProtocol>

@end
