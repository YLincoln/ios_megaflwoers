//
//  NITFeedbackModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef CF_ENUM (NSUInteger, FeedbackType) {
    FeedbackTypeWrite   = 0,
    FeedbackTypeCall    = 1,
    FeedbackTypeRequest = 2
};

@interface NITFeedbackModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NSString * text;
@property (nonatomic) FeedbackType type;

- (instancetype)initWithType:(FeedbackType)type;

@end
