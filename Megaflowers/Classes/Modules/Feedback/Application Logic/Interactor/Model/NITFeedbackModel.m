//
//  NITFeedbackModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackModel.h"

@implementation NITFeedbackModel

- (instancetype)initWithType:(FeedbackType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

- (void)setType:(FeedbackType)type
{
    _type = type;
    
    switch (type) {
        case FeedbackTypeWrite:
            self.title = NSLocalizedString(@"Напишите нам", nil);
            self.text = NSLocalizedString(@"Вы можете оставить отзыв о нашей работе или пожаловаться на работу службы доставки.", nil);
            break;
            
        case FeedbackTypeCall:
            self.title = NSLocalizedString(@"Позвонить нам", nil);
            self.text = NSLocalizedString(@"Позвоните по горячей линии и задайте любые интересующие вас вопросы! Горячая линия работает круглосуточно, звонок бесплатный.", nil);
            break;
            
        case FeedbackTypeRequest:
            self.title = NSLocalizedString(@"Оставить заявку", nil);
            self.text = NSLocalizedString(@"Либо оставьте заявку на звонок и наши операторы свяжутся с вами в удобное для вас время.", nil);
            break;
    }
}

@end
