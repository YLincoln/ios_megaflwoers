//
//  NITFeedbackProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackModel.h"

@protocol NITFeedbackInteractorOutputProtocol;
@protocol NITFeedbackInteractorInputProtocol;
@protocol NITFeedbackViewProtocol;
@protocol NITFeedbackPresenterProtocol;
@protocol NITFeedbackLocalDataManagerInputProtocol;
@protocol NITFeedbackAPIDataManagerInputProtocol;

@class NITFeedbackWireFrame;

@protocol NITFeedbackViewProtocol
@required
@property (nonatomic, strong) id <NITFeedbackPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITFeedbackModel *> *)models;
@end

@protocol NITFeedbackWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITFeedbackModuleFrom:(id)fromView;
- (void)showCallOrderFrom:(id)fromView;
- (void)showWriteFeedbackFrom:(id)fromView;
@end

@protocol NITFeedbackPresenterProtocol
@required
@property (nonatomic, weak) id <NITFeedbackViewProtocol> view;
@property (nonatomic, strong) id <NITFeedbackInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFeedbackWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)writeFeedback;
- (void)callFeedback;
- (void)requestFeedback;
@end

@protocol NITFeedbackInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@optional
- (void)feedbackSended;
@end

@protocol NITFeedbackInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITFeedbackInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFeedbackAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFeedbackLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITFeedbackDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITFeedbackAPIDataManagerInputProtocol <NITFeedbackDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITFeedbackLocalDataManagerInputProtocol <NITFeedbackDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
