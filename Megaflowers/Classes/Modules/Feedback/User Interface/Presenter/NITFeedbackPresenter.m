//
//  NITFeedbackPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackPresenter.h"
#import "NITFeedbackWireframe.h"

@implementation NITFeedbackPresenter

- (void)initData
{
    NSArray * data = @[
                       [[NITFeedbackModel alloc] initWithType:FeedbackTypeWrite],
                       [[NITFeedbackModel alloc] initWithType:FeedbackTypeCall],
                       [[NITFeedbackModel alloc] initWithType:FeedbackTypeRequest]
                       ];
    
    [self.view reloadDataByModels:data];
}

- (void)writeFeedback
{
    [self.wireFrame showWriteFeedbackFrom:self.view];
}

- (void)callFeedback
{
    [PHONE callToPhoneNumber:USER.cityPhone];
}

- (void)requestFeedback
{
    [self.wireFrame showCallOrderFrom:self.view];
}

@end
