//
//  NITFeedbackPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackProtocols.h"

@class NITFeedbackWireFrame;

@interface NITFeedbackPresenter : NITRootPresenter <NITFeedbackPresenterProtocol, NITFeedbackInteractorOutputProtocol>

@property (nonatomic, weak) id <NITFeedbackViewProtocol> view;
@property (nonatomic, strong) id <NITFeedbackInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFeedbackWireFrameProtocol> wireFrame;

@end
