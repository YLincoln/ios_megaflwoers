//
//  NITFeedbackWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackProtocols.h"
#import "NITFeedbackViewController.h"
#import "NITFeedbackLocalDataManager.h"
#import "NITFeedbackAPIDataManager.h"
#import "NITFeedbackInteractor.h"
#import "NITFeedbackPresenter.h"
#import "NITFeedbackWireframe.h"
#import "NITRootWireframe.h"

@interface NITFeedbackWireFrame : NITRootWireframe <NITFeedbackWireFrameProtocol>

@end
