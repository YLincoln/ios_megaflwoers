//
//  NITFeedbackWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackWireFrame.h"
#import "NITCallOrderWireFrame.h"
#import "NITFeedbackCreateWireFrame.h"

@implementation NITFeedbackWireFrame

+ (id)createNITFeedbackModule
{
    // Generating module components
    id <NITFeedbackPresenterProtocol, NITFeedbackInteractorOutputProtocol> presenter = [NITFeedbackPresenter new];
    id <NITFeedbackInteractorInputProtocol> interactor = [NITFeedbackInteractor new];
    id <NITFeedbackAPIDataManagerInputProtocol> APIDataManager = [NITFeedbackAPIDataManager new];
    id <NITFeedbackLocalDataManagerInputProtocol> localDataManager = [NITFeedbackLocalDataManager new];
    id <NITFeedbackWireFrameProtocol> wireFrame= [NITFeedbackWireFrame new];
    id <NITFeedbackViewProtocol> view = [(NITFeedbackWireFrame *)wireFrame createViewControllerWithKey:_s(NITFeedbackViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITFeedbackModuleFrom:(UIViewController *)fromViewController
{
    NITFeedbackViewController * vc = [NITFeedbackWireFrame createNITFeedbackModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showCallOrderFrom:(UIViewController *)fromViewController
{
    [NITCallOrderWireFrame presentNITCallOrderModuleFrom:fromViewController];
}

- (void)showWriteFeedbackFrom:(UIViewController *)fromViewController
{
    [NITFeedbackCreateWireFrame presentNITFeedbackCreateModuleFrom:fromViewController];
}

@end
