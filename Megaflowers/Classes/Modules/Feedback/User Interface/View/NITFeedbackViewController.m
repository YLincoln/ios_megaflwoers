//
//  NITFeedbackViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackViewController.h"
#import "NITFeedbackCell.h"

@interface NITFeedbackViewController ()
<
UITableViewDataSource,
UITableViewDelegate,
NITFeedbackCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITFeedbackModel *> * items;

@end

@implementation NITFeedbackViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - NITFeedbackPresenterProtocol
- (void)reloadDataByModels:(NSArray <NITFeedbackModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Обратная связь", nil);
    
    // table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITFeedbackCell) bundle:nil] forCellReuseIdentifier:_s(NITFeedbackCell)];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITFeedbackCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITFeedbackCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - NITFeedbackCellDelegate
- (void)didWriteFeedback
{
    [self.presenter writeFeedback];
}

- (void)didCallFeedback
{
    [self.presenter callFeedback];
}

- (void)didRequestFeedback
{
    [self.presenter requestFeedback];
}

@end
