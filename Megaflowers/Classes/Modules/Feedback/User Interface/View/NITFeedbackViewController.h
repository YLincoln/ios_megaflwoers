//
//  NITFeedbackViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITFeedbackProtocols.h"

@interface NITFeedbackViewController : NITBaseViewController <NITFeedbackViewProtocol>

@property (nonatomic, strong) id <NITFeedbackPresenterProtocol> presenter;

@end
