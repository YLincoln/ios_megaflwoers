//
//  NITFeedbackCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCell.h"
#import "NITFeedbackModel.h"

@interface NITFeedbackCell ()

@property (nonatomic) IBOutlet UILabel * text;
@property (nonatomic) IBOutlet UIButton * actionBtn;

@end

@implementation NITFeedbackCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITFeedbackModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)actionTapped:(id)sender
{
    switch (self.model.type) {
        case FeedbackTypeWrite:
            if ([self.delegate respondsToSelector:@selector(didWriteFeedback)])
            {
                [self.delegate didWriteFeedback];
            }
            break;
            
        case FeedbackTypeCall:
            if ([self.delegate respondsToSelector:@selector(didCallFeedback)])
            {
                [self.delegate didCallFeedback];
            }
            break;
            
        case FeedbackTypeRequest:
            if ([self.delegate respondsToSelector:@selector(didRequestFeedback)])
            {
                [self.delegate didRequestFeedback];
            }
            break;
    }
}

#pragma mark - Private
- (void)updateData
{
    self.text.text = self.model.text;
    [self.actionBtn setTitle:self.model.title forState:UIControlStateNormal];
}

@end
