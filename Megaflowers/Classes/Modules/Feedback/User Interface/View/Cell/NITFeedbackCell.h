//
//  NITFeedbackCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITFeedbackModel;

@protocol NITFeedbackCellDelegate <NSObject>

- (void)didWriteFeedback;
- (void)didCallFeedback;
- (void)didRequestFeedback;

@end

@interface NITFeedbackCell : UITableViewCell

@property (nonatomic) NITFeedbackModel * model;
@property (nonatomic, weak) id <NITFeedbackCellDelegate> delegate;

@end
