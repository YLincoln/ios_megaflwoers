//
//  NITBarcodeScannerAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBarcodeScannerProtocols.h"

@interface NITBarcodeScannerAPIDataManager : NSObject <NITBarcodeScannerAPIDataManagerInputProtocol>

@end
