//
//  NITBarcodeScannerInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBarcodeScannerProtocols.h"

@interface NITBarcodeScannerInteractor : NSObject <NITBarcodeScannerInteractorInputProtocol>

@property (nonatomic, weak) id <NITBarcodeScannerInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBarcodeScannerAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBarcodeScannerLocalDataManagerInputProtocol> localDataManager;

@end
