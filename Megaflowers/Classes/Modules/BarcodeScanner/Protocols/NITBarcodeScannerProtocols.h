//
//  NITBarcodeScannerProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITBarcodeScannerInteractorOutputProtocol;
@protocol NITBarcodeScannerInteractorInputProtocol;
@protocol NITBarcodeScannerViewProtocol;
@protocol NITBarcodeScannerPresenterProtocol;
@protocol NITBarcodeScannerLocalDataManagerInputProtocol;
@protocol NITBarcodeScannerAPIDataManagerInputProtocol;

@class NITBarcodeScannerWireFrame;

typedef void (^ScanBarcodeBlock) (NSString * code);

@protocol NITBarcodeScannerViewProtocol
@required
@property (nonatomic, strong) id <NITBarcodeScannerPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITBarcodeScannerWireFrameProtocol
@property (nonatomic) ScanBarcodeBlock scanBlock;
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITBarcodeScannerModuleFrom:(id)fromView title:(NSString *)title handler:(ScanBarcodeBlock)handler;
- (void)dismissView:(id)view withBarcode:(NSString *)barcode;
@end

@protocol NITBarcodeScannerPresenterProtocol
@required
@property (nonatomic, weak) id <NITBarcodeScannerViewProtocol> view;
@property (nonatomic, strong) id <NITBarcodeScannerInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBarcodeScannerWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * title;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)scanCode:(NSString *)scannedCode;
- (void)closeAction;
@end

@protocol NITBarcodeScannerInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITBarcodeScannerInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITBarcodeScannerInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBarcodeScannerAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBarcodeScannerLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITBarcodeScannerDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITBarcodeScannerAPIDataManagerInputProtocol <NITBarcodeScannerDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITBarcodeScannerLocalDataManagerInputProtocol <NITBarcodeScannerDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
