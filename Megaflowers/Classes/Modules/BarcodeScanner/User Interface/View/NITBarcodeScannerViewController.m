//
//  NITBarcodeScannerViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBarcodeScannerViewController.h"
#import "RMScannerView.h"

@interface NITBarcodeScannerViewController ()
<
RMScannerViewDelegate
>

@property (nonatomic) IBOutlet UILabel * titleLbl;
@property (nonatomic) IBOutlet RMScannerView * scannerView;

@end

@implementation NITBarcodeScannerViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

- (IBAction)closeAction:(id)sender
{
    [self.presenter closeAction];
}

- (void)initUI
{
    [self.navigationController setNavigationBarHidden:true];
    
    self.titleLbl.text = self.presenter.title;
    
    [self.scannerView setVerboseLogging:true];
    [self.scannerView setAnimateScanner:false];
    [self.scannerView setDisplayCodeOutline:false];
    [self.scannerView startCaptureSession];
}

#pragma mark - RMScannerViewDelegate
- (void)didScanCode:(NSString *)scannedCode onCodeType:(NSString *)codeType
{
    [self.presenter scanCode:scannedCode];
}

- (void)errorGeneratingCaptureSession:(NSError *)error
{
    [HELPER logError:error method:THIS_METHOD];
}

- (void)errorAcquiringDeviceHardwareLock:(NSError *)error
{
    [HELPER logError:error method:THIS_METHOD];
}

- (BOOL)shouldEndSessionAfterFirstSuccessfulScan
{
    return true;
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

@end
