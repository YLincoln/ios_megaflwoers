//
//  NITBarcodeScannerViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITBarcodeScannerProtocols.h"

@interface NITBarcodeScannerViewController : UIViewController <NITBarcodeScannerViewProtocol>

@property (nonatomic, strong) id <NITBarcodeScannerPresenterProtocol> presenter;

@end
