//
//  NITBarcodeScannerPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBarcodeScannerProtocols.h"

@class NITBarcodeScannerWireFrame;

@interface NITBarcodeScannerPresenter : NITRootPresenter <NITBarcodeScannerPresenterProtocol, NITBarcodeScannerInteractorOutputProtocol>

@property (nonatomic, weak) id <NITBarcodeScannerViewProtocol> view;
@property (nonatomic, strong) id <NITBarcodeScannerInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBarcodeScannerWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * title;

@end
