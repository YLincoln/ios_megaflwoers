//
//  NITBarcodeScannerPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBarcodeScannerPresenter.h"
#import "NITBarcodeScannerWireframe.h"

@implementation NITBarcodeScannerPresenter

#pragma mark - NITBarcodeScannerPresenterProtocol
- (void)scanCode:(NSString *)scannedCode
{
    [self.wireFrame dismissView:self.view withBarcode:scannedCode];
}

- (void)closeAction
{
    [self.wireFrame dismissView:self.view withBarcode:nil];
}

@end
