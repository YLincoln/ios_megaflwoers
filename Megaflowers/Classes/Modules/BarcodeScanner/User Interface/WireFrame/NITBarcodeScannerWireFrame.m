//
//  NITBarcodeScannerWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBarcodeScannerWireFrame.h"

@implementation NITBarcodeScannerWireFrame

+ (id)createNITBarcodeScannerModuleWithBlock:(ScanBarcodeBlock)block
{
    // Generating module components
    id <NITBarcodeScannerPresenterProtocol, NITBarcodeScannerInteractorOutputProtocol> presenter = [NITBarcodeScannerPresenter new];
    id <NITBarcodeScannerInteractorInputProtocol> interactor = [NITBarcodeScannerInteractor new];
    id <NITBarcodeScannerAPIDataManagerInputProtocol> APIDataManager = [NITBarcodeScannerAPIDataManager new];
    id <NITBarcodeScannerLocalDataManagerInputProtocol> localDataManager = [NITBarcodeScannerLocalDataManager new];
    id <NITBarcodeScannerWireFrameProtocol> wireFrame= [NITBarcodeScannerWireFrame new];
    id <NITBarcodeScannerViewProtocol> view = [(NITBarcodeScannerWireFrame *)wireFrame createViewControllerWithKey:_s(NITBarcodeScannerViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    wireFrame.scanBlock = block;
    
    return view;
}

#pragma mark - NITBarcodeScannerWireFrameProtocol
+ (void)presentNITBarcodeScannerModuleFrom:(UIViewController *)fromViewController title:(NSString *)title handler:(ScanBarcodeBlock)handler
{
    NITBarcodeScannerViewController * vc = [NITBarcodeScannerWireFrame createNITBarcodeScannerModuleWithBlock:handler];
    vc.presenter.title = title;
    [fromViewController presentViewController:vc animated:true completion:false];
}

- (void)dismissView:(UIViewController *)viewController withBarcode:(NSString *)barcode
{
    [viewController dismissViewControllerAnimated:true completion:^{
    
        if (self.scanBlock && barcode.length > 0)
        {
            self.scanBlock(barcode);
        }
    }];
}

@end
