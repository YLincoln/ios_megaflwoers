//
//  NITBarcodeScannerWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/09/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBarcodeScannerProtocols.h"
#import "NITBarcodeScannerViewController.h"
#import "NITBarcodeScannerLocalDataManager.h"
#import "NITBarcodeScannerAPIDataManager.h"
#import "NITBarcodeScannerInteractor.h"
#import "NITBarcodeScannerPresenter.h"
#import "NITBarcodeScannerWireframe.h"
#import "NITRootWireframe.h"

@interface NITBarcodeScannerWireFrame : NITRootWireframe <NITBarcodeScannerWireFrameProtocol>

@property (nonatomic) ScanBarcodeBlock scanBlock;

@end
