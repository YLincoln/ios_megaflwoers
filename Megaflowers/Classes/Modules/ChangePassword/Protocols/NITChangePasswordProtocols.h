//
//  NITChangePasswordProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordModel.h"

@protocol NITChangePasswordInteractorOutputProtocol;
@protocol NITChangePasswordInteractorInputProtocol;
@protocol NITChangePasswordViewProtocol;
@protocol NITChangePasswordPresenterProtocol;
@protocol NITChangePasswordLocalDataManagerInputProtocol;
@protocol NITChangePasswordAPIDataManagerInputProtocol;

@class NITChangePasswordWireFrame;

typedef CF_ENUM (NSUInteger, ChangePasswordMode) {
    ChangePasswordModeNew   = 0,
    ChangePasswordModeReset = 1
};

@protocol NITChangePasswordDelegate <NSObject>

- (void)didChangePassword:(NSString *)password;

@end

@protocol NITChangePasswordViewProtocol
@required
@property (nonatomic, strong) id <NITChangePasswordPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITChangePasswordModel *)model;
- (void)hideViewIfNeed:(BOOL)need;
@end

@protocol NITChangePasswordWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITChangePasswordModuleFrom:(id)fromView withDelegate:(id)delegate;
+ (void)presentNITChangePasswordModuleFrom:(id)fromView withDelegate:(id)delegate pincode:(NSString *)pincode token:(NSString *)token;
- (void)backActionFrom:(id)fromView toRoot:(BOOL)toRoot;;
@end

@protocol NITChangePasswordPresenterProtocol
@required
@property (nonatomic, weak) id <NITChangePasswordViewProtocol> view;
@property (nonatomic, strong) id <NITChangePasswordInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITChangePasswordWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITChangePasswordDelegate> delegate;
@property (nonatomic) ChangePasswordMode mode;
@property (nonatomic) NSString * token;
@property (nonatomic) NSString * pincode;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)changePassword;
@end

@protocol NITChangePasswordInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITChangePasswordInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITChangePasswordInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITChangePasswordAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITChangePasswordLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)setNewPassword:(NSString *)password pincode:(NSString *)pincode token:(NSString *)token withHandler:(void (^)(BOOL success, NSString *error))handler;
- (void)setNewPasswordByModel:(NITChangePasswordModel *)model withHandler:(void (^)(BOOL, NSString *))handler;
- (void)loginUserByPhone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(BOOL success))handler;
@end


@protocol NITChangePasswordDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITChangePasswordAPIDataManagerInputProtocol <NITChangePasswordDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)setNewPassword:(NSString *)password pincode:(NSString *)pincode token:(NSString *)token withHandler:(void (^)(BOOL success, NSString *error))handler;
- (void)setNewPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword oldPassword:(NSString *)oldPassword withHandler:(void (^)(BOOL success, NSString *error))handler;
@end

@protocol NITChangePasswordLocalDataManagerInputProtocol <NITChangePasswordDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
