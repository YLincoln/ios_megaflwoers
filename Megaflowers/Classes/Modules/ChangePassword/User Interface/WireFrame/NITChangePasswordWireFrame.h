//
//  NITChangePasswordWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordProtocols.h"
#import "NITChangePasswordViewController.h"
#import "NITChangePasswordLocalDataManager.h"
#import "NITChangePasswordAPIDataManager.h"
#import "NITChangePasswordInteractor.h"
#import "NITChangePasswordPresenter.h"
#import "NITChangePasswordWireframe.h"
#import "NITRootWireframe.h"

@interface NITChangePasswordWireFrame : NITRootWireframe <NITChangePasswordWireFrameProtocol>

@end
