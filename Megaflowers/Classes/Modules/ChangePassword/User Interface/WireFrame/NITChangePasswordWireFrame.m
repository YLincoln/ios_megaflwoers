//
//  NITChangePasswordWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordWireFrame.h"

@implementation NITChangePasswordWireFrame

+ (id)createNITChangePasswordModule
{
    // Generating module components
    id <NITChangePasswordPresenterProtocol, NITChangePasswordInteractorOutputProtocol> presenter = [NITChangePasswordPresenter new];
    id <NITChangePasswordInteractorInputProtocol> interactor = [NITChangePasswordInteractor new];
    id <NITChangePasswordAPIDataManagerInputProtocol> APIDataManager = [NITChangePasswordAPIDataManager new];
    id <NITChangePasswordLocalDataManagerInputProtocol> localDataManager = [NITChangePasswordLocalDataManager new];
    id <NITChangePasswordWireFrameProtocol> wireFrame= [NITChangePasswordWireFrame new];
    id <NITChangePasswordViewProtocol> view = [(NITChangePasswordWireFrame *)wireFrame createViewControllerWithKey:_s(NITChangePasswordViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITChangePasswordModuleFrom:(UIViewController *)fromViewController withDelegate:(id)delegate
{
    NITChangePasswordViewController * view = [NITChangePasswordWireFrame createNITChangePasswordModule];
    view.presenter.delegate = delegate;
    view.presenter.mode = ChangePasswordModeNew;
    
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITChangePasswordModuleFrom:(UIViewController *)fromViewController withDelegate:(id)delegate pincode:(NSString *)pincode token:(NSString *)token
{
    NITChangePasswordViewController * view = [NITChangePasswordWireFrame createNITChangePasswordModule];
    view.presenter.delegate = delegate;
    view.presenter.pincode = pincode;
    view.presenter.token = token;
    view.presenter.mode = ChangePasswordModeReset;
    
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromView toRoot:(BOOL)toRoot
{
    if (toRoot)
    {
        [fromView.navigationController popToRootViewControllerAnimated:true];
    }
    else
    {
        [fromView.navigationController popViewControllerAnimated:true];
    }
}

@end
