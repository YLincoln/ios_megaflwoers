//
//  NITChangePasswordViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordViewController.h"

@interface NITChangePasswordViewController ()

@property (nonatomic) IBOutlet UITextField * oldPassword;
@property (nonatomic) IBOutlet UITextField * password;
@property (nonatomic) IBOutlet UITextField * checkPassword;
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray * hidenConstraints;

@property (nonatomic) NITChangePasswordModel * model;

@end

@implementation NITChangePasswordViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)changeAction:(id)sender
{
    [self.presenter changePassword];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Сменить пароль", nil);
    
    [self setKeyboardActiv:true];
}

#pragma mark - NITChangePasswordPresenterProtocol
- (void)reloadDataByModel:(NITChangePasswordModel *)model
{
    self.model = model;
    
    self.oldPassword.textColor = self.model.oldPasswordHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.oldPassword.text = self.model.oldPassword;
    self.oldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.oldPassword.placeholder
                                                                             attributes:@{NSForegroundColorAttributeName:
                                                                                              self.model.oldPasswordHighlight ? RGB(217, 67, 67) : RGB(164, 183, 171)}];
    
    self.password.textColor = self.model.passwordHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.password.text = self.model.password;
    self.password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.password.placeholder
                                                                          attributes:@{NSForegroundColorAttributeName:
                                                                                           self.model.passwordHighlight ? RGB(217, 67, 67) : RGB(164, 183, 171)}];
    
    self.checkPassword.textColor = self.model.checkPasswordHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.checkPassword.text = self.model.checkPassword;
    self.checkPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.checkPassword.placeholder
                                                                               attributes:@{NSForegroundColorAttributeName:
                                                                                                self.model.checkPasswordHighlight ? RGB(217, 67, 67) : RGB(164, 183, 171)}];
}

- (void)hideViewIfNeed:(BOOL)need
{
    if (need)
    {
        for (NSLayoutConstraint * c in self.hidenConstraints)
        {
            c.constant = 0;
        }
        [self.view layoutIfNeeded];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder * nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder)
    {
        [nextResponder becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return false;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    textField.textColor = RGB(10, 88, 43);

    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case 100:
            self.model.oldPasswordHighlight = false;
            self.model.oldPassword = textField.text;
            break;
            
        case 101:
            self.model.passwordHighlight = false;
            self.model.password = textField.text;
            break;
            
        case 102:
            self.model.checkPasswordHighlight = false;
            self.model.checkPassword = textField.text;
            break;
    }
}


@end
