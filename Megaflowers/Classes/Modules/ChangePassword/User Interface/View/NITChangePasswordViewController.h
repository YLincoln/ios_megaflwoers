//
//  NITChangePasswordViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITChangePasswordProtocols.h"

@interface NITChangePasswordViewController : NITBaseViewController <NITChangePasswordViewProtocol>

@property (nonatomic, strong) id <NITChangePasswordPresenterProtocol> presenter;

@end
