//
//  NITChangePasswordPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITChangePasswordProtocols.h"

@class NITChangePasswordWireFrame;

@interface NITChangePasswordPresenter : NITRootPresenter <NITChangePasswordPresenterProtocol, NITChangePasswordInteractorOutputProtocol>

@property (nonatomic, weak) id <NITChangePasswordViewProtocol> view;
@property (nonatomic, strong) id <NITChangePasswordInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITChangePasswordWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITChangePasswordDelegate> delegate;
@property (nonatomic) ChangePasswordMode mode;
@property (nonatomic) NSString * token;
@property (nonatomic) NSString * pincode;

@end
