//
//  NITChangePasswordModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordModel.h"
#import "NITChangePasswordProtocols.h"

@implementation NITChangePasswordModel

- (BOOL)isCorrect
{
    BOOL correct = true;
    
    switch (self.mode) {
        case ChangePasswordModeNew: {
            
            if ([self.oldPassword length] == 0 || ![self.oldPassword isEqualToString:USER.password])
            {
                self.oldPasswordHighlight = true;
                correct = false;
            }
        }
            break;
            
        default:
            break;
    }
    
    if ([self.password length] == 0)
    {
        self.passwordHighlight = true;
        correct = false;
    }

    if (![self.password isEqualToString:self.checkPassword] || [self.checkPassword length] == 0)
    {
        self.checkPasswordHighlight = true;
        correct = false;
    }
    
    return correct;
}

@end
