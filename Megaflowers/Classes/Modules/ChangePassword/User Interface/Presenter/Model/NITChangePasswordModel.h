//
//  NITChangePasswordModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@interface NITChangePasswordModel : NSObject

@property (nonatomic) NSInteger mode;
@property (nonatomic) NSString * oldPassword;
@property (nonatomic) NSString * password;
@property (nonatomic) NSString * checkPassword;
@property (nonatomic) BOOL oldPasswordHighlight;
@property (nonatomic) BOOL passwordHighlight;
@property (nonatomic) BOOL checkPasswordHighlight;
@property (nonatomic) BOOL isCorrect;

@end
