//
//  NITChangePasswordPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordPresenter.h"
#import "NITChangePasswordWireframe.h"

@interface NITChangePasswordPresenter ()

@property (nonatomic) NITChangePasswordModel * model;

@end

@implementation NITChangePasswordPresenter

- (void)initData
{
    self.model = [NITChangePasswordModel new];
    self.model.mode = self.mode;
    [self.view reloadDataByModel:self.model];
    
    switch (self.mode) {
        case ChangePasswordModeNew:
            [self.view hideViewIfNeed:false];
            break;
            
        case ChangePasswordModeReset:
            [self.view hideViewIfNeed:true];
            break;
    }
}

- (void)changePassword
{
    if ([self.model isCorrect])
    {
        weaken(self);
        switch (self.mode) {
            case ChangePasswordModeNew: {
                
                [self.interactor setNewPasswordByModel:self.model withHandler:^(BOOL success, NSString *error) {
                    if (success)
                    {
                        USER.password = weakSelf.model.password;
                        
                        if ([weakSelf.delegate respondsToSelector:@selector(didChangePassword:)])
                        {
                            [weakSelf.delegate didChangePassword:weakSelf.model.password];
                        }
                        
                        [weakSelf.wireFrame backActionFrom:weakSelf.view toRoot:false];
                    }
                    else
                    {
                        [weakSelf showError:error];
                    }
                }];
            }
                break;
                
            case ChangePasswordModeReset: {
                
                [self.interactor setNewPassword:self.model.password pincode:self.pincode token:self.token withHandler:^(BOOL success, NSString *error) {
                    if (success)
                    {
                        USER.password = weakSelf.model.password;
                        [weakSelf.interactor loginUserByPhone:USER.phone password:weakSelf.model.password withHandler:^(BOOL success) {
                            [weakSelf.wireFrame backActionFrom:weakSelf.view toRoot:true];
                        }];
                    }
                    else
                    {
                        [weakSelf showError:error];
                    }
                }];
            }
                break;
        }
    }
    else
    {
        [self.view reloadDataByModel:self.model];
    }
}

- (void)showError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ok", nil) actionBlock:^{}];
    [HELPER showActionSheetFromView:self.view withTitle:error actionButtons:nil cancelButton:cancel];
}

@end
