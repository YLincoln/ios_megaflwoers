//
//  NITChangePasswordInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordInteractor.h"
#import "NITLoginInteractor.h"
#import "NITLoginAPIDataManager.h"

@implementation NITChangePasswordInteractor

- (void)setNewPassword:(NSString *)password pincode:(NSString *)pincode token:(NSString *)token withHandler:(void (^)(BOOL success, NSString *error))handler
{
    [self.APIDataManager setNewPassword:password pincode:pincode token:token withHandler:handler];
}

- (void)setNewPasswordByModel:(NITChangePasswordModel *)model withHandler:(void (^)(BOOL, NSString *))handler
{
    [self.APIDataManager setNewPassword:model.password confirmPassword:model.checkPassword oldPassword:model.oldPassword withHandler:handler];
}

- (void)loginUserByPhone:(NSString *)phone password:(NSString *)password withHandler:(void(^)(BOOL success))handler
{
    NSString * phoneNumber = [self checkPhone:phone];
    NITLoginInteractor * loginInteractor = [NITLoginInteractor new];
    loginInteractor.APIDataManager = [NITLoginAPIDataManager new];
    [loginInteractor loginByEmail:nil phone:phoneNumber password:password withHandler:^(BOOL success, NSError *error) {
        handler(success);
    }];
}

#pragma mark - Private
- (NSString *)checkPhone:(NSString *)phone
{
    return [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
}

@end
