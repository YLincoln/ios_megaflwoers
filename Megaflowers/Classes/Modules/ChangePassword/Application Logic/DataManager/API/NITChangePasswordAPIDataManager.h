//
//  NITChangePasswordAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITChangePasswordProtocols.h"

@interface NITChangePasswordAPIDataManager : NSObject <NITChangePasswordAPIDataManagerInputProtocol>

@end
