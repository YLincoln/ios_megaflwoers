//
//  NITChangePasswordAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/28/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangePasswordAPIDataManager.h"
#import "SWGAccountApi.h"
#import "SWGUserApi.h"

@implementation NITChangePasswordAPIDataManager

- (void)setNewPassword:(NSString *)password pincode:(NSString *)pincode token:(NSString *)token withHandler:(void (^)(BOOL success, NSString *error))handler
{
    [HELPER startLoading];
    [[SWGAccountApi new] accountPasswordPostWithTokenId:token token:pincode password:password lang:HELPER.lang site:API.site completionHandler:^(NSString *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:THIS_METHOD];
            handler(false, error.interfaceDescription);
        }
        else
        {
            handler(true, nil);
        }
    }];
}

- (void)setNewPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword oldPassword:(NSString *)oldPassword withHandler:(void (^)(BOOL success, NSString *error))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userPasswordPostWithOldPassword:oldPassword password:password confirmPassword:oldPassword completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:THIS_METHOD];
            handler(false, error.interfaceDescription);
        }
        else
        {
            handler(true, nil);
        }
    }];
}

@end
