//
//  NITAppFeedbackAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAppFeedbackAPIDataManager.h"
#import "SWGFeedbackApi.h"

@implementation NITAppFeedbackAPIDataManager

#pragma mark - NITAppFeedbackAPIDataManagerInputProtocol
- (void)sendFeedback:(SWGFeedbackRequest *)feedback withHandler:(void(^)(NSError *error))handler
{
    [HELPER startLoading];
    [[SWGFeedbackApi new] feedbackPostWithSite:API.site lang:HELPER.lang feedback:feedback city:USER.cityID completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (handler) handler(error);
    }];
}

@end
