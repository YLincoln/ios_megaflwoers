//
//  NITAppFeedbackLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITAppFeedbackProtocols.h"

@interface NITAppFeedbackLocalDataManager : NSObject <NITAppFeedbackLocalDataManagerInputProtocol>

@end
