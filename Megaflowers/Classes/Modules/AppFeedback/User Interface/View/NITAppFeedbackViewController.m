//
//  NITAppFeedbackViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAppFeedbackViewController.h"

@interface NITAppFeedbackViewController ()
<
UITextFieldDelegate,
UITextViewDelegate
>

@property (nonatomic) IBOutlet UITextField * nameField;
@property (nonatomic) IBOutlet UITextField * emailField;
@property (nonatomic) IBOutlet UITextView * messageTextView;
@property (nonatomic) IBOutlet UIButton * sendBtn;

@property (nonatomic) NITFeedbackCreateModel * model;

@end

@implementation NITAppFeedbackViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)sendAction:(id)sender
{
    [self.presenter sendFeedback];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Отзыв", nil);
    
    [self setKeyboardActiv:true];
    
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.emailField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
    self.messageTextView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Ваш отзыв", nil)
                                                                                 attributes:@{NSForegroundColorAttributeName:RGB(164, 183, 171)}];
}

- (void)updateModelByField:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 102: // email
            self.model.email = textField.text;
            break;
            
        case 103: // phone
            self.model.phone = textField.text;
            break;
            
        default:
            break;
    }
}

#pragma mark - NITAppFeedbackPresenterProtocol
- (void)reloadDataByModel:(NITFeedbackCreateModel *)model
{
    self.model = model;
    
    self.nameField.textColor = self.model.nameHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.nameField.text = model.name;
    self.nameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameField.placeholder
                                                                           attributes:@{NSForegroundColorAttributeName:
                                                                                            self.model.nameHighlight ? RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.emailField.textColor = self.model.emailHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.emailField.text = self.model.email;
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.emailField.placeholder
                                                                            attributes:@{NSForegroundColorAttributeName:
                                                                                             self.model.emailHighlight ? RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
    
    self.messageTextView.textColor = self.model.messageHighlight ? RGB(217, 67, 67) : RGB(10, 88, 43);
    self.messageTextView.text = self.model.message;
    self.messageTextView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Ваш отзыв", nil)
                                                                                 attributes:@{NSForegroundColorAttributeName:
                                                                                                  self.model.messageHighlight ? RGBA(217, 67, 67, 0.4) : RGB(164, 183, 171)}];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    textField.textColor = RGB(10, 88, 43);
    [self updateModelByField:textField];
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateModelByField:textField];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    textView.textColor = RGB(10, 88, 43);
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView isEqual:self.messageTextView])
    {
      self.model.message = textView.text;
    }
}

@end
