//
//  NITAppFeedbackViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITAppFeedbackProtocols.h"

@interface NITAppFeedbackViewController : NITBaseViewController <NITAppFeedbackViewProtocol>

@property (nonatomic, strong) id <NITAppFeedbackPresenterProtocol> presenter;

@end
