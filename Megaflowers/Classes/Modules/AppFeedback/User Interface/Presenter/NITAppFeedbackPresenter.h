//
//  NITAppFeedbackPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITAppFeedbackProtocols.h"

@class NITAppFeedbackWireFrame;

@interface NITAppFeedbackPresenter : NITRootPresenter <NITAppFeedbackPresenterProtocol, NITAppFeedbackInteractorOutputProtocol>

@property (nonatomic, weak) id <NITAppFeedbackViewProtocol> view;
@property (nonatomic, strong) id <NITAppFeedbackInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITAppFeedbackWireFrameProtocol> wireFrame;

@end
