//
//  NITAppFeedbackPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAppFeedbackPresenter.h"
#import "NITAppFeedbackWireframe.h"

@interface NITAppFeedbackPresenter ()

@property (nonatomic) NITFeedbackCreateModel * model;

@end

@implementation NITAppFeedbackPresenter

#pragma mark - NITAppFeedbackPresenterProtocol
- (void)initData
{
    self.model = [NITFeedbackCreateModel new];
    [self.view reloadDataByModel:self.model];
}

- (void)sendFeedback
{
    if ([self.model isCorrect])
    {
        [self.interactor sendFeedbackByModel:self.model];
    }
    else
    {
        [self.view reloadDataByModel:self.model];
    }
}

#pragma mark - NITFeedbackCreateInteractorOutputProtocol
- (void)feedbackSended
{
    weaken(self);
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{
        [weakSelf.wireFrame backFrom:weakSelf.view];
    }];

    [HELPER showActionSheetFromView:self.view
                          withTitle:NSLocalizedString(@"Спасибо за ваш отзыв!\nВы делаете нас лучше!", nil)
                      actionButtons:@[]
                       cancelButton:cancel];
}

- (void)feedbackSendError:(NSString *)error
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Ок", nil) actionBlock:^{}];
    
    [HELPER showActionSheetFromView:self.view
                          withTitle:error
                      actionButtons:@[]
                       cancelButton:cancel];
}

@end
