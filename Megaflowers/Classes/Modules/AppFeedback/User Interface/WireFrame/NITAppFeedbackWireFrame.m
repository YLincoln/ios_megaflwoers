//
//  NITAppFeedbackWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAppFeedbackWireFrame.h"

@implementation NITAppFeedbackWireFrame

+ (id)createNITAppFeedbackModule
{
    // Generating module components
    id <NITAppFeedbackPresenterProtocol, NITAppFeedbackInteractorOutputProtocol> presenter = [NITAppFeedbackPresenter new];
    id <NITAppFeedbackInteractorInputProtocol> interactor = [NITAppFeedbackInteractor new];
    id <NITAppFeedbackAPIDataManagerInputProtocol> APIDataManager = [NITAppFeedbackAPIDataManager new];
    id <NITAppFeedbackLocalDataManagerInputProtocol> localDataManager = [NITAppFeedbackLocalDataManager new];
    id <NITAppFeedbackWireFrameProtocol> wireFrame= [NITAppFeedbackWireFrame new];
    id <NITAppFeedbackViewProtocol> view = [(NITAppFeedbackWireFrame *)wireFrame createViewControllerWithKey:_s(NITAppFeedbackViewController) storyboardType:StoryboardTypeGuide];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITAppFeedbackWireFrameProtocol
+ (void)presentNITAppFeedbackModuleFrom:(UIViewController *)fromViewController
{
    NITAppFeedbackViewController * vc = [NITAppFeedbackWireFrame createNITAppFeedbackModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)backFrom:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
