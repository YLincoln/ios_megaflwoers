//
//  NITAppFeedbackWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAppFeedbackProtocols.h"
#import "NITAppFeedbackViewController.h"
#import "NITAppFeedbackLocalDataManager.h"
#import "NITAppFeedbackAPIDataManager.h"
#import "NITAppFeedbackInteractor.h"
#import "NITAppFeedbackPresenter.h"
#import "NITAppFeedbackWireframe.h"
#import "NITRootWireframe.h"

@interface NITAppFeedbackWireFrame : NITRootWireframe <NITAppFeedbackWireFrameProtocol>

@end
