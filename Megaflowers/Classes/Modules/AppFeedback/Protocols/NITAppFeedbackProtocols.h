//
//  NITAppFeedbackProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/21/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCreateModel.h"
#import "SWGFeedbackRequest.h"

@protocol NITAppFeedbackInteractorOutputProtocol;
@protocol NITAppFeedbackInteractorInputProtocol;
@protocol NITAppFeedbackViewProtocol;
@protocol NITAppFeedbackPresenterProtocol;
@protocol NITAppFeedbackLocalDataManagerInputProtocol;
@protocol NITAppFeedbackAPIDataManagerInputProtocol;

@class NITAppFeedbackWireFrame;

@protocol NITAppFeedbackViewProtocol
@required
@property (nonatomic, strong) id <NITAppFeedbackPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITFeedbackCreateModel *)model;
@end

@protocol NITAppFeedbackWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITAppFeedbackModuleFrom:(id)fromView;
- (void)backFrom:(id)fromView;
@end

@protocol NITAppFeedbackPresenterProtocol
@required
@property (nonatomic, weak) id <NITAppFeedbackViewProtocol> view;
@property (nonatomic, strong) id <NITAppFeedbackInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITAppFeedbackWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)sendFeedback;
@end

@protocol NITAppFeedbackInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)feedbackSended;
- (void)feedbackSendError:(NSString *)error;
@end

@protocol NITAppFeedbackInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITAppFeedbackInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITAppFeedbackAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITAppFeedbackLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)sendFeedbackByModel:(NITFeedbackCreateModel *)model;
@end


@protocol NITAppFeedbackDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITAppFeedbackAPIDataManagerInputProtocol <NITAppFeedbackDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)sendFeedback:(SWGFeedbackRequest *)feedback withHandler:(void(^)(NSError *error))handler;
@end

@protocol NITAppFeedbackLocalDataManagerInputProtocol <NITAppFeedbackDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
