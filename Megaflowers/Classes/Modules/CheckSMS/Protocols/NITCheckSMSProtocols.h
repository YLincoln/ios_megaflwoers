//
//  NITCheckSMSProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITCheckSMSInteractorOutputProtocol;
@protocol NITCheckSMSInteractorInputProtocol;
@protocol NITCheckSMSViewProtocol;
@protocol NITCheckSMSPresenterProtocol;
@protocol NITCheckSMSLocalDataManagerInputProtocol;
@protocol NITCheckSMSAPIDataManagerInputProtocol;

@class NITCheckSMSWireFrame;

typedef CF_ENUM (NSUInteger, CheckSMSState) {
    CheckSMSStateNormal = 0,
    CheckSMSStateError  = 1
};

typedef CF_ENUM (NSUInteger, SMSRecoveryMode) {
    SMSRecoveryModeActivateAccount = 0,
    SMSRecoveryModeResetPassword   = 1,
    SMSRecoveryModeChangeNamber    = 2
};

@protocol NITCheckSMSDelegate <NSObject>

- (void)didCheckSMS:(BOOL)success;

@end

@protocol NITCheckSMSViewProtocol
@required
@property (nonatomic, strong) id <NITCheckSMSPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadTitle:(NSString *)title;
- (void)reloadState:(CheckSMSState)state andPhone:(NSString *)phone;
- (void)updateTimerString:(NSString *)timerString;
- (void)updateResendAvailability:(BOOL)available;
@end

@protocol NITCheckSMSWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCheckSMSModuleFrom:(id)fromView
                        withDelegate:(id)delegate
                                mode:(SMSRecoveryMode)mode
                               phone:(NSString *)phone
                               token:(NSString *)token;
- (void)backActionFrom:(id)fromView;
- (void)setNewPasswordByToken:(NSString *)token pincode:(NSString *)pincode from:(id)fromView;
@end

@protocol NITCheckSMSPresenterProtocol
@required
@property (nonatomic, weak) id <NITCheckSMSViewProtocol> view;
@property (nonatomic, strong) id <NITCheckSMSInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCheckSMSWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITCheckSMSDelegate> delegate;
@property (nonatomic) SMSRecoveryMode mode;
@property (nonatomic) NSString * phoneNumber;
@property (nonatomic) NSString * smsCode;
@property (nonatomic) NSString * token;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)sendCode:(NSString *)code;
- (void)resendCode;
@end

@protocol NITCheckSMSInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITCheckSMSInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCheckSMSInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCheckSMSAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCheckSMSLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)sendSMSForPhone:(NSString *)phone;
- (void)activateByPhone:(NSString *)phone smsCode:(NSString *)smsCode withHahdler:(void(^)(BOOL success, NSString * error))handler;
@end


@protocol NITCheckSMSDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCheckSMSAPIDataManagerInputProtocol <NITCheckSMSDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)sendSMSForPhone:(NSString *)phone;
- (void)activateByPhone:(NSString *)phone smsCode:(NSString *)smsCode withHahdler:(void(^)(BOOL success, NSString * result))handler;
@end

@protocol NITCheckSMSLocalDataManagerInputProtocol <NITCheckSMSDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
