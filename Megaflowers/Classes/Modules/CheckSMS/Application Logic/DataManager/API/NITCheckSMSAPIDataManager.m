//
//  NITCheckSMSAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckSMSAPIDataManager.h"
#import "SWGAccountApi.h"

@implementation NITCheckSMSAPIDataManager

- (void)sendSMSForPhone:(NSString *)phone
{
    [HELPER startLoading];
    [[SWGAccountApi new] accountVerifyPhonePostWithPhone:phone lang:HELPER.lang site:API.site completionHandler:^(NSString *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:THIS_METHOD];
        }
    }];
}

- (void)activateByPhone:(NSString *)phone smsCode:(NSString *)smsCode withHahdler:(void (^)(BOOL success, NSString *error))handler
{
    [HELPER startLoading];
    [[SWGAccountApi new] accountActivatePhonePostWithPhone:phone token:smsCode lang:HELPER.lang site:API.site completionHandler:^(NSString *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:THIS_METHOD];
            handler(false, error.interfaceDescription);
        }
        else
        {
            handler(true, nil);
        }
    }];
}

@end
