//
//  NITCheckSMSLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCheckSMSProtocols.h"

@interface NITCheckSMSLocalDataManager : NSObject <NITCheckSMSLocalDataManagerInputProtocol>

@end
