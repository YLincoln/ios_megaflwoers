//
//  NITCheckSMSInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckSMSInteractor.h"

@implementation NITCheckSMSInteractor

- (void)sendSMSForPhone:(NSString *)phone
{
    [self.APIDataManager sendSMSForPhone:phone];
}

- (void)activateByPhone:(NSString *)phone smsCode:(NSString *)smsCode withHahdler:(void (^)(BOOL success, NSString *error))handler
{
    NSString * phoneNumber = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
    [self.APIDataManager activateByPhone:phoneNumber smsCode:smsCode withHahdler:^(BOOL success, NSString *error) {
        handler(success, error);
    }];
}

@end
