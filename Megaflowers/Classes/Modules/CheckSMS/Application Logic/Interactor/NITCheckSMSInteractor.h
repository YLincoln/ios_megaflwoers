//
//  NITCheckSMSInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCheckSMSProtocols.h"

@interface NITCheckSMSInteractor : NSObject <NITCheckSMSInteractorInputProtocol>

@property (nonatomic, weak) id <NITCheckSMSInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCheckSMSAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCheckSMSLocalDataManagerInputProtocol> localDataManager;

@end
