//
//  NITCheckSMSWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckSMSProtocols.h"
#import "NITCheckSMSViewController.h"
#import "NITCheckSMSLocalDataManager.h"
#import "NITCheckSMSAPIDataManager.h"
#import "NITCheckSMSInteractor.h"
#import "NITCheckSMSPresenter.h"
#import "NITCheckSMSWireframe.h"
#import "NITRootWireframe.h"

@interface NITCheckSMSWireFrame : NITRootWireframe <NITCheckSMSWireFrameProtocol>

@end
