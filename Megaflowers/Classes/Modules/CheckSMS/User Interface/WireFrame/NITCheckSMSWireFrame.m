//
//  NITCheckSMSWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckSMSWireFrame.h"
#import "NITChangePasswordWireFrame.h"

@implementation NITCheckSMSWireFrame

+ (id)createNITCheckSMSModule
{
    // Generating module components
    id <NITCheckSMSPresenterProtocol, NITCheckSMSInteractorOutputProtocol> presenter = [NITCheckSMSPresenter new];
    id <NITCheckSMSInteractorInputProtocol> interactor = [NITCheckSMSInteractor new];
    id <NITCheckSMSAPIDataManagerInputProtocol> APIDataManager = [NITCheckSMSAPIDataManager new];
    id <NITCheckSMSLocalDataManagerInputProtocol> localDataManager = [NITCheckSMSLocalDataManager new];
    id <NITCheckSMSWireFrameProtocol> wireFrame= [NITCheckSMSWireFrame new];
    id <NITCheckSMSViewProtocol> view = [(NITCheckSMSWireFrame *)wireFrame createViewControllerWithKey:_s(NITCheckSMSViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITCheckSMSModuleFrom:(UIViewController *)fromViewController
                        withDelegate:(id)delegate
                                mode:(SMSRecoveryMode)mode
                               phone:(NSString *)phone
                               token:(NSString *)token
{
    NITCheckSMSViewController * view = [self createNITCheckSMSModule];
    view.presenter.delegate = delegate;
    view.presenter.phoneNumber = phone;
    view.presenter.mode = mode;
    view.presenter.token = token;
    
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)backActionFrom:(UIViewController *)fromView
{
    [fromView.navigationController popViewControllerAnimated:true];
}

- (void)setNewPasswordByToken:(NSString *)token pincode:(NSString *)pincode from:(UIViewController *)fromViewController
{
    [NITChangePasswordWireFrame presentNITChangePasswordModuleFrom:fromViewController withDelegate:nil pincode:pincode token:token];
}

@end
