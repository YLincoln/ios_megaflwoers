//
//  NITCheckSMSPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCheckSMSProtocols.h"

@class NITCheckSMSWireFrame;

@interface NITCheckSMSPresenter : NITRootPresenter <NITCheckSMSPresenterProtocol, NITCheckSMSInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCheckSMSViewProtocol> view;
@property (nonatomic, strong) id <NITCheckSMSInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCheckSMSWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITCheckSMSDelegate> delegate;
@property (nonatomic) SMSRecoveryMode mode;
@property (nonatomic) NSString * phoneNumber;
@property (nonatomic) NSString * smsCode;
@property (nonatomic) NSString * token;

@end
