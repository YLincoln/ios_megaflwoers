//
//  NITCheckSMSPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckSMSPresenter.h"
#import "NITCheckSMSWireframe.h"

static NSInteger const resendPeriod = 300; // sec

@interface NITCheckSMSPresenter ()

@property (nonatomic) NSTimer * resendTimer;
@property (nonatomic) NSInteger seconds;

@end

@implementation NITCheckSMSPresenter

- (void)initData
{
    self.seconds = 0;
    [self startUpdate];
    [self.view reloadState:CheckSMSStateNormal andPhone:self.phoneNumber];
    
    [self.interactor sendSMSForPhone:self.phoneNumber];
    
    switch (self.mode) {
        case SMSRecoveryModeActivateAccount:
            [self.view reloadTitle:NSLocalizedString(@"Подтверждение регистрации", nil)];
            break;
            
        case SMSRecoveryModeResetPassword:
            [self.view reloadTitle:NSLocalizedString(@"Подтверждение сброса пароля", nil)];
            break;
            
        case SMSRecoveryModeChangeNamber:
            [self.view reloadTitle:NSLocalizedString(@"Подтверждение изменения пароля", nil)];
            break;
    }
}

- (void)sendCode:(NSString *)code
{
    if (code.length > 0)
    {
        weaken(self);
        switch (self.mode) {
            case SMSRecoveryModeActivateAccount: {
                
                [self.interactor activateByPhone:self.phoneNumber smsCode:code withHahdler:^(BOOL success, NSString *error) {
                    if (success)
                    {
                        [weakSelf.wireFrame backActionFrom:weakSelf.view];
                        if ([weakSelf.delegate respondsToSelector:@selector(didCheckSMS:)])
                        {
                            [weakSelf.delegate didCheckSMS:true];
                        }
                    }
                    else
                    {
                        [weakSelf.view reloadState:CheckSMSStateError andPhone:weakSelf.phoneNumber];
                    }
                }];
            }
                break;
                
            case SMSRecoveryModeResetPassword: {
               
                [self.wireFrame setNewPasswordByToken:self.token pincode:code from:self.view];
            }
                break;
                
            case SMSRecoveryModeChangeNamber: {
                
            }
                break;
        }
        


    }
    else
    {
        [self.view reloadState:CheckSMSStateError andPhone:self.phoneNumber];
    }
}

- (void)resendCode
{
    // resend code
    self.seconds = 0;
    [self.interactor sendSMSForPhone:self.phoneNumber];
}

#pragma mark - Private
- (void)startUpdate
{
    if (!self.resendTimer)
    {
        self.resendTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(update) userInfo:nil repeats:true];
    }
}

- (void)stopUpdate
{
    [self.resendTimer invalidate];
    self.resendTimer = nil;
}

- (void)update
{
    self.seconds++;
    [self.view updateResendAvailability:self.seconds > resendPeriod];
    [self.view updateTimerString:[self timeFormatted:self.seconds]];
}

- (NSString *)timeFormatted:(NSInteger)totalSeconds
{
    NSInteger seconds = totalSeconds % 60;
    NSInteger minutes = (totalSeconds / 60) % 60;
    
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

- (void)dealloc
{
    [self stopUpdate];
}

@end
