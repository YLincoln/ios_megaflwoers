//
//  NITCheckSMSViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCheckSMSViewController.h"

static NSInteger const smsCodeLength = 4;

@interface NITCheckSMSViewController ()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UILabel * errorMesage;
@property (nonatomic) IBOutlet UILabel * phoneNumber;
@property (nonatomic) IBOutlet UILabel * timerLbl;
@property (nonatomic) IBOutlet UITextField * codeField;
@property (nonatomic) IBOutlet UIButton * resendBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *errorMessageTop;

@end

@implementation NITCheckSMSViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}


#pragma mark - IBAction
- (IBAction)resendCodeAction:(id)sender
{
    [self.presenter resendCode];
}

- (IBAction)sendCodeAction:(id)sender
{
    [self.presenter sendCode:self.codeField.text];
}

#pragma mark - Private
- (void)initUI
{    
    [self setKeyboardActiv:true];
}

#pragma mark - NITCheckSMSPresenterProtocol
- (void)reloadTitle:(NSString *)title
{
    self.title = title;
}

- (void)reloadState:(CheckSMSState)state andPhone:(NSString *)phone
{
    self.phoneNumber.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"На номер", nil), phone];
    
    switch (state) {
        case CheckSMSStateNormal:
            self.errorMesage.hidden = true;
            self.errorMessageTop.constant = 0;
            self.codeField.textColor = RGB(10, 88, 43);
            break;
            
        case CheckSMSStateError:
            self.errorMesage.hidden = false;
            self.errorMessageTop.constant = 79;
            self.codeField.textColor = RGB(217, 67, 67);
            break;
    }
}

- (void)updateTimerString:(NSString *)timerString
{
    self.timerLbl.text = timerString;
}

- (void)updateResendAvailability:(BOOL)available
{
    self.resendBtn.enabled = available;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.codeField.textColor = RGB(10, 88, 43);

    return !([textField.text length] > smsCodeLength && [string length] > range.length);
}

@end
