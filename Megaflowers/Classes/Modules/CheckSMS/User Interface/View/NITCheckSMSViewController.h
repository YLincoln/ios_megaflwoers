//
//  NITCheckSMSViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/23/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITCheckSMSProtocols.h"

@interface NITCheckSMSViewController : NITBaseViewController <NITCheckSMSViewProtocol>

@property (nonatomic, strong) id <NITCheckSMSPresenterProtocol> presenter;

@end
