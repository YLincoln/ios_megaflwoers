//
//  NITPaymentOrderProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderModel.h"
#import "NITPaymentOrderSectionModel.h"
#import "PayPalMobile.h"
#import "NITASDKCardIOScanner.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse2002.h"
#import "SWGBill.h"
#import "SWGOrder.h"
#import "NITPaymentOrderWebViewController.h"
#import "NITOrderModel.h"
#import "NITBillModel.h"
#import "NITBillAcquiringModel.h"
#import "NITMyOrderModel.h"
#import "NITBasketProductModel.h"

@protocol NITPaymentOrderInteractorOutputProtocol;
@protocol NITPaymentOrderInteractorInputProtocol;
@protocol NITPaymentOrderViewProtocol;
@protocol NITPaymentOrderPresenterProtocol;
@protocol NITPaymentOrderLocalDataManagerInputProtocol;
@protocol NITPaymentOrderAPIDataManagerInputProtocol;

@class NITPaymentOrderWireFrame;

@protocol NITPaymentOrderViewProtocol
@required
@property (nonatomic, strong) id <NITPaymentOrderPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITPaymentOrderSectionModel *> *)models rootSection:(NITPaymentOrderSectionModel *)rootSection;
- (void)reloadAvailabilityNextButton:(BOOL)available;
@end

@protocol NITPaymentOrderWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITPaymentOrderModuleFrom:(id)fromView forOrderID:(NSNumber *)orderID;
- (void)showPaymentSubTypeForModel:(NITPaymentOrderModel *)model from:(id)fromView;
- (void)showPaymentFinalFrom:(id)fromView withPaymentState:(BOOL)state;
- (void)openMyOrders:(id)fromView;
- (void)presentViewController:(id)presentVC from:(id)fromView;
- (void)dismissViewControllerFrom:(id)fromView;
- (void)backActionFrom:(id)fromView;
@end

@protocol NITPaymentOrderPresenterProtocol
@required
@property (nonatomic, weak) id <NITPaymentOrderViewProtocol> view;
@property (nonatomic, strong) id <NITPaymentOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPaymentOrderWireFrameProtocol> wireFrame;
@property (nonatomic) NSNumber * orderID;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)selectPaymentTypeByModel:(NITPaymentOrderModel *)model;
- (void)backAction;
- (void)nextAction;
@end

@protocol NITPaymentOrderInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDataByModels:(NSArray <NITPaymentOrderSectionModel *> *)models;
- (void)updateCurrentBill:(NITBillModel *)model;
- (void)updateOrderBills:(NSArray <NITBillModel *> *)models;
- (void)updateBillStatus:(BillStatus)status;
- (void)billMarkDeleted;
- (void)showError:(NSError *)error;
@end

@protocol NITPaymentOrderInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPaymentOrderInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPaymentOrderAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPaymentOrderLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getOrderByID:(NSNumber *)orderID withHandler:(void(^)(SWGOrder * model))handler;
- (void)updateDataByOrderID:(NSNumber *)orderID;
- (void)markOrderAsProcessedById:(NSNumber *)orderId withHandler:(void (^)(BOOL success, SWGOrder *order))handler;
- (void)getBillByOrderID:(NSNumber *)orderID paymentId:(NSNumber *)paymentId;
- (void)getPaymentStatusByOrderID:(NSNumber *)orderID billId:(NSNumber *)billId;
- (void)updateBillById:(NSNumber *)billId transactionId:(NSString *)transactionId orderId:(NSNumber *)orderId withHandler:(void(^)())handler;
- (void)markDeletedBillById:(NSNumber *)billId orderID:(NSNumber *)orderID;
@end


@protocol NITPaymentOrderDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITPaymentOrderAPIDataManagerInputProtocol <NITPaymentOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getPaymentTypesByOrderID:(NSNumber *)orderID sum:(NSNumber *)sum withHandler:(void(^)(NSArray<SWGInlineResponse2002> *output))handler;
- (void)postPaymentByOrderID:(NSNumber *)orderID orderHash:(NSString *)orderHash paymentId:(NSNumber *)paymentId sum:(NSNumber *)sum withHandler:(void(^)(SWGBill *output, NSError *error))handler;
- (void)getPaymentStatusByOrderID:(NSNumber *)orderID orderHash:(NSString *)orderHash billId:(NSNumber *)billId withHandler:(void(^)(SWGInlineResponse200 *output))handler;
- (void)getOrderBillsByOrderID:(NSNumber *)orderID orderHash:(NSString *)orderHash withHandler:(void(^)(NSArray<SWGBill> *output))handler;
- (void)updateBillById:(NSNumber *)billId transactionId:(NSString *)transactionId orderID:(NSNumber *)orderID orderHash:(NSString *)orderHash withHandler:(void(^)(BOOL success))handler;
- (void)markDeletedBillById:(NSNumber *)billId orderID:(NSNumber *)orderID orderHash:(NSString *)orderHash withHandler:(void(^)(BOOL success))handler;
@end

@protocol NITPaymentOrderLocalDataManagerInputProtocol <NITPaymentOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
