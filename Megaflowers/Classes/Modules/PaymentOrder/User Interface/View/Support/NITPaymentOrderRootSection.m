//
//  NITPaymentOrderRootSection.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/14/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderRootSection.h"
#import "NITPaymentOrderSectionModel.h"
#import "NITPaymentOrderModel.h"
#import "NITPaymentOrderCell.h"

@interface NITPaymentOrderRootSection ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;

@end

@implementation NITPaymentOrderRootSection

+ (instancetype)viewWithSection:(NITPaymentOrderSectionModel *)section delegate:(id)delegate
{
    NITPaymentOrderRootSection * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITPaymentOrderRootSection) owner:self options:nil] firstObject];
    [view setSection:section];
    [view setDelegate:delegate];
    
    return view;
}

#pragma mark - Custom accessors
- (void)setSection:(NITPaymentOrderSectionModel *)section
{
    _section = section;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    [self setFrame:[self calculateFrame]];
    [self.tableView registerNibArray:@[_s(NITPaymentOrderCell)]];
    [self.tableView reloadData];
}

- (CGRect)calculateFrame
{
    CGFloat height = 0;
    
    for (NITPaymentOrderModel * type in self.section.items)
    {
        height += [type rowHeight];
    }
    
    if (height > 0)
    {
        height += 34;
    }
    
    return CGRectMake(0, 0, ViewWidth(WINDOW), height);
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.section.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPaymentOrderModel * model = self.section.items[indexPath.row];
    return [model rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPaymentOrderCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITPaymentOrderCell) forIndexPath:indexPath];
    cell.model = self.section.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectPayment:)])
    {
        [self.delegate didSelectPayment:self.section.items[indexPath.row]];
    }
}


@end
