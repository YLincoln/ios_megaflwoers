//
//  NITPaymentOrderRootSection.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/14/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class NITPaymentOrderSectionModel, NITPaymentOrderModel;

@protocol NITPaymentOrderRootSectionDelegate <NSObject>

- (void)didSelectPayment:(NITPaymentOrderModel *)payment;

@end

@interface NITPaymentOrderRootSection : UIView

@property (nonatomic) NITPaymentOrderSectionModel * section;
@property (nonatomic, weak) id <NITPaymentOrderRootSectionDelegate> delegate;

+ (instancetype)viewWithSection:(NITPaymentOrderSectionModel *)section delegate:(id)delegate;

@end
