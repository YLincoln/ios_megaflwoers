//
//  NITPaymentOrderWebViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"

@interface NITPaymentOrderWebViewController : NITBaseViewController

@property (nonatomic) NSString * urlString;
@property (nonatomic) NSString * successUrlString;
@property (nonatomic) NSString * failUrlString;
@property (nonatomic) void (^onSuccess)(NSString *paymentId);
@property (nonatomic) void (^onCancelled)();
@property (nonatomic) void (^onError)(NSError *error);

@end
