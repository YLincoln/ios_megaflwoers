//
//  NITPaymentOrderViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITPaymentOrderProtocols.h"
#import "RRNCollapsableTableViewController.h"

@interface NITPaymentOrderViewController : RRNCollapsableTableViewController <NITPaymentOrderViewProtocol>

@property (nonatomic, strong) id <NITPaymentOrderPresenterProtocol> presenter;

@end
