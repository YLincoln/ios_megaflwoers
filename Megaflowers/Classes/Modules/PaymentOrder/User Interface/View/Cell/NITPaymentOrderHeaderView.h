//
//  NITPaymentOrderHeaderView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "RRNTableViewHeaderFooterView.h"

@interface NITPaymentOrderHeaderView : RRNTableViewHeaderFooterView

@end
