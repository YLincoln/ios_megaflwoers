//
//  NITPaymentOrderCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderCell.h"
#import "NITPaymentOrderModel.h"
#import "UIImageView+AFNetworking.h"

@interface NITPaymentOrderCell ()

@property (nonatomic) IBOutlet UIImageView * icon;
@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITPaymentOrderCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITPaymentOrderModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    UIImage * image = [UIImage imageNamed:self.model.iconName];
    if (image)
    {
        self.icon.contentMode = UIViewContentModeCenter;
        self.icon.image = image;
    }
    else
    {
        self.icon.contentMode = UIViewContentModeScaleAspectFit;
        [self.icon setImageWithURL:[NSURL URLWithString:self.model.iconName]];
    }
    
    self.title.text = self.model.title;
    self.accessoryView = [[UIImageView alloc] initWithImage:self.model.selected ? [UIImage imageNamed:@"ic_check"] : nil];
}

@end
