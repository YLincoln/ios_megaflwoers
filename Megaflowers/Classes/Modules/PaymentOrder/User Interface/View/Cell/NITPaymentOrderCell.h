//
//  NITPaymentOrderCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITPaymentOrderModel;

@interface NITPaymentOrderCell : UITableViewCell

@property (nonatomic) NITPaymentOrderModel * model;

@end
