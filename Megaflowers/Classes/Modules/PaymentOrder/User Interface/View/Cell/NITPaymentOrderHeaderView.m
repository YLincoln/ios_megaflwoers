//
//  NITPaymentOrderHeaderView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderHeaderView.h"

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

@interface NITPaymentOrderHeaderView ()

@property (nonatomic) IBOutlet UIImageView * imageView;
@property (nonatomic) IBOutlet UIImageView * bg;
@property (nonatomic) IBOutlet UILabel * titleLabel;
@property (nonatomic) IBOutlet UIView * line;
@property (nonatomic) IBOutlet NSLayoutConstraint * lineLeading;

@end

@implementation NITPaymentOrderHeaderView
{
    BOOL isRotating;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        self.bg.image = [UIImage imageWithColor:[UIColor whiteColor]];
    }
    
    return self;
}

- (void)updateTitle:(NSString *)title
{
    self.titleLabel.text = title;
    self.bg.image = [UIImage imageWithColor:[UIColor whiteColor]];
}

- (void)openAnimated:(BOOL)animated
{
    if (animated && !isRotating)
    {
        isRotating = YES;
        
        [UIView animateWithDuration:0.2 delay:0.0 options: UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveLinear animations:^{
            self.imageView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            isRotating = NO;
            [self layoutIfNeeded];
        }];
        
    }
    else
    {
        [self.layer removeAllAnimations];
        self.imageView.transform = CGAffineTransformIdentity;
        isRotating = NO;
    }
}

- (void)closeAnimated:(BOOL)animated
{
    if (animated && !isRotating) {
        
        isRotating = YES;
        
        [UIView animateWithDuration:0.2 delay:0.0 options: UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveLinear animations:^{
            self.imageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180.0f));
        } completion:^(BOOL finished) {
            isRotating = NO;
            [self layoutIfNeeded];
        }];
        
    }
    else
    {
        [self.layer removeAllAnimations];
        self.imageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180.0f));
        isRotating = NO;
    }
}

@end
