//
//  NITPaymentOrderWebViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderWebViewController.h"

@interface NITPaymentOrderWebViewController () <UIWebViewDelegate>

@property (nonatomic) UIWebView * webView;

@end

@implementation NITPaymentOrderWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - IBAction
- (IBAction)dismissAction:(id)sender
{
    [self dismissViewControllerAnimated:true completion:^{
        if (self.onCancelled) self.onCancelled();
    }];
}

#pragma mark - Private
- (void)initUI
{
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(ViewX(self.view), ViewY(self.view) + 44, ViewWidth(self.view), ViewHeight(self.view) - 44)];
    self.webView.delegate = self;
    
    [self.view addSubview:self.webView];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    
    UIButton * cancel =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancel setTitle:NSLocalizedString(@"Отмена", nil) forState:UIControlStateNormal];
    [cancel setTitleColor:RGB(10, 88, 43) forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(dismissAction:)forControlEvents:UIControlEventTouchUpInside];
    [cancel setFrame:CGRectMake(0, 0, 80, 44)];
    [cancel setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancel];
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] absoluteString] isEqualToString:self.successUrlString])
    {
        [self dismissViewControllerAnimated:true completion:^{
            if (self.onSuccess) self.onSuccess(@"");
        }];
    }
    else if ([[[request URL] absoluteString] isEqualToString:self.failUrlString])
    {
        [self dismissViewControllerAnimated:true completion:^{
            if (self.onError) self.onError(nil);
        }];
    }
    
    return true;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [HELPER startLoading];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [HELPER stopLoading];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [HELPER stopLoading];
    [self dismissViewControllerAnimated:true completion:^{
        if (self.onError) self.onError(error);
    }];
}

@end
