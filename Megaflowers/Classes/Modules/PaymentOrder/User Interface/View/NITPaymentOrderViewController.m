//
//  NITPaymentOrderViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderViewController.h"
#import "NITPaymentOrderCell.h"
#import "NITPaymentOrderHeaderView.h"
#import "NITPaymentOrderRootSection.h"

@interface NITPaymentOrderViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITPaymentOrderRootSectionDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIButton * nextBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * nextBtnBottom;
@property (nonatomic) NSArray <NITPaymentOrderSectionModel *> * items;

@end

@implementation NITPaymentOrderViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:true];
}

#pragma mark - IBAction
- (IBAction)backAction:(id)sender
{
    [self.presenter backAction];
}

- (IBAction)nextAction:(id)sender
{
    [self.presenter nextAction];
}

#pragma mark - NITPaymentOrderPresenterProtocol
- (void)reloadDataByModels:(NSArray <NITPaymentOrderSectionModel *> *)models rootSection:(NITPaymentOrderSectionModel *)rootSection
{
    self.items = models;

    [self.tableView setTableHeaderView:[NITPaymentOrderRootSection viewWithSection:rootSection delegate:self]];
    UIView * footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewHeight(self.tableView), 1)];
    footer.backgroundColor = RGB(212, 215, 213);
    [self.tableView setTableFooterView:footer];
    [self.tableView reloadData];
}

- (void)reloadAvailabilityNextButton:(BOOL)available
{
    self.nextBtnBottom.constant = available ? 0 : - ViewHeight(self.nextBtn);
    [self.view layoutIfNeeded];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Оплата заказа", nil);
    
    // table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITPaymentOrderCell) bundle:nil] forCellReuseIdentifier:_s(NITPaymentOrderCell)];
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NITPaymentOrderSectionModel * model = self.items[section];
    return model.sectionHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPaymentOrderSectionModel * section = self.items[indexPath.section];
    NITPaymentOrderModel * model = section.items[indexPath.row];
    
    return [model rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPaymentOrderCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITPaymentOrderCell) forIndexPath:indexPath];
    
    NITPaymentOrderSectionModel * model = self.items[indexPath.section];
    cell.model = model.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPaymentOrderSectionModel * model = self.items[indexPath.section];
    [self.presenter selectPaymentTypeByModel:model.items[indexPath.row]];
}

#pragma mark - NITPaymentOrderRootSectionDelegate
- (void)didSelectPayment:(NITPaymentOrderModel *)payment
{
    [self.presenter selectPaymentTypeByModel:payment];
}

#pragma mark - RRNCollapsableTableView
- (UITableView *)collapsableTableView
{
    return self.tableView;
}

- (NSArray <RRNCollapsableTableViewSectionModelProtocol> *)model
{
    return (id)self.items;
}

- (NSString *)sectionHeaderNibName
{
    return _s(NITPaymentOrderHeaderView);
}

- (NSBundle *)sectionHeaderNibBundle
{
    return [NSBundle bundleForClass:NSClassFromString([self sectionHeaderNibName])];
}

- (BOOL)singleOpenSelectionOnly
{
    return true;
}

@end
