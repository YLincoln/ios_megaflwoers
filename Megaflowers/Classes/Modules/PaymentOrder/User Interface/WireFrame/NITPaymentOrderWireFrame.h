//
//  NITPaymentOrderWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderProtocols.h"
#import "NITPaymentOrderViewController.h"
#import "NITPaymentOrderLocalDataManager.h"
#import "NITPaymentOrderAPIDataManager.h"
#import "NITPaymentOrderInteractor.h"
#import "NITPaymentOrderPresenter.h"
#import "NITPaymentOrderWireframe.h"
#import "NITRootWireframe.h"

@interface NITPaymentOrderWireFrame : NITRootWireframe <NITPaymentOrderWireFrameProtocol>

@end
