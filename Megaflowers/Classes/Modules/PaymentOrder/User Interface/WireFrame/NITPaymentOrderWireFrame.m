//
//  NITPaymentOrderWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderWireFrame.h"
#import "NITPaymentOrderTypeWireFrame.h"
#import "NITPaymentFinalWireFrame.h"
#import "NITOrdersListWireFrame.h"
#import "NITAuthorizationWireFrame.h"
#import "NITOrderViewController.h"

@implementation NITPaymentOrderWireFrame

+ (id)createNITPaymentOrder
{
    // Generating module components
    id <NITPaymentOrderPresenterProtocol, NITPaymentOrderInteractorOutputProtocol> presenter = [NITPaymentOrderPresenter new];
    id <NITPaymentOrderInteractorInputProtocol> interactor = [NITPaymentOrderInteractor new];
    id <NITPaymentOrderAPIDataManagerInputProtocol> APIDataManager = [NITPaymentOrderAPIDataManager new];
    id <NITPaymentOrderLocalDataManagerInputProtocol> localDataManager = [NITPaymentOrderLocalDataManager new];
    id <NITPaymentOrderWireFrameProtocol> wireFrame= [NITPaymentOrderWireFrame new];
    id <NITPaymentOrderViewProtocol> view = [(NITPaymentOrderWireFrame *)wireFrame createViewControllerWithKey:_s(NITPaymentOrderViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITPaymentOrderModuleFrom:(UIViewController *)fromViewController forOrderID:(NSNumber *)orderID
{
    NITPaymentOrderViewController * vc = [NITPaymentOrderWireFrame createNITPaymentOrder];
    vc.presenter.orderID = orderID;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showPaymentSubTypeForModel:(NITPaymentOrderModel *)model from:(UIViewController *)fromViewController
{
    [NITPaymentOrderTypeWireFrame presentNITPaymentOrderTypeModuleFrom:fromViewController withPaymentModel:model];
}

- (void)showPaymentFinalFrom:(UIViewController *)fromViewController withPaymentState:(BOOL)state
{
    [NITPaymentFinalWireFrame presentNITPaymentFinalModuleFrom:fromViewController withPaymentState:state];
}

- (void)openMyOrders:(UIViewController *)fromViewController
{
    UIViewController * rootVC = [fromViewController.navigationController.viewControllers firstObject];
    rootVC.navigationController.viewControllers = @[rootVC];
    [NITOrdersListWireFrame presentNITOrdersListModuleFrom:rootVC];
}

- (void)presentViewController:(UIViewController *)presentVC from:(UIViewController *)fromViewController
{
    [fromViewController.navigationController presentViewController:presentVC animated:true completion:^{}];
}

- (void)dismissViewControllerFrom:(UIViewController *)fromViewController
{
    [fromViewController dismissViewControllerAnimated:true completion:^{}];
}

- (void)backActionFrom:(UIViewController *)fromViewController
{
    BOOL hasCreateOrderVC = [fromViewController.navigationController.viewControllers bk_select:^BOOL(UIViewController *obj) {
        return [obj isKindOfClass:[NITOrderViewController class]];
    }].count > 0;
    
    if (hasCreateOrderVC)
    {
        [fromViewController.navigationController popToRootViewControllerAnimated:true];
    }
    else
    {
        [fromViewController.navigationController popViewControllerAnimated:true];
    }
}

@end
