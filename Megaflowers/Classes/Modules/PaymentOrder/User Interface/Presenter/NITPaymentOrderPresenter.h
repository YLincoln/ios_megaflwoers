//
//  NITPaymentOrderPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPaymentOrderProtocols.h"

@class NITPaymentOrderWireFrame;

@interface NITPaymentOrderPresenter : NITRootPresenter <NITPaymentOrderPresenterProtocol, NITPaymentOrderInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPaymentOrderViewProtocol> view;
@property (nonatomic, strong) id <NITPaymentOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPaymentOrderWireFrameProtocol> wireFrame;
@property (nonatomic) NSNumber * orderID;

@end
