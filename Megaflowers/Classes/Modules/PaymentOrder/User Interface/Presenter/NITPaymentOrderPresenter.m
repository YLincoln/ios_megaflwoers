//
//  NITPaymentOrderPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderPresenter.h"
#import "NITPaymentOrderWireframe.h"
#import "NSArray+BlocksKit.h"
#import "NITPayPalModule.h"
#import "NITTinkoffPayModule.h"
#import "NITWebPayModule.h"

static NSString * const kTinkoffCode    = @"tinkoffip";
static NSString * const kPayPalCode     = @"16";
static NSString * const kPayLaterCode   = @"76";

static NSInteger const maxBillRequestCount  = 20;
static NSInteger const kRootSectionId       = 1;
static NSInteger const kPopularSectionId    = 7;

@interface NITPaymentOrderPresenter ()

@property (nonatomic) NSArray <NITPaymentOrderSectionModel *> * items;
@property (nonatomic) NITPaymentOrderModel * paymentType;
@property (nonatomic) NITWebPayModule * webPayModule;
@property (nonatomic) NITPayPalModule * payPalModule;
@property (nonatomic) NITTinkoffPayModule * tinkoffModule;
@property (nonatomic) NSNumber * billID;
@property (nonatomic) NSInteger billRequestCount;

@end

@implementation NITPaymentOrderPresenter

#pragma mark - Custom accessors
- (NITWebPayModule *)webPayModule
{
    if (!_webPayModule)
    {
        _webPayModule = [NITWebPayModule new];
    }
    
    return _webPayModule;
}

- (NITPayPalModule *)payPalModule
{
    if (!_payPalModule)
    {
        _payPalModule = [NITPayPalModule new];
    }
    
    return _payPalModule;
}

- (NITTinkoffPayModule *)tinkoffModule
{
    if (!_tinkoffModule)
    {
        _tinkoffModule = [NITTinkoffPayModule new];
    }
    
    return _tinkoffModule;
}

#pragma mark - NITPaymentOrderPresenterProtocol
- (void)initData
{
    [self.view reloadAvailabilityNextButton:false];
    [self.interactor updateDataByOrderID:self.orderID];
}

- (void)selectPaymentTypeByModel:(NITPaymentOrderModel *)model
{
    self.items = [self.items bk_map:^id(NITPaymentOrderSectionModel * obj) {
        obj.items = [obj.items bk_map:^id(NITPaymentOrderModel * obj) {
            obj.selected = [model.identifier isEqualToNumber:obj.identifier];
            return obj;
        }];
        return obj;
    }];
    
    self.paymentType = model;
    [self reloadView];
}

- (void)backAction
{
    [self.wireFrame backActionFrom:self.view];
}

- (void)nextAction
{
    if (self.paymentType)
    {
        if ([[self.paymentType.identifier stringValue] isEqualToString:kPayLaterCode])
        {
            [TRACKER trackEvent:TrackerEventSelectPaymentType parameters:@{@"name":self.paymentType.title}];
            
            weaken(self);
            [self.interactor markOrderAsProcessedById:self.orderID withHandler:^(BOOL success, SWGOrder *order) {

                if (success)
                {
                    [USER updateLastOrder:order];
                    [weakSelf.wireFrame openMyOrders:weakSelf.view];
                }
                else
                {
                    [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:false];
                }
            }];
        }
        else
        {
            [self.interactor getBillByOrderID:self.orderID paymentId:self.paymentType.identifier];
        }
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"OK", nil) actionBlock:^{}];
        [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Выберите тип оплаты", nil) actionButtons:nil cancelButton:cancel];
    }
}

#pragma mark - NITPaymentOrderInteractorOutputProtocol
- (void)updateDataByModels:(NSArray<NITPaymentOrderSectionModel *> *)models
{
    self.items = models;
    
    [self.items bk_each:^(NITPaymentOrderSectionModel *obj) {
        
        if (obj.sectionId.integerValue == kPopularSectionId)
        {
            NITPaymentOrderModel * model = obj.items.firstObject;
            model.selected = true;
            self.paymentType = model;
        }
    }];
    
    [self reloadView];
}

- (void)updateCurrentBill:(NITBillModel *)billModel
{
    self.billRequestCount = 0;
    self.billID = billModel.identifier;
    self.paymentType = billModel.payment;
    
    [TRACKER trackEvent:TrackerEventSelectPaymentType parameters:@{@"name":self.paymentType.title}];
    
    switch (self.paymentType.paymentType) {
            
        case PaymentTypeNone:
        case PaymentTypeCash: {
            
            weaken(self);
            [self.interactor markOrderAsProcessedById:self.orderID withHandler:^(BOOL success, SWGOrder *order) {

                if (success)
                {
                    [USER updateLastOrder:order];
                    [TRACKER trackEvent:TrackerEventSelectPaymentType parameters:@{@"name":self.paymentType.title}];
                    [weakSelf.wireFrame showPaymentFinalFrom:weakSelf.view withPaymentState:true];
                    /*[weakSelf.wireFrame openMyOrders:weakSelf.view];*/
                }
                else
                {
                    [weakSelf.wireFrame showPaymentFinalFrom:weakSelf.view withPaymentState:false];
                }
            }];
            
        } break;

        default: {
            
            [self buyByBill:billModel success:^(NSString *paymentId) {
                
                [TRACKER trackEvent:TrackerEventOrderPayment parameters:@{@"name":self.paymentType.title, @"sum":billModel.sum}];

                [self.interactor markOrderAsProcessedById:self.orderID withHandler:^(BOOL success, SWGOrder *order) {

                    if (success)
                    {
                        [USER updateLastOrder:order];
                        
                        if (paymentId.length > 0)
                        {
                            [self.interactor updateBillById:self.billID transactionId:paymentId orderId:self.orderID withHandler:^{
                                [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:true];
                            }];
                        }
                        else
                        {
                            [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:true];
                        }
                    }
                    else
                    {
                        [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:false];
                    }
                }];
                
            } cancelled:^{
                
                [self.interactor markDeletedBillById:billModel.identifier orderID:self.orderID];
                
            } error:^(NSError *error) {
                
                [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:false];
            }];
            
        } break;
    }
}

- (void)updateOrderBills:(NSArray<NITBillModel *> *)models
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NSArray * buttons = [models bk_map:^id(NITBillModel *obj) {
        NSString * title = [NSString stringWithFormat:@"%@ №%@", NSLocalizedString(@"Счет", nil), obj.num];
        return [[NITActionButton alloc] initWithTitle:title actionBlock:^{
            [self updateCurrentBill:obj];
        }];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Выберите счет для оплаты", nil) actionButtons:buttons cancelButton:cancel];
}

- (void)updateBillStatus:(BillStatus)status
{
    switch (status) {
        case BillStatusNew: {
            if (self.billRequestCount > maxBillRequestCount)
            {
                self.billRequestCount = 0;
                [HELPER stopLoading];
                [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:false];
            }
            else
            {
                self.billRequestCount++;
                [HELPER startLoading];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.interactor getPaymentStatusByOrderID:self.orderID billId:self.billID];
                });
            }
        } break;
            
        default: {
            [HELPER stopLoading];
            [TRACKER trackEvent:TrackerEventSelectPaymentType parameters:@{@"name":self.paymentType.title}];
            [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:true];
        } break;
    }
}

- (void)showError:(NSError *)error
{
    if (error.swaggerCode == 500 || error.swaggerCode == 0)
    {
        [self.wireFrame showPaymentFinalFrom:self.view withPaymentState:false];
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"OK", nil) actionBlock:^{}];
        [HELPER showActionSheetFromView:self.view withTitle:error.interfaceDescription actionButtons:nil cancelButton:cancel];
    }
}

- (void)billMarkDeleted
{
    self.billID = nil;
    
    if (self.items.count == 0)
    {
        [self.interactor updateDataByOrderID:self.orderID];
    }
}

#pragma mark - Private
- (void)reloadView
{
    NITPaymentOrderSectionModel * rootSection = [self.items bk_select:^BOOL(NITPaymentOrderSectionModel *obj) {
        return obj.sectionId.integerValue == kRootSectionId;
    }].firstObject;
    
    NSArray * sections = [self.items bk_select:^BOOL(NITPaymentOrderSectionModel *obj) {
        return obj.sectionId.integerValue != kRootSectionId;
    }];
    
    [self.view reloadAvailabilityNextButton:self.paymentType != nil];
    [self.view reloadDataByModels:sections rootSection:rootSection];
}

- (void)buyByBill:(NITBillModel *)billModel
          success:(void (^)(NSString *paymentId))onSuccess
        cancelled:(void (^)())onCancelled
            error:(void(^)(NSError *error))onError
{
    [self.interactor getOrderByID:self.orderID withHandler:^(SWGOrder *order) {
        
        switch (self.paymentType.paymentType) {
        
            case PaymentTypeWeb:
            case PaymentTypeDocument: {
                
                [self.webPayModule buyByPayPalForOrder:order
                                                  bill:billModel
                                    fromViewController:self.view
                                               success:^(NSString *paymentId) {
                                                   onSuccess(paymentId);
                                               }
                                             cancelled:^{
                                                 onCancelled();
                                                 
                                             } error:^(NSError *error) {
                                                 onError(error);
                                             }];
            } break;
                
            case PaymentTypeSdk: {
                
                
                if ([self.paymentType.code isEqualToString:kPayPalCode]) // PayPal
                {
                    [self.payPalModule buyByPayPalForOrder:order
                                                      bill:billModel
                                        fromViewController:self.view
                                                   success:^(NSString *paymentId) {
                                                       onSuccess(paymentId);
                                                   }
                                                 cancelled:^{
                                                     onCancelled();
                                                     
                                                 } error:^(NSError *error) {
                                                     onError(error);
                                                 }];
                }
                else if ([self.paymentType.code isEqualToString:kTinkoffCode]) // tinkoff
                {
                    [self.tinkoffModule buyByPayPalForOrder:order
                                                       bill:billModel
                                         fromViewController:self.view
                                                    success:^(NSString *paymentId) {
                                                        onSuccess(paymentId);
                                                    }
                                                  cancelled:^{
                                                      onCancelled();
                                                      
                                                  } error:^(NSError *error) {
                                                      onError(error);
                                                  }];
                }
                else
                {
                    [self.webPayModule buyByPayPalForOrder:order
                                                      bill:billModel
                                        fromViewController:self.view
                                                   success:^(NSString *paymentId) {
                                                       onSuccess(paymentId);
                                                   }
                                                 cancelled:^{
                                                     onCancelled();
                                                     
                                                 } error:^(NSError *error) {
                                                     onError(error);
                                                 }];
                }
                
            } break;
                
            default:
                break;
        }
    }];
}

@end
