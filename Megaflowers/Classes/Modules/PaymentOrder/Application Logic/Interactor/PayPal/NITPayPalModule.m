//
//  NITPayPalModule.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/17/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPayPalModule.h"
#import "PayPalMobile.h"
#import "SWGOrder.h"
#import "NITBillModel.h"
#import "NITBillAcquiringModel.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITPayPalModule ()
<
PayPalPaymentDelegate,
PayPalFuturePaymentDelegate,
PayPalProfileSharingDelegate
>

@property (nonatomic) PayPalConfiguration * payPalConfig;
@property (nonatomic) NSString * environment;
@property (nonatomic) UIViewController * viewController;;
@property (nonatomic) void (^onSuccess)(NSString *paymentId);
@property (nonatomic) void (^onCancelled)();
@property (nonatomic) void (^onError)(NSError *error);

@end

@implementation NITPayPalModule

#pragma mark - Public
- (void)buyByPayPalForOrder:(SWGOrder *)oredrModel
                       bill:(NITBillModel *)billModel
         fromViewController:(UIViewController *)viewController
                    success:(void (^)(NSString *paymentId))onSuccess
                  cancelled:(void (^)())onCancelled
                      error:(void(^)(NSError *error))onError

{
    NSParameterAssert(viewController);
    NSParameterAssert(onSuccess);
    NSParameterAssert(onCancelled);
    NSParameterAssert(onError);
    
    self.viewController = viewController;
    self.onSuccess = onSuccess;
    self.onCancelled = onCancelled;
    self.onError = onError;
    
    NSDictionary * sdkParam = (NSDictionary *)billModel.acquiring.sdkParam;
    NSString * mode = sdkParam[@"mode"];
    NSString * clientId = sdkParam[@"clientId"];
    
    if (mode.length == 0 || clientId.length == 0)
    {
        self.onError([NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"Не верные параметры SDK", nil)}]);
        return;
    }
    
    if ([mode isEqualToString:@"sandbox"])
    {
        [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentSandbox : clientId}];
        self.environment = PayPalEnvironmentSandbox;

    }
    else
    {
        [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : clientId}];
        self.environment = PayPalEnvironmentProduction;
    }
    
    NSNumber * sum = billModel.sum;
    
    if (sum.floatValue <= 0)
    {
        self.onError([NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"Заказ уже оплачен", nil)}]);
        return;
    }
    
    NSString * title = [NSString stringWithFormat:@"%@ №%@", NSLocalizedString(@"Счет", nil), billModel.num];
    
    PayPalPayment * payment = [[PayPalPayment alloc] init];
    payment.amount = [[NSDecimalNumber alloc] initWithString:[sum stringValue]];;
    payment.currencyCode = @"RUB";
    payment.shortDescription = title;
    payment.invoiceNumber = billModel.num;
    payment.intent = PayPalPaymentIntentAuthorize;
    
    if (!payment.processable)
    {
        self.onError([NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"Неверные входные параметры", nil)}]);
        return;
    }
    
    //Account SandBox
    //'Email ID'=>'paypal-payment-facilitator@megaflowers.ru', Add a comment to this line
    //'Signature' => 'As7fhbmuSA8SmAqHOljnEjyAlT-EAwYx6xBg.HSBa2SwekXAtG0iFvBc',
    //'Password' => '5YCTC2CAQDD42Z5M',
    //'Username' => 'paypal-payment-facilitator_api1.megaflowers.ru',
    
    //'Email ID'=>'paypal-payment-buyer@megaflowers.ru',
    //'Password' => '5YCTC2CAQDD42Z5M',

    
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = false;
    _payPalConfig.merchantName = @"Megaflowers";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionProvided;
    _payPalConfig.rememberUser = true;
    
    if ([mode isEqualToString:@"sandbox"])
    {
        _payPalConfig.forceDefaultsInSandbox = true;
        _payPalConfig.sandboxUserPassword = @"5YCTC2CAQDD42Z5M";
        _payPalConfig.sandboxUserPin = @"5YCTC2CAQDD42Z5M";
        _payPalConfig.defaultUserEmail = @"paypal-payment-buyer@megaflowers.ru";
    }
    else
    {
        _payPalConfig.forceDefaultsInSandbox = false;
    }
    
    PayPalPaymentViewController * paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                 configuration:_payPalConfig
                                                                                                      delegate:self];
    
    [self.viewController presentViewController:paymentViewController animated:true completion:^{
        [PayPalMobile preconnectWithEnvironment:self.environment];
    }];
}

#pragma mark - IBAction
- (IBAction)getUserAuthorizationForFuturePayments:(id)sender
{
    PayPalFuturePaymentViewController * futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
    [self.viewController presentViewController:futurePaymentViewController animated:true completion:^{}];
}

#pragma mark PayPalPaymentDelegate methods
- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    DDLogInfo(@"PayPal Payment Success!");
    DDLogInfo(@"%@", [completedPayment description]);
    
    NSString * transactionId = completedPayment.confirmation[@"response"][@"id"];
    [self.viewController dismissViewControllerAnimated:true completion:^{}];
    self.onSuccess(transactionId);
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    DDLogInfo(@"PayPal Payment Canceled");
    [self.viewController dismissViewControllerAnimated:true completion:^{}];
    self.onCancelled();
}

#pragma mark PayPalFuturePaymentDelegate methods
- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization
{
    DDLogInfo(@"PayPal Future Payment Authorization Success!");
    DDLogInfo(@"%@", [futurePaymentAuthorization description]);
    
    [self.viewController dismissViewControllerAnimated:true completion:^{}];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController
{
    DDLogInfo(@"PayPal Future Payment Authorization Canceled");
    [self.viewController dismissViewControllerAnimated:true completion:^{}];
}

#pragma mark PayPalProfileSharingDelegate methods
- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization
{
    DDLogInfo(@"PayPal Profile Sharing Authorization Success!");
    DDLogInfo(@"%@", [profileSharingAuthorization description]);
    
    [self.viewController dismissViewControllerAnimated:true completion:^{}];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
{
    DDLogInfo(@"PayPal Profile Sharing Authorization Canceled");
    [self.viewController dismissViewControllerAnimated:true completion:^{}];
}

@end
