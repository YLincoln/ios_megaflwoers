//
//  NITPaymentOrderInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderInteractor.h"
#import "NITOrderDetailsAPIDataManager.h"
#import "SWGRequestOrder.h"
#import "NITOrderAPIDataManager.h"

@implementation NITPaymentOrderInteractor

- (void)getOrderByID:(NSNumber *)orderID withHandler:(void (^)(SWGOrder * model))handler
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        NITOrderDetailsAPIDataManager * orderAPI = [NITOrderDetailsAPIDataManager new];
        [orderAPI getUserOrderByID:orderID withHandler:^(SWGOrder *result) {
            if (handler) handler(result);
        }];
    }];
}

- (void)updateDataByOrderID:(NSNumber *)orderID
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        weaken(self);
        [self getOrderByID:orderID withHandler:^(SWGOrder *model) {
            
            [weakSelf.APIDataManager getOrderBillsByOrderID:orderID orderHash:model.orderHash withHandler:^(NSArray<SWGBill> *output) {
                
                NSArray <NITBillModel *> * bills = [[output bk_map:^id(SWGBill *obj) {
                    return [[NITBillModel alloc] initWithModel:obj];
                }] bk_select:^BOOL(NITBillModel *obj) {
                    return obj.status == BillStatusNew;
                }];
                
                if (bills.count == 1)
                {
                    [weakSelf.presenter updateCurrentBill:bills.firstObject];
                }
                else if (bills.count > 1)
                {
                    [weakSelf.presenter updateOrderBills:bills];
                }
                else
                {
                    [weakSelf.APIDataManager getPaymentTypesByOrderID:model._id sum:model.sumRemain withHandler:^(NSArray<SWGInlineResponse2002> *output) {
                        [weakSelf parseResult:output];
                    }];
                }
            }];
        }];
    }];
}

- (void)markOrderAsProcessedById:(NSNumber *)orderId withHandler:(void (^)(BOOL, SWGOrder *))handler
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        
        [self getOrderByID:orderId withHandler:^(SWGOrder *order) {
            
            SWGBasketRequest * request = [SWGBasketRequest new];
            
            request.bouquets = (id)[order.basket.bouquets bk_map:^id(SWGBasketBouquet *obj) {
                SWGBasketRequestBouquets * requestBouquet = [SWGBasketRequestBouquets new];
                requestBouquet.skuId = obj.bouquetSkuId;
                requestBouquet.count = obj.count;
                requestBouquet.components = (id)[obj.components bk_map:^id(SWGBasketBouquetComponent *comp) {
                    SWGBasketRequestComponents * requestComponent = [SWGBasketRequestComponents new];
                    requestComponent._id = comp._id;
                    requestComponent.colorId = comp.color._id;
                    requestComponent.count = comp.count;
                    return requestComponent;
                }];
                return requestBouquet;
            }];
            
            request.attaches = (id)[order.basket.attaches bk_map:^id(SWGBasketAttach *obj) {
                SWGBasketRequestAttaches * requestAttach = [SWGBasketRequestAttaches new];
                requestAttach.skuId = obj.attachSkuId;
                requestAttach.count = obj.count;
                return requestAttach;
            }];
            
            order.statusId = @(2);
            order.basket = (id)request;
            
            [[NITOrderAPIDataManager new] sendOrderwithID:order._id order:(SWGRequestOrder *)order withHandler:^(BOOL success, NSError *error) {
                [API validateBasket];
                if (handler) handler(error == nil, order);
            }];
        }];
    }];
}

- (void)getBillByOrderID:(NSNumber *)orderID paymentId:(NSNumber *)paymentId
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        weaken(self);
        [self getOrderByID:orderID withHandler:^(SWGOrder *model) {
            
            [weakSelf.APIDataManager getOrderBillsByOrderID:orderID orderHash:model.orderHash withHandler:^(NSArray<SWGBill> *output) {
                
                NSArray <NITBillModel *> * bills = [[output bk_map:^id(SWGBill *obj) {
                    return [[NITBillModel alloc] initWithModel:obj];
                }] bk_select:^BOOL(NITBillModel *obj) {
                    return obj.status == BillStatusNew;
                }];
                
                if (bills.count == 1)
                {
                    [weakSelf.presenter updateCurrentBill:bills.firstObject];
                }
                else if (bills.count > 1)
                {
                    [weakSelf.presenter updateOrderBills:bills];
                }
                else
                {
                    [weakSelf.APIDataManager postPaymentByOrderID:orderID orderHash:model.orderHash paymentId:paymentId sum:model.sumRemain withHandler:^(SWGBill *output, NSError *error) {
                        if (error)
                        {
                            [weakSelf.presenter showError:error];
                        }
                        else
                        {
                            [weakSelf.presenter updateCurrentBill:[[NITBillModel alloc] initWithModel:output]];
                        }
                    }];
                }
            }];
        }];
    }];
}

- (void)updateBillById:(NSNumber *)billId transactionId:(NSString *)transactionId orderId:(NSNumber *)orderId withHandler:(void(^)())handler
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        weaken(self);
        [self getOrderByID:orderId withHandler:^(SWGOrder *model) {
            [weakSelf.APIDataManager updateBillById:billId transactionId:transactionId orderID:model._id orderHash:model.orderHash withHandler:^(BOOL success) {
                if (handler) handler();
            }];
        }];
    }];
}

- (void)markDeletedBillById:(NSNumber *)billId orderID:(NSNumber *)orderID
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        weaken(self);
        [self getOrderByID:orderID withHandler:^(SWGOrder *model) {
            [weakSelf.APIDataManager markDeletedBillById:billId orderID:orderID orderHash:model.orderHash withHandler:^(BOOL success) {
                [weakSelf.presenter billMarkDeleted];
            }];
        }];
    }];
}

- (void)getPaymentStatusByOrderID:(NSNumber *)orderID billId:(NSNumber *)billId
{
    [USER checkAutorizationWithHandler:^(BOOL success) {
        weaken(self);
        [self getOrderByID:orderID withHandler:^(SWGOrder *model) {
            [weakSelf.APIDataManager  getPaymentStatusByOrderID:orderID orderHash:model.orderHash billId:billId withHandler:^(SWGInlineResponse200 *output) {
                [weakSelf.presenter updateBillStatus:output.status.integerValue];
            }];
        }]; 
    }];
}

#pragma mark - Private
- (void)parseResult:(NSArray<SWGInlineResponse2002> *)result
{
    NSMutableArray * sections = [NSMutableArray new];
    
    for (SWGInlineResponse2002 * model in result)
    {
        NITPaymentOrderSectionModel * section = [[NITPaymentOrderSectionModel alloc] initWithModel:model];
        section.items = [model.items bk_map:^id(SWGPayment * payment) {
            return [[NITPaymentOrderModel alloc] initWithPayment:payment];
        }];
        
        [sections addObject:section];
    }
    
    [self.presenter updateDataByModels:sections];
}

@end
