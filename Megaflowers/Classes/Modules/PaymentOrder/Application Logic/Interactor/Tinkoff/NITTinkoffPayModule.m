//
//  NITTinkoffPayModule.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/18/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITTinkoffPayModule.h"
#import <ASDKUI/ASDKUI.h>
#import "NITASDKCardIOScanner.h"
#import "SWGOrder.h"
#import "NITBillModel.h"
#import "NITBillAcquiringModel.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITTinkoffPayModule ()

@property (nonatomic) UIViewController * viewController;;
@property (nonatomic) void (^onSuccess)(NSString *paymentId);
@property (nonatomic) void (^onCancelled)();
@property (nonatomic) void (^onError)(NSError *error);

@end

@implementation NITTinkoffPayModule

#pragma mark - Public
- (void)buyByPayPalForOrder:(SWGOrder *)oredrModel
                       bill:(NITBillModel *)billModel
         fromViewController:(UIViewController *)viewController
                    success:(void (^)(NSString *paymentId))onSuccess
                  cancelled:(void (^)())onCancelled
                      error:(void(^)(NSError *error))onError

{
    NSParameterAssert(viewController);
    NSParameterAssert(onSuccess);
    NSParameterAssert(onCancelled);
    NSParameterAssert(onError);
    
    self.viewController = viewController;
    self.onSuccess = onSuccess;
    self.onCancelled = onCancelled;
    self.onError = onError;
    
    NSNumber * sum = billModel.sum;
    
    if (sum.floatValue <= 0)
    {
        self.onError([NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"Заказ уже оплачен", nil)}]);
        return;
    }
    
    NSString * title = [NSString stringWithFormat:@"%@ №%@", NSLocalizedString(@"Счет", nil), billModel.num];
    
    NSMutableDictionary * additionalPaymentData = [NSMutableDictionary new];
    [additionalPaymentData addEntriesFromDictionary:(NSDictionary *)billModel.acquiring.sdkParam];
    [additionalPaymentData setObject:billModel.num forKey:@"order_id"];
    
    if (oredrModel.senderEmail)
    {
        [additionalPaymentData setObject:oredrModel.senderEmail forKey:@"email"];
    }
    
    if (USER.identifier)
    {
        [additionalPaymentData setObject:USER.identifier forKey:@"customerKey"];
    }
    
    if ([NITTinkoffPayModule isPayWithAppleAvailable])
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{
            self.onCancelled();
        }];
        
        NITActionButton * bankCardBtn = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Оплатить картой", nil) actionBlock:^{
            
            [NITTinkoffPayModule buyItemWithName:title
                                     description:@""
                                          amount:sum
                           additionalPaymentData:additionalPaymentData
                              fromViewController:self.viewController
                                         success:^(NSString *paymentId) {
                                             self.onSuccess(paymentId);
                                         }
                                       cancelled:^{
                                           self.onCancelled();
                                       }
                                           error:^(NSError *error) {
                                               self.onError(error);
                                           }];
        }];
        
        NITActionButton * applePayBtn = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Оплатить Apple Pay", nil) actionBlock:^{
            
            [NITTinkoffPayModule buyWithApplePayAmount:sum
                                           description:title
                                                 email:oredrModel.senderEmail
                                       appleMerchantId:kApplePayMerchantID
                                       shippingMethods:@[]
                                       shippingContact:nil
                                 additionalPaymentData:additionalPaymentData
                                    fromViewController:self.viewController
                                               success:^(NSString *paymentId) {
                                                   self.onSuccess(paymentId);
                                               }
                                             cancelled:^{
                                                 self.onCancelled();
                                             }
                                                 error:^(NSError *error) {
                                                     self.onError(error);
                                                 }];
        }];
        
        [HELPER showActionSheetFromView:self.viewController
                              withTitle:NSLocalizedString(@"Выберите способ оплаты", nil)
                          actionButtons:@[bankCardBtn, applePayBtn]
                           cancelButton:cancel];
    }
    else
    {
        [NITTinkoffPayModule buyItemWithName:title
                                 description:@""
                                      amount:sum
                       additionalPaymentData:additionalPaymentData
                          fromViewController:self.viewController
                                     success:^(NSString *paymentId) {
                                         self.onSuccess(paymentId);
                                     }
                                   cancelled:^{
                                       self.onCancelled();
                                   }
                                       error:^(NSError *error) {
                                           self.onError(error);
                                       }];
    }
}

#pragma mark - Private
+ (ASDKPaymentFormStarter *)paymentFormStarterWithParams:(NSDictionary *)sdkParams
{
    return [ASDKPaymentFormStarter paymentFormStarterWithAcquiringSdk:[NITTinkoffPayModule acquiringSdkWithParams:sdkParams]];
}

+ (ASDKAcquiringSdk *)acquiringSdkWithParams:(NSDictionary *)sdkParams
{
    ASDKStringKeyCreator * stringKeyCreator = [[ASDKStringKeyCreator alloc] initWithPublicKeyString:sdkParams[@"publicKey"]];
    ASDKAcquiringSdk * acquiringSdk = [ASDKAcquiringSdk acquiringSdkWithTerminalKey:sdkParams[@"terminalKey"]
                                                                            payType:nil//@"O"//@"T"
                                                                           password:sdkParams[@"password"]
                                                                publicKeyDataSource:stringKeyCreator];
    
    [acquiringSdk setDebug:true];
    [acquiringSdk setTestDomain:false];
    [acquiringSdk setLogger:nil];
    
    return acquiringSdk;
}

+ (NSString *)customerKey
{
    return @"undefined_user";
}

+ (NSError *)errorWithASDKAcquringSdkError:(ASDKAcquringSdkError *)error
{
    NSString * alertTitle = error.errorMessage ? error.errorMessage : @"Ошибка";
    NSString * alertDetails = error.errorDetails ? error.errorDetails : error.userInfo[kASDKStatus];
    NSString * description = [NSString stringWithFormat:@"%@\n%@", alertTitle, alertDetails];
    
    return [NSError errorWithDomain:error.domain
                               code:error.code
                           userInfo:@{NSLocalizedDescriptionKey : description}];
}

+ (void)buyItemWithName:(NSString *)name
            description:(NSString *)description
                 amount:(NSNumber *)amount
  additionalPaymentData:(NSDictionary *)data
     fromViewController:(UIViewController *)viewController
                success:(void (^)(NSString *paymentId))onSuccess
              cancelled:(void (^)())onCancelled
                  error:(void(^)(NSError *error))onError
{
    /*ASDKAcquiringSdk * acquiringSdk = [NITTinkoffPayController acquiringSdkWithParams:data];
     [acquiringSdk initWithAmount:amount
     orderId:data[@"order_id"]
     description:description
     payForm:@""
     customerKey:data[@"customerKey"] ? : [NITTinkoffPayController customerKey]
     recurrent:true
     additionalPaymentData:data[@"email"] ? @{@"email" : data[@"email"]} : nil
     success:^(ASDKInitResponse *response) {
     
     }
     failure:^(ASDKAcquringSdkError *error) {
     
     }];*/
    
    ASDKPaymentFormStarter * paymentFormStarter = [NITTinkoffPayModule paymentFormStarterWithParams:data];
    paymentFormStarter.cardScanner = [[NITASDKCardIOScanner alloc] init];
    [paymentFormStarter presentPaymentFormFromViewController:viewController
                                                     orderId:data[@"order_id"]
                                                      amount:amount
                                                       title:name
                                                 description:description
                                                      cardId:nil
                                                       email:data[@"email"]
                                                 customerKey:data[@"customerKey"] ? : [NITTinkoffPayModule customerKey]
                                       additionalPaymentData:data[@"email"] ? @{@"email" : data[@"email"]} : nil
                                                     success:^(NSString *paymentId) {
                                                         onSuccess(paymentId);
                                                     }
                                                   cancelled:^{
                                                       onCancelled();
                                                   }
                                                       error:^(ASDKAcquringSdkError *error) {
                                                           
                                                           [viewController dismissViewControllerAnimated:true completion:nil];
                                                           onError([NITTinkoffPayModule errorWithASDKAcquringSdkError:error]);
                                                       }];
}

+ (BOOL)isPayWithAppleAvailable
{
    return [ASDKPaymentFormStarter isPayWithAppleAvailable];
}

+ (void)buyWithApplePayAmount:(NSNumber *)amount
                  description:(NSString *)description
                        email:(NSString *)email
              appleMerchantId:(NSString *)appleMerchantId
              shippingMethods:(NSArray<PKShippingMethod *> *)shippingMethods
              shippingContact:(PKContact *)shippingContact
        additionalPaymentData:(NSDictionary *)data
           fromViewController:(UIViewController *)viewController
                      success:(void (^)(NSString *paymentId))onSuccess
                    cancelled:(void (^)())onCancelled
                        error:(void(^)(NSError *error))onError
{
    ASDKPaymentFormStarter * paymentFormStarter = [NITTinkoffPayModule paymentFormStarterWithParams:data];
    [paymentFormStarter payWithApplePayFromViewController:viewController
                                                   amount:amount
                                                  orderId:data[@"order_id"]
                                              description:description
                                              customerKey:data[@"customerKey"] ? : [NITTinkoffPayModule customerKey]
                                                sendEmail:true
                                                    email:email
                                          appleMerchantId:appleMerchantId
                                          shippingMethods:@[]
                                          shippingContact:shippingContact
                                   shippingEditableFields:PKAddressFieldNone
                                    additionalPaymentData:nil
                                                  success:^(NSString *paymentId) {
                                                      onSuccess(paymentId);
                                                  }
                                                cancelled:^{
                                                    onCancelled();
                                                }
                                                    error:^(ASDKAcquringSdkError *error) {
                                                        [viewController dismissViewControllerAnimated:true completion:nil];
                                                        onError([NITTinkoffPayModule errorWithASDKAcquringSdkError:error]);
                                                    }];
}

@end
