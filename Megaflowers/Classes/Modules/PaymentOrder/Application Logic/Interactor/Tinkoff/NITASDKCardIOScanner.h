//
//  NITASDKCardIOScanner.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ASDKUI/ASDKUI.h>

@interface NITASDKCardIOScanner : NSObject <ASDKAcquiringSdkCardScanner>

+ (instancetype)scanner;

@end
