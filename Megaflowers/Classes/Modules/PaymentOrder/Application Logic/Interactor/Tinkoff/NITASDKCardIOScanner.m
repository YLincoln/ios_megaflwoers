//
//  NITASDKCardIOScanner.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITASDKCardIOScanner.h"
#import <AVFoundation/AVFoundation.h>
#import "CardIO.h"

@interface NITASDKCardIOScanner () <CardIOPaymentViewControllerDelegate>

@property (nonatomic, strong) void (^successBlock)(NSString * cardNumber);
@property (nonatomic, strong) void (^cancelBlock)();

@end

@implementation NITASDKCardIOScanner

+ (instancetype)scanner
{
    NITASDKCardIOScanner *scanner = [[NITASDKCardIOScanner alloc] init];
    
    return scanner;
}

- (void)openCardScaner
{
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           if (granted)
                           {
                               CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
                               scanViewController.disableBlurWhenBackgrounding = true;
                               scanViewController.navigationBarStyle = UIBarStyleBlack;
                               [self setupCardScaner:scanViewController];
                               
                               UIViewController * topVc = [NITASDKCardIOScanner topMostController];
                               [topVc presentViewController:scanViewController animated:true completion:nil];
                           }
                           else
                           {
                               //ALERT
                           }
                       });
    }];
}

- (void)setupCardScaner:(CardIOPaymentViewController *)cardScaner
{
    [cardScaner setKeepStatusBarStyle:true];
    [cardScaner setSuppressScanConfirmation:true];
    [cardScaner setUseCardIOLogo:true];
    [cardScaner setDisableManualEntryButtons:true];
    [cardScaner setCollectExpiry:false];
    [cardScaner setCollectCVV:false];
}

+ (UIViewController *)topMostController
{
    UIViewController * topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    NSString *cardNumber = info.cardNumber;
    
    if (self.successBlock)
    {
        self.successBlock(cardNumber);
    }
    
    [self closePaymentViewController:paymentViewController];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    if (self.cancelBlock)
    {
        self.cancelBlock();
    }
    
    [self closePaymentViewController:paymentViewController];
}

- (void)scanCardSuccess:(void (^)(NSString *cardNumnber))success
                failure:(void (^)(ASDKAcquringSdkError *error))failure
                 cancel:(void (^)())cancel
{
    self.successBlock = success;
    self.cancelBlock = cancel;
    
    [self openCardScaner];
}

- (void)closePaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    [paymentViewController dismissViewControllerAnimated:true completion:nil];
}

@end
