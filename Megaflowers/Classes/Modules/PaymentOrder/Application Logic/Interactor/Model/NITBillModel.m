//
//  NITBillModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/25/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBillModel.h"
#import "SWGBill.h"
#import "NITBillAcquiringModel.h"
#import "NITPaymentOrderModel.h"

@implementation NITBillModel

- (instancetype)initWithModel:(SWGBill *)model
{
    if (self = [super init])
    {
        self.identifier     = model._id;
        self.xmlId          = model.xmlId;
        self.toId           = model.toId;
        self.status         = model.status.integerValue;
        self.statusText     = model.statusText;
        self.num            = model.num;
        self.sum            = model.sum;
        self.paymentId      = model.paymentId;
        self.transactionId  = model.transactionId;
        self.transactionSdk = model.transactionSdk;
        self.acquiring      = [[NITBillAcquiringModel alloc] initWithModel:model.acquiring];
        self.payment        = [[NITPaymentOrderModel alloc] initWithPayment:model.payment];
    }
    
    return self;
}

@end
