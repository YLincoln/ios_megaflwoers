//
//  NITBillModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/25/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SWGBill, NITBillAcquiringModel, NITPaymentOrderModel;

typedef CF_ENUM (NSUInteger, BillStatus) {
    BillStatusNew       = 0, // Выставлен
    BillStatusPending   = 1, // В обработке
    BillStatusCanceled  = 2, // Отменен
    BillStatusHold      = 3, // Захолдирован
    BillStatusSuccess   = 4, // Оплачен
    BillStatusConfirm   = 5, // Оплачен и подтвержден
    BillStatusReturned  = 6, // Возвращен
    BillStatusError     = 7, // Ошибка
    BillStatusRejected  = 8  // Отклонен платежной системой
};

@interface NITBillModel : NSObject

@property (nonatomic) BillStatus status;
@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSNumber * xmlId;
@property (nonatomic) NSNumber * toId;
@property (nonatomic) NSString * statusText;
@property (nonatomic) NSString * num;
@property (nonatomic) NSNumber * sum;
@property (nonatomic) NSNumber * paymentId;
@property (nonatomic) NSString * transactionId;
@property (nonatomic) NSString * transactionSdk;
@property (nonatomic) NITPaymentOrderModel * payment;
@property (nonatomic) NITBillAcquiringModel * acquiring;

- (instancetype)initWithModel:(SWGBill *)model;

@end
