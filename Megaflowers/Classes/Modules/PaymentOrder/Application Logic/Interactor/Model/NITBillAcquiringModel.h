//
//  NITBillAcquiringModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/25/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SWGBillAcquiring;

@interface NITBillAcquiringModel : NSObject

@property (nonatomic) NSString * url;
@property (nonatomic) NSDictionary * formData;
@property (nonatomic) NSString * paymentUrl;
@property (nonatomic) NSString * successUrl;
@property (nonatomic) NSString * failUrl;
@property (nonatomic) NSObject * sdkParam;

- (instancetype)initWithModel:(SWGBillAcquiring *)model;

@end
