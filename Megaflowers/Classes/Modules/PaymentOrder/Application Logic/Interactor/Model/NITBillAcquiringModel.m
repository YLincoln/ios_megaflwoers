//
//  NITBillAcquiringModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 5/25/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBillAcquiringModel.h"
#import "SWGBillAcquiring.h"

@implementation NITBillAcquiringModel

- (instancetype)initWithModel:(SWGBillAcquiring *)model
{
    if (self = [super init])
    {
        self.url        = model.url;
        self.paymentUrl = model.paymentUrl;
        self.successUrl = model.successUrl;
        self.failUrl    = model.failUrl;
        self.formData   = (id)model.formData;
        self.sdkParam   = (id)model.sdkParam;
    }
    
    return self;
}


@end
