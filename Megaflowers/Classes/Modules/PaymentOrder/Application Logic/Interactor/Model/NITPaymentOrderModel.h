//
//  NITPaymentOrderModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class SWGPayment;

typedef CF_ENUM (NSUInteger, PaymentType) {
    PaymentTypeNone     = 0,
    PaymentTypeWeb      = 1,
    PaymentTypeCash     = 2,
    PaymentTypeSdk      = 3,
    PaymentTypeDocument = 4
};

@interface NITPaymentOrderModel : NSObject

@property (nonatomic) PaymentType paymentType;
@property (nonatomic) NSString * title;
@property (nonatomic) NSNumber * identifier;
@property (nonatomic) NSString * code;
@property (nonatomic) NSNumber * position;
@property (nonatomic) NSString * manualLink;
@property (nonatomic) NSNumber * _auto;
@property (nonatomic) NSNumber * cash;
@property (nonatomic) NSNumber * returnType;
@property (nonatomic) NSNumber * official;
@property (nonatomic) NSNumber * paymentSectionId;
@property (nonatomic) NSNumber * currencyId;
@property (nonatomic) NSNumber * amountLimit;
@property (nonatomic) NSNumber * canSdk;
@property (nonatomic) NSString * descriptionText;
@property (nonatomic) NSString * iconName;

@property (nonatomic) BOOL selected;
@property (nonatomic) CGFloat rowHeight;

- (instancetype)initWithPayment:(SWGPayment *)payment;

@end
