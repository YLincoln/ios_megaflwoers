//
//  NITPaymentOrderModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderModel.h"
#import "SWGPayment.h"

@implementation NITPaymentOrderModel

- (instancetype)initWithPayment:(SWGPayment *)payment
{
    if (self = [super init])
    {
        if (payment)
        {
            self.iconName           = [payment.image.preview valueForKey:[API imageKey]];
            self.title              = [payment.name stringByStrippingHTML];
            self.paymentType        = payment.paymentType.integerValue;
            self.identifier         = payment._id;
            self.code               = payment.code;
            self.position           = payment.position;
            self.manualLink         = payment.manualLink;
            self._auto              = payment._auto;
            self.cash               = payment.cash;
            self.returnType         = payment.returnType;
            self.official           = payment.official;
            self.paymentSectionId   = payment.paymentSectionId;
            self.currencyId         = payment.currencyId;
            self.amountLimit        = payment.amountLimit;
            self.canSdk             = payment.canSdk;
            self.descriptionText    = payment._description;
        }
    }
    
    return self;
}

#pragma mark - Custom accessors
- (CGFloat)rowHeight
{
    CGFloat k = 191.0 / 320.0;
    CGFloat height = [self calcHeightForText:self.title width:ViewWidth(WINDOW) * k] + 16;
    
    return MAX(height, 56);
}

#pragma mark - Private
- (CGFloat)calcHeightForText:(NSString *)text width:(CGFloat)width
{
    return [text boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                              options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17 weight:UIFontWeightLight]}
                              context:nil].size.height;
}

@end
