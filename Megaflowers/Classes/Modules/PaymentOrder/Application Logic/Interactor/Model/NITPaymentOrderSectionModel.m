//
//  NITPaymentOrderSectionModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderSectionModel.h"
#import "SWGInlineResponse2002.h"

@implementation NITPaymentOrderSectionModel

@synthesize title = _title;
@synthesize isVisible = _isVisible;
@synthesize items = _items;

- (instancetype)initWithModel:(SWGInlineResponse2002 *)model;
{
    if (self = [super init])
    {
        if (model)
        {
            self.title = model.sectionName;
            self.sectionId =  model.sectionId;
            self.sectionCode = model.sectionCode;
            
            if ([self.sectionId isEqualToNumber:@(7)])
            {
                _isVisible = @(true);
            }
        }
    }
    
    return self;
}

#pragma mark - Custom accessors
- (CGFloat)sectionHeight
{
    if (self.items.count == 0)
    {
        return 0;
    }
    
    return 56;
}

@end
