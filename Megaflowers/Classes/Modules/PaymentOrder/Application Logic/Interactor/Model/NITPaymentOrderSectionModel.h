//
//  NITPaymentOrderSectionModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "RRNCollapsableTableViewController.h"

@class NITPaymentOrderModel, SWGInlineResponse2002;

@interface NITPaymentOrderSectionModel : NSObject <RRNCollapsableTableViewSectionModelProtocol>

@property (nonatomic) NSNumber * sectionId;
@property (nonatomic) NSString * sectionCode;
@property (nonatomic) CGFloat sectionHeight;

- (instancetype)initWithModel:(SWGInlineResponse2002 *)model;

@end
