//
//  NITWebPayModule.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/18/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class SWGOrder, NITBillModel;

@interface NITWebPayModule : NSObject

- (void)buyByPayPalForOrder:(SWGOrder *)oredrModel
                       bill:(NITBillModel *)billModel
         fromViewController:(id)viewController
                    success:(void (^)(NSString *paymentId))onSuccess
                  cancelled:(void (^)())onCancelled
                      error:(void(^)(NSError *error))onError;

@end
