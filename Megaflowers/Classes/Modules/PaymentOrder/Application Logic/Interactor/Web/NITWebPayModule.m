//
//  NITWebPayModule.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/18/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITWebPayModule.h"
#import "SWGOrder.h"
#import "NITBillModel.h"
#import "NITBillAcquiringModel.h"
#import "NITPaymentOrderWebViewController.h"

@interface NITWebPayModule ()

@property (nonatomic) UIViewController * viewController;;
@property (nonatomic) void (^onSuccess)(NSString *paymentId);
@property (nonatomic) void (^onCancelled)();
@property (nonatomic) void (^onError)(NSError *error);

@end

@implementation NITWebPayModule

#pragma mark - Public
- (void)buyByPayPalForOrder:(SWGOrder *)oredrModel
                       bill:(NITBillModel *)billModel
         fromViewController:(UIViewController *)viewController
                    success:(void (^)(NSString *paymentId))onSuccess
                  cancelled:(void (^)())onCancelled
                      error:(void(^)(NSError *error))onError

{
    NSParameterAssert(viewController);
    NSParameterAssert(onSuccess);
    NSParameterAssert(onCancelled);
    NSParameterAssert(onError);
    
    self.viewController = viewController;
    self.onSuccess = onSuccess;
    self.onCancelled = onCancelled;
    self.onError = onError;
    
    NSNumber * sumRemain = billModel.sum;
    
    if (sumRemain.floatValue <= 0)
    {
        self.onError([NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"Заказ уже оплачен", nil)}]);
        return;
    }
    
    NITPaymentOrderWebViewController * webVC = [NITPaymentOrderWebViewController new];
    
    webVC.urlString = [billModel.acquiring paymentUrl];
    webVC.onSuccess = onSuccess;
    webVC.onCancelled = onCancelled;
    webVC.onError = onError;
    webVC.successUrlString = billModel.acquiring.successUrl;
    webVC.failUrlString = billModel.acquiring.failUrl;
    
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webVC];
    [self.viewController presentViewController:navigationController animated:true completion:^{}];
}

@end
