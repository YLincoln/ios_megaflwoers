//
//  NITPaymentOrderAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderAPIDataManager.h"
#import "SWGPaymentsApi.h"
#import "SWGOrderApi.h"

@implementation NITPaymentOrderAPIDataManager

- (void)getPaymentTypesByOrderID:(NSNumber *)orderID sum:(NSNumber *)sum withHandler:(void(^)(NSArray<SWGInlineResponse2002> *output))handler
{
    [HELPER startLoading];
    [[SWGPaymentsApi new] paymentsListGetWithLang:HELPER.lang site:API.site city:USER.cityID orderId:orderID sum:sum online:nil completionHandler:^(NSArray<SWGInlineResponse2002> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(output);
    }];
}

- (void)postPaymentByOrderID:(NSNumber *)orderID orderHash:(NSString *)orderHash paymentId:(NSNumber *)paymentId sum:(NSNumber *)sum withHandler:(void(^)(SWGBill *output, NSError *error))handler
{
    [HELPER startLoading];
    [[SWGPaymentsApi new] paymentsBillPostWithLang:HELPER.lang site:API.site city:USER.cityID orderId:orderID orderHash:orderHash paymentId:paymentId sum:sum completionHandler:^(SWGBill *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(output, error);
    }];
}

- (void)getPaymentStatusByOrderID:(NSNumber *)orderID orderHash:(NSString *)orderHash billId:(NSNumber *)billId withHandler:(void(^)(SWGInlineResponse200 *output))handler
{
    [HELPER startLoading];
    [[SWGPaymentsApi new] paymentsBillIdStatusGetWithLang:HELPER.lang site:API.site city:USER.cityID _id:billId orderId:orderID orderHash:orderHash completionHandler:^(SWGInlineResponse200 *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(output);
    }];
}

- (void)getOrderBillsByOrderID:(NSNumber *)orderID orderHash:(NSString *)orderHash withHandler:(void(^)(NSArray<SWGBill> *output))handler
{
    [HELPER startLoading];
    [[SWGPaymentsApi new] paymentsBillGetWithLang:HELPER.lang site:API.site city:USER.cityID orderId:orderID orderHash:orderHash completionHandler:^(NSArray<SWGBill> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(output);
    }];
}

- (void)updateBillById:(NSNumber *)billId transactionId:(NSString *)transactionId orderID:(NSNumber *)orderID orderHash:(NSString *)orderHash withHandler:(void(^)(BOOL success))handler
{
    [HELPER startLoading];
    [[SWGPaymentsApi new] paymentsBillIdPatchWithLang:HELPER.lang site:API.site city:USER.cityID _id:billId orderId:orderID orderHash:orderHash status:nil paymentId:nil transactionId:transactionId completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(error ? false : true);
    }];
}

- (void)markDeletedBillById:(NSNumber *)billId orderID:(NSNumber *)orderID orderHash:(NSString *)orderHash withHandler:(void(^)(BOOL success))handler
{
    [HELPER startLoading];
    [[SWGPaymentsApi new] paymentsBillIdPatchWithLang:HELPER.lang site:API.site city:USER.cityID _id:billId orderId:orderID orderHash:orderHash status:@(BillStatusCanceled) paymentId:nil transactionId:nil completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(error ? false : true);
    }];
}

@end
