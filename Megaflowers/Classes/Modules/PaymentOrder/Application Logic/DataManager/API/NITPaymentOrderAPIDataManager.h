//
//  NITPaymentOrderAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPaymentOrderProtocols.h"

@interface NITPaymentOrderAPIDataManager : NSObject <NITPaymentOrderAPIDataManagerInputProtocol>

@end
