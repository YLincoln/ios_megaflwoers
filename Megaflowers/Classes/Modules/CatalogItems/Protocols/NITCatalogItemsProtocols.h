//
//  NITCatalogItemsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemViewModel.h"
#import "NITCatalogItemSKUViewModel.h"
#import "NITBasketProductModel.h"

@protocol NITCatalogItemsInteractorOutputProtocol;
@protocol NITCatalogItemsInteractorInputProtocol;
@protocol NITCatalogItemsViewProtocol;
@protocol NITCatalogItemsPresenterProtocol;
@protocol NITCatalogItemsLocalDataManagerInputProtocol;
@protocol NITCatalogItemsAPIDataManagerInputProtocol;

@class NITCatalogItemsWireFrame;

@protocol NITCatalogItemsViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogItemsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEW
 */
- (void)reloadView;
@end

@protocol NITCatalogItemsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)initCatalogItemsModuleForView:(id)view;
- (void)showCatalogItemPreviewByModel:(NITCatalogItemViewModel *)model;
- (void)showCatalogItemDetailsByModel:(NITCatalogItemViewModel *)model from:(id)fromView;
- (void)showBasketFrom:(id)fromView;
- (void)showUserRegistrationFrom:(id)fromView;
@end

@protocol NITCatalogItemsPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogItemsViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogItemsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogItemsWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEW -> PRESENTER
 */
+ (void)initCatalogItemsModuleForView:(id)view;
- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model;
- (void)showCatalogItemPreviewByModel:(NITCatalogItemViewModel *)model;
- (void)showCatalogItemDetailsByModel:(NITCatalogItemViewModel *)model;
- (void)addCatalogItemToBasket:(NITCatalogItemViewModel *)model;
@end

@protocol NITCatalogItemsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITCatalogItemsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogItemsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogItemsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogItemsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model andSku:(NITCatalogItemSKUViewModel *)sku;
- (void)addCatalogItemToBasket:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex;
- (NSArray <NITCatalogItemSKUViewModel *> *)favoriteSkusByBouquet:(NITCatalogItemViewModel *)model;
@end


@protocol NITCatalogItemsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogItemsAPIDataManagerInputProtocol <NITCatalogItemsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)changeFavoriteStateForBouquetId:(NSNumber *)bouquetId state:(BOOL)state;
@end

@protocol NITCatalogItemsLocalDataManagerInputProtocol <NITCatalogItemsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)bouquet andSku:(NITCatalogItemSKUViewModel *)sku;
- (void)addCatalogItemToBasket:(NITBasketProductModel *)model;
- (NSArray <NITFavoriteBouquet *> *)favoriteSkusByBouquetId:(NSNumber *)bouquetId;
@end
