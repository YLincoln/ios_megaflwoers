//
//  NITCatalogItemsView.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/23/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemsView.h"
#import "NITCatalogDirectoryCell.h"
#import "KRLCollectionViewGridLayout.h"
#import "NITCatalogItemsPresenter.h"
#import "UIView+Screenshot.h"
#import "UIView+Genie.h"

@interface NITCatalogItemsView ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
NITCatalogDirectoryCellDelegate
>

@property (nonatomic) KRLCollectionViewGridLayout * gridLayout;

@end

@implementation NITCatalogItemsView

#pragma mark - Base methods
- (void)viewDidLoad
{
    [NITCatalogItemsPresenter initCatalogItemsModuleForView:self];
    [self initUI];
}

#pragma mark - Custom accessors
- (KRLCollectionViewGridLayout *)gridLayout
{
    return (KRLCollectionViewGridLayout *)self.collectionView.collectionViewLayout;
}

#pragma mark - Public
- (void)reloadCatalogItemsByModels:(NSArray <NITCatalogItemViewModel *> *)models delegate:(id)delegate
{
    self.delegate = delegate;
    self.items = models;
    [self reloadView];
}

- (void)insertDataByModels:(NSArray<NITCatalogItemViewModel *> *)models lastYOffset:(CGFloat)lastYOffset
{
    if (models.count > 0)
    {
        NSMutableArray * allItems = [NSMutableArray arrayWithArray:self.items];
        [allItems addObjectsFromArray:models];
        
        NSArray * indexPaths = [models bk_map:^id(id obj) {
            return [NSIndexPath indexPathForRow:[allItems indexOfObject:obj] inSection:0];
        }];
        
        self.items = allItems;
        
        [self.collectionView insertItemsAtIndexPaths:indexPaths];
        [self.collectionView setContentOffset:CGPointMake(0, lastYOffset + 50) animated:true];
    }
}

- (void)updateViewMode:(CatalogItemsViewMode)CatalogItemsViewMode
{
    switch (CatalogItemsViewMode) {
        case CatalogItemsViewModeList: {
            self.gridLayout.numberOfItemsPerLine = 1;
            self.gridLayout.aspectRatio = 1.3f;
        } break;
            
        case CatalogItemsViewModeSection: {
            self.gridLayout.numberOfItemsPerLine = 2;
            self.gridLayout.aspectRatio = 0.71f;
        } break;
    }
    
    [self.collectionView setContentOffset:self.collectionView.contentOffset animated:false];
    [UIView animateWithDuration:.2f delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.collectionView performBatchUpdates:^{
            [self.collectionView.collectionViewLayout invalidateLayout];
        } completion:^(BOOL finished) {
            [[self.collectionView visibleCells] bk_each:^(NITCatalogDirectoryCell *cell) {
                [cell changeViewMode:CatalogItemsViewMode];
            }];
        }];
        
    } completion:nil];
}

#pragma mark - Private
- (void)initUI
{
    [self setKeyboardActiv:true];
    [self.collectionView registerNibArray:@[_s(NITCatalogDirectoryCell)]];
    [self.collectionView setAlwaysBounceVertical:true];
    [self.collectionView.panGestureRecognizer setCancelsTouchesInView:true];
    [self.gridLayout setSectionInset:UIEdgeInsetsZero];
}

- (void)animateItemToCartByIndex:(NSInteger)index
{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    UICollectionViewCell * cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    UICollectionViewLayoutAttributes * attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellFrameInSuperview = [self.collectionView convertRect:attributes.frame toView:[[self.collectionView superview] superview].superview.superview];
    
    UIImageView * genieView = [[UIImageView alloc] initWithFrame:cellFrameInSuperview];
    genieView.image = [cell imageByRenderingView];
    [self addSubview:genieView];
    
    CGPoint endPoint = WINDOW.basketCenter;
    CGRect endRect = CGRectMake(endPoint.x, endPoint.y, 1, 1);

    [genieView genieInTransitionWithDuration:.6f destinationRect:endRect destinationEdge:BCRectEdgeTop completion:^{
        [genieView removeFromSuperview];
    }];
}

#pragma mark - NITCatalogItemsViewProtocol
- (void)reloadView
{
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITCatalogDirectoryCell) forIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(NITCatalogDirectoryCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if ([self.delegate respondsToSelector:@selector(catalogTableWillBeginDecelerating:)])
    {
        [self.delegate catalogTableWillBeginDecelerating:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([self.delegate respondsToSelector:@selector(catalogTableDidScroll:)])
    {
        [self.delegate catalogTableDidScroll:scrollView];
    }
}

#pragma mark - NITCatalogDirectoryCellDelegate
- (void)didChangeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model
{
    [self.presenter changeFavoriteStateForBouquet:model];
}

- (void)didShowPreviewForBouquet:(NITCatalogItemViewModel *)model
{
    [self.presenter showCatalogItemPreviewByModel:model];
}

- (void)didSelectBouquet:(NITCatalogItemViewModel *)model
{
    [self.presenter showCatalogItemDetailsByModel:model];
}

- (void)didAddtoBasketProduct:(NITCatalogItemViewModel *)model
{
    [self.presenter addCatalogItemToBasket:model];
    NSInteger index = [self.items indexOfObject:model];
    [self animateItemToCartByIndex:index];
}


@end
