//
//  NITCatalogItemsView.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/23/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "XibView.h"
#import "NITCatalogItemsProtocols.h"

@protocol NITCatalogItemsViewDelegate <NSObject>

- (void)catalogTableWillBeginDecelerating:(UIScrollView *)catalogTable;
- (void)catalogTableDidScroll:(UIScrollView *)catalogTable;

@end

@interface NITCatalogItemsView : XibView <NITCatalogItemsViewProtocol>

@property (nonatomic) IBOutlet UICollectionView * collectionView;
@property (nonatomic, strong) id <NITCatalogItemsPresenterProtocol> presenter;
@property (nonatomic, weak) id <NITCatalogItemsViewDelegate> delegate;
@property (nonatomic) NSArray <NITCatalogItemViewModel *> * items;

- (void)reloadView;
- (void)reloadCatalogItemsByModels:(NSArray <NITCatalogItemViewModel *> *)models delegate:(id)delegate;
- (void)insertDataByModels:(NSArray<NITCatalogItemViewModel *> *)models lastYOffset:(CGFloat)lastYOffset;
- (void)updateViewMode:(CatalogItemsViewMode)CatalogItemsViewMode;

@end
