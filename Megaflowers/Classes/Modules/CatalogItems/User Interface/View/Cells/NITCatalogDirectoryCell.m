//
//  NITCatalogDirectoryCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryCell.h"
#import "NITCatalogItemViewModel.h"
#import "UIImageView+AFNetworking.h"
#import "NSArray+BlocksKit.h"
#import "DGActivityIndicatorView.h"

static NSInteger const verticalTag      = 101;
static NSInteger const horizontalTag    = 102;
static CGFloat   const imageTopInset    = 29.f;

@interface NITCatalogDirectoryCell ()

@property (nonatomic) IBOutlet UIView * horizontalView;
@property (nonatomic) IBOutlet UIView * verticalView;

@property (nonatomic) IBOutletCollection(UILabel) NSArray * title;
@property (nonatomic) IBOutletCollection(UILabel) NSArray * price;
@property (nonatomic) IBOutletCollection(UIButton) NSArray * favoriteBtn;
@property (nonatomic) IBOutletCollection(UIImageView) NSArray * icon;
@property (nonatomic) IBOutletCollection(UIImageView) NSArray * image;
@property (nonatomic) IBOutletCollection(UIImageView) NSArray * basketImg;
@property (nonatomic) IBOutletCollection(UIButton) NSArray * basketBtn;
@property (nonatomic) IBOutletCollection(UIView) NSArray * lineRight;
@property (nonatomic) IBOutletCollection(UIView) NSArray * lineBottom;
@property (nonatomic) IBOutletCollection(DGActivityIndicatorView) NSArray * indicator;
@property (nonatomic) IBOutlet NSLayoutConstraint * imageTop;
@property (nonatomic) IBOutlet NSLayoutConstraint * imageH;
@property (nonatomic) IBOutlet NSLayoutConstraint * titleH;

@property (nonatomic) CatalogItemsViewMode catalogItemsViewMode;

@end

@implementation NITCatalogDirectoryCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (![HELPER forceTouchAvailable])
    {
        UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [self addGestureRecognizer:longPress];
    }
    
    UITapGestureRecognizer * select = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(collectionViewSelectCell:)];
    [self addGestureRecognizer:select];
    
    for (DGActivityIndicatorView * loader in self.indicator)
    {
        loader.tintColor = RGB(10, 88, 43);
        loader.type = DGActivityIndicatorAnimationTypeBallClipRotate;
    }
}

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogItemViewModel *)model
{
    _model = model;
    [self updateData];
}

- (void)setCatalogItemsViewMode:(CatalogItemsViewMode)catalogItemsViewMode
{
    _catalogItemsViewMode = catalogItemsViewMode;
    
    self.horizontalView.hidden = catalogItemsViewMode != CatalogItemsViewModeList;
    self.verticalView.hidden = catalogItemsViewMode != CatalogItemsViewModeSection;
}

#pragma mark - IBAction
- (IBAction)favoriteAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didChangeFavoriteStateForBouquet:)])
    {
        [self.delegate didChangeFavoriteStateForBouquet:self.model];
        [self updateFavoriteState];
    }
}

- (IBAction)addToBasketProduct:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didAddtoBasketProduct:)])
    {
        [self.delegate didAddtoBasketProduct:self.model];
        
        for (UIButton * button in self.basketBtn)
        {
            [button setEnabled:false];
            [button setImage:[UIImage imageNamed:@"ic_in_basket_active"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - Public
- (void)changeViewMode:(CatalogItemsViewMode)CatalogItemsViewMode
{
    self.catalogItemsViewMode = CatalogItemsViewMode;
    [self updateData];
}

- (void)changeImageTopRelativelyPosition:(CGFloat)position
{
    /*self.imageTop.constant = (position - ViewY(self)) / 12.0 + imageTopInset;
    [self layoutIfNeeded];*/
}

#pragma mark - Private
- (void)updateData
{
    self.catalogItemsViewMode = self.model.CatalogItemsViewMode;
    
    for (UILabel * label in self.title)
    {
        label.text = self.model.title;
        if (label.tag == verticalTag)
        {
            self.titleH.constant = [label heightOfMultiLineLabel];
            [self layoutIfNeeded];
        }
    }
    
    NSString * pricePrefix = [self.model.skus count] > 1 ? [NSString stringWithFormat:@"%@ ", NSLocalizedString(@"От", nil)] : @"";
    
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSString * priceString = [self.model.price stringValue];//[[formatter stringFromNumber:self.model.price] stringByReplacingOccurrencesOfString:@"," withString:@" "];
    NSString * discountPriceString = [self.model.discountPrice stringValue];//[[formatter stringFromNumber:self.model.discountPrice] stringByReplacingOccurrencesOfString:@"," withString:@" "];
    
    for (UILabel * label in self.price)
    {
        if ([self.model.price integerValue] != [self.model.discountPrice integerValue])
        {
            NSString * title = [NSString stringWithFormat:@"%@ %@ %@ %@", pricePrefix, priceString, discountPriceString, NSLocalizedString(@"руб.", nil)];
            NSMutableAttributedString * attrText = [[NSMutableAttributedString alloc] initWithString:title];

            [attrText addAttribute:NSBaselineOffsetAttributeName
                             value:@(0)
                             range:NSMakeRange(pricePrefix.length + 1, priceString.length)];

            [attrText addAttribute:NSStrikethroughStyleAttributeName
                             value:@(2)
                             range:NSMakeRange(pricePrefix.length + 1, priceString.length)];
            
            NSInteger length = pricePrefix.length + priceString.length + 1;
            [attrText addAttribute:NSForegroundColorAttributeName
                             value:RGB(254, 148, 205)
                             range:NSMakeRange(length, attrText.length - length)];
            
            label.attributedText = attrText;
        }
        else
        {
            label.text = [NSString stringWithFormat:@"%@ %@ %@", pricePrefix, discountPriceString, NSLocalizedString(@"руб.", nil)];
        }
    }
    

    for (UIImageView * imageView in self.icon)
    {
        imageView.hidden = false;
        imageView.image = nil;
        [imageView setImageWithURL:[NSURL URLWithString:self.model.iconPath]];
    }

    [self updateImage];
    [self updateFavoriteState];
    [self updateBasketState];
}

- (void)updateImage
{
    for (UIImageView * imageView in self.image)
    {
        imageView.image = nil;
        if (imageView.tag == horizontalTag)
        {
            __weak UIImageView * weakImageView = imageView;
            DGActivityIndicatorView * loader = self.indicator.firstObject;
            loader.hidden = false;
            [loader startAnimating];
            [imageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.model.previewPath]]
                             placeholderImage:nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                          loader.hidden = true;
                                          [loader stopAnimating];
                                          if (image)
                                          {
                                              self.imageH.constant = ViewWidth(weakImageView) * image.size.height / image.size.width;
                                              self.imageTop.constant = - MIN(self.imageH.constant / 100 * self.model.offset - imageTopInset,
                                                                             self.imageH.constant - ViewHeight(self) + imageTopInset);
                                              [self layoutIfNeeded];
                                              weakImageView.image = image;
                                          }
                                      }
                                      failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                          loader.hidden = true;
                                          [loader stopAnimating];
                                      }];
        }
        else
        {
            __weak UIImageView * weakImageView = imageView;
            DGActivityIndicatorView * loader = self.indicator.lastObject;
            loader.hidden = false;
            [loader startAnimating];
            [imageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.model.previewPath]]
                             placeholderImage:nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                          loader.hidden = true;
                                          [loader stopAnimating];
                                          weakImageView.image = image;
                                      }
                                      failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                          loader.hidden = true;
                                          [loader stopAnimating];
                                      }];
        }
    }
}

- (void)updateFavoriteState
{
    NSString * imgaeName = self.model.favorite ? @"pink_heart" : @"green_heart";
    for (UIButton * button in self.favoriteBtn)
    {
        button.hidden = self.model.contentType != ProductContentTypeBouqets;
        [button setImage:[UIImage imageNamed:imgaeName] forState:UIControlStateNormal];
    }
}

- (void)updateBasketState
{
    NSString * imgaeName = self.model.inBasket ? @"ic_in_basket_active" : @"ic_in_basket";
    for (UIButton * button in self.basketBtn)
    {
        button.enabled = !self.model.inBasket;
        [button setImage:[UIImage imageNamed:imgaeName] forState:UIControlStateNormal];
    }
}

- (void)longPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan)
    {
        if ([self.delegate respondsToSelector:@selector(didShowPreviewForBouquet:)])
        {
            [self.delegate didShowPreviewForBouquet:self.model];
        }
    }
}

- (void)collectionViewSelectCell:(UITapGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateEnded)
    {
        if ([self.delegate respondsToSelector:@selector(didSelectBouquet:)])
        {
            [self.delegate didSelectBouquet:self.model];
        }
    }
}

@end
