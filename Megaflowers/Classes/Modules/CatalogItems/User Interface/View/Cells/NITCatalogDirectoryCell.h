//
//  NITCatalogDirectoryCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/28/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogDirectoryProtocols.h"

@class NITCatalogItemViewModel;

@protocol NITCatalogDirectoryCellDelegate <NSObject>

- (void)didChangeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model;
- (void)didShowPreviewForBouquet:(NITCatalogItemViewModel *)model;
- (void)didSelectBouquet:(NITCatalogItemViewModel *)model;
- (void)didAddtoBasketProduct:(NITCatalogItemViewModel *)model;

@end

@interface NITCatalogDirectoryCell : UICollectionViewCell

@property (nonatomic) NITCatalogItemViewModel * model;
@property (nonatomic, weak) id <NITCatalogDirectoryCellDelegate> delegate;

- (void)changeViewMode:(CatalogItemsViewMode)CatalogItemsViewMode;
- (void)changeImageTopRelativelyPosition:(CGFloat)position;

@end
