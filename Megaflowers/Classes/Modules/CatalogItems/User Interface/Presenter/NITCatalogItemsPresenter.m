//
//  NITCatalogItemsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemsPresenter.h"
#import "NITCatalogItemsWireframe.h"

@implementation NITCatalogItemsPresenter

#pragma mark - NITCatalogItemsPresenterProtocol
+ (void)initCatalogItemsModuleForView:(id)view
{
    [NITCatalogItemsWireFrame initCatalogItemsModuleForView:view];
}

- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model
{
    if ([USER isAutorize])
    {
        if (model.skus.count == 1)
        {
            NITCatalogItemSKUViewModel * sku = model.skus.firstObject;
            sku.favorite = !sku.favorite;
            [self.interactor changeFavoriteStateForBouquet:model andSku:sku];
        }
        else if (model.skus.count > 4)
        {
            NITCatalogItemSKUViewModel * sku = model.defaultSku ? : model.skus.firstObject;
            sku.favorite = !sku.favorite;
            [self.interactor changeFavoriteStateForBouquet:model andSku:sku];
        }
        else if (model.skus.count > 1)
        {
            NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
            
            NSString * title;
            NSArray * skus;
            
            if (model.favorite)
            {
                title = NSLocalizedString(@"Выберите какой размер букета удалить из избранного", nil);
                skus = [self.interactor favoriteSkusByBouquet:model];
                
                if (skus.count == 1)
                {
                    NITCatalogItemSKUViewModel * sku = skus.firstObject;
                    sku.favorite = !sku.favorite;
                    [self.interactor changeFavoriteStateForBouquet:model andSku:sku];
                    
                    return;
                }
            }
            else
            {
                title = NSLocalizedString(@"Выберите какой размер букета добавить в избранное", nil);
                skus = model.skus;
            }
            
            weaken(self);
            NSMutableArray * actions = [NSMutableArray new];
            for (NITCatalogItemSKUViewModel * sku in skus)
            {
                NITActionButton * action = [[NITActionButton alloc] initWithTitle:sku.presentName actionBlock:^{
                    sku.favorite = !sku.favorite;
                    [weakSelf.interactor changeFavoriteStateForBouquet:model andSku:sku];
                    [weakSelf.view reloadView];
                }];
                [actions addObject:action];
            }
            
            [HELPER showActionSheetFromView:WINDOW.visibleController withTitle:title  actionButtons:actions cancelButton:cancel];
        }
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        
        weaken(self);
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Регистрация", nil) actionBlock:^{
            [weakSelf.wireFrame showUserRegistrationFrom:WINDOW.visibleController];
        }];
        
        [HELPER showActionSheetFromView:WINDOW.visibleController
                              withTitle:NSLocalizedString(@"Данная функция приложения не доступна без регистрации", nil)
                          actionButtons:@[action1] cancelButton:cancel];
    }

}

- (void)addCatalogItemToBasket:(NITCatalogItemViewModel *)model
{
    [self.interactor addCatalogItemToBasket:model skuIndex:0];
}

- (void)showCatalogItemPreviewByModel:(NITCatalogItemViewModel *)model
{
    [self.wireFrame showCatalogItemPreviewByModel:model];
}

- (void)showCatalogItemDetailsByModel:(NITCatalogItemViewModel *)model
{
    [self.wireFrame showCatalogItemDetailsByModel:model from:WINDOW.visibleController];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadView) name:kSaveBusketBouquetNotification object:nil];
}

- (void)reloadView
{
    [self.view reloadView];
}

@end
