//
//  NITCatalogItemsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogItemsProtocols.h"

@class NITCatalogItemsWireFrame;

@interface NITCatalogItemsPresenter : NSObject <NITCatalogItemsPresenterProtocol, NITCatalogItemsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogItemsViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogItemsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogItemsWireFrameProtocol> wireFrame;

@end
