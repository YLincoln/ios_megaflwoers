//
//  NITCatalogItemsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemsWireFrame.h"
#import "NITCatalogItemsView.h"
#import "NITProductDetailsWireFrame.h"
#import "NITAuthorizationWireFrame.h"

@implementation NITCatalogItemsWireFrame

#pragma mark - NITCatalogItemsWireFrameProtocol
+ (void)initCatalogItemsModuleForView:(NITCatalogItemsView *)view
{
    // Generating module components
    id <NITCatalogItemsPresenterProtocol, NITCatalogItemsInteractorOutputProtocol> presenter = [NITCatalogItemsPresenter new];
    id <NITCatalogItemsInteractorInputProtocol> interactor = [NITCatalogItemsInteractor new];
    id <NITCatalogItemsAPIDataManagerInputProtocol> APIDataManager = [NITCatalogItemsAPIDataManager new];
    id <NITCatalogItemsLocalDataManagerInputProtocol> localDataManager = [NITCatalogItemsLocalDataManager new];
    id <NITCatalogItemsWireFrameProtocol> wireFrame = [NITCatalogItemsWireFrame new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
}

- (void)showCatalogItemDetailsByModel:(NITCatalogItemViewModel *)model from:(UIViewController *)fromViewController
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController bouquet:model selectType:ProductSelectTypeView];
}

- (void)showCatalogItemPreviewByModel:(NITCatalogItemViewModel *)model
{
    [NITProductDetailsWireFrame presentNITProductDetailsPreviewModuleWithBouquet:model isViewMode:false];
}

- (void)showUserRegistrationFrom:(UIViewController *)fromViewController
{
    [NITAuthorizationWireFrame presentNITAuthorizationModuleFrom:fromViewController];
}

- (void)showBasketFrom:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:1];
}

@end
