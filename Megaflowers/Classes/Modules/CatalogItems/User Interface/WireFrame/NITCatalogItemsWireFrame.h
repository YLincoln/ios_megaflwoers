//
//  NITCatalogItemsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogItemsProtocols.h"
#import "NITCatalogItemsLocalDataManager.h"
#import "NITCatalogItemsAPIDataManager.h"
#import "NITCatalogItemsInteractor.h"
#import "NITCatalogItemsPresenter.h"
#import "NITCatalogItemsWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCatalogItemsWireFrame : NSObject <NITCatalogItemsWireFrameProtocol>

@end
