//
//  NITCatalogItemsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemsAPIDataManager.h"
#import "SWGUserApi.h"

@implementation NITCatalogItemsAPIDataManager

#pragma mark - NITCatalogItemsAPIDataManagerInputProtocol
- (void)changeFavoriteStateForBouquetId:(NSNumber *)bouquetId state:(BOOL)state
{
    if (!state)
    {
        [HELPER startLoading];
        [[SWGUserApi new] userBouquetPostWithUser:@([USER.identifier integerValue]) bouquet:bouquetId completionHandler:^(NSError *error) {
            [HELPER stopLoading];
            if (error) [HELPER logError:error method:METHOD_NAME];
        }];
    }
    else
    {
        [HELPER startLoading];
        [[SWGUserApi new]  userBouquetIdDeleteWithId:bouquetId completionHandler:^(NSError *error) {
            [HELPER stopLoading];
            if (error) [HELPER logError:error method:METHOD_NAME];
        }];
    }
}

@end
