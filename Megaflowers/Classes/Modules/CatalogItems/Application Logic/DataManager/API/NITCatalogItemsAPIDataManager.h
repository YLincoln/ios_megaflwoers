//
//  NITCatalogItemsAPIDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogItemsProtocols.h"

@interface NITCatalogItemsAPIDataManager : NSObject <NITCatalogItemsAPIDataManagerInputProtocol>

@end
