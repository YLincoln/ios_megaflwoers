//
//  NITCatalogItemsLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemsLocalDataManager.h"
#import "NITFavoriteBouquet.h"
#import "NITBusketBouquet.h"
#import "NITBusketAttachment.h"

@implementation NITCatalogItemsLocalDataManager

#pragma mark - NITCatalogLocalDataManagerInputProtocol
- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)bouquet andSku:(NITCatalogItemSKUViewModel *)sku
{
    BOOL favorite = !sku.favorite;
    
    if (favorite)
    {
        [NITFavoriteBouquet createFavoriteBouquetByModel:bouquet andSkuModel:sku];
        [TRACKER trackEvent:TrackerEventLikeBouquet parameters:@{@"id":sku.identifier}];
    }
    else
    {
        [NITFavoriteBouquet deleteBouquetBySkuID:sku.identifier];
    }
}

- (void)addCatalogItemToBasket:(NITBasketProductModel *)model
{
    if (model)
    {
        switch (model.contentType) {
            case ProductContentTypeBouqets: [NITBusketBouquet addBouquet:model];        break;
            case ProductContentTypeAttach:  [NITBusketAttachment addAttachment:model];  break;
        }
    }
}

- (NSArray <NITFavoriteBouquet *> *)favoriteSkusByBouquetId:(NSNumber *)bouquetId
{
    return [NITFavoriteBouquet skusByBouquetID:bouquetId];
}

@end
