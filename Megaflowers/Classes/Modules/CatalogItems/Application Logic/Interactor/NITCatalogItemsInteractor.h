//
//  NITCatalogItemsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogItemsProtocols.h"

@interface NITCatalogItemsInteractor : NSObject <NITCatalogItemsInteractorInputProtocol>

@property (nonatomic, weak) id <NITCatalogItemsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogItemsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogItemsLocalDataManagerInputProtocol> localDataManager;

@end
