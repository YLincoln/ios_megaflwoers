//
//  NITCatalogItemsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 06/24/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemsInteractor.h"
#import "NITFavoriteBouquet.h"
#import "NITProductDetailsAPIDataManager.h"

@implementation NITCatalogItemsInteractor

#pragma mark - NITCatalogItemsInteractorInputProtocol
- (void)changeFavoriteStateForBouquet:(NITCatalogItemViewModel *)model andSku:(NITCatalogItemSKUViewModel *)sku
{
    if ([USER isAutorize])
    {
        [self.localDataManager changeFavoriteStateForBouquet:model andSku:sku];
        [self.APIDataManager changeFavoriteStateForBouquetId:sku.identifier state:!sku.favorite];
    }
}

- (void)addCatalogItemToBasket:(NITCatalogItemViewModel *)model skuIndex:(NSInteger)skuIndex
{
    if (model && [model.skus count] > 0)
    {
        NITProductDetailsAPIDataManager * productDirectoryAPIDataManager = [NITProductDetailsAPIDataManager new];
        switch (model.contentType) {
            case ProductContentTypeBouqets: {
                [productDirectoryAPIDataManager getBouquetsByID:model.identifier withHandler:^(SWGBouquet *output, NSError * error) {
                    
                    NITCatalogItemViewModel * bouquetModel = model;
                    if (output)
                    {
                        bouquetModel = [[NITCatalogItemViewModel alloc] initWithBouquet:output];
                        NITBasketProductModel * item = [[NITBasketProductModel alloc] initWidthModel:bouquetModel skuIndex:skuIndex];
                        [self.localDataManager addCatalogItemToBasket:item];
                    }
                }];
            } break;
                
            case ProductContentTypeAttach: {
                [productDirectoryAPIDataManager getAttachByID:model.identifier withHandler:^(SWGAttach *output) {
                    
                    NITCatalogItemViewModel * attachModel = model;
                    if (output)
                    {
                        attachModel = [[NITCatalogItemViewModel alloc] initWithAttach:output];
                        NITBasketProductModel * item = [[NITBasketProductModel alloc] initWidthModel:attachModel skuIndex:skuIndex];
                        [self.localDataManager addCatalogItemToBasket:item];
                    }
                }];
            } break;
        }
    }
}

- (NSArray <NITCatalogItemSKUViewModel *> *)favoriteSkusByBouquet:(NITCatalogItemViewModel *)model
{
    NSMutableArray * skus = [NSMutableArray new];
    NSArray * result = [self.localDataManager favoriteSkusByBouquetId:model.identifier];
    for (NITFavoriteBouquet * b in result)
    {
        [skus addObjectsFromArray:[model.skus bk_select:^BOOL(NITCatalogItemSKUViewModel * obj) {
            return [obj.identifier isEqualToNumber:@(b.identifier.integerValue)];
        }]];
    }
    
    return skus;
}

@end
