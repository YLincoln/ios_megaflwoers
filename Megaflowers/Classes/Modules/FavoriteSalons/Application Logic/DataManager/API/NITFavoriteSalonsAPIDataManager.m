//
//  NITFavoriteSalonsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsAPIDataManager.h"
#import "SWGUserApi.h"

@implementation NITFavoriteSalonsAPIDataManager

- (void)removeFavoriteSalonByID:(NSNumber *)salonID
{
    [HELPER startLoading];
    [[SWGUserApi new] userSalonIdDeleteWithId:salonID site:API.site completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
    }];
}

@end
