//
//  NITFavoriteSalonsLocalDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsLocalDataManager.h"

@implementation NITFavoriteSalonsLocalDataManager

- (NSArray <NITFavoriteSalon *> *)getFavoriteSalons
{
    return [NITFavoriteSalon allSalons];
}

- (void)removeFavoriteSalonByID:(NSNumber *)salonID
{
    [NITFavoriteSalon deleteSalonByID:salonID];
}

@end
