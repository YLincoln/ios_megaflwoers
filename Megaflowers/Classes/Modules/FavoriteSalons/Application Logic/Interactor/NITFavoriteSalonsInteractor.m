//
//  NITFavoriteSalonsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsInteractor.h"
#import "NSArray+BlocksKit.h"
#import "NITLoginAPIDataManager.h"

@interface NITFavoriteSalonsInteractor ()

@property (nonatomic)  NSArray <NITAddressModel *> * data;

@end

@implementation NITFavoriteSalonsInteractor

- (void)getFavoriteSalonsFromApi
{
    NITLoginAPIDataManager * api = [NITLoginAPIDataManager new];
    [api getUserSalonsWithHandler:^(NSArray<SWGSalon> *result) {
        [NITFavoriteSalon addSalons:[NITAddressModel salonModelsFromEntities:result]];
    }];
}

- (void)getFavoriteSalons;
{
    self.data = [self bouquetsModelsFromEntities:[self.localDataManager getFavoriteSalons]];
    [self.presenter updateSalons:self.data];
}

- (void)removeFavoriteSalon:(NITAddressModel *)model
{
    [self.localDataManager removeFavoriteSalonByID:model.identifier];
    [self.APIDataManager removeFavoriteSalonByID:model.identifier];
    [self getFavoriteSalons];
}

#pragma mark - Private
- (NSArray <NITAddressModel *> *)bouquetsModelsFromEntities:(NSArray <NITFavoriteSalon *> *)entities
{
    return [entities bk_map:^id(NITFavoriteSalon * obj) {
        return [[NITAddressModel alloc] initWidthFavoriteSalon:obj];
    }];
}

@end
