//
//  NITFavoriteSalonsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteSalonsProtocols.h"

@interface NITFavoriteSalonsInteractor : NSObject <NITFavoriteSalonsInteractorInputProtocol>

@property (nonatomic, weak) id <NITFavoriteSalonsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFavoriteSalonsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFavoriteSalonsLocalDataManagerInputProtocol> localDataManager;

@end
