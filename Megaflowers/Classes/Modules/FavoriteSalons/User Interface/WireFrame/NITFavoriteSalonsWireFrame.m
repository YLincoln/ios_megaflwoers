//
//  NITFavoriteSalonsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsWireFrame.h"
#import "NITSalonsWireFrame.h"

@implementation NITFavoriteSalonsWireFrame

+ (id)createNITFavoriteSalonsModule
{
    // Generating module components
    id <NITFavoriteSalonsPresenterProtocol, NITFavoriteSalonsInteractorOutputProtocol> presenter = [NITFavoriteSalonsPresenter new];
    id <NITFavoriteSalonsInteractorInputProtocol> interactor = [NITFavoriteSalonsInteractor new];
    id <NITFavoriteSalonsAPIDataManagerInputProtocol> APIDataManager = [NITFavoriteSalonsAPIDataManager new];
    id <NITFavoriteSalonsLocalDataManagerInputProtocol> localDataManager = [NITFavoriteSalonsLocalDataManager new];
    id <NITFavoriteSalonsWireFrameProtocol> wireFrame= [NITFavoriteSalonsWireFrame new];
    NITFavoriteSalonsViewController * view = [(NITFavoriteSalonsWireFrame *)wireFrame createViewControllerWithKey:_s(NITFavoriteSalonsViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITFavoriteSalonsModuleFrom:(UIViewController *)fromViewController
{
    NITFavoriteSalonsViewController * view = [NITFavoriteSalonsWireFrame createNITFavoriteSalonsModule];
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showSalonsPreviewByModel:(NITAddressModel *)model from:(UIViewController *)fromViewController;
{
    [NITSalonsWireFrame presentNITSalonsPreviewModuleFrom:fromViewController model:model];
}

@end
