//
//  NITFavoriteSalonsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsProtocols.h"
#import "NITFavoriteSalonsViewController.h"
#import "NITFavoriteSalonsLocalDataManager.h"
#import "NITFavoriteSalonsAPIDataManager.h"
#import "NITFavoriteSalonsInteractor.h"
#import "NITFavoriteSalonsPresenter.h"
#import "NITFavoriteSalonsWireframe.h"
#import "NITRootWireframe.h"

@interface NITFavoriteSalonsWireFrame : NITRootWireframe <NITFavoriteSalonsWireFrameProtocol>

@end
