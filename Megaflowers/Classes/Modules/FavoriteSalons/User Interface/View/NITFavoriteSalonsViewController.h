//
//  NITFavoriteSalonsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITFavoriteSalonsProtocols.h"

@interface NITFavoriteSalonsViewController : NITBaseViewController <NITFavoriteSalonsViewProtocol>

@property (nonatomic, strong) id <NITFavoriteSalonsPresenterProtocol> presenter;

@end
