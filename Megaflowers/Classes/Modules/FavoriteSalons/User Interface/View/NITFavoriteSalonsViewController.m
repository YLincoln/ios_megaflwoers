//
//  NITFavoriteSalonsViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsViewController.h"
#import "NITFavoriteSalonsCell.h"

@interface NITFavoriteSalonsViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
NITFavoriteSalonsCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIView * emptyView;
@property (nonatomic) NSArray <NITAddressModel *> * items;

@end

@implementation NITFavoriteSalonsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Любимые магазины", nil);
    
    // table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITFavoriteSalonsCell) bundle:nil] forCellReuseIdentifier:_s(NITFavoriteSalonsCell)];
}

#pragma mark - NITFavoriteSalonsPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITAddressModel *> *)models
{
    self.items = models;
    self.emptyView.hidden = models.count > 0;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.presenter showSalonsPreviewByModel:self.items[indexPath.row]];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITFavoriteSalonsCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITFavoriteSalonsCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - NITFavoriteSalonsCellDelegate
- (void)didCallToPhone:(NSString *)phone
{
    [self.presenter callToPhone:phone];
}

- (void)didCreateRouteByModel:(NITAddressModel *)address
{
    [self.presenter createRouteByModel:address];
}

- (void)didChangeFavoriteStateForSalon:(NITAddressModel *)model
{
    [self.presenter removeFavoriteSalon:model];
}

- (void)didShowPreviewForAddress:(NITAddressModel *)mdoel
{
    [self.presenter showSalonsPreviewByModel:mdoel];
}

@end
