//
//  NITFavoriteSalonsCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsCell.h"
#import "NITAddressModel.h"

@interface NITFavoriteSalonsCell ()

@property (nonatomic) IBOutlet UILabel * address;
@property (nonatomic) IBOutlet UILabel * schedule;
@property (nonatomic) IBOutlet UILabel * phone;
@property (nonatomic) IBOutlet UIButton * likeBtn;
@property (nonatomic) IBOutlet UIButton * routeBtn;

@end

@implementation NITFavoriteSalonsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITAddressModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)likeAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didChangeFavoriteStateForSalon:)])
    {
        [self.delegate didChangeFavoriteStateForSalon:self.model];
    }
}

- (IBAction)callAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didCallToPhone:)])
    {
        [self.delegate didCallToPhone:self.model.phone];
    }
}

- (IBAction)routeAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didCreateRouteByModel:)])
    {
        [self.delegate didCreateRouteByModel:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.address.text = self.model.address;
    self.schedule.text = self.model.schedule;
    
    [self updatePhone];
}

- (void)updatePhone
{
    NSMutableAttributedString * attrPhone = [[NSMutableAttributedString alloc] initWithString:self.model.phone];
    
    [attrPhone addAttribute:NSFontAttributeName
                      value:[UIFont systemFontOfSize:14 weight:UIFontWeightRegular]
                      range:NSMakeRange(0, attrPhone.length)];
    
    [attrPhone addAttribute:NSUnderlineStyleAttributeName
                      value:@(1)
                      range:NSMakeRange(0, attrPhone.length)];
    
    [attrPhone addAttribute:NSForegroundColorAttributeName
                      value:RGB(10, 88, 43)
                      range:NSMakeRange(0, attrPhone.length)];
    
    self.phone.attributedText = attrPhone;
}

@end
