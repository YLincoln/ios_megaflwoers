//
//  NITFavoriteSalonsCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITAddressModel;

@protocol NITFavoriteSalonsCellDelegate <NSObject>

- (void)didCallToPhone:(NSString *)phone;
- (void)didCreateRouteByModel:(NITAddressModel *)address;
- (void)didChangeFavoriteStateForSalon:(NITAddressModel *)model;

@end

@interface NITFavoriteSalonsCell : UITableViewCell

@property (nonatomic) NITAddressModel * model;
@property (nonatomic, weak) id <NITFavoriteSalonsCellDelegate> delegate;

@end
