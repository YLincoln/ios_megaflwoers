//
//  NITFavoriteSalonsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteSalonsPresenter.h"
#import "NITFavoriteSalonsWireframe.h"

@implementation NITFavoriteSalonsPresenter

- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Data
- (void)initData
{
    [self.interactor getFavoriteSalons];
    [self.interactor getFavoriteSalonsFromApi];
}

- (void)updateData
{
    [self.interactor getFavoriteSalons];
}

- (void)removeFavoriteSalon:(NITAddressModel *)model
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    weaken(self);
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Удалить", nil) actionBlock:^{
        [weakSelf.interactor removeFavoriteSalon:model];
    }];
    
    NSString * title = [NSString stringWithFormat:@"%@ %@ %@",
                        NSLocalizedString(@"Магазин по адресу", nil),
                        model.address,
                        NSLocalizedString(@"будет удален из любимых магазинов", nil)];
    
    [HELPER showActionSheetFromView:self.view withTitle:title actionButtons:@[action1] cancelButton:cancel];
}

- (void)callToPhone:(NSString *)phone
{
    [PHONE callToPhoneNumber:phone];
}

- (void)createRouteByModel:(NITAddressModel *)model
{
    if ([LOCATION_MANAGER currentLocation])
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        
        NSMutableArray * actions = [NSMutableArray new];

        // apple
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://maps.apple.com/"]])
        {
            NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Maps", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action1];
        }
        
        // google
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]])
        {
            NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Google Maps", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action2];
        }
        
        // yandex
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"yandexmaps://maps.yandex.ru/"]])
        {
            NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Yandex Maps", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"yandexmaps://build_route_on_map/?lat_from=%f&lon_from=%f&lat_to=%f&lon_to=%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action3];
        }
        
        [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:actions cancelButton:cancel];
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Перейти к настройкам?", nil) actionBlock:^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        
        [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Для проложения маршрута необходимо включить доступ к геолокации!", nil) actionButtons:@[action1] cancelButton:cancel];
    }
}

- (void)showSalonsPreviewByModel:(NITAddressModel *)model
{
    [self.wireFrame showSalonsPreviewByModel:model from:self.view];
}

#pragma mark - NITFavoriteSalonsInteractorOutputProtocol
- (void)updateSalons:(NSArray <NITAddressModel *> *)models
{
    [self.view reloadDataByModels:models];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveFavoriteSalonNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
