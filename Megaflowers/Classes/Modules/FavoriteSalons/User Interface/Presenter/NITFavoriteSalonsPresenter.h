//
//  NITFavoriteSalonsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteSalonsProtocols.h"

@class NITFavoriteSalonsWireFrame;

@interface NITFavoriteSalonsPresenter : NITRootPresenter <NITFavoriteSalonsPresenterProtocol, NITFavoriteSalonsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITFavoriteSalonsViewProtocol> view;
@property (nonatomic, strong) id <NITFavoriteSalonsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFavoriteSalonsWireFrameProtocol> wireFrame;

@end
