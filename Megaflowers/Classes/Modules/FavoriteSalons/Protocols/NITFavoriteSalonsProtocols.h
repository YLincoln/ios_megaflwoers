//
//  NITFavoriteSalonsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/05/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITAddressModel.h"
#import "NITFavoriteSalon.h"

@protocol NITFavoriteSalonsInteractorOutputProtocol;
@protocol NITFavoriteSalonsInteractorInputProtocol;
@protocol NITFavoriteSalonsViewProtocol;
@protocol NITFavoriteSalonsPresenterProtocol;
@protocol NITFavoriteSalonsLocalDataManagerInputProtocol;
@protocol NITFavoriteSalonsAPIDataManagerInputProtocol;

@class NITFavoriteSalonsWireFrame;

@protocol NITFavoriteSalonsViewProtocol
@required
@property (nonatomic, strong) id <NITFavoriteSalonsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITAddressModel *> *)models;
@end

@protocol NITFavoriteSalonsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITFavoriteSalonsModuleFrom:(id)fromView;
- (void)showSalonsPreviewByModel:(NITAddressModel *)model from:(id)fromViewController;
@end

@protocol NITFavoriteSalonsPresenterProtocol
@required
@property (nonatomic, weak) id <NITFavoriteSalonsViewProtocol> view;
@property (nonatomic, strong) id <NITFavoriteSalonsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFavoriteSalonsWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updateData;
- (void)callToPhone:(NSString *)phone;
- (void)createRouteByModel:(NITAddressModel *)model;
- (void)removeFavoriteSalon:(NITAddressModel *)model;
- (void)showSalonsPreviewByModel:(NITAddressModel *)model;
@end

@protocol NITFavoriteSalonsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateSalons:(NSArray <NITAddressModel *> *)models;
@end

@protocol NITFavoriteSalonsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITFavoriteSalonsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFavoriteSalonsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFavoriteSalonsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getFavoriteSalons;
- (void)getFavoriteSalonsFromApi;
- (void)removeFavoriteSalon:(NITAddressModel *)model;
@end


@protocol NITFavoriteSalonsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
- (void)removeFavoriteSalonByID:(NSNumber *)salonID;
@end

@protocol NITFavoriteSalonsAPIDataManagerInputProtocol <NITFavoriteSalonsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITFavoriteSalonsLocalDataManagerInputProtocol <NITFavoriteSalonsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITFavoriteSalon *> *)getFavoriteSalons;
@end
