//
//  NITRootWireframe.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//


typedef CF_ENUM (NSUInteger, StoryboardType) {
    StoryboardTypeGuide     = 0,
    StoryboardTypeCatalog   = 1,
    StoryboardTypeBasket    = 2,
    StoryboardTypeProfile   = 3,
    StoryboardTypeSalons    = 4
};

@interface NITRootWireframe : NSObject

- (void)showRootViewController:(UIViewController *)viewController inWindow:(UIWindow *)window;

- (id)createViewControllerWithKey:(NSString *)key storyboardType:(StoryboardType)storyboardType;
- (id)createViewControllerFromXib:(NSString *)xib;

@end
