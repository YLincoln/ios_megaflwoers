//
//  NITRootWireframe.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITRootWireframe.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITRootWireframe

- (void)showRootViewController:(UIViewController *)viewController inWindow:(UIWindow *)window
{
    UINavigationController * navigationController = [self navigationControllerFromWindow:window];
    navigationController.viewControllers = @[viewController];
}

- (UINavigationController *)navigationControllerFromWindow:(UIWindow *)window
{
    UINavigationController * navigationController = (UINavigationController *)[window rootViewController];
    
    return navigationController;
}

#pragma mark - Create ViewControllers
- (id)createViewControllerWithKey:(NSString *)key storyboardType:(StoryboardType)storyboardType
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:[self storyboardNameByType:storyboardType] bundle:[NSBundle mainBundle]];
    UIViewController * viewController = [storyboard instantiateViewControllerWithIdentifier:key];
    
    if (!viewController)
    {
        DDLogError(@"❗️Error --> %@ - %@", THIS_METHOD, key);
    }
    
    return viewController;
}

- (NSString *)storyboardNameByType:(StoryboardType)type
{
    switch (type) {
        case StoryboardTypeGuide:
            return @"Guide";
        case StoryboardTypeCatalog:
            return @"Catalog";
        case StoryboardTypeBasket:
            return @"Basket";
        case StoryboardTypeProfile:
            return @"Profile";
        case StoryboardTypeSalons:
            return @"Salons";
        default:
            return @"Guide";
    }
}

- (id)createViewControllerFromXib:(NSString *)xib
{
    return [[[NSBundle mainBundle] loadNibNamed:xib owner:nil options:nil] firstObject];
}

@end
