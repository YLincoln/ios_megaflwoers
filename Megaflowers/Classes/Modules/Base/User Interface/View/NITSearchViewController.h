//
//  NITSearchViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/26/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"

@interface NITSearchViewController : NITBaseViewController

@property (nonatomic) IBOutlet TOSearchBar * searchBar;
@property (nonatomic) IBOutlet UIView * noResultsView;
@property (nonatomic) UIView * mainTitle;
@property (nonatomic) UIBarButtonItem * leftBarButtonItem;
@property (nonatomic) UIBarButtonItem * rightBarButtonItem;

- (void)initSearchBar;
- (void)showSearchBarWithAnimations:(void(^)())animations;
- (void)hideSearchBarWithAnimations:(void(^)())animations;
- (void)enableSearchBarCanelButton;

@end
