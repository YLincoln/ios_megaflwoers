//
//  NITBaseViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"

@interface NITBaseViewController ()

@end

@implementation NITBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = false;
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    [self.navigationController.interactivePopGestureRecognizer setEnabled:true];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:true];
    
    [HELPER stopLoading];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Accessors
- (void)setKeyboardActiv:(BOOL)keyboardActiv
{
    if (_keyboardActiv != keyboardActiv)
    {
        _keyboardActiv = keyboardActiv;
        if (_keyboardActiv)
        {
            [self registerForKeyboardNotifications];
            [self addKeyboardHideTouch];
        }
        else
        {
            [self unregisterForKeyboardNotifications];
        }
    }
}

#pragma mark - Action
- (IBAction)backAction:(id)sender
{
    if (self.backViewController)
    {
        [self dismissViewControllerAnimated:true completion:^{}];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (IBAction)dismissAction:(id)sender
{
    [self dismissViewControllerAnimated:true completion:^{}];
}

#pragma mark - Keyboard
- (void)registerForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(keyboardWasShown:)
                   name:UIKeyboardWillShowNotification object:nil];
    
    [center addObserver:self
               selector:@selector(keyboardWillBeHidden:)
                   name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIViewAnimationCurve animationCurve;
    NSTimeInterval animationDuration;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    for (NSLayoutConstraint *c in self.keyboardShiftConstraints)
    {
        CGFloat devider = 1.f;
        c.constant = kbSize.height/devider;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self.view layoutIfNeeded];
    
    if (self.keyboardScrollViewBottomInset)
    {
        UIEdgeInsets insets = UIEdgeInsetsZero;
        insets.bottom += kbSize.height;
        self.keyboardScrollViewBottomInset.contentInset = insets;
    }
    
    [UIView commitAnimations];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    UIViewAnimationCurve animationCurve;
    NSTimeInterval animationDuration;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    for (NSLayoutConstraint *c in self.keyboardShiftConstraints)
    {
        CGFloat value = 0.f;
        c.constant = value;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self.view layoutIfNeeded];
    
    if (self.keyboardScrollViewBottomInset)
    {
        self.keyboardScrollViewBottomInset.contentInset = UIEdgeInsetsZero;
    }
    
    [UIView commitAnimations];
}

- (void)addKeyboardHideTouch
{
    UIGestureRecognizer * tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = false;
    
    [self.view addGestureRecognizer:tapper];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:true];
}

@end
