//
//  NITGMSMapViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/22/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITGMSMapViewController.h"
#import "HSClusterRenderer.h"
#import "HSClusterMarker.h"
#import "NITClusterRenderer.h"

@interface NITGMSMapViewController ()
<
GMSMapViewDelegate
>

@end

@implementation NITGMSMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mapView = [[HSClusterMapView alloc] initWithRenderer:[NITClusterRenderer new]];
    
    UIView * mapContainer = [self.view viewWithTag:123];
    self.mapView.frame = mapContainer.bounds;
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [mapContainer addSubview:self.mapView];
    [mapContainer sendSubviewToBack:self.mapView];
    
    self.mapView.clusteringEnabled = false;
    self.mapView.myLocationEnabled = true;
    self.mapView.delegate = self;
    
    [LOCATION_MANAGER updateLocationWithHandler:nil];
}

#pragma mark - Public
- (UIView *)mapViewMarkerInfoWindow:(GMSMarker *)marker
{
    // need implement
    return [UIView new];
}

- (void)showMarkerInfoWindowForMarker:(GMSMarker *)marker
{
    if ([[self.mapView unclusteredMarkers] containsObject:marker])
    {
        self.mapView.selectedMarker = marker;
        self.currentlyTappedMarker = marker;
        
        self.isCameraMoved = false;
        self.markerTapped = true;
        self.cameraMoving = true;
        
        if (CLCOORDINATES_EQUAL(self.mapView.camera.target, marker.position))
        {
            [self mapView:self.mapView idleAtCameraPosition:self.mapView.camera];
        }
        else
        {
            /*GMSCameraUpdate * cameraUpdate = [GMSCameraUpdate setTarget:marker.position];
             [self.mapView animateWithCameraUpdate:cameraUpdate];*/
            CGPoint markerPoint = [self pointFromCoord:self.currentlyTappedMarker.position];
            [self fixCenterPositionForPoint:markerPoint];
            
            self.needShowMarker = true;
        }
    }
}

- (void)hideMarkerInfoWindow
{
    [self.displayedInfoWindow removeFromSuperview];
    self.displayedInfoWindow = nil;
}

- (void)didSelectMarker:(GMSMarker *)marker
{
    
}

#pragma mark - Private
- (void)fixCenterPositionForPoint:(CGPoint)point
{
    point.y -= IS_IPHONE_4 ? 140 : 80;
    GMSCameraUpdate * camera = [GMSCameraUpdate setTarget:[self.mapView.projection coordinateForPoint:point]];
    [self.mapView animateWithCameraUpdate:camera];
}

- (CGPoint)pointFromCoord:(CLLocationCoordinate2D)coordinate
{
    return [self.mapView.projection pointForCoordinate:coordinate];
}

- (CGPoint)roundPoint:(CGPoint)point
{
    point.x = [[NSString stringWithFormat:@"%.2f", point.x] floatValue];
    point.y = [[NSString stringWithFormat:@"%.2f", point.y] floatValue];
    
    return point;
}

#pragma mark - GMSMapViewDelegate
// Since we want to display our custom info window when a marker is tapped, use this delegate method
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    // A marker has been tapped, so set that state flag
    self.markerTapped = true;
    
    // If a marker has previously been tapped and stored in currentlyTappedMarker, then nil it out
    if (self.currentlyTappedMarker)
    {
        self.currentlyTappedMarker = nil;
    }
    
    // make this marker our currently tapped marker
    self.currentlyTappedMarker = marker;
    
    // if our custom info window is already being displayed, remove it and nil the object out
    if ([self.displayedInfoWindow isDescendantOfView:self.view])
    {
        [self hideMarkerInfoWindow];
    }
    
    CGPoint markerPoint = [self roundPoint:[self pointFromCoord:marker.position]];
    CGPoint markerPointCenter = [self roundPoint:[self pointFromCoord:marker.position]];
    markerPointCenter.y -= IS_IPHONE_4 ? 140 : 80;
    
    if(CGPointEqualToPoint(self.mapView.center, markerPointCenter))
    {
        self.needShowMarker = true;
        [self mapView:self.mapView idleAtCameraPosition:self.mapView.camera];
    }
    else if(!CGPointEqualToPoint(self.mapView.center, markerPoint))
    {
        self.needShowMarker = true;
        [self fixCenterPositionForPoint:markerPoint];
    }
    else if (CLCOORDINATES_EQUAL(self.mapView.camera.target, marker.position))
    {
        self.markerTapped = true;
        self.cameraMoving = true;
        
        [self mapView:self.mapView idleAtCameraPosition:self.mapView.camera];
    }
    else
    {
        GMSCameraUpdate * cameraUpdate = [GMSCameraUpdate setTarget:marker.position];
        [self.mapView animateWithCameraUpdate:cameraUpdate];
    }

    return true;
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    /* if we got here after we've previously been idle and displayed our custom info window,
     then remove that custom info window and nil out the object */
    self.isCameraMoved = true;
    if(self.idleAfterMovement && [self.displayedInfoWindow isDescendantOfView:self.view])
    {
        [self hideMarkerInfoWindow];
    }
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{    
    // if we got here after a marker was tapped, then set the cameraMoving state flag to YES
    if (self.markerTapped)
    {
        self.cameraMoving = true;
    }
}

// This method gets called whenever the map was moving but has now stopped
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if((self.markerTapped && self.cameraMoving) || self.needShowMarker)
    {
        self.markerTapped = false;
        self.cameraMoving = false;
        self.idleAfterMovement = true;
        [self didSelectMarker:self.currentlyTappedMarker];

        CGPoint markerPoint = [self pointFromCoord:self.currentlyTappedMarker.position];
        NSInteger top = IS_IPHONE_4 ? 170 : 260;
        if (markerPoint.y < top && !self.needShowMarker)
        {
            self.needShowMarker = true;
            [self fixCenterPositionForPoint:markerPoint];
            
            return;
        }
        
        [self markerInfo];
    }
}

- (void)markerInfo
{
    UIView * markerInfo = [self mapViewMarkerInfoWindow:self.currentlyTappedMarker];
    self.displayedInfoWindow = [UIView new];
    self.displayedInfoWindow.backgroundColor = [UIColor clearColor];
    
    CGPoint markerPoint = [self pointFromCoord:self.currentlyTappedMarker.position];
    
    self.displayedInfoWindow.frame = CGRectMake(markerPoint.x - ViewWidth(markerInfo)/2 - 3,
                                                markerPoint.y - ViewHeight(markerInfo) - 30,
                                                ViewWidth(markerInfo),
                                                ViewHeight(markerInfo));
    
    [self.displayedInfoWindow addSubview:markerInfo];
    
    [self.mapView addSubview:self.displayedInfoWindow];
    self.needShowMarker = false;
}

/* If the map is tapped on any non-marker coordinate, reset the currentlyTappedMarker and remove our
 custom info window from self.view */
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (self.currentlyTappedMarker)
    {
        self.currentlyTappedMarker = nil;
    }
    
    if ([self.displayedInfoWindow isDescendantOfView:self.view])
    {
       [self hideMarkerInfoWindow];
    }
}

@end
