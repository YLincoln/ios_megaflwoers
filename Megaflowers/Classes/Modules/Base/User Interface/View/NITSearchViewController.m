//
//  NITSearchViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/26/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITSearchViewController.h"

@interface NITSearchViewController ()

@end

@implementation NITSearchViewController

#pragma mark - Search
- (void)initSearchBar
{
    // serach bar
    self.searchBar.frame = RectSetWidth(self.searchBar.frame, ViewWidth(self.view));
    self.searchBar.searchTextField.textColor = RGB(10, 88, 43);
    
    // nav bar
    self.mainTitle = self.navigationItem.titleView;
    self.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
    self.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    
    [self.noResultsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.searchBar action:@selector(resignFirstResponder)]];
}

- (void)showSearchBarWithAnimations:(void(^)())animations
{
    [UIView animateWithDuration:0.1 animations:^{} completion:^(BOOL finished) {
        
        self.navigationItem.rightBarButtonItems = nil;
        self.navigationItem.leftBarButtonItem  = nil;
        self.navigationItem.titleView = self.searchBar;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.searchBar.hidden = false;
                             self.noResultsView.hidden = true;
                             if (animations) animations();
                         } completion:^(BOOL finished) {
                             [self.searchBar becomeFirstResponder];
                         }];
    }];
}

- (void)hideSearchBarWithAnimations:(void(^)())animations
{
    [UIView animateWithDuration:0.1f animations:^{
        self.searchBar.hidden = true;
        if (animations) animations();
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = self.mainTitle;
        self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
        self.navigationItem.leftBarButtonItem = self.leftBarButtonItem;
    }];
}

- (void)enableSearchBarCanelButton
{
    for (UIView * view in self.searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:true];
                return;
            }
        }
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    self.searchBar.showsCancelButton = true;
}

@end
