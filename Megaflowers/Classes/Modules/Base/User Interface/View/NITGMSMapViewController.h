//
//  NITGMSMapViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/22/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSearchViewController.h"
#import "HSClusterMapView.h"

@interface NITGMSMapViewController : NITSearchViewController

@property (nonatomic) IBOutlet HSClusterMapView * mapView;

// this will hold the custom info window we're displaying
@property (strong, nonatomic) UIView * displayedInfoWindow;

/* these three will be used to guess the state of the map animation since there's no
 delegate method to track when the camera update ends */
@property (nonatomic) BOOL markerTapped;
@property (nonatomic) BOOL cameraMoving;
@property (nonatomic) BOOL needShowMarker;
@property (nonatomic) BOOL idleAfterMovement;
@property (nonatomic) BOOL isCameraMoved;

/* Since I'm creating the info window based on the marker's position in the
 mapView:idleAtCameraPosition: method, I need a way for that method to know
 which marker was most recently tapped, so I'm using this to store that most
 recently tapped marker */
@property (nonatomic) GMSMarker * currentlyTappedMarker;

- (UIView *)mapViewMarkerInfoWindow:(GMSMarker *)marker;
- (void)showMarkerInfoWindowForMarker:(GMSMarker *)marker;
- (void)hideMarkerInfoWindow;
- (void)didSelectMarker:(GMSMarker *)marker;

@end
