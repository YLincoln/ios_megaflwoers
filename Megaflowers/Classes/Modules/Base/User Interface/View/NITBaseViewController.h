//
//  NITBaseViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//


@interface NITBaseViewController : UIViewController

#pragma mark - Action
- (IBAction)backAction:(id)sender;
- (IBAction)dismissAction:(id)sender;

#pragma mark - Keyboard
@property (nonatomic) IBInspectable BOOL keyboardActiv;
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray * keyboardShiftConstraints;
@property (nonatomic, weak) IBOutlet UIScrollView * keyboardScrollViewBottomInset;
@property (nonatomic) UIViewController * backViewController;

- (void)keyboardWasShown:(NSNotification *)aNotification;
- (void)keyboardWillBeHidden:(NSNotification *)aNotification;


@end
