//
//  NITRootPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITRootPresenter.h"

@implementation NITRootPresenter

#pragma mark - Life cyrcle
- (instancetype)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public
- (void)showError:(NSString *)error fromView:(UIViewController *)view
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"OK", nil) actionBlock:^{}];
    [HELPER showActionSheetFromView:view withTitle:error actionButtons:nil cancelButton:cancel];
}

#pragma mark - Private
- (void)setup
{
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRestoreNetworkConnection) name:kNetworkConnectionRestored object:nil];
}

- (void)didRestoreNetworkConnection
{
    // abstract
}

@end
