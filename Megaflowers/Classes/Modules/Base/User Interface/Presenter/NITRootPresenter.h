//
//  NITRootPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 3/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITRootPresenter : NSObject

- (void)showError:(NSString *)error fromView:(id)view;

@end
