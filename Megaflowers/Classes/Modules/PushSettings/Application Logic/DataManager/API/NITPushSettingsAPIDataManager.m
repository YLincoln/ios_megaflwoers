//
//  NITPushSettingsAPIDataManager.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsAPIDataManager.h"
#import "SWGUserApi.h"
#import "SWGPushApi.h"

@implementation NITPushSettingsAPIDataManager

- (void)getUserPushSettingsWithHandler:(void(^)(NSArray<SWGUserPush *> *output))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userPushGetWithLang:HELPER.lang site:API.site city:USER.cityID device:USER.pushId completionHandler:^(NSArray<SWGUserPush> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        handler(output);
    }];
}

- (void)getDefaultsPushSettingsWithHandler:(void(^)(NSArray<SWGInlineResponse2003 *> *output))handler
{
    [HELPER startLoading];
    [[SWGPushApi new] pushGetWithSite:API.site lang:HELPER.lang completionHandler:^(NSArray<SWGInlineResponse2003> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(output);
    }];
}

- (void)getDevicePushSettingsWithHandler:(void(^)(NSArray<SWGInlineResponse2004 *> *output))handler
{
    [HELPER startLoading];
    [[SWGPushApi new] pushDeviceGetWithSite:API.site lang:HELPER.lang device:USER.pushId completionHandler:^(NSArray<SWGInlineResponse2004> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(output);
    }];
}

- (void)saveUserPushSettings:(NSArray<SWGOptions> *)settings withHandler:(void(^)(BOOL success))handler
{
    [HELPER startLoading];
    [[SWGUserApi new] userPushDevicePutWithDevice:USER.pushId options:settings completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(error == nil);
    }];
}

- (void)savePushSettings:(NSArray<SWGOptions> *)settings withHandler:(void(^)(BOOL success))handler
{
    [HELPER startLoading];
    [[SWGPushApi new] pushDevicePutWithSite:API.site lang:HELPER.lang device:USER.pushId options:settings completionHandler:^(NSError *error) {
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(error == nil);
    }];
}

- (void)changePushSetting:(NITPushSettingModel *)setting withHandler:(void(^)(BOOL success))handler
{
    [HELPER startLoading];
    [[SWGPushApi new] pushDevicePatchWithSite:API.site lang:HELPER.lang device:USER.pushId key:setting.identifier value:@(setting.value) type:@(kiOSType) completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(error == nil);
    }];
}

- (void)changeUserPushSetting:(NITPushSettingModel *)setting withHandler:(void(^)(BOOL success))handler
{
    [HELPER startLoading];
    [HELPER startLoading];
    [[SWGUserApi new] userPushDevicePatchWithSite:API.site lang:HELPER.lang device:USER.pushId key:setting.identifier value:@(setting.value) type:@(kiOSType) completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:THIS_METHOD];
        if (handler) handler(error == nil);
    }];
}


@end
