//
//  NITPushSettingsLocalDataManager.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPushSettingsProtocols.h"

@interface NITPushSettingsLocalDataManager : NSObject <NITPushSettingsLocalDataManagerInputProtocol>

@end
