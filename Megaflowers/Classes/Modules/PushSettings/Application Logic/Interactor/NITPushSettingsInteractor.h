//
//  NITPushSettingsInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPushSettingsProtocols.h"

@interface NITPushSettingsInteractor : NSObject <NITPushSettingsInteractorInputProtocol>

@property (nonatomic, weak) id <NITPushSettingsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPushSettingsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPushSettingsLocalDataManagerInputProtocol> localDataManager;

@end
