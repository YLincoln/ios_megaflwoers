//
//  NITPushSettingsInteractor.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsInteractor.h"

@implementation NITPushSettingsInteractor

- (void)getPushSettings
{
    /*if ([USER isAutorize])
    {
        [self.APIDataManager getUserPushSettingsWithHandler:^(NSArray<SWGUserPush *> *output) {
            
            if (output.count > 0)
            {
                [self.presenter updateDataByModels:[output bk_map:^id(SWGUserPush *obj) {
                    return [[NITPushSettingModel alloc] initWithModel:obj];
                }]];
            }
            else
            {
                [self getDeviceSettingsWithHandler:^(NSArray<NITPushSettingModel *> *output) {
                    [self.presenter updateDataByModels:output];
                }];
            }
        }];
    }
    else
    {
        [self getDeviceSettingsWithHandler:^(NSArray<NITPushSettingModel *> *output) {
            [self.presenter updateDataByModels:output];
        }];
    }*/
    
    [self getDeviceSettingsWithHandler:^(NSArray<NITPushSettingModel *> *output) {
        [self.presenter updateDataByModels:output];
    }];
}

- (void)changePushSetting:(NITPushSettingModel *)model
{
    [self.APIDataManager changePushSetting:model withHandler:^(BOOL success){}];
    if ([USER isAutorize]) [self.APIDataManager changeUserPushSetting:model withHandler:^(BOOL success){}];
}

#pragma mark - Private
- (void)getDeviceSettingsWithHandler:(void(^)(NSArray <NITPushSettingModel *> *))handler
{
    [self.APIDataManager getDevicePushSettingsWithHandler:^(NSArray<SWGInlineResponse2004 *> *output) {
        
        if (output.count > 0)
        {
            NSArray * result = [output bk_map:^id(SWGInlineResponse2004 *obj) {
                return [[NITPushSettingModel alloc] initWithDeviceModel:obj];
            }];
            
            [self saveDevicePushSettings:result];
            [self saveUserPushSettings:result];
            
            if (handler) handler(result);
        }
        else
        {
            [self.APIDataManager getDefaultsPushSettingsWithHandler:^(NSArray<SWGInlineResponse2003 *> *output) {
                
                NSArray * result = [output bk_map:^id(SWGInlineResponse2003 *obj) {
                    return [[NITPushSettingModel alloc] initWithDefaultModel:obj];
                }];

                if (result.count > 0)
                {
                    [self saveDevicePushSettings:result];
                    [self saveUserPushSettings:result];
                }
                
                if (handler) handler(result);
            }];
        }
    }];
}

- (void)saveDevicePushSettings:(NSArray <NITPushSettingModel *> *)settings
{
    NSArray * options = [settings bk_map:^id(NITPushSettingModel *obj) {
        SWGOptions * op = [SWGOptions new];
        op.key = obj.identifier;
        op.value = @(obj.value);
        op.type = @(kiOSType);
        return op;
    }];
    
    [self.APIDataManager savePushSettings:(id)options withHandler:^(BOOL success){}];
}

- (void)saveUserPushSettings:(NSArray <NITPushSettingModel *> *)settings
{
    if ([USER isAutorize])
    {
        NSArray * options = [settings bk_map:^id(NITPushSettingModel *obj) {
            SWGOptions * op = [SWGOptions new];
            op.key = obj.identifier;
            op.value = @(obj.value);
            op.type = @(kiOSType);
            return op;
        }];
        
        [self.APIDataManager saveUserPushSettings:(id)options withHandler:^(BOOL success){}];
    }
}

@end
