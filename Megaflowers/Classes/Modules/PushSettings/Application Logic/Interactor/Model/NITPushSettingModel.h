//
//  NITPushSettingModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/11/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class SWGUserPush, SWGInlineResponse2004, SWGInlineResponse2003;

@interface NITPushSettingModel : NSObject

@property (nonatomic) NSString * identifier;
@property (nonatomic) NSString * name;
@property (nonatomic) BOOL value;

- (instancetype)initWithModel:(SWGUserPush *)model;
- (instancetype)initWithDeviceModel:(SWGInlineResponse2004 *)model;
- (instancetype)initWithDefaultModel:(SWGInlineResponse2003 *)model;

@end
