//
//  NITPushSettingsModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsModel.h"

static NSString * const kProductTracking            = @"kProductTracking";
static NSString * const kFriendsBirthdays           = @"kFriendsBirthdays";
static NSString * const kInternationalHolidays      = @"kInternationalHolidays";
static NSString * const kPromotions                 = @"kPromotions";
static NSString * const kPersonalizedSuggestions    = @"kPersonalizedSuggestions";
static NSString * const kHolidaysMegaflowers        = @"kHolidaysMegaflowers";

@implementation NITPushSettingsModel

- (NSInteger)settingsCount
{
    return 6;
}

- (NSString *)getNameByIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            return NSLocalizedString(@"Отслеживание товара", nil);
        case 1:
            return NSLocalizedString(@"Дни рождения друзей", nil);
        case 2:
            return NSLocalizedString(@"Международные праздники", nil);
        case 3:
            return NSLocalizedString(@"Акции и скидки", nil);
        case 4:
            return NSLocalizedString(@"Персональные предложения", nil);
        case 5:
            return NSLocalizedString(@"Праздники Megaflowers", nil);
        default:
            return @"";
    }
}

- (BOOL)getValueByIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            return self.productTracking;
        case 1:
            return self.friendsBirthdays;
        case 2:
            return self.internationalHolidays;
        case 3:
            return self.promotions;
        case 4:
            return self.personalizedSuggestions;
        case 5:
            return self.holidaysMegaflowers;
        default:
            return false;
    }
}

- (void)setValue:(BOOL)value byIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            self.productTracking = value;
            break;
        case 1:
            self.friendsBirthdays = value;
            break;
        case 2:
            self.internationalHolidays = value;
            break;
        case 3:
            self.promotions = value;
            break;
        case 4:
            self.personalizedSuggestions = value;
            break;
        case 5:
            self.holidaysMegaflowers = value;
            break;
        default:
            break;
    }
    
    if (!value)
    {
       [TRACKER trackEvent:TrackerEventPushSettingOff parameters:@{@"name":[self getNameByIndex:index]}];
    }
}

- (BOOL)productTracking
{
    return [self Bool:kProductTracking];
}

- (void)setProductTracking:(BOOL)productTracking
{
    [self setBool:kProductTracking value:productTracking];
}

- (BOOL)friendsBirthdays
{
    return [self Bool:kFriendsBirthdays];
}

- (void)setFriendsBirthdays:(BOOL)friendsBirthdays
{
    [self setBool:kFriendsBirthdays value:friendsBirthdays];
}

- (BOOL)internationalHolidays
{
    return [self Bool:kInternationalHolidays];
}

- (void)setInternationalHolidays:(BOOL)internationalHolidays
{
    [self setBool:kInternationalHolidays value:internationalHolidays];
}

- (BOOL)promotions
{
    return [self Bool:kPromotions];
}

- (void)setPromotions:(BOOL)promotions
{
    [self setBool:kPromotions value:promotions];
}

- (BOOL)personalizedSuggestions
{
    return [self Bool:kPersonalizedSuggestions];
}

- (void)setPersonalizedSuggestions:(BOOL)personalizedSuggestions
{
    [self setBool:kPersonalizedSuggestions value:personalizedSuggestions];
}

- (BOOL)holidaysMegaflowers
{
    return [self Bool:kHolidaysMegaflowers];
}

- (void)setHolidaysMegaflowers:(BOOL)holidaysMegaflowers
{
    [self setBool:kHolidaysMegaflowers value:holidaysMegaflowers];
}

@end
