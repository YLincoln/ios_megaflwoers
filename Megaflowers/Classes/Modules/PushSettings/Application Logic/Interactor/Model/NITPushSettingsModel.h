//
//  NITPushSettingsModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITStorageManager.h"

@interface NITPushSettingsModel : NITStorageManager

@property (nonatomic) BOOL productTracking;
@property (nonatomic) BOOL friendsBirthdays;
@property (nonatomic) BOOL internationalHolidays;
@property (nonatomic) BOOL promotions;
@property (nonatomic) BOOL personalizedSuggestions;
@property (nonatomic) BOOL holidaysMegaflowers;

- (NSInteger)settingsCount;
- (NSString *)getNameByIndex:(NSInteger)index;
- (BOOL)getValueByIndex:(NSInteger)index;
- (void)setValue:(BOOL)value byIndex:(NSInteger)index;

@end
