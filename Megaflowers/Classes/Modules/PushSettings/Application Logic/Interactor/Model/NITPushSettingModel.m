//
//  NITPushSettingModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 6/11/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingModel.h"
#import "SWGUserPush.h"
#import "SWGInlineResponse2004.h"
#import "SWGInlineResponse2003.h"

@implementation NITPushSettingModel

- (instancetype)initWithModel:(SWGUserPush *)model
{
    if (self = [super init])
    {
        self.identifier = model.key;
        self.name = model.name;
        self.value = model.value.boolValue;
    }
    
    return self;
}

- (instancetype)initWithDeviceModel:(SWGInlineResponse2004 *)model
{
    if (self = [super init])
    {
        self.identifier = model.key;
        self.name = model.name  ;
        self.value = model.value.boolValue;
    }
    
    return self;
}

- (instancetype)initWithDefaultModel:(SWGInlineResponse2003 *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.name = model.name  ;
        self.value = model._default.boolValue;
    }
    
    return self;
}


@end
