//
//  NITPushSettingsProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingModel.h"
#import "NITPushSettingsModel.h"
#import "SWGUserPush.h"
#import "SWGInlineResponse2003.h"
#import "SWGInlineResponse2004.h"
#import "SWGOptions.h"

@protocol NITPushSettingsInteractorOutputProtocol;
@protocol NITPushSettingsInteractorInputProtocol;
@protocol NITPushSettingsViewProtocol;
@protocol NITPushSettingsPresenterProtocol;
@protocol NITPushSettingsLocalDataManagerInputProtocol;
@protocol NITPushSettingsAPIDataManagerInputProtocol;

@class NITPushSettingsWireFrame;

static NSInteger const kiOSType = 0;

@protocol NITPushSettingsViewProtocol
@required
@property (nonatomic, strong) id <NITPushSettingsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITPushSettingModel *> *)models;
@end

@protocol NITPushSettingsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITPushSettingsModuleFrom:(id)fromView;
@end

@protocol NITPushSettingsPresenterProtocol
@required
@property (nonatomic, weak) id <NITPushSettingsViewProtocol> view;
@property (nonatomic, strong) id <NITPushSettingsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPushSettingsWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)changePushSetting:(NITPushSettingModel *)model;
@end

@protocol NITPushSettingsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDataByModels:(NSArray <NITPushSettingModel *> *)models;
@end

@protocol NITPushSettingsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPushSettingsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPushSettingsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPushSettingsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getPushSettings;
- (void)changePushSetting:(NITPushSettingModel *)model;
@end


@protocol NITPushSettingsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITPushSettingsAPIDataManagerInputProtocol <NITPushSettingsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getUserPushSettingsWithHandler:(void(^)(NSArray<SWGUserPush *> *output))handler;
- (void)getDefaultsPushSettingsWithHandler:(void(^)(NSArray<SWGInlineResponse2003 *> *output))handler;
- (void)getDevicePushSettingsWithHandler:(void(^)(NSArray<SWGInlineResponse2004 *> *output))handler;
- (void)saveUserPushSettings:(NSArray<SWGOptions> *)settings withHandler:(void(^)(BOOL success))handler;
- (void)savePushSettings:(NSArray<SWGOptions> *)settings withHandler:(void(^)(BOOL success))handler;
- (void)changePushSetting:(NITPushSettingModel *)setting withHandler:(void(^)(BOOL success))handler;
- (void)changeUserPushSetting:(NITPushSettingModel *)setting withHandler:(void(^)(BOOL success))handler;
@end

@protocol NITPushSettingsLocalDataManagerInputProtocol <NITPushSettingsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
