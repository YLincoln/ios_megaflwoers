//
//  NITPushSettingsWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsProtocols.h"
#import "NITPushSettingsViewController.h"
#import "NITPushSettingsLocalDataManager.h"
#import "NITPushSettingsAPIDataManager.h"
#import "NITPushSettingsInteractor.h"
#import "NITPushSettingsPresenter.h"
#import "NITPushSettingsWireframe.h"
#import "NITRootWireframe.h"

@interface NITPushSettingsWireFrame : NITRootWireframe <NITPushSettingsWireFrameProtocol>

@end
