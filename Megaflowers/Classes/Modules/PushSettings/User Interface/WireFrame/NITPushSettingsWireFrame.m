//
//  NITPushSettingsWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsWireFrame.h"

@implementation NITPushSettingsWireFrame

+ (id)createNITPushSettingsModule
{
    // Generating module components
    id <NITPushSettingsPresenterProtocol, NITPushSettingsInteractorOutputProtocol> presenter = [NITPushSettingsPresenter new];
    id <NITPushSettingsInteractorInputProtocol> interactor = [NITPushSettingsInteractor new];
    id <NITPushSettingsAPIDataManagerInputProtocol> APIDataManager = [NITPushSettingsAPIDataManager new];
    id <NITPushSettingsLocalDataManagerInputProtocol> localDataManager = [NITPushSettingsLocalDataManager new];
    id <NITPushSettingsWireFrameProtocol> wireFrame= [NITPushSettingsWireFrame new];
    id <NITPushSettingsViewProtocol> view  = [(NITPushSettingsWireFrame *)wireFrame createViewControllerWithKey:_s(NITPushSettingsViewController) storyboardType:StoryboardTypeProfile];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITPushSettingsModuleFrom:(UIViewController *)fromViewController
{
    NITPushSettingsViewController * vc = [NITPushSettingsWireFrame createNITPushSettingsModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

@end
