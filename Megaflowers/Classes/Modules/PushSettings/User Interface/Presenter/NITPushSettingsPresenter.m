//
//  NITPushSettingsPresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsPresenter.h"
#import "NITPushSettingsWireframe.h"

@implementation NITPushSettingsPresenter

#pragma mark - Base methohds
- (void)didRestoreNetworkConnection
{
    [self initData];
}

#pragma mark - NITPushSettingsPresenterProtocol
- (void)initData
{
    [self.interactor getPushSettings];
}

- (void)changePushSetting:(NITPushSettingModel *)model
{
    [self.interactor changePushSetting:model];
}

#pragma mark - NITPushSettingsInteractorOutputProtocol
- (void)updateDataByModels:(NSArray<NITPushSettingModel *> *)models
{
    [self.view reloadDataByModels:models];
}

@end
