//
//  NITPushSettingsPresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPushSettingsProtocols.h"

@class NITPushSettingsWireFrame;

@interface NITPushSettingsPresenter : NITRootPresenter <NITPushSettingsPresenterProtocol, NITPushSettingsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPushSettingsViewProtocol> view;
@property (nonatomic, strong) id <NITPushSettingsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPushSettingsWireFrameProtocol> wireFrame;

@end
