//
//  NITPushSettingsViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsViewController.h"
#import "NITPushSettingsCell.h"

@interface NITPushSettingsViewController ()
<
UITableViewDataSource,
UITableViewDelegate,
NITPushSettingsCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITPushSettingModel *> * items;

@end

@implementation NITPushSettingsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

#pragma mark - NITPushSettingsPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITPushSettingModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Push-уведомления", nil);
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNibArray:@[_s(NITPushSettingsCell)]];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPushSettingsCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITPushSettingsCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - NITPushSettingsCellDelegate
- (void)didChangePushSetting:(NITPushSettingModel *)model
{
    [self.presenter changePushSetting:model];
}

@end
