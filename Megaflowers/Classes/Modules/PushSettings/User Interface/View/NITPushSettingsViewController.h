//
//  NITPushSettingsViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITPushSettingsProtocols.h"

@interface NITPushSettingsViewController : NITBaseViewController <NITPushSettingsViewProtocol>

@property (nonatomic, strong) id <NITPushSettingsPresenterProtocol> presenter;

@end
