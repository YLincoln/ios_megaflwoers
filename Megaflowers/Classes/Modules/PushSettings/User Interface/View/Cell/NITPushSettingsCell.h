//
//  NITPushSettingsCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

@class NITPushSettingModel;

@protocol NITPushSettingsCellDelegate <NSObject>

- (void)didChangePushSetting:(NITPushSettingModel *)model;

@end

@interface NITPushSettingsCell : UITableViewCell

@property (nonatomic) NITPushSettingModel * model;
@property (nonatomic, weak) id <NITPushSettingsCellDelegate> delegate;

@end
