//
//  NITPushSettingsCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/13/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPushSettingsCell.h"
#import "NITPushSettingModel.h"

@interface NITPushSettingsCell ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UISwitch * switcher;

@end

@implementation NITPushSettingsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setModel:(NITPushSettingModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)changeSwitchAction:(id)sender
{
    self.model.value = self.switcher.on;
    
    if ([self.delegate respondsToSelector:@selector(didChangePushSetting:)])
    {
        [self.delegate didChangePushSetting:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.name;
    self.switcher.on = self.model.value;
}

@end
