//
//  NITPaymentOrderTypePresenter.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderTypePresenter.h"
#import "NITPaymentOrderTypeWireframe.h"

@implementation NITPaymentOrderTypePresenter

- (void)initData
{
    NSArray * items = @[
                        [[NITPaymentOrderTypeModel alloc] initWithType:PaymentOrderSubTypeBankCard],
                        [[NITPaymentOrderTypeModel alloc] initWithType:PaymentOrderSubTypeElectronicMoney],
                        [[NITPaymentOrderTypeModel alloc] initWithType:PaymentOrderSubTypeBankTransfers],
                        [[NITPaymentOrderTypeModel alloc] initWithType:PaymentOrderSubTypeTerminalsTicketing],
                        [[NITPaymentOrderTypeModel alloc] initWithType:PaymentOrderSubTypeAllOptions]
                        ];
    
    [self.view reloadDataByModels:items];
}

- (void)selectPaymentSupTypeModel:(NITPaymentOrderTypeModel *)model
{
    [self.wireFrame showPaymentSupTypeDetailByModel:model from:self.view];
    [TRACKER trackEvent:TrackerEventSelectPaymentType parameters:@{@"name":model.title}];
}

@end
