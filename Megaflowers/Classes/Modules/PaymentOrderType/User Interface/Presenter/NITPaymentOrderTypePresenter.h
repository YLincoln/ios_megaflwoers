//
//  NITPaymentOrderTypePresenter.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPaymentOrderTypeProtocols.h"

@class NITPaymentOrderTypeWireFrame;

@interface NITPaymentOrderTypePresenter : NITRootPresenter <NITPaymentOrderTypePresenterProtocol, NITPaymentOrderTypeInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPaymentOrderTypeViewProtocol> view;
@property (nonatomic, strong) id <NITPaymentOrderTypeInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPaymentOrderTypeWireFrameProtocol> wireFrame;
@property (nonatomic) NITPaymentOrderModel * paymentModel;

@end
