//
//  NITPaymentOrderTypeViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderTypeViewController.h"
#import "NITPaymentOrderTypeCell.h"

@interface NITPaymentOrderTypeViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (weak, nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITPaymentOrderTypeModel *> * items;

@end

@implementation NITPaymentOrderTypeViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - NITPaymentOrderPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITPaymentOrderTypeModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Оплата заказа", nil);
    
    // table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITPaymentOrderTypeCell) bundle:nil] forCellReuseIdentifier:_s(NITPaymentOrderTypeCell)];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [NITPaymentOrderTypeModel headerView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPaymentOrderTypeCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITPaymentOrderTypeCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.presenter selectPaymentSupTypeModel:self.items[indexPath.row]];
}

@end
