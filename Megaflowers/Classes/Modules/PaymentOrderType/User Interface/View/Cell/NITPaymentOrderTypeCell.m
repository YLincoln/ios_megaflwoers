//
//  NITPaymentOrderTypeCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderTypeCell.h"
#import "NITPaymentOrderTypeModel.h"

@interface NITPaymentOrderTypeCell ()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITPaymentOrderTypeCell

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)setModel:(NITPaymentOrderTypeModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_right"]];
}

@end
