//
//  NITPaymentOrderTypeViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITPaymentOrderTypeProtocols.h"

@interface NITPaymentOrderTypeViewController : NITBaseViewController <NITPaymentOrderTypeViewProtocol>

@property (nonatomic, strong) id <NITPaymentOrderTypePresenterProtocol> presenter;

@end
