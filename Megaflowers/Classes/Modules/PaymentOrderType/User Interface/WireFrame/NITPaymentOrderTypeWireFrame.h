//
//  NITPaymentOrderTypeWireFrame.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderTypeProtocols.h"
#import "NITPaymentOrderTypeViewController.h"
#import "NITPaymentOrderTypeLocalDataManager.h"
#import "NITPaymentOrderTypeAPIDataManager.h"
#import "NITPaymentOrderTypeInteractor.h"
#import "NITPaymentOrderTypePresenter.h"
#import "NITPaymentOrderTypeWireframe.h"
#import "NITRootWireframe.h"

@interface NITPaymentOrderTypeWireFrame : NITRootWireframe <NITPaymentOrderTypeWireFrameProtocol>

@end
