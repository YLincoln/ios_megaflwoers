//
//  NITPaymentOrderTypeWireFrame.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderTypeWireFrame.h"
#import "NITPaymentFinalWireFrame.h"

@implementation NITPaymentOrderTypeWireFrame

+ (id)createNITPaymentOrderTypeModule
{
    // Generating module components
    id <NITPaymentOrderTypePresenterProtocol, NITPaymentOrderTypeInteractorOutputProtocol> presenter = [NITPaymentOrderTypePresenter new];
    id <NITPaymentOrderTypeInteractorInputProtocol> interactor = [NITPaymentOrderTypeInteractor new];
    id <NITPaymentOrderTypeAPIDataManagerInputProtocol> APIDataManager = [NITPaymentOrderTypeAPIDataManager new];
    id <NITPaymentOrderTypeLocalDataManagerInputProtocol> localDataManager = [NITPaymentOrderTypeLocalDataManager new];
    id <NITPaymentOrderTypeWireFrameProtocol> wireFrame= [NITPaymentOrderTypeWireFrame new];
    id <NITPaymentOrderTypeViewProtocol> view = [(NITPaymentOrderTypeWireFrame *)wireFrame createViewControllerWithKey:_s(NITPaymentOrderTypeViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITPaymentOrderTypeModuleFrom:(UIViewController *)fromViewController withPaymentModel:(NITPaymentOrderModel *)model
{
    NITPaymentOrderTypeViewController * vc = [NITPaymentOrderTypeWireFrame createNITPaymentOrderTypeModule];
    vc.presenter.paymentModel = model;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showPaymentSupTypeDetailByModel:(NITPaymentOrderTypeModel *)model from:(UIViewController *)fromViewController
{
    #warning test
    [NITPaymentFinalWireFrame presentNITPaymentFinalModuleFrom:fromViewController withPaymentState:false];
}

@end
