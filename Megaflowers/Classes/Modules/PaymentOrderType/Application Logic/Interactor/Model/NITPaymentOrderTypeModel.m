//
//  NITPaymentOrderTypeModel.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderTypeModel.h"

@implementation NITPaymentOrderTypeModel

- (instancetype)initWithType:(PaymentOrderSubType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

- (void)setType:(PaymentOrderSubType)type
{
    _type = type;
    [self updateData];
}

+ (UIView *)headerView
{
    UIView * sectionView = [[[NSBundle mainBundle] loadNibNamed:@"NITSearchEventHeader" owner:self options:nil] firstObject];
    UILabel * title = [sectionView viewWithTag:101];
    title.text = NSLocalizedString(@"Выберите способ оплаты", nil);
    
    return sectionView;
}

#pragma mark - Private
- (void)updateData
{
    switch (self.type) {
        case PaymentOrderSubTypeBankCard:
            self.title = NSLocalizedString(@"Банковская карта", nil);
            break;
            
        case PaymentOrderSubTypeElectronicMoney:
            self.title = NSLocalizedString(@"Электронные деньги", nil);
            break;
            
        case PaymentOrderSubTypeBankTransfers:
            self.title = NSLocalizedString(@"Банковские переводы", nil);
            break;
            
        case PaymentOrderSubTypeTerminalsTicketing:
            self.title = NSLocalizedString(@"Терминалы и кассы", nil);
            break;
            
        case PaymentOrderSubTypeAllOptions:
            self.title = NSLocalizedString(@"Все варианты", nil);
            break;
    }
}

@end
