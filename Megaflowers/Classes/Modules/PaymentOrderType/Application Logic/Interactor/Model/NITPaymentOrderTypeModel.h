//
//  NITPaymentOrderTypeModel.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, PaymentOrderSubType) {
    PaymentOrderSubTypeBankCard             = 0,
    PaymentOrderSubTypeElectronicMoney      = 1,
    PaymentOrderSubTypeBankTransfers        = 2,
    PaymentOrderSubTypeTerminalsTicketing   = 3,
    PaymentOrderSubTypeAllOptions           = 4
};
@interface NITPaymentOrderTypeModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) PaymentOrderSubType type;

- (instancetype)initWithType:(PaymentOrderSubType)type;
+ (UIView *)headerView;

@end
