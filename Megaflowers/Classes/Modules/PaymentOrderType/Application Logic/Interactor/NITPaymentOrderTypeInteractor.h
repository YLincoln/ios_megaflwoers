//
//  NITPaymentOrderTypeInteractor.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPaymentOrderTypeProtocols.h"

@interface NITPaymentOrderTypeInteractor : NSObject <NITPaymentOrderTypeInteractorInputProtocol>

@property (nonatomic, weak) id <NITPaymentOrderTypeInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPaymentOrderTypeAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPaymentOrderTypeLocalDataManagerInputProtocol> localDataManager;

@end
