//
//  NITPaymentOrderTypeProtocols.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 12/19/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymentOrderModel.h"
#import "NITPaymentOrderTypeModel.h"

@protocol NITPaymentOrderTypeInteractorOutputProtocol;
@protocol NITPaymentOrderTypeInteractorInputProtocol;
@protocol NITPaymentOrderTypeViewProtocol;
@protocol NITPaymentOrderTypePresenterProtocol;
@protocol NITPaymentOrderTypeLocalDataManagerInputProtocol;
@protocol NITPaymentOrderTypeAPIDataManagerInputProtocol;

@class NITPaymentOrderTypeWireFrame;

@protocol NITPaymentOrderTypeViewProtocol
@required
@property (nonatomic, strong) id <NITPaymentOrderTypePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITPaymentOrderTypeModel *> *)models;
@end

@protocol NITPaymentOrderTypeWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITPaymentOrderTypeModuleFrom:(id)fromView withPaymentModel:(NITPaymentOrderModel *)model;
- (void)showPaymentSupTypeDetailByModel:(NITPaymentOrderTypeModel *)model from:(id)fromView;
@end

@protocol NITPaymentOrderTypePresenterProtocol
@required
@property (nonatomic, weak) id <NITPaymentOrderTypeViewProtocol> view;
@property (nonatomic, strong) id <NITPaymentOrderTypeInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPaymentOrderTypeWireFrameProtocol> wireFrame;
@property (nonatomic) NITPaymentOrderModel * paymentModel;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)selectPaymentSupTypeModel:(NITPaymentOrderTypeModel *)model;
@end

@protocol NITPaymentOrderTypeInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITPaymentOrderTypeInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPaymentOrderTypeInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPaymentOrderTypeAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPaymentOrderTypeLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITPaymentOrderTypeDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITPaymentOrderTypeAPIDataManagerInputProtocol <NITPaymentOrderTypeDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITPaymentOrderTypeLocalDataManagerInputProtocol <NITPaymentOrderTypeDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
