//
//  AppDependencies.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "AppDependencies.h"
#import "NITAppTheme.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "NITGuideWireFrame.h"
#import "IQKeyboardManager.h"
#import <OneSignal/OneSignal.h>

#import "NITTabBarWireFrame.h"
#import "NITNotificationView.h"
#import "NITOrder.h"

static NSString * ONESIGNAL_APPID = @"c36a3144-acf7-4f88-9c80-daca20b00295"; //@"373e924a-fd24-47ff-99f6-825a063ce222"; //

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface AppDependencies () <CrashlyticsDelegate>

@property (nonatomic, strong) NITGuideWireFrame * guideWireframe;
@property (nonatomic) NSMutableDictionary * shortcutItemsActions;


@end

@implementation AppDependencies

#pragma mark - LifeCycle
- (instancetype)init
{
    if ((self = [super init]))
    {
        [self configureDependencies];
    }
    
    return self;
}

- (void)configureDependencies
{
    // crashes
    [Fabric with:@[[Crashlytics class]]];
    if ([USER isAutorize])
    {
        [CrashlyticsKit setUserIdentifier:USER.identifier];
        [CrashlyticsKit setUserEmail:USER.email];
        [CrashlyticsKit setUserName:USER.name];
    }
    
    // logs
    [self loadLogging];
    
    // init DB
    [NITRealmManager sharedInstance];
    
    // init managers
    [UserData sharedInstance];
    [NITHelperManager sharedInstance];
    
    // init api
    [API loadSettingsIfNeed];
    [API updateCurrentSession];
    
    // register Google maps id
    [GMSServices provideAPIKey:kGoogleAPIKey];
    
    [TRACKER initGoogleAnalytics];
    
    // keyboard
    [IQKeyboardManager sharedManager].enable = true;
    [IQKeyboardManager sharedManager].toolbarDoneBarButtonItemText = NSLocalizedString(@"Готово", nil);
    
    // ui
    [NITAppTheme applyTheme];
    self.guideWireframe = [[NITGuideWireFrame alloc] init];
}

- (void)deallocDependencies
{
   //
}

- (void)installRootViewControllerIntoWindow:(UIWindow *)window
{
    [self.guideWireframe presentGuideInterfaceFromWindow:window];
}

#pragma mark - Video player
- (BOOL)vcIsVideoPlayer
{
    UIViewController * vc = [self topMostController];
    
    NSString * className = vc ? NSStringFromClass([vc class]) : nil;
    
    return (
            [className isEqualToString:@"MPInlineVideoFullscreenViewController"] ||
            [className isEqualToString:@"MPMoviePlayerViewController"] ||
            [className isEqualToString:@"AVFullScreenViewController"]
            );
}

- (UIViewController *)topMostController
{
    UIViewController * topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    
    return topController;
}
    
#pragma mark - Crashlytics
- (void) loadCrashlytics
{
    CrashlyticsKit.delegate = self;
    [Fabric with:@[[Crashlytics class]]];
}
    
#pragma mark - CrashlyticsDelegate
- (BOOL)crashlyticsCanUseBackgroundSessions:(Crashlytics *)crashlytics
{
    return true;
}
    
#pragma mark - Logs
- (void)loadLogging
{
    for (DDAbstractLogger * logger in @[[DDTTYLogger sharedInstance]])
    {
        [DDLog addLogger:logger];
    }
}

#pragma mark - Notification
+ (void)registerPushNotificationsWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:ONESIGNAL_APPID
          handleNotificationReceived:^(OSNotification *notification) {
        
        OSNotificationPayload * payload = notification.payload;
        
        NSString * messageTitle = @"";
        NSString * fullMessage = [payload.body copy];
        
        if (payload.title.length > 0)
        {
            messageTitle = [NSString stringWithFormat:@"%@. ", payload.title];
        }

        [HELPER logString:[NSString stringWithFormat:@"New notification with additionalData: %@", payload.additionalData]];
        
        [NITNotificationView showWithText:[messageTitle stringByAppendingString:fullMessage] tapHandler:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kNewPushNotification object:payload.additionalData];
        }];
    }
            handleNotificationAction:^(OSNotificationOpenedResult *result) {
                
                OSNotificationPayload * payload = result.notification.payload;
                [HELPER logString:[NSString stringWithFormat:@"New notification with additionalData: %@", payload.additionalData]];
                [AppDependencies handleNotificationData:payload.additionalData];
            }
                            settings:@{kOSSettingsKeyInAppAlerts:@false}];
    
    [HELPER logString:[NSString stringWithFormat:@"OneSignal app_id: %@", [OneSignal app_id]]];
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        
        USER.pushId = userId;
        
        [HELPER logString:[NSString stringWithFormat:@"OneSignal userId: %@", userId]];
        [HELPER logString:[NSString stringWithFormat:@"OneSignal pushToken: %@", pushToken]];
        [API sendPushTags];
    }];
}

+ (void)handleNotificationData:(NSDictionary *)data
{
    if (HELPER.interfaceLoaded)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNewPushNotification object:data];
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [AppDependencies handleNotificationData:data];
        });
    }
}

- (void)initPushNotificationsWithOptions:(NSDictionary *)launchOptions
{
    if (![HELPER isFirstLanch])
    {
        [AppDependencies registerPushNotificationsWithOptions:launchOptions];
    }
}

#pragma mark - Handle open from url
- (void)handleOpenFromUrl:(NSURL *)url
{
    NSString * query = [url query];
    NSArray * queryPairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary * pairs = [NSMutableDictionary dictionary];
    
    for (NSString * queryPair in queryPairs)
    {
        NSArray * bits = [queryPair componentsSeparatedByString:@"="];
        
        if ([bits count] != 2)
        {
            continue;
        }
        
        NSString * key = [[bits objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString * value = [[bits objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [pairs setObject:value forKey:key];
    }
    
    DDLogInfo(@"%@ %@", THIS_METHOD, pairs);
}

#pragma mark - ShortcutItems
- (void)createShortcutItem
{
    if ([HELPER forceTouchAvailable])
    {
        if([Shortcut isCreatedShortcut]) [Shortcut deleteShortcut];
        [Shortcut createShortcut];
    }
}

- (BOOL)shortcutMethod:(NSDictionary *)launchOptions
{
    if([Shortcut isCreatedShortcut]) [Shortcut deleteShortcut];

    BOOL shouldPerformAdditionalDelegateHandling = YES;
    if([[UIApplicationShortcutItem class] respondsToSelector:@selector(new)])
    {
        UIApplicationShortcutItem *shortcutItem = [launchOptions objectForKeyedSubscript:UIApplicationLaunchOptionsShortcutItemKey];
        if(shortcutItem)
        {
            DDLogInfo(@"3D Touch is available");
            [Shortcut handleShortCutItem:shortcutItem];
            shouldPerformAdditionalDelegateHandling = NO;
        }
        else
        {
            [self launchWithoutQuickAction];
        }
    }
    else
    {
        [self launchWithoutQuickAction];
    }
    
    return shouldPerformAdditionalDelegateHandling;
}

-(void)launchWithoutQuickAction
{
    DDLogInfo(@"3D Touch is not available");
}

- (void)fromBackgroundShortcutItem:(UIWindow *)window
{
    [NITTabBarWireFrame presentNITTabBarModuleFromWindow:WINDOW];
}

@end
