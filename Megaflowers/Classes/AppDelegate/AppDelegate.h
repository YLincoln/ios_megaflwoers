//
//  AppDelegate.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "AppDependencies.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;
@property (nonatomic, strong) AppDependencies * appDependencies;
@property (nonatomic) BOOL videoIsInFullscreen;

extern NSString *asShortCutType;

@end

