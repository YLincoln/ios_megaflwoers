//
//  AppDependencies.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//


@interface AppDependencies : NSObject

- (void)installRootViewControllerIntoWindow:(UIWindow *)window;
- (void)deallocDependencies;

- (void)createShortcutItem;
- (BOOL)shortcutMethod:(NSDictionary *)launchOptions;
- (void)fromBackgroundShortcutItem:(UIWindow *)window;

- (BOOL)vcIsVideoPlayer;
- (void)handleOpenFromUrl:(NSURL *)url;

+ (void)registerPushNotificationsWithOptions:(NSDictionary *)launchOptions;
- (void)initPushNotificationsWithOptions:(NSDictionary *)launchOptions;

@end
