//
//  NITImageView360.m
//  NITImage360
//
//  Created by Алексей Гуляев on 20.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import "NITImageView360.h"

@interface UIImage(Frame)

- (UIImage*)getSubImageWithRect:(CGRect)rect;

@end

@implementation UIImage(Frame)

- (UIImage*)getSubImageWithRect:(CGRect)rect
{
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, self.size.width, self.size.height);
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    [self drawInRect:drawRect];
    UIImage* subImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return subImage;
}

@end



@interface NITImageView360() <UIGestureRecognizerDelegate>
{
    __weak UIPanGestureRecognizer *_gesture;
    NSInteger beginFrame;
}

@end

@implementation NITImageView360


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self gesturePrepare];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self gesturePrepare];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self)
    {
        [self gesturePrepare];
    }
    
    return self;
}

- (instancetype)initWithImage:(UIImage *)image
{
    self = [super initWithImage:image];
    
    if (self)
    {
        [self gesturePrepare];
    }
    
    return self;
}

- (instancetype)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage
{
    self = [super initWithImage:image highlightedImage:highlightedImage];
    
    if (self)
    {
        [self gesturePrepare];
    }
    
    return self;
}

- (void)gesturePrepare
{
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(gestureAction:)];
    
    [self addGestureRecognizer:gesture];
    gesture.delegate = self;
    self.userInteractionEnabled = YES;
    _gesture = gesture;
}

#pragma mark - Custom Accessors

- (void)setImageFrame:(NSUInteger)value
{
    if ([self.frames count] > value)
    {
        _imageFrame = value;
        [super setImage:self.frames[value]];
    }
}

- (void)setFrameImage:(UIImage *)frameImage
{
    _frameImage = frameImage;
    [self framingImage:frameImage count:self.imageFrameCount withHandler:^(NSArray<UIImage *> *frames) {
        _frames = frames;
        self.imageFrame = 0;
    }];
}

#pragma mark - Public



#pragma mark - Gesture

- (void)gestureAction:(UIPanGestureRecognizer*)gesture
{
    UIGestureRecognizerState state = [gesture state];
    
    if (state == UIGestureRecognizerStateBegan)
    {
        beginFrame = self.imageFrame;
    }
    else
    {
        CGFloat position = [gesture translationInView:self].x;
        CGFloat width = CGRectGetWidth(self.bounds)/self.imageFrameCount;
        
        NSInteger frameCount = self.imageFrameCount;
        NSInteger shiftFrame = -roundf(position / width);
        NSInteger nextFrame = beginFrame + shiftFrame;
        
        if (nextFrame >= frameCount)
        {
            nextFrame -= frameCount;
        }
        else if (nextFrame < 0)
        {
            nextFrame += frameCount;
        }
        
        self.imageFrame = nextFrame;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer class] == [UIPanGestureRecognizer class])
    {
        UIPanGestureRecognizer *g = (UIPanGestureRecognizer *)gestureRecognizer;
        CGPoint point = [g velocityInView:self];
        
        if (fabs(point.x) > fabs(point.y))
        {
            return YES;
        }
    }
    
    return NO;
}


#pragma mark - Private
- (void)framingImage:(UIImage*)image count:(NSUInteger)count withHandler:(void(^)(NSArray<UIImage*>*))handler
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        CGSize frameSize = image.size;
        frameSize.width /= count;
        
        NSMutableArray *frames = [NSMutableArray new];
        
        for (NSUInteger i = 0; i < count; i++)
        {
            CGRect rect = CGRectMake(frameSize.width * i, 0.f, frameSize.width, frameSize.height);
            UIImage *img = [image getSubImageWithRect:rect];
            
            if (img)
            {
                [frames addObject:img];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            handler([NSArray arrayWithArray:frames]);
        });
    });
}


- (NSArray<UIImage*>*)framingImage:(UIImage*)image count:(NSUInteger)count
{
    CGSize frameSize = image.size;
    frameSize.width /= count;
    
    NSMutableArray *frames = [NSMutableArray new];
    
    for (NSUInteger i = 0; i < count; i++)
    {
        CGRect rect = CGRectMake(frameSize.width * i, 0.f, frameSize.width, frameSize.height);
        UIImage *img = [image getSubImageWithRect:rect];
        
        if (img)
        {
            [frames addObject:img];
        }
    }
    
    return [NSArray arrayWithArray:frames];
}


@end
