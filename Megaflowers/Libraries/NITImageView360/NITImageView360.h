//
//  NITImageView360.h
//  NITImage360
//
//  Created by Алексей Гуляев on 20.12.16.
//  Copyright © 2016 Napoleon It. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITImageView360 : UIImageView

@property(nonatomic) NSUInteger imageFrame;
@property(nonatomic) NSUInteger imageFrameCount;

@property(nonatomic, strong) UIImage *frameImage;
@property(nonatomic, readonly) NSArray<UIImage*> *frames;

@end
